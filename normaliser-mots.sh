#!/usr/bin/sh

# Ce petit script normalise l’orthographe d’une liste de mots afin de faciliter
# les comparaisons entre bases. On met tout en majuscule, supprime la
# ponctuation, les diacritiques et les ligatures.

cat \
| awk '{print toupper($0)}' \
| sed "s/[- '’]//g" \
| sed 's/[ÁÀÂÄ]/A/g' \
| sed 's/[ÉÈÊË]/E/g' \
| sed 's/[ÍÌÎÏ]/I/g' \
| sed 's/[ÓÒÔÖ]/O/g' \
| sed 's/[ÚÙÛÜ]/U/g' \
| sed 's/[ÝỲŶŸ]/Y/g' \
| sed 's/Ç/C/g' \
| sed 's/Œ/OE/g' \
| sed 's/Æ/AE/g' \
| cat
