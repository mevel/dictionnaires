#!/usr/bin/sh

# this script sorts the log outputted by `extract-prons-frwikt.py`
# by alphabetical order of words, while preserving the order of messages
# for a given word.

# we first delete logging lines that are just there to show progress,
# and would clutter diffs unnecessarily.
#
# log messages are of the following form:
#
#     INFO: "word spelling": remainder of the message
#     ^ or ATTENTION|ERREUR|ANOMALIE|...
#
# by splitting on double quotes (--field-separator),
# we obtain the word spelling as the 2nd “field”; we use it as the --key,
# and we preserve the order of messages for a given word (--stable).
#
# message lines that are not specific to a word
# (such as the global statistics shown at the end of processing)
# contain no double quote. they are sorted before all other lines;
# and their relative ordering is preserved.
#
grep -E -v '^INFO: [0-9_]*000e article traité:' \
| sort --field-separator '"' --key 2,2 --stable
