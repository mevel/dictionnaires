#!/usr/bin/sh

# this script sorts the log outputted by `extract-prons-frwikt.py`,
# turns it into wiki syntax (in order to copy-paste it into a wiki page),
# and distributes the log messages into several files according to their kind.

# output file for other messages:
out_misc=log.divers.txt
# output file for messages about bad pronunciations:
out_pronbad=log.mauvaise-pron.txt
# output file for messages about rectification of pronunciations:
out_pronrect=log.pron-rectifiée.txt

tmp_file1="$(mktemp)"
tmp_file2="$(mktemp)"

# sort the messages by word spelling,
# then add a bit of wiki-formatting:

"$(dirname "$0")"/sort-log.sh \
| sed -E '
    # add list bullets to each message:
    s/^/# /;
    # turn word spellings into wiki-links:
    s/"([^"]*)":/"[[\1]]":/;
    # escape template names:
    s/\{\{/{{modèle|/;
' > "$tmp_file1"

# classify log messages into 3 files:

pat_misc='": plusieurs sections|": section ou type gram|": plusieurs lemmes|": plusieurs types|": désaccord ligne de forme|": {{modèle\|fr-'
pat_pronrect='": pron\. rectifiée|": prons\. fusionnées'

grep -E    "$pat_misc" < "$tmp_file1" > "$out_misc"
grep -E -v "$pat_misc" < "$tmp_file1" > "$tmp_file2"

grep -E    "$pat_pronrect" < "$tmp_file2" > "$out_pronrect"
grep -E -v "$pat_pronrect" < "$tmp_file2" > "$out_pronbad"

rm -- "$tmp_file1" "$tmp_file2"

echo 'nombre de messages:'
exec wc -l -- "$out_misc" "$out_pronbad" "$out_pronrect"
