Le programme `extract-prons-frwikt.py` traite un dump XML du Wiktionnaire
francophone pour en extraire la prononciation des mots français. Lire [cette
présentation][annonce-wikidemie] pour quelques explications concernant les
opérations effectuées (nettoyage et fusion des prononciations, notamment) et les
messages affichés.

[annonce-wikidemie]: https://fr.wiktionary.org/wiki/Wiktionnaire:Wikidémie/novembre_2023#Extraction_de_prononciations%2C_le_retour

## Usage

Ce programme lit le dump XML depuis le fichier `frwikt.xml`, il faut donc
s’assurer que ce fichier existe et que, si c’est un lien symbolique, sa cible
existe également. On peut se procurer un dump du Wiktionnaire francophone
[ici][dumps-frwikt] (il faut prendre le fichier `***-pages-articles.xml.bz2` et
veiller à décompresser l’archive).

[dumps-frwikt]: https://dumps.wikimedia.org/frwiktionary/

Utilisation en ligne de commande :

    # [préliminaires, à ne faire qu’une seule fois]
    # crée un environnement Python local et y installe "mwparserfromhell" :
    # (plus d’infos : https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/ )
    python -m venv .python_env
    .python_env/bin/pip install mwparserfromhell

    # [à faire dans chaque console où on veut exécuter le programme]
    # active l’environnement local :
    source .python_env/bin/activate

    # enregistre le tableur résultat dans le fichier `frwikt.tsv` ;
    # les erreurs et avertissements s’affichent dans la console et sont
    # enregistrés dans le fichier `log.txt` :
    ./extract-prons-frwikt.py 2>&1 > frwikt.tsv | tee log.txt

Attention, le XML est très gros (environ 5 Gio) et donc le programme prend un
certain temps pour tout traiter (1H chez moi). Si jamais l’exécution est
interrompue pour une quelconque raison, on peut relancer le programme en
indiquant sur sa ligne de commande le dernier mot déjà traité ; alors, le
programme sautera tous les mots jusqu’à celui-ci :

    ./extract-prons-frwikt.py DERNIER_MOT_TRAITÉ 2>&1 > frwikt-part2.tsv | tee log-part2.txt

Il suffira ensuite de concaténer les tableurs produits par chaque séquence
(attention aux en-têtes).

Les mots sont traités dans un ordre arbitraire (l’ordre dans lesquels ils
apparaissent dans la source XML, probablement l’ordre d’ancienneté dans le
Wiktionnaire). Pour trier le tableur résultat par ordre alphabétique :

    # trier le tableur résultat :
    ./sort-tsv.sh < frwikt.tsv > frwikt-trié.tsv

Le programme affiche des (dizaines de) milliers d’erreurs/avertissements,
indiquant souvent des erreurs dans le Wiktionnaire, qu’il faudrait corriger.
On peut trier tous ces messages et les répartir par thématique :

    # trier les messages par ordre alphabétique de mot-vedette,
    # et les répartir dans plusieurs fichiers thématiques:
    ./split-log.sh < log.txt

Afin de détecter des prononciations suspectes, le programme calcule un score
pour chaque prononciation rencontrée. Il enregistre tous les scores dans le
fichier `scores.txt`. Si l’on souhaite lister les prononciations les plus
suspectes, c’est-à-dire avec le score le plus élevé, on peut trier le fichier
ainsi :

    # trier les scores en ordre décroissant :
    ./sort-scores.sh < scores.txt > scores-trié.txt

## Résultat

Le programme ne considère que les mots français et ignore un certain nombre de
catégories, comme les abréviations, les noms propres et les proverbes. Pour
l’instant, il ignore également le contenu des sections `Prononciation` car leur
format est trop anarchique pour un traitement automatique.

Le programme produit un tableur TSV (un mot par ligne, champs séparés par des
tabulations) avec les champs suivants :

  - **`ortho` : l’orthographe** du mot (par exemple `hussarde`)
  - **`cgram` : la catégorie grammaticale** du mot sous une forme abrégée ;
    si le mot est une flexion, la catégorie est suffixée par une astérisque `*`
    (par exemple `ADJ*` pour un adjectif fléchi, `ARTdef` pour un article défini)
  - **`num` : le numéro d’ordre** du mot pour cette catégorie, utile en cas d’homographes
    (correspond au numéro dans « Nom commun 1, Nom commun 2, etc. »)
  - **`lem` : le lemme** (lacunaire) ;
    en cas de variantes orthographiques, il peut y avoir plusieurs lemmes,
    séparés par des points-virgules
    (par exemple, `jeune fille; jeune homme` pour « jeunes gens »)
  - **`gn` : le genre et le nombre** (lacunaire), codés par un caractère chacun
    (par exemple `fs`) :
    + genre :
      * `-` non indiqué,
      * `m` masculin,
      * `f` féminin,
      * `+` masculin et féminin identiques (épicène),
      * `?` l’usage hésite
    + nombre :
      * `-` non indiqué,
      * `s` singulier,
      * `p` pluriel,
      * `S` uniquement singulier (_singulare tantum_),
      * `P` uniquement pluriel (_plurale tantum_),
      * `+` singulier et pluriel identiques
  - **`conj` : les conjugaisons** du lemme auxquelles ce mot correspond,
    séparées par des points-virgules ; les conjugaisons suivent la syntaxe du
    modèle `{{fr-verbe-flexion}}`
    (par exemple `imp.p.2s; ind.p.1s; ind.p.3s; sub.p.1s; sub.p.3s`) ;
  - **`phon` : les prononciations extraites**, séparées par des points-virgules ;
    les H aspirés sont indiqués par `ʔ`, les H muets par `‿` ;
    (par exemple `ʔy.saʁd` pour « hussarde », `jɛʁ; ‿i.jɛʁ` pour « hier ») ;
    les prononciations suspectes sont préfixées par des points d’exclamation `!`
    dont le nombre augmente avec le degré de suspicion ;
    le programme ne sait pas interpréter certains modèles d’accord rares,
    il se contente de rendre ces modèles tels quels dans le tableur
    (par exemple `ɛ.mɑ̃; {{fr-accord-t-avant1835|aiman|ɛ.mɑ̃}} pour « aimans »`).

Améliorations possibles :

  - [ ] simplifier et normaliser un peu l’écriture des prononciations (facile) ;
    en particulier, ceci améliore le dédoublonnage des prononciations
    + [x] nettoyer les caractères Unicode
    + [x] nettoyer les bugs de syllabation
    + [ ] optionnellement : remplacer les caractères Unicode de l’API par un
      alphabet ad-hoc en ASCII, comme celui utilisé par Lexique3 par exemple
      (réduit un peu la taille du fichier, facilite la saisie)
    + [ ] normaliser la façon de noter les sons optionnels
      et les altérations de la syllabation qui en résultent
      - [ ] schwa optionnel: `[gaʁ,d(ə).fʁɔ̃.tjɛʁ]`
        (la virgule marque où couper si la parenthèse suivante est prononcée)
      - [ ] liaison optionnelle: `[a.si,d(.z)‿al.kɔl]` (de même)
      - [ ] interpréter les parenthèses dans les prononciations extraites,
        et les corriger selon notre convention
  - [ ] fusion intelligente des prononciations :
    + [x] si deux prononciations donnent des précisions différentes,
      alors garder toutes les précisions des deux prononciations
      - [x] syllabes
      - [x] h muets ou aspirés
      - [x] voyelles alternatives
        (`[ɑ]` préféré sur `[a]`, `[ɛ]` sur `[e]`, `[o]` sur `[ɔ]`)
    + [ ] compacter les différences (si deux prononciations ne diffèrent
      que de quelques sons)
      - [ ] avec ou sans schwa
      - [ ] avec ou sans liaison
  - [ ] **graphémisation**
  - [ ] prendre en compte la graphémisation dans la syllabation
    (schwas, mots composés, sigles)
  - [x] interpréter tous les modèles d’accord (facile mais fastidieux)
    + il manque seulement `{{fr-accord-t-avant1835}}` et `{{fr-accord-personne}}`
  - [ ] exploiter aussi les pages de conjugaison ([exemple][ex-p-conjug])
    (facile mais fastidieux)
  - [ ] **interpréter les lignes de définition des flexions**,
    du style « _Féminin pluriel de_ » (difficile ?)
  - [ ] vérifier l’indication `3=flexion` du modèle `{{S}}`
  - [ ] **relier les flexions à leurs lemmes** pour compléter leurs informations
    (difficile ? car il faut identifier la section correspondant au lemme)
    + nécessaire pour exploiter des infos comme `{{équiv-pour}}`, `{{avant
      1835}}`, variante orthographique… dans les flexions
  - [ ] ré-associer masculin et féminin pour les noms communs, en exploitant
    `{{équiv-pour}}`
    + pour des paires comme « joueur/joueuse »,
      mais pas pour des mots réellement différents comme « homme/femme »
  - [ ] déduire la prononciation manquante d’un nom commun de celle d’un
    adjectif identique, ou réciproquement
  - [ ] vérifier la cohérence des prononciations entre la ligne de forme et le
    modèle d’accord (créera *beaucoup* d’alertes)
  - [ ] pour chaque lemme, vérifier la cohérence des prononciations données par
    la page (modèle d’accord) de chaque flexion
  - [ ] extraire les informations de régions pour les associer aux
    prononciations (difficile car format anarchique)
  - [ ] extraire davantage de relations entre mots, comme les variantes
    orthographiques (notamment réforme de 1990)
  - [ ] extraire les informations de désuétude, comme `{{désuet}}`,
    `{{archaïque}}`, `{{avant 1835}}`… supprimer ces mots ?
  - [ ] ignorer divers mots :
    + [ ] abréviations signalées par `{{abréviation|fr}}` ([exemple][ex-abr])
    + [ ] sigles signalés par `{{sigle|fr}}` ([exemple][ex-sigle])
    + [ ] mots très rares / hapax signalés par `{{extrêmement rare}}` ([exemple][ex-extr-rare])
    + [ ] néologismes inclusifs signalés par `{{term|Langage épicène}}`,
      `{{non standard}}`… ([exemple][ex-inclusif])
    + [ ] néologismes de fiction signalés par `{{term|Fiction}}` ([exemple][ex-fiction])
  - [ ] trier le tableur par lemme (facile)
  - [x] ne pas extraire le modèle `{{pron}}` dans les textes d’exemples (pose
    problème pour `phone`, `allophone`, `syllabe ouverte`, `syllabe fermée`,
    `sigmatisme latéral`)

[ex-p-conjug]: https://fr.wiktionary.org/wiki/Conjugaison:français/faire
[ex-abr]: https://fr.wiktionary.org/wiki/i.-e.
[ex-sigle]: https://fr.wiktionary.org/wiki/MGEN
[ex-extr-rare]: https://fr.wiktionary.org/wiki/fest-nozs#fr
[ex-inclusif]: https://fr.wiktionary.org/wiki/ceulles
[ex-fiction]: https://fr.wiktionary.org/wiki/soirétoile
