#!/usr/bin/sh

# this script sorts a TSV spreadsheet of words,
# as outputted by `extract-prons-frwikt.py`

# preserve the header line:
IFS= read -r header
echo "$header"

# sort the remaining lines:
#
# --key 1,1 sets 1st field as primary key, disregarding what comes after
#     (e.g. with this option "testé\tVER" is sorted before "testés\tADJ";
#     without this option \t would be ignored and these 2 lines swapped).
#
# the primary keys (i.e. word spellings) are sorted case-insensitively.
#
# lines with identical keys are sorted according to their whole contents.
#
# [for homographs this is slightly unstable: lines with identical first fields
# (i.e. spelling and grammatical kind) are sorted according to later fields,
# which tend to be more changing data (i.e. gender, number, pronunciation).]
#
exec sort --field-separator $'\t' --key 1,1 --ignore-case
