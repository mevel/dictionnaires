#!/usr/bin/env python

# USAGE: see README.md

################################################################################
### Basic utilities ############################################################
################################################################################

# To parse XML, we use the standard ElementTree library:
#     https://docs.python.org/3/library/xml.etree.elementtree.html
#
# To parse MediaWiki code, we use mwparserfromhell, which can be installed via
# `pip` and has its doc here:
#     https://mwparserfromhell.readthedocs.io/en/latest/
#
# NOTE:
#   An extensive list of MediaWiki parsers in Python:
#       https://www.mediawiki.org/wiki/Alternative_parsers )
#   The most interesting ones:
#     - mwparserfromhell https://github.com/earwig/mwparserfromhell/
#     - wikitextparser https://github.com/5j9/wikitextparser
#   A project that parses the English wiktionary to extract contents such as
#   pronunciations and etymology to a machine-readable format:
#       https://github.com/Suyash458/WiktionaryParser

import xml.etree.ElementTree as xmltree
import mwparserfromhell as mw
import re
import sys
import enum
from dataclasses import dataclass

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

################################################################################
### Wikicode utilities #########################################################
################################################################################

# Clean up wikicode by expanding useful templates, stripping styling tags and
# generally removing unwanted stuff w.r.t. our goal (extracting pronunciations
# and spellings; it is important that spellings are stripped because we need to
# compare them in order to find the relevant word form in agreement templates).
def strip_wikicode(wc):
    # expand HTML entities:
    for ent in wc.ifilter_html_entities(recursive=True):
        c = ent.normalize()
        # we replace normal spaces by NBSPs for now,
        # otherwise they would be lost when trimming parameter values:
        if c == ' ':
            c = '\u00A0'
        wc.replace(ent, c, recursive=True)
    # expand or strip some useful templates:
    for tpl in wc.ifilter_templates(recursive=True):
        if not wc.contains(tpl):  # we might have removed some ancestor of `tpl`
            continue
        tname = template_name(tpl)
        # expand {{liaison}} (used in pronunciations):
        if tname == 'liaison':
            wc.replace(tpl, '‿', recursive=True)
        # strip subscripts and superscripts (used in displayed spellings):
        elif tname in ['o', 'O']:
            wc.replace(tpl, 'o', recursive=True)
        elif tname == 'er':
            wc.replace(tpl, (tpl.get(1).value if tpl.has(1) else 'er'), recursive=True)
        elif tname in ['re', 'ère']:
            wc.replace(tpl, (tpl.get(1).value if tpl.has(1) else 're'), recursive=True)
        elif tname in ['e', 'ex', 'ème']:
            wc.replace(tpl, (tpl.get(1).value if tpl.has(1) else 'e'), recursive=True)
        elif tname in ['in', 'indice']:
            wc.replace(tpl, (tpl.get(1).value if tpl.has(1) else ''), recursive=True)
        # remove {{!}} and everything that comes after it,
        # if occuring in a template parameter
        # ({{!}} is used to separate a stripped-down spelling (suitable for
        # a wiki-link for instance) from a displayed spelling (eg. with markup);
        # we keep the stripped-down spelling):
        elif tname == '!':
            try:
                parent = wc.get_parent(tpl)
                if isinstance(parent, mw.nodes.template.Template):
                    # find which parameter of the parent template {{!}} is in:
                    param = next(param for param in parent.params if param.value.contains(tpl))
                    # remove {{!}} and everything after it:
                    i = param.value.index(tpl)
                    del param.value.nodes[i:]
            except (StopIteration, ValueError): pass  # should not happen
    # strip HTML/wiki markup:
    for tag in wc.ifilter_tags(recursive=True):
        if not wc.contains(tag):  # we might have removed some ancestor of `tag`
            continue
        # strip styling markup (used in displayed spellings):
        tname = str(tag.tag)
        if tname in ['b', 'i', 'u', 'small', 'sup', 'sub']:
            wc.replace(tag, tag.contents, recursive=True)
        # remove <ref> entirely (incorrectly used in some pronunciation fields,
        # and there are also occurrences in the "ligne de forme" with wrong
        # {{pron}}):
        elif tname == 'ref':
            wc.remove(tag)
    # strip unparsed wiki styling markup (''italic'' and '''bold'''):
    # NOTE: we don’t use `wc.remove` or `wc.replace` because they wreak havoc on
    # the syntax tree, it seems!
    if wc.contains("''"):
        for txt in wc.ifilter_text(recursive=True):
            txt.value = txt.value.replace("'''", "").replace("''", "")

# Returns the name of the wiki template, as a string:
def template_name(t):
    return str(t.name).strip().replace('_', ' ')

# Converts wikicode to a string that is trimmed, and (after trimming)
# non-breaking spaces are replaced with normal spaces:
def str_of_wikicode(wc):
    return str(wc).strip(' \t\n').replace('\u00A0', ' ')

# Returns the value of a given parameter of the wiki template, as a string:
def template_param(t, pname, default=None):
    if default == None:
        return str_of_wikicode(t.get(pname).value)
    else:
        return str_of_wikicode(t.get(pname).value) if t.has(pname) else default

# Converts the wikicode template to a Python dictionary,
# trimming and converting everything to strings.
# Empty named arguments are kept; empty numbered arguments are thrown.
def dict_of_template(t):
    d = dict()
    for p in t.params:
        name = str(p.name).strip()
        value = str_of_wikicode(p.value)
        if value != '' or name not in ['1', '2', '3', '4', '5', '6', '7', '8']:
            d[name] = value
    return d

# Returns a string representation of the wiki template
# that does not contain tabulation nor newline characters:
blank_flattening_map = str.maketrans('\t\n', '  ')
def flatten_template(t):
    s = str(t)
    # remove blanks before }} and |
    s = re.sub(r'\s+(?=}}|\|)', '', s)
    # remove blanks after {{ and |
    s = re.sub(r'(?<={{|.\|)\s+', '', s)
    # replace remaining offending blanks with spaces (last resort):
    s = s.translate(blank_flattening_map)
    return s

################################################################################
### Pronunciation characters and cleanup #######################################
################################################################################

# Our notation to materialize a mute or aspirated h at the beginning of a word:
aspi_h = 'ʔ'
mute_h = '‿'

# The characters that are expected in pronunciations.
# Characters outside this set are not rejected, but they trigger a warning.
good_pron_chars = ''
# punctuation and diacritical signs, useful for French:
good_pron_chars += '…() \u00A0.‿ː'
good_pron_chars += '\u0361' # combining inverted breve above, joining 2 consonants (affricates)
#good_pron_chars += '\u032F' # combining inverted breve below, for short vowels (diphthongs)
good_pron_chars += '\u0303' # combining tilde, for nasal vowels
# (non-nasal) French vowels:
good_pron_chars += 'aɑeɛøœəioɔyu'
#good_pron_chars += 'ɪ' # not standard French, but used in Canadian accent
# French semi-vowels:
good_pron_chars += 'jɥw'
# French consonants:
good_pron_chars += 'gkbpdtvfzsʒʃmnɲŋlʁ'
good_pron_chars += '\u0261' # variant of 'g', to be substituted (accepted because dominant)
good_pron_chars += 'ʔ' # glottal stop; used for French (mainly for aspirated h),
                       # this symbol is silent but indicates a disjunction
                       # between sounds before and after, preventing a liaison
good_pron_chars += 'x' # technically not a French sound, but familiar enough
                       # that it deserves inclusion (allophone of [ʁ] in French;
                       # used in the neighboring languages Spanish and German)

good_pron_chars = set(good_pron_chars)

tilde = '\u0303'
vowels_allowed_with_tilde = 'ɑɛœɔ'
nasal_vowels = { v + tilde for v in vowels_allowed_with_tilde }

pat_vowel = fr'(?:[aɑeɛøœəiɪoɔyu]{tilde}?ː?)'
pat_semivowel = r'[jɥw]'
pat_consonant           = r'[gɡkbpdtvfzsʒʃhʔmnɲŋlʁx]'
pat_consonant_except_S  = r'[gɡkbpdtvfzʒʃhʔmnɲŋlʁx]'
pat_consonant_except_RL = r'[gɡkbpdtvfzsʒʃhʔmnɲŋ]'
pat_consonant_before_R  = r'[gɡkbpdtvf]'
pat_consonant_before_L  = r'[gɡkbpf]'
pat_consonant_seq = fr'(?:{pat_consonant}|{pat_consonant_before_R}[ʁx]|{pat_consonant_before_L}l)'

cpat_vowel_dirty = \
    re.compile(fr'(?:[aɑeɛøœəiɪoɔyuàâêéèëîïôùûüÿAÀÂEÉÈÊËIÎÏOÔUÙÛÜYŸ]{tilde}?)')
pat_consonant_dirty = r'[gɡkbpdtvfszʃʒhʔmnɲŋlʁxBcCçÇDFGhHJKLMNPqQrRSTVXZ]'

##
## Pronunciation score
##

# To detect many erroneous pronunciations (often caused my misuses of agreement
# templates in the French Wiktionary), we compute a score based on the spelling
# and pronunciation. The higher the score, the more suspicious the pronunciation.
#
# Examples of erroneous pronunciations (from the September 2023 version of the
# Wiktionary) that we want to detect:
#
#     "hrissehs" [fr]
#     "ravalées" [fr]
#     "incriminées" [fr]
#     "infratidaux" [fro]
#     "infratidale" [fral]
#     "infratidales" [fral]
#     "accipitridés" [pron]
#     "organismes végétaux" [en cours]
#     "soute" [soute]
#     "testateur" [testateur]
#     "immotif" [immotif]
#     "noyau basal" [noyau basal]
#     "garde-nationale" [garde-nationale]
#     "hot rod" [hot ʁod]
#     "neveux" [neveu]
#     "trameuses" [tram]
#     "sauteur en hauteur" [saut]
#     "pelletier" [pelleti]
#     "montignier" [montiɡnie]
#     "lomonier" [Lomonie]
#     "alimenteur" [aliment]
#     "vrai-faux" [vrai-fo]
#     "aveugles" [aveugler]
#     "usager" [usagie y.za.ʒe]
#     "péri-natal" [périnatal pe.ʁi.na.t]
#     "péri-natale" [périnatal pe.ʁi.na.t]
#     "péri-natales" [périnatal pe.ʁi.na.t]
#     "bi-racial" [biracial biʁa.sj]
#     "mononationaux" [binationo mɔ.nɔ.na.sjɔ̃.n]
#     "fétiaux" [fécio fe.sj]
#     "biacromial" [bja.kʁɔ.mjalbjakʁɔmjal]
#     "biacromiaux" [bja.kʁɔ.mjobjakʁɔmjo]
#     "uropygiaux" [uʁopyɡio]
#     "pentagonaux" [pentaɡono]
#     "lacaniens" [lacaniɛ̃ la.ka.nj]
#     "sévérien" [sévériɛ̃ se.ve.ʁj]
#     "sévérienne" [sévériɛn se.ve.ʁj]
#     "raëlienne" [raëliɛn ʁa.ɛ.lj]
#     "collégiennes" [collégiɛn kɔ.le.ʒj]
#     "tarsiens" [tarsiɛ̃]
#     "lyciennes" [lyciɛn]
#     "estonienne" [estoniɛn]
#     "pomasiens" [pomasiɛ̃]
#     "dehoniens" [dehoniɛ̃]
#     "déhérisien" [dehérisiɛ̃]
#     "Jerseyens" [Jerseyɛ̃]
#     "jungiens" [iungiɛ̃]
#     "jungiennes" [iungiɛn]
#     "jaurésiennes" [jaurésiɛn]
#     "sus-hyoïdien" [sus-hyoïdiɛ̃]
#     "Marcelin" [Marcelɛ̃ maʁ.sə.lɛ̃]
#     "napolitaine" [napolitɛn na.pɔ.li.t]
#     "antraiguains" [ɑ̃.tʁɛ.ɡɛ̃ antraiguain]
#     "transatier" [transate tʁɑ̃.za.tj]
#     "transatière" [transatɛʁ tʁɑ̃.za.tj]
#     "pierritière" [pieʁʁitiɛʁ]
#     "potinière" [potiniɛʁ]
#     "pulluleux" [pullulø]
#     "footballeux" [footballø]
#     "translumineux" [tʁansluminø]
#     "toussoteur" [toussotœʁ]
#     "lumbersexuels" [lumbersexuɛl]
#     "interactionnelle" [interactionnɛl]
#     "soigneur d’animaux" [soign]
#     "induvial" [induvial]

# thresholds above which we report the pronunciation as suspicious:
pron_score__threshold__garbage = 105
pron_score__threshold__suspicious = 75
pron_score__threshold__slightly_suspicious = 55

# Suspicious pronunciations will be prefixed with exclamation marks;
# the more suspicious, the more exclamation mark:
def mark_suspicion(p, lvl=None, score=None):
    if lvl == None:
        assert score != None
        if score >= pron_score__threshold__garbage:
            lvl = 3
        elif score >= pron_score__threshold__suspicious:
            lvl = 2
        elif score >= pron_score__threshold__slightly_suspicious:
            lvl = 1
        else:
            lvl = 0
    return ('!' * lvl) + p
#
# the function below counts the number of leading exclamation marks:
def level_of_suspicion(p):
    i = 0
    for c in p:
        if c != '!':
            return i
        i += 1
    return len(p)

# coefficients associated to various features for computing the score:
pron_score__coeff__longest_chunk = 10  # for overly long syllables, likely missing syllabation
pron_score__coeff__ratio = 60  # for overly long or overly short pronunciations (see below)
pron_score__bonus__pron_is_prefix = 100  # for when the pron is a prefix of the spelling (possibly a misuse of agreement templates)
pron_score__bonus__pron_has_suffix = 50  # for when the pron is a prefix of the spelling + some pron suffix (idem, see below)

# among other things, the score calculation uses the length ratio between
# spelling and pronunciation; outliers (when pronunciation is significantly
# shorter, or significantly longer, than spelling) are likely to be errors;
#
# some useful statistics about this ratio:
#   pron / spelling:
#     median ratio = 1.0
#     mean ratio = 1.038593469827428
#   spelling / pron-no-syl:
#     median ratio = 1.25
#     mean ratio = 1.2857389604427427
#   all (+) ratios >= 3.4 are errors
#   many (-) ratios >= 1.5 are errors
#
# this is the mean ratio of typical words (where the pronunciation string has no
# syllable info, i.e. dots and spaces):
pron_score__mean_ratio = 1.29
# we ignore the ratio for some words which are likely abreviations, using the
# following pattern:
pron_score__pat_no_ratio = \
    re.compile(r'^.$|^\w{,4}$|[.·&º°*+/%ω0-9⁰¹²³⁴⁵⁶⁷⁸⁹₀₁₂₃₄₅₆₇₈₉½¼¾⅓⅔⅕⅖⅗⅘⅙⅚]|[^- ][A-Z]|à\w')

# suspicious pronunciation patterns, with coefficients:
suspicious_pron_patterns = {
  (re.compile(pat), sc) for (pat, sc) in {
    (r'.-.', 100),
    (r'^.*[‿.].*$', -4),
    (r'[‿ːɑɪəɥɲŋɡʃʒʔ]', -2),
    (r'[.ɛøɔʁyk]', -1),
    (r'[go]', 1),
    (r'[-xh]', 3),
    (r'[’cçqréè]', 6),
    (r'[àâêëîïôùûüÿA-ZÀÂÉÈÊËÎÏÔÙÛÜŸ]', 100),
    (r'bb|cc|dd|ff|[gɡ][gɡ]|kk|ll|mm|nn|pp|[rʁ][rʁ]|ss|tt|vv|zz', 100),
    (r'gn(?![aɑɔo])', 25),
    (r'ign', 50),
    (r'[cpt]h|qu', 50),
    (r'^h', 50),
    (r'^[‿ʔ]h', 200),
    (r'ai|au|ei|eu|oi', 4),
    (r'ain|ein|oin', 12),
    (r'iɛ|io|ou|ui', 30),
    (r'i[eéè]|eau|œu|aux|eux|oui|oï|uï', 50),
    (fr'{cpat_vowel_dirty.pattern}{{3}}', 100),
    (fr'{tilde}{cpat_vowel_dirty.pattern}', 50), # [ʒɔ.ve.ɛ̃ɛ̃, ʒɔ.ve.ɛ̃ɛn, sak.sɔ̃ɔ̃, ku.ʁɑ̃e]
    (r'ɛɛ', 50), # [my.ni.ʃjɛɛn, ve.ne.zɥe.ljɛɛ̃]
    (r'ee', 50), # [tweetøz, lɑ̃.ɡa.ʒjee, ã.bɛʁ.lu.viee]
    (r'oo', 50), # [footballø, pʁe.voo]
    #(r'tt|ff|fv|vv', 200), # [mal.fə.zɑ̃tt, ʒɥiff, ʒɥifv, fɔ̃.da.tivv]
    (r'(?:aal|alal|alo|oo|ɑ̃ɑ̃|ɑ̃an|anan|oɛl|ɛlɛl|ee|eɛʁ|ɛʁɛʁ|ɛɛ|ɛɛt|ɛtɛt|ɛ̃ɛ̃|ɛ̃ɛn|ɛnɛn|ɛ̃in|inin|ɔlɔl|ɔ̃ɔ̃|ɔ̃ɔn|ɔnɔn|øø|øøz|œʁœʁ|œʁøz|øzøz|œʁʁ?is|ʁ?isʁ?is|ff|fv|vv)(?: |$)', 200),
    # [spi.naal, e.ly.vjalal, va.ʒi.nalo, pi.zanan, kɔ.lɔ.nɛlɛl, lɑ̃.ɡa.ʒjee,
    # pɑ̃.ta.lɔ.njeɛʁ, ɛ̃.fiʁ.mjɛʁɛʁ, ʒɔ.ve.ɛ̃ɛ̃, ʒɔ.ve.ɛ̃ɛn, vɑ̃.de.ɛnɛn, sak.sɔ̃ɔ̃,
    # sak.sɔnɔn, o.ne.ʁøøz, e.pi.lœʁœʁ, ...]
    (r'[ɥw][jɥw]', 200), # [ɑ̃.nɥjœ, ʒwje, wjɛ.nwaz, sɑ̃.ta.lwwan]
    (fr'\.{pat_consonant_dirty}*[. ]', 50),
    (fr'\.{pat_consonant_dirty}*$', 100),
    (fr'(?:{pat_consonant_dirty}|\.){pat_semivowel}$', 200),
    (r'(?:^| )fr$', 200),
    (r' ou ', 200),
}}

# sensible spelling-pronunciation pairs at the beginning of words; when the word
# does not match one of these patterns, it is likely an erroneous pronunciation,
# we add a large value to the score:
#
# NOTE: In the future, I’d like to extend this to the full word, so as to obtain
# a decomposition of words into graphemes, associated to their pronunciation.
# That’s data I am interested in, generally speaking, and it would also be the
# nuclear weapon of error detection. I’ve done it for a smaller word base
# (Lexique3). But, due to the large input set of the Wiktionary, with loosely
# controled character sets for spelling and pronunciation, it still needs a lot
# of work.
pron_score__bonus__beginning = 1000
nonsuspicious_pron_beginnings = {
  (re.compile(pat_spelling), re.compile(pat_pron)) for (pat_spelling, pat_pron) in {
    #(r'[aâàAÂÀ]', r'[aɑoɔeɛ]'),
    (r'[aâàAÂÀ](?![uiîymn])', r'[aɑ]'),
    (r'[aâàAÂÀ][mn]', r'[aɑ]ː?[mnɲ]|ɑ̃'),
    (r'[aâàAÂÀ][iîy](?![mn])', r'[aɑ][ij]|[eɛ]'),
    (r'[aâàAÂÀ][iîy][mn]', r'([aɑ][ij]|[eɛ]ː?)[mnɲ]|ɛ̃'),
    (r'[aâàAÂÀ]u', r'[oɔ]'),
    (r'[æÆ]|[aA]e', r'[eɛ]'),
    (r'[bB]', r'b'),
    #(r'[cC]', r'[ksʃ]|t͡?ʃ|\(t\)ʃ'),
    (r'[cC](?!h)', r'[ks]'),
    (r'[cC]h', r'[kʃ]|t͡?ʃ|\(t\)ʃ'),
    (r'[çÇ]', r's'),
    (r'[dD]', r'd'),
    #(r'[eE]', r'[eɛøœəoɔ]|ɑ̃'),
    (r'[eE](?![iîymn]|au)', r'[eɛøœə]'),
    (r'[eE][mn]', r'[eɛøœə]ː?[mnɲ]|ɑ̃'),
    (r'[eE][iîy](?![mn])', r'[eɛ]'),
    (r'[eE][iîy][mn]', r'[eɛ]ː?[mnɲ]|ɛ̃'),
    (r'[eE]au', r'[oɔ]'),
    (r'[éÉ]', r'[eɛ]'),
    (r'[èêÈÊ]', r'[eɛ]'),
    (r'[œŒ]|oe', r'[eɛœø]'),
    (r'[fF]', r'f'),
    (r'[gG]', r'[gɡʒɲ]|nj|d͡?ʒ|\(d\)ʒ'),
    (r'[hH]', r''),
    #(r'[iîIÎ]', r'[ij]|ɛ̃'),
    (r'[iîIÎ](?![mn])', r'[ij]'),
    (r'[iîIÎ][mn]', r'iː?[mnɲ]|ɛ̃'),
    (r'[jJ]', r'[ʒj]|d͡?ʒ|\(d\)ʒ|[xʁ]'),
    (r'[kK]', r'k'),
    (r'[lL]', r'l'),
    (r'[mM]', r'm'),
    (r'[nN]', r'n'),
    (r'[nN]i', r'ɲ'),
    #(r'[oôOÔ]', r'[oɔuw]'),
    (r'[oôOÔ](?![uûùiîymn])', r'[oɔ]'),
    (r'[oôOÔ][mn]', r'[oɔ]ː?[mnɲ]|ɔ̃'),
    (r'[oôOÔ][iîy](?![mn])', r'wa|[oɔ]j'),
    (r'[oôOÔ][iîy][mn]', r'(wa|ɔj)[mnɲ]|wɛ̃'),
    (r'[oôOÔ][uûù]', r'[uw]'),
    (r'[pP](?!h)', r'p'),
    (r'[pP]h', r'f'),
    (r'[qQ]', r'k'),
    (r'[rR]', r'[rʁ]'),
    (r'[sS]', r'[sʃz]'),
    (r'[tT]', r't'),
    #(r'[uûUÛ]', r'[ɥyu]|œ̃|ɔ̃'),
    (r'[uûUÛ](?![mn])', r'[ɥyu]'),
    (r'[uûUÛ][mn]', r'[yu]ː?[mnɲ]|œ̃|ɔ̃'),
    (r'[vV]', r'v'),
    (r'[wW]', r'[vw]'),
    (r'[xX]', r'[gɡ]z|ks|[xʁ]'),
    (r'[yŷÿYŶŸ]', r'[ij]'),
    (r'[zZ]', r'z|d͡?z|t͡?s'),
    # special cases: French:
    (r'[iI]mm', r'i\(m\)m'),
    (r'[iI]nn', r'i\(n\)n'),
    (r'[oO]nz', r'ʔɔ̃z'),
    (r'eue?s?($|[- ])|euss|eurent$|e[uû]t|eûmes$', r'y'),
    (r'oign', r'[oɔ]ɲ'),
    (r'ï', r'j'),
    (r'[aA]o', r'[oɔ]'),
    (r'[aA]o[uû]t', r'u|\([aɑ]\)u'),
    (r'ê', r'aɛ̯'),
    (r'petit', r'ti'),
    # special cases: misc languages:
    (r'[gG]b', r'b'),
    (r'[nN]’?g', r'ŋɡ'),
    (r'[mM]', r'ᵐ'),
    (r'[nN]', r'ⁿ'),
    (r'[ñÑ]|[nN]h', r'ɲ'),
    (r'[eE]ng', r'[eɛ]ŋ'),
    (r'[cC]i', r't͡?ʃ|\(t\)ʃ'),
    (r'[cC]z', r't͡?s|t͡?ʃ'),
    (r'[kK]h', r'[xʁχ]'),
    (r'[zZ]hu', r'ʃw|t͡?ʃw|d͡?ʒw|d͡?w'),
    (r'[qQ](?!u)', r'ʃ|t͡?ʃ'),
    (r'[xX]', r'ʃ'),
    (r'[aA]a', r'o'),
    # special cases: English:
    (r'[aA][- ]', r'ɛj'),
    (r'[eE][- ]|email|emark|emote|Ewok|eSIM', r'i'),
    (r'[iI][- ]', r'aj'),
    (r'[eE]a', 'i'),
    (r'[oO]o', r'u'),
    (r'[üÜ]', r'y'),
    (r'[uU]', r'ju'),
    (r'[uU]m', r'œm'),
    (r'[uU]n', r'œn'),
    (r'[uU]p', r'œp'),
    (r'[oO]ut', r'awt|[aɑ]ut'),
    (r'[tT]h', r's'),
    (r'[kK]n.ck', r'n.k'),
    (r'[aA]ll', r'ol'),
    (r'[aA]ny', r'[eɛ]n[iɪ]'),
    (r'[oO]ne', r'wan'),
    (r'ace', r'[eɛ]j'),
    (r'ice', r'aj'),
    (r'[wW]r', r'ʁ'),
    (r'Iow', r'ajo'),
    (r'[iI]rish', r'aj'),
    (r'[eE]i(?:[ns]|ch)', 'aj'),
    # special cases: acronyms:
    (r'lg', r'[eɛ]lʒe'),
    (r'ph', r'peaʃ'),
    (r'sms', r'ɛsɛm'),
    (r'chroot', r'seaʃ'),
    (r'xor', r'iks'),
    # special cases: letter names:
    (r'[lL][- ]', r'[eɛ]l'),
    (r'[mM][- ’,]', r'[eɛ]m'),
    (r'[nN][- ’,]', r'[eɛ]n'),
    (r'[sS][- ]', r'[eɛ]s'),
    (r'[rR][- ]', r'[eɛ]ʁ'),
    (r'[wW][- ]', r'dubl'),
    (r'[xX][- ]', r'iks'),
    # special cases: other abreviations:
    (r'Ier|Ire', r'pʁəmj'),
    (r'Ve', r'sɛ̃kj'),
    (r'Xe', r'dizj'),
    (r'[xX]ième', r'iksj'),
    (r'[nN]i?ème', r'[eɛ]nj'),
    (r'Xbre', r'desɑ̃bʁ'),
    # weird characters:
    (r'\W|[ʻʿʾαβγδσμ0-9]|.$|\w\.|[A-Z][A-Z0-9]|.*[^- ][A-Z]|.*à\w', r''),
}}

# due to wrong usage of agreement templates in the Wiktionary, it is often the
# case that a spelling prefix is given where a pronunciation prefix was
# expected, which leads to an eroneous pronunciation string composed of
# a spelling prefix and a pronunciation suffix; we detect these cases. below is
# the pattern we use to detect pronunciation suffixes:
pron_score__pat_pron_suffixes = \
    re.compile(r'(?:ɛ̃|ɛn|ɛl|ɛʁ|œʁ|ø|øz|o)(?: |$)')

syllable_erasure_map = str.maketrans('', '', ' ‿.')

def number_of_vowels(s):
    return len(re.findall(cpat_vowel_dirty, s))

def number_of_vowels_in_last_chunk(s):
    return number_of_vowels(s[s.rfind(' ')+1:])

# the function that, given a spelling and its pronunciation, computes the score,
# as a floating-point number:
def pron_score(spelling, pron):
    pron_without_syls = pron.translate(syllable_erasure_map)
    if not pron_without_syls:
        return 0.0
    score = 0.0
    #
    # longest chunk:
    #
    len_max_chunk, max_chunk = \
        max((len(chunk), chunk) for chunk in re.split(r'[ ‿.]+', pron))
    nb_expected_syllables = number_of_vowels(max_chunk)
    score += pron_score__coeff__longest_chunk * len_max_chunk * abs(nb_expected_syllables - 1)
    #
    # length ratio:
    #
    if re.search(pron_score__pat_no_ratio, spelling) \
      or pron.startswith(('-', '…',)) or pron.endswith(('-', '…',)) \
      or number_of_vowels(spelling) == 0 \
      or ( \
        len(spelling) <= 8 and len(pron) <= 4 \
        and len_max_chunk == len(pron) \
        and nb_expected_syllables == 1 \
      ):
        ratio = None
    else:
        #ratio = len(pron) / len(spelling)
        ratio = len(spelling) / (len(pron_without_syls) * pron_score__mean_ratio)
        abs_ratio = ratio if ratio >= 1.0 else 1.0 / ratio
        score += pron_score__coeff__ratio * (abs_ratio - 1.0)
    #
    # common prefix + added suffix:
    #
    i = 0
    while i < len(spelling) and i < len(pron) \
      and (spelling[i] == pron[i] \
        or (spelling[i] == 'r' and pron[i] == 'ʁ') \
        or (spelling[i] == 'g' and pron[i] == 'ɡ') \
      ):
        i += 1
    if i == len(pron):
        j = i
        if i < len(spelling) \
          and (spelling[i-1] == spelling[i] \
            or (spelling[i-1] == 'g' and spelling[i] == 'u') \
            or spelling[i] == 'h' \
          ):
            j += 1
        pref = pron
        suff = spelling[j:]
        if number_of_vowels(pref) >= 2 \
          or (number_of_vowels(suff) >= 1 and suff not in ['e', 'es', 'ent']):
        #if spelling[j:] not in ['’', 'e', 's', 'es', 'ent']:
            score += pron_score__bonus__pron_is_prefix
            if not set(pron).issubset(set('aeœiouwgɡkbpdtvfszmnlʁ')): # TODO: j?
                score += pron_score__bonus__pron_is_prefix
    elif pron_score__pat_pron_suffixes.match(pron, pos=i) \
      and number_of_vowels_in_last_chunk(spelling[:i]) >= 1:
        score += pron_score__bonus__pron_has_suffix
    #
    # suspicious beginning:
    #
    if number_of_vowels(spelling) >= 1 \
     and not pron.startswith(('-', '…',)):
        for (pat_spelling, pat_pron) in nonsuspicious_pron_beginnings:
            if re.match(pat_spelling, spelling) and re.match(pat_pron, pron_without_syls):
                break
        else:
            score += pron_score__bonus__beginning
    #
    # suspicious patterns:
    #
    for (pat, sc) in suspicious_pron_patterns:
        occs = re.findall(pat, pron)
        score += sc * len(occs)
        # each occurrence that is also found in the spelling scores double:
        i = 0
        for occ in occs:
            j = spelling.find(occ, i)
            if j >= 0:
                score += sc
                i = j + len(occ)
    return score

##
## Pronunciation cleanup and rectifications
##

# We fix common mistakes and normalize a bit the pronunciations
# by doing some substitutions:
subst_pron_chars = str.maketrans({
    '\u00A0' : ' ', # non-breaking space to normal space
    '\u035C' : '‿', # liaison (wrong: U+035C / good: U+203F)
    ':' : 'ː', # long vowels (wrong: ASCII / good: U+02D0)
    'ʰ' : 'ʔ', # aspirated h
    '\u0261' : 'g', # [g] consonant
    'r' : 'ʁ', # [ʁ] consonant (very often, spelling mistakenly put in pron. field)
    'ʀ' : 'ʁ', # [ʁ] consonant
    'ε' : 'ɛ', # [ɛ] vowel (wrong: Greek epsilon / good: U+025B)
    'ɜ' : 'ɛ', # [ɛ] vowel
    'ǝ' : 'ə', # schwa vowel (wrong: U+01DD / good: U+0259)
    'µ' : 'ɥ', # [ɥ] vowel (wrong: micro sign U+00B5 / good: U+0265)
    'μ' : 'ɥ', # [ɥ] vowel (wrong: Greek letter U+03BC / good: U+0265)
    # pre-combined nasal vowels
    # (assuming the open vowel was meant rather than the closed vowel):
    'ã' : 'ɑ\u0303',
    'ẽ' : 'ɛ\u0303',
    'õ' : 'ɔ\u0303',
    # we have simpler symbols for these 3 breve vowels:
    #'i\u032F' : 'j',
    #'y\u032F' : 'ɥ',
    #'u\u032F' : 'w',
})

# We rectify syllables with some simple patterns, e.g. [at.ʁa] -> [a.tʁa]
# (where [a] is any vowel). Our input has many occurrences of such patterns
# where syllabification has been done according to spelling (whenever there
# are 2 consonants, split in between them) rather than phonology.
#
# Often this is an improvement, sometimes it is disappointing w.r.t morphology;
# typically, when a schwa has been eluded from the input pronunciation,
# e.g. when [at.ʁa] should have been [a.t(ə.)ʁa]. We must choose a side;
# it seems these rewritings improve more words than they deteriorate and,
# in my opinion, optional schwas should always be noted (with parentheses).
syll_rectifications = {
  (re.compile(pat), rep) for (pat, rep) in {
    # in the comments below, '#' stands for a space.
    #
    ## add missing syllable separators:
    #
    # [vowel (semivowel) vowel]
    (fr'({pat_vowel})({pat_semivowel}?{pat_vowel})', r'\1.\2'),
    # [vowel (semivowel) consonant (semivowel) vowel]
    (fr'({pat_vowel}{pat_semivowel}?)({pat_consonant}{pat_semivowel}?{pat_vowel})', r'\1.\2'),
    # [vowel (semivowel) selected-consonant ʁ vowel]
    (fr'({pat_vowel}{pat_semivowel}?)({pat_consonant_before_R}[ʁx]{pat_vowel})', r'\1.\2'),
    # [vowel (semivowel) selected-consonant l vowel]
    (fr'({pat_vowel}{pat_semivowel}?)({pat_consonant_before_L}l{pat_vowel})', r'\1.\2'),
    # [vowel (semivowel) non-S-consonant consonants (semi)vowel]
    (fr'({pat_vowel}{pat_semivowel}?{pat_consonant_except_S})({pat_consonant_seq}(?:{pat_vowel}|{pat_semivowel}))', r'\1.\2'),
    # [ vowel (semivowel) (non-S-consonant) consonants ‿ (semi)vowel ]
    (fr'({pat_vowel}{pat_semivowel}?{pat_consonant_except_S}?)({pat_consonant_seq}‿(?:{pat_vowel}|{pat_semivowel}))', r'\1.\2'),
    #
    ## move or remove wrong syllable separators:
    #
    # [vowel . semivowel consonant]
    (fr'({pat_vowel})\.({pat_semivowel})({pat_consonant})', r'\1\2.\3'),
    # [vowel . semivowel # ]
    # [vowel . semivowel] at end of word
    (fr'({pat_vowel})\.({pat_semivowel})(?= |$)', r'\1\2'),
    # [semivowel . vowel]
    (fr'({pat_semivowel})\.({pat_vowel})', r'.\1\2'),
    # [semivowel # vowel]
    #(fr'({pat_semivowel}) ({pat_vowel})', r'.\1‿\2'),
    # [consonant . (semi)vowel]
    (fr'(?<!‿)({pat_consonant})\.({pat_vowel}|{pat_semivowel})', r'.\1\2'),
    # [consonant # (semi)vowel]
    #(fr'(?<!‿)({pat_consonant}) ({pat_vowel}|{pat_semivowel})', r'.\1‿\2'),
    # [selected-consonant . ʁ] not followed by a semivowel
    (fr'({pat_consonant_before_R})\.([ʁx])(?!{pat_semivowel})', r'.\1\2'),
    # [selected-consonant . l] not followed by a semivowel
    (fr'({pat_consonant_before_L})\.(l)(?!{pat_semivowel})', r'.\1\2'),
    # [vowel (semivowel) . consonants . consonant]
    (fr'({pat_vowel}{pat_semivowel}?)\.({pat_consonant}+)\.({pat_consonant})', r'\1\2.\3'),
    # [vowel (semivowel) . consonants # ]
    # [vowel (semivowel) . consonants] at end of word
    (fr'({pat_vowel}{pat_semivowel}?)\.({pat_consonant}+)(?= |$)', r'\1\2'),
    #
    ## fix notation of liaison:
    #
    # [ ‿ consonants . (semi)vowel ]
    # [ ‿ consonants # (semi)vowel ]
    (fr'‿({pat_consonant}+)[ .]({pat_vowel}|{pat_semivowel})', r'.\1‿\2'),
    # [ vowel (semivowel) ‿ consonants (semi)vowel ]
    (fr'({pat_vowel}{pat_semivowel}?)‿({pat_consonant}+)({pat_vowel}|{pat_semivowel})', r'\1.\2‿\3'),
    #
    # cleanup after the substitutions above:
    (fr'(^|[ ‿.])\.', r'\1'),
}}

# we log which characters have been encountered in pronunciations, with counts:
encountered_pron_chars = dict()

# we log all encountered spelling-pronunciation pairs with their scores, so as
# to list errors to be fixed in the Wiktionary:
score_file = open('scores.txt', 'w')

# Returns a tidied version of the pronunciation,
# or None if the pronunciation was empty after cleaning:
def clean_pron(word, p):
    # in order to serialize the pronunciation into a TSV spreadsheet,
    # we must check that it does not contain \t nor \n:
    if '\n' in p:
        eprint(f'ANOMALIE: "{word}": pron. contenant \\n: [{p}]')
        p = p.replace('\n', '')
    if '\t' in p:
        eprint(f'ANOMALIE: "{word}": pron. contenant \\t: [{p}]')
        p = p.replace('\t', ' ')
    # if the pronunciation is a dumped wiki template, we keep it as-is,
    # otherwise we post-process it:
    if p.startswith('{{'):
        return p
    # log unusual pronunciation characters:
    i = len(aspi_h) if p.startswith(aspi_h) else \
        len(mute_h) if p.startswith(mute_h) else 0
    chars_p = set(p[i:])
    if not chars_p.issubset(good_pron_chars):
        weird_chars = '[' + '], ['.join(sorted(chars_p - good_pron_chars)) + ']'
        eprint(f'ATTENTION: "{word}": pron. contenant un caractère atypique: {weird_chars} dans [{p}]')
    chars_tilde = set(re.findall(fr'.{tilde}', p))
    if not chars_tilde.issubset(nasal_vowels):
        weird_chars = '[' + '], ['.join(sorted(chars_tilde - nasal_vowels)) + ']'
        eprint(f'ATTENTION: "{word}": pron. contenant un tilde atypique: {weird_chars} dans [{p}]')
    for c in chars_p:
        if c in encountered_pron_chars:
            encountered_pron_chars[c] += 1
        else:
            encountered_pron_chars[c] = 1
    # compute score, and mark suspicious pronunciation:
    if p in ['', '.', '‿', mute_h, aspi_h]:
        score = 0.0
    else:
        score = pron_score(word, p)
        print(f'{score:.3f}\t"{word}"\t[{p}]', file=score_file)
        if score >= pron_score__threshold__garbage:
            eprint(f'ANOMALIE: "{word}": pron. sans doute erronée: [{p}]')
    # cleanup the pronunciation:
    p = p.translate(subst_pron_chars)
    if '. ' in p or ' .' in p or p.startswith('.') or p.endswith('.'):
        eprint(f'ANOMALIE: "{word}": pron. contenant [.] en bout de mot: [{p}]')
        # we keep dots at the beginning and end of a word,
        # because they often manifest serious errors:
        #while p.startswith('.'):  p = p[1:]
        #while p.endswith('.'):  p = p[:-1]
        p = re.sub(r'\.* \.*', ' ', p)
    if any(pat in p for pat in ['..', '‿‿', '‿ ', ' ‿', '‿.', '.‿']):
        eprint(f'ANOMALIE: "{word}": pron. mal syllabée: [{p}]')
        p = re.sub(r'\.\.+', '.', p)
        p = re.sub(r'([. ]*‿[. ]*)+', '‿', p)
    if '  ' in p or p.startswith(' ') or p.endswith(' '):
        eprint(f'ANOMALIE: "{word}": pron. contenant trop d’espaces: [{p}]')
        p = re.sub(r'  +', ' ', p.strip())
    # rectify syllables with simple patterns:
    p2 = p
    rectified = False
    again = True
    while again:
        again = False
        for (pat, rep) in syll_rectifications:
            p2, n = re.subn(pat, rep, p2)
            again = again or n > 0
        rectified = rectified or again
    if rectified:
        eprint(f'INFO: "{word}": pron. rectifiée: [{p}] -> [{p2}]')
        p = p2
    # filter out empty pronunciations after cleaning:
    if p in ['', '.', '‿', mute_h, aspi_h]:
        return None
    else:
        return mark_suspicion(p, score=score)

##
## Pronunciation merging
##

# a small record that represents the result of merging two pronunciations, along
# with some boolean flags carrying info about what the merge did:
@dataclass
class PronMergeResult:
    # the merged pronunciation:
    pron : str
    # whether both prons had syllable info and they were different info:
    conflicting_syllables : bool = False
    # whether both prons had “rich” vowels and they were different info:
    conflicting_rich_vowels : bool = False
    # whether we assimilated vowels in a non-consensual way:
    nonconsensual_merge : bool = False

class MergeFlags(enum.IntFlag):
    NO_FLAG = 0
    FST_HAS_MORE_SYLS   = 1 << 0
    SND_HAS_MORE_SYLS   = 1 << 1
    FST_HAS_RICHER_VWLS = 1 << 2
    SND_HAS_RICHER_VWLS = 1 << 3
    #CONFLICTING_SYLS = 1 << 4
    CONFLICTING_SYLS = FST_HAS_MORE_SYLS | SND_HAS_MORE_SYLS
    #CONFLICTING_RICH_VWLS = 1 << 5
    NONCONSENSUAL = 1 << 6
    __FST_FLAGS = FST_HAS_MORE_SYLS | FST_HAS_RICHER_VWLS
    __SND_FLAGS = SND_HAS_MORE_SYLS | SND_HAS_RICHER_VWLS
    def swap_sides(self):
        return ((self & MergeFlags.__FST_FLAGS) << 1) \
            | ((self & MergeFlags.__SND_FLAGS) >> 1) \
            | (self & ~(MergeFlags.__FST_FLAGS | MergeFlags.__SND_FLAGS))

merge_pairs = [
    # order matters
    #
    # (preferred, other, relative flags, absolute flags)
    #
    # merge wrong API chars with their right alternative:
    ('ʁ', 'r', MergeFlags.NO_FLAG),
    ('k', 'c', MergeFlags.NO_FLAG),
    ('e', 'é', MergeFlags.NO_FLAG),
    ('ɛ', 'è', MergeFlags.NO_FLAG),
    # non-consensual vowel merge:
    ('ɛ', 'e', MergeFlags.NONCONSENSUAL),
    ('o', 'ɔ', MergeFlags.NONCONSENSUAL),
    # “rich” vowel merge: prefer [ɑ] over [a], and keep [ː] (long vowels):
    ('ɑ', 'a', MergeFlags.FST_HAS_RICHER_VWLS),
    ('ː', '', MergeFlags.FST_HAS_RICHER_VWLS),
    # keep aspirated h:
    ('ʔ', '', MergeFlags.NO_FLAG),
    # merge syllable info:
    (' ', '.', MergeFlags.CONFLICTING_SYLS),
    ('‿', ' ', MergeFlags.FST_HAS_MORE_SYLS),
    ('‿', '.', MergeFlags.CONFLICTING_SYLS),
    (' ', '', MergeFlags.CONFLICTING_SYLS),
    ('‿', '', MergeFlags.CONFLICTING_SYLS),
    ('.', '', MergeFlags.FST_HAS_MORE_SYLS),
]

# we use these simplification rules to determine whether 2 prons can be merged:
stripped_pron_chars = str.maketrans({
    # stripping suspicion info:
    '!' : '',
    # stripping syllable info:
    ' ' : '',
    '.' : '',
    '‿' : '',
    'ʔ' : '',
    # assimilating wrong API characters with their right equivalent:
    'ʁ' : 'r',
    'c' : 'k',
    'é' : 'e',
    'è' : 'e',
    # simplifying similar vowels:
    'ː' : '',
    'ɑ' : 'a',
    # non-consensual assimilation of vowels:
    'ɛ' : 'e',
    'ɔ' : 'o',
})

# Tries to merge two pronunciations.
# Returns a PronMergeResult if it succeeded, or None if they weren’t mergeable.
# This function takes the current word for logging purposes.
def merge_two_prons(word, p1, p2):
    # to be mergeable, pronunciations must be equal when stripped:
    p1_simple = p1.translate(stripped_pron_chars)
    p2_simple = p2.translate(stripped_pron_chars)
    if p1_simple != p2_simple:
        return None
    # the merge is as suspicious as the less suspicious of the 2 pronunciations:
    suspicion1 = level_of_suspicion(p1)
    suspicion2 = level_of_suspicion(p2)
    p1 = p1[suspicion1:]
    p2 = p2[suspicion2:]
    # if one pronunciation starts with an aspirated h
    # and the other one starts with a mute h,
    # then they are not mergeable:
    pref1 = aspi_h if p1.startswith(aspi_h) else mute_h if p1.startswith(mute_h) else ''
    pref2 = aspi_h if p2.startswith(aspi_h) else mute_h if p2.startswith(mute_h) else ''
    if pref1 and pref2 and pref1 != pref2:
        return None
    # otherwise, they are mergeable.
    p = pref1 or pref2 or ''
    i1 = len(pref1)
    i2 = len(pref2)
    # these booleans are useful for reporting what happened:
    p1_has_rich_vowels = any(c in p1[i1:].replace('ɑ̃', '') for c in 'ːɑ')
    p2_has_rich_vowels = any(c in p2[i2:].replace('ɑ̃', '') for c in 'ːɑ')
    flags = MergeFlags(0)
    # merge both pronunciations by consuming their characters synchronously:
    while i1 < len(p1) or i2 < len(p2):
        c1 = p1[i1] if i1 < len(p1) else ''
        c2 = p2[i2] if i2 < len(p2) else ''
        if c1 == c2:
            p += c1
            i1 += 1
            i2 += 1
        else:
            for (c1, c2, pair_flags) in merge_pairs:
                if p1.startswith(c1, i1) and p2.startswith(c2, i2):
                    p += c1
                    i1 += len(c1)
                    i2 += len(c2)
                    flags |= pair_flags
                    break
                if p1.startswith(c2, i1) and p2.startswith(c1, i2):
                    p += c1
                    i1 += len(c2)
                    i2 += len(c1)
                    flags |= pair_flags.swap_sides()
                    break
            else:
                # should not happen
                eprint(f'ERREUR: "{word}": fusion avortée: [{p1}] <> [{p2}]')
                return None
    return PronMergeResult(
        pron = mark_suspicion(p, lvl=min(suspicion1, suspicion2)),
        conflicting_syllables = MergeFlags.CONFLICTING_SYLS in flags,
        #conflicting_syllables = \
        #    MergeFlags.CONFLICTING_SYLS in flags \
        #    or (MergeFlags.FST_HAS_MORE_SYLS in flags \
        #        and MergeFlags.SND_HAS_MORE_SYLS in flags),
        conflicting_rich_vowels = \
            p1_has_rich_vowels and p2_has_rich_vowels \
            and (MergeFlags.FST_HAS_RICHER_VWLS in flags \
                 or MergeFlags.SND_HAS_RICHER_VWLS in flags),
        nonconsensual_merge = MergeFlags.NONCONSENSUAL in flags,
    )

# Merges as many pronunciations as possible in the given set of pronunciations.
# Returns a new set. Takes the current word for logging purposes.
def merge_all_prons(word, prons):
    # for reproducibility, we use lists internally, and we sort the input set;
    # assuming the merge operation is well-behaved, this should only affect
    # logging, not the output set.
    merged_prons = [ ]
    additional_prons = [ ]
    for p2 in sorted(prons):
        # don’t try to merge dumped wiki templates:
        if p2.startswith('{{'):
            additional_prons.append(p2)
            continue
        # we don’t stop if we successfully merge p2 into another pronunciation,
        # rather we try to merge p2 into ALL other pronunciations; this way, we
        # potentially increase the amount of info in each pronunciation.
        merged = False
        for i in range(len(merged_prons)):
            p1 = merged_prons[i]
            # if the pronunciation is found as-is, do not report a merge:
            if p1 == p2:
                merged = True
                continue
            m = merge_two_prons(word, p1, p2)
            if m != None:
                indic = ' (audacieusement)' if m.nonconsensual_merge else ''
                cmp = '<' if m.pron == p1 else '>' if m.pron == p2 else '<>'
                if m.conflicting_syllables:
                    eprint(f'ATTENTION: "{word}": prons. fusionnées{indic} mais conflit de syllabes: [{p1}] {cmp} [{p2}]')
                if m.conflicting_rich_vowels:
                    eprint(f'ATTENTION: "{word}": prons. fusionnées{indic} mais conflit de voyelles: [{p1}] {cmp} [{p2}]')
                if not m.conflicting_syllables and not m.conflicting_rich_vowels:
                    eprint(f'INFO: "{word}": prons. fusionnées{indic}: [{p1}] {cmp} [{p2}]')
                merged = True
                merged_prons[i] = m.pron
        if not merged:
            merged_prons.append(p2)
    return set(merged_prons + additional_prons)

# This is a sorting key for pronunciations which makes sure that dumped
# templates come after extracted pronunciations:
def pron_sorting_key(pron):
    return ((2 if pron.startswith('{') else 1), pron)

################################################################################
### Known grammatical kinds, and other sections ################################
################################################################################

# Grammatical kinds we are interested in.
# https://fr.wiktionary.org/wiki/Wiktionnaire:Liste_des_sections_de_types_de_mots
# Abbreviations are an invention of this program.
grammar_kinds = {
    'nom' : 'NOM',
    'nom commun' : 'NOMcom',
    'substantif' : 'SUB',
    'adjectif' : 'ADJ',
    'adj' : 'ADJ',
    'employé adjectivement' : 'ADJemp',
    'verbe' : 'VER',
    'adverbe' : 'ADV',
    'adv' : 'ADV',
    'locution' : 'LOC',
    'locution nominale' : 'LOCnom',
    'locution adverbiale' : 'LOCadv',
    'pronom' : 'PRO',
    'pronom indéfini' : 'PROind',
    'pronom personnel' : 'PROper',
    'pronom possessif' : 'PROpos',
    'article indéfini' : 'ARTind',
    'article défini' : 'ARTdef',
    'article partitif' : 'ARTpar',
    'adjectif indéfini' : 'ADJind',
    'adjectif possessif' : 'ADJpos',
    'adjectif relatif' : 'ADJrel',
    'adjectif démonstratif' : 'ADJdem',
    'pronom relatif' : 'PROrel',
    'pronom démonstratif' : 'PROdem',
    'pronom interrogatif' : 'PROint',
    'adjectif interrogatif' : 'ADJint',
    'adjectif exclamatif' : 'ADJexc',
    'adverbe interrogatif' : 'ADVint',
    'adverbe relatif' : 'ADVrel',
    'préposition' : 'PRE',
    'postposition' : 'POST',
    'conjonction' : 'CONJ',
    'conjonction de coordination' : 'CONJcoord',
    'adjectif numéral' : 'ADJnum',
    'particule' : 'PRT',
    'interjection' : 'ITJ',
    'interj' : 'ITJ',
    'onomatopée' : 'ONO',
    'onom' : 'ONO',
}

# Grammatical kinds we ignore.
grammar_kinds_ignore = {
    'variante typographique', 'variante par contrainte typographique', 'erreur',
    'lettre', 'symbole', 'symb', 'numéral',
    'racine', 'préfixe', 'préf', 'suffixe', 'infixe', 'interfixe',
    'nom propre', 'nom-pr', 'prénom', 'nom de famille', 'patronyme',
    'phrase', 'locution-phrase', 'locution phrase', 'proverbe',
    'nom scientifique',
}

# Other level-3 sections to ignore.
#
# NOTE: many of these should in fact be level-4 sections,
#       we should report these mistakes. Source:
#       https://fr.wiktionary.org/wiki/Wiktionnaire:Structure_des_pages
subsecs_ignore = \
    grammar_kinds_ignore | \
    {
        'étymologie', 'notes', 'attestations', 'taux de reconnaissance',
        'abréviations', 'diminutifs', 'composés', 'phrases', 'dérivés', 'apparentés',
        'variantes', 'variantes orthographiques',
        'synonymes', 'quasi-synonymes', 'antonymes', 'paronymes', 'vocabulaire',
        'vocabulaire apparenté par le sens',
        'traductions', 'traductions à trier',
        'homophones', 'anagrammes',
        'références', 'source', 'sources', 'bibliographie', 'notes et références',
        'voir aussi', 'voir', 'liens externes',
    }

################################################################################
### Known agreement templates ##################################################
################################################################################

# https://fr.wiktionary.org/wiki/Wiktionnaire:Liste_de_tous_les_modèles/Français#Accords
# https://fr.wiktionary.org/wiki/Catégorie:Modèles_d’accord_en_français
#
# Agreement templates which we now fully support:
supported_agreement_templates = {
    # (indentation shows the hierarchy between these wiki templates)
    'fr-inv',
    'fr-accord-ind',
    'fr-rég',
        'fr-rég-x',
    'fr-accord-mf',
        'fr-accord-comp-mf',
        'fr-accord-mf-al', 'fr-rég-al',
        'fr-accord-mf-ail',
    'fr-accord-mixte',
        'fr-accord-comp',
        'fr-accord-mixte-rég',
            'fr-accord-rég', 'fr-accord',
            'fr-accord-cons',
            'fr-accord-s',
            'fr-accord-f',
            'fr-accord-eur',
            'fr-accord-eux',
            'fr-accord-oux',
            'fr-accord-un',
            'fr-accord-ot',
    # Lua templates:
    'fr-accord-ain',
    'fr-accord-al',
    'fr-accord-an',
    'fr-accord-at',
    'fr-accord-eau',
    'fr-accord-el',
    'fr-accord-en',
    'fr-accord-er',
    'fr-accord-et',
    'fr-accord-in',
    'fr-accord-oin',
    'fr-accord-ol',
    'fr-accord-on',
}
# Agreement templates whose pronunciation is (in principle…) invariable:
# NOTE:
#     This was used by a previous version of the program, as a 1st approximation
#     to easily extract pronunciations. All of these templates are now fully
#     supported, thus commented in.
invar_agreement_templates = set({
    #'fr-inv',
    #'fr-rég',
    #'fr-accord', 'fr-accord-rég',
    #'fr-rég-x',
})
# Agreement templates whose pronunciation varies (thus, with these, we need
# grammatical info to deduce the correct pronunciation for the considered word):
# NOTE:
#     Those that are now fully supported are commented in;
#     now, the only unsupported ones are rare and a bit special.
#     Our program dumps the unsupported templates to the output as-is.
var_agreement_templates = {
  # generic templates:
    #'fr-accord-mixte',
    #'fr-accord-mixte-rég', # not listed in the help page above (shouldn’t be used directly)
    #'fr-accord-mf',
    #'fr-accord-ind',
  # semi-generic templates:
    #'fr-accord-cons',
    #'fr-accord-comp',
    #'fr-accord-comp-mf',
  # templates specialized for one ending:
    #'fr-accord-s',
    #'fr-accord-f',
    #'fr-accord-er',
    #'fr-accord-eur',
    #'fr-accord-eux',
    #'fr-accord-oux',
    #'fr-accord-eau',
    #'fr-accord-mf-ail', # not listed in the help page above
    #'fr-accord-mf-al', 'fr-rég-al',
    #'fr-accord-al',
    #'fr-accord-el',
    #'fr-accord-ol', # not listed in the help page above
    #'fr-accord-at', # not listed in the help page above (obsolete)
    #'fr-accord-et',
    #'fr-accord-ot',
    #'fr-accord-an',
    #'fr-accord-ain',
    #'fr-accord-en',
    #'fr-accord-in',
    #'fr-accord-oin',
    #'fr-accord-on',
    #'fr-accord-un',
  # special templates:
    'fr-accord-personne', # not listed in the help page above
    'fr-accord-t-avant1835', # not listed in the help page above
}
# All agreement templates:
agreement_templates = \
    supported_agreement_templates \
    | invar_agreement_templates | var_agreement_templates

# These parameters to “invariable” agreement templates make their pronunciation,
# in fact, variable…
# NOTE: This is now useless, as we fully support the involved templates.
unsupported_agreement_tparams = {
    'pronradp', 'pron2radp', 'pron3radp', # {{fr-accord-rég}}
    'ps', 'pp', 'préfps', 'préfpp', # {{fr-rég-x}}
}

################################################################################
### Processing agreement templates #############################################
################################################################################

# Replaces the templates for aspirated / mute h by their API notation:
def replace_h(s):
    s = re.sub(r'{{\s*h([ _]aspiré)?\s*(\|.*?)?}}', aspi_h, s)
    s = re.sub(r'{{\s*h[ _]muet\s*(\|.*?)?}}', mute_h, s)
    # some prononciation prefixes include {{rare}}, so we strip that:
    s = re.sub(r'{{\s*rare\s*(\|.*?)?}}', '', s)
    return s

def parse_agreement_template(word, tname, t):
    #eprint(f'DEBUG: "{word}": traite {{{{{tname}}}}}')
    d = dict_of_template(t)
    # every agreement template supports a title parameter, which contains
    # relevant info (such as which spelling variant, pre- or post-1990, this
    # table corresponds to); we extract this title, and ideally we should do
    # something with it:
    title = d.pop('titre') if 'titre' in d else None
    return (title, *parse_agreement_template_dict(word, tname, d))

# Parses the given agreement template. This returns a list of word forms,
# where each word form is a tuple (gn, orthos, prons) where:
#   - gn is a 2-character string describing the gender+number,
#   - orthos is a list of spellings,
#   - and prons is a list of pronunciations.
# It is assumed that, within a form, every spelling matches every pronunciation.
# The first listed form should be the lemma.
def parse_agreement_template_dict(word, tname, d):
    infl_type = None
    infl_type_suffix = ''
    # all forms will go there:
    forms = [ ]
    # non-root templates (i.e. templates which are implemented with another one)
    # use None as a fake parameter, which must be ignored:
    if None in d:
        del d[None]
    # we track which template parameters are actually used, in order to show
    # warnings about misuses of the template:
    used = set()
    # get the value of the first listed parameter that exists:
    def get_shadowed(param_names, default=''):
        for p in param_names:
            if p in d:
                used.add(p)
                return d[p]
        return default
    # get the named parameter as a boolean value; wikicode templates regard as
    # True anything that is non-empty, however here we show a warning when the
    # value is weird (this can catch actual errors: if, for example, someone
    # tried to set the boolean to False by writing "0"):
    def get_bool(pname):
        if pname in d:
            used.add(pname)
            pvalue = d[pname]
            if pvalue not in ['', '1', 'o', 'oui']:
                eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}} avec {pname}="{d[pname]}"')
            return pvalue != ''
        else:
            return False

    # generic implementation of the templates which, in the Wiktionary,
    # are implemented in Lua rather than Wikicode:
    # ( https://fr.wiktionary.org/wiki/Module:fr-flexion )
    is_lua = False
    def lua_agreement(suf_ms, suf_mp, suf_fs, suf_fp, \
                      psuf_ms, psuf_mp, psuf_fs, psuf_fp, \
                      compat_syntax=False):
        is_lua = True
        # detect the radix and invariable suffix of the spelling, from the word:
        word_ = get_shadowed(['mot'], default=word)
        m = re.search(suf_ms + r'\b', word_) \
            or re.search(suf_mp + r'\b', word_) \
            or re.search(suf_fs + r'\b', word_) \
            or re.search(suf_fp + r'\b', word_)
        if not m:
            eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}} (Lua): graphie du radical non détectée')
            return
        rad = word_[:m.start()]
        inv = word_[m.end():]
        if inv != '' and not inv.startswith((' ', '-',)):
            eprint(f'ERREUR: "{word}", {{{{{tname}}}}} (Lua): le suffixe invariable détecté ne commence pas par une espace ni un tiret')
        # generate all spellings:
        ortho_ms = rad + suf_ms + inv
        ortho_mp = rad + suf_mp + inv
        ortho_fs = rad + suf_fs + inv
        ortho_fp = rad + suf_fp + inv
        # 'ms', 'inv', 'trait' are deprecated parameters; there are still tons
        # of them, so we do not report them; however we check that what is now
        # detected automatically is consistent with these old parameters:
        if 'ms' in d:
            old_ms = get_shadowed(['ms'])
            if old_ms != rad and old_ms != rad + suf_ms:
                eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}} (Lua): ancien param. en conflit avec la valeur détectée: ms={old_ms} / détecté={rad}+{suf_ms}')
        if 'inv' in d:
            old_trait = get_bool('trait')
            old_inv = get_shadowed(['inv'])
            if ('-' if old_trait else ' ') + old_inv != inv:
                eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}} (Lua): ancien param. en conflit avec la valeur détectée: trait={old_trait}, inv={old_inv} / détecté={inv}')
        # generate all pronunciations:
        prons_ms = [ ]
        prons_mp = [ ]
        prons_fs = [ ]
        prons_fp = [ ]
        old_syntax = False
        # some Lua templates support two incompatible syntaxes, and they
        # discriminate between both by using a hack that we reproduce here:
        if compat_syntax:
            one = get_shadowed(['1'])
            two = get_shadowed(['2'])
            # in the "old" syntax, param 1 was the spelling radix;
            # in the "new" syntax, it is the pronunciation radix;
            # so if 1 is given and is different from the spelling radix,
            # then it must be a pronunciation, following the new syntax:
            if one and one == rad:
                # if 1 is equal to the spelling radix, then, counting on the
                # fact that pronunciations would countain syllable separators,
                # 1 is likely a spelling, following the old syntax;
                # this fails for some mono-syllabic words whose spelling
                # coincide with the API, though.
                if rad in [
                  'b', 'bl', 'd', 'f', 'k', 'kl', 'l', 'm', 'n',
                  'p', 'pl', 'ps', 's', 't', 'ts', 'v', 'w', 'z',
                  'g', 'gl',
                  'br', 'dr', 'fr', 'gr', 'kr', 'pr', 'sr', 'tr', 'vr',
                ]:
                    old_syntax = (two == one)
                    # the current Lua code only tests the radix 'b', but other
                    # radixes are possible in theory (see list above); so here
                    # we signal if another radix is found such that it would
                    # change the interpretation w.r.t. what Lua does:
                    if rad != 'b' and not old_syntax:
                        eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}} (Lua): interprété à tort avec l’ancienne syntaxe par le code Lua')
                else:
                    old_syntax = True
        if old_syntax:
            # old syntax should be migrated; we can signal it, but it adds a lot
            # of noise to our program and, anyway, it would be easy to detect
            # from the Wiktionary itself (by using categories):
            #eprint(f'ATTENTION: "{word}": {{{{{tname}}}}} (Lua): ancienne syntaxe à migrer')
            pron_rad = get_shadowed(['2'])
            pron_inv = get_shadowed(['pinv'], default='')
        else:
            pron_rad = get_shadowed(['1', 'pron'])
            pron_inv = get_shadowed(['2', 'pinv'], default='')
        if pron_inv != '' and not pron_inv.startswith(('.', ' ', '\u00A0', '‿',)):
            pron_inv = ' ' + pron_inv
        pref_pron = replace_h(get_shadowed(['préfpron'], default=''))
        if pron_rad:
            prons_ms.append(pref_pron + pron_rad + psuf_ms + pron_inv)
            prons_mp.append(pref_pron + pron_rad + psuf_mp + pron_inv)
            prons_fs.append(pref_pron + pron_rad + psuf_fs + pron_inv)
            prons_fp.append(pref_pron + pron_rad + psuf_fp + pron_inv)
            pron_rad2 = get_shadowed(['pron2'])
            pref_pron2 = replace_h(get_shadowed(['préfpron2'], default=''))
            if pron_rad2:
                prons_ms.append(pref_pron2 + pron_rad2 + psuf_ms + pron_inv)
                prons_mp.append(pref_pron2 + pron_rad2 + psuf_mp + pron_inv)
                prons_fs.append(pref_pron2 + pron_rad2 + psuf_fs + pron_inv)
                prons_fp.append(pref_pron2 + pron_rad2 + psuf_fp + pron_inv)
                pron_rad3 = get_shadowed(['pron3'])
                pref_pron3 = replace_h(get_shadowed(['préfpron3'], default=''))
                if pron_rad3:
                    prons_ms.append(pref_pron3 + pron_rad3 + psuf_ms + pron_inv)
                    prons_mp.append(pref_pron3 + pron_rad3 + psuf_mp + pron_inv)
                    prons_fs.append(pref_pron3 + pron_rad3 + psuf_fs + pron_inv)
                    prons_fp.append(pref_pron3 + pron_rad3 + psuf_fp + pron_inv)
        forms.append( ('ms', [ortho_ms], prons_ms) )
        forms.append( ('mp', [ortho_mp], prons_mp) )
        forms.append( ('fs', [ortho_fs], prons_fs) )
        forms.append( ('fp', [ortho_fp], prons_fp) )
        # type of inflection:
        if inv:
            infl_type_suff = '+inv'

    # big switch ahead! implementation of every supported agreement template:
    # (Here Be Dragons)
    match tname:

      #
      # root templates
      #

      case 'fr-accord-mixte':  # both genders, both numbers
        if '1' in d and 'pron' not in d:
            d['pron'] = d.pop('1')
        # spelling:
        def process_orthos(gender, number, default):
            gn = gender + number
            gn2 = gn + '2'
            ortho = get_shadowed([gn, gender], default=default)
            return [ ortho, d.pop(gn2) ] if gn2 in d else [ ortho ]
        orthos_ms = process_orthos('m', 's', default=word)
        orthos_mp = process_orthos('m', 'p', default=orthos_ms[0]+'s')
        orthos_fs = process_orthos('f', 's', default=orthos_ms[0]+'e')
        orthos_fp = process_orthos('f', 'p', default=orthos_fs[0]+'s')
        # pronunciation:
        def process_prons(gender, number):
            prons = [ ]
            for i in ['', '2', '3']:
                pgn = 'p' + gender + number + i
                pg = 'p' + gender + i
                p = 'pron' + i
                pref_pgn = 'préf' + pgn
                pref_pg = 'préf' + pg
                pref_p = 'préf' + p
                ref_pgn = 'réf' + pgn
                if ref_pgn in d:
                    del d[ref_pgn]
                pref = get_shadowed([pref_pgn, pref_pg, pref_p], default='')
                pron = get_shadowed([pgn, pg, p], default=None)
                if pron:
                    prons.append(replace_h(pref) + pron)
            return prons
        forms.append( ('ms', orthos_ms, process_prons('m', 's')) )
        forms.append( ('mp', orthos_mp, process_prons('m', 'p')) )
        forms.append( ('fs', orthos_fs, process_prons('f', 's')) )
        forms.append( ('fp', orthos_fp, process_prons('f', 'p')) )
        # type of inflection:
        if len(orthos_ms) == 1 and len(orthos_mp) == 1 \
          and len(orthos_fs) == 1 and len(orthos_fp) == 1 \
          and orthos_mp[0] == orthos_ms[0] + 's' \
          and orthos_fs[0] == orthos_ms[0] + 'e' \
          and orthos_fp[0] == orthos_fs[0] + 's':
            infl_type = '.e.s?' # FIXME: could be wrong because of pronunciations
        else:
            infl_type = 'mf_sp'

      case 'fr-rég':  # no/one/identical gender, both numbers
        if '1' in d and 'pron' not in d:
            d['pron'] = d.pop('1')
        # gender:
        gender = '+' if get_bool('mf') else '-'
        # spelling:
        ortho_suffix = get_shadowed(['inv'], default='')
        if ortho_suffix != '':
            ortho_suffix = ' ' + ortho_suffix
        ortho_s1 = get_shadowed(['s'], default=word)
        ortho_p1 = get_shadowed(['p', 'p1'], default=ortho_s1+'s')
        ortho_s2 = get_shadowed(['s2'], default=None)
        ortho_p2 = get_shadowed(['p2'], default=(ortho_s2+'s' if ortho_s2 else None))
        if ortho_s2 == None and ortho_p2 != None:
            ortho_s2 = ortho_s1
        ortho_s3 = get_shadowed(['s3'], default=None)
        ortho_p3 = get_shadowed(['p3'], default=(ortho_s3+'s' if ortho_s3 else None))
        if ortho_s3 == None and ortho_p3 != None:
            ortho_s3 = ortho_s1
        for ref in ['réfs', 'réfp', 'réfs2', 'réfp2', 'réfs3', 'réfp3']:
            if ref in d:
                del d[ref]
        # pronunciation:
        def process_pron(prons, pref_names, pron_names):
            for ref in ['réf'+x for x in pron_names]:
                if ref in d:
                    del d[ref]
            pref = get_shadowed(pref_names, default='')
            pron = get_shadowed(pron_names, default=None)
            if pron:
                prons.append(replace_h(pref) + pron)
        prons_s1 = [ ]
        process_pron(prons_s1, ['préfps', 'préfpron'], ['ps', 'pron'])
        process_pron(prons_s1, ['préfps2', 'préfpron2'], ['ps2', 'pron2'])
        process_pron(prons_s1, ['préfps3', 'préfpron3'], ['ps3', 'pron3'])
        forms.append( (gender + 's', [ortho_s1 + ortho_suffix], prons_s1) )
        prons_p1 = [ ]
        process_pron(prons_p1, ['préfpp', 'préfpron'], ['pp', 'pron'])
        process_pron(prons_p1, ['préfpp2', 'préfpron2'], ['pp2', 'pron2'])
        process_pron(prons_p1, ['préfpp3', 'préfpron3'], ['pp3', 'pron3'])
        forms.append( (gender + 'p', [ortho_p1 + ortho_suffix], prons_p1) )
        # the default values below may seem weird; this is the actual behavior
        # of {{fr-rég}}, which is very weird indeed.
        if ortho_s2:
            prons_s2 = [ ]
            process_pron(prons_s2, ['préfp2s', 'préfpron'], ['p2s', 'ps', 'pron'])
            process_pron(prons_s2, ['préfp2s2', 'préfpron'], ['p2s2', 'ps2', 'pron2'])
            process_pron(prons_s2, ['préfp2s3', 'préfpron'], ['p2s3', 'ps3', 'pron3'])
            forms.append( (gender + 's', [ortho_s2 + ortho_suffix], prons_s2) )
            prons_p2 = [ ]
            process_pron(prons_p2, ['préfp2p', 'préfpron'], ['p2p', 'pp', 'pron'])
            process_pron(prons_p2, ['préfp2p2', 'préfpron2'], ['p2p2', 'pron2'])  # no 'pp2'
            process_pron(prons_p2, ['préfp2p3', 'préfpron3'], ['p2p3', 'pron3'])  # no 'pp3'
            forms.append( (gender + 'p', [ortho_p2 + ortho_suffix], prons_p2) )
        if ortho_s3:
            prons_s3 = [ ]
            process_pron(prons_s3, ['préfp3s', 'préfpron'], ['p3s', 'ps', 'pron'])
            forms.append( (gender + 's', [ortho_s3 + ortho_suffix], prons_s3) )
            prons_p3 = [ ]
            process_pron(prons_p3, ['préfp3p', 'préfpron'], ['p3p', 'pp', 'pron'])
            forms.append( (gender + 'p', [ortho_p3 + ortho_suffix], prons_p3) )
        # type of inflection:
        if ortho_s2 or ortho_s3 or ortho_p1 != ortho_s1 + 's':
            infl_type = 'sp_rég'
        elif ortho_suffix:
            infl_type = '.s+inv'
        else:
            infl_type = '.s'

      case 'fr-accord-mf':  # no/one/identical gender, both numbers
        # gender:
        gender = '+' if get_bool('mf') else '-'
        # spelling and pronunciation:
        for (number, default_ortho) in [('s', word), ('p', None)]:
            for (alt, default_ortho) in [('', default_ortho), ('2', None)]:
                ortho = get_shadowed([number + alt], default=default_ortho)
                if ortho == None:
                    continue
                prons = [ ]
                for i in ['', '2', '3']:
                    pnalt = 'p' + alt + number + i
                    pn = 'p' + number + i
                    p = 'pron' + i
                    pref_pnalt = 'préf' + pnalt
                    pref_pn = 'préf' + pn
                    pref_p = 'préf' + p
                    ref_pnalt = 'réf' + pnalt
                    if ref_pnalt in d:
                        del d[ref_pnalt]
                    pref = get_shadowed([pref_pnalt, pref_pn, pref_p], default='')
                    pron = get_shadowed([pnalt, pn, p], default=None)
                    if pron:
                        prons.append(replace_h(pref) + pron)
                forms.append( (gender + number, [ortho], prons) )
        # type of inflection:
        infl_type = 'sp' # TODO: improve specificity

      case 'fr-accord-ind': # both genders, no/one/identical number
        # spelling:
        ortho_m = get_shadowed(['m'], default=word)
        ortho_f = get_shadowed(['f'], default=ortho_m+'e')
        # pronunciation:
        prons = [ ]
        def process_prons(gender):
            pg = 'p' + gender
            p = 'pron'
            pref_pg = 'préf' + pg
            pref_p = 'préf' + p
            pref = get_shadowed([pref_pg, pref_p], default='')
            pron = get_shadowed([pg, p], default=None)
            return [ replace_h(pref) + pron ] if pron else [ ]
        forms.append( ('m-', [ortho_m], process_prons('m')) )
        forms.append( ('f-', [ortho_f], process_prons('f')) )
        # type of inflection:
        if ortho_f == ortho_m + 'e':
            infl_type = '.e?' # FIXME: could be wrong because of pronunciations
        else:
            infl_type = 'mf'

      case 'fr-inv': # no/one/identical gender, no/one/identical number
        if '1' in d and 'pron' not in d:
            d['pron'] = d.pop('1')
        # gender and number:
        gn = '--'
        sp = get_bool('sp')
        mf = get_bool('mf')
        # the table heading can be customized with free-form text,
        # which often contains gender/number info:
        if 'inv_titre' in d:
            inv_titre = d.pop('inv_titre')
            if sp:
                eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}}: sp= masqué par inv_titre="{inv_titre}"')
            match inv_titre.lower():
                case 'masculin singulier' | '{{m}} {{s}}' | 'masculin singulier invariable':
                    gn = 'mS'
                case 'masculin pluriel' | '{{m}} {{p}}':
                    gn = 'mP'
                case 'féminin singulier' | '{{f}} {{s}}' | 'féminin singulier invariable':
                    gn = 'fS'
                case 'féminin pluriel' | '{{f}} {{p}}':
                    gn = 'fP'
                case '{{m}}':
                    gn = 'm-'
                case 'féminin':
                    gn = 'f-'
                case 'singulier' | 'singulier uniquement' \
                  | 'au singulier' | 'au singulier uniquement' \
                  | '{{au singulier uniquement|fr}}' \
                  | 'singulare tantum' | 'singular tantum' \
                  | 'singulare tantum, indénombrable' \
                  | 'singulier<br>{{indénombrable|fr}}' :
                    gn = '-S'
                case 'pluriel' | 'au pluriel' | 'au pluriel uniquement' | 'plurale tantum' \
                  | 'sigle (au pluriel)':
                    gn = '-P'
                case '{{pluriel ?|nocat=1}}' | '{{pluriel ?|fr}}':
                    gn = '-S'
                case 'singulier et pluriel' | 'singulier et pluriel identiques' \
                  | 'singulier ou pluriel' | '{{sp}}':
                    gn = '-+'
                case 'invariable' | '{{invar}}' \
                  | 'interjection' | 'locution interjective' | 'onomatopée' \
                  | 'préposition' | 'locution prépositive' | 'conjonction' \
                  | 'adverbe' | 'adverbe de temps' | 'locution adverbiale' \
                  | 'locution verbale' :
                    gn = '--'
                case 'abréviation' | 'acronyme' | 'sigle' \
                  | 'pronom' | 'pronom interrogatif' | 'pronom indéfini' \
                  | 'nom propre' | 'nom propre, acronyme' \
                  | 'nom commun' | 'nom invariable' \
                  | 'adjectif' | 'adjectif invariable' | 'adjectif numéral' \
                  | 'indénombrable' | '{{indénombrable|fr}}' :
                    gn = '--'
                case _:
                    eprint(f'INFO: "{word}": {{{{{tname}}}}}: inv_titre non reconnu: {inv_titre}')
                    gn = '--'
        if sp:  gn = gn[0] + '+'
        if mf:  gn = '+' + gn[1]
        # spelling:
        ortho = get_shadowed(['s'], default=word)
        # pronunciation:
        prons = [ ]
        for i in ['', '2', '3']:
            p = 'pron' + i
            pref_p = 'préf' + p
            pref = get_shadowed([pref_p], default='')
            pron = get_shadowed([p], default=None)
            if pron:
                prons.append(replace_h(pref) + pron)
        forms.append( (gn, [ortho], prons) )
        # type of inflection:
        infl_type = 'inv'

      #
      # derived templates
      #

      case 'fr-accord-mixte-rég':
        inv = get_shadowed(['inv'])
        if inv != '':
            inv = ('-' if get_shadowed(['trait']) else ' ') + inv
        radp = get_shadowed(['radp','1'])
        suff = get_shadowed(['suff','sufm'])
        plur = get_shadowed(['plur'], default='s')
        pron1 = get_shadowed(['2'])
        pron2 = get_shadowed(['pron2'])
        pron3 = get_shadowed(['pron3'])
        pron1radp = get_shadowed(['pronradp','2'])
        pron2radp = get_shadowed(['pron2radp','pron2'])
        pron3radp = get_shadowed(['pron3radp','pron3'])
        psufm = get_shadowed(['psufm'])
        psuff = get_shadowed(['psuff','psufm'])
        psufmp = get_shadowed(['psufmp','psufm'])
        psuffp = get_shadowed(['psuffp','psuff','psufm'])
        pinv = get_shadowed(['pinv'])
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte', {
            'ms': get_shadowed(['1']) + get_shadowed(['sufm']) + inv,
            'mp': radp + get_shadowed(['sufmp','sufm']) + plur + inv,
            'fs': get_shadowed(['1']) + suff + inv,
            'fp': radp + suff + get_shadowed(['plurf'], default=plur) + inv,
            'pms' : (pron1 + psufm + pinv if pron1 else ''),
            'pms2': (pron2 + psufm + pinv if pron2 else ''),
            'pms3': (pron3 + psufm + pinv if pron3 else ''),
            'réfpms' : get_shadowed(['réfpms']),
            'réfpms2': get_shadowed(['réfpms2']),
            'réfpms3': get_shadowed(['réfpms3']),
            'pmp' : (pron1radp + psufmp + pinv if pron1radp else ''),
            'pmp2': (pron2radp + psufmp + pinv if pron2radp else ''),
            'pmp3': (pron3radp + psufmp + pinv if pron3radp else ''),
            'pfs' : (pron1 + psuff + pinv if pron1 else ''),
            'pfs2': (pron2 + psuff + pinv if pron2 else ''),
            'pfs3': (pron3 + psuff + pinv if pron3 else ''),
            'pfp' : (pron1radp + psuffp + pinv if pron1radp else ''),
            'pfp2': (pron2radp + psuffp + pinv if pron2radp else ''),
            'pfp3': (pron3radp + psuffp + pinv if pron3radp else ''),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
        })
        #infl_type = infl_type # TODO: improve specificity
        if inv:
            infl_type_suffix = '+inv'

      case 'fr-accord-rég':
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['ms'], default=word),
            '2': get_shadowed(['1','pron']),
            'suff': 'e',
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
        })
        infl_type = '.e.s'

      case 'fr-accord':  # alias
        return parse_agreement_template_dict(word, 'fr-accord-rég', d)

      case 'fr-accord-cons':
        cons_spell = get_shadowed(['3'])
        cons_pron = get_shadowed(['2'])
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['ms'], default=word),
            '2': get_shadowed(['1']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'suff': cons_spell + 'e',
            'psufm': get_shadowed(['psufm']),
            'psuff': cons_pron,
        })
        infl_type = f'cons-{cons_spell}-{cons_pron}'

      case 'fr-accord-s':
        cons_spell = get_shadowed(['2'])
        cons_pron = get_shadowed(['3'], default='z')
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['ms'], default=word),
            '2': get_shadowed(['pron','1']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'suff': cons_spell + 'e',
            'psuff': cons_pron,
            'plur': '',
            'plurf': 's',
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
        })
        infl_type = f's.{cons_spell}e[{cons_pron}].s'

      case 'fr-accord-f':
        infm = get_shadowed(['3','infm'])
        pinfm = get_shadowed(['4','pinfm'])
        inff = get_shadowed(['5','inff','3','infm'])
        pinff = get_shadowed(['6','pinff','4','pinfm'])
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['ms','1']),
            '2': get_shadowed(['2']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'sufm': infm + 'f',
            'psufm': pinfm + 'f',
            'suff': inff + 've',
            'psuff': pinff + 'v',
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
        })
        if infm == inff:
            infl_type = 'v.e.s'
        elif infm == 'e' and inff == 'è':
            infl_type = 'èv.e.s'
        else:
            infl_type = f'f-{infm}f[{pinfm}f]-{inff}ve[{pinff}v].s'

      case 'fr-accord-eur':
        ice = get_shadowed(['ice']) != ''
        rice = get_shadowed(['rice']) != ''
        eresse = get_shadowed(['eresse']) != ''
        suff = 'ice' if ice else 'rice' if rice else 'eresse' if eresse else 'euse'
        psuff = 'is' if ice else 'ʁis' if rice else '(ə).ʁɛs' if eresse else 'øz'
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['1']),
            '2': get_shadowed(['2']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'réfpms' : get_shadowed(['réfpron']),
            'réfpms2': get_shadowed(['réfpron2']),
            'réfpms3': get_shadowed(['réfpron3']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'sufm': 'eur',
            'suff': suff,
            'psufm': 'œʁ',
            'psuff': psuff,
        })
        if suff == 'ice':
            infl_type = 'eur,ice'
        elif suff == 'rice':
            infl_type = 'eur,rice'
        elif suff == 'eresse':
            infl_type = 'eur,eresse'
        elif suff == 'euse':
            infl_type = 'eur,euse'
        else:
            assert False

      case 'fr-accord-eux':
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['1']),
            '2': get_shadowed(['2']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'sufm': 'eux',
            'suff': 'euse',
            'plur': '',
            'plurf': 's',
            'psufm': 'ø',
            'psuff': 'øz',
        })
        infl_type = 'x,se.s'

      case 'fr-accord-oux':
        suff = get_shadowed(['suff'], default='c')
        psuff = get_shadowed(['psuff'], default='s')
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['1']),
            '2': get_shadowed(['2']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'sufm': 'oux',
            'suff': 'ou' + suff + 'e',
            'plur': '',
            'plurf': 's',
            'psufm': 'u',
            'psuff': 'u' + psuff,
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
        })
        infl_type = f'x,{suff}e[{psuff}].s'

      case 'fr-accord-un':
        deux_n = get_shadowed(['deux_n'])
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['ms'], default=word),
            '2': get_shadowed(['pron','1']),
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
            'suff': ('ne' if deux_n else 'e'),
            'psufm': 'œ̃',
            'psuff': 'yn',
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
        })
        infl_type = 'un.ne.s' if deux_n else 'un.e.s'

      case 'fr-accord-ot':
        one = get_shadowed(['1'])
        two = get_shadowed(['2'])
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte-rég', {
            '1': get_shadowed(['ms'], default=word),
            '2': one,
            'pron2': get_shadowed(['pron2']),
            'pron3': get_shadowed(['pron3']),
            'inv': get_shadowed(['inv']),
            ('trait' if 'trait' in d else None): get_shadowed(['trait']),
            'pinv': get_shadowed(['pinv']),
            ('radp' if 'radp' in d else None): get_shadowed(['radp']),
            ('pronradp' if 'pronradp' in d else None): get_shadowed(['pronradp']),
            ('pron2radp' if 'pron2radp' in d else None): get_shadowed(['pron2radp']),
            ('pron3radp' if 'pron3radp' in d else None): get_shadowed(['pron3radp']),
            'suff': two + 'e',
            'psufm': ('o' if one else ''),
            'psuff': ('ɔt' if one else ''),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
        })
        infl_type = f'ot.{two}e.s'

      case 'fr-accord-comp':
        one = get_shadowed(['1'])
        two = get_shadowed(['2'])
        three = get_shadowed(['3'])
        four = get_shadowed(['4'])
        f1 = get_shadowed(['f1'], default=one+'e')
        f2 = get_shadowed(['f2'], default=two+'e')
        trait = get_shadowed(['trait'], default='-')
        ptrait = get_shadowed(['ptrait'], default='.')
        ptraitp = get_shadowed(['ptraitp'], default=ptrait)
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mixte', {
            'ms': one + trait + two,
            'mp': get_shadowed(['mp1'], default=one+'s') + trait \
                    + get_shadowed(['mp2'], default=two+'s'),
            'fs': get_shadowed(['fs1'], default=f1) + trait \
                    + get_shadowed(['fs2'], default=f2),
            'fp': get_shadowed(['fp1'], default=f1+'s') + trait \
                    + get_shadowed(['fp2'], default=f2+'s'),
            'pms': (three + ptrait if three else '') + four,
            'pmp': get_shadowed(['pmp1'], default=(three + ptraitp if three else '')) \
                    + get_shadowed(['pmp2','4']),
            'pfs': get_shadowed(['pfs1','pf1','3']) \
                    + get_shadowed(['ptraitfs'], default=ptrait) \
                    + get_shadowed(['pfs2','pf2','4']),
            'pfp': get_shadowed(['pfp1','pf1','3']) \
                    + get_shadowed(['ptraitfp'], default=ptraitp) \
                    + get_shadowed(['pfp2','pf2','4']),
            ('préfpron' if 'préfpron' in d else None): get_shadowed(['préfpron']),
            ('préfpm' if 'préfpm' in d else None): get_shadowed(['préfpm']),
            ('préfpf' if 'préfpf' in d else None): get_shadowed(['préfpf']),
            'préfpms': get_shadowed(['préfpms']),
            'préfpmp': get_shadowed(['préfpmp']),
            'préfpfs': get_shadowed(['préfpfs']),
            'préfpfp': get_shadowed(['préfpfp']),
        })
        infl_type = 'comp2_mf_sp' # TODO: improve specificity

      case 'fr-accord-comp-mf':
        one = get_shadowed(['1'])
        two = get_shadowed(['2'])
        three = get_shadowed(['3'])
        four = get_shadowed(['4'])
        pp1 = get_shadowed(['pp1','3'])
        pp2 = get_shadowed(['pp2','4'])
        pp1b = get_shadowed(['pp1b','pp1','ps1b','3'])
        pp2b = get_shadowed(['pp2b','pp2','ps2b','4'])
        trait = get_shadowed(['trait'], default='-')
        ptrait = get_shadowed(['ptrait'], default='.')
        ptraitp = get_shadowed(['ptraitp'], default=ptrait)
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mf', {
            'mf': get_shadowed(['mf']),
            's': one + trait + two,
            'p': get_shadowed(['p1'], default=one+'s') + trait \
                    + get_shadowed(['p2'], default=two+'s'),
            'réfps': get_shadowed(['réfps']),
            'réfpp': get_shadowed(['réfpp']),
            'ps': (three + ptrait if three else '') + four,
            ('ps2' if 'ps1b' in d or 'ps2b' in d else None): \
                    get_shadowed(['ps1b','3']) + ptrait \
                    + get_shadowed(['ps2b','4']),
            'pp': (pp1 + ptraitp if pp1 else '') + pp2,
            ('pp2' if pp1b+pp2b != pp1+pp2 else None): \
                    pp1b + ptraitp + pp2b,
            ('préfps' if 'préfps' in d else None): get_shadowed(['préfps']),
            ('préfpp' if 'préfpp' in d else None): get_shadowed(['préfpp']),
            ('préfpron' if 'préfpron' in d else None): get_shadowed(['préfpron']),
        })
        infl_type = 'comp2_sp' # TODO: improve specificity

      case 'fr-accord-mf-al':
        one = get_shadowed(['1'])
        two = get_shadowed(['2'])
        pron2 = get_shadowed(['pron2'])
        pron3 = get_shadowed(['pron3'])
        radp = get_shadowed(['radp','1'])
        inv = get_shadowed(['inv'])
        if inv != '':
            inv = ' ' + inv
        pinv = get_shadowed(['pinv'])
        if pinv != '':
            pinv = ' ' + pinv
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mf', {
            's': one + 'al' + inv,
            'p': radp + 'aux' + inv,
            'ps': (two + 'al' + pinv if two else ''),
            'réfps': get_shadowed(['réf']),
            'pp': (get_shadowed(['pronradp','2']) + 'o' + pinv if two else ''),
            'ps2': (pron2 + 'al' + pinv if pron2 else ''),
            'pp2': (get_shadowed(['pron2radp','pron2']) + 'o' + pinv if pron2 else ''),
            'réfps2': get_shadowed(['réf2']),
            'ps3': (pron3 + 'al' + pinv if pron3 else ''),
            'pp3': (get_shadowed(['pron3radp','pron3']) + 'o' + pinv if pron3 else ''),
            'réfps3': get_shadowed(['réf3']),
            'préfpron' : get_shadowed(['préfpron']),
            'préfpron2': get_shadowed(['préfpron2']),
            'préfpron3': get_shadowed(['préfpron3']),
        })
        if one == radp:
            infl_type = 'al,aux'
        else:
            infl_type = 'sp_rég_al'
        if inv:
            infl_type_suffix = '+inv'

      case 'fr-rég-al':  # alias
        return parse_agreement_template_dict(word, 'fr-accord-mf-al', d)

      case 'fr-accord-mf-ail':
        one = get_shadowed(['1'])
        two = get_shadowed(['2'])
        inv = get_shadowed(['inv'])
        if inv != '':
            inv = ' ' + inv
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-accord-mf', {
            's': one + 'ail' + inv,
            'p': one + 'aux' + inv,
            'ps': (two + 'aj' if two else ''),
            'réfps': get_shadowed(['réf']),
            'pp': (two + 'o' if two else ''),
            'préfpron' : get_shadowed(['préfpron']),
        })
        infl_type = 'ail,aux'
        if inv:
            infl_type_suffix = '+inv'

      case 'fr-rég-x':
        s = get_shadowed(['s'], default=word)
        p = get_shadowed(['p'], default=s+'x')
        (infl_type, forms) = parse_agreement_template_dict(word, 'fr-rég', {
            'pron': get_shadowed(['pron','1']),
            'pron2': get_shadowed(['pron2']),
            's': s,
            'réfs': get_shadowed(['réfs']),
            'p': p,
            'réfp': get_shadowed(['réfp']),
            ('ps' if 'ps' in d else None): get_shadowed(['ps']),
            'réfps': get_shadowed(['réfps']),
            ('pp' if 'pp' in d else None): get_shadowed(['pp']),
            'réfpp': get_shadowed(['réfpp']),
            'préfpron': get_shadowed(['préfpron']),
            ('préfps' if 'préfps' in d else None): get_shadowed(['préfps']),
            ('préfpp' if 'préfpp' in d else None): get_shadowed(['préfpp']),
            'inv': get_shadowed(['inv']),
            'mf': get_shadowed(['mf']),
        })
        if p == s + 'x':
            infl_type = '.x'
        else:
            infl_type = 'sp_rég_x'

      #
      # Lua templates
      #

      case 'fr-accord-ain':
        n = 'n' if get_bool('nn') else ''
        lua_agreement('ain', 'ains', f'ain{n}e', f'ain{n}es', 'ɛ̃', 'ɛ̃', 'ɛn', 'ɛn')
        infl_type = f'ain.{n}e.s'

      case 'fr-accord-al':
        lua_agreement('al', 'aux', 'ale', 'ales', 'al', 'o', 'al', 'al', compat_syntax=True)
        infl_type = 'al,aux,ale.s'

      case 'fr-accord-an':
        n = 'n' if get_bool('nn') else ''
        lua_agreement('an', 'ans', f'an{n}e', f'an{n}es', 'ɑ̃', 'ɑ̃', 'an', 'an')
        infl_type = f'an.{n}e.s'

      case 'fr-accord-at':
        eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}}: modèle obsolète, utiliser {{{{fr-accord-cons}}}}')
        lua_agreement('at', 'ats', 'atte', 'attes', 'a', 'a', 'at', 'at')
        infl_type = 'at.te.s' # TODO: 't.te.s'

      case 'fr-accord-eau':
        lua_agreement('eau', 'eaux', 'elle', 'elles', 'o', 'o', 'ɛl', 'ɛl', compat_syntax=True)
        infl_type = 'eau.x,elle.s'

      case 'fr-accord-el':
        lua_agreement('el', 'els', 'elle', 'elles', 'ɛl', 'ɛl', 'ɛl', 'ɛl')
        infl_type = 'el.le.s' # TODO: 'l.le.s'

      case 'fr-accord-en':
        lua_agreement('en', 'ens', 'enne', 'ennes', 'ɛ̃', 'ɛ̃', 'ɛn', 'ɛn')
        infl_type = 'en.ne.s'

      case 'fr-accord-er':
        lua_agreement('er', 'ers', 'ère', 'ères', 'e', 'e', 'ɛʁ', 'ɛʁ', compat_syntax=True)
        infl_type = 'èr.e.s'

      case 'fr-accord-et':
        ett = 'èt' if get_bool('è') else 'ett'
        lua_agreement('et', 'ets', f'{ett}e', f'{ett}es', 'ɛ', 'ɛ', 'ɛt', 'ɛt')
        infl_type  = 'èt.e.s' if ett == 'èt' else 'et.te.s' # TODO: 't.te.s'

      case 'fr-accord-in':
        n = 'n' if get_bool('deux_n') else ''
        lua_agreement('in', 'ins', f'in{n}e', f'in{n}es', 'ɛ̃', 'ɛ̃', 'in', 'in')
        infl_type = f'in.{n}e.s'

      case 'fr-accord-oin':
        n = 'n' if get_bool('nn') else ''
        lua_agreement('oin', 'oins', f'oin{n}e', f'oin{n}es', 'wɛ̃', 'wɛ̃', 'wan', 'wan')
        infl_type = f'oin.{n}e.s'

      case 'fr-accord-ol':
        lua_agreement('ol', 'ols', 'olle', 'olles', 'ɔl', 'ɔl', 'ɔl', 'ɔl')
        infl_type = f'ol.le.s'

      case 'fr-accord-on':
        n = 'n' if not get_bool('un_n') else ''
        lua_agreement('on', 'ons', f'on{n}e', f'on{n}es', 'ɔ̃', 'ɔ̃', 'ɔn', 'ɔn')
        infl_type = f'on.{n}e.s'

      case _:
        assert False

    # end of parse_agreement_template_dict:

    assert infl_type != None or eprint(f'DEBUG: "{word}": {{{{{tname}}}}}: type None')
    infl_type += infl_type_suffix

    # detect ambiguous cases of several spellings for several pronunciations:
    if tname == 'fr-accord-mixte':
        # this template allows several spellings and several pronunciations,
        # but it is ambiguous as to which spelling maps to which pronunciation(s).
        # here we report instances where there are at the same time several
        # spellings and several pronunciations.
        for i, (gn, orthos, prons) in enumerate(forms):
            no = len(orthos)
            np = len(prons)
            definitely_separate = False
            if no == 2 and np == 2: # only situation that arises in practice
                o0 = orthos[0]
                o1 = orthos[1]
                p0 = prons[0]
                p1 = prons[1]
                # recognize common cases (e.g. "finals/finaux, aïeuls/aïeux")
                # where 1st (resp. 2nd) spelling maps to 1st (resp. 2nd) pron.:
                for (pat_o0, pat_o1, pat_p0, pat_p1) in [ \
                  (r'ale?s$', r'aux$', r'al$', r'o$'),
                  (r'euls$', r'eux$', r'œl$', r'ø$'),
                  (r'euses?$', r'rices?$', r'øz$', r'ʁis$'),
                  (r'euses?$', r'eues?$', r'øz$', r'ø$'),
                  (r'ignes?$', r'ines?$', r'iɲ$', r'in$'),
                  (r'\bcomme une? ', r'\bcomme des ', r'\bkɔ\.?m[‿ ](œ̃|yn)', r'\bkɔm d[eɛ]'),
                  (r'\bcomme un ', r'\bcomme une ', r'\bkɔ\.?m[‿ ]œ̃', r'\bkɔ\.?m[‿ ]yn'),
                ]:
                    if (    re.search(pat_o0, o0) and re.search(pat_o1, o1) \
                        and re.search(pat_p0, p0) and re.search(pat_p1, p1) ) \
                    or (    re.search(pat_o1, o0) and re.search(pat_o0, o1) \
                        and re.search(pat_p1, p0) and re.search(pat_p0, p1) ):
                        definitely_separate = True
                        break
                # ALWAYS map 1st (resp. 2nd) spelling to 1st (resp. 2nd) pron.;
                # I’ve checked manually that this is the case in all instances,
                # currently, even those which do not match the patterns above:
                forms[i:i+1] = [ (gn, [o0], [p0]), (gn, [o1], [p1]) ]
            if no >= 2 and np >= 2:
                indic = ' (motif reconnu)' if definitely_separate else ''
                orthos = '"' + '", "'.join(orthos) + '"'
                prons = '[' + '], ['.join(prons) + ']'
                eprint(f'INFO: "{word}": {{{{{tname}}}}}: {np} prons. pour {no} orthos au {gn}{indic}: {orthos}, {prons}')
    elif tname in ['fr-accord-mf', 'fr-rég']:
        # these templates allow several spellings and several pronunciations,
        # where each pronunciation applies to a specific spelling; but they are
        # often misused. here we report suspicious instances where there is both
        # a 2nd spelling (s2 or p2), and a 2nd pronunciation applied to the 1st
        # spelling (pron2, ps2 or pp2) instead of the 2nd one (p2s or p2p).
        for n in ['s', 'p']:
            if f'{n}2' in d and f'p2{n}' not in d and ('pron2' in d or f'p{n}2' in d):
                (orthos, prons) = \
                    next(
                        (orthos, prons) for (gn, orthos, prons) in forms
                            if gn == gender + n
                    )
                orthos = '"' + '", "'.join(orthos) + '"'
                prons = '[' + '], ['.join(prons) + ']'
                eprint(f'ATTENTION: "{word}": {{{{{tname}}}}}: usage suspect de pron2 ou p{n}2: {orthos}, {prons}')

    # show a warning about possible misuse of the template:
    for k in used:
        del d[k]
    if d:
        lua = ' (Lua)' if is_lua else ''
        eprint(f'ATTENTION: "{word}": {{{{{tname}}}}}{lua}: param. ignoré: {", ".join(d.keys())}')

    #eprint(f'DEBUG: "{word}": {{{{{tname}}}}}: type = {infl_type}, flexions = {forms}')
    return (infl_type, forms)

################################################################################
### Processing a word’s article ################################################
################################################################################

# Returns data about the word, extracted from the wikicode:
def extract_data_from_wikicode(word, wikicode):
    # lemma(s) (possibly several, due to orthographic variants):
    lemmas = set()
    # type(s) of inflection:
    infl_types = [ ]
    # gender and number:
    gender = '-'
    number = '-'
    form_genders = set() # data extracted from agreement templates
    form_numbers = set() # data extracted from agreement templates
    # for verbs, to which conjugations (inflections) of the infinitive (lemma)
    # does this spelling correspond:
    conjs = set()
    # set of pronunciations:
    prons = set()
    def add_pron(p):
        # NOTE: we will merge pronunciations only once we are done gathering and
        # deduplicating them all; otherwise, we would get duplicated logging
        # messages about successful merges.
        p = clean_pron(word, p)
        if p:
            prons.add(p)
    # these variables record whether we just processed a template for mute or
    # aspirated h (because these templates should immediately precede {{pron}}):
    h_pref = ''
    h = ''
    # process all templates in the wikicode:
    for t in wikicode.ifilter_templates(recursive=False):
        h_pref = h
        h = ''
        tname = template_name(t)
        # big switch!
        match tname:

          #
          # gender/number on the "ligne de forme":
          #

          case 'm' | 'masculin':
            gender = '+' if gender in ['f', '+', '?'] else 'm'
          case 'f' | 'féminin':
            gender = '+' if gender in ['m', '+', '?'] else 'f'
          case 'mf':
            gender = '+'
          case 'mf ?' | 'mf?' | 'fm ?' | 'fm?':
            gender = '?'
          case 's' | 'singulier':
            number = '+' if number == 'p' else 's'
          case 'p' | 'pluriel':
            number = '+' if number == 's' else 'p'
          case 'au singulier uniquement' | 'singulare tantum':
            number = 'S'
          case 'au pluriel uniquement' | 'plurale tantum':
            number = 'P'
          case 'sp':
            number = '+'
          case 'sp ?':
            number = '+'
          #case 'msing' | 'fsing' | 'mplur' | 'fplur': # deprecated and unused
          #  gender = tname[0]
          #  number = tname[1]
          #    # ^ or 'S'/'P'? https://fr.wiktionary.org/wiki/Wiktionnaire:Gestion_des_modèles/2018#Modèles_de_genre_et_nombre
          case 'invar' | 'invariable':
            # TODO?
            pass

          #
          # verb data:
          #

          case 'fr-verbe-flexion':
            # extract the infinitive as a lemma:
            inf = template_param(t, 1)
            if inf:
                lemmas.add(inf)
            else:
                eprint(f'ANOMALIE: "{word}": {{{{{tname}}}}}: infinitif manquant')
            # extract the list of conjugations the current word corresponds to:
            for (param_name, param_value) in dict_of_template(t).items():
                if param_value != '' \
                and ('.' in param_name \
                    or param_name in ['ppr','pp','ppm','ppms','ppmp','ppf','ppfs','ppfp']):
                    conjs.add(param_name)

          #
          # pronunciations:
          #

          # very special words which are not said at all:
          case 'sans pron':
            # TODO?
            pass

          # mute or aspirated h:
          case 'h' | 'h aspiré' | 'h muet':
            h = mute_h if tname == 'h muet' else aspi_h # {{h}} is an alias of {{h aspiré}}

          # the main {{pron}} template:
          case 'pron' | 'prononciation' | '//':
            if template_param(t, 2, default='') == 'fr' \
            or template_param(t, 'lang', default='') == 'fr':
                if t.has(1):
                    add_pron(h_pref + template_param(t, 1))
                if t.has('pron'):
                    add_pron(h_pref + template_param(t, 'pron'))

          # fully supported agreement template:
          case _ if tname in supported_agreement_templates:
            (title, infl_type, forms) = parse_agreement_template(word, tname, t)
            # extract the lemma (assuming it is given by the first listed form):
            if len(forms) == 0:
                eprint(f'ERREUR: "{word}": aucune forme produite par {{{{{tname}}}}}')
            else:
                (_, lem_orthos, _) = forms[0]
                for o in lem_orthos:
                    lemmas.add(o)
            # extract the type of inflection
            infl_types.append(infl_type)
            # find which form(s) the current inflection corresponds to;
            # add the associated pronunciations, and record its gender/number.
            for (form_gn, form_orthos, form_prons) in forms:
                if word in form_orthos:
                    form_genders.add(form_gn[0])
                    form_numbers.add(form_gn[1])
                    for p in form_prons:
                        add_pron(p)
            # TODO: use title

          # agreement template whose pronunciation is invariable:
          case _ if tname in invar_agreement_templates:
            # if the template has an unsupported parameter,
            # which affects the pronunciation,
            # then we dump the template as is:
            if any(t.has(p) for p in unsupported_agreement_tparams):
                eprint(f'ATTENTION: "{word}": param. non supporté dans {flatten_template(t)}')
                add_pron(flatten_template(t))
                continue
            if t.has(1):
                pref = replace_h(template_param(t, 'préfpron', default=''))
                add_pron(pref + template_param(t, 1))
            if t.has('pron'):
                pref = replace_h(template_param(t, 'préfpron', default=''))
                add_pron(pref + template_param(t, 'pron'))
            if t.has('pron2'):
                pref = replace_h(template_param(t, 'préfpron2', default=''))
                add_pron(pref + template_param(t, 'pron2'))
            if t.has('pron3'):
                pref = replace_h(template_param(t, 'préfpron3', default=''))
                add_pron(pref + template_param(t, 'pron3'))

          # agreement template whose pronunciation varies:
          case _ if tname in var_agreement_templates:
            # TODO: support these templates;
            # for now we just dump them on the output
            add_pron(flatten_template(t))

          # unknown agreement template:
          case _ if tname.startswith(('fr-accord', 'fr-rég-',)):
            eprint(f'ERREUR: "{word}": modèle d’accord inconnu: {flatten_template(t)}')

          # audio template:
          case 'écouter':
            # these are unreliable, sometimes other parts of speech are appended
            # or prepended to the word…
            if template_param(t, 3, default='') == 'fr' \
            or template_param(t, 'lang', default='') == 'fr':
                if t.has(2):
                    add_pron(template_param(t, 2))
                if t.has('pron'):
                    add_pron(template_param(t, 'pron'))

    # after the loop:

    if len(lemmas) > 1:
        eprint(f'INFO: "{word}": plusieurs lemmes: {", ".join(sorted(lemmas))}')

    if len(infl_types) > 1:
        eprint(f'INFO: "{word}": plusieurs types: {"; ".join(sorted(infl_types))}')

    # try to reconcile genders/numbers found in the "ligne de forme"
    # with those deduced from agreement templates:
    if len(form_genders) > 0:
        form_gender = '-'
        if len(form_genders) == 1:
            form_gender = form_genders.pop()
        elif ('m' in form_genders and 'f' in form_genders) or '+' in form_genders:
            form_gender = '+'
        if form_gender != '-':
            if gender == '-':
                gender = form_gender
            elif gender == '?' and form_gender == '+':
                gender = '?'
            elif gender != form_gender:
                eprint(f'ANOMALIE: "{word}": désaccord ligne de forme / modèle d’accord sur le genre: {gender} / {form_gender}')
    if len(form_numbers) > 0:
        form_number = '-'
        if len(form_numbers) == 1:
            form_number = form_numbers.pop()
        elif ('s' in form_numbers and 'p' in form_numbers) or '+' in form_numbers:
            form_number = '+'
        if form_number != '-':
            if number == '-':
                number = form_number
            elif number == 's' and form_number == 'S' \
              or number == 'S' and form_number == 's':
                number = 'S'
            elif number == 'p' and form_number == 'P' \
              or number == 'P' and form_number == 'p':
                number = 'P'
            elif number != form_number:
                eprint(f'ANOMALIE: "{word}": désaccord ligne de forme / modèle d’accord sur le nombre: {number} / {form_number}')

    # at last, return extracted data:
    return {
        'lemmas' : lemmas,
        'infl_types' : infl_types,
        'gn' : gender + number,
        'conjs' : conjs,
        'prons' : merge_all_prons(word, prons),
    }

################################################################################
### Main loop: processing all French words in the French Wiktionary ############
################################################################################

# Tests whether the given title is {{langue|fr}}, up to wiki syntax variations:
def matches_section_fr(title):
    ok = False
    i = 0
    if len(title.nodes) > 0 and isinstance(title.nodes[0], mw.nodes.text.Text) \
    and title.nodes[0].strip() == '':
        i += 1
    if len(title.nodes) <= i:
        return False
    else:
        tpl = title.nodes[i]
        return isinstance(tpl, mw.nodes.template.Template) \
                and template_name(tpl) == 'langue' \
                and template_param(tpl, 1, default='') == 'fr'

# Set up the required XML namespaces (this does not seem to strip the namespace
# from the tag names automatically, alas).
namespaces = {
    '': 'http://www.mediawiki.org/xml/export-0.10/' #'
}
for prefix, uri in namespaces.items():
    xmltree.register_namespace(prefix, uri)

# The user can provide a word on the command line;
# then, only this word and those that come after in the XML dump are processed.
# this allows for incremental usage, in case execution is interrupted before
# completiong.
skip_until_word = None
if len(sys.argv) > 1:
    skip_until_word = sys.argv[1]
    eprint(f'INFO: ne traitera aucun article avant "{skip_until_word}"')

# some counters for logging:
skipped_word_count = 0
word_count = 0

# write TSV headers on the output:
print('ortho\tcgram\tnum\tlem\ttypflx\tgn\tconj\tpron')

# The XML file is very big (~ 5 Gb), so we parse it incrementally, using
# `iterparse`.
#
# We must take care of avoiding memory leaks, so we remove XML elements after we
# are done with them. Just doing `elem.clear()` on the current element is not
# enough because, although the element itself is made empty, it remains there as
# a child of its parent, at least until its parent itself is cleared, which
# would not happen until the parent’s closing tag.
#
# This is an actual problem with the XML dump of the Wiktionary, because it has
# millions of pages, each of which being an XML element attached to the same
# parent (the root). Missing to remove the page elements from their parent would
# leak gigabytes of memory, which would cause either the program or the machine
# to crash before completion.
#
# For proper garbage collection, we thus need access to the parent of the
# current element. For that, we keep track of the current path (tag stack) from
# the XML root down to the current element, as explained there:
# https://stackoverflow.com/questions/2170610/access-elementtree-node-parent-node#76596818
#
# With that technique, total memory usage remains under 30 megabytes. :-)

def garbage_collect(parent, elem):
    #elem.clear()
    if parent:  parent.remove(elem)
    # Since we are removing all elements anyway, and we do not care about the
    # parents of removed elements, we might even clear the parent completely; in
    # addition to children, this also removes the parent’s attributes and text.
    # In practice, the memory gain is negligible, and it does not seem faster
    # than just removing the current child.
    #if parent:  parent.clear()

# the path to the current element, which allows finding its parent:
cur_path = [ ]

# we are interested in <page> elements, so we must not remove the elements that
# are within them:
is_inside_page_elem = False

for event, elem in xmltree.iterparse('frwikt.xml', events=['start', 'end']):

    # clear XML namespace from tag name:
    i = elem.tag.find('}')
    if i >= 0:
        elem.tag = elem.tag[i+1:]

    # maintain the current path:
    if event == 'start':
        cur_path.append(elem)
        if elem.tag == 'page':
            is_inside_page_elem = True
        continue # we treat elements once they are ended
    else:
        assert event == 'end'
        assert len(cur_path) > 0 and cur_path[-1] is elem
        cur_path.pop()
        if elem.tag == 'page':
            is_inside_page_elem = False

    # the parent of `elem` (needed for garbage collection):
    parent = cur_path[-1] if len(cur_path) > 0 else None

    # we want the <page> elements whose namespace is '0', i.e. article pages:
    if elem.tag != 'page' or elem.findtext('ns') != '0':
        if not is_inside_page_elem:
            garbage_collect(parent, elem)
        continue

    # the word is the page title:
    word = elem.findtext('title')
    assert word != None

    # some logging (feedback to reassure the user that something is going on):
    word_count += 1
    if word_count % 10_000 == 0:
        if skip_until_word != None:
            eprint(f'INFO: {word_count:_}e article sauté: "{word}"')
        else:
            eprint(f'INFO: {word_count:_}e article traité: "{word}"')

    # we ignore all words before the one given by `skip_until_word`:
    if skip_until_word != None:
        if word == skip_until_word:
            skip_until_word = None
            skipped_word_count = word_count - 1
            word_count = 1
            eprint(f'INFO: 1er mot traité: "{word}"')
        else:
            garbage_collect(parent, elem)
            continue

    # obtain the latest revision of the page:
    revs = elem.findall('revision')
    assert len(revs) == 1
    rev = revs[0]
    assert rev.findtext('model') == 'wikitext'
    assert rev.findtext('format') == 'text/x-wiki'
    page_contents = rev.findtext('text')
    assert page_contents != None

    # parse the wiki code:
    #
    # we keep style tags (''italic'' and '''bold''') as plain text, because many
    # pages have bugs related to these style tags (i.e. missing closing tag),
    # which causes parsing to fail (more exactly, it produces a wrong syntax
    # tree, so that finding sections is bugged).
    parsed = mw.parse(page_contents, skip_style_tags=True)
    # extract the French-language section:
    fr_secs = parsed.get_sections(levels=[2], matches=matches_section_fr, include_headings=False)
    # if there are none, this is not a French word, drop it:
    if len(fr_secs) == 0:
        pass
    elif len(fr_secs) > 1:
        eprint(f'ANOMALIE: "{word}": plusieurs sections langue fr')
    else:
        fr_sec = fr_secs[0]

        # iterate on all entries (level-3 sub-sections) in the French section:
        for sec in fr_sec.get_sections(levels=[3], include_headings=True):
            is_lemma = True
            sec_num = '1'
            # the section title gives the grammatical kind:
            sec_title = next(sec.ifilter_headings()).title
            sec_title_tpls = sec_title.filter_templates()
            if len(sec_title_tpls) >= 2:
                eprint(f'ANOMALIE: "{word}": section non reconnue (plusieurs modèles): {sec_title}')
                continue
            if len(sec_title_tpls) == 0:
                sec_kind = sec_title
            else:
                sec_title_tpl = sec_title_tpls[0]
                if not (template_name(sec_title_tpl) == 'S' and sec_title_tpl.has(1)):
                    eprint(f'ANOMALIE: "{word}": section non reconnue (pas le modèle S): {sec_title}')
                    continue
                if template_param(sec_title_tpl, 3, default='') == 'flexion':
                    is_lemma = False
                sec_num = template_param(sec_title_tpl, 'num', default='1')
                sec_kind = template_param(sec_title_tpl, 1)
            sec_kind = str(sec_kind).strip().lower()
            if sec_kind in subsecs_ignore:
                continue
            if sec_kind in ['prononciation', 'prononciations']:
                # for now we ignore everything in the "Prononciation" sub-section
                # (additional pronunciations, often regional variations,
                # unfortunately it is in an anarchic format and thus very hard
                # to process):
                continue
            if sec_kind not in grammar_kinds:
                eprint(f'ERREUR: "{word}": section ou type grammatical non reconnu: {sec_kind}')
                continue
            # use abbreviations for the grammatical kind,
            # and append an asterisk for non-lemmas:
            gram_kind = grammar_kinds[sec_kind]
            if not is_lemma:
                gram_kind += '*'
            # discard everything starting from the first definition (numbered
            # list item); we do this because, in principle, the data we want are
            # located before the definitions, in the inflection boxes and the
            # "ligne de forme"; later wikicode may contain undesired occurrences
            # of looked-for templates (for instance, a few examples contain
            # {{pron}}, usage notes sometimes use {{pron}} to indicate WRONG
            # pronunciations, and translations often contain {{p}}):
            for i, node in enumerate(sec.nodes):
                if isinstance(node, mw.nodes.tag.Tag) and str(node) == '#':
                    del sec.nodes[i:]
                    break
            # preprocess the wikicode, to clean it a bit:
            strip_wikicode(sec)
            # extract the pronunciations:
            data = extract_data_from_wikicode(word, sec)
            lemmas = data['lemmas']
            if is_lemma:
                lemmas.add(word)
            lemmas = "; ".join(sorted(lemmas))
            infl_types = "; ".join(sorted(data['infl_types']))
            gn = data['gn']
            conjs = "; ".join(sorted(data['conjs']))
            prons = "; ".join(sorted(data['prons'], key=pron_sorting_key))
            # write the words with their pronunciations on the output:
            print(f'{word}\t{gram_kind}\t{sec_num}\t{lemmas}\t{infl_types}\t{gn}\t{conjs}\t{prons}')

    garbage_collect(parent, elem)

# end of main loop: show some statistics

if skip_until_word != None:
    skipped_word_count = word_count
    word_count = 0

eprint(f'INFO: mots sautés: {skipped_word_count:_}')
eprint(f'INFO: mots traités: {word_count:_}')

eprint(f'INFO: caractères rencontrés dans des prononciations:')
for (c, count) in sorted(encountered_pron_chars.items()):
    eprint(f'INFO:    [{c}]: {count:_}')
