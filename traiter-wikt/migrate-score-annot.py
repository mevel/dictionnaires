#!/usr/bin/env python

# Migrate annotations written manually in an old score dump
# to a newer dump (score dumps are files such as `scores.txt`
# produced by `extract-prons-frwikt.py`).
# An annotation is associated to a (spelling, pronunciation) pair.
# It is kept when the same pair exists in the new score dump.

import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def get_line_fields(line):
    if line[-1] == '\n':
        line = line[:-1]
    return line.split('\t')

annots = dict()

with open('scores-old-with-annot.txt', 'r') as table:
    for line in table:
        (_score, ortho, pron, annot) = get_line_fields(line)
        assert ortho[0] == '"' and ortho[-1] == '"'
        assert pron[0] == '[' and pron[-1] == ']'
        if annot != '':
            key = (ortho, pron)
            annots[key] = annot

with open('scores-new.txt', 'r') as table:
    for line in table:
        (score, ortho, pron) = get_line_fields(line)
        assert ortho[0] == '"' and ortho[-1] == '"'
        assert pron[0] == '[' and pron[-1] == ']'
        key = (ortho, pron)
        annot = annots[key] if key in annots else ''
        print(f'{score}\t{ortho}\t{pron}\t{annot}')
