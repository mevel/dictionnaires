#!/usr/bin/sh

# this script sorts and deduplicates a dump of pronunciation scores,
# as outputted by `extract-prons-frwikt.py`

LC_ALL=C sort -rg | uniq
