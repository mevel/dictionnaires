#!/usr/bin/python

import csv
import sys

def pron_sorting_key(pron):
    return ((2 if pron.startswith('{') else 1), pron)

champs_sortie = ["ortho", "cgram", "num", "pron"]

if len(sys.argv) != 1:
    print(f'USAGE: {sys.argv[0]} < SOURCE.tsv > DEST.tsv')
    exit(1)

with sys.stdin as fichier_entree, \
  sys.stdout as fichier_sortie:
    tableur_entree = csv.DictReader(fichier_entree, delimiter="\t")
    tableur_sortie = csv.DictWriter(fichier_sortie, \
      delimiter="\t", lineterminator='\n',
      quoting=csv.QUOTE_NONE, doublequote=False, quotechar='\0',
      fieldnames=champs_sortie)
    tableur_sortie.writeheader()
    mot_prec = None
    for mot in tableur_entree:
        print(f'LIGNE: {mot}')
        if 'pron' not in mot or mot['pron'] == None:
            print(f'! ANOMALIE: ligne incomplète: {mot}', file=sys.stderr)
            print(f'!           ligne précédente: {mot_prec}', file=sys.stderr)
        else:
            if None in mot:
                print(f'! ANOMALIE: trop de champs dans la ligne (\\t dans le champ "pron"?)', file=sys.stderr)
                print(f'!           {mot}', file=sys.stderr)
                mot['pron'] = mot['pron'] + '\t' + '\t'.join(mot[None])
            prons = mot['pron'].split('; ')
            prons = sorted(prons, key=pron_sorting_key)
            prons = ( p for p in prons if p not in ['', '(h)', '(H)'] )
            mot['pron'] = '; '.join(prons)
        tableur_sortie.writerow(mot)
        mot_prec = mot
