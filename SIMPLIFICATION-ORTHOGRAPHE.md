# Rationaliser l’orthographe française

<!-- table des matières: -->
[[_TOC_]]





<!------------------------------------------------------------->

_Attention : l’auteur de ce document est un amateur qui, s’il
a acquis une certaine culture concernant l’écriture du français
et s’intéresse à la lingüistique, n’a aucune formation dans ces
domaines._

_Les suggestions sont bienvenues !_





<!------------------------------------------------------------->

## Introduction



Le présent travail penche davantage vers l’idéalisme que vers le
pragmatisme. Il fait suite à une question de pure curiosité :
à quoi ressemblerait une orthographe idéale du français ? Il
s’agit donc de balayer le champ des possibles. Il reste
à évaluer plus précisément l’utilité des idées évoquées et leur
applicabilité dans la société du 21e siècle (son inertie et même
sa résistance active aux changements). Certaines sont clairement
irréalistes, d’autres très raisonnables et d’ailleurs sont
régulièrement proposées depuis au moins les hussards noirs de
Jules Ferry.

Certains ont argumenté que pour avoir une quelconque chance de
succès, une réforme devrait se cantonner à un petit nombre de
modifications faciles à expliquer et à apprendre, et qui ne
perturbent pas radicalement la « physionomie » graphique des
mots. Les modifications généralement identifiées comme
prioritaires visent les consonnes doubles, les pluriels en _-x_
(mots en _-oux, -eux_) et les lettres grecques ; auxquels
s’ajoute, dépassant du champ de l’orthographe, le complexe
accord du participe passé.

En effet, ces difficultés-là touchent un très grand nombre de
mots, sont responsables de certaines des fautes les plus
courantes en pratique, et leur élimination est très simple. Mais
le présent document en atteste : outre quelques points
d’application très large, la complexité de l’écriture du
français, c’est la somme d’un très grand nombre de petites
particularités, des irrégularités vis-à-vis de principes plus
généraux. Beaucoup peuvent paraître bénignes à un alphabète
confirmé (par exemple, l’orthographe de « aujourd’hui, j’ai eu,
il est, et, œil, accueil, dessus, pied, nez, mer, terre, second,
femme, évidemment, résident, longtemps, corps, fils, fille,
ville, crayon, spatial, piqûre… ») mais elles ne le sont
pourtant pas : elles sont la raison pour laquelle les petits
Français ont besoin de deux ans pour acquérir la lecture
fondamentale quand la plupart des Européens savent lire au bout
d’un an. Elles représentent une masse d’informations
contradictoires à retenir toutes ensemble. Forcément, ça finit
par déborder, et c’est alors le drame, la faute.

Ce document est donc très long et assez fouillis (un effort de
synthèse serait nécessaire). Voici quelques pistes de lecture.

+ Si vous vous sentez d’humeur révolutionnaire, vous pouvez lire
  le détail des changements proposés (section « Idées de
  changements »).
+ Si vous vous demandez ce qu’il est possible d’améliorer sans
  changer la face de l’écrit, vous pouvez lire en priorité les
  sections concernant les consonnes doubles, les pluriels en
  _-x_, les « Prononciations irrégulières de voyelles », les
  « Mots divers » et les « Régularisations grammaticales », par
  exemple.
+ Si vous êtes curieux mais pressés, vous pouvez sauter aux
  textes d’exemples, en gardant à l’esprit qu’ils ne montrent
  qu’une sélection de changements possibles.
+ Enfin, si vous vous reconnaissez dans le lectorat du Figaro,
  vous pouvez poursuivre la lecture de cette Introduction.



### Pourquoi simplifier ?

<!-- abbé de Dangeau, 17e siècle (1643–1723): -->
[historique-dangeau]: https://fr.wikipedia.org/wiki/Louis_de_Courcillon_de_Dangeau
<!-- Du Marsais, 1730: -->
[historique-du-marsais]: https://fr.wikipedia.org/wiki/César_Chesneau_Dumarsais
<!-- Duclos, 1754: -->
[historique-duclos]: https://fr.wikipedia.org/wiki/Charles_Pinot_Duclos
<!-- Didot l’aîné, 18e siècle (1750–1804): -->
[historique-didot]: https://fr.wikipedia.org/wiki/François-Ambroise_Didot
<!-- Ambroise Firmin Didot, 1867: -->
[historique-firmin-didot]: https://fr.wikipedia.org/wiki/Ambroise_Firmin_Didot

[D’autres argumentent mieux que moi][convivialité],
[et][historique-dangeau]
[ce][historique-du-marsais]
[depuis][historique-duclos]
[trois][historique-didot]
[siècles][historique-firmin-didot],
de la nécessité de réformer l’orthographe.
Néanmoins, voici ce que *je* peux dire, sous forme d’un essai
personnel qui synthétise les arguments principaux et, plus
encore, qui répond aux objections classiques.

Pourquoi simplifier l’orthographe ?

 1. **L’orthographe, ce n’est pas la langue.** C’est avant tout
    une norme pour transcrire la langue. Quand on questionne
    l’orthographe, beaucoup s’indignent qu’on s’attaque à la
    langue elle-même. Ce n’est pas le cas. Nous aimons tous la
    langue française et nous souhaitons le meilleur pour elle.
    La question se pose : la norme est-elle adaptée à la
    fonction qu’elle est censée remplir ? Fait-elle honneur au
    français ? Sert-elle les francophones ou leur nuit-elle ?

 1. L’orthographe française, notoirement compliquée, est un
    enjeu de société. En France en 2021, selon l’[ANLCI][anlci],
    7 % de la population de 18 à 65 ans ayant été scolarisée en
    France serait en situation d’**illettrisme**. Il est vrai
    que dans le monde moderne, l’illettrisme est déterminé par
    l’accès à l’éducation bien plus que par la difficulté de
    l’écriture ; cependant simplifier l’orthographe réduit le
    temps d’apprentissage.

    Surtout, il y a écrire et écrire. Même parmi ceux qui ont un
    niveau dit fonctionnel de lecture et d’écriture du français,
    au sens où ils sont capables de se faire comprendre, on
    constate de **larges disparités** de maitrise de
    l’orthographe — et de la grammaire. Sa difficulté en fait un
    marqueur social, qui permet de distinguer ceux qui ont
    bénéficié d’une éducation favorisée (incluant par exemple le
    latin) de ceux issus de milieux plus modestes, ou
    d’immigration. L’orthographe érigée en valeur morale est
    source de **discrimination**, de **mépris**, de perte
    d’estime de soi et d’échec scolaire. (Bien sûr, il y a bien
    d’autres sources de discrimination, comme le vocabulaire, le
    registre de langue, l’accent, pour ne citer que ce qui
    a trait à la langue… Ça ne doit pas nous empêcher d’agir sur
    l’une des plus faciles à lisser.) Ah, les attaques
    _ad orthographiam_ ! Cette pression est même internalisée
    dans le phénomène d’**[insécurité
    linguistique][insécurité]**, notable [en
    français][insécurité-france], où des locuteurs se mettent
    à douter de leur maitrise de leur propre langue maternelle,
    et finissent par s’auto-censurer !

    Malgré ce qu’on se plaît à croire sur la « patrie des droits
    de l’Homme » et sa supposée méritocratie, la France est
    [l’un des pays développés dont le système éducatif est le
    plus perpétuateur des **inégalités
    sociales**][pisa-2019-inégalités]. L’orthographe française
    y participe car elle est **délibérément élitiste**.

    [anlci]: http://www.anlci.gouv.fr/
    [insécurité]: https://fr.wikipedia.org/wiki/Insécurité_linguistique
    [insécurité-france]: https://www.tract-linguistes.org/baisse-du-niveau-en-francais-fantasme-ou-realite/
    [pisa-2019-inégalités]: https://www.liberation.fr/societe/education/rapport-pisa-des-resultats-en-france-parmi-les-plus-bas-jamais-mesures-20231205_OXVPWFLD55CGZI3RB2GDKASJ5Y/

 1. « Moi, je n’ai jamais fait de fautes. »
    Grand bien vous fasse.

 1. **L’orthographe française *est* compliquée** : c’est un fait
    scientifique. [Une étude][anglais-seymour] à l’échelle
    européenne montre que quelques langues surpassent nettement
    les autres en temps d’acquisition de la lecture élémentaire
    par les enfants : tout d’abord l’anglais, suivi du trio
    danois–portugais–français.

 1. L’écriture du français est, en fait, remplie de
    **complexités superflues voire complètement artificielles**,
    qui représentent une véritable perte de temps à l’école (on
    parle de 80 H pour enseigner l’accord du participe passé) et
    un gaspillage de ressources intellectuelles (nombreuses
    règles et exceptions à mémoriser bêtement et se remémorer
    constamment, toute sa vie durant, dès qu’il s’agit
    d’écrire).

    De nombreuses règles et exceptions, loin de refléter de
    grands principes de la langue, ne sont édictées que pour
    fossiliser à postériori des situations incohérentes qu’il
    est pourtant possible de rectifier. Les informations
    étymologiques, principale complication de l’orthographe,
    sont chaotiques et même parfois erronées. Elles ne prennent
    en compte ni les progrès en étymologie accomplis par les
    lexicographes et les lingüistes ces 200 dernières années, ni
    — surtout — les besoins réels du lexique français, en termes
    d’apparentés et d’homonymies. On peut donc gagner sur tous
    les tableaux, même sans sacrifier l’étymologie, en la
    corrigeant et en la régularisant.

 1. Un biais naturel, quand on ne sait pas, est de **supposer**
    consciemment ou non que ce qui existe a une bonne raison
    d’exister, même si on n’a pas daigné nous la dire.
    L’orthographe aurait été conçue globalement et en toute
    intelligence par de supposés experts (on peut parler de
    mystère religieux). On veut **croire** qu’on n’a pas
    souffert à l’école pour rien, qu’on ne s’embête pas au
    quotidien pour rien… Biais qui va jusqu’à faire de nous les
    défenseurs réflexes de l’état de fait
    (`#jesuiscirconflexe`).

    Si tout ce qui existe a forcément (évidemment) une *cause*
    historique, une cause n’est (évidemment ?) pas une
    *justification*. Un tas de choses très mauvaises existent.
    La cause peut être complètement accidentelle, une erreur,
    une décision ancienne motivée par de mauvaises raisons, ou
    bien par des raisons qui étaient bonnes mais qui ont disparu
    depuis des siècles. Des incohérences peuvent être le produit
    de simples omissions, des recoins où personne n’a pensé
    à balayer. De fait, le présent document souligne de nombreux
    défauts de l’orthographe actuelle.

    Les anciens sçavants n’en sçavoient pas plus que nous. On
    dispose aujourd’hui de la **science** (la **lingüistique**,
    d’émergence récente ; en 2020, il n’y a d’ailleurs toujours pas de
    lingüiste à l’Académie), de vastes ressources documentaires
    avec un accès aisé (imprimerie puis Internet), et même de
    l’outil informatique (qui permet par exemple d’analyser
    rapidement des corpus, d’exploiter des bases de données
    lexicales…). On a acquis l’expérience de l’**enseignement de
    masse** (qui n’existait pas lors de la dernière réforme
    orthographique d’ampleur, en 1835). Et, bien sûr, notre
    français du 21e siècle n’est pas celui des anciens ni même
    celui du 19e. Les réformistes sont en général bien
    informés ; beaucoup sont lingüistes. Ce ne sont pas des
    sagouins !

 1. Étant admis que les complications de l’orthographe française
    ne se justifient pas, on leur imagine des vertus collatérales.
    Selon certains conservatistes, le douloureux apprentissage
    de l’orthographe (confondue avec la grammaire) durant
    l’enfance serait formateur pour l’esprit : il inculquerait
    la logique, la rigueur, le goût de l’effort, entrainerait la
    mémoire. Comme pour la boîte manuelle, on va jusqu’à se
    prendre d’affection pour des règles tordues tellement il est
    *gratifiant* de les maîtriser ; mais à quoi bon ? Qu’en
    est-il vraiment ?

      - **Concernant la logique, c’est on-ne-peut-plus faux.**
        Vu l’**irrationalité de l’orthographe**, c’est tout le
        contraire. Puisqu’on est dans les spéculations
        psycho-éducatives de comptoir, on peut même conjecturer
        que les complications inutiles entravent le
        développement de l’esprit scientifique : les nombreux
        écueils et le poids moral de la faute incitent
        à accorder une attention démesurée à des vétilles au
        détriment de l’essence d’un sujet ; la menace permanente
        d’une exception qui confirmerait la règle empêche
        l’induction de lois générales en lesquelles se fier,
        l’un des fondements même de l’intelligence. La **vraie
        logique** mathématique, elle, ne souffre aucune
        exception. En France, elle n’est enseignée que dans le
        supérieur, en cursus scientifique, donc à des esprits
        matures et qui ont déjà prouvé leurs facultés
        intellectuelles. Les élèves français sont [les plus
        mauvais de l’OCDE][timss-2019] (quelle honte !) [en
        sciences][timss-2019-sciences] et [particulièrement en
        mathématiques][timss-2019-maths] (le système éducatif
        français excelle à former une petite élite et à laisser
        la masse sur le carreau) ; des discours politiques
        émaillés de rhétorique fallacieuse et d’erreurs de
        raisonnement basiques jouissent d’une large écoute ;
        astrologie, médecines chinoises et autres superstitions
        new-age ne se sont jamais aussi bien portées ; la France
        est la patrie de l’homéopathie. C’est bien sûr la
        conjonction de nombreux facteurs, mais la question
        demeure : l’école réussit-elle à développer les facultés
        intellectuelles des Français ? Est-ce en consacrant
        autant de temps au français, plutôt qu’aux sciences,
        qu’elle y parviendra ? Peut-on réellement se gargariser
        d’une vertu (dé)formatrice de notre orthographe ?

      - Quant à la rigueur : en effet, on forme les enfants
        à **se conformer aveuglément à des règles arbitraires**
        sans chercher à connaitre leur raison d’être — qui
        souvent n’existe pas. On n’a rien à répondre à ceux qui
        demandent « pourquoi ? » Inculquer dès l’école primaire
        une discipline militaire qui forcément crée des
        conflits, des rejets et, _in fine_, de l’échec
        scolaire ; tuer dans l’œuf l’esprit critique ; est-ce ça
        qu’on veut ? La passion française des administrations
        inutilement compliquées, irréformables : corrélation ?

      - La mémoire enfin, peut avantageusement se travailler
        par l’apprentissage de **savoirs moins dénués de
        sens** : sciences naturelles, géographie, etc. Plutôt
        que « hibou, pou, chou, bijou, joujou, genou, clou,
        caillou » (trouver l’erreur), apprenons « Mercure,
        Vénus, Terre, Mars, Jupiter, Saturne, Uranus, Neptune »
        (apprenons-le plus tôt).

    Finalement le temps qu’on économiserait sur l’apprentissage
    de l’orthographe (voir aussi [Catach, p218]), on pourrait
    avantageusement le consacrer à des bribes de logique (!),
    [de science][enseignement-sciences], d’Histoire, de culture…
    qui sinon seraient vues plus tard ou jamais ;
    à l’argumentation, aux impostures rhétoriques… Les enfants
    n’en seraient que plus intelligents, plus cultivés, dotés
    d’un meilleur esprit critique, soit de futurs citoyens plus
    capables et moins victimes de discrimination. Voilà qui
    rappelle l’ambition contrariée de l’école républicaine. Ce
    n’est pas pour rien que la nécessité de simplifier
    l’orthographe occupait le devant de la scène à l’époque de
    Jules Ferry.

    [timss-2019]: https://www.francetvinfo.fr/societe/enfance-et-adolescence/niveau-en-maths-et-sciences-les-eleves-francais-classes-parmi-les-derniers-d-europe-et-de-locde_4210837.html
    [timss-2019-sciences]: https://www.cafepedagogique.net/lexpresso/Pages/2021/01/05012021Article637454263756780953.aspx.html
    [timss-2019-maths]: https://www.liberation.fr/france/2020/12/08/niveau-en-maths-et-en-sciences-la-france-s-enfonce_1807926/
    [enseignement-sciences]: https://www.polytechnique-insights.com/dossiers/societe/que-signifie-avoir-confiance-en-la-science/comment-trier-le-bon-du-mauvais-doute/

    <!--
      - TIMSS 2019
      - PISA 2023
      - rapport Villani–Torossian, « 21 mesures pour l’enseignement des mathématiques », 2018
        cf chapitre 4, sur la formation des instits (« 4.1. Un constat alarmant »)
        https://www2.animath.fr/wp-content/uploads/2018/08/Rapport_Villani_Torossian-1.pdf
    -->

    > « Demandez à vos directeurs, à vos inspecteurs : le cri
    > sera unanime, l’orthographe est le fléau de l’école […].
    > Comme tout y est illogique, contradictoire, que, à peu
    > près seule, la mémoire visuelle s’y exerce, il oblitère la
    > faculté de raisonnement ; pour tout dire, il abêtit. »  
    > (Ferdinand Brunot, lingüiste, au ministre de l’Instruction
    > publique, 1905)

    > « On a trop réduit la connaissance de la langue à la
    > simple mémoire. Faire de l’orthographe le signe de la
    > culture, signe des temps et de sottise. »  
    > (Paul Valéry, écrivain, 1941)

    > « Mémoire de l’absurde […] nous apprenons ainsi
    > l’illogisme aux enfants. »  
    > (Hervé Bazin, écrivain ; cité dans [Catach, p174])

 1. Le sentiment qui sous-tend, en fait, nombre d’opinions
    conservatrices, sûres de leur croyance en l’arbitraire, est
    le suivant : céder aux sirènes de la simplification, ce
    serait abandonner le « génie de la langue » à un **supposé
    abêtissement général**, une paresse intellectuelle
    galopante, une véritable décadence. La **condamnation
    morale** n’est jamais assez terrible !

    D’abord, que de **raccourcis fumeux** quand on mesure
    l’intelligence à la culture, la culture à la maîtrise de la
    langue, et la maîtrise de la langue à la maîtrise de son
    orthographe ! Ensuite, on vient de voir que l’orthographe
    actuelle est loin d’être parfaite et que simplifier, ce
    n’est pas niveler par le bas, mais par le haut. Ce n’est pas
    s’abîmer dans la bêtise mais faire jaillir, du chaos, la
    raison.

    Enfin, on peut souligner à quel point l’abêtissement n’est
    que fantasmé. On entend souvent la rengaine selon laquelle
    nos aînés, qui bien souvent n’ont pas dépassé l’école
    primaire, auraient mieux su écrire que les jeunes
    générations… qui seraient donc simplement bêtes ou
    paresseuses. Non seulement c’est une **idée reçue** (lire
    des lettres de Poilus…) nourrie par le **passéisme** et le
    fait que tout quidam aujourd’hui écrit et se fait lire bien
    davantage qu’il y a cent ans, sans être corrigé par des
    professionnels de l’édition (Internet…) ; mais encore cet
    argument oublie qu’au début du 20e siècle, l’instruction
    publique consacrait une très grande partie de son temps
    à trois fondamentaux : lire, écrire et compter. Confrontée
    à la difficulté de l’orthographe, l’instruction publique
    avait des ambitions réduites ! (cf. citation de Ferdinand
    Brunot, plus haut). De nos jours on aborde des compétences
    heureusement bien plus variées et, fatalement, cela laisse
    moins de place pour l’orthographe.

    [Il faut réformer l’enseignement du
    français][enseignement-temps-réforme], c’est sûr, mais
    sûrement pas dans le sens du travail **bête et méchant**, de
    l’augmentation du temps alloué, de l’abrutissement des
    élèves, de l’intensification des dictées, du rôle punitif de
    l’orthographe ; et il faut réformer l’orthographe.

    [enseignement-temps-réforme]: https://www.tract-linguistes.org/lorthographe-francaise-de-1878-nest-plus-enseignable-depuis-longtemps-dans-le-temps-imparti-aux-cours-de-francais/

    > « épargnons ce temps si précieux qu’on dépense trop
    > souvent dans les vétilles de l’orthographe, dans les
    > règles de la dictée qui font de cet exercice une manière
    > de tour de force et une espèce de casse-tête chinois. »  
    > (Jules Ferry, ministre de l’Instruction publique, 1880)

 1. Ultime objection du conservatisme : l’orthographe (on ne
    parle même plus de langue), c’est notre Histoire, notre
    patrimoine. Seulement :

      - Le français est une **langue vivante**, un outil avant
        tout. La vie quotidienne de dizaines de millions de
        locuteurs n’a pas à porter le fardeau de deux
        millénaires d’Histoire dans une orthographe sanctifiée
        comme un monument aux morts.

      - L’Histoire, la vraie, avec un grand _H_, on la consigne
        dans les documents historiques, les monuments, les
        musées, les livres, on l’enseigne en cours… d’Histoire.
        L’orthographe (avec deux petits _h_), ses nombreuses
        difficultés, ce sont en partie l’accumulation
        d’**accidents historiques**, d’errements
        contradictoires, un millénaire de bidouilles conservées
        même quand elles sont devenues largement obsolètes, le
        tout n’étant essentiellement l’œuvre que de rares
        lettrés d’avant l’époque moderne ; en bref, le résultat
        de *petites histoires*, avec un petit _h_, qui ne sont
        même pas enseignées ! Combien savent la raison des
        pluriels en _-x_, du _y_ bizarre dans « crayon, pays »,
        du _h_ dans « huile, huitre », du _d_ dans « poids » ?
        Pour ce dernier, c’est une erreur d’étymologie commise
        par une personne unique (Robert Estienne) au
        16e siècle : on est loin du mouvement inexorable de
        l’Histoire.

      - Contrairement à une croyance répandue (voir aussi le
        mythe de la France éternelle), il n’y a **pas de
        continuité orthographique** avec nos Grands Auteurs du
        passé (illustration ? chercher dans ce document
        l’épigramme de Clément Marot auquel on doit,
        précisément, l’accord du participe passé). Molière et
        Racine, Montaigne et Corneille, comme tous les autres,
        ont été réécrits au fil des siècles. L’orthographe au
        temps de Molière était d’ailleurs nettement moins
        étymologisante qu’aujourd’hui, et le dramaturge lui-même
        s’insurgeait contre l’accord à la Marot.

 1. Simplifier l’orthographe, ce n’est pas tuer la langue. Au
    contraire c’est, en ôtant des entraves à son emploi qui se
    sont accumulées au cours des siècles, en éclairant les
    grands principes qui gouvernent à la formation de nouveaux
    mots, lui offrir une **nouvelle vitalité**.

    En particulier, simplifier c’est rendre le français plus
    compétitif à l’international, où il a perdu son statut
    privilégié. C’est lui permettre d’être plus souvent
    empruntée qu’emprunteuse, c’est faciliter son apprentissage
    en langue seconde, c’est favoriser la diffusion culturelle,
    c’est même encourager le tourisme.

 1. Simplifier l’orthographe, enfin, c’est faire des économies
    sur les ressources nécessaire à son enseignement. Voilà au
    moins un argument qui devrait convaincre ! ;-)



### Les bêtises habituelles

_(lecture facultative)_

L’argumentaire ci-dessus verse-t-il trop dans l’éloquence ?
C’est que les discours conservatistes sont trop souvent, sans
gêne aucune, d’une bêtise, d’une condescendance et d’une
violence extrêmes !
Ainsi dans le journal Le Monde du 31 décembre 1990,
un certain « Comité Robespierre » réclamait,
à propos des rectifications de 1990,
« la *guillotine* morale du mépris contre les technocrates
*sans âme et sans pensée* qui ont *osé* *profaner* notre
langue ».
Condamnation morale, déshumanisation, attaque personnelle voire
appel au meurtre, idolâtrie du monument historique… Champ
lexical du sacré, c’est bel et bien une question de religion !
Il n’y a pourtant presque rien de contestable dans la très
timorée réforme de 1990…

Chez les commentateurs,
certains s’inventent des histoires pour s’expliquer
l’inexplicable, et tant pis si elles ne tiennent pas debout
(superstition) :

> "Souvent, les enseignants savent expliquer comment on accorde,
> mais pas pourquoi. " Eh bien mon instit, d'avant le
> *pédagogisme*, lui, le savait ! "J'ai acheté une maison" :
> c'est l'acte qui compte (l'achat) "la maison que j'ai
> achetée" : cela veut dire : "j'ai une maison achetée" (état)
> Et mon prof d'anglais plus tard s'appuyait sur ce fait pour
> nous faire comprendre la différence entre prétérit et present
> perfect : "I buyed a house" yesterday, teh last week ...
> (acte), et "I have buyed a house" (possession actuelle).
> *Trop subtil pour des profs belges ?*

Certains nient ou méconnaissent carrément la complexité ;
ainsi, à propos de l’accord du participe passé (sachant aussi
que, même si chaque règle de grammaire individuellement était
simple, ce qui n’est pas le cas, leur *nombre* même représente
une complexité qui s’accumule):

> Il n'y a rien de plus simple que l'accord du participe passé :
> tous ceux qui ne la comprennent pas *devraient être embastillés*

> Ce n'est pas parce que *certains jeunes* n'arrivent pas
> à comprendre cette règle qu'il faut la bannir. Elle n'est tout
> de même pas bien compliquée.

> C'est une règle élémentaire du français qui s'apprend sans
> difficulté en primaire *(sauf pour les exceptions)*.

> Je ne suis pas plus grammairienne que cela mais la règle de
> l'accord avec le COD précédant l'auxiliaire "avoir" est
> logique. Ceux qui veulent abolir cette règle ont tout faux,
> ils voudraient simplement écrire en phonétique, au détriment
> du sens.

Si c’est logique après, pourquoi ça ne l’est pas avant,
vis-à-vis du sens ? Et non, écrire en phonétique n’est
absolument pas le propos de ceux qui veulent abolir la règle du
participe passé : un exemple parmi tant d’autres d’opinion mal
informée, de réflexion défaillante ou carrément de mauvaise foi.

> Mouais. Il y a quand même des règles plus difficiles
> à assimiler et bien plus tordues comme l'impératif: "Tu fais
> ce qu'il te plaît" "*Fait* ce qu'il te plaît"

Belle illustration combinée de [sophisme du
pire][sophisme-du-pire] et de défense d’une orthographe pourtant
pas maitrisée.

[sophisme-du-pire]: https://fr.wikipedia.org/wiki/Sophisme_du_pire

> Ce que vous trouvez complexe peut paraître simple à quelqu'un
> d'autre.

Oui oui, tout est relatif, c’est Einstein qui l’a dit.
Et même qu’il était nul en maths à l’école (non).

Dans la catégorie sophisme relativiste, on trouve aussi :

> Moi j’ai aimé apprendre les pluriels en X à l’école. Ça plaît 
> donc à certains enfants. Vous voudriez les priver de ce 
> plaisir ? Quel égoïsme !

Eh bien moi quand j’avais neuf ans j’adorais jouer aux échecs. 
Je trouve parfaitement injuste que les enfants en soient privés. 
Je pense donc qu’il faut des cours d’échecs obligatoires pour 
tous dès le CP. Évidemment, aux entretiens d’embauche, il faudra 
jouer une partie contre le recruteur. Notez que, contrairement 
aux pluriels en X, les échecs font travailler la réflexion…

Certains voient la poésie de toute chose :

> Les règles d'accord des participes passés sont un des charmes
> de notre langue.

L’orthographe serait un art raffiné ; il est donc bien normal
qu’elle soit inaccessible au commun des mortels :

> mes échanges avec des férus de la langue française m’ont
> permis de mieux apprécier l’élégante logique qui sous-tend la
> grammaire française, qui n’a rien à envier sur le plan de la
> cohérence conceptuelle à bien des théories scientifiques. Bien
> sûr, plusieurs règles de français ont leurs exceptions, mais
> ces exceptions sont très souvent régies par des règles tout
> à fait logiques.

Sur l’élégance, vertu toute subjective, on se gardera bien de
commenter. En revanche la logique et la science (la
lingüistique, par exemple) souffrent qu’on les compare à la
norme orthographique française. S’il faut consulter des férus
d’orthographe pour pouvoir s’y conformer, c’est peut-être qu’il
y a un problème, non ?

La langue touche à la pensée et à l’esprit humain :

> Écrire français, c’est penser en français et vouloir niveler
> par le bas c’est abârtadir la structure propre à la pensée
> française.

> Rabaisser, rabaisser, rabaissee. Si meme les profs ne savent
> plus expliquer... comment les enfants maitriseront les
> complexites de la vie, s'ils ne sont pas capables de maitriser
> celles de la langue, donc de la pensee .

Argument fascinant : ***la pensée française, c’est donc une
pensée tordue ? Une pensée toute de pinaillages et d’exceptions
perverses ?*** L’exception qui confirme la règle serait-elle un
[principe de raisonnement][doublepensée] ? Voilà qui pourrait
expliquer des choses ! Confusion semble régner entre réflexion
logique et complication gratuite, entre finesse d’analyse et
pensées aux méandres inextricables, entre « c’est logique » et
« ça découle de la sous-règle 34b, alinéa (i), cas spécial Q ».

[doublepensée]: https://fr.wikipedia.org/wiki/Doublepensée

D’innombrables commentateurs se plaignent de l’orthographe
déplorable de leurs contemporains (on peut d’ailleurs goûter
l’ironie de ces commentaires eux-mêmes tout pleins de fautes).
Un tel constat devrait engendrer au moins le début d’une remise
en question de la norme orthographique (esprit critique,
disions-nous), mais eux n’y voient [« que »][parcimonie] la 
bêtise et la paresse de toute une population :

[parcimonie]: https://fr.wikipedia.org/wiki/Principe_de_parcimonie

> à force de vouloir *tout* assouplir dans le but de rendre la
> langue française moins rugueuse et dans le but de nourrir le
> *dogme* de la facilité, n’est-il pas justifié de se demander
> si nos jeunes seront formés pour affronter les difficultés
> qu’ils devront affronter à court terme ? Nous devenons des
> pâtes molles, confortablement vautrés devant nos écrans !

> Le modèle americain : tout le monde à raz les pâquerettes.
> L'imbécilité pour tous, voila ce qui fait consommer !

Mais oui ma bonne dame, tout fout le camp, où va le monde.
Attention seulement à votre usage à contre-sens du mot
« dogme » : on l’a vu, le réformisme est guidé par la raison, le
conservatisme par la religion…

> A force d'émoticônes, d'abréviations plus ou moins
> acceptables, d'expressions de plus en plus raccourcies et
> autres fantaisies permettant de cacher leur incurie en
> français, les jeunes d'aujourd'hui ne savent plus parler le
> français, ni l'écrire. Cela se traduit par un vocabulaire se
> réduisant comme une peau de chagrin, et si cette tendance se
> poursuit les générations futures communiqueront par des
> grognements comme ces bons vieux hommes des cavernes. Et ce
> n'est pas les téléphones mobiles, tablettes et autres qui
> arrangeront les choses, bien au contraire... d'autant que ces
> derniers ont éradiqué la communication et le dialogue direct
> entre jeunes. Grave, très grave.

Le français, évoluer ? Comble de l’horreur ! Notre mauvais
patois dégénéré de latin vulgaire abâtardi par les barbares
germains superstrateurs est au faîte de sa perfection, mais
c’est une beauté fragile : le danger rôde, seule la règle
d’accord du participe passé distingue l’Homme de la bête.
Vigilance constante !

Décidément, la concurrence est rude pour la palme d’or de la
pente glissante la plus raide, apocalyptique même, teintée bien
souvent d’obsessions nationalistes et plus ou moins complotistes :

> Il convient donc de simplifier la grammaire, puis
> l'orthographe. Il ne restera plus qu'à simplifier l'histoire,
> et là nous ne serons plus Français!

> Voilà comment une culture et une langue disparaissent, par
> paresse de ceux qui sont censés la représenter

> Décidément les Belges sont toujours en avance sur nous. Leur
> apauvrissement intellectuel, voulu par les élites
> mondialistes, est tel qu'ils en abandonnent leur langue. Lire
> 1984 et la Novlangue, on est en plein dedans.

_1984_, cette personne anonyme ne l’a sûrement pas lu.
Quant aux « élites mondialistes », les profs de français, hum !

Le mépris des enseignants est d’ailleurs récurrent :

> Il y a quelque chose de profondément agaçant dans cette
> démission des enseignants devant les obstacles.

Sans parler des Belges et des Québécois, qui, il faut bien le
dire, ont des idées bizarres et ne parlent pas le vrai français.
Tout ça à cause de certains jeunes (inutile de préciser : de
banlieue, « de couleur », issus de l’immigration)…

… Bon, dira-t-on, tout ça c’est affligeant, mais ce ne sont que
des commentaires de quidams sur Internet, le caniveau du débat.
Certes ; mais ils sont symptomatiques de l’opinion publique, si
bruyante et si réfractaire au changement. Cependant, que disent
les discours anti-réformistes supposés *sérieux* ?

On abordera le cas de l’Académie plus tard ; pour l’heure,
intéressons-nous à une pépite parue dans le journal Le Devoir le
18 avril 2023, où un certain Patrick Moreau, professeur de
littérature à Montréal, s’étonne du temps nécessaire
à l’enseignement de la règle d’accord du participe passé
(l’emphase est de moi).

> *Je n’enseigne pas pour ma part* au secondaire, mais ce
> chiffre de 80 heures *me* laisse franchement pantois.
> A priori, *si* les élèves savent repérer le complément direct
> dans une phrase ainsi que les pronoms compléments qui se
> retrouvent syntaxiquement le plus fréquemment devant le verbe,
> enseigner cette règle demande *tout au plus 15 minutes* ;
> davantage, si on y inclut les verbes pronominaux, mais
> certainement pas 80 heures !
>
> Ce chiffre sorti de nulle part me semble être une pure
> exagération rhétorique …

Il oppose donc son estimation personnelle, individuelle, hors de
sa zone d’expertise, sans données concrètes, à une enquête de
terrain réalisée par un groupe de personnes compétentes.
Lui-même professant dans le supérieur, il ne doit pas avoir vu
d’enfants depuis plusieurs décennies, pour les confondre avec
des robots programmables à qui il suffirait de fournir des
règles de grammaire qui seraient instantanément enregistrées,
comprises et appliquées sans faute. Quinze minutes. *Quinze
minutes !* « Tout au plus » ! Hallucinant. Il trahit aussi son
ignorance de la réalité scolaire (une sous-évaluation un tant
soit peu réaliste du temps nécessaire inclut évidemment des
exercices en classe, des exercices à la maison, un contrôle en
classe, et la correction de tout ça, ce qui fait déjà *au
minimum* plusieurs heures).

> … destinée à convaincre un public compatissant, qui imaginera
> volontiers de pauvres enfants penchés des heures durant sur
> leurs pupitres à trimer comme des bons pour mémoriser et
> appliquer des règles absconses, archaïques et qui ne
> correspondent pas à leur « réalité ».

Effectivement, ses propres souvenirs d’école doivent être
lointains. Il y a de quoi rester songeur devant son pamphlet
_Pourquoi nos enfants sortent-ils de l’école ignorants ?_ En
revanche, la rhétorique, le vieux renard connait bien.

> Espérons qu’on n’appliquera pas la même logique aux règles des
> mathématiques et de la physique, certaines pas si simples
> pourtant, et dont quelques-unes — _horresco referens_ ! —
> remontent à Archimède ou à Pascal…

[rhétorique]: https://cortecs.org/language-argumentation/moisissures-argumentatives/

Les [impostures rhétoriques][rhétorique] qu’il vient de
commettre, donc, sont une analogie douteuse (la langue est un
fait humain, les lois de la Nature sont indépendantes de notre
volonté) et un homme de paille (il déforme l’argument des
réformistes selon qui certaines complexités ne sont plus
adaptées au français actuel, pour la présenter comme une
détestation de tout ce qui est vieux). Le tout subtilement
renforcé par un jugement moral (« espérons qu’on »).

> Je n’ai jamais lu que les professeurs d’anglais proposaient de
> réduire la liste des verbes irréguliers ou de rendre
> l’orthographe lexicale de la langue de Shakespeare plus
> phonétique …

[anglais-réformes]: https://en.wikipedia.org/wiki/English-language_spelling_reform
[anglais-spellsoc]: https://www.spellingsociety.org/economic-and-social-costs-of-english-spelling
[anglais-seymour]: https://www.researchgate.net/publication/10710502_Foundation_literacy_acquisition_in_European_orthographies_Electronic_version

Il ne doit pas parler à beaucoup de professeurs d’anglais, lui
qui enseigne pourtant au Canada ? <!-- On y devinerait presque
la même ignorance méprisante vis-à-vis de la langue anglaise,
que celle qu’on trouve chez des nombreux autres commentateurs…
--> Car la question de [réformer l’orthographe de
l’anglais][anglais-réformes] est, en fait, [bien
vivace][anglais-spellsoc]. Sait-il seulement que l’orthographe
nord-américaine est déjà simplifiée vis-à-vis de la
britannique ? L’anglais, [parlons-en][anglais-seymour], est
parmi les voisins du français l’une des rares langues dont
l’orthographe lui soit plus ardue ; c’est la langue d’Europe de
l’Ouest la plus longue à acquérir par ses propres locuteurs, qui
n’ont ensuite plus le temps ou les facultés pour apprendre une
langue étrangère… Prendre exemple sur le pire exemple qui soit,
c’est tout de même cocasse ! De toute façon, l’anglais, on s’en
fiche, c’est le français qui nous intéresse. Côté rhétorique, on
peut parler de faux dilemme (ce n’est pas : ou bien on simplifie
l’anglais et le français, ou bien aucun des deux), appelé par
d’autres erreurs de raisonnement (justifier de ne pas résoudre
un problème en mentionnant un autre problème, indépendant et
donc hors-sujet ; établir une analogie avec l’anglais, dont la
situation est différente au point que certains lingüistes
comparent l’écriture de l’anglais, dite lexicale, à celle du
chinois [Catach, p46 et p336]).

> … ou que ceux de mathématiques voulaient simplifier la
> trigonométrie !

À force qu’il persiste dans cette douteuse analogie, on commence
à se demander si lui-même ne concevrait pas la norme
orthographique, à l’instar des lois mathématiques, comme une
vérité immuable qui transcenderait la condition humaine, créée
par on-ne-sait-qui, et qu’on ne pourrait qu’accepter humblement.
On frôle à nouveau le religieux. On pourrait lui rappeler que la
règle de Marot ne s’est réellement imposée qu’à la fin du
19e siècle, quand l’école républicaine a normalisé la langue.
Pas très intemporel !

> *Étonnamment*, il *n’y a que* le français que l’on se *croit
> en droit* de transformer, de simplifier, de *malmener*.

Il se crée des étonnements. Généralités grandiloquentes,
complètement fausses, hors-sujet : un vrai discours de bistro.
Eh bien oui, en fait, *on* a tous les *droits*. Quelle autorité
supérieure serait propriétaire de la langue française ? Croyance
toujours. Quant au champ lexical : le français, pauvre créature
malmenée, souffrante, blessée, torturée ? Infâme cruauté ! Non,
le français n’est pas un animal, c’est une chose immatérielle et
dépourvue de sentiment comme de volonté, n’existant que pour
nous servir. N’ayons aucun scrupule à la réduire en esclavage.

> *À ce compte*, il faudrait bien supprimer également le pronom
> relatif « dont » que les élèves ont notoirement du mal
> à utiliser correctement, uniformiser les conjugaisons, abolir
> le subjonctif, etc.

Encore un homme de paille, ou un argument de la pente glissante,
voire un chiffon rouge.

Cet opinioniste va jusqu’à sous-entendre que la notion
d’« exception » (mot qu’il n’écrit qu’entre guillemets), qui
obséderait les réformistes… n’existerait pas !

> les grammairiens d’autrefois n’étaient ni des imbéciles ni
> d’infâmes dictateurs (…), mais des personnes pour la plupart
> honnêtes et souvent érudites qui, comme nous-mêmes, essayaient
> de rendre l’orthographe du français plus logique et plus
> cohérente

Eh oui… comme les réformistes actuels. Grandiose note finale :

> une leçon de respect pour ces êtres humains du passé.

Ne vous en déplaise, Professeur, j’aime mieux respecter les
vivants…



### Qui décide ?

La réponse, en tout cas, n’est sûrement pas l’Académie.

Il y a beaucoup à dire sur l’Académie française…
Pour faire court :
L’Académie est un club coopté d’écrivains,
apte à décerner des prix littéraires<!--
(avec les dérives qu’on peut y associer :
entre-soi, copinage, finances suspectes…)-->,
mais en aucun cas à régenter la langue française.
L’Académie française n’a qu’un pouvoir très limité,
n’est pas la seule institution dans le paysage,
empêche structurellement toute évolution de la langue,
est incompétente sur la langue française,
assène avec mépris des contre-vérités,
véhicule l’idée reçue d’une langue supérieure et immuable,
et défend une idéologie délétère, passéiste et misogyne.

Pour plus de détails, on pourra
lire le tract _Le français va très bien, merci_ (chapitre 4)
des « Linguistes atterré·e·s »,
[cet article de Jean-Benoît Nadeau dans L’Actualité][critique-aca-nadeau],
[ce billet de blog][critique-aca-spacefox],
regarder [cette vidéo de « Linguisticae » sur YouTube][critique-aca-linguisticae],
ou lire le long développement qui suit
_(lecture facultative)_.

[critique-aca-linguisticae]: https://www.youtube.com/watch?v=hfUsGmcr1PI
[critique-aca-spacefox]: https://textes.spacefox.fr/rédiger/l-academie-francaise-ne-sert-a-rien/
[critique-aca-nadeau]: https://lactualite.com/societe/trop-de-romanciers-pas-assez-de-linguistes/

**L’Académie française bloque structurellement l’évolution de la
langue.**

L’Académie s’est montrée volontariste durant les deux premiers
siècles de son existence. Dans un paysage où ne préexistait pas
de norme orthographique universellement suivie, elle en a créé
une, et n’a pas hésité à la réformer largement dans les éditions
successives de son dictionnaire — à plusieurs reprises dans le
sens de la simplification. 1835 est le tournant réactionnaire :
à cette date, restaurée par Napoléon, contrôlée par l’élite
bourgeoise de la Monarchie de Juillet, l’Académie effectue sa
dernière réforme d’ampleur : un retour à une orthographe plus
érudite, qui permet aux nantis de se distinguer. Pour
l’essentiel, cette orthographe de 1835, on y est encore en 2020.

Aujourd’hui l’Académie a beau jeu de se prétendre — quand ça
l’arrange — l’humble « greffier de l’usage » ; commodément
invoqué voire inventé, cet usage est plus que jamais prisonnier
d’une norme que, par le passé, elle a largement contribué
à codifier, et dont elle est aujourd’hui la gardienne farouche.
Tout écart à cette norme est une faute à combattre. Ainsi par
exemple de la non-féminisation des noms de métiers, refusée au
prétexte d’un usage que l’académisme lui-même avait installé aux
siècles précédents, selon des conceptions sexistes, contre
certaines pratiques préexistantes. Ainsi de l’apparition de
nouveaux mots ou de nouveaux sens ; le vocabulaire français
serait donc figé pour l’éternité, ce qui confère un sens très
particulier à la devise de l’Académie : « À l’immortalité » !
Or, une langue qui ne change plus, c’est une langue morte.

- Méconnaissant les besoins réels — de sens comme de concision —
  et la vie d’une langue en constante évolution, l’Académie
  s’oppose constamment aux anglicismes, mais aussi aux apocopes,
  aux extensions de sens jugées « abusives » (comme « juré »
  pour « membre de jury » ; ou le verbe « supporter » dans son
  sens sportif… alors qu’elle recommande « les supporteurs » !)
  qui sont pourtant une façon naturelle et normale de faire
  vivre une langue… et même à des mots formés de façon tout
  à fait régulière (comme « fuiter »). Obsession de pureté,
  bannissement de mots sous prétexte qu’ils peuvent être
  exprimés par d’autres, au mépris de leurs nuances de sens :
  ceux qui citent le novlangue d’Orwell à tort et à travers
  devraient en reconnaître là un trait majeur.

Si l’Académie a approuvé la réforme de 1990, par exemple, c’est
sous la pression politique et après en avoir largement édulcoré
le projet, qui n’émanait pas d’elle. Elle s’en est même
[complètement dédite][aca-renie-1990] en 2016. Il n’y a rien
à espérer d’elle.

[aca-renie-1990]: https://www.lemonde.fr/societe/article/2016/02/13/l-academie-francaise-contre-toute-reforme-de-l-orthographe_4864931_3224.html

Le blocage structurel de l’Académie a conduit à deux évolutions
majeures au tournant de 1900. Premièrement, les réformistes ont
compris que tout changement devait désormais se faire non plus
par prescription, mais de façon optionnelle : redonner du mou
pour laisser une chance à l’usage de se créer. C’est l’esprit
tant des tolérances Leygues de 1901 que des rectifications 1990.
Hélas, en matière d’écriture, il semble que l’usage soit plus
royaliste que le Roy : l’opinion publique française,
méconnaissante, attachée sans recul à ce qu’on lui a inculqué,
ignore ou refuse hostilement des changements que même l’Académie
a acceptés.

**Non, l’Académie française ne contrôle pas l’orthographe.**

Deuxième conséquence du blocus académicien, un second acteur
entre en scène : l’État, via des arrêtés et des commissions.
L’incarnation moderne en France en est la DGLFLF (Délégation
générale à la langue française et aux langues de France,
rattachée au ministère de la Culture), qui coordonne la CELF
(Commission d'enrichissement de la langue française). La CELF
définit des termes néologiques pour refléter les nouvelles
réalités du monde — en général, il s’agit de traduire des
anglicismes ; le lexique qu’elle instaure est en théorie imposé
dans les documents de l’État, tandis que les anglicismes y sont
interdits. L’Académie est membre de la CELF — et c’est là son
seul pouvoir officiel.

Mais la langue française dépasse les frontières de la France. En
Belgique existent des institutions analogues, le SLF (Service de
la langue française) et le CLFPL (Conseil de la langue française
et de la politique linguistique). Au Québec surtout, se trouvent
l’OQLF (Office québécois de la langue française),
particulièrement actif dans la croisade contre les anglicismes
et à qui l’on doit de nombreux néologismes plus ou moins
fantaisistes, et le Bureau de la traduction du Canada, auteur de
la base lexicale TERMIUM Plus. Il est à noter que Suisse,
Belgique et Québec sont tous trois en situation de
plurilingüisme, ce qui rend d’autant plus intéressants les
travaux de leurs institutions. Enfin existe une association
internationale, la CILF (Conseil international de la langue
française). Tous ces organismes coopèrent.

Toutefois, leur activité se cantonne à la création — parfois
artificielle — de nouveaux mots, à la traduction — parfois
à côté de la plaque — de locutions, à la réglementation du
lexique. Elles ne touchent ni à l’orthographe ni à la grammaire.
Celles-ci ne semblent être entre les mains de personne, sinon de
l’enseignement public et de la coutume.

<!--

Il faut distinguer *description* objective de la langue qui
existe, et *prescription* subjective de ce qu’elle devrait être.

Dans l’entièreté des
[statuts de l’Académie française][aca-statuts]
([PDF][aca-statuts-pdf]),
seuls deux passages définissent sa fonction (et encore, si les
statuts de 1635 émanent du pouvoir royal, ceux de 1816 semblent
auto-décidés) :

> « La principale fonction de l’Académie sera de travailler avec
> tout le soin *et toute la diligence possibles* à donner des
> règles certaines à notre langue et à la rendre pure, éloquente
> et capable de traiter les arts et les sciences. »  
> (article 24 des Statuts de 1635)

> « travailler à épurer et à fixer la langue, à en éclaircir les
> difficultés et à en maintenir le caractère et les principes »  
> (article 6 des Statuts de 1816)

[aca-statuts]: https://www.academie-francaise.fr/linstitution/statuts-et-reglements
[aca-statuts-pdf]: https://www.academie-francaise.fr/sites/academie-francaise.fr/files/statuts_af_0.pdf

On peut en effet voir dans cette fonction un rôle prescripteur.
L’Académie peut réfléchir, proposer et donner des avis. Elle est
membre décisionnaire de la Commission d’enrichissement de la
langue française (instance de la DGLFLF), et par là possède un
pouvoir sur le lexique employé et promu en France par l’État
— administrations et ministères. Cependant, *à ma connaissance*,
aucun texte extérieur à elle n’en fait la détentrice d’une
quelconque autorité directe sur la langue ; son orthographe
encore moins. En dehors d’instances officielles telles que la
Commission, les *opinions* de l’Académie n’ont pas plus force de
lois que celles de n’importe quel groupe privé, comme par
exemple le dictionnaire Le Robert.

TODO: la loi Toubon (étendue en 2023, pour interdire l’écriture
inclusive) impose le français dans un cadre beaucoup plus large
que les ministères publics, y compris des entreprises privées
(communication, publicité, etc.); vérifier si la langue imposée
a un rapport légal avec l’Académie.

-->

**L’Académie française est défaillante.**

La mission concrète confiée à l’Académie était de rédiger un
dictionnaire, une grammaire, un traité de rhétorique et un
traité de poésie. Elle est aujourd’hui, vis-à-vis de cette
mission, défaillante : son dictionnaire est décevant, il aura
fallu 300 ans pour qu’elle publie finalement une grammaire, et
celle-ci a été [tellement
critiquée][critique-aca-grammaire-brunot] que cette mission
a été totalement abandonnée, tout comme celles de produire une
rhétorique et une poésie dont on n’aura jamais vu le premier
mot.

[critique-aca-grammaire-brunot]: https://www.tract-linguistes.org/ferdinand-brunot-le-pionnier-atterre/

La rédaction du dictionnaire de l’Académie française est si
lente que la dernière édition complète (la 8e) remonte à 1935 et
que la prochaine (la 9e), entamée en 1986, toujours pas finie,
est déjà obsolète — ou le serait sans les ajouts pas toujours
naturels des commissions de terminologie (exemple :
[« courriel »][aca-dico-courriel]). En plus, cette rédaction est
largement sous-traitée ; on ne peut même pas attribuer cet
accomplissement unique aux académiciens eux-mêmes.

Le contenu même de ce dictionnaire est décevant, quoique la
9e édition soit notablement augmentée : le vocabulaire couvert
est assez limité (quoiqu’il passe de 32 000 à 55 000 mots dans
la 9e édition), l’étymologie ne fait son apparition que dans la
9e édition, et on ne renseigne toujours pas la prononciation, ni
familles de mots, thésaurus, synonymes, etc. En outre, si le
dictionnaire est mis à jour avec l’orthographe de 1990, celle-ci
n’est en général mentionnée qu’à la marge et seule l’ancienne
orthographe est mise en vedette (exemple :
[« gageure »][aca-dico-gageure]).

*Mise à jour :* La 9e édition a été *enfin* achevée et publiée
fin 2024, et elle est effectivement [très critiquable][critique-aca-dico9].

[critique-aca-dico9]: https://www.tract-linguistes.org/neuvieme-edition-du-dictionnaire-de-lacademie-francaise/

En description de la langue, il existe bien meilleur que
l’Académie : par exemple, pour le lexique, le dictionnaire
d’Émile Littré au 19e siècle ou le [_Trésor de la Langue
française_ (consultable en ligne)][TLFi] à la fin du 20e ; pour
la grammaire, le _Bon Usage_ de Maurice Grevisse pour le début
du 20e siècle ou la _Grande Grammaire du Français_ pour l’an
2000.

[TLFi]: https://www.cnrtl.fr/definition/

Et pour cause :

**Non, l’Académie française n’est pas compétente sur la langue
française.**

En 2020, sur 35 membres, elle ne compte ni lingüiste, ni
grammairien, ni même lexicographe. Seules deux personnes
déclarent une profession, philologues, qui pourrait à priori les
rendre aptes à la rédaction d’un dictionnaire… mais celles-ci ne
siègent pas à la Commission du Dictionnaire ! Les autres se
déclarent en majorité écrivains, mais on trouve aussi nombre de
politiques, diplomates, hauts fonctionnaires, avocats,
journalistes, historiens (mais pas d’histoire des langues),
philosophes… un réalisateur, un évêque et un biologiste. La
nomination de ce dernier, juste après son obtention du Nobel de
médecine et sans avoir rien publié en français, est de toute
évidence une affaire de prestige et non de langue française. La
désignation des membres de l’Académie, par cooptation, est
sujette à tous les biais, jeux de pouvoir et copinages. De plus,
elle favorise la perpétuation de la norme et non sa remise en
question.

En clair, les membres de l’Académie ne sont pas des
professionnels compétents pour la rédaction d’un dictionnaire
descriptif, contrairement par exemple aux auteurs du Robert.
Faute d’[expertise lingüistique][médias-opinion], elle ne peut
se prétendre le « greffier » d’un usage qu’elle ne sait pas
étudier ; tout au plus peut-elle — peut-être — se prévaloir de
quelque érudition littéraire. L’Académie n’a pas changé de
nature depuis sa création ; son *opinion* reste,
fondamentalement, celle d’un comité d’écrivains qui évalue
— selon des critères intangibles et subjectifs — la beauté ou
plutôt la pureté du langage d’une œuvre littéraire à fin,
éventuellement, de lui décerner un prix ; ce même jugement n’a
pas vocation à s’étendre à toute l’expression quotidienne,
écrite et orale, de tous les francophones.

[médias-opinion]: https://theconversation.com/linguistique-education-quand-les-medias-confondent-opinion-et-expertise-215615

Si aujourd’hui l’opinion publique en France — en France mais pas
en Belgique ou au Québec — lui accorde autant d’attention, c’est
bien souvent en raison d’un prestige supposé, par méconnaissance
de sa nature réelle… voire par opportunisme, pour faire front
commun contre les souillures anglicistes ou les outrages
féministes. On cite l’Académie quand ça nous arrange !

**L’Académie française est réactionnaire.**

L’Académie n’est pas neutre politiquement. De par sa
constitution, elle est fermement ancrée dans la droite
conservatrice, mode de pensée qui imprègne toutes ses prises de
position. Prendre l’Académie comme référence universelle pour la
langue française, c’est faire infuser ses biais dans la société
francophone. Or, l’Académie promeut une vision faussée du monde
et une idéologie délétère.

L’Académie, on l’a déjà dit, exerce sur la langue un purisme
extrême, résolument passéiste et xénophobe. Elle déconsidère les
faits lingüistiques récents qu’elle ne voit que comme des modes
futiles et ignorantes qui corrompraient la langue. Elle oublie
que les usages d’aujourd’hui sont les barbarismes d’hier, ou que
les mots qu’on emprunte aujourd’hui de l’anglais, par exemple,
nous reviennent bien souvent du français ou du latin.

Concernant ce purisme, en outre, l’Académie voit midi à sa
porte. Le supposé bon usage qu’elle défend n’a en réalité rien
d’universel et est biaisé par l’idiolecte (les habitudes
langagières personnelles) de ses membres, façonné par leurs
origines sociales et où domine nettement la bourgeoisie
parisienne.

L’Académie alimente le mythe historique d’une France [monolingue][plurilinguisme]
et d’une langue française uniforme et intemporelle. Dans la
droite lignée de la planification lingüistique révolutionnaire,
convaincue que laisser exister des faits régionaux serait un
péril mortel pour l’unité de la France, elle s’est par exemple
opposée — outrepassant ses attributions — à la Charte européenne
des langues régionales ou minoritaires, qui vise à défendre et
promouvoir lesdites langues (breton, basque, etc.).

[plurilinguisme]: https://www.agirparlaculture.be/philippe-blanchet-le-plurilinguisme-est-une-caracteristique-de-lhumanite/

L’Académie manifeste régulièrement son mépris pour les autres
langues que le français, et cela va plus loin que la simple
lutte contre les emprunts étrangers et les baragouins régionaux.
En dépit de la réalité scientifique, elle semble croire à une
supériorité du français et participe à la croyance collective
selon laquelle il serait la langue de la rigueur et de la
précision.

L’Académie a été — et demeure — notoirement misogyne. Ce n’est
qu’en 1980 qu’une femme (Marguerite Yourcenar) y entre enfin, au
prix d’interminables débats où on a pu entendre des opinions
exorbitantes (« si on élisait une femme, on finirait par élire
un nègre »). Ce n’est qu’en 2019 et à contrecœur qu’elle a fini
par accepter la féminisation des noms de métiers — ou plus
exactement des titres prestigieux, comme « avocate, députée »,
pourtant déjà naturels et implantés dans l’usage ; elle ne
voyait rien à redire à « caissière, ouvrière, coiffeuse ». On
n’a de cesse de comparer la langue française, ou l’Académie
elle-même, à une « vieille dame » vénérable dont il faudrait,
par égard, passer les outrages de l’âge ; ces métaphores au
formol, en 2024, ça ne fait pas très sérieux.

L’Académie, enfin, est mûe par un anti-scientifisme permanent,
apparemment nécessaire pour justifier son existence face à une
science lingüistique qui a émergé après elle, et à laquelle elle
est restée étrangère. Pour dénigrer ce qui lui déplait,
dépourvue d’arguments sérieux, elle se livre fréquemment à des
rhétoriques violentes, aussi ignobles que sans valeur.

Les exemples ne manquent pas. On peut lire par exemple les
attaques de Lancelot alias Abel Hermant contre le lingüiste
Ferdinand Brunot (qui critique le traité de grammaire de
l’Académie), ou la tribune « L’Assemblée nationale corruptrice »
de Maurice Druon dans le Figaro en 1999 (contre la féminisation
des titres et métiers). On peut également faire un tour sur le
site Internet de l’Académie française, en particulier sa fameuse
section « Dire, ne pas dire ». On y trouvera à loisir
incompétence, mauvaise foi, bassesse rhétorique, condescendance,
pédantisme, jacobinisme lingüistique, passéisme… Morceaux
choisis :

+ [Dans un billet][aca-tibère] perfidement allusif, on invoque
  l’empereur romain Tibère (pire, on le cite à travers un
  mauvais historien) pour « rappeler qu’il n’est peut-être pas
  mauvais que les hommes d’État s’expriment dans *la langue du
  pays* dont ils ont la charge. » Non seulement l’anecdote
  n’illustre en rien cette thèse, mais en plus elle oublie que
  le latin n’était en aucun cas la langue unique du vaste Empire romain.
  Quant à croire, aujourd’hui, que le latin aurait souffert des
  emprunts grecs… !
+ [À propos de « _follower_ »][aca-dire-follower], l’Académie
  étale son mépris général de toute langue étrangère, démontre
  sa mécompréhension particulière du terme qu’elle se propose de
  traduire (qui signifie davantage « abonné » que « fidèle »),
  associe ceux qui l’emploient à Staline, sort un exemple de son
  contexte, convoque nos racines chrétiennes, tartine
  oiseusement une érudition hors de propos pour finalement
  conclure, sans qu’on sache si le sens du ridicule est voulu et
  à quoi rimerait cette mauvaise blague, que la meilleure
  traduction de « _follower_ » serait « acolyte des illustres ».
+ [À propos de « propale »][aca-dire-propale], les mots-valises
  sont longuement comparés à d’affreux monstres de Frankenstein
  qui se retourneraient contre leurs créateurs fous. Par cette
  métaphore surréaliste, on voit donc encore que l’Académie est
  opposée à la création de mots dans la langue !
+ [Dans un billet][aca-dacc], un académicien s’insurge contre
  les apocopes (comme « vélo » pour « vélocipède », « cap’ »
  pour « capitaine »), qu’il voit partout et qui sont absolument
  intolérables, y compris à l’oral. « Nos aînés, eux,
  n’abusaient pas de l’apocope », prétend-il. « Ils avaient
  plutôt un penchant pour l’aphérèse \[comme “pitaine” pour
  “capitaine”\]. » Pas de doute, c’était mieux avant !

[aca-dico-gageure]: https://www.dictionnaire-academie.fr/article/A9G0040
[aca-dico-courriel]: https://www.dictionnaire-academie.fr/article/A9_0090
[aca-dire-follower]: https://www.academie-francaise.fr/followers
[aca-dire-propale]: https://www.academie-francaise.fr/propale-pour-proposition-commerciale
[aca-tibère]: https://www.academie-francaise.fr/tibere-et-les-anglicismes
[aca-dacc]: https://www.academie-francaise.fr/dacc-0

Bref.



### Autorité, punition, affection

**Non, on ne punit pas ceux qui ont appris l’ancienne orthographe.**

> « Évidemment, c’est difficile de dire à ceux qui ont souffert,
> qu’ils ont souffert pour rien. »  
> (_La faute de l’orthographe_, Arnaud Hoedt et Jérôme Piron)

On l’a vu en 2016 (`#jesuiscirconflexe`) : l’hostilité en France
vis-à-vis de tout changement est largement le fait de gens
ordinaires, qui ont souffert à l’école, qui ont peur que
l’orthographe qu’ils ont consacré tant d’efforts à maitriser
— et que bien souvent, malgré tout, ils ne maitrisent pas
entièrement — soit tout-à-coup déclarée hors-la-loi (le fantasme
d’une autorité qui régulerait la langue). Et qui souhaitent
donc, fort logiquement, que leurs enfants souffrent autant
qu’eux ; après tout ils ont survécu, non ? Ces personnes
défendent le système qui les oppresse en s’imaginant que toutes
ces complications devraient avoir une raison, qu’eux-mêmes
ignorent, puisqu’elles auraient forcément été sciemment décidées
par des experts du français (voir plus haut l’argument sur la
discipline contre l’esprit critique).

Il convient donc de combattre l’idée reçue selon laquelle une
langue vivante devrait être régulée par une autorité supérieure
(voir section précédente). Il faut également rassurer
abondamment les locuteurs que *leur* orthographe restera
acceptée tant qu’ils vivront, et qu’ils ne subiront pas de
discrimination (par exemple dans les lettres de motivation).

Il faut, en fait, désacraliser l’orthographe, décrisper les
esprits à son sujet. L’important n’est pas son immuable unité,
mais l’intercompréhension.

Il y a aussi, dans cet attachement à l’orthographe apprise dans
les petites classes, côtoyée toute sa vie durant, aussi
familière, aussi intime qu’un meilleur ami, un évident rapport
affectif. Quelque chose de l’identité, quelque chose même du
rapport à l’enfance. Pour qui l’intemporelle _cuiller_
n’évoque-t-elle pas bien plus qu’une terne cuillère, un bon gros
couvert en bois, tout simple et rassurant, où laper au coin du
feu la bonne soupe chaude de grand’mère dans une maisonnette au
cœur des bois merveilleux de Perrault ? Quel triste cochliophobe
ne se remémore pas avec bonheur un monde simple et pastel de
chaises miniatures, de goûters dans le cartable, de lettres
rondes et d’odeur de craie ? Pour certains locuteurs, attaquer
l’orthographe crée sans doute un sentiment de dépossession,
d’acculturation. Certes, mais si c’est vrai, alors les nouvelles
générations se prendront d’affection pour la nouvelle
orthographe, celle qu’ils auront apprise ; ou bien, parce qu’on
aura pris soin de la désacraliser, ne seront-ils plus aussi
sensibles à de telles broutilles.



### Une orthographe bêtement phonétique ?

**Non, une orthographe rationalisée n’est pas bêtement phonétique.**

> « Je ne veux pas qu’on m’abîme mes mots »  
> (Colette, écrivaine)

Il est de bon ton de tourner en dérision les propositions de
simplification en extrapolant une orthographe purement
phonétique, ironiquement qualifiée de « lojik ». Or, rares sont
ceux qui le proposent sérieusement. En effet, une orthographe
adaptée au français réalise nécessairement un compromis entre
**phonologie**, **morphologie** et **étymologie**. Ce que tente
déjà l’orthographe actuelle, de façon défectueuse.

Ceux qui se moquent des propositions de réforme se dispensent en
général d’argument ; ils donnent quelques exemples et semblent
estimer que le choc graphique qu’ils provoquent sur le lecteur
suffit en à démontrer le supposé ridicule. Seulement la
**physionomie** des mots, leur aspect graphique, est une
habitude acquise (normalement) durant la petite enfance.

Il y a cependant là un vrai point qui doit être soulevé : on ne
lit pas lettre à lettre, du moins pas efficacement. On ne passe
pas non plus par la prononciation pour comprendre ce qu’on lit
(exemple : "ville, fille" : on reconnait instantanément les mots
signifiés alors même que la prononciation suggérée par leur
graphie est fausse ; au contraire, avec une écriture phonétique,
reconnaitre #"si san si si si si san si siprè" nécessite une
oralisation et pas mal de réflexion). Les mots sont en grande
partie reconnus de façon réflexe par leur aspect d’ensemble.
C’est la clé d’une lecture rapide.

Donc, d’une part, pour ne pas porter préjudice aux adultes qui
ont appris l’ancienne orthographe, la nouvelle orthographe ne
devrait pas s’en éloigner trop, faute de quoi il serait
fastidieux pour ces personnes de lire la nouvelle orthographe.

D’autre part, adopter une orthographe complètement phonétique
réduit la différentiabilité visuelle des mots, en supprimant les
lettres muettes et en minimisant le nombre de graphies possibles
pour un même son. Dans le cas extrême, cela crée des
homographes. Même si cette orthographe était acquise durant
l’enfance, elle resterait donc plus difficile à lire (quand bien
même les mots seraient plus courts ! raccourcir les mots n’a que
peu d’intérêt).

Le besoin de différentiabilité plaide en faveur d’une
orthographe morphologique et non seulement phonétique.

Il existe plusieurs autres raisons de ne pas adopter un système
purement phonétique.

 1. Une orthographe du français doit assurer une certaine
    continuité en surmontant autant que possible les variations
    de prononciation, autant dans l’espace (accents régionaux)
    que dans le temps (évolution de la langue). L’évolution
    future de la langue étant difficile à prédire, le second
    point est plus délicat, mais il est certain que coller au
    plus près de la phonétique impose de modifier très
    régulièrement l’orthographe, et ainsi de rendre rapidement
    les textes antérieurs illisibles. Les variations de
    prononciation peuvent même concerner un même locuteur, selon
    le soin de l’élocution par exemple. De plus, certains mots
    admettent plusieurs prononciations.

    Il faut donc à tout le moins que l’orthographe soit, non pas
    phonétique, mais *phonologique*, c’est-à-dire qu’elle fasse
    abstraction des variations mineures de prononciation (par
    exemple les différentes façons de prononcer le R, les
    liaisons facultatives, les voyelles longues ou brèves,
    ouvertes ou fermées, des S qui se prononcent `[s]` ou `[z]`
    selon les locuteurs…).

 1. Le français oral comporte de très nombreux *homophones*,
    entre autres parce qu’il a effacé beaucoup de consonnes
    vis-à-vis de ses origines latines (beaucoup plus que
    l’espagnol par exemple) et modifié de nombreuses voyelles.
    Une orthographe phonétique confondrait tous ces mots.

 1. L’orthographe actuelle porte des informations précieuses sur
    la *morphologie* des mots. Les terminaisons permettent de
    passer du masculin au féminin, du singulier au pluriel, de
    déduire le temps et la personne à laquelle un verbe est
    conjugué. Or, ces terminaisons sont très souvent muettes.

    L’orthographe peut également indiquer qu’un mot est
    l’assemblage d’un préfixe et d’un radical, par exemple, ce
    qui peut éclairer sur son sens.

 1. L’orthographe actuelle comporte de nombreuses informations
    d’*étymologie*. Celles-ci sont censées éclairer sur le sens
    des mots. Cet argument est à nuancer au 21e siècle, alors
    que le sens-même peut avoir très largement dérivé, et que la
    lecture n’est plus l’apanage de bourgeois lettrés versés
    dans le latin et le grec ancien. Il n’est pas raisonnable
    qu’une langue vivante requière la connaissance d’une autre
    langue, morte, pour être comprise. _Delenda est Carthago_ !
    Toutefois, même si les liens des mots actuels vers leurs
    racines anciennes ne sont pas directement intéressants, ceux
    *entre* mots actuels issus d’une même racine le sont (par
    exemple, il n’y a plus aucun son commun entre [kalsifER] et
    [So], mais la graphie de ces mots "calcifère" et "chaux"
    rappelle leur relation). Il convient donc de conserver une
    certaine quantité d’étymologie dans l’orthographe, quitte
    à effacer celle qui n’est plus nécessaire.

    L’étymologie facilite également les échanges et
    l’intercompréhension avec les langues voisines, dont la
    prononciation a dévié bien plus que l’orthographe.

    L’étymologie constitue enfin un recours supplémentaire pour
    distinguer graphiquement des homophones quand la morphologie
    n’y suffit pas.





<!------------------------------------------------------------->

## Une vieille rengaine



### Principales difficultés de l’orthographe

Difficultés identifiées de façon récurrente
depuis au moins le 19e siècle
(ici ordonnées par importance pratique, classement subjectif):

 1. **CONSONNES DOUBLES**. celles-ci sont :
    - non nécessaires (on a inventé l’accent grave)
    - parfois contraire à la phonologie (ex: "ressembler, resituer, interaction")
 1. **consonnes semi-muettes** en fin de mot, qui s’entendent
    parfois en liaison
 1. **consonnes muettes** (en général étymologiques) en milieu
    de mot
 1. **multiples façons d’écrire de nombreux sons**. les pires :
    - `[s]` ("ce/se, ça/sa…")
    - `[E]`
    - `[5]`
    - `[@]` ("résident" et autres mots en -ent)
    - lettres grecques
 1. **circonflexe** multifonction et souvent inutile
 1. **tréma** mal expliqué et pas systématique
 1. **H** multifonction
 1. Le tout est aggravé par la _complexité accidentelle_ du
    français : de très nombreuses **IRRÉGULARITÉS** et
    exceptions inutiles (ex: mots en "-oux, -eux")… qui parfois
    même résultent d’erreurs ! Ces irrégularités troublent les
    proximités (étymologiques ou morphologiques) entre mots
    (contraire à la motivation des difficultés listées
    précédemment).
 1. **MOTS COMPOSÉS** (espace, tiret, rien ?) et leurs pluriels

(La réforme de 1990 s’attaque au circonflexe et au tréma, dans
les deux cas sans aller jusqu’au bout, elle s’attaque à une très
petite portion des consonnes doubles, et elle opère quelques
corrections ponctuelles. Elle modifie aussi certains mots
composés, sans pouvoir être considérée comme une simplification
sur ce point.)

Difficulté secondaire : **ambigüités orthographe → phonologie**

Difficultés qui débordent du cadre strict de l’orthographe pour
toucher à la grammaire :

  - **ACCORD DU PARTICIPE PASSÉ**
  - -S de l’impératif
  - accord de "tout"
  - accord de "vingt" et "cent"

On trouve dans [Catach, p55] des statistiques (anciennes) sur
les fautes à la dictée selon leur nature. Les pires fauteurs de
troubles sont de loin les consonnes doubles et le circonflexe ;
puis "an/en" et les lettres grecques.

#### Florilège personnel

Quant à moi, alors même que j’estime modestement maitriser
davantage l’orthographe normative que la majorité des gens, et
avoir su écrire pratiquement sans faute depuis l’enfance, il
y a certains points qui m’ont longtemps posé problème, certains
même qui continuent et ne voudront *jamais* rentrer.

+ **consonnes doubles** : impossible de mémoriser l’orthographe
  de mots comme "abattre, abréviation, aggraver, agresser,
  alourdir, attraper, atterrir, résonance, essoufflé, ressortir,
  débarrasser, cale, chope",
  et même "courir" que je veux toujours écrire #"courrir", et
  même la famille de "lettré, illettrisme" dont ma mémoire
  a retenu qu’un des mots comporterait piégeusement un
  `{t}` simple ("allitération", et l’orthographe anglaise) ;
  le suffixe `{-an(n)e}` est infernal ;
  en cas de doute, pas d’autre recours que le dictionnaire !
  - je vais jusqu’à [éviter d’écrire][insécurité] certains mots
    courants: "balade, tranquille" (car il est plus rapide
    d’écrire "promenade" que de chercher dans un dico) ;
  - il y une forme de « mémoire de l’écriture » : souvent, je
    suis incapable d’épeler un mot de tête bien que je l’écrive
    correctement sans réfléchir ;
+ **consonnes doubles** (doublons la mise) ;
+ pluriels en `{-x}` des mots en `{-eu, -ou}` : je suis bien
  incapable de me rappeler quels mots sont concernés, je ne sais
  jamais quel est le pluriel de "feu", je doute de plus en plus
  souvent pour "lieu", je suis incapable de réciter les 7 mots
  magiques en `{-ou}` et, pire, je ne me souviens jamais si ce
  sont ces 7 mots qui prennent un `{-x}` et tous les autres un
  `{-s}`, ou vice-versa !
+ "aujourd’hui" : l’orthographe de ce mot me terrifiait durant
  l’enfance ; je l’ai diversement écrit #"aujour’dui,
  aujour-dui, aujourd-hui, auj’ourdui…" (il y une apostrophe
  quelque part au milieu du mot, un peu comme "jusqu’à") ou
  #"au-jour-d’hui" (le maitre d’école ayant bien été obligé
  d’expliquer que c’est une ancienne composition); si maintenant
  je l’écris sans faute, c’est parce que j’ai consacré beaucoup
  d’efforts, à l’école, à retenir son orthographe ;
+ "s’il te plait" : j’écris systématiquement des tirets ; mon
  cerveau a retenu qu’il y a un piège dans ce mot, mais il est
  distrait par la question du possible circonflexe ;
+ "en dessous" : je l’écris avec un tiret, comme "au-dessus" ;
+ "champ" : à cause de "corps, temps", mon cerveau insiste,
  encore aujourd’hui, pour y mettre un `{-s}` ;
+ "fonds" : impossible de me résoudre à écrire "un fonds",
+ tréma : impossible de me faire aux orthographes pré-1990 comme
  "aiguë", qui sont contraires à l’usage du tréma tel qu’on me
  l’a enseigné et tel que je le comprends ; et pourquoi n’y en
  a-t-il pas sur "linguiste, équidistant, équation" et tant
  d’autres ?
+ circonflexe :
  - `{â}` : je veux écrire #"châpeau, châpiteau"
    comme "château, gâteau, hôpitaux",
    confusion mentale aggravée par l’association d’idées
    chapeau--cime--circonflexe (#"cîme", comme "faîte" ?)
  - `{ê}` : je me trompe toujours pour "crème, barème, emblème",
    d’autres mots suggérant fortement le circonflexe ("crêpe,
    chrême, carême, blême, même, extrême…")
  - `{ô}` : je veux mettre un circonflexe sur de nombreux `{o}`
    que je prononce fermés en syllabe finale fermée
    (#"vélodrôme, mélanôme, idiôme, j’ôse, psychôse, osmôse…"
    comme "drôle, Drôme, arôme, diplôme, fantôme, binôme, Nivôse")
    et dans les dérivés de ces mots
    (#"drôlatique, arômatique, fantômatique, binômial, polynômial…"
    comme "diplômé, rôliste"),
    et même #"halô" (comme "allô" et "hâle") ;
    impossible de savoir quand en mettre ;
+ "piqûre, piqure" : j’écris #"piqüre",
  car c’est un cas d’école du tréma, comme dans "aigüe"… non ?
+ accent aigu/grave: "évènement" (cette graphie n’a pas de sens,
  soit on prononce "événement" soit on prononce #"évènment"),
  "télescope"
+ "cauchemar" : je l’écris avec `{-d}` comme "cauchemardesque" ;
  si on me dit qu’il n’y a pas de `{-d}`, je l’écris avec `{-e}`
  comme "tintamarre" ;
+ "shampooing" : combien de `{p}`, combien de `{o}`, pourquoi un `{g}` ?
  ayant retenu que ce mot est très bizarre, je l’écris toujours `{sch-}` ;
  au secours !
+ verbes en `{-soudre}` : j’ai été pénalisé
  en cours de mathématiques(!) pour avoir écrit #"on résoud" ;
+ fautes que je ne commets pas moi-même, mais que je rencontre
  très fréquemment et suis incapable de considérer comme
  illogiques : #"connection" (innombrables ressources
  informatiques, y compris livres imprimés), #"réservé aux
  résidants" (panneaux urbains !).
+ "gageure" : n’ayant vu ce mot qu’écrit, je n’ai appris sa
  véritable prononciation qu’au détour d’un document sur
  l’orthographe de 1990, une dizaine d’années après ma première
  rencontre avec ce mot ; quant à des mots comme "Œdipe,
  œsophage", même mes profs de français de collège les
  prononçaient `[œ-]` ;
+ pluriel des mots composés : évidemment…
  (une fois de plus, dico = seule solution)
+ majuscule ou minuscule à "nord, sud, est, ouest" ?
+ accord de "tout" : je n’avais aucun souvenir de ces règles
  (qui certes reflètent ce qu’on fait inconsciemment à l’oral) ;
  est-ce qu’on me les a enseignées à l’école ?
+ accord de "cent, vingt" : idem.



### Principales pistes de simplification

Propositions les plus fréquentes, qui s’attaquent aux problèmes
identifiés comme les plus importants :

  - consonnes doubles
    + suppression (dé-doublement)
  - pluriels en "-x"
    + suppression de l’exception en "-x" concernant 7 mots en "-ou"
    + suppression de l’exception en "-x" pour les mots en "-eu"
      (mais les mots qui s’écrivent "-eux" même au singulier,
      comme "peureux", resteraient inchangés)
    + suppression de l’exception en "-x" pour les mots en "-au"
      (mais le pluriel des mots en "-ail, -al" resterait "-aux")
    + plus brutal : remplacement systématique de "-x" par "-s",
      même pour les mots comme "peureux",
      même pour les pluriels des mots en "-ail, -al"
  - lettres grecques
    + suppression (écriture phonétique)
    + *ou* réécriture de "ch, ph" en "kh, fh"
  - h muets
    + suppression
  - mots en "-ent, -ence" (problème plus mineur mais facile à résoudre)
    + réécriture en "-ant, -ance"
  - accent plat ([Catach])
  - accord du participe passé
    + diverses tolérances
    + simplification drastique:
      toujours invariable avec l’auxiliaire "avoir"



### Bref historique

(Cliché : au 12e siècle aurait existé une bonne adéqüation entre
l’oral et l’écrit, avec une orthographe qui aurait fidèlement
rendu compte des nombreuses voyelles de l’époque et des accents
locaux… Plus ou moins vrai, mais nuancé par [Catach, p209].)

Principales réformes « récentes » :

  - 1740 : Académie réformatrice, près de 1/3 du dictionnaire
    est modifié dans le sens de la simplification
  - 1835 : Académie réactionnaire, remplace "oi" par "ai" et les
    pluriels "-ans" par "-ants", mais réintroduit de nombreuses
    graphies plus érudites
  - 1900, 1901 : arrêtés (Leygues), déclare diverses tolérances
    (mots composés, nombreux cas d’hésitation singulier/pluriel,
    accord de "tout, cent, vingt", du participe passé…) — ignoré
  - 1976 : tolérances (Haby, Mistler) inspirées des précédentes
    — ignoré
  - 1990
  - *bientôt ?*

Une proposition révolutionnaire de nouvel alphabet : Domergue, 1805

Principales propositions de réforme « récentes » :

<!--
  - abbé de Dangeau, 17e siècle
  - Du Marsais, 1730
  - Duclos, 1754
  - Didot l’aîné, 18e siècle
  - Ambroise Firmin Didot, 1867
-->

  - Littré
  - Firmin-Didot, 1867
  - note Gréard, 1893
  - commission Meyer puis réponse de Faguet, 1903–1905
    * significatif: **l’Académie elle-même, par la voix d’Émile Faguet,
      « reconnaît qu’il y a des simplifications désirables »**,
      certaines importantes : suppression des pluriels en -OUX,
      des lettres grecques Y et RH, de certains circonflexes
      (fait en 1990), alignement de consonnes doubles
      (fait en 1990), des suffixes -ENT/-ANT, -TIEL/-CIEL…
  - Pernot-Bruneau, 1948
  - Beaulieux, 1952
  - Ligue pour une réforme orthographique, 1952
  - commission Beslais, 1955
  - Mistler, 1976
  - *aujourd’hui !*

L’article _La bataille de l’orthographe aux alentours de 1900_
(cf biblio) retrace l’Histoire des débats et des propositions
autour de l’an 1900, en resituant leur contexte historique et
notamment le rôle de l’Académie et de l’État.

Dans [Catach, p239] on trouve un exemple de texte réécrit selon
plusieurs propositions de réforme.



### Ceux qui en parlent aujourd’hui

Quelques noms qui reviennent tout le temps:

+ [RENOUVO][], association qui explique et promeut la réforme de 1990
+ [ÉROFA][], association publiant des études sur la rationalisation de l’orthographe,
  fondée par le lingüiste Claude Gruaz,
  dans le prolongement des travaux de Nina Catach
  (leur dictionnaire: _Dictionnaire de l’orthographe rationalisée du français_)
+ Nina Catach, lingüiste et historienne, spécialiste de l’orthographe française
  (son livre qu’on cite tout le temps: _Les Délires de l’orthographe_)
+ Arnaud Hoedt et Jérôme Piron, professeurs belges de français
  qui se sont faits les chevaliers de la simplification
  (leur livre et spectacle: _La Faute de l’orthographe_)
+ [les « Linguistes atterré·e·s »][tract-linguistes],
  collectif qui a donné un coup de pied dans la fourmilière à l’été 2023
  (leur tract incroyablement sulfureux: _Le français va très bien, merci_)
+ …

[RENOUVO]: https://www.renouvo.org/
[ÉROFA]: http://erofa.free.fr/
[tract-linguistes]: https://www.tract-linguistes.org/
[convivialité]: https://www.agirparlaculture.be/pour-une-orthographe-a-notre-service-spectacle-la-convivialite/





<!------------------------------------------------------------->

## Notations

+ phonologies: entre `[crochets]` (plutôt qu’entre `/barres obliques/`)
+ graphèmes ou plus généralement portions de mots: entre `{accolades}`
  - si seulement en position suffixe: `{-suffixe}`
  - si seulement en position préfixe: `{préfixe-}`
  - si dans un certain contexte: `{contexte<graphème>contexte}`
+ graphies de mots complets: entre `"guillemets droits"`
  - pour parler de tous les mots apparentés: `"dinosaure+"`
+ les graphies spéculatives sont précédées d’un dièse: `#{x}`, `#"x"`
+ le tilde `~` signifie « prononcé comme »

L’alphabet phonétique utilisé dans ce document
est celui de la base de donnée Lexique3
et est spécialisé pour le français;
il est compatible avec l’API (alphabet phonétique international)
mais utilise des symboles plus simples
à la place de ceux qui sont difficiles à saisir au clavier.
Ci-dessous le détail de cet alphabet phonétique
(pour chaque symbole
 est indiqué le symbole API si différent
 puis une transcription en graphie française si pas évident):

* voyelles et semi-voyelles:
  + `[a]`
  + `[A]` = `[ɑ]` = `{â}` ("pâte"), distingué de `[a]` par quelques locuteurs
  + `[@]` = `[ɑ̃]` = `{an}`
  + `[°]` = `[ə]` = `{e}` caduc, aussi appelé schwa ("cheval")
  + `[2]` = `[ø]` = `{eû}` fermé ("deux")
  + `[9]` = `[œ]` = `{eu}` ouvert ("neuf")
  + `[e]` ------- = `{é}`
  + `[E]` = `[ɛ]` = `{è}`
  + `[i]`
  + `[j]` ------- = `{i}` court ("ciel")
  + `[5]` = `[ɛ̃]` = `{in}`
  + `[o]` ------- = `{ô}` fermé ("côte")
  + `[O]` = `[ɔ]` = `{o}` ouvert ("cote")
  + `[§]` = `[ɔ̃]` = `{on}`
  + `[u]` ------- = `{ou}`
  + `[w]` ------- = `{ou}` court ("oui, moi")
  + `[y]` ------- = `{u}`
  + `[8]` = `[ɥ]` = `{u}` court ("huit, visuel")
  + `[1]` = `[œ̃]` = `{un}`
* consonnes:
  + `[b]`
  + `[d]`
  + `[f]`
  + `[g]` ------- = `{g}` dur ("gag")
  + `[k]`
  + `[l]`
  + `[m]`
  + `[n]`
  + `[N]` = `[ɲ]` = `{gn}` ("vigne"; très proche de `[nj]`)
  + `[G]` = `[ŋ]` = `{ng}` ("camping"; anglicismes)
  + `[p]`
  + `[R]` = `[ʁ]` = `{r}`
  + `[s]`
  + `[S]` = `[ʃ]` = `{ch}`
  + `[t]`
  + `[v]`
  + `[x]` ------- = jota (`{j}` espagnol, pas en français standard)
  + `[z]`
  + `[Z]` = `[ʒ]` = `{j}`





<!------------------------------------------------------------->

## Méthodologie

- **Histoire.**
  Tout ce qui existe a forcément une *cause*. Avant de
  simplifier un aspect, on peut, par curiosité, rechercher son
  origine historique. Toutefois, une cause n’est pas une
  *justification*. Ce qui nous intéresse réellement, c’est la
  pertinence et l’utilité de la chose dans la langue actuelle.
  Par exemple, la régularité des règles de grammaire, la
  facilité de reconnaître des mots, la cohérence interne des
  familles de mots.

  Ce principe s’applique entre autres à l’étymologie: pour un
  mot donné, **ce qui nous intéresse n’est pas tant son origine
  ancienne que les mots *actuels* qui lui sont apparentés**.
  Inutile donc de conserver une lettre muette étymologique si
  elle ne s’entend plus dans aucun mot de la même famille. Même
  quand des mots existent où le son peut s’entendre, c’est
  à relativiser en fonction de la fréquence des mots et de leur
  proximité de sens (ex: est-il utile de rappeler que "caillou"
  est de la même famille que "calcul" ?)

  De nombreuses irrégularités du français sont la conséquence de
  tendances divergentes qui ont été suivies selon les époques.
  Cette remarque s’applique à une grande partie du vocabulaire:
  à partir de la Renaissance, pour créer de nouveaux mots, on
  s’est amusé à repartir des racines antiques plutôt que de
  construire régulièrement sur les mots actuels, d’où les
  dérivés savants omniprésents
  (ex:
  "ostréiculture" au lieu de #"huîtriculture";
  "réfrigérateur" au lieu de #"refroidisseur";
  "sport équestre" au lieu de "sport #chevalin";
  "mécanique" au lieu de #"machinique";
  "domestique" au lieu de #"maisonnique"…).
  Il ne nous appartient pas, ici, de changer le vocabulaire;
  en revanche, on s’intéresse à régulariser l’orthographe qui
  porte, elle aussi, les stigmates de diverses modes historiques
  (ex: comparer les graphies anciennes "ressembler, télescope,
  paysanne" aux modernes "resituer, téléski, partisane").
  **Il ne devrait pas être nécessaire de connaitre la date
  d’apparition d’un mot pour savoir l’écrire.** Les mêmes
  principes orthographiques devraient s’appliquer uniformément
  à tous les mots du lexique actuel.

+ **Fidélité à la langue existante.**
  Rappelons que l’orthographe n’est pas la langue. Ici on
  s’efforce de ne pas modifier la langue actuelle, sa
  prononciation ou, pire, sa grammaire.

  - On ne veut pas supprimer brutalement des consonnes muettes
    finales si celles-ci peuvent encore s’entendre en liaison
    (cf section liaison).
  - On ne supprime pas non plus de `{e}` « muets »,
    car ceux-ci peuvent être prononcés,
    selon l’accent ou le soin de l’élocution.
  - S’attaquer à la grammaire,
    par exemple le terrible accord du participe passé,
    sort également à priori de ce cadre.
  - Ne parlons pas de rectifier des conjugaisons irrégulières,
    des mots formés de façon irrégulière, etc.

  Néanmoins on s’attaque à certains points à la lisière entre
  l’orthographe et la grammaire, autrement dit qui peuvent être
  considérés comme des phénomènes purement graphiques sans
  grande conséquence à l’oral (conjugaison des verbes en -DRE,
  -S de l’impératif, accord de "tout, cent, vingt"…).

  L’ambition de fidélité à la langue est à relativiser: on sait
  que l’écrit influence l’oral (« effet Buben »); par exemple
  une graphie trompeuse peut induire une certaine prononciation;
  des locuteurs peuvent commettre de l’hypercorrection; la façon
  dont un mot est écrit détermine la façon dont ses dérivés
  futurs seront construits.

  De plus, la plupart des réformes ne prennent pas de telles
  précautions (par exemple, la réforme de 1990 a (heureusement)
  régularisé le participe passé "absous" en "absout", et réécrit
  le mot singulier "relais" en "relai", modifiant la
  prononciation d’hypothétiques liaisons).

  (Dans le reste de ce document, on qualifie de « destructrice »
  une modification qui a un impact sur la langue même.)

+ **Plus `[plys]` de phonétique ou moins de phonétique ?**
  (cf aussi la section « Une orthographe bêtement phonétique ? »)

  Pourquoi veut-on de la phonétique ? Parce que les locuteurs
  natifs apprennent à parler bien avant d’apprendre à écrire.
  Pour eux, écrire, c’est surtout transcrire la langue orale
  qu’ils connaissent. Par conséquent, **plus l’orthographe
  s’éloigne de la prononciation, plus *écrire* devient
  difficile**.

  C’est beaucoup moins vrai dans le sens de la lecture quand
  l’objectif est de comprendre le texte : les mots lus ne sont
  pas reconnus par leur prononciation (d’ailleurs la lecture est
  en général muette) mais par leur aspect visuel. La grammaire
  est également porteuse de sens (par exemple le pluriel). Par
  conséquent, **moins les mots sont graphiquement différenciés
  et les informations grammaticales écrites, plus *comprendre un
  texte* devient difficile**.

  Donc, pour la facilité d’écrire, il est important de minimiser
  le nombre de façons différentes de transcrire un même son (ce
  qu’ailleurs dans ce document on appelle les *ambigüités
  prononciation → graphie*). Mais, comme le français comporte
  beaucoup d’homophonies et que beaucoup d’informations
  grammaticales (comme les -S du pluriel) sont muettes, ce
  besoin s’oppose à celui de faciliter la compréhension. C’est
  un **compromis** à faire ; actuellement, de par les
  irrégularités graphiques omniprésentes, le compromis penche
  lourdement, exagérément, en faveur de la compréhension. Il est
  possible d’améliorer l’écriture sans vraiment détériorer la
  compréhension (parfois même en l’améliorant, car certaines
  irrégularités obscurcissent les apparentés entre mots).

  Voilà pour la transcription prononciation → graphie. Quid du
  voyage retour ? Est-il important de minimiser le nombre de
  façons différentes de prononcer une graphie (ce qu’on appelle
  les *ambigüités graphie → prononciation*) ? C’est utile pour
  faciliter la lecture phonétique, où le but est de savoir
  prononcer un texte écrit. C’est un besoin moins courant que
  celui de comprendre un texte (et où on ne se préoccupe pas du
  tout de l’oraliser). Puisque, comme on a vu, les mots sont de
  toute façon reconnus visuellement, la lecture phonétique n’est
  guère utile que pour apprendre la prononciation d’un mot qu’on
  ne connaîtrait pas déjà. Quelques situations :

  - un locuteur natif qui découvre un mot soutenu, culturel ou
    technique (par exemple "gageure, ciguë, Œdipe, cœlacanthe")
    auquel on est rarement exposé oralement, dans l’enfance ou
    au quotidien… sauf à grandir dans un milieu bourgeois. Ce
    qui permet au mépris de classe de s’exprimer (« évidemment
    que ça se dit `[gaZyR]`, `[edip]`, tout le monde sait ça »).
  - un locuteur non-natif, qui apprend simultanément l’écrit et
    l’oral, et pour qui l’écrit sert de support
    à l’apprentissage. Un bon couplage graphie-prononciation
    facilite la mémorisation du vocabulaire.
  - (plus mineur :) un touriste non-francophone demandant son
    chemin (ou même un francophone d’ailleurs, étant donné
    l’anarchie qui règne dans les graphies des noms propres…).

  Il est donc utile de minimiser aussi les ambigüités graphie
  → prononciation. C’est toutefois moins essentiel que dans
  l’autre sens. De plus, là encore, il faut un compromis, car
  certains aspects de l’oral peuvent être considérés comme des
  détails sur lesquels il est inutile, voire nuisible, d’être
  trop précis à l’écrit (longueur des voyelles, voyelles
  alternantes, liaisons…). Comme déjà expliqué dans la section
  « Comment simplifier », il est désirable **que la graphie
  fasse abstraction de petites différences de prononciation**
  entre les époques, les régions, les locuteurs, tolère
  plusieurs prononciations pour certains mots (d’où, par
  exemple, l’introduction de l’accent plat).

  Ce document hésite donc parfois entre les deux côtés de la
  balance.

  - Faut-il supprimer tous les H aspirés,
    ou au contraire en ajouter un dans "onze" ?
  - Faut-il rendre l’adverbe "tout" graphiquement invariable,
    ou au contraire mieux marquer son euphonie au féminin ?
  - Faut-il mieux marquer le genre grammatical à l’écrit, ou au
    contraire l’effacer quand il ne s’entend pas ?
  - Faut-il mieux marquer les consonnes finales non-muettes ?
    Cela résoudrait de nombreuses homographies,
    mais souvent la prononciation est variable
    et pourrait encore évoluer dans le futur…
  - Faut-il remplacer QU par Q quand le U est muet,
    sachant que parfois les deux prononciations co-existent ?
  - etc.

+ **N-grammes.**
  Un des objectifs ici est de réduire le nombre de n-grammes,
  c’est-à-dire de successions de lettres qui doivent être considérées
  comme une entité unique pour la prononciation (« graphème »),
  par exemple `{ch, ph, oi, au, ain, ill, gu}`
  (on peut y inclure la ligature `{œ}`,
  confondue à tort avec la succession de lettres `{oe}`).

  Cela passe par
  l’unification de n-grammes qui produisent le même son,
  l’emploi de symboles uniques plutôt qu’une succession de n lettres
  (par exemple `#{ø}` au lieu de `{eu}`),
  ou encore l’emploi de lettres muettes
  (par exemple `#{fh}` au lieu de `{ph}`,
  `#{gh}` au lieu de `{gu}`;
  et même `#{ǎo}` au lieu de `{au}`,
  cf sections AU et diacritique lettre muette).

  En effet, chaque n-gramme contient un risque d’ambigüité:
  dans un mot donné, une succession de lettres
  doit-elle est interprétée comme un n-gramme ou isolément ?
  (ex: `{gu}` dans "gui, fatiguant" / `{g-u}` dans "aiguille, aigu, guano")
  Il peut même y avoir plusieurs découpages en n-grammes possibles !
  (ex: "rongeure, gageure" : `{g-eu}` ou `{ge-u}` ?)
  L’ambigüité peut en général se lever avec un mécanisme tel que le tréma
  mais actuellement ce mécanisme n’est pas utilisé de façon systématique
  (cf sections H, tréma).
  De plus, la notation au moyen de n-grammes
  privilégie la prononciation « spéciale » d’une graphie
  (ex: `{an}` prononcé `[@]`),
  par rapport à sa prononciation « normale »
  (ex: `{an}` prononcé `[an]`)
  qui nécessite d’altérer la graphie.
  C’est un problème car de nombreux mots étrangers
  sont écrits sans altération graphique
  (ex: "fan, tennisman, Einstein, leu…")
  (cf section compatibilité avec les langues étrangères).

  Quand on propose de nouveaux n-grammes,
  on essaie de suivre le principe,
  déjà existant dans `{ain, ein, œu}`,
  que la première lettre est muette (info étymologique)
  et que les lettres suivantes donnent la prononciation
  (ex: `{au}` --> `#{aô}`, cf section AU).

+ **Symboles exotiques.**
  Il est tentant d’adopter des symboles exotiques, pour une
  adéquation la plus grande possible aux besoins particuliers du
  français.

  L’inconvénient le plus évident, c’est de rendre la saisie
  informatique difficile avec les claviers actuels. Mais il faut
  noter à ce sujet que, de toute façon, les claviers AZERTY
  comme QWERTY sont nullissimes pour écrire en français (ou dans
  n’importe quelle autre langue) et qu’il est urgent d’en
  changer, là aussi.

  En choisissant des symboles qui existent dans les autres
  langues d’Europe (comme `#{ø, å, ů, č, ţ}`), non seulement on
  favorise l’intercompréhension, mais aussi on s’assure un bon
  support informatique, et en particulier, de meilleures chances
  de s’intégrer dans un hypothétique futur clavier européen, par
  exemple…





<!------------------------------------------------------------->

## État des lieux

Pour un inventaire poussé de la phonétique du français,
on peut par exemple aller voir [le Wiktionnaire][wikt-pron]
(jeter aussi un coup d’œil aux pages comme [celle-ci][wikt-pron-x]
qui détaillent les façons possibles de prononcer une lettre donnée),
ou [Wikipédia][wikip-pron],
qui évoque aussi les accents régionaux.

On peut aussi jeter un œil au site [françaisdenosrégions][fr-region]
qui rapporte de nombreuses études sur les variations régionales,
de prononciation comme de vocabulaire.

[wikt-pron]: https://fr.wiktionary.org/wiki/Annexe:Prononciation/français
[wikt-pron-x]: https://fr.wiktionary.org/wiki/Annexe:Prononciation_du_X_en_français
[wikip-pron]: https://fr.wikipedia.org/wiki/Prononciation_du_français
[fr-region]: https://francaisdenosregions.com/2019/04/14/qui-sont-ces-francophones-qui-prononcent-laccent-circonflexe/



### Principales transcriptions phonologie → graphie

(sans les graphèmes empruntés à des langues étrangères, comme `{cc}` ~ `[S]`)

* **consonnes:**
  (`{X*}` = consonne pouvant être doublée)

  + `[l]` → `{l*}`
  + `[m]` → `{m*}`
  + `[n]` → `{n*}`
  + `[N]` → `{gn, (ni)}`
  + `[R]` → `{r*}`
  + `[t]` → `{t*, th}`
  + `[d]` → `{d*}`
  + `[f]` → `{f*, ph}`
  + `[v]` → `{v}`
  + `[p]` → `{p*}`
  + `[b]` → `{b*}`
  + `[k]` → `{k*, c*, ch, q(u), x}`
  + `[g]` → `{g*, gu, x}`
  + `[S]` → `{ch, sh}`
  + `[Z]` → `{j, g, ge}`
  + `[s]` → `{s*, c, ç, <t>i, x}`
  + `[z]` → `{z, s, x}`

* **voyelles:**
  (on omet les trémas ici car ne font que dissocier des n-grammes)

  + `[a]` → `{a, o<i>}`
  + `[A]` → `{a, â}`  
     .
  + `[O]` → `{o}`
  + `[o]` → `{ô, o, (e)au}`  
     .
  + `[°]` → `{e}`
  + `[œ]` → `{e, œ, eu, œu, c<ue>il, g<ue>il}`
  + `[ø]` → `{eû, eu, œu}`  
     .
  + `[E]` → `{e, è, ê, ë, eI, aI, -et}`, `"est"` (`{I}` = `{i}` ou `{î}` ou `{y}`)
  + `[e]` → `{é, -er, -ez, -ed, (æ)}`, `"et"`  
     .
  + `[i]` → `{i, î, y}`
  + `[j]` → `{i, î, -il, ill, y}`  
     .
  + `[y]` → `{u, û}`
  + `[8]` → `{u}` (le plus souvent devant `{i}`)  
     .
  + `[u]` → `{ou, oû}`
  + `[w]` → `{ou, w, <o>i, q<u>a}`  
     .
  + `[§]` → `{oN}` (`{N}` = `{m}` ou `{n}`)
  + `[@]` → `{aN, eN, aon}`
  + `[5]` → `{eN, IN, aIN, eIN}` (`{I}` = `{i}` ou `{î}` ou `{y}`)
  + `[1]` → `{uN}`

On voit que la quasi-intégralité des sons
peuvent être écrits de plusieurs façons principales !



### Principales ambigüités graphie → phonologie

* **consonnes**:

  + `{h}` → aspiré ou non-aspiré
  + `{sh}` → `[S]` ou `[z]` (ex: "désherbant, déshériter")
  + `{ch}` → `[S]` ou `[k]` (voire `[x]`)
  + `{ph}` → `[f]` ou `[p]` ???
  + `{x}` → `[ks]` ou `[gz]` ou `[s]` ("dix") ou `[z]` ("dixième")
    - en général levé par le contexte
      mais rarement enseigné, [règles complexes et exceptions][wikt-pron-x])
  + (`{j, g}` → `[Z]` ou `[dZ]`)
  + `{w}` → `[w]` ou `[v]` (germanismes)
  + `{c}` → `[k]` ou `[s]` (levé par le contexte)
  + `{s}` → `[s]` ou `[z]`
    - en général levé par le contexte, mais des exceptions
      où `{s}` entre voyelles est prononcé `[s]`:
      p.ex. "parasol, vraisemblable, resituer, désensibiliser, dysenterie, primesautier")
  + `{g}` → `[g]` ou `[Z]`
    - en général levé par le contexte, mais exceptions dans mots d’emprunt
  + `{qu}` → `[k]` ou `[k8]` (devant certains `{i}`)
     ou `[kw]` (devant certains `{a}`)
  + `{gn}` → `[gn]` ou `[N]`
    (pas très grave, ces 2 réalisations sont comprises à l’identique, je pense)

* **voyelles**:

  + `{y}` précédé par voyelle →
    - se comporte souvent comme `{i-i}` (ex: "pays, payer, asseye, loyal")
    - mais pas toujours (ex: "bayer, kayak, mayo")
  + `{-il, ill}` →
    - `[i]` (ex: "outil")
    - `[ij]` (ex: "sillage, chenille, ouille, deuil, ail")
    - `[il]` (ex: "mille, million, ville, village, cil, civil, il, avril")
  + `{en}` →
    - muet dans la terminaison verbale `{-ent}`
    - `[@]`
    - `[5]` le plus souvent après `{i}` (ex: "bien, mien, païen")
      mais pas toujours (ex: "benjamin, pentacle, benzène, ben, Agen")
    - `[En]` parfois en fin de mot (ex: "pollen, cérumen, amen, dolmen")
  + `{an}` →
    - `[@]`
    - `[an]` dans des mots étrangers (ex: "van, fan, tennisman")
  + `{um}` →
    - `[1]`
    - `[Om]` en fin des mots latins
    - `[§]` ! (graphies vieillottes)
  + `{aon}` →
    - `[@]` dans "faon, paon(ne), taon"
    - `[a§]` dans "lycaon, machaon, pharaon"
  + `{e}` → `[°]` ou `[E]` ;
    levé par le contexte, selon ouverture de la syllabe,
    mais complications en syllabe finale,
    et exceptions dues à `{ss}`: p.ex: "dessus, ressource, ressembler…")
  + `{o}` → `[o]` ou `[O]` (pas grave) ;
    levé par le contexte en théorie,,
    mais pratique moins claire et dépend du locuteur
  + `{eu, œu}` →
    - `[ø]` ou `[œ]` (pas grave) ; levé par le contexte
    - `[y]` dans "eu, eussions", etc.
  + `{eû}` →
    - `[ø]` dans "jeûne+"
    - `[y]` dans "eût, eûmes, eûtes"

* autres ambigüités:

  + **consonnes muettes** !
  + voyelles nasales avec redoublement (ou non !) du `{N}`:
    "ennui, emmener, emmitoufler, enivrer, enamourer…"



### Consonnes doubles

Usage des consonnes doubles:

+ phonétique: `{s}` doublé en `{ss}` pour forcer `[s]`
+ phonétique: ouvrir une voyelle (cf après)
+ morphologique: marquer l’accolement d’un suffixe ou d’un préfixe
  (ex: "ap-pel, il-logique, "ir-ration-nel, in-nommable, en-nui, res-sembler")
  - en particulier: terminaison verbale (ex: "nous mour-rons ≠ nous mourons")
+ d’autres usages ?
  (ex: pourquoi "nommer, battre, bourrer, souffler, anniv, enveloppe…"
  et tant d’autres ???)

Même la lettre `{q}` se double, ce qui s’écrit… `{cqu}`
(ex: "acquitter, acquérir" = "ad- + quitter, quérir";
"grecque" = "grec + -e" avec doublement du `{q}` pour ouvrir la voyelle).
Ce qui est incompris de la plupart des gens, pas enseigné,
et est donc à supprimer en priorité.

Ces usages sont parfois en conflit, p.ex.:

- "dessus, ressort, ressource, ressembler":
  le 1er `{e}` est prononcé `[°]` et non `[E]`
- inversement: "parasol, vraisemblable, asymétrique, resituer, désensibiliser, dysenterie, primesautier":
  `{s}` est prononcé `[s]` et non `[z]`
- "rationnel": `{o}` est prononcé `[o]` (dans mon accent) et non `[O]`

De plus, l’usage pour les préfixes/suffixes est loin d’être systématique
(ex: "a-gresser, a-lourdir, é-peler, a-tonal, in-espéré, en-ivrer, re-situer")…
et incohérent même au sein des familles étymologiques ou champs lexicaux
(ex: "
alléger/alourdir, abaisser/affaisser, abréviation/addition,
aggraver/agresser, apercevoir/apparaître, apaiser/apparier,
allonger/élonger, appeler/épeler, accueil/écueil, effrayer/réfréner,
difforme/déformer, offense/défense, ressemblant/vraisemblable, ressaut/soubresaut,
chariot/charrette, salle/salon, chope/échoppe, chape/échapper, trappe/attraper,
donner/donation, résonner/résonance, concourir/concurrent, récurer/récurrent,
siffler/persifler, demander/commander, homme/hominidé (alors que pomme/pommier),
honneur/honorer, national/rationnel/rationaliser, allitération/littérature…").

L’analogie graphique entre sons similaires n’est pas davantage respectée
(ex: "
cape,chape,trapu / nappe,trappe;
attraper / happer;
clope,salope,cyclope,héliotrope / enveloppe,échoppe;
civil(e) / tranquille;
…").

Une tendance générale est que le préfixe latin _a(d)-_,
exprimant… pas grand-chose de clair (direction, destination),
provoque le doublement de la consonne qui suit
(ex: "addition, aggraver, alléger, approcher, apprendre, attribut, raccourci");
ce qui, incidemment, le distingue du préfixe grec _a(n)-_
exprimant la privation, pour lequel on ne double pas la consonne
(ex: "apériodique, amoral, asymptomatique, anaérobie").
Mais comme on voit, il y a plein d’exceptions non justifiables
(ex: devant `{b}`; devant `{g}` sauf "aggraver";
dans "alourdir, alunir"; devant `{m}`;
dans "apaiser, apercevoir, apeurer, apitoyer, aplanir, aplatir,
aposter, apostropher, apurer";
la plupart de ces mots s’écrivaient avec une consonne double
par le passé).
De plus, le préfixe _a(d)-_ est essentiellement mort,
et n’éclaire guère le sens des mots actuels.

Les consonnes doubles sont une des difficultés majeures de
l’orthographe française, puisqu’elles n’ont pas de réalité orale
et que leur usage ou non-usage est incohérent, donc imprévisible
(dico = seule solution).



### Ouvert / fermé

Une syllabe est dite *fermée* si une consonne la termine, *ouverte* sinon.
Ceci détermine si la voyelle doit être prononcée ouverte (en consonne fermée)
ou fermée (en consonne ouverte);
trois voyelles graphiques sont concernées,
auxquelles il faut ajouter une paire graphique:

+ `{o}` ~ `[o / O]` (voyelle fermée en syllabe ouverte / voyelle ouverte en syllabe fermée)
  - `{ô}` force la 1re prononciation
+ `{eu, œu}` ~ `[ø / œ]`
  - `{eû}` force la 1re prononciation (cas unique de "jeûne")
+ `{e}` ~ `[° / E]`
  - `{è}` force la 2e prononciation
+ `{é / è}` ~ `[e / E]`
  - chaque graphie force une prononciation
  - occurrences de ces graphies:
    * `{é}`: toujours en syllabe ouverte
    * `{è}`: en syllabe fermée ou en syllabe finale ouverte ("dès, près, après, grès…")
      ou dans de rares cas, en syllabe non finale ouverte,
      avant un schwa non élidable (ex: "mièvrerie, chèvrefeuille, lèveriez")

Complications: statut ouvert/fermé pas toujours évident d’après la graphie,
pour plusieurs raisons:

+ consonnes muettes finales:
  la syllabe semble graphiquement fermée
  mais phonologiquement ouverte;
+ séquences de plusieurs consonnes:
  il arrive qu’un radical terminé par une voyelle
  soit accolé à un radical commençant par plusieurs consonnes,
  alors la voyelle est fermée
  mais la graphie suggère à tort qu’elle serait ouverte
  (ex: "autostop, aérostat, aérospatiale, stéthoscope, introspection");
  parfois une consonne est doublée exprès pour signaler un accolement (!)
  (ex: "commande");
  - le problème est particulièrement gênant pour `{e}`
    (ex: "restaurer, restituer, restreindre / restatuer, resituer, restructurer");
+ la situation contraire existe aussi:
  `{e}` prononcé `[E]` alors qu’il est suivi par une seule voyelle
  (ex: "poterie, papetier / interaction, papeterie")
+ séquences de plusieurs consonnes (bis):
  les règles typographiques de césure
  (codifiées par l’imprimerie, enseignées à l’école)
  disent de toujours couper entre 2 consonnes,
  ce qui est parfois contraire aux syllabes phonologiques
  (ex: "meublé" se découpe graphiquement en "meub-lé"
  mais se prononce [mø.ble]);
+ consonnes doubles,
  qui sont un artifice graphique et non une réalité phonologique:
  non seulement la prononciation de la consonne n’est pas prolongée,
  mais en plus, après `{o}` et `{eu}`,
  les consonnes doubles semblent rarement voire jamais influencer
  la prononciation en pratique de la voyelle,
  davantage déterminée par les syllabes phonologiques ou l’accent du locuteur
  - après `{o}`: (ex: "pommier, rationnel, résonner, occasion…"
    p.ex. "pommier" se découpe graphiquement "pom-mier"
    mais se prononce [po.mje], pas [pOm.mje];
    cf aussi les nombreux contre-exemples plus loin);
  - après `{eu}`: les rares instances de `{eu}` suivi de consonnes doubles sont
    "beurre/beurrer, leurre/leurrer";
    dont la prononciation suit l’alternance ouvert/fermé
    indépendamment des consonnes doubles;
  - après `{e}`, les consonnes doubles ouvrent effectivement la voyelle,
    en général
    (cf plus loin);
+ schwas non prononcés:
  dans ce cas, la syllabe précédant le schwa peut passer d’ouverte à fermée
  (car reçoit la consonne du schwa)…
  ce qui modifie *ou non (!)* la prononciation de la voyelle
  (ex: "rose", dont la graphie suggère 2 syllabes ouvertes "ro-se",
  peut en effet se prononcer [Ro.z°] en 2 syllabes ouvertes,
  mais plus souvent [Roz] ou [ROz] en 1 syllabe fermée;
  "heureuse" se prononce [ø.Røz];
  "événement" se prononce [e.ve.n°.m@], [e.ven.m@] ou [e.vEn.m@]);
+ complications supplémentaires pour `{e}`:
  - prononcé `[E]` dans la terminaison `{-et}`
    et dans les mots monosyllabiques terminés par un `{-s}` muet
    ("es, des, les, mes, tes, ses, ces");
  - prononcé `[°]` avant `{ss}` dans certains mots
    (ex: "dessus, dessous, ressource, ressac, ressembler, ressemeler")

Plutôt que d’ajouter une diacritique sur la voyelle,
un artifice graphique fréquent pour forcer la prononciation ouverte
d’une voyelle (en général `{e}`)
dans une syllabe qui *semblerait graphiquement* ouverte
consiste à doubler la consonne qui suit;
ainsi, la syllabe *semble graphiquement* fermée
(ex: "appeler → appelle, atteler → attelle, lunetier → lunette, bel → belle,
net → nette, chien → chienne, trot → trotte, reddition").
Là encore ce n’est qu’une apparence graphique,
la consonne n’est pas prolongée !
(ex: les syllabes phonologiques dans "appelle" sont `[a.pEl]` ou `[a.pE.l°]`,
mais pas `[a.pEl.l°]`).
C’est une bidouille qui date d’avant l’invention de l’accent grave,
la réforme de 1990 favorise l’écriture d’un accent grave
plutôt que d’une consonne double
(ex: "peler → pèle", mais toujours "appeler → appelle").

De plus, en pratique,
l’alternance n’est pas aussi nettement marquée/respectée
que le dit la règle, et dépend des locuteurs.

+ très irrégulier pour `{o}`,
  qui est souvent fermée en syllabe fermée, selon le locuteur; p.ex:
  - "rose, grosse" `[O]` au Sud (conforme à la phonologie) mais `[o]` au Nord
  - "diplôme" `[O]` au Sud (malgré la graphie circonflexe), `[o]` au Nord
  - "axiome, polygone" `[o]` (comme "binôme")
  - "économe" `[O]`
  - "pomme, gomme, je nomme, bosse" `[O]` → "pommier, gommer, nommer, bossu" `[o]`
    (conforme à la phonologie mais contraire à la graphie)
  - "fosse" `[o]` dans mon accent → "fossé" `[o]` (idem)
  - "rationnel, occasionnel" `[o]` (idem)
  - => dans mon propre accent,
    il semble que `[s]` ou `[z]` ne suffit pas à garder `[O]` ouvert:
    "rose, grosse, chose, dose, pose, fosse, dossier, postal, hospitalier…" `[o]`
    (quelques exceptions: "bosse, brosse, rosse, cosse, adosse, noce…" `[O]`)
  - "coté" `[O]` (malgré la syllabe ouverte) !
+ pas complètement clair pour `{eu}` non plus:
  - `{-eur}` ~ `[œR]` (conforme au principe général)
  - `{-eure}` ~ `[œR]` (idem en considérant que le schwa est muet)
  - `{-euse}` ~ `[øz]` (contraire au principe général)
+ même `{è}`, qui en principe force la prononciation `[E]`,
  est plus ou moins fermé `[e]` dans de nombreux mots selon les locuteurs,
  la distinction n’est en fait pas très nette.
  - par exemple je prononce "événement" [e.ven.m@],
    sa réécriture en "évènement" en 1990
    n’est donc pas pour moi une correction, au contraire
+ par contre, pour `{e}` qui produit `[° / E]`,
  la distinction entre les 2 sons est par contre très nette
  et semble faite à l’identique par tout le monde;
  - mais là encore, certains `[E]` sont plutôt prononcés `[e]`
    (ex: "effet, essai, dessert").

Préciser le caractère ouvert/fermé a 2 inconvénients :

+ forcer une prononciation parmi plusieurs,
  contredire celle de certains locuteurs;
+ rendre nécessaire des changements orthographiques au cours du temps
  (des `{é}` devenus `{è}` en 1835, 1878, 1990).





<!------------------------------------------------------------->

## Idées de changements

Objectifs:

+ réduire le nombre de façons possibles d’écrire un même son;
+ réduire le nombre de façons possibles de prononcer une même graphie;
+ régulariser aussi la façon de noter les informations autres que la phonologie
  (marque du pluriel, étymologie, relations entre mots…);
  - autrement dit, réduire le nombre d’exceptions,
    et même de règles grammaticales;
+ … sans perdre les informations étymologiques *utiles*.

**Non**-objectifs:

+ orthographe complètement phonétique;
+ orthographe la plus courte possible;
+ orthographe d’apparat maximisant le nombre de symboles exotiques;
+ modifier la langue elle-même,
  c’est-à-dire opérer des changements qui seraient contraires à l’oral.

Ci-dessous, un ensemble d’idées en vrac
(pas triées par importance pratique, utilité, faisabilité…),
et pas toujours compatibles entre elles.
Certaines modifs ne changent pratiquement pas
l’aspect écrit (physionomie) de la langue,
d’autres la changent radicalement.
Pour certaines modifs,
la lecture des nouvelles graphie
ne nécessite pas de nouvel apprentissage.
Pour d’autres il faut un apprentissage
(nouveaux digrammes, nouveaux symboles…).



### Un nouveau système d’écriture ?

Il faut *évidemment* se poser la question d’abandonner l’alphabet latin
et d’adopter un tout nouveau système d’écriture, adapté au français.
C’est *évidemment* extrêmement déraisonnable,
bien que Domergue l’ait proposé en 1805.
Posons-nous tout de même la question :
à quoi ressemblerait un système d’écriture adapté au français ?

+ Un syllabaire pur serait clairement inadapté :
  les consonnes et plus encore les voyelles
  ont une fâcheuse tendance à muter en français,
  au cours du temps ou entre mots apparentés.
  Se pose également la question des nombreuses
  informations muettes (étymologie, grammaire).

+ Un abjad (comme l’arabe, on ne note que les consonnes) ?
  Non, horrible !

+ Un **alphabet**,
  à savoir un système où on note chaque son par un signe ?
  Il faut souligner
  (cf les annexes des _Délires de l’orthographe_ de Nina Catach),
  que l’écriture actuelle du français
  est effectivement, plus ou moins, un *alphabet*,
  mais que *l’alphabet véritable du français n’est pas l’alphabet latin*
  (même étendu) :
  ce qu’il faut regarder, ce sont les *graphèmes*.
  Par exemple, `{eu, ai, oi, ou, ch, qu, gn}` sont des graphèmes.
  Ce sont eux qu’il faut considérer comme les lettres de notre alphabet.
  Le premier problème est qu’on transcrit ces graphèmes par des successions
  de plusieurs symboles qui peuvent aussi s’interpréter comme des lettres isolées.
  Dit en termes informatiques :
  on a un codage ambigu de l’alphabet français dans l’alphabet latin.
  Une première amélioration serait donc d’éliminer l’intermédiaire latin,
  et d’employer des symboles qui transcriraient directement
  les « lettres » de l’alphabet français
  (raison pour laquelle on propose par exemple, plus loin,
  le symbole `#{ø}` au lieu de `{eu}`,
  ou encore le e-accent-plat à distinguer du schwa,
  lettre typiquement française).
  Restent deux autres problèmes :
  le nombre de graphèmes produisant le même son,
  et l’omniprésence de lettres muettes.
  Dans ce document,
  on s’efforce de réduire le nombre de façons d’écrire un même son,
  et on fait… ce qu’on peut pour les lettres muettes.

+ Un **système logographique** (comme le chinois)
  serait peut-être adapté…
  Ça déplacerait complètement la difficulté d’écrire le français,
  depuis une écriture approximativement phonétique très tordue,
  vers une écriture très découplée de la langue orale.
  En privilégiant le sens sur la prononciation,
  on pourrait faire abstraction d’un grand nombre d’irrégularités
  (par exemple, on peut imaginer qu’une construction graphique régulière
  permette de passer de "cheval" à "cavalier"
  alors que la prononciation de ces mots n’a rien à voir,
  et qu’au contraire "chevalier" soit une graphie à part).
  Mais on entre alors dans un tout autre domaine,
  auquel je ne connais rien !

Finalement, je me demande si un système adapté au français
ne serait pas une **écriture mixte** qui superposerait
des informations de formation du mot, seules porteuses de sens
(par exemple: "[cheval]-[personne en rapport avec]-[pluriel]"),
et des précisions de prononciation
quand celle-ci ne s’en déduit pas de façon régulière.
Dans une telle écriture,
les deux sous-système utiliseraient
deux ensembles de signes entièrement séparés.
Car c’est, je pense, la source de nos problèmes avec l’alphabet latin :
le besoin de rendre les racines apparentes est en conflit permanent
avec celui d’indiquer la prononciation,
puisqu’on utilise les mêmes symboles pour les deux,
et que la prononciation d’une racine change au cours du temps.

[clé-sinogramme]: https://fr.wikipedia.org/wiki/Clé_d'un_sinogramme
[hanja]: https://fr.wikipedia.org/wiki/Hanja

Autre idée
(inspirée des [radicaux des sinogrammes][clé-sinogramme]
et des [hanjas][hanja] du coréen) :
un système primaire **essentiellement phonologique**,
complété par un ensemble restreint d’idéogrammes très génériques
qui ne serviraient qu’à distinguer des homophones ;
chaque mot, écrit selon sa prononciation
(sauf éventuellement consonnes muettes pertinentes ?),
pourrait *optionnellement* être **précisé par un idéogramme**
(la « clé » du mot).
Exemples :

- "la forêt, le foret" --> #"forè[nature], forè[outil]"
- "le faux, la faux, il faut, le pho"
  --> #"fo[idée], fo[outil], fo[verbe], fo[nourriture]"
- "la peau, le pot" --> #"po[corps], po[outil]"
- "sot, seau, sceau" --> #"so[personne], so[outil], so[écriture]"
- "au, eau, haut" --> #"o[grammaire/lieu], o[matériau], o[idée]"
- "où, ou, houx, houe" --> "ou[lieu], ou[grammaire], ou[nature], ou[outil]"
- "toi, toit" --> #"toi[personne], toi[lieu]"
- "hauteur, auteur" --> #"oteur[idée], oteur[personne]"
- "ver, verre, vert, vers, vers"
  --> #"ver[nature], ver[matériau], ver[couleur], ver[écriture], ver[lieu]"
- "pin, pain, peint" --> #"pin[nature], pin[nourriture], pin[verbe]"
- "occident, oxydant" --> #"oksidan[lieu], oksidan[matériau]"
- "Terre, terre" --> #"ter[espace/lieu], ter[lieu], terre[matériau]"

En concevant judicieusement l’ensemble d’idéogrammes,
trouver la clé d’un mot serait le plus souvent naturel ;
de plus, on tolérerait plusieurs clés différentes pour un même mot.

Avantages :
résout le problème de la prononciation et celui des homophones ;
facilite grandement la recherche dans un dictionnaire,
par prononciation puis clé.
Inconvénient :
occulte la formation et les apparentés des mots.

Notons que ce système n’a rien de spécifique au français
et se transpose aisément aux autres langues.



### QU

question épineuse:
redondance entre les lettres `{c, q, k}`:
cf section Q/C.

rationalisation en attendant:
`{q}` est systématiquement suivi de `{u}` presque systématiquement muet…
qui est donc inutile, on peut l’enlever.

> **très facile:**
> + `{qu}` ~ `[k]` --> `#{q}`
> + `{qui}` ~ `[kwi]` --> `{qui}` (ex: "équidistant")
> + `{qua}` ~ `[kwa]` --> `#{qwa}` (ex: "aquarium, équateur")

avantages:

+ raccourcit de nombreux mots en leur ôtant des lettres
  complètement inutiles
+ "cinq, coq, piqure" ne sont plus des bizarreries graphiques
+ désambigüe les quelques cas où le `{u}` est prononcé, devant `{i}` ou `{a}`
  (aussi faisable avec un tréma: `#{qüi}`, `#{qüa}`)

dans de rares cas, les deux prononciations (`{u}` muet ou non) co-existent.
avec la modification proposée, ces mots auraient donc 2 orthographes.

note culturelle (histoire de Q, K, C):

 1. l’alphabet phénicien,
    un des premiers alphabets de l’Histoire,
    apparu au 11e siècle avant JC,
    possédait deux consonnes notant chacune un son différent:
    + _kaf_ (𐤊, à l’origine de notre K)
      notait le son `[k]` qui existe aussi en français;
    + _qof_ (𐤒, à l’origine de notre Q)
      notait **le son noté `[q]`** dans l’alphabet phonétique international,
      qui est inconnu en français et qui ressemble à `[k]`.
 1. le grec archaïque,
    au 8e siècle avant JC,
    a emprunté ces 2 lettres au phénicien
    pour former ses propres lettres,
    avec le même usage:
    _kappa_ (Κ, notant le son `[k]`)
    et [_qoppa_][wikip-koppa] (Ϙ, notant une variante « postérieure » de `[k]`,
    un son à mi-chemin entre `[k]` et `[q]`).
    sauf qu’en grec, contrairement au phénicien,
    la distinction entre ces deux sons n’était pas pertinente:
    il s’agissait de
    **[variantes contextuelles][wikip-variante-contextuelle] allophones**,
    c’est-à-dire qu’ils étaient compris comme un même « phonème »
    et que le son à prononcer était déterminé automatiquement
    selon le contexte phonétique, pour des raisons d’articulation:
    _qoppa_ devant `[o]` ou `[u]`,
    _kappa_ devant une autre voyelle
    (de même qu’en français, inconsciemment,
    on articule différemment le `{c}` de "coup" et le `{qu}` de "qui",
    le premier a une articulation plus « postérieure »,
    plus proche de `[q]` !).
    on ne pouvait écrire _qoppa_ que devant `[o]` ou `[u]`,
    et on ne pouvait pas écrire _kappa_ dans de tels contextes.
    ces sons n’étant pas distingués à l’oral,
    il était inutile et même nuisible de les distinguer à l’écrit,
    et le grec a donc abandonné la lettre _qoppa_ au profit de _kappa_
    dès le 5e siècle avant JC.
 1. cependant l’étrusque
    a emprunté ces 2 lettres au grec,
    à l’époque où _qoppa_ existait encore,
    vers le 7e siècle avant JC.
    or la situation était exactement la même qu’en grec:
    il s’agissait de 2 variantes contextuelles allophones:
    Q devant U,
    K ailleurs.
    pour empirer les choses,
    l’étrusque avait aussi la lettre C prononcée `[k]`,
    descendante de la lettre grecque _gamma_ (Γ, qui en grec se prononçait `[g]`,
    phonème inconnu des étrusques),
    il y avait donc trois lettres Q, K, C pour noter le même phonème !
    comme en grec, la distinction était inutile
    et l’étrusque a donc remplacé ces 3 lettres par une seule, le C,
    au 5e siècle avant JC.
 1. cependant le latin
    a emprunté ces 3 lettres à l’étrusque
    à l’époque où elles existaient encore.
    or les Romains ignoraient complètement le son `[q]`
    et, par contre, ils distinguaient les 2 phonèmes `[k]` et `[g]`;
    en latin archaïque,
    chacune de ces 3 lettres pouvait produire chacun de ces 2 phonèmes !
    comme en grec et en étrusque,
    le choix de la lettre à écrire était automatique:
    Q devant O ou U,
    K devant A,
    C devant E ou I.
    plus tard, le latin s’est partiellement débarrassé de cette redondance,
    en écrivant C partout
    *sauf* Q devant une lettre U quand celle-ci se prononçait `[w]`
    (semi-consonne précédant une voyelle).
    il y avait donc
    C prononcé `[k]`,
    QU prononcé `[kw]`,
    et aucun autre Q,
    ni aucun K.
    les Romains percevaient le `[kw]` comme un phonème à part entière de leur langue,
    mais il n’y avait pas réellement de raison de l’écrire QU plutôt que CU;
    ils auraient pu se débarrasser entièrement de la lettre Q.
 1. aux époques ultérieures, K est réapparu en latin
    pour noter les emprunts à d’autres langues.
 1. lors de l’évolution du latin vers le français,
    le phonème `[kw]` est devenu `[k]`,
    autrement dit le U est devenu muet (sauf exceptions).
 1. **bilan:**
    la lettre Q
    nous vient de l’écriture d’une langue morte il y a 1600 ans (5e siècle après JC)
    et qui n’est pas du tout apparentée ni au français ni au latin, ni au grec
    (le phénicien, langue sémitique, apparentée à l’hébreu et à l’arabe).
    elle servait à noter un son propre à cette langue
    et n’avait plus de pertinence dans la suite de cette petite histoire.
    elle s’est transmise jusqu’à nous
    à travers trois autres alphabets,
    de trois autres langues mortes
    (le grec ancien mort au 6e siècle après JC,
    l’étrusque mort au 2e siècle avant JC,
    le latin supplanté par le gallo-roman au 9e siècle après JC)
    dont aucune n’en avait besoin,
    et qui toutes ont fini par s’en débarrasser
    — incomplètement, dans le cas du latin.
    <!-- -->
    la perpétuation du Q
    n’est peut-être qu’une répétition de malentendus,
    un téléphone arabe,
    un grand quiproquo sur trois millénaires…
    <!-- -->
    il est d’ailleurs amusant de remarquer que
    ce jeu de chaises musicales a conduit à ce que
    notre usage moderne de QU comme variante « dure » de C devant E/I,
    soit exactement l’inverse de
    l’usage initial de Q, qui ne s’écrivait que devant O/U !
    <!-- -->
    bref, en orthographe française, il semble *tout à fait* inutile
    de distinguer `{q}` de `{c}` pour des raisons étymologiques;
    qu’attend-on pour se débarrasser du Q ?

[wikip-koppa]: https://fr.wikipedia.org/wiki/Koppa
[wikip-variante-contextuelle]: https://fr.wikipedia.org/wiki/Variante_contextuelle

note:
`{q}` sans `{u}` existe déjà dans des transcriptions de l’arabe (entre autres),
où le son d’origine est `[q]` plutôt que `[k]` (ex: "Iraq, Qatar").



### CUEIL, GUEIL

bidouille vraiment bizarre où,
dans la succession `{c + eu + il}`, et seulement dans ce cas,
on échange les deux lettres de `{eu}`
pour que `{c}` se prononce `[k]` plutôt que `[s]`.
résultat, une graphie qui devrait logiquement se lire `[k8Ej]`,
mais qui est en fait une façon additionnelle d’écrire des sons
qu’on peut déjà écrire autrement.
facile de s’en débarrasser, il y a une façon systématique pour ça:

> **très facile:**
> + `{cueil}` --> `#{queuil}` (puis modifs relatives à Q)
> + `{gueil}` --> `#{gueuil}` (puis modifs relatives à G)

"œil" est une singularité similaire,
il manque le `{u}` !
estime-t-on que son rôle est tenu
par le `{o}` qui se trouve avant le `{e}` ?
il n’y a aucune raison à cette singularité.
il y a bien un `{u}` dans le pluriel "yeux",
ainsi que dans "œuf, cœur…"

> **très facile:**
> + "œil" --> #"œuil"

(cf aussi section EU, section Œ)



### GU

question épineuse:
redondance entre `{g}` et `{j}`,
faut-il garder l’alternance de sons de la lettre `{g}`:
cf section G/J.

amélioration claire en attendant:
remplacer le `{u}` muet par un `{h}` (comme en italien),
ainsi on ne se demande plus si `{u}` est prononcé
dans des mots comme "fatiguant, baguage, guano…".
cohérent avec l’usage de `{h}` comme lettre muette
qui bloque les combinaisons de lettres.

> **très facile:**
> + `{gu}` ~ `[g]` --> `#{gh}`

Particularités rigolotes/horribles:
- "fatigant" (adjectif) ≠ "fatiguant" (participe présent),
  ces deux mots ont même des sens assez contraires…
- "bagage" (valise) ≠ "baguage" (action de baguer)
- …



### C dur, CH ?

actuellement, pour forcer la prononciation `[k]` de la lettre `{c}`,
il y a 3 techniques:

+ général: remplacer `{c}` par `{qu}`
  (ex: "caudal → queue, embarcation → embarquer")
+ spécifique: `{cueil, gueil}` (cf section CUEIL)
+ très spécifique: "cœur" (cf sections EU et Œ)

en attendant de s’attaquer à la question épineuse des lettres `{c, q, k}`
(cf section Q/C),
il serait utile de pouvoir forcer la prononciation `[k]` de la lettre `{c}`
de façon systématique.
écrire `{q}` au lieu de `{c}` est la solution la plus simple (cf section QU).

si on veut conserver la lettre `{c}` apparente, il faut autre chose.
on ne peut pas appliquer directement le principe d’ajouter un `{h}`,
car il existe le digramme `{ch}` qui produit le son `[S]`.
On pourrait à la rigueur réécrire ledit digramme autrement,
par exemple en `#{č}` (comme dans de nombreuses langues d’Europe)
ou `#{ĉ}` (ressemblant à l’espéranto).
Ainsi on se rapprocherait du principe que la lettre `{h}` est toujours muette,
et on libérerait la graphie `{ch}`…
mais cette nouvelle lettre `#{ĉ}` n’est pas facile à saisir au clavier
(quoique ? les claviers AZERTY possèdent déjà la lettre C
et la touche morte circonflexe, donc c’est parfaitement possible…),
c’est une modif lourde
et, surtout, il semble une mauvaise idée de ré-utiliser
un digramme qui existait précédemment avec une autre signification.

> bof:
> + `{ch}` ~ `[S]` --> `#{č}` ou `#{ĉ}`
> + `{sh}` ~ `[S]` --> `#{š}` ou `#{ŝ}`

notons aussi [l’idée][t-cédille]
d’écrire `#{çh}` pour le son `[S]`,
par opposition à `{ch}` pour forcer le son `[k]`
(ex: "#monarche, #monarçhie, #roch, #roçher").



### SH

Le digramme `{sh}` transcrit le son `[S]`
dans beaucoup de langues étrangères,
auxquelles, dans les faits, on emprunte fréquemment
(cf aussi section compatibilité langues étrangères).
Bien que d’origine barbare,
Il ne semble pas plus raisonnable de s’y attaquer
que de s’attaquer à `{ch}` prononcé `[S]`.
Problème: on trouve aussi cette succession de lettres de façon fortuite,
dans des mots issus de la concaténation d’une composante en `{-s}`
et d’une composante en `{h-}`
(ex: "bonshommes, désherbant, déshonneur, déshabiller,
déshériter, déshydrater, transhumanisme"),
et alors il se prononce `[s / z]` (le `{h}` est muet).

Il faut commencer par voir si on maintient le `{h}` dans chacun de ces mots
(cf section H).

Si on maintient le `{h}`,
la solution reste facile:

> **facile:**
> + `{sh}` ~ `[z]` --> `{zh}`

Ça ne résout pas le cas où `{sh}` est prononcé `[s]`,
mais ce cas est heureusement beaucoup plus rare
(rares exemples: "transhumanisme?, transocéanique?, transhistorique").

On pourrait aussi ré-insérer un tiret,
ou employer une généralisation du tréma
(ex: #"bons:hommes, trans:historique")
(cf section tréma).

Pour que ce soit cohérent avec l’usage de `{s}` ailleurs
(ex: autres occurrences du préfixe `{dés-}`, le mot pluriel "bons"…)
il faut s’attaquer plus généralement à l’alternance S/Z
(cf section S/Z).



### Lettres grecques (CH, PH, RH, TH, Y)

causent des ambigüités de prononciation:

+ `{ch}` ~ `[S]` ou `[k]`
+ `{ph}` ~ `[p]` ou `[k]`
  (je ne sais pas si `[p]` est avéré, mais c’est théoriquement possible avec
  des préfixations, tout comme `{sh}` se prononce `[z]` dans "désherbant")

question polémique:
conserver ou non cette info étymologique partielle,
pas donnée de façon systématique (pourquoi seulement ces lettres-ci ?
pourquoi n’écrit-on pas, ou plus, "rhythme, aphthe, ophthalmologie, orthogrhaphe…" ?)
ni cohérente (ex: "colère / choléra"),
et parfois même de façon erronée (ex: "style, sylvestre, nénuphar, algorithme").
Sachant en plus que de nombreux mots estampillés grecs
sont au moins autant latins que grecs,
ayant été empruntés au latin qui les avait empruntés au grec
(ex: "lacrymal" du latin "_lacrima_", du grec "_dákruma_":
i latin ou y grec ?
"compatir" du latin "_patior_",
apparenté au grec "_pathos_"
dont est issu "sympathie":
`{t}` latin ou `{th}` grec ?
"orée" du latin "_ora_", du grec "_hóros_"
dont est aussi issu "horizon":
`{h}` muet ou pas de `{h}` ?
"dauphin, phare…").

En 1905 l’Académie elle-même avait résolu de
se débarrasser au moins de la lettre grecque `{rh}`
p.ex. dans "rhume".

rationalisation facile en attendant:
supprimer l’ambigüité de prononciation par les réécritures suivantes:

> **très facile:**
> + `{ch}` ~ `[k]` --> `#{kh}`
> + `{ph}` ~ `[f]` --> `#{fh}`

(ne pose guère d’ambigüités étymologiques
car `{kh}` n’apparaît que dans quelques translittérations de l’arabe,
et `{fh}` pas du tout.)

réécrire `{ch}` en `{kh}` est cohérent avec la prononciation grecque d’origine
(`k` aspiré) et avec les notations internationales actuelles, où ce digramme
note souvent le son `[x]`, y compris en translittération du grec moderne.

on pourrait même se débarrasser du `{h}`
en marquant plutôt les lettres grecques d’une diacritique «lettre grecque»,
ce qui permettrait aussi de traiter `{y}`;
ainsi on libère les lettres `{h}` et `{y}`,
et les ambigüités phono → graphie deviennent une simple question de diacritique
(`[f]` s’écrirait `{f}` ou `#{f^}`, `[i]` s’écrirait `{i}` ou `#{i^}`…).

> **envisageable:**
> + `{ch}` ~ `[k]` --> `#{k^}`
> + `{ph}` ~ `[f]` --> `#{f^}`
> + `{rh}` --> `#{r^}`
> + `{th}` --> `#{t^}`
> + `{y}` grec --> `#{î}`

inconvénient: diacritique supplémentaire, difficile à saisir au clavier

(le choix du circonflexe est vaguement cohérent avec son usage actuel
de signaler une lettre disparue,
et ne crée aucun conflit une fois éliminés les circonflexes existants sur `{i}`,
cf réforme de 1990.
d’ailleurs, ne le cachons pas, le but à long terme est que
cette diacritique soit elle-même jugée superflue et abandonnée,
comme c’est le cas du circonflexe actuel…)



### H

usages de `{h}`:

+ phonétique: ne produit pas de son en soi mais marque une absence de liaison,
  découpe les syllabes
  - `{h}` aspiré en début de mot (ex: "hauteur (altitude), hérisson, haricot…")
  - `{h}` en milieu de mot (ex: "ahaner, brouhaha, hi-han");
    redondant avec tréma (cf après)
+ étymologique: `{h}` non-aspiré en début de mot/radical
  (ex: "homme, huis, hydravion, chlorhydrique")
+ bidouille graphique obsolète depuis l’invention de la lettre `{u}` au Moyen-Âge
  (ex: "huile, huitre, huis, huit")
  - (dans "huit" le H est devenu aspiré, il faut donc le conserver)
+ phonétique: digrammes `{ch}` et `{sh}` (cf sections CH et SH)
+ étymologique: digrammes lettres grecques (cf section lettres grecques)

=> usages nombreux et contradictoires (`{h}` initial aspiré ou non, `{ch}`).
dans l’idéal, on aimerait réduire `{h}` à un seul usage:
lettre séparatrice et toujours muette.

`{h}` aspirés en début de mot:
à garder ? (cf section H aspiré, plus loin)
(ou à remplacer par une généralisation du tréma ?
du style "hauteur" --> #":auteur"
cf section tréma)

`{h}` séparateurs de voyelles en milieu de mot:
certains pourraient devenir superflus si on généralise le tréma,
ou si on définit une phonétique plus claire et moins équivoque
pour les suites de voyelles
(ex: #"brouaa" se lirait sans équivoque [bRu.a.a]).

`{h}` non-aspirés étymologiques: voir si on veut les garder…
(cf aussi section diacritique lettre muette)

#### huile

dans le cas de la bidouille graphique,
supprimer le `{h}` qui est non seulement inutile mais carrément nuisible
car il brouille l’étymologie et donc les apparentés
("huile → oléagineux, olive, pétrole, terminaison chimique -ol,
italien _olio_, portugais _óleo_, anglais _oil_, allemand _Öl_;
huitre → ostréicole,
italien _ostrica_, espagnol _ostra_, anglais _oyster_, allemand _Auster_;
huis → italien _uscio_, espagnol _uzo_).

> **très facile:**
> + "huile" --> #"uile"
> + "huitre" --> #"uitre"
> + "huis, huissier" --> #"uis, uissier"

note culturelle:
quelle est cette «bidouille graphique» dans "huile, huitre, huis" ?
l’alphabet latin était, pour la langue latine,
un *alphabet* au sens véritable,
c’est-à-dire un système qui
transcrit chaque son (voyelle ou consonne) par exactement un signe.
À partir du 1er siècle, il y avait deux exceptions:
la même lettre V transcrivait 2 sons différents (`[u]` et `[v]`, ce dernier ayant évolué depuis le son `[w]`);
la même lettre I transcrivait 2 sons différents (`[i]` et `[j]` puis `[Z]`).
ce n’était pas un problème pour le latin
car le contexte suffisait à déduire quel son était produit:
par exemple,
V  faisait forcément `[u]` s’il était entouré de consonnes,
mais `[v]` en début de mot si suivi par une voyelle
(pour une illustration, voir la citation de Clément Marot
dans la section accord du participe passé).
en français, qui a par ailleurs gagné beaucoup de sons par rapport au latin,
ça marchait de moins en moins…
mais pendant des siècles on a continué (et on continue !)
d’utiliser ce vieil ensemble de signes
de moins en moins adapté à cette nouvelle langue.
d’où le recours massif à des digrammes, trigrammes…
et l’emploi d’une lettre spéciale, le H, à toutes les sauces.
en l’occurrence,
à l’époque où l’alphabet n’avait pas encore distingué deux lettres U et V,
on a inséré un H au début de "huile, huitre, huis"
pour les distinguer de "uile, uitre, uis" ("vile, vitre, vis"),
ce qui semblait malin
car la consonne H suggérait de prononcer le U/V comme une voyelle.
cette lettre non étymologique ne se retrouve pas dans les dérivés
("oléagineux, ostréicole")
et obscurcit donc les apparentés étymologiques.
cela fait maintenant de nombreux siècles
qu’on a eu l’idée brillante de distinguer U de V dans l’alphabet,
la bidouille est donc largement obsolète !

#### H aspiré

Plus haut on a dit qu’il était désirable de garder les H aspirés,
pour refléter la prononciation.
On peut en fait réfléchir à ce principe:
peut-être est-il désirable, au contraire,
de découpler l’orthographe de ce détail de prononciation ?
(cf section méthodo).
Il est déjà vrai actuellement qu’on ne note pas précisément les liaisons
(cf section liaison).

Selon ce principe, on n’écrirait plus de H aspirés,
et la question de la liaison ou non-liaison
serait alors reléguée au domaine exclusif de l’oral.



### Consonnes doubles

> **facile:**
> + consonne double indiquant une conjugaison (ex: "nous mourrons ≠ nous mourons")
>   --> à conserver
> + consonne double indiquant des préfixes/suffixes
>   --> dédoubler la consonne
>
> **moyennement facile:**
> + consonne double modifiant la valeur d’une voyelle `{e}`
>   --> dédoubler, et ajouter un accent grave ou aigu
>   (cf section voyelles alternantes, et en particulier section E ouvert)
>   - le plus souvent, ce sera un accent grave
>     (ex: #"j’apèle, bèle, lunète, chiène")
>   - parfois il faudrait plutôt un accent aigu
>     (ex: "effet, essai, dessert")
>   - pour `{o}` et `{eu}`, pas d’accent du tout,
>     car la distinction est fausse ou pas pertinente
>     (cf section voyelles alternantes)
> + `{s}` doublé en `{ss}` pour forcer `[s]` -->
>   supprimer la redondance entre `{s}` et `{z}`
>   (`{s}` se prononcerait toujours `[s]`),
>   cf section S/Z
>   - changement plus tendancieux, mais nécessaire
>     pour régler le problème des `{s}` prononcés `[s]` après schwas
>     (ex: "ressembler, resituer", etc.)



### Diacritiques

état des lieux des diacritiques existantes:

_LÉGENDE: lettre ; prononciation en voyelle ouverte/fermée ; rôle ; remarque_

+ `{e}` ; `[° / E]`
+ `{é}` ; `[e / -]` ; phonétique ; n’existe qu’en voyelle ouverte
+ `{è}` ;   `[E]`   ; phonétique  
+ `{ê}` ;   `[E]`   ; • distinctif pour quelques mots (ex: "forêt")  
        ------------- • étymologique (ex: "bête, fête")  
        ------------- • décoratif (ex: "extrême, frêle")
+ `{ë}` ;   `[E]`   ; graphique  
   .
+ `{o}` ; `[o / O]`
+ `{ô}` ;   `[o]`   ; phonétique(?) (mais prononciation réelle assez variable…)  
        ------------- additionnellement: distinctif pour "côlon, côte, côté, gnôle, rôder, nôtre, vôtre"  
   .
+ `{a}` ;   `[a]`
+ `{à}` ;   `[a]`   ; • distinctif pour 3 mots importants: "à, là, çà"  
        ------------- • inutile dans "déjà"
+ `{â}` ;   `[A]`   ; • distinctif pour "tâche"  
        ------------- • décoratif (ex: "âme, grâce, âcre")  
        ------------- • décoratif (passé simple, subjonctif imparfait)  
        ------------- • phonétique(?) pour les locuteurs qui distinguent encore `[a]` et `[A]`  
        -------------   (mais ce n’est pas toujours le circonflexe qui fait la distinction…)  
   .
+ `{i}` ;   `[i]`
+ `{î}` ;   `[i]`   ; • décoratif ; supprimé en 1990  
        ------------- • décoratif (passé simple)  
        ------------- • distinctif (subjonctif imparfait)
+ `{ï}` ;   `[i]`   ; graphique  
   .
+ `{u}` ;   `[y]`
+ `{ù}` ;   `[y]`   ; distinctif pour "où"
+ `{û}` ;   `[y]`   ; • décoratif ; supprimé en 1990  
        ------------- • décoratif (passé simple)  
        ------------- • distinctif (subjonctif imparfait)  
        ------------- • distinctif pour quelques mots (ex: "dû, mûr, sûr, jeûne")  
        ------------- • graphique pour "piqûre" (pré-1990)
+ `{ü}` ;   `[y]`   ; graphique  
   .
+ `{y}` ;   `[i]`
+ `{ÿ}` ;   `[i]`   ; ??? ; très rares noms propres  
   .
+ `{ç}` ;   `[s]`   ; phonétique

#### Cédille

cf section consonnes alternantes (Q/C).

#### É, È

cf section voyelles alternantes (accent plat).

#### Tréma

indépendant de la voyelle qui le porte,
sert à découper des syllabes et empêcher des groupements de lettres en n-grammes;
en ce sens il est redondant avec `{h}`
(cf section H)
=> choisir un des deux et jeter l’autre

**facile et évident:**
généraliser l’usage du tréma
(ex: "#dolmën, #tennismän, #brouää,
#güano, #aigüille, #aigüe, #ambigüe, #ambigüité,
#éqüidistant, #éqüateur, #piqüre").

**idée:** remplacer le tréma par un symbole *entre* les lettres;
meilleur car:

+ le tréma est asymétrique (pourquoi sur la 2e lettre ?),
  ce qui n’est pas anodin: règle mal comprise et mal enseignée,
  p.ex. beaucoup ont cru que "ciguë" se prononce [sige] mais c’est [sigu]
  (rectifié "cigüe" en 1990)
+ le tréma risque d’être en conflit avec une autre diacritique
  (vérifier si ça peut vraiment arriver;
  il est vraisemblable que cette autre diacritique
  empêcherait déjà la formation du n-gramme)
+ permet aussi de forcer la prononciation «dure» de `{g}` et `{c}`
  (cf sections GU, CH, et consonnes alternantes)
  (ex: #"g:éridon, acc:euil, c:eue, c:eur");
+ en écriture manuscrite cursive,
  le u-tréma `{ü}` se confond avec `{ii}`, en particulier dans `{üi}`.

quel symbole ?

- deux-points `{:}` ? (ex: #"ha:ir, sto:ique, gage:ure").
  certaines langues comme le suédois utilisent déjà `{:}`
  au milieu de mots pour séparer des lettres
  mais le risque de confusion avec le signe de ponctuation me semble excessif.
- apostrophe `{’}` ? (ex: #"ha’ir, sto’ique, gage’ure").
  déjà utilisée pour marquer une liaison *entre* des mots,
  où une élision au milieu d’un mot (notation du langage familier),
  risque de confusion avec un accent aigu.
- tiret `{-}` ? (ex: #"ha-ir, sto-ique, gage-ure").
  très mauvaise idée, le tiret évoque la composition de deux racines.
- lettre `{h}` ?
  pour ça il faut libérer les autres usages de `{h}`
  et réaliser le principe qu’il est toujours muet (cf section H).
  avantage: moins de symboles spéciaux.

dans l’idéal, un système orthographique mieux conçu
ferait disparaitre le besoin du tréma…

#### Circonflexe

usages multiples… le plus souvent sans rôle
ni phonologique (il peut être remplacé par un accent grave, ou pas d’accent du tout)
ni distinctif,
mais plutôt:

+ soit étymologique
  (ex: "bête → bestial; hôpital → hospitalier;
  île → insulaire, isolé; goût → gustatif; abîme → abismal")
  - même quand cette info n’est plus pertinente dans le lexique actuel
    (ex: "gâteau, arête, dîner, boîte, coût, guêpe, rêve, tête, champêtre → ???")
  - au contraire, cette info étymologique, même pertinente,
    est manquante dans le cas de `{é}` où elle est masquée par l’accent aigu
    (ex: "étant, était… → être;  état → statut;  été → estival;
    répondre → correspondre;  Étienne → Stéphane;
    étrange → anglais _strange_, latin _extra_;
    étonner → anglais _astonish_…");
    faudrait-il un e-accent-aigu-circonflexe `{ế}`?
    ceci semble plaider pour un e-accent-plat,
    avec un glyphe qui tolère le circonflexe (cf section idoine);
+ soit, à la rigueur,
  reflétant une nuance de prononciation qui existait au 18e siècle
  (voyelles longues)
  mais qui a disparu depuis
  (ex: "frêle, flûte")
  - on a vu que l’écrit ne doit pas coller de trop près aux accidents de l’oral,
    en voilà une illustration flagrante
+ soit de pure fantaisie («accent de majesté») !
  (ex: "âme, âcre, khâgne, trône…")
+ soit morphologique dans certaines conjugaisons:
  - 3e personne du singulier du subjonctif imparfait
    (ex: "qu’il mangeât"):
    distinctif dans ce cas
    (ex: "il vit, il vint, il but / qu’il vît, qu’il vînt, qu’il bût")
    mais est-ce important?
    on confond déjà de nombreuses autres formes conjuguées,
    et le subjonctif imparfait est en voie de disparition…
  - 1re et 2e personne du pluriel du passé simple
    (`{-^mes, -^tes}`):
    inutile dans ce cas

dans le cas de `{ê}` et `{ô}` (et `{eû}` dans le cas unique de "jeûne")
il a une incidence phonétique,
mais il existe d’autres diacritiques prévues pour
(accent grave ou aigu),
d’autant plus que l’effet est contraire dans ces deux cas
(ouvre une voyelle et ferme l’autre !).
dans le cas de `{ô}`,
la prononciation ouverte ou fermée selon les locuteurs semble,
en réalité, assez décorrélée de la présence du circonflexe
(cf sections ouvert/fermé),
donc il pourrait être avantageux de l’écrire tout simplement `{o}`
(ce qui nous évite un nouveau symbole «O accent aigu») ?
mais que faire des quelques paires homographes (ex: "cote/côte, etc.) ?

dans toutes les occurrences où `{ê}` a un rôle distinctif (ex: "foret/forêt"),
il n’est en fait pas essentiel que ce soit un circonflexe,
un accent grave suffirait à la distinction.

**moyennement facile:** on peut donc:

+ supprimer tous les circonflexes sans rôle distinctif,
  et même ceux du subjonctif imparfait
+ les garder pour les quelques mots où il a un rôle distinctif
  - à remplacer par la nouvelle diacritique de distinction des homographes
    (cf plus loin)
+ pour `{ê}`, `{ô}`, `{eû}`,
  les remplacer par des diacritiques phonétiques comme `{è}`, `#{ó}`, `#{eú}`
  (attention à ce que ça ne provoque pas la prononciation d’une consonne muette,
  p.ex. "#forèt, #tót"; cf section consonnes muettes;
  ceci dit, dans le cas de `{-ôt}` --> `#{-ót}`,
  comme on est en syllabe ouverte finale l’accent aigu est inutile,
  donc ici il suffit d’écrire --> `#{-ot}`)

pour plus de détails,
voir l’article _À quoi sert l’accent circonflexe_ de Maurice Tournier (cf biblio).

#### À, Ù, diacritique de distinction des homographes

l’accent grave sur `{à}` et `{ù}`
a un rôle uniquement distinctif,
pour un tout petit nombre de mots spéciaux.

**idée:**
on peut imaginer une diacritique dont la *seule* signification
serait de distinguer des homographes;
cette diacritique remplacerait `{à}`, `{ù}`, et les circonflexes distinctifs.
cette nouvelle diacritique pourrait être
l’accent grave pour prolonger son usage actuel,
ou l’accent circonflexe lui-même une fois débarrassé de ses autres usages
(cf aussi diacritique grecque précédemment).
ceci dit, pour une paire homographe,
le choix de quel mot porte la diacritique, et sur quelle lettre,
est parfois arbitraire… donc imprévisible…

cette diacritique est absolument inutile dans "déjà".

> **très facile:**
> + "déjà" --> #"déja"

Elle est moyennement utile dans "voilà"
(rappelle la formation du mot: "vois là",
et la paire "voici/voilà" comme "ci/là").

Dans d’autres sections
on propose divers autres emplois de la diacritique distinctive, notamment :

> + "au" --> #"àu" (cf section AU, EAU)
> + "en" --> #"àn" (cf section Voyelles nasales)

##### si

puisqu’on distingue "là/la" et "où/ou",
il serait logique de distinguer également
"si" ("oui, non, si") / "si" ("si, alors, sinon").
À mon avis c’est plutôt le 1er qui devrait porter la diacritique,
car:
"si (= oui), là, où" ont pour point commun de pouvoir s’employer isolément,
contrairement à "si (alors), la, ou";
que l’autre "si" est probablement plus fréquent à l’écrit;
et qu’il est préférable d’écrire l’autre "si" comme dans "sinon".
Cela rapprocherait le français de l’espagnol ("sí/si")
et de l’italien ("sì", avec "si" qui signifie autre chose).
Dire que certains s’enorgueillissent que le français serait
soi-disant plus précis que les langues voisines…
La diacritique permettrait de lever l’ambigüité
dans une phrase écrite telle que "si c’est ça…"

> **possible et facile:**
> + "si" (= "oui") --> #"sì"

##### plus

de même pour "plus" (+) / "plus" ("ne … plus"),
homographie qui cause de nombreuses ambigüités réellement gênantes
dans le langage courant écrit
(ex: "j’en veux plus; plus que la moitié").
Le premier pourrait être réécrit #"plùs"
(mêmes arguments à ce choix que pour "#sì/si";
en outre, la prononciation de "plus" signifiant "davantage" est spéciale,
tantôt le `{-s}` est muet et se lie en `[-z]`,
tantôt il se prononce `[-s]`:
"le plus grand/ancien, plus grand/ancien que, plus que,
de plus en plus grand/ancien, de plus en plus, de plus,
plus de riz, plus ou moins, le signe plus…";
noter aussi l’existence du participe passé "ils se sont plus"
prononcé comme le "plus" de négation).
Inconvénient mineur: parenté avec "plusieurs";
il pourrait se justifier que c’est le "plus" de négation
qu’il faudrait réécrire #"plùs"
(car c’est le mot le plus « spécial » des deux,
et car il exprime une négation tout comme #"sì"
exprime une affirmation).

> **possible et facile:**
> + "plus" (= "+") --> #"plùs"



### Voyelles alternantes

Pour rappel (cf avant), les voyelles sujettes à l’alternance ouvert/fermé sont:

+ `{o}` ~ `[o / O]`
+ `{eu, œu}` ~ `[ø / œ]`
+ `{e}` ~ `[° / E]`
+ `{é / è}` ~ `[e / E]`

Il n’est pas souhaitable en général de marquer la différence de prononciation
dans la graphie,
car cette prononciation est trop instable
(dépend de l’époque, des locuteurs, de l’effort de prononciation…),
même pour `{é / è}`.
De plus, même si la différence s’entend,
les paires de sons `[o / O]`, `[ø / œ]`, `[e / E]`
sont en général comprises comme équivalents.

#### Accent plat

Donc non seulement il vaut mieux garder
le fait que les graphèmes `{o}` et `{eu}` produisent chacun 2 sons différents,
il est même pertinent de ne plus distinguer `{é / è}` en général
et d’utiliser un graphème unique pour cette paire,
par exemple `#{ē}` («accent plat», proposé par Nina Catach, cf biblio).
Ainsi, la graphie ne varie plus
selon les époques, les locuteurs et le soin de l’élocution
(ex: "événement/évènement").

Dans la plupart des cas ça ne crée pas de problème,
vu les positions où on peut trouver respectivement `{é}` et `{è}`
dans l’orthographe actuelle (cf état des lieux plus haut).
Le seul cas problématique est `{è}` en syllabe finale ouverte
(ex: "dès, près, après, grès").
Pour ces cas il faut conserver l’accent grave `{è}`
(ce qui évite des homographies: p.ex: "des dés / dès, prés / près, grés / grès").

Éventuellement, on pourrait distinguer
les `{è}` qui sont associés à un `{é}` (ex: "je cède, évènement")
de ceux qui sont associés à un schwa,
et qui souvent sont employés au lieu de doubler une consonne (ex: "je lève, je pèle, près").
Seul le premier type devrait être remplacé par l’accent plat.
C’est un compromis différent entre phonétique et morphologie:
+ remplacer tous les `{è}` =
  orthographe davantage phonétique;
+ remplacer seulement les `{è}` associés à `{é}` =
  orthographe davantage morphologique
  car on utilise davantage les mêmes lettres dans les flexions et mots dérivés
  (en considérant que `{e}` et `{ē}` sont des lettres différentes).

> **possible:**
> + `{é}` --> `#{ē}`
> + `{è}` sauf en syllabe finale ouverte (ou si associé à `{e}` ?) --> `#{ē}`

Considérations graphiques:

- en principe le nouveau symbole pourrait ressembler à n’importe quoi,
  par exemple `#{&}`, mais il faut que ça rappelle la lettre `{e}` d’origine;
- `#{ē}` est satisfaisant par sa symétrie
  mais le risque de confusion visuelle avec `{è}` est trop grand
  (encore plus dans l’écriture manuscrite !),
  et il est difficile à saisir au clavier;
- réutiliser le symbole `#{é}` est plus pragmatique,
  mais cela assigne un nouveau sens à l’accent aigu,
  qui force déjà une voyelle fermée
  (c’est-à-dire le contraire de son miroir l’accent grave),
  usage qu’on propose par ailleurs d’étendre à `#{ó}` et `#{eú}`
  (cf section diacritiques);
- `#{ê}` est un autre candidat,
  qui a l’avantage de la symétrie,
  mais l’accent circonflexe a déjà plein d’usages différents,
  qu’on voudrait au contraire réduire (cf section diacritiques)…
- il serait pertinent de considérer ce nouveau symbole
  comme une lettre à part entière, différente de `{e}`
  (influence sur l’ordre alphabétique, la comparaison insensible aux diacritiques, etc.);
- dans l’idéal, pour vraiment respecter ce principe,
  il faudrait distinguer `{è}` associé à `{e}`
  de `{è}` associé au nouveau symbole,
  le second cas devrait être considéré comme
  une variante diacritée dudit nouveau symbole,
  plutôt que de la lettre `{e}`;
  il faudrait donc que le nouveau symbole
  puisse facilement prendre un accent grave…
  mais ça devient trop compliqué;
- autre possibilité, on peut revenir à la graphie latine `{e}` tout court,
  auquel cas il faut au contraire un nouveau symbole pour le schwa
  qui est actuellement noté `{e}`… (cf section compatibilité langues étrangères)
- vu que c’est les lettres les plus courantes en français,
  il faut qu’elles soient faciles à écrire à la main et au clavier.

#### E ouvert

Par contre, dans le cas de `{e}` qui produit `[° / E]`,
il est important de pouvoir dire clairement lequel des 2 sons doit être produit.
Or (cf état des lieux ouvert/fermé),
l’orthographe actuelle ne le rend pas évident;
de plus, on veut se débarrasser des consonnes doubles.

Est-il judicieux de remplacer tous les `{e}` prononcés `[E]` par des `{è}` ?

- cas particulier de la terminaison `{-et}`:
  attention que la graphie `#{-èt}`
  ne provoque pas la prononciation du `{-t}` muet (cf section consonnes muettes)
- cas particulier des mots monosyllabiques terminés par `{-s}` muets:
  attention aux homographes "es / ès, des / dès, les / lès",
  résoluble avec une nouvelle diacritique de distinction des homographes
  (cf section diacritiques)

En pratique, changement beaucoup trop lourd visuellement:
affecte énormément de mots
(puisque `{e}` est la lettre la plus courante en français),
dont de nombreux mots extrêmement courants
(ex: #"lès, dès, cès, sès, mès"),
et l’accent grave apparait souvent redondant avec les consonnes multiples
(ex: #"cèrtain").

Il faut plutôt régulariser
(1) les règles qui déterminent si un `{e}` doit être prononcé ouvert `[E]`,
et (2) la façon dont on décide de mettre un accent grave ou pas.
Outre les règles générales des syllabes ouvertes/fermées,
il y a 2 principes additionnels pour `{e}` non accentué:

 1. Dans les mots monosyllabiques dont la seule voyelle est `{e}`,
    si le mot se termine graphiquement par une consonne *même muette*,
    alors le `{e}` se prononce ouvert
    (ex: "les, des, ces, ses, mes, es, est, fret…")
 2. Dans la terminaison `{-et}`, même si le `{t}` est muet,
    le `{e}` se prononce ouvert
    (ex: "fret, sujet, aspect").

Et des irrégularités:

+ Certains monosyllabes sont écrits avec accent grave
  pourtant inutile d’après le principe des monosyllabes:
  "très, près, grès, dès, lès, ès"  
  => éliminer ces irrégularités autant que possible: -> #"tres, pres"
  (pour "dès, lès" il faut éviter l’homographie avec "des, les",
  cf section diacritique distinctive;
  pour "ès", il faut éviter l’homographie avec "tu es"
  et signaler que le `{s}` est prononcé,
  donc --> #"ess", cf section consonnes finales muettes).
+ Ces principes sont violés par certaines terminaisons comme `{-er, -ez, -ed}`,
  où `{e}` est prononcé fermé et la consonne est muette
  (ex: "chier, bêcher, chez, narguez, pied, assied");
  sachant que dans d’autres mots,
  ces mêmes terminaisons se prononcent bien `[-ER, -Ez, -Ed]`
  (ex: "cher, bécher, fez, merguez, bled")  
  => éliminer ces exceptions
  en réécrivant `{e}` en `#{ē}` quand il est prononcé fermé
  (cf section -ER, -EZ, -ED).
+ Ces principes sont violés par "et"  
  => éliminer cette exception --> #"ē"
  (cf section "et")
+ Dans certains mots, en raison de l’accolement de radicaux,
  on trouve `{e}` fermé devant plusieurs consonnes
  qui devraient normalement l’ouvrir
  (ex: "restaurer, restituer, restreindre / restatuer, resituer, restructurer";
  y a-t-il d’autres instances que le préfixe `{re-}` ?)  
  ou le contraire
  (ex: "poterie, papetier / interaction, papeterie")  
  => ajouter un tiret ?
  cf section tiret
+ Devant certains `{ss}` doubles, `{e}` se prononce `[°]`
  (ex: "dessus, ressource, ressembler…")  
  => éliminer tous les `{ss}` doubles,
  cf section S/Z.
+ (moins grave) Certains `{e}` ouverts sont prononcés plutôt `[e]` que `[E]`,
  selon l’accent
  (ex: "effet, essai, dessert")  
  => éventuellement, réécrire avec l’accent plat,
  cf section consonnes doubles
+ Dans `{emm, enn}`, `{e}` se prononce parfois `[a]`
  (ex: "femme, solennel", adverbes en `{-emment}`)  
  => réécrire ces `{e}` en `{a}`
  (cf section "femme", et -EMMENT dans section voyelles nasales).
+ Dans des emprunts (anglicismes, germanismes),
  `{e}` peut se prononcer `[œ]` même en syllabe fermée,
  particulièrement dans la terminaison `{-er}`;
  on ne peut pas y faire grand-chose
  à part franciser les graphies en `{-eur}`.

> **facile:**
> + "très" --> #"tres"
> + "près" --> #"pres" (bonus: rend plus visible la parenté avec "presque")
> + "ès" --> #"ess" (cf section consonnes muettes finales)
> + `{e}` ~ `[e]` --> `#{ē}` (puis modifs relatives aux consonnes doubles)
>
> **envisageable:**
> + préfixe `{re-}` ~ `[R°]` devant plusieurs consonnes --> ajouter un tiret
>   - éventuellement, généraliser à toutes les occurrences,
>     et à d’autres préfixes (ex: `{dé(s)-}`)

**bof:**
On pourrait aussi éliminer les 2 principes additionnels
(monosyllabes, terminaison `{-et}`)
en réécrivant `{e}` en `{è}` dans les 2 cas.
Mais lourd car concerne des mots très courants
(cf exemples déjà donnés).



### Prononciations irrégulières de voyelles

#### -ER, -EZ, -ED

Dans ces terminaisons, le `{e}` n’est prononcé ni `[°]` ni `[E]`, mais `[e]`.
Concerne des verbes (y compris "sied, assied"),
des noms communs (ex: "sorcier… nez+, rez-de-chaussée, pied+"),
des adjectifs (ex: "familier…")
et quelques adverbes ("volontiers, assez, chez").

Il serait judicieux de les réécrire avec `{é}`
(ou avec `#{ē}` si on adopte l’accent plat, cf section accent plat).
Il faut spécifier que ces nouvelles graphies ne provoquent pas
la prononciation de la consonne muette (cf section consonnes muettes).

> **très facile:**
> + `{-er}` ~ `[e]` --> `#{-ér}` (puis `#{ēr}` si accent plat) (cf aussi section consonne finale muette -R)
> + `{-ez}` ~ `[e]` --> `#{-éz}` (puis `#{ēz}` si accent plat) (cf aussi section -S, -Z, -X)
> + `{-ed}` ~ `[e]` --> `#{-éd}` (puis `#{ēd}` si accent plat)

Cette modification permet de clarifier
la prononciation de la consonne finale `{-r}`,
cf section consonne muette -R.
Elle résout d’ores et déjà de nombreux homographes
(ex: "scanner" = l’appareil / #"scannér" = le verbe;
"fier" = orgueilleux / #"se fiér" = faire confiance;
"bécher" = le récipient / #"bêchér" = travailler la terre).

#### -ENT

La terminaison verbale `{-ent}` (3e personne du pluriel)
est encore un cas bizarre.
La prononciation muette est très surprenante,
alors que graphie suggère à tort la prononciation `[@]`
comme dans "ils résident, ils content / le résident content"
(cf aussi section voyelles nasales),
ou `[5]` comme dans "ils convient, ils dévient / il convient, il devient".

Motivé par les formes irrégulières "ils ont, sont, vont, font" au présent,
et la terminaison uniforme "ils …-ront" au futur.

Je n’ai pas d’idée très convaincante ici…
Si la terminaison `{-et}` ~ `[-E(t)]` a été réécrite avec un accent grave
(cf section E ouvert)
on pourrait réécrire la terminaison verbale `{-ent}` en `#{-et}`…
Bof car
(1) impose de réécrire les `{-et}` existants, ce qui est peu désirable,
et (2) changement incompatible de la prononciation associée à la graphie `{-et}`.

Idée en l’air: utiliser une diacritique grammaticale
(dans le même esprit que la diacritique proposée dans la section -S, -Z, -X
pour distinguer les terminaisons verbales des lettres finales étymologiques)

Dans la section voyelles nasales,
on propose de réécrire `{-ent}` ~ `[-@]` en `{-ant}`.
Ça ne laisse que le conflit avec `{-ient}`;
on pourrait le résoudre en réécrivant `{ien}` ~ `[j5]` en `#{yen}`
(cf section Y),
mais bof de signaler la semi-voyelle seulement dans ce cas.

On doit garder le `{t}` car il s’entend en liaison.

#### et

Le mot "et" est une singularité où `{et}` se prononce `[e]`,
et où le `{t}` ne fait jamais de liaison.
On a conservé cette vieille graphie pour coller au latin.

> **très facile:**
> + "et" --> #"é" (puis `#{ē}` si accent plat)

Remarque: cela ne nuit nullement à la reconnaissabilité du mot.
Il s’écrit de façon similaire, avec une seule lettre,
dans d’autres langues latines où il est dérivé du latin "et":
portugais ("e"),
espagnol ("y/e"),
italien ("e/ed")…
D’autres langues l’écrivent également avec une seule lettre,
p.ex. russe ("i").

#### est

La forme "est" du verbe "être" est une singularité.
Elle crée une paire homographe non-homophone "on est / à l’est".

En toute logique la forme conjuguée devrait s’écrire "êt",
comme pour les autres formes conjuguées du verbe "être"
où on a remplacé un `{s}` amuï par un circonflexe (ex: "vous êtes"),
mais, la forme conjuguée "on est", et uniquement celle-ci,
a été conservée à cause de sa fréquence.
Le son `[s]` ne s’entend plus dans aucune forme du verbe "être"
(hormis "tu es" en liaison, qu’on propose par ailleurs
de réécrire #"tu èz", cf section -S, -Z, -X).

> **très facile:**
> + "est" du verbe "être" --> #"êt" (puis éventuelles modifs concernant le circonflexe)

#### Formes du verbe avoir avec le son [y]

La conjugaison du verbe "avoir" contient une singularité graphique,
où le son `[y]` est transcrit par le digramme `{eu}`:

+ participe passé: "eu(e)(s)"
+ passé simple: "j’eus, tu eus, il eut, nous eûmes, vous eûtes, ils eurent"
+ subjonctif imparfait: "j’eusse, tu eusses, il eût, nous eussions, vous eussiez, ils eussent"

La raison que je suppose:
ce serait un vestige d’une diphtongue en ancien français,
comme "veu, seu, peu, deu, creu, seur"
qui ont donné "vu, su, pu, dû, crû, sûr";
ou alors, c’est encore à cause de la confusion U/V dans l’alphabet latin
(cf section "huile")…
En tout cas, Alors que toutes ces graphies ont été régularisées en `{u}`,
la vieille graphie aurait été préservée
pour le verbe "avoir" en raison de sa fréquence.

En tout cas ça ne se justifie pas actuellement:
ce `{e}` n’est pas étymologique
et ne rappelle aucune autre forme du verbe "avoir",
qui est plutôt basé sur la lettre `{a}`.

Supprimer cette singularité:

> **très facile:**
> + `{eu}` ~ `[y]` --> `{u}` ("j’ai eu, il eut, nous eussions…")
> + `{eû}` ~ `[y]` --> `{û}` ("nous eûmes, il eût…")

Le seul souci, très relatif, est l’homographie avec "us" et "ut";
premièrement, ces deux mots sont vieillis donc ce n’est pas si grave;
deuxièmement, c’est déjà le cas de nombreux mots de ce genre,
certains bien plus courants, comme "pus, put, bus, but, sus, crus, chut, vis, vit…"
troisièmement,
"us" (coutumes) pourrait être réécrit par exemple
#"uss" (si on considère que le `{-s}` est sonore; cf section -S, -Z, -X)
ou #"ùs" (cf diacritique de distinction des homographes),
et de même
"ut" (la note de musique "do") pourrait être réécrit #"utt" ou #"ùt".

#### femme

Dans quelques mots, `{e}` suivi de `{mm, nn}` se prononce `[a]`.
Liste assez complète: "femme, solennel+",
adverbes en `{-emment}` (ex: "évidemment").

Pour les adverbes en `{-emment}`:
le `{e}` rappelle la graphie `{-ent}` de l’adjectif,
mais de toute façon
il est judicieux de réécrire toutes ces terminaisons avec `{a}`,
cf -ENT, -ENCE, -EMMENT dans section voyelles nasales.

Pour "solennel":
étymologie inutile, graphie héritée du latin
mais le `{e}` est prononcé `[a]` dans tous les mots de la famille.

Pour "femme":
étymologie utile,
on entend `[e]` dans les apparentés (ex: "féminin").
Mais vu que "femme" est l’un des mots les plus élémentaires de la langue,
il parait raisonnable de l’écrire régulièrement selon sa prononciation,
personne ne s’y trompera.

Réécrire tous ces `{e}` en `{a}`, conformément à la prononciation.

> **très facile:**
> + `{e}` ~ `[a]` --> `{a}` ("femme, solennel+")



### N-grammes de voyelles

#### OI

se prononce `[wa]`. la lettre `{o}` porte parfois une info étymologique
(ex: "voix → vocal, bois → bosquet, joie → jovial");
en revanche, sauf erreur de ma part,
il semble plus rare que la lettre `{i}` soit étymologique
(ex: "doigt → digital, voisin → vicinal, froid → frigo, poisson → piscicole") ?

donc on pourrait éventuellement le réécrire `#{oa}`.
d’un autre côté,
ça ne s’applique pas au trigramme `{oin}` auquel il est associé
(ex: "oignant → oindre, joindre → jonction"),
et ça nécessiterait éventuellement d’ajouter un tréma
dans les occurrences existantes de `{oa}` (ex: "boa, coaguler, rétroactif")…
donc bof.

> bof:
> + `{oi}` --> `#{oa}`
> + `{oa}` ~ `[oa]` --> `#{oä}`

note: par le passé, ce digramme trompeur `{oi}`, et plus précisément `{oign}`,
a induit des modifications de prononciation:
au début du 19e siècle, les mots "poigne, poignard, moignon"
se prononçaient encore comme "pogne, pognard, mognon",
mais l’orthographe enseignée à l’école de la IIIe République a induit en erreur
(la réforme de 1990 a corrigé la graphie des quelques mots survivants,
à savoir "ognon, encognure").

#### AU, EAU

`{(e)au}` se prononce toujours `[o]` fermé
(très rares exceptions: "saur, dinosaure+, Paul"),
bien que dans les accents du Sud de la France il puisse s’ouvrir.

la graphie `{au}` sert à rappeler une étymologie en `{a}`,
ce qui est très utile
(ex: "au → à, chevaux → cheval, haut → altitude").
de même, la graphie `{eau}` rappelle une étymologie en `{e}`
(ex: "beau → bel, peau → peler, bandeau → bander, bateau → batelier").

en revanche, sauf erreur de ma part,
l’utilisation de la lettre `{u}` est quelque peu arbitraire
puisqu’on ne la retrouve pas dans les mots apparentés,
on n’entend jamais `[y]` ni `[u]`
(vestige de la terminaison latine `{-us}`, trace d’une diphtongue médiévale ?).
dans `{(e)au}`, aucune des 2 ou 3 lettres ne se rapporte à la prononciation;
et dans `{eau}`, ni le `{a}` ni le `{u}` ne se rapporte à l’étymologie.

on peut imaginer remplacer le `{u}` par un `{o}`
qui aurait au moins l’intérêt d’indiquer la prononciation.
il faut alors ajouter des trémas dans certains mots existants contenant `{ao}`
(pas un problème pour `{eo}` car dans toutes les occurrences existantes,
`{e}` est muet, il suit un `{g}`).

> **possible, alternative 1/2:**
> + `{ao}` ~ `[ao]` --> `#{aö}`
> + `{au}` --> `#{ao}`
> + `{eau}` --> `#{eo}`

(pas cohérent avec le rare trigramme `{aon}` existant,
mais on propose par ailleurs de supprimer ledit trigramme,
cf section voyelles nasales.)

*mieux:* écrire plutôt `{ô}`,
ce qui force la prononciation fermée `[o]`,
rejoint la graphie existante de "Saône, geôle" (cas isolés)
et évite de devoir ajouter des trémas:

> **possible, alternative 2/2:**
> + `{au}` --> `#{aô}`
> + `{eau}` --> `#{eô}`

cas particulier: "eau → aquatique", racine en `{a}`;
cette graphie sert à distinguer la paire homophone "au / eau".
plutôt que d’ajouter arbitrairement un `{e}`,
on pourrait distinguer les deux mots
en suivant l’usage existant de `{à}` et `{ù}`
(cf aussi diacritique de distinction des homographes);
en plus, la nouvelle graphie de "au" rappelle le mot "à":

> **facile:**
> + "au" --> #"àu" --> #"ào" ou #"àô" (selon modif retenue concernant `{au}`)
> + "eau" --> #"au" --> #"ǎo" ou "aô" (idem)

cf aussi: diacritique lettre muette

évidemment, examiner au cas par cas la pertinence de l’info
étymologique…
(ex:
"sauvage" du latin "silvaticus", `[a]` ne s’entend jamais,
ressemblance trompeuse avec "sauver, salvation";
"dauphin" du latin "delphinus", `[a]` ne s’entend jamais;
"épaulard → épaule → spatule" ???)

parfois l’info étymologique est donnée
de façon incohérente au sein d’une famille,
ce qu’il faudrait corriger quand le lien est pertinent
(ex: "chose/cause, oreille/auriculaire, or/aurifère")

##### Å (AU, EAU, EU, OU)

autre idée amusante:
`{au}` pourrait plutôt être réécrit `#{å}` comme dans les langues scandinaves.
on peut même étendre le principe aux autres digrammes terminés par `{u}`,
dans ce cas l’accent rond indique un arrondissement de la bouche:

> **possible/bof:**
> + `{(e)au}` --> `#{å}` (comme langues scandinaves)
>   * éventuellement `{eau}` --> `#{e̊}`
> + `{eu}` --> `#{e̊}` (dans langues scandinaves, noté `{ø}` ou `{ö}`, translitéré `{oe}`)
>   * mais on a une meilleure option, cf section EU,
>     ce qui permet de garder `#{e̊}` pour `{eau}`
> + `{ou}` --> `#{o̊}` ou plutôt #{ů}` (comme en tchèque)

intérêt:

+ moins de n-grammes donc facilite l’interprétation phonétique de la graphie
+ `#{å}` est plus léger à écrire que `#{ǎô}`
+ permet de translitérer naturellement certains emprunts

mais multiplication de symboles,
risque que "å" soit incorrectement translitérée en "a" informatiquement,
modif assez superflue => bof

#### EU

Le digramme `{eu}` (ou `{œu}`) qui produit `[ø / œ]`
vient le plus souvent (presque toujours ?) d’une racine `{o}`
(ex: "yeux → oculaire, heure → horaire, fleur → floral,
peuple → populaire, seul → soliste, recteur → rectorat,
douleur → douloureux, meurs → mourir, feuille → folio,
feu → foyer, jeu → jouer, cueillir → collecte…").
Auquel cas il n’est pas clair pourquoi on écrit une lettre `{u}`
(vestige de la terminaison latine `{-us}` ?
plutôt trace d’une diphtongue médiévale… ce qui remonte pas mal !).
(cas assez rares ou ce graphème correspond à un `{u}` :
"fleuve → fluvial, pleut → pluie, beurre → butyreux";
autre cas rare : "queue → caudal")

On pourrait remanier ce graphème pour rappeler l’étymologie `{o}`
(ce que l’actuelle graphie `{œu}` ne fait que pour
un ensemble de mots petit et arbitraire, cf section Œ):

> **possible et facile:**
> + *soit* `{eu, œu}` --> `{œ}` et supprimer les autres usages de `{œ}`
>   (bof car confusion avec `{oe}`; cf section Œ)
> + *soit* `{eu, œu}` --> `#{oe}`
>   et ajouter un tréma sur toutes les occurrences existantes de `{oe}`
>   (bof car nouveau digramme, trémas à ajouter ailleurs; cf aussi section Œ)
> + *soit* `{eu, œu}` --> `#{ǒe}` (cf section diacritique lettre muette)
> + *soit* `{eu, œu}` --> `#{ö}` (comme en allemand et suédois,
>   bof car autre signification du tréma, et risque que le tréma soit omis)
> + *soit* `{eu, œu}` --> `#{ø}` (comme en norvégien)
>   — **meilleure solution** à mon avis
> + *soit* `{eu, œu}` --> `#{e̊}` (cf section Å)

Il faut choisir si la voyelle `#{ø}` est dure ou douce après C et G.
Il y a à peu près deux fois plus d’occurrences
de `{queu, cueil, gueil}` que de `{ceu}`,
et deux fois plus de `{gueu, jeu}` que de `{geu}`.
De plus, `#{ø}` est associé et ressemble graphiquement à `{o}`, qui est dur.
Donc il semble préférable que `#{ø}` soit dur.
Ce qui nous débarrasse d’ailleurs des graphies spéciales `{cueil, gueil}`.

En toute objectivité, moi je suis fan de cette lettre Ø :
#"factør, inspectør, pøple, rigør…"
Même plus besoin de plisser les yeux pour voir l’anglais, euh, le latin !

#### Æ, Œ

Les ligatures `{æ}` et `{œ}` sont problématiques
car elles *doivent être considérées comme des lettres à part entière*,
mais elles sont largement confondues avec
les digrammes non soudés `{ae}` et `{oe}`
qui sont pourtant très différents.
(La confusion est permanente:
l’école nous apprend à épeler « O, E-dans-l’O » comme si c’était 2 lettres,
l’ordre alphabétique usuel traite `{œ}` comme la succession de 2 lettres `{oe}`,
et la ligature est transcrite `{oe}` dans de nombreuses saisies informatiques.)

Les ligatures induisent en erreur sur la prononciation
car on ne prononce pas du tout `[a]` ni `[o]`,
et car `{c}` suivi de `{æ}` ou `{œ}` doit parfois être prononcé `[s]` !

ex: "coefficient" [koE-], "cœur" [kœ-], "cœlacanthe" [se-] ! "et cætera" [se-] !

De plus `{œ}` a plusieurs usages contradictoires
qui induisent en confusion
et ont déjà entrainé des modifications de prononciations (ex: "Œdipe").

**`{æ}`** n’existe pas réellement en français,
il n’est employé que par pure coquetterie
comme variante esthétique de `{e}` ou `{ae}`
dans des mots latins non francisés (ex: "et cætera, ex æquo, curriculum vitæ")
et dans le prénom "Læticia".
Se remplace avantageusement par `{e}` (latin) ou `{é}` (français),
ce qui en outre clarifie la prononciation.

> **très facile:**
> + `{æ}` --> `{é}`

**`{œu}`** produit, tout comme `{eu}`, l’alternance `[œ / ø]`
(ex: "bœuf / bœufs").
Il rappelle une étymologie en `{o}`
(ex: "œil → oculaire, sœur → sororité, chœur → chorale")
mais son emploi n’est pas systématique
(ex: "yeux, fleur → floral, heure → horaire, recteur → rectorat…").
En fait, la très grande majorité des graphies `{eu}`
correspondent de toute façon à une racine `{o}` (cf section EU).
L’emploi très ponctuel de `{œ}` pour marquer cette étymologie
est donc incohérent (ex: "les vœux / je veux").
On peut donc remplacer `{œu}` par `{eu}`
(attention, il faut réécrire "cœur, rancœur" en "#queur, #ranqueur").

> **très facile:**
> + `{œu}` --> `{eu}`
>   - puis éventuelle modif de `{eu}` (cf section EU)

**`{œ}`** tout court a deux origines et deux prononciations
complètement différentes:

1. dans des mots gréco-latins, il produit `[e]`
   (exactement comme `{æ}` pour le latin)
   (ex: "fœtus, œsophage, Œdipe");
2. dans des emprunts germano-scandinaves, il produit `[œ / ø]`
   (variante de `{oe}`, utilisé pour translitérer les lettres `{ö, ø}`)
   (ex: "fœhn").

L’usage comme lettre grecque est cantonné à un petit ensemble arbitraire de mots
(ex: pourquoi n’écrit-on pas #"œconomie" ?).

Par confusion entre les deux et avec `{œu}`,
on a souvent cru que des `{œ}` grecs se prononçaient `[œ / ø]`,
au point qu’on ne peut plus parler de prononciation fautive
mais de nouvelle prononciation (ex: "œsophage, Œdipe").
Là encore il vaut mieux se débarrasser de cette lettre:

> **très facile:**
> + `{œ}` --> `{eu}` ou `{é}` ou les deux, selon les cas
>   - puis éventuelle modif de `{eu}` (cf section EU)

Avantages:

+ moins de façons différentes d’écrire un même son
+ moins de symboles différents
+ supprime le besoin du E tréma `{ë}`
  (puisque `{æ, œ}` sont les seuls graphèmes du français terminés par `{e}`)

Inconvénient ? ok c’est mignon…
Mais pas de panique, la mignonnerie revient au centuple dans la section EU.

#### AI, EI

même remarque que pour les cas particuliers `{ain}` et `{ein}`
(cf section voyelles nasales):

+ `{ai}` indique souvent une étymologie en `{a}`
  (ex: "baigner → balnéaire, saigner → sang, paire → parité")
  - dans certains cas il ne porte pas d’info étymologique
    (suffixe `{-ais}` dans "français", `{-aine}` dans "dizaine")
+ `{ei}` indique soit (le plus souvent ?) une étymologie en `{e}`
  (ex: "peine → pénal, veine → vénal, reine → réginal, peigne → pectiniforme")
  auquel cas le `{i}` semble superflu,
  soit une étymologie en `{i}`
  (ex: "veille → vigile, enseigne → signe"; plus rare ?)

on pourrait donc envisager les réécritures suivantes:

> + `{ei}` --> `{è}`
> + `{ai}` --> `#{aè}`
>   - voire: `{ai}` --> `{è}` si l’info étymologique ne nous intéresse pas

cf aussi: diacritique lettre muette

ceci peut créer des homographes
(ex: "reine/rêne, peine/pêne, paire/père, vair/ver,
maitre/mètre, repaire/repère…")

remarque: s’applique en particulier aux terminaisons verbales
de l’imparfait et du conditionnel (`{-ais, -ait, -aient…}`).

là encore, des incohérences à corriger
(ex: "contraindre, astreindre, restreindre, constricteur"
sont de la même famille, avec une racine en `{i}`;
"frais → frisquet", de _frisk_;
"vrai → vérité;
certain → certitude").

cette info étymologique n’est pas donnée systématiquement,
même dans des cas où elle serait pertinente
(ex: "mère → maternel, maman;
père → paternel, papa;
frère → fraternel;
équerre → carré;
trouvère → troubadour")

un cas fréquent:
la terminaison `{-el(le)}`, doublon de `{-al(e)}`
et dont le `{e}` devient `{a}` en dérivé
(ex: "réel→réalité; originel/original")
alors que "aile → alaire".

#### Voyelles nasales

elles sont indiquées par la lettre `{N}` (`{n}` ou `{m}`).
elles sont dénasalisées et prononcées normalement, ainsi que le `{N}`,
lorsqu’elles sont suivies d’une voyelle
(ou parfois en fin de mot
ou dans des mots étrangers, p.ex: "lichen, tennisman").

problèmes:

+ plusieurs façons d’écrire un même son nasal;
  ces graphies multiples sont dues
  pour une part à des alternatives dénasalisées différentes,
  et pour une autre part à des marques étymologiques:
  - `[@]` admet 2 graphies principales: `{aN}`, `{eN}`;
  - `[5]` admet 4 graphies principales: `{eN}`, `{IN}`, `{eIN}`, `{aIN}`;
    plus `{uM}` pour les locuteurs qui confondent `[5]` et `[1]`;
+ inversement, `{en}` transcrit 2 sons différents: `[@]`, `[5]`;
+ le préfixe `{en-}` est parfois prononcé `[@n]` !
  (ex: "ennui, enivrer")
+ (rare) `{uN}` est parfois prononcé `[§ / On]`…

état des lieux:

+ `{aN}`: `[@]` / `[an]` (ex: "pan / panneau")
+ `{aon}`: `[@]` / `[an]` (seulement 3 mots: "faon(ne), paon, taon");
  très rare, issue d’une consonne disparue entre `{a}` et `{o}`
+ `{eN}`: `[@]` / jamais dénasalisé ?
  - sauf dans "en",
    on ne trouve jamais `{en}` prononcé `[@]` seul en fin de mot,
    il est toujours suivi d’une consonne, ce qui fait qu’ajouter
    un suffixe ne le dénasalise pas
  - parfois la graphie `{eN}` indique une racine autre que `{a}`
    (ex: "enfant → infantile, dent → orthodontiste, sembler → similaire");
  - parfois le choix de `{en}` plutôt que `{an}` n’est pas significatif,
    et l’usage a apparemment hésité au Moyen-Âge
    lors de la fixation de l’orthographe
    (ex: "résident", "example" en anglais)
    <!--(d’ailleurs l’usage a aussi hésité pour ajouter `{-t}` aux participes présents
    (ex: "paysan, courtisan"), et omettait le `{-t}` au m.p. jusqu’en 1835);-->
+ `{eN}`: `[5]` / `[En]` (ex: "mien / mienne")
  - souvent précédé de `{i}` (ex: "mien, bien, païen");
    `{ien}` pourrait être un raccourci pour `#{iein}` ?
  - mais pas toujours (ex: "ben, benjamin, pentacle, benzène")
+ `{eiN}`: `[5]` / `[En]` (ex: "frein / freiner, plein / pleine");
  - indique le plus souvent une racine `{e}`
    (ex: "rein → rénal, plein → plénitude,
    ceinture → ceignons, geindre → geignard");
    dans ce cas, si `{en}` produit le même son, le `{i}` est inutile ?
  - indique parfois une racine `{i}`
    (ex: "peindre → pictural, éteindre → extinction, seing → signer")
+ `{aiN}`: `[5]` / `[En]` (ex: "sain / saine");
  indique une racine `{a}` (ex: "sain → santé, main → manuel, gain → gagner")
+ `{iN}`: `[5]` / `[in]` (ex: "fin / fine");
  indique une racine `{i}`
+ `{yN}`: `[5]` / `[in]` (ex: "thym / thymol", seule alternance trouvée);
  indique une racine grecque upsilon
+ `{eun}`: `[1]` / `[øn]` (ex: "à jeun / jeûne", seule occurrence trouvée)
+ `{uN}`: `[1]` / `[yn]` (ex: "un / une");
+ `{uN}`: `[§]` / `[On]` (ex: "acupuncture, homuncule, secundo, rhumerie, summum, rumsteck");
  - systématique pour les mots latins en `{-um}` mais rare ailleurs
+ `{oN}`: `[§]` / `[On]` (ex: "bon / bonne")
+ `{aon}`: `[a§]` / `[aOn]` (ex: "lycaon, machaon, pharaon", seules occurrences)

en cherchant longtemps,
voici toutes les occurrences que j’ai trouvées
où `{en}` correspond à un son `[e/E/œ]` ailleurs dans la famille;
il y en a sûrement d’autres mais en tout cas ça semble rare,
et les liens de parenté sont rarement directs
(du type infinitif→conjugaison, verbe→nom, adjectif→nom, masculin→féminin)…
(beaucoup de `{en}` viennent directement du latin
où ils étaient suivis d’une consonne
qui semble n’avoir jamais disparu dans un dérivé;
p.ex: "vent, lent, centre, mentir, argent, défense, offense")

- "temps → tempo" (via l’italien)
- "trembler → trémolo" (via l’italien)
- "suspens → suspense" (via l’anglais)
- ? "sens → sémantique" (non, coïncidence)
- "mensuration → mesure"
- "mensuel → semestre"
  (de toute façon la famille est déjà brouillée vu que le mot principal se dit "mois")
- "prévention, convention, subvention, intervention →
  prévenir, convenir, subvenir, intervenir"
- "content → contenir" (très lointain)
- "éventuel → évènement, avenir"
- "engendrer → génération"
- "genre → gène"
- "gens → gène" (très lointain)
- "rendre → reddition"
- "prendre → prenons, prenne" (et dérivés "apprendre, comprendre…")
  (verbe de toute façon irrégulier)
- "compenser → peser" (lointain)
- "penser → peser" (très lointain)
  * note culturelle: "penser", du latin "pensare",
    s’écrivait aussi bien "penser" que "panser" en ancien français;
    en français moderne, "penser" et "panser" sont un dédoublement de ce même verbe !
- "vendre → vénal" (lointain)
- prononciation initialement fautive `[en-]` (plutôt que `[@n-]`)
  du préfixe `{en-}` dans quelques mots
  (ex: "enamourer / énamourer"),
  à cause de la graphie trompeuse (cf [TLFi][tlfi-en-])

À l’inverse, `{en}` est associé au son `[a]`
dans la famille de suffixe `{-ence, -ent, -emment}`
(ex: "conscience → consciemment") !

[tlfi-en-]: https://www.cnrtl.fr/definition/en-

##### -ENCE, -ENT, -EMMENT

les terminaisons `{-ence}`, `{-ent(e)}`, `{-emment}`
(ex: "patient / patience / patiemment; prudent / prudence / prudemment; déshérence")
ne sont pas particulièrement étymologiques, mais morphologiques
(c’est un suffixe identique ajouté à tous les mots concernés;
elles sont en doublon avec `{-ance}`, `{-ant(e)}`, `{-amment}`
(ex: "constant / constance / constamment; garant / garance; errant / errance")
qui sont plus courantes (à cause des participes présents).
on peut aligner toutes ces terminaisons sur `{-ance}`, `{-ant(e)}`, `#{-ammant}`;
avantages:

+ plus logique quand le mot correspond au participe présent d’un verbe
  ("résident" est à "résider" ce que "habitant" est à "habiter",
  d’ailleurs la graphie prétendument fautive "résidant" est très courante)
+ plus besoin de se demander sans cesse
  si un adverbe s’écrit `{-amment}` ou `{-emment}`
+ supprime une ambigüité graphie → phono
  responsable de nombreux homographes non homophones
  (ex: "résident, parent, couvent, évident…" [homonymes.txt](./homonymes.txt))
  (cf aussi section -ENT).
+ pour les adverbes, supprime une prononciation irrégulière `{e}` ~ `[a]`
  (ce qui montre encore qu’écrire `{-ent}` plutôt que `{-ant}`
  était finalement assez arbitraire).

> **très facile:**
> + `{-ence(s)}` --> `{-ance(s)}`
> + `{-ent(e)(s)}` ~ `[@]` --> `{-ant(e)(s)}`
    (ceci inclut tous les adverbes en `{-ment}`)
> + `{-emment}` pour un adverbe dérivé d’un adj. en `{-ent}` --> `#{-ammant}`

##### EN-

**moyennement facile:**
il faut faire quelque chose pour le préfixe `{en-}` prononcé `[@n]`…

+ peut-être ajouter un tiret ?
  (ex: "#en-ui, #en-ivrer, #en-amourer, #en-mener")
  cohérent avec la prononciation d’une liaison entre 2 mots
  (ex: "en avant, un ami, mon ami")
  mais ça signifie re-dé-composer des mots lexicalisés…
+ ou un tréma généralisé (cf section tréma) ?
  (ex: "#en:nui, #en:nivrer, #en:namourer, #en:mener")
+ si on a éliminé les consonnes doubles
  (cf consonnes doubles plus haut),
  on peut aussi décider d’écrire un `{nn}` double
  uniquement pour ce cas
  (ex: "ennui, #ennivrer, #ennamourer, emmener")
+ si le mot "en" est réécrit #"àn" (cf plus loin),
  alors le préfixe `{en-}` peut être réécrit de la même façon
  (car il s’agit en fait de la même chose),
  et on peut convenir que `{à}` suivi de `{n}` est toujours nasalisé
  même quand le `{n}` se prononce
  (ex: "#ànui, #ànivrer, #ànamourer, #àmener").
  mais pour être logique,
  ça implique d’écrire `{à}` dans plein de mots en `{en-}`
  où il n’aurait pas de fonction distinctive
  et où ce problème de prononciation n’existe pas,
  et ça implique aussi (comme pour la solution du tiret)
  de re-dé-composer des mots…
  donc bof bof.

##### Approche globale

réécrire `{uN}` en `{oN}` dans les quelques mots où ça se prononce `[§ / On]`.
conserver la terminaison latine `{-um}`.

réécrire la terminaison latine `{-um`} serait un changement plus lourd,
qui altérerait la graphie de mots latins
qu’on ne considère pas toujours comme français,
et dont jusqu’à présent on a préservé la graphie.
de plus, il n’y a guère d’ambigüité:
le seul mot avec une terminaison `{-um}` qui ne se prononce pas `[Om]`
est "parfum".

> **très facile:**
> + `{uN}` ~ `[§]` --> `{oN}` sauf la terminaison latine `{-um}`

plus délicat:
réduire globalement le nombre d’ambigüités
et de graphies différentes du même son…
une tentative:

> **à réfléchir:**
> + `[@]` / `[an]`:
>   - `{aN}` conservé
>   - `{aon}` --> `{an}` (ne concerne que "faon(ne), paon, taon", crée des homographes)
>   - `{eN}` --> `{aN}`
>     * ou: `{eN}` --> `#{eaN}` si on veut garder l’info étymologique
>       (suit le même principe que `{ain, ein}`:
>       la 1re lettre est muette et étymologique,
>       les autres lettres donnent la prononciation)
> + `[5]` / `[En]`:
>   - `{eN}` conservé
>   - `{eiN}` --> `{eN}`
>   - `{aiN}` --> `#{aeN}`
>     * voire: `{aiN}` --> `{eN}` si l’info étymologique ne nous intéresse pas
> + `[5]` / `[in]`:
>   - `{iN}` conservé
>   - `{yN}` --> `{iN}`
> + `[1]` / `[yn]`:
>   - `{uN}` conservé
> + `[1]` / `[øn]`:
>   - `{eun}` conservé (ou --> `#{øn}`, cf section EU) (ne concerne que "à jeun")
> + `[§]` / `[On]`:
>   - `{oN}` conservé
>   - `{uN}` --> `{oN}` sauf éventuellement la terminaison latine `{-um}`

cf aussi: diacritique lettre muette

ceci crée des homographes
(ex: "en/an, sens/sans, sein/sain, plein/plain, ben/bain…")

cas particulier de "en/an":
on peut distinguer les deux mots
en suivant l’usage existant de `{à}`
(cf aussi diacritique de distinction des homographes),
ce qui est agréable car "en" et "à" ont des emplois proches
(cf section "en dessous"):

> **possible:**
> + "en" --> #"àn"

les occurrences irrégulières
où le n-gramme n’est pas nasalisé
peuvent s’indiquer par un tréma
(cf section Tréma):

> **très facile:**
> + `{aN}` ~ `[an]` --> `#{äN}` (ex: "tennisman, fan, van, macadam, Adam")
> + `{eN}` ~ `[En]` --> `#{ëN}` (ex: "amen, dolmen, pollen, cérumen")
> + `{iN}` ~ `[in]` --> `#{ïN}` (ex: "clim, gym")
> + `{oN}` ~ `[On]` --> `#{öN}` (ex: ???)
> + `{um}` ~ `[Om]` --> `#{üm}` voire `#{öm}` (ex: "album, aquarium…")

##### M avant P/B

**très facile:**
La règle actuelle est que `{n}` est remplacé par `{m}` devant `{p/b}`.
Cela s’explique clairement par des contraintes phonétiques naturelles
(facilité d’articuler ces séquences de consonnes).
Cependant, cette raison disparait
quand `{m/n}` nasalise la voyelle qui précède
et n’est donc plus prononcé en temps que tel.
On pourrait donc décider que les sons nasalisés
s’écrivent toujours avec `{n}`, jamais avec `{m}`.
Ceci divise par 2 le nombre de façons d’écrire un même son nasal et, surtout,
réduit à 1 le nombre de prononciations possibles des graphies contenant `{m}`.
Avantages:
améliore les emprunts étrangers non nasalisés (ex: "tempo")
(cf section Compatibilité langues étrangères)
ou même nasalisés (ex: "Istanbul");
améliore la soudure de mots
(ex: "bonbon, néanmoins, mainmise" qui sont actuellement des raretés;
#"ronpoint" plutôt que "rond-point, rondpoint, #rompoint"
pour lesquels soit le pluriel est difficile,
soit il y a une consonne muette au milieu du mot,
soit la composante "rond" est moins identifiable;
cf section Mots composés)
(il y a aussi l’inexplicable singularité "bonbonne");
en particulier le préfixe `{en-}` (ex: #"enmagasiner") (cf section EN-).



### Y, ILL

la lettre `{y}` dans des groupes de voyelles
a un effet assez imprévisible
(et donc, en voyant le mot écrit, impossible de savoir comment ça se prononce;
p.ex: "pagaye" ???):

 1. parfois elle produit simplement `[j]`
    (ex: "mayo, bayou, kayak, bayer, asseye, goyave");
 2. parfois elle se combine à la voyelle précédente tout comme le ferait un `{i}`
    (ex: "saynète, claymore, keynésien");
 3. le plus souvent elle fait les deux en même temps (!)
    et est donc équivalente à `{i-i}`,
    où le 1er `{i}` se combine à la voyelle précédente
    et le 2e `{i}` produit `[i]` ou `[j]` isolément
    (ex: "pays, abbaye, payer, doyen, loyal, noyau, bruyère");
    - en effet, la plupart des `{y}` (hors lettres grecques)
      sont une ancienne ligature pour `{ii}`, devenu `{ij}` puis `{y}`;
      cette situation est réellement étrange:
      il faut donc voir Y non pas comme une lettre,
      mais comme une *abréviation* pour une suite de 2 lettres,
      qui ne respecte même pas les frontières entre graphèmes
      (la 1re lettre se combine à ce qui précède) !

le graphème `{ill}`:

+ après une voyelle, il vaut `[j]` (donc le `{i}` ne se combine pas)
+ sinon, il vaut en général `[ij]`
+ dans quelques cas il vaut `[il]` (ex: "mille+, ville+, tranquille")

le graphème `{-il}`:

+ après une voyelle, il vaut `[j]` (donc le `{i}` ne se combine pas)
+ sinon, il vaut en général `[il]`
+ dans quelques cas il vaut `[i]` (ex: "gentil, outil, fusil")
+ parfois les deux prononciations co-existent (ex: "sourcil, persil")

de plus, on peut trouver dommage d’écrire 3 lettres dont 2 consonnes
pour transcrire juste un demi-son, de type voyelle…

**idée 1**,
pour éliminer l’ambigüité phonétique de `{ill}` et `{-il}`:
n’écrire la consonne double `{ill}` que pour le son `[i/j/ij]`,
et n’écrire `{il}` que pour le son `[il]`.
si on se débarrasse des consonnes doubles (cf section consonnes doubles),
le `{ll}` restera alors avec une signification spéciale, différente de `{l}`.
noter qu’on se retrouve avec des `{-ill}` terminaux.
le seul souci est qu’il faut accepter
les homophones "ville / vile" et "mille / mile"
("mile" est un anglicisme, qui se traduit justement par "mille",
et de toute façon on pourrait écrire "mille" --> #"mil",
cf section marque du féminin;
"vile" est un adjectif assez rare,
de plus le rapprochement entre "ville" et "civil" est intéressant)…

> **facile:**
> + `{ill}` ~ `[il]` --> `{il}`
>   (ex: #"la vile, mile ans, tranquile")
> + `{-il}` ~ `[i]` (après consonne) --> `{-ill}`
>   (ex: #"gentill, outill, fusill")
> + `{-il}` ~ `[j]` (après voyelle) --> `{-ill}`
>   (ex: #"aill, baill, treuill, accueill, vermeill")

**idée 2**
(indépendante de l’idée 1),
pour éliminer l’ambigüité phonétique de `{y}`:
n’utiliser la lettre `{y}` que pour le son `[j]`
(cf aussi section lettres grecques);

> **facile:**
> + `{y}` isolé --> `{i}` ou `{î}` (cf section lettres grecques)
> + `{i}` ~ `[j]` --> `{y}` (ex: "bien") ? non, bof (cf ci-dessous)
> + `{y}` cas 1 --> conservé
> + `{y}` cas 2 --> `{i}`
> + `{y}` cas 3 --> `#{iï}` ou `#{iy}` selon prononciation
>   (ex: "pays --> #paiïs, payer --> #paiyer")

l’inconvénient de la graphie `#{iï}` est son manque de lisibilité,
`{ii}` écrit en attaché se confond avec un u-tréma `{ü}`;
c’est d’ailleurs ce qui a historiquement conduit à la variation graphique `{ij}`,
qui par la suite a été ligaturée.
proposition alternative: employer un igrec-tréma `#{ÿ}` !
(ex: #"paÿs, paÿer")
intérêts:
modif plus légère de l’existant (on ajoute juste un tréma),
emploi pas si éloigné que ça de l’usage actuel du tréma,
et les deux points du tréma rappellent ceux des deux i :-)
(on évite la ligature `#{ĳ}` du néerlandais car indifférenciable visuellement
de la succession de lettres `{ij}`, et le `{j}` français se prononce `[Z]`).
inconvénient:
pas cohérent avec l’usage de `{y}` ailleurs pour transcrire le i bref.

**idée 2b** (s’ajoute à 2 et remplace 1)
on pourrait aussi utiliser `{y}`
à la place de `{-il, ill}` qui produisent `[i/j/ij]`:

> **facile:**
> + `{-il, ill}` --> `#{iy}` (ex: "outil, fille, vrille")
>   sauf quand prononcé `[il]`;
>   en particulier:
>   - `{-ail, aill}` --> `#{ay}`
>   - `{-eil, eill}` --> `#{ey}`
>   - `{-euil, euill}` --> `#{euy}`
>     * `{-cueil, cueill}` --> `#{queuy}` (cf section CUEIL)
>     * `{œil}` --> `#{euy}` (cf section Œ)
>   - `{-ouil, ouill}` --> `#{ouy}`

note: si on ne veut pas perdre l’info étymologique fournie par le `{l}`
(ex: "famille → familial, meilleur → améliorer,
abeille → apicole, veille → vigile"),
on peut à la rigueur mettre un circonflexe sur le `{y}`…
(mais cf sections lettres grecques et circonflexe).

note culturelle:
historiquement, `{ill}` produisait un son différent de `[j]`,
qui ressemblerait plutôt à `[lj]` ou au `{ll}` espagnol.
cette distinction a disparu.

note: la graphie `#{ouy}` qui produit `[uj]`
est à distinguer de la graphie `{oui}` (ou `#{wi}` ?) qui produit `[wi]`.

#### Semi-voyelles

Le français présente trois sons «semi-voyelles»:

+ `[w]`: `{ou}` court, dans `{oui, oi, oin}`, "watt" p.ex.
+ `[8]`: `{u}` court, dans `{ui}`, "visuel" p.ex.
+ `[j]`: `{i}` court, dans `{ion, ill}`, "kayak" p.ex.

L’orthographe actuelle utilise les mêmes lettres
pour noter les semi-voyelles que leurs équivalents longs
(il existe la lettre `{w}` mais elle n’est utilisée que pour des mots étrangers).

C’est une bonne chose, il n’est pas utile ni souhaitable
de coller trop précisément à la prononciation car:

+ la façon dont les voyelles se raccourcissent suit un mécanisme assez général:
  `[w, y, i]` se raccourcissent quand suivies par une autre voyelle  
    (ex: w: "quoi, loin, couac, couic, couenne")  
    (ex: u: "nuit, écuelle, lueur")  
    (ex: i: "ion, iode, ciel, alliage")  
  sauf certains contextes qui rendent ça difficile
  (après certaines séquences de consonnes, surtout):  
    (ex: w: "clou-er, tro-ëne")  
    (ex: u: "cru-el, pu-ant, nu-age")  
    (ex: i: "tri-age, cri-er, sangli-er")
+ parfois les deux prononciations co-existent
  (ex: "fluide, huer, lueur, scier")
+ de façon générale, même quand une voyelle est censée être courte,
  il est tolérable de la prononcer longue (articulation exagérée);
  par ailleurs, la prononciation évolue dans le temps.

Cependant il existe des cas où la longueur d’une voyelle
est obligatoire pour distinguer des mots qui sinon seraient homophones
(ex: "ouille/oui, paye/pays, abeille/abbaye, pied/piller, pie/pille, faille/failli…")
C’est pourquoi on dédie une lettre `{y}` à noter le son court `[j]`,
et on réécrit `{-il, ill}` en `#{(i)y}` selon les cas,
plutôt que toujours en `#{i(ï)}`.

Le principe est que de dans une séquence de plusieurs (semi-)voyelles,
on précise la longueur de chacune d’entre elles sauf la 1re.

- AY: "abeille" --> #"abeiye"
- AYA: rayon" --> #"raiyon"
- YA: "ciel" -?-> #"cyel" (superflu)
- YAY: il "piaille" --> #"piaye" -?->  #"pyaye" (superflu)
- YAYA: "piailler" --> #"piayer" -?-> #"pyayer" (superflu)
- YIYA: "cuillère" --> #"cuiyère" -?-> #"cuihère / cui:ère / cuière"
  (on peut considérer que dans `[ija]`,
  le `[j]` n’est qu’un effet secondaire du `[i]` qui précède,
  et qui déborde un peu sur la voyelle qui suit)
- YIYA: "tuyau" -> #"tuiyau" -?-> #"tuihau / tui:au / tuiau"
- IYA: "sillage" --> #"siyage" -?-> #"sihage / si:age" mais pas #"siage"
- AA: "abbaye" --> #"abaiïe"
- remarque: "cahier" --> #"cayer"



### V/W

La lettre `{w}` produit en général la semi-voyelle `[w]`,
mais dans des emprunts germaniques elle produit la consonne `[v]`.

Parfois les deux prononciations co-existent (ex: "welter, wisigoth, éwé").

Pour des mots intégrés au français,
qui ne sont pas spécifiques à la culture germanique,
on peut simplement réécrire ça en `{v}`.
Ça concerne essentiellement "wagon".

> **très facile:**
> + "wagon" --> "vagon"



### MN

Dans quelques mots, `{mn}` se prononce `[n]`.
Liste complète: "automne+, damner+, condamner+".

La raison est étymologique,
le `{m}` ne s’entend dans aucun mot apparenté(?).

Supprimer le `{m}`, suivant ainsi la prononciation.

> **très facile:**
> + `{mn}` ~ `[n]` --> `{n}`



### Lettres muettes

Une difficulté importante de l’orthographe actuelle est les consonnes muettes:

+ consonnes étymologiques complètement muettes en milieu de mot
  - (ex: "(C) arctique, (D) poids(!),
    (G) vingt, doigt, longtemps, sangsue, amygdale,
    (H) silhouette,
    (L) fils, gentilhomme, pouls, (M) automne,
    (P) sept, corps, sculpture, piedmont, (S) c’est, mesdames, vosgien,
    (T) montgolfière, (X) auxquels")
  - et même en fin de mot! (ex: "monsieur")
+ consonnes étymologiques muettes en fin de mot,
  qui peuvent s’entendre en liaison ou dans des dérivés
  - (ex: "(B) plomb, (C) blanc, croc, (D) grand,
    (F) nerf, cerf, bœufs, (G) long, sang, (L) outil,
    (P) trop, coup, camp, (S) mas, (T) c’est, vingt,
    (Z) nez, riz, (RS) gars, (CT) respect, (PT) exempt…")
+ consonnes morphologiques muettes en fin de mot,
  qui peuvent s’entendre en liaison
  (ex: `{-s}` ou `{-x}` du pluriel, `{-nt}` verbal, terminaison `{-er}`)

La réforme de 1990 en ajoute en soudant des mots composés
(ex: "rondpoint")…

**vaste chantier:**
déterminer au cas par cas quelles lettres étymologiques
sont vraiment utiles et correctes(!),
et jeter les autres;
éventuellement, ajouter certaines lettres muettes
si cela permet de gagner en cohérence.
p.ex:

- `{c}` dans "arctique", `{m}` dans "automne", `{p}` dans "sculpture"…
  sont inutiles car ne s’entendent dans aucun mot apparenté
- `{g}` dans "vingt", mot élémentaire,
  ne s’entend que dans "vigésimal", mot très pointu !
  (évite l’homographie "vint / vingt", mais cf section "20":
  on pourrait utiliser une diacritique distinctive,
  ou réécrire le verbe #"veint")
- `{h}` dans "silhouette", muet de toute façon puisque c’est un `{h}`,
  je ne sais même pas pourquoi il est là (du basque "xiloeta")
- `{t}` dans "montgolfière" est pire qu’inutile, il est hors-sujet
  car ce mot n’a aucun rapport avec "mont + golfier"
- "piedmont" est certes dérivé de "pied",
  mais ce n’est pas une info très intéressante…
- "vosgien" devrait s’écrire #"vôgien"
- "est" devrait s’écrire #"êt"
- …
- `{d}` dans "poids" est une erreur !
  (évite l’homographie "pois / poids", mais cf diacritique distinctive,
  et par ailleurs "poids" pourrait être réécrit #"poiz", cf section -S)

#### Liaisons

L’orthographe actuelle n’indique pas les liaisons de l’oral.
Le faire semble une très mauvaise idée,
car beaucoup trop complexe
(très peu de règles générales),
et instable
(liaisons facultatives
dépendant du locuteur,
du registre de langue,
d’éventuelle hypercorrection…).
Ce serait trop lier l’écrit aux détails de l’oral.

Préciser les liaisons ne semble pas nécessaire
car une mauvaise oralisation des liaisons
(oubli d’une liaison obligatoire,
ou réalisation d’une liaison interdite)
empêche rarement la compréhension d’un discours.

Remarque de méthodologie
pour les modifications proposées ailleurs dans ce document:
la possibilité d’une liaison
est parfois la seule raison de conserver une consonne muette finale.
C’est pourquoi,
*si on veut se cantonner à réformer l’orthographe et non la langue elle-même*,
on ne peut pas supprimer des consonnes muettes sans discernement.
Il faudrait examiner au cas par cas
si la liaison est réalisable et réalisée en pratique,
avec quelle fréquence…

Exemples:

- Peut-on supprimer `{-s}` dans "corps" au singulier ?
  Cette lettre ne fait pas partie de la racine étymologique
  mais est issue du suffixe latin générique `{-us}`,
  marquant le nominatif;
  on ne la retrouve pas dans les dérivés ("corporel, incorporer…").
  J’ai l’impression qu’on dit "un cor étranger"
  mais jamais "un cor z’étranger"
  (en revanche on peut dire "des cor z’étrangers"),
  on ne dit jamais non plus "le cor z’et l’esprit";
  ce qui suggère qu’on pourrait supprimer `{-s}` au singulier
  et rendre le pluriel régulier…
  mais on entend `[z]` dans l’expression figée "corps et âme" !
  l’existence de cette expression relativement rare
  suffit-elle à justifier
  le maintien d’un `{-s}` irrégulier dans ce mot élémentaire ?
  On pourrait aussi lexicaliser l’expression
  pour marquer son euphonie
  (l’écrire par exemple #"corp-z-et ame"
  comme on écrit déjà "entre quatre-z-yeux").
  D’après le Littré, le `{-s}` ne se lie pas.

- Peut-on supprimer `{-x}` dans "choix" au singulier ?
  J’ai l’impression qu’on dit "un choi avisé"
  mais jamais "un choi z’avisé"
  (par contre on dit "des choi z’avisés),
  donc peut-être qu’on peut supprimer `{-x}` au singulier
  et rendre le pluriel régulier en `{-s}`…
  Ceci dit la lettre muette permet de retrouver le verbe dérivé "choisir".

cf section singuliers en -S, -X.

#### Diacritique lettre muette

**moyennement facile:**
pour les lettres toujours muettes, en milieu de mot,
mais maintenue pour des raisons étymologiques,
on peut imaginer une nouvelle diacritique «lettre muette»,
qui pourrait se placer sur n’importe quelle lettre et la rendrait muette.
le symbole exact importe peu,
pour les exemples ici on utilise arbitrairement le caron `ˇ`
(circonflexe à l’envers,
rappelle l’emploi du circonflexe pour signaler une lettre disparue;
la brève serait peut-être un meilleur choix,
elle indiquerait un son tellement « bref » qu’il ne se prononce même plus,
par exemple `ă, ğ`…).

- exemples: #"vinǧt, doiǧt, lonǧtemps, fiľs, sep̌t, corp̌s, gařs, aux̌quels"

grâce à cette nouvelle diacritique,
on pourrait envisager de réintroduire des lettres disparues
(pas sûr qu’on veuille vraiment *ça*,
mais ça donne un argument pour se débarrasser du circonflexe
qui apporte la même info de façon moins claire).

- exemples: #"ȟošpital, forešt, išle, ešt"

on peut aussi s’en servir pour marquer les `{h}` non aspirés en début de mot,
si toutefois on décide de les garder (cf section H):

- exemples: #"ȟomme, ȟydravion"

on peut aussi s’en servir sur des voyelles en n-grammes,
ce qui rendrait la lecture phonétique plus simple
(moins de combinaisons possibles,
par exemple il n’y a plus besoin de considérer `{au}` ou `{ain}`
comme des graphèmes)
et permet d’économiser des trémas.
p.ex:

+ `{au}` ~ `[o]` --> `#{ǎo}` (cf section AU)
+ `{eau}` ~ `[o]` --> `#{ěo}` (cf section AU)
+ `{œu}` ~ `[ø / e]` --> `#{ǒeu}`
  (mais plutôt #`{ø}` ou `{eu}`, cf sections EU et Œ)
+ `{ai}` ~ `[E]` --> `#{ǎè}` (cf section AI)
+ `{ei}` ~ `[E]` --> `#{ǐè}` (mais plutôt `{è}`, cf section EI)
+ `{ain}` ~ `[5]` --> `#{ǎin}` ou `#{ǎen}` (cf section voyelles nasales)
+ `{ein}` ~ `[5]` --> `#{ěin}` ou `#{ǐen}`
  (mais plutôt `{en}`, cf section voyelles nasales)
+ `{aon}` ~ `[@]` --> `#{aǒn}` (bizarre)
  (mais plutôt `{an}`, cf section voyelles nasales)
+ `{en}` ~ `[@]` --> `#{ěan}`
  (mais plutôt `{an}` (cf section voyelles nasales)
+ `{eu}` ~ `[ø / e]` --> `#{ǒe}`

voir si les nombreux usages que gagnerait instantanément cette diacritique
valent l’effort d’introduire un nouveau symbole exotique dans l’orthographe,
sachant qu’il n’est pas facile, informatiquement, de trouver une diacritique
qui puisse se mettre sur *toutes* les lettres.

#### Consonnes muettes finales

pas toujours clair d’après la graphie
si des consonnes en fin de mot sont muettes ou prononcées,
et il y a même des homographes non homophones;
p.ex:

- C: "sac, troc, donc, bouc, arc, turc, sec, duc / tabac, croc, tronc, caoutchouc, marc, clerc, porc"
- D: "raid, stand, oued, baroud, fjord / laid, grand, pied, coud, nord"
- G: "gang, gong, boing, erg / sang, long, poing, bourg"
- L: "civil, fioul, nul, calcul / outil, saoul, cul"
- P: "cap, rap, croup, stop / drap, coup, trop"
- R: "fier, hier, cher, ter, super, amer / fier, nier, archer, ôter, souper, aimer, monsieur, gars"
- S: "un as, un alias, ès, herpès, bis, pubis, fils, biceps, plus, bus, sus!, ours, couscous, mœurs, sens, afrikaans
  / tu as, tu allias, tu es, abcès, pain bis, rubis, fils, ceps, plus, je bus, je sus, cours, coucous, cœurs, gens, sans"
- T: "mat, (t)chat, but, kart, appart / mât, chat, il but, quart, part"
- X: "fax / six, dix / prix, paix, voix, flux, -aux, -eux, -oux"
- Z: "gaz, quiz, fez / raz, riz, nez"
- CT: "compact, contact, direct, strict, verdict…" (prononcé) / "exact, abject" (prononcé ou muet) / "aspect, respect, suspect, instinct" (muet)
- PT: "rapt, script, abrupt, concept, transept" (prononcé) / "prompt" (prononcé ou muet) / "exempt, corrompt" (muet) / "sept" ({p} muet mais {t} prononcé !)

si la graphie distinguait les lettres muettes et non-muettes,
non seulement la prononciation serait plus claire,
mais en plus on éviterait de nombreux homographes
(un cas très fréquent, et facile à résoudre:
`{-er(s)}` qui est prononcé soit `[-e]`
soit `[-ER / -9R]`;
p.ex: "fier/fier, scanner/scanner, river/rover,
lacer/laser, éviers/avers, aimer/amer").

état des lieux des consonnes finales
(ou séquences de consonnes finales),
indiquant pour chacune si elle est en général
muette ou sonore,
avec les exceptions notables
(inventaire dressé d’après la base de données Lexique3):

* sonore:

  + H: sonore (n-grammes comme CH, SH, SCH, TH…)
  + J, K, Q: sonore
  + L: sonore
    (y compris suivi par d’autres consonnes,
    p.ex: "self, colt, scalp, feld, talc"
    => entraîne la prononciation de la consonne qui suit, sauf -S)
    - sauf dans "cul, saoul, pouls", certains -IL
  + R: sonore
    (y compris suivi par d’autres consonnes)
    - sauf dans la terminaison -ER quand elle est prononcée `[-e]`
      (infinitifs, noms communs, adjectifs)
      * mais parfois -ER est prononcé `[-ER]` ou `[-9R]`
        (ex: "mer, amer, fer, hiver, vers, tiers, univers, rover, bunker…"
        nombreux emprunts anglais ou germaniques)
    - sauf dans "messieurs, gars"
  + F, V: sonore
    - sauf parfois après R (ex: "nerf, cerf, serf" mais "turf, surf")
    - sauf "œufs, bœufs"
  + M, N: nasalisé ou sonore
    (y compris suivi par d’autres consonnes)
  + B: sonore
    - sauf après M, autrement dit, dans "plomb"

* variable:

  + P: variable
    (y compris dans -PT)
  + C: très variable
    (y compris dans -CT)
  + G: très variable

* muet:

  + S: muet
    - sauf -SS
    - sauf nombreuses exceptions (dont mots latins en -US)
    - prononciation parfois optionnelle (ex: "tandis")
  + Z: muet
  + X: muet
    - sauf exceptions diverses (ex: "fax, index, bombyx, lynx, sphinx")
  + D: muet
    - sauf exceptions diverses (ex: "raid, stand, bled, plaid, barmaid")
  + T: muet
    - sauf certains -CT
    - sauf certains -PT
    - sauf -ST (sauf sauf "il est")
    - sauf -FT (anglicismes)
    - sauf -LT (anglicismes)
    - sauf -TT

propriété toujours vérifiée pour les séquences de consonnes finales:
si une consonne est prononcée, alors celles qui précèdent aussi

- très rares exceptions: "sept, fils"
  => cas d’école de la diacritique lettre muette:
  --> #"sep̌t’, fiľs’"

**proposition:**
définir pour chaque consonne si,
en position finale,
elle est «muette par défaut»
ou «prononcée par défaut».
marquer les exceptions.
énoncer que dans une séquence de consonnes finales,
la prononciation d’une consonne entraine
celle de toutes celles qui précèdent.

+ consonne «sonore par défaut»:
  H, J, K, Q, L, R, F, V, M, N, B(?), P(?), C(?), G(?)
  - M et N sont nasalisées par défaut,
    distinguer avec un tréma quand elles sont prononcées séparément
  + L est «super-sonore»:
    toute consonne qui suit L est prononcée, sauf -S
  - *sauf* R dans -ÉR
    (cf section -R plus loin)
  - (*sauf* B, C, G après une nasale:
    on pourrait définir cette exception mais + complexe…)
+ consonne «muette par défaut»:
  S, Z, X, T, D
  - T est «faiblement muette»:
    elle devient sonore dès qu’elle est précédée
    par S ou par une consonne sonore (non nasalisée), hormis R
  - (pour X, éventuellement, règle plus raffinée:
    cf section -X plus loin)

comment marquer les lettres anormalement muettes?
diacritique lettre muette
(il faut convenir que cette diacritique n’empêche pas
la prononciation en liaison)

(ex: #"plomb̌, pouľs, œuf̌s, nerf̌, croč, blanč, sanǧ, loup̌,
"instinčt, exemp̌t")

comment marquer les lettres anormalement non-muettes?
(il suffit de marquer la dernière lettre)

+ apostrophe `{’}` après la consonne ?
  (ex: #"as’, alias’, herpès’, biceps’, gaz’, fax’, kart’")
  - cohérent avec l’usage actuel de l’apostrophe
    (qui signale une liaison ou une élision,
    p.ex. "appart’, champ’")
  - bizarre quand on ajoute un -S (muet) de pluriel
    (ex: #"des kart’s, des but’s")
+ doublement de la consonne ?
  (ex: #"ass, aliass, herpèss, bicepss, gazz, faxx, kartt")
  - cohérent avec les graphies actuelles
    (ex: "mess, jazz, watt")
  - plus visible, risque moins d’être omis
  - un peu bizarre pour X, mais on s’y fait
  - ok avec un -S de pluriel
  - certaines langues (norvégien ?) semblent employer cette solution
+ plus radical: ajouter `{-e}` ?
  (ex: #"un asse, aliasse, herpesse, , bicepse, gaze, faxe, karte")
  (cf section Marque du féminin)

pour les lettres variables (B, P, C, G),
privilégier le cas où elles sont prononcées améliore
la propriété phonétique de la graphie
et les emprunts étrangers
(cf section compatibilité langues étrangères).
de plus, minimiser l’une des 2 listes de consonnes
(«muettes par défaut» ou «prononcées par défaut»)
facilite la mémorisation.

##### -C

cf section Q/C:
on propose d’écrire `{-q}` quand la consonne est prononcée
et `{-c}` quand elle est muette.

auquel cas `{c}` ne doit pas être prononcée par défaut,
mais muette par défaut.

(affecte quelques mots clés: #"aveq, donq")

##### -L

cf section -IL pour supprimer la majorité des `{-l}` muets.

##### -R

prononcée quasiment systématiquement;
la seule exception est la terminaison `{-er}`
dans l’infinitif des verbes
et dans certains noms et adjectifs (ex: "potager, paysager, évier")…
sachant que certains `{-er}` se prononcent `[-ER]` voire `[-9R]`
(ex: "mer, amer, fier, divers, laser, bunker…",
nombreux anglicismes et germanismes
mais également, on le voit, beaucoup de mots bien français).

ailleurs, on propose de réécrire la terminaison en `#{-ér}`
quand elle est prononcée `[-e]`
(cf section -ER).

il y a aussi les 2 exceptions isolées "messieurs, gars",
qu’il serait pertinent de réécrire,
p.ex. en #"messieux" (cf section pluriels en -X),
#"gařs" (cf section diacritique lettre muette).

=> règle simple:
`{-r}` se prononce toujours, sauf précédé par `{é}`
(ou `#{ē}` si accent plat, cf section accent plat);
la voyelle fermée force la syllabe à être ouverte,
donc pas de consonne prononcée à la fin de la syllabe.

cette règle améliore l’emprunt de mots, notamment anglais,
dont la graphie n’est pas ajustée
(ex: "poker, scanner, laser")
(cf aussi section compatibilité avec langues étrangères).
leur graphie se distingue d’emblée de celle des mots français,
prononcés `[e]`,
pour lesquels il faut ajouter une diacritique,
ce qui résout d’ailleurs des homographies
(ex: "scanner" = l’appareil / #"scannér" = le verbe;
"fier" = orgueilleux / #"se fiér" = faire confiance).

##### -S, -Z, -X

cf section -S, -Z, -X.

lettres à considérer comme muettes par défaut
vu leur fréquence comme marqueurs de pluriel,
ou de conjugaison.

difficulté: la prononciation de certains `{-s}` est optionnelle…

si on veut faire dans le détail, -X est:

+ muet dans les terminaisons `{-aux, -oux, -eux, -oix, -aix}`,
  et dans les mots "prix, perdrix, crucifix, aulx";
+ prononcé dans les autres cas
  (y compris dans les autres mots en `{-ux}`, p.ex. "lux, vélux").

il pourrait être utile de se débarrasser de la liste d’exceptions "prix" etc.
en réécrivant ces mots
(ex: "aulx" --> #"aux" pluriel semi-régulier de "ail",
sachant que "aux" signifiant "à les" s’écrirait #"aus", voire #"àus").

##### -T, -D

comme `{-s}`, muettes par défaut
(terminaison verbale, et nombreux noms ou adj. en `{-et}` p.ex.).



### Singuliers en -S, -X

Beaucoup de mots singuliers se terminent par un `{-s}` ou  `{-x}` muet,
ce qui est embêtant car c’est aussi la marque du pluriel.
Ces mots sont donc identiques au singulier et au pluriel, de façon irrégulière.
On peut s’interroger au cas par cas
s’il est raisonnable de supprimer ce `{-s, -x}` muet au singulier,
afin de rendre ces mots réguliers.

[L’emploi de `{-x}` au lieu de `{-s}`,
dans les terminaisons `{-aux, -eux, -oux}`
et dans divers autres mots (ex: "choix, noix, prix"),
est une complication le plus souvent inutile
(cf section pluriels en -X pour l’explication)
dont on s’occupera plus tard
(cf section -S, -Z, -X).]

Vis-à-vis de la prononciation:
en suivant la méthode exposée précédemment
(cf section liaisons),
il *me semble* qu’il y a une grande tendance:

+ dans la plupart de ces mots, quand ils sont *adjectifs*,
  le `{-s, -x}` produit une liaison en `[z]`
  et s’entend dans le féminin, qui est en `{-s(s)e}` ~ `[z/s]`
  (peut-être que la liaison est influencée par l’existence du féminin ?);
+ quand ils sont *noms*, en revanche,
  il n’y a pas de liaison au singulier;
  on peut toujours en faire une au pluriel,
  c’est en fait la marque régulière du pluriel.

Exemples:

- adj./nom. "preux": "un preux z’adversaire" / "le preux [.] a vaincu la vouivre"
- adj. "valeureux → valeureuse": "un valeureux z’adversaire"
- adj. "jaloux → jalouse": "un jaloux z’homme" (liaison obligatoire !)
- adj./nom. "creux → creuse":
  "un très creux z’argumentaire??"
  / "j’ai un creux [.] énorme, un creux [.] et une bosse"
- adj./nom. "gris → grise":
  "un bien gris z’automne??
  l’automne est gris z’et morne??
  le ciel gris z’et les feuilles mortes??"
  / "le gris [.] et le rouge??"
- adj. "mieux" (invariable): "il va mieux z’et il recommence à parler??"
- adj. "doux → douce": "un doux z’avenir; un temps doux z’et sec??"
- nom. "choix (→ choisir)": "un choix [.] avisé; le choix [.] ou le hasard??"

Ça mériterait une étude lingüistique plus sérieuse !

Finalement, on veut probablement
conserver la plupart des `{-s, -x}` au singulier, car:

+ selon ces observations sur les liaisons,
  il faudrait le garder pour les adjectifs,
  or, un « même » mot est souvent nom et adjectif
  (n’importe quel adjectif peut être converti en nom)
  et il est hors de question de l’écrire différemment
  selon sa nature grammaticale;
+ *la terminaison muette indique comment former les dérivés*
  (ex: "gris → grise, creux → creuse, choix → choisir");
  c’est en fait le rôle de toutes les terminaisons muettes
  (ex: "dent → dentaire, champ → champêtre, sang → sanguin, croc → crochu").
  le seul problème, ce qui rend `{-s, -x}` spécial,
  c’est que c’est identique à la marque du pluriel.

On peut néanmoins réformer certains mots ponctuels
(ex: "relais" est devenu "relai" en 1990;
"vieux" devrait clairement être réécrit, cf section "vieux";
"corps, fonds, legs, rubis…" devraient être réécrits sans `{-s}`,
cf section Mots divers).
Un certain nombre de `{-s}` muets singuliers sont des vestiges
de la terminaison latine `{-us}` indiquant le nominatif,
information grammaticale qui subsistait en ancien français
mais est ensuite devenue obsolète;
cette terminaison ne fait pas partie de la racine proprement dite,
et ne se retrouve donc pas dans les mots apparentés;
il faut donc clairement s’en débarrasser.



### Pluriels en -X

rappel des règles:

* pluriels «réguliers» (obéissent à la règle la plus courante):
  + le pluriel est formé en ajoutant `{-s}`
  + si le singulier se termine par `{-s, -z, -x}`,
    le pluriel est identique au singulier
* pluriels «semi-réguliers» (obéissent aussi à une règle):
  + le pluriel d’un mot en `{-ail}` est souvent `{-aux}`, parfois `{-ails}`
  + le pluriel d’un mot en `{-al}` est souvent `{-aux}`, parfois `{-als}`
  + le pluriel d’un mot en `{-(e)au}` est `{-(e)aux}` **(irrégularité inutile !)**
    - rares exceptions en `{-aus}`: "landau, sarrau" (régulier !)
  + le pluriel d’un mot en `{-eu}` est `{-eux}` **(irrégularité inutile !)**
    - rares exceptions en `{-eus}`: "bleu, pneu" (régulier !)
  + le pluriel d’un mot en `{-ou}` est `{-ous}` (régulier !)
    - les 7 exceptions magiques en `{-oux}`:
      "hibou, pou, chou, genou, joujou, caillou, joujou" **(irrégularité inutile !)**
* pluriels vraiment irréguliers:
  + aucune règle, au cas par cas (ex: "ceux, cieux, yeux, œufs, bœufs, messieurs, bonshommes, os, us…")

bonne blague: différence entre des "lieus communs" et des "lieux communs"?

déduire le singulier à partir d’un pluriel (semi-)régulier:
pas évident car de nombreux mots singuliers se terminent par `{-s}` ou `{-x}`.
les simplifications ci-dessous améliorent ce point pour les mots en `{-x}`,
car alors `{-x}` ne peut plus être une marque de pluriel
(sauf éventuellement dans `{-aux}` si on conservé le pluriel de `{-ail, al}`,
mais par chance les mots non pluriels en `{-aux}` sont très rares:
"faux, taux, chaux").
cette situation peut être encore améliorée
en revoyant les terminaisons `{-s, -x, -z}` de mots singuliers (cf section idoine):
beaucoup de singuliers en `{-s}` ou `{-x}` pourraient devenir des `{-z}`.

complexité inutile (accidentelle):
pour les mots en `{-(e)au, -eu, -ou}`,
le pluriel en `{-x}` fonctionne en fait
exactement comme un pluriel régulier en `{-s}`:
on ajoute simplement une lettre,
muette en isolation, prononcée `[z]` en liaison.
c’est régulier du point de vue de la langue,
l’irrégularité est artificiellement créée par l’orthographe !
et en plus, il y a des exceptions à cette irrégularité…

note culturelle:
d’où vient cette complication inutile et bizarre ?

1. au Moyen-Âge (du temps de Charlemagne, 8e siècle),
   sans papier et sans imprimerie,
   l’écriture était couteuse en argent (parchemin) et en temps;
   on a donc redoublé d’astuces pour faire des économies.
   on s’est mis à écrire plus petit (naissance des lettres minuscules),
   tout en attaché, et à utiliser plein d’abréviations.
   or, à l’époque, on écrivait encore essentiellement du latin,
   langue où la terminaison `{-us}` est extrêmement fréquente.
   on a donc introduit un petit symbole pour abréger cette terminaison,
   qui est devenu une lettre `{-x}`.
2. on a continué à utiliser cette *abréviation* pour le français;
   ainsi le pluriel régulier "totaus" (si ce mot existait à l’époque)
   se serait écrit "totax".
3. plusieurs siècles plus tard (15e siècle),
   avec l’arrivée du papier et de l’imprimerie,
   ces économies de bouts de chandelles sont devenues inutiles
   et on a promptement «défait» toutes ces abréviations.
   sauf que, semble-t-il, on avait un peu oublié leur signification…
   en tout cas on a réinséré un `{-u}` avant le `{-x}`,
   mais en gardant `{-x}` ! d’où "totaux".
4. encore un peu plus tard,
   on (l’Académie, sauf erreur de ma part) a voulu corriger ça en partie,
   et on donc décrété que
   le pluriel des mots en `{-ou}` s’écrivait à nouveau `{-ous}`…
   sauf qu’exactement 7 mots ont échappé à la rectification;
   pourquoi ? étymologie désuète, simple oubli ? personne ne sait vraiment.
   en tout cas, on a décrété que c’était des exceptions !
   et c’est pourquoi, au 21e siècle,
   tous les écoliers apprennent par cœur cette liste à la Prévert: "chou, hibou…".
5. pourquoi les pluriels de `{-(e)au}` et `{-eu}` n’ont pas été rectifiés ?
   je ne sais pas, mais peut-être que
   les terminaisons `{-aux}` et `{-eux}` étaient perçues comme plus uniformes
   avec les autres mots existants ?
   (il y a beaucoup de mots singuliers qui s’écrivent `{-eux}`,
   p.ex: "creux, preux…").
   les exceptions ("landau, pneu…")
   sont des mots arrivés plus tard dans la langue.
6. *en bref:* il s’agit d’un accident historique
   qu’on ne veut sûrement pas continuer à subir au 21e siècle.
   L’Académie elle-même, en 1908, a voulu supprimer les pluriels en `{-oux}`.

éliminer cette complexité inutile
en écrivant systématiquement le pluriel régulier avec un `{-s}`:

> **très facile:**
> + `{-(e)aux}` pluriel de `{-(e)au}` --> `{-(e)aus}` sans exception
> + `{-eux}` pluriel de `{-eu}` --> `{-eus}` sans exception
> + `{-oux}` pluriel de `{-ou}` --> `{-ous}` sans exception

plus fort:
il est un tout petit peu moins évident
qu’on veuille remplacer `{-x}` par `{-s}` pour les autres pluriels semi-réguliers,
à savoir `{-ail, -al}`:
dans ces cas, le mécanisme du pluriel est réellement différent,
et donc il pourrait être utile de les signaler.

- avec la graphie "landaus", sachant que c’est un mot au pluriel,
  on déduit que le singulier est "landau";
- avec la graphie "travaux", sachant que c’est un mot au pluriel,
  on déduit que le singulier est soit "travail" soit "traval",
  mais en tout cas pas "travau".

mais ce n’est pas si une info très puissante…
si on utilisait `#{-aus}` pour le pluriel de `#{-ail, -al}`,
la situation ne serait pas terriblement pire:

- avec la graphie "landaus" (resp. #"travaus"),
  sachant que c’est un mot au pluriel,
  on déduit que le singulier est "landau" ou "landal" ou "landail"
  (resp. "travau" ou "traval" ou "travail").

remarque importante:
par chance il n’y a qu’un seul mot en `{-aus}` non pluriel: "blockhaus"
(où le `{-s}` est prononcé et serait donc écrit différemment
dans le cadre de notre réforme de l’orthographe, cf section -S)

> **facile:**
> + `{-aux}` pluriel de `{-al}` --> `{-aus}`
> + `{-aux}` pluriel de `{-ail}` --> `{-aus}`

**idée** supplémentaire pas très bien réfléchie:
systématiser un peu l’usage de `{-x}` pour signaler un pluriel irrégulier ?
p.ex: "ceux, cieux, #œux, #bœux, #messieux, #ox (pluriel d’un os), #ux (pluriel d’un us)…"

**plus généralement**, de nombreux mots pas forcément pluriels s’écrivent
`{-aux, -eux, -oux}` plutôt que `{-aus, -eus, -ous}` pour la même raison,
et sont donc à régulariser de la même façon:
cf section "peux, veux, vaux",
section "deux",
et pour une approche systématique, section -S, -Z, -X.



### -S, -Z, -X

cf aussi section S/Z.

inventaire des utilisations de `{-s}`, `{-z}`, `{-x}` en position finale
(À VÉRIFIER/COMPLÉTER):

_LÉGENDE: type de terminaison ; \[prononciation isolée, en liaison, dans mots dérivés\] ; occurrences_

* terminaisons grammaticales (muettes):
  + type **V** (verbal):
    - `{-s}` verbal ; `[--, z, --]` ; 1re et 2e pers. du sing. et du plur.
    - `{-z}` verbal ; `[--, z, --]` ; 2e pers. du plur. `{-ez}`
  + type **P** (pluriel):
    - `{-s}` pluriel ; `[--, z, --]` ; plur. régulier
    - `{-x}` pluriel ; `[--, z, --]` ; plur. semi-régulier de `{-ail, -al, -(e)au, -eu, -ou}`
* autres terminaisons muettes:
  + type **Ms**:
    - `{-s}` muet qui donne `[s]` en dérivé ; `[--, z, s]` ; p.ex. bras, gros, souris…
    - `{-x}` muet qui donne `[s]` en dérivé ; `[--, z, s]` ; p.ex. faux, poix, paix, doux…
  + type **Mz**:
    - `{-s}` muet qui donne `[z]` en dérivé ; `[--, z, z]` ; p.ex. poids, bois, gris, ras, français, chinois…
    - `{-x}` muet qui donne `[z]` en dérivé ; `[--, z, z]` ; p.ex. choix, époux, creux, deux…
    - `{-z}` muet ; `[--, z, z]` ; assez, chez, nez, rez, riz, raz (liste exhaustive)
  + type **Mx**:
    - `{-x}` muet qui donne divers sons en dérivé ; `[--, z, k/s/z]` ; p.ex. croix (s/z), voix (k/z), prix (s/z)…
* terminaisons sonores:
  + type **Ss**:
    - `{-s}` prononcé `[s]` ; `[s, s, s/z]` ; p.ex. as, os, us, pathos, sinus, sens…
    - `{-x}` prononcé `[s]` ; `[s, s/z, z]` ; six, dix, coccyx (liste exhaustive)
  + type **Sz**:
    - `{-z}` prononcé `[z]` ; `[z, z, z]` ; p.ex. gaz, quiz, jazz (très rare)
  + type **Sx**:
    - `{-x}` prononcé `[ks]` ; `[ks, ks, ks]` ; p.ex. fax, apex, latex…

détail des formes verbales:

- 1re pers. sing.: `{-s, -is, -ais}`
- 2e pers. sing.:  `{-s -es, -is, -sses, -ais}`
- 1re pers. plur.: `{-ons -^mes}`
- 2e pers. plur.:  `{-ez -^tes}`

d’une part on veut simplifier
en écrivant de la même manière toutes les terminaisons du même type,
qui font la même chose (ou presque).
d’autre part, on veut rendre l’orthographe plus éclairante
en écrivant différemment, dans la mesure du possible, les types différents;
au minimum, il faut distinguer les terminaisons sonores de celles muettes.
plus fort:
il serait intéressant que l’orthographe permette de distinguer à coup sûr
les terminaisons grammaticales (marque de pluriel, conjugaison)
de celles qui font partie de la racine des mots.

#### -S, -Z, -X sonores

(cf section consonnes muettes finales)
on peut marquer que la consonne finale est sonore,
par exemple en suffixant le mot par une apostrophe `{’}`:

> **facile, alternative 1/2:**
> + type Ss --> #`{-s’}`
> + type Sz --> #`{-z’}`
> + type Sx --> #`{-x’}`

alternative plus visible et qui évite de surcharger l’apostrophe:
doubler la consonne:

> **facile, alternative 2/2:**
> + type Ss --> #`{-ss}`
> + type Sz --> #`{-zz}`
> + type Sx --> #`{-xx}`

remarque: s’applique à tous les mots latins en `{-us}`
dont, jusqu’à présent, on gardé la graphie à l’identique.

remarque: "six" et "dix" sont très spéciaux,
cf section "6, 10".

indiquer les consonnes finales non muettes,
non seulement clarifie la prononciation,
mais en plus évite certaines homographies,
et indique aussi que la terminaison
n’est pas une marque de pluriel ou de conjugaison.

#### -S, -X du pluriel

une première étape facile est de
réduire le nombre de pluriels en `{-x}`
en les réécrivant tous ou une grande partie en `{-s}`
(cf section pluriels en -X).
si on conserve certains pluriels en `{-x}`.
alors `{-s}` indique un pluriel régulier
et `{-x}` un pluriel semi-régulier.

#### -S, -Z verbaux

pas de raison de distinguer la 2e personne du pluriel avec une lettre `{-z}`
(qui ne marque ni la 2e personne, ni le pluriel,
puisque à la 2e personne du singulier
comme à la 1re personne du pluriel
on trouve `{-s}` et jamais `{-z}`).
la graphie `{-ez}` sert à ce que le `{e}` soit prononcé `[e]`,
mais on propose par ailleurs d’utiliser un accent aigu/plat (cf section -EZ).

on peut donc employer la même lettre pour toutes les terminaisons verbales
en remplacement de `{-s}` et `{-z}`.
disons `{-z}`, pour distinguer un verbe conjugué d’un mot au pluriel:

> **envisageable:**
> + `{-s}` verbal --> `#{-z}` (ex: "je #suiz, tu #vaz, nous #ironz, tu #mangez, tu #prendz")
> + `{-ez}` verbal --> `#{-éz}` (puis `#{ēz}` si accent plat) (ex: "vous #mangéz")

il faut alors décider que,
en position finale, le `{-z}` se comporte désormais comme un `{-s}`;
c’est-à-dire qu’il est muet par défaut (même après une consonne, comme dans `{-dz}`),
et que dans `#{-ez}` le `{e}` ne se prononce pas `[e]`,

une irrégularité à corriger: cf section "peux, veux, vaux"

#### Autres -S, -Z, -X muets

pour commencer, on peut essayer de supprimer cette terminaison muette
dans certains mots (cf section singuliers en -S, -X; section mots divers).

pour les mots qui restent, on peut rationaliser
en adoptant toujours la même terminaison `{-s}` pour le type Ms
et `{-z}` pour le type Mz;
autrement dit:

+ on réécrit `{-s}` muet en `{-z}`
  quand le son des mots dérivés est `[z]`;
+ et on réécrit `{-x}` en `{-s}` ou `{-z}`,
  selon les dérivés.

le type Mx est à priori plus épineux
car les dérivés d’un même mot manifestent plusieurs sons différents
(ex: "croix → croiser [z], crucial [s];
voix → voiser [z], vocal [k];
prix → priser [z], précieux [s]");
cependant
on peut considérer que certains dérivés sont « directs »
tandis que d’autres sont des « déformations savantes »
comme il en existe déjà tant dans la langue
(ex: "cheval → cavalier; chaud → calorie, œil → oculaire");
on classe alors les mots dans le type Ms ou Mz
en fonction de leurs dérivés directs
(ex: "croix, voix, prix" seraient du type Mz),
et on les réécrit en accord avec ce classement.
(je n’ai pas trouvé de mot en `{-x}`
dont tous les dérivés manifesteraient le son `[k]`;
s’il en existe, il faudrait les réécrire en `{-c}`.)

> **moyennement facile:**
> + `{-s}` du type Mz --> `{-z}` (ex: "#poidz, #boiz, #griz, #françaiz")
>   - et `{s}` --> `{z}` dans les mots dérivés (ex: "#pezer, #boizé, #grize, #françaize")
> + `{-x}` du type Ms --> `{-s}` (ex: "#faus, #pois, #pais, #dous")
>   - éventuellement, `{c/ç}` --> `{ss}` dans les mots dérivés
>     (ex: "#faussille, #passifique, #dousse, #doussatre…")
>     (cf aussi section C/S)
> + `{-x}` du type Mz --> `{-z}` (ex: "#choiz, #épouz, #deuz, #croiz, #voiz, #priz")
>   - et `{s}` --> `{z}` dans les mots dérivés
>     (ex: "#choizir, #épouze, #deuzième, #croizer, #voizer, #prizer")

ces changements améliorent les relations entre mots de la même famille,
en régularisant la façon de passer par exemple du masculin au féminin
(ex: comparer "gros → grosse, gris → grise, doux → douce, époux → épouse"
à "gros → grosse, #griz → #grize, #dous → #dousse, #épouz → #épouze").

remarque: ces changements rendent beaucoup plus courante la terminaison `{-z}`,
qui jusqu’ici était assez rare en dehors des verbes conjugués.
il faut clarifier que `{-z}` est muet par défaut
(cf section consonnes muettes finales).
en outre, ces changements remplacent des `{s}` par des `{z}`
en milieu de mot (dans les mots dérivés, p.ex: #"épouze");
il faut le faire de façon plus systématique,
sans quoi le choix entre `{s}` et `{z}` pour le son `[z]`
serait une nouvelle difficulté
(cf section S/Z).

#### Distinguer racines et terminaisons grammaticales

on pourrait vouloir distinguer à coup sûr
un mot singulier se terminant par `{-s, -z, -x}`
d’un mot avec une marque de pluriel `{-s, -x}`
ou d’un verbe conjugué.

intérêts:
supprime des homographies,
meilleure info grammaticale,
permet de savoir à coup sûr qu’un mot en `{-s}` est un pluriel
et d’en déduire son singulier.

dans certains cas, l’emploi actuel d’un `{-x}` au lieu de `{-s}`
sert à éviter une homographie avec une forme verbale qui n’a rien à voir
(ex: "prix/pris (prendre), choix/chois (choir),
croix/crois (croire), voix/vois (voir)").
distinguer les terminaisons grammaticales résout ce problème.

il suffit d’inventer une graphie différente pour les deux situations
(consonne grammaticale / autre consonne muette),
par exemple une diacritique à ajouter sur la consonne.
ici on utilise un petit point sur la consonne: `#{-ṡ, -ż, -ẋ}`.

réserver la graphie sans diacritique
à la situation la plus fréquente / générale,
c’est-à-dire le cas d’une consonne grammaticale
(en effet:
consonne grammaticale = phénomène général et systématique,
qui concerne même des mots très importants tels que "les, des, ses…";
consonne de racine = pour un ensemble fini (certes grand !) de cas particuliers).

ex: "#groṡ, #poidż, #voiẋ ou #voiż";
"je #voiz / j’ai la #voiż cassée";
"je #choiz / un choiż avisé";
"j’ai #priz le bus / le juste #priż".

inutile par contre de laisser la diacritique dans les mots dérivés,
à n’utiliser que sur une lettre finale.

il peut se justifier d’utiliser plutôt la diacritique lettre muette
(cf section diacritique lettre muette),
mais alors il faut préciser que cette diacritique
(1) indique une lettre muette *étymologique*
(car les `{-s, -z, -x}` grammaticaux sont muets aussi…),
et (2) n’empêche pas la lettre d’être prononcée en liaison.

#### Résumé

si on applique toutes les modifs suggérées dans cette section, on obtient ça:

> * terminaisons grammaticales:
>   + terminaison verbale --> `#{-z}`
>   + marque du pluriel --> `#{-s}` ou (éventuellement) `#{-x}`
> * autres terminaisons muettes:
>   + type Ms --> `#{-ṡ}`
>   + type Mz --> `#{-ż}`
>   + type Mx --> `#{-ẋ}`
> * terminaisons sonores:
>   + type Ss --> `#{-s’}` ou `#{-ss}`
>   + type Sz --> `#{-z’}` ou `#{-zz}`
>   + type Sx --> `#{-x’}` ou `#{-xx}`

#### Alternative plus simple, moins informative

la proposition ci-dessus est hautement informative
au sujet de la morphologie des mots et des apparentés entre mots.
c’est intéressant pour la *lecture*,
en particulier pour des locuteurs non-natifs,
qui apprennent la langue écrite en même temps que la langue orale.

en revanche,
pour des locuteurs natifs qui ont appris l’oral avant l’écrit,
ça peut rendre l’*écriture* plus difficile
car il faut réfléchir si un mot est du type Ms, Mz ou Mx.
ce n’est pas une difficulté nouvelle,
car on ne fait qu’appliquer plus systématiquement
le grand principe de l’écriture du français concernant les consonnes finales
(il faut déjà se demander si un mot se termine
par `{-s}`, `{-t}`, `{-d}`, `{-p}`…),
mais ça accroît peut-être la difficulté existante.

une alternative plus simple pour l’écriture,
mais qui confond plus de choses:

> * terminaisons grammaticales:
>   + terminaison verbale --> `#{-z}`
>   + marque du pluriel --> `#{-s}` dans tous les cas
> * autres terminaisons muettes:
>   + type Ms, Mz, Mx --> `#{-x}` dans tous les cas
> * terminaisons sonores:
>   + type Ss --> `#{-s’}` ou `#{-ss}`
>   + type Sz --> `#{-z’}` ou `#{-zz}`
>   + type Sx --> `#{-x’}` ou `#{-xx}`

ainsi,
`#{-s}` indique toujours un pluriel
mais ne précise pas comment obtenir le singulier quand la terminaison est `{-aus}`,
et `#{-x}` indique toujours un mot « irrégulier »
mais ne précise pas comment trouver les mots dérivés.

bonus: dans cette situation, on peut maintenant décider que
le `{-s}` du pluriel s’ajoute même aux mots en `{-x}`,
qui deviennent réguliers !
il faut seulement préciser que la terminaison `{-xs}` reste muette.

les règles du pluriel deviennent finalement beaucoup plus simples:

+ le pluriel est formé en ajoutant `{-s}`
+ le pluriel d’un mot en `{-ail}` est souvent `{-aus}` plutôt que `{-ails}`
+ le pluriel d’un mot en `{-al}` est souvent `{-aus}` plutôt que `{-als}`
+ certains mots ont un pluriel irrégulier, au cas par cas
  (ex: "ceux, cieux, yeux, œufs, bœufs, messieurs, bonshommes, os, us…")



### La valse des consonnes

K, Q, C, S, Z, X, G, J, H et même L, T:
quand la moitié des consonnes de l’alphabet sont ambigües ou redondantes,
c’est peut-être qu’il est temps de revoir l’alphabet…

#### Consonnes alternantes

Quatre consonnes changent de prononciation selon le contexte:

+ `{x}` selon le contexte (règles assez complexes)
+ `{s}` selon s’il est entouré ou non de voyelles
+ `{c}` et `{g}`
  selon s’il est suivi d’une voyelle «douce» (`{e, i, y}`) ou «dure» (autre).
  - `{c}` fait aussi partie du digramme `{ch}`
  - `{g}` fait aussi partie du digramme `{gn}`

De plus,
les règles déterminant quel son est produit
ne s’appliquent pas systématiquement
et on a souvent besoin de forcer l’une des deux prononciations.
Pour `{c}` et `{g}`, on a les graphèmes suivants
(force dur / alterne / force doux):

+ `{qu / c / ç}` ; plus `{ch}`
  - `{qu}` aussi devant des consonnes dures
    (ex: "quoi, qualité, j’arquai")
  - `{qu}` en doublon avec `{k}` (mots étrangers)
  - `{ç}` en doublon avec `{s(s)}` (autre étymologie)
+ `{gu / g / ge}`
  - `{gu}` aussi devant des consonnes dures,
    dans des verbes conjugués (ex: "fatiguant")
  - `{ge}` en doublon avec `{j}` (mots étrangers… mais pas que)

On utilise la même lettre avec des sons différents
pour des raisons étymologiques / de parentés entre mots,
mais est-ce encore pertinent ?
Autrement dit, a-t-on actuellement des familles de mots
où la même consonne transcrit deux sons différents ?

Le fait qu’il existe une façon de forcer chacune des deux prononciations
indique qu’en fait, les quatre combinaisons sont possibles
(consonne dure/douce devant voyelle dure/douce),
donc la règle d’alternance n’est peut-être pas si adaptée que ça…

En tout cas, les digrammes `{qu}`, `{gu}`, `{ge}` sont ambigus
et il faut donc régler leur sort
("qualité/équateur, fatiguant/guano, rongeur/rongeure"; cf sections QU et GU).

#### G/J

+ occurrences de l’alternance `[g / Z]`: assez courant  
  (`!` indique que l’un des deux sons n’est de toute façon
  pas transcrit par un `{g}` simple):
  - "frigo / réfrigérer"
  - ! "gambader / jambe"
  - ! "garrot / jarret"
  - "légal / légiste"
  - "obligatoire / obligé"
  - "cargaison / charge"
  - "dérogation / déroger"
  - "longueur / allonger"
  - ~ "a**c**tion / agir"
  - ~ "da**c**tylo / digital" (éventuellement, lointain)
  - …

Cette alternance semble suffisamment fréquente pour être conservée.

En fait, quels sont les problèmes qu’on pourrait souhaiter résoudre ?

 1. il faut choisir entre `{g(e)}` et `{j}` pour noter le son `[Z]`
 2. le digramme `{ge}` est problématique
    + => à priori le seul cas problématique est la succession de lettres `{geu}`
      comme dans "rongeure, gageure", qu’un tréma suffit à résoudre
 3. le digramme `{gu}` est problématique
    + => il suffit de le réécrire `{gh}` (cf section GU)
 4. les mots d’emprunt dont la graphie n’est pas modifiée
    (cf section compatibilité langues étrangères)
    peuvent enfreindre la règle de prononciation de `{g}`
    pour produire le son `[g]` avant voyelle douce
    (ex: "burger, magyar, geisha, yogi, gestapo, hégélien, groggy, boggie")
 5. dans certains mots d’emprunt, `{g/j}` se prononcent `[dZ]`
    ou même `[j]`
    (problème moins important, même problème que `{ch}`)

Le problème des mots d’emprunt ne peut pas être ignoré:
certes il suffit de réécrire ces mots conformément à leur phonétique,
mais le fait est que de nombreux mots étrangers
sont employés sans modification graphique.

Le problème du digramme `{ge}` pourrait éventuellement être résolu
en remplaçant ce digramme par une version diacritée de la lettre G,
analogue au C cédille…
par exemple le G cédille `#{ģ}`
(ou le G caron `#{ǧ}` qui est employé de façon similaire en tchèque).
Mais nouveau symbole, difficile à saisir au clavier,
et difficile à justifier vu la rareté du problème avec `{ge}`
et vu qu’il existe déjà la lettre J…

Tous les problèmes listés (sauf celui des prononciation exotiques)
sont résolus si on supprime l’alternance
et qu’on garde les graphies `{gh}` et `{j}`:

> `{gh / -- / j}`

(et la lettre `{g}` seule ne se rencontrerait que dans des graphies d’emprunt)

Remplacer `{g}` dur par `{gh}` rallonge certaines graphies.
Il ne serait pas absurde
d’écrire simplement `{g}` plutôt que `{gh}` pour le son `[g]`,
mais ça signifie changer la valeur d’une lettre existante,
incompatible avec l’orthographe actuelle
(cf aussi section QU/C pour le même argument à propos de la lettre C).

La lettre J en français a des origines très diverses,
elle vient autant de I latins initialement prononcés `[j]`,
que de G latins dont la prononciation est devenue `[Z]`,
que d’emprunts récents ou anciens à toutes sortes de langues
y compris le franc…
On ne peut donc pas, contrairement à K, W, Z,
la considérer comme marquant les mots « étrangers ».
Puisque certaines attestations correspondent déjà à un G latin
(ex: "jambe, jarret"… cas rares ?),
il n’est peut-être pas choquant de consacrer J comme variante douce de G.

… Je ne sais pas si supprimer l’alternance
améliore la graphie du français en théorie,
mais en tout cas c’est un changement lourd
qui ne répond pas à un besoin pressant
(peu de fautes en pratique dûes à une confusion entre G et J).

Une bizarrerie à noter: les prénoms "Jean, Jeanne", et leurs dérivés.

#### CH/C

+ occurrences de l’alternance `[k / S]`: courant
  - "cavalier / cheval"
  - "canin / chien"
  - "carne / chair"
  - "carriole / charriot"
  - "cargaison / charge"
  - "campagne / champ"
  - "cantique / chant"
  - "calvitie / chauve"
  - "camisole / chemise"
  - "sac / sachet"
  - "sec / sécher"
  - "roc / rocher"
  - "duc / duché"
  - ! "évêque / évêché"
  - ! "acquisition / achat"
  - …

Il est clair qu’on veut garder le son `[S]` associé à la lettre `{c}`.

#### Q/C

+ occurrences de l’alternance `[k / s]`  
  (`!` indique que l’un des deux sons n’est de toute façon
  pas transcrit par un `{c}` simple):
  - ! "-ique / -icien, -icité"
  - ! "opaque / opacité"
  - ! "réciproque / réciprocité"
  - "caduc / caducité"
  - "turc / turcique"
  - "Québec / québécisme"
  - ! "franque / français" (dur devant doux / doux devant dur !)
  - "compact / compacité"
  - "rancœur / rance"
  - "fraction / façon" (parenté lointaine)
  - …

L’alternance semble surtout courante à travers le son `[-k]` en fin de mot
qui devient `[s]` quand suffixé par `{-ien, -ité, isme}` par exemple;
et dans la plupart de ces cas,
il n’est de toute façon pas écrit par la lettre `{c}` mais par `{qu}`,
car suivi d’un schwa !
Les autres instances sont des parentés plus lointaines et pas si utiles.

Dans ces conditions
(à moins que j’aie raté beaucoup d’occurrences, ce qui est fort possible),
il semble que l’alternance ne soit pas si intéressante.

De plus le choix de la graphie à utiliser est ambigu
car `{q}` est aussi employé devant des voyelles dures.
En effet, actuellement la lettre `{q}` semble avoir plusieurs usages:

+ forcer la prononciation dure d’un `{c}` devant une voyelle douce
  (ex: "arc → arquer; caudal → queue");
+ info étymologique ancienne:
  marquait en latin le phonème prononcé `[kw]`, qui s’écrivait `{qu}`
  (cf note culturelle dans section QU),
  auquel cas `{q}` peut aussi se trouver devant une voyelle dure
  (ex: "quoi, qualité, quotient, équateur");
+ … parfois utilisé sans raison claire
  (ex: "rorqual", mot d’origine norvégienne).

Cf note culturelle dans la section QU:
Le latin n’avait pas de bonne raison d’écrire `{qu}` plutôt que `{cu}`,
l’info étymologique n’est pas intéressante,
et on a tout intérêt à assimiler `{q}` à `{c}`.

Ci-dessous deux propositions de simplification;
elles réalisent toutes deux les propriétés suivantes:

+ le choix des lettres à écrire
  est purement mécanique, sans ambigüité;
+ `{q}`, comme `{ç}`,
  n’est plus à considérer que comme une variante graphique de la lettre `{c}`,
  et non comme une lettre séparée de l’alphabet
  (incidence sur la comparaison de mots, l’ordre alphabétique, etc.);
  on pourrait même imaginer la remplacer par une variante diacritée de `{c}`…
  mais ce serait une modif aussi lourde qu’inutile.

Remarque:
On voit dans la co-existence des suffixes `{-que}` et `{-c}`
que `{qu}` est actuellement employé de façon inconsistante pour forcer `[k]`.
Il serait raisonnable de réécrire systématiquement
les suffixes `{-que}` en `{-c}` au masculin,
mais on se refuse à le faire ici car on ne veut pas modifier la langue.
Ceci dit, selon les propositions ci-dessous,
ces suffixes deviendraient respectivement `#{-qe}` et `#{-q}`,
et il est possible (boule de cristal) qu’une évolution future de la langue
ré-interprète `{e}` dans le 1er cas comme la marque habituelle du féminin,
et donc l’enlève au masculin (effet Buben)
(cf section Marque du féminin).

Remarque:
À propos d’effet Buben,
on peut imaginer (tarot divinatoire) que sous l’influence de la graphie,
après modifications proposées,
les dérivations futures de mots en `{-q(e)}`
ne produiraient plus le son `[s]` mais conserveraient le son `[k]`.

##### Proposition radicale

Proposition:
supprimer la consonne dont la prononciation alterne,
ne garder que ses variantes qui forcent la prononciation:

> + `{qu}` --> `#{q}` (cf section QU)
> + `{c}` dur --> `#{q}`
> + `{c}` doux --> `{ç}`

Les graphèmes relatifs à C deviennent alors:

> `{q / -- / ç}` ; plus `{ch}`

Il peut sembler bête de supprimer complètement le symbole `{c}` simple
(qui n’apparaitrait plus que dans `{ç}` et dans `{ch}`),
pourquoi ne pas l’utiliser à la place de `{q}`, ou de `{ç}` ?
Car pour des raisons de compatibilité avec l’orthographe actuelle,
il vaut mieux éviter de changer la valeur d’une lettre existante.
Peut-être dans quelques siècles,
si cette proposition est adoptée maintenant,
décidera-t-on de simplifier `{ç}` en `{c}`…

##### Proposition plus parcimonieuse

La modif précédente est visuellement très lourde
car elle remplace toutes les lettres `{c}`
par `{ç}` ou pire, par `{q}`
(ex: #"çéçité, çétaçé, qontaqt, qalqul, éqlairçir, aqçès").
Une alternative beaucoup plus parcimonieuse
est de conserver la lettre `{c}` dont la prononciation alterne,
et de réserver les graphies `{q}` et `{ç}`
au fait de forcer la prononciation dure/douce d’une lettre C
devant une voyelle douce/dure.

Il faut décider si le son `[-k]` en fin de mot
doit être écrit `{-c}` (comme dans "sac, lac…")
ou `{-q}` (comme dans "cinq, coq").
En principe `{-c}` suffirait, mais cette lettre
est susceptible d’être muette en position finale
(cf section consonnes muettes finales).
Donc on préfère `{-q}`.
Bénéfice collatéral:
on réduit le problème des consonnes finales muettes ou non-muettes
(ex: "des #laqs de montagne / un entrelacs").

> + `{qu}` devant voyelle dure --> `{c}`
> + `{qu}` devant voyelle douce --> `{q}` (cf section QU)
> + `{-c}` ~ `[k]` --> `{-q}` (y compris si suivi d’un `{-s}` de pluriel…)

(ex: #"coi, calité, cotient, écuateur").

#### K/Q/C

Les lettres `{k}` et `{q}` (variante dure de `{c}`) sont redondantes
en ce qu’elles produisent le même son `[k]`.
La différence est que
`{q}` est réservé à des racines latines ou du moins anciennes
(ou à des graphies de mots étrangers francisés il y a longtemps),
quand `{k}` est employé pour des emprunts plus exotiques/récents.

La question qui se pose est:
est-il utile/souhaitable de maintenir cette distinction.
Je ne sais pas.

Les langues germaniques font un usage systématique de `{k}`.

#### Autres façons d’écrire le son [k]

+ graphies principales:
  - `{c}`
  - `{qu}`
  - `{k}`
  - `{ch}` (cf section lettres grecques)
+ graphies un peu spéciales:
  - `{cc}` (ex: "accoucher, accalmie, accorder, occasion, occuper, sirocco")
  - `{cqu}` (liste assez complète: "acquérir, acquitter, acquiescer, becquée, grecque, Jacques")
  - `{ck}` (ex: "ticket, cocktail, rock, flash-back…" emprunts anglais)
+ graphies très exotiques:
  - `{cch}` (dans "saccharose, ecchymose, bacchanale, macchabée")
  - `{kk}` (dans "drakkar, akkadien, trekkeur")
  - `{g}` (dans "tungstène")
  - `{kg}` (dans "ginkgo")
+ graphies qui produisent plusieurs sons dont `[k]`:
  - `{x}`

On peut supprimer au moins les graphies exotiques:

+ `{cch}`: précision étymologique grecque extrêmement superflue
  (indique qu’en grec ancien cela il y avait 2 consonnes, `{κχ}` ou `{κκ}`),
  réécrire `{ch}` puis appliquer éventuelle modifications des lettres grecques;
  ou bien, vu que ces mots sont déjà suffisamment reconnaissables,
  réécrire tout simplement en `{c}`
+ `{kk, g, kg}` ~ `[k]` --> `{k}` (graphies d’emprunt)
  (cf aussi section consonnes doubles)

##### CC, CQU

(`{cqu}` est la variante de `{cc}` forçant la prononciation dure devant voyelle douce.)
Indique en général la composition de deux éléments
(dont, souvent, le préfixe latin `{ac-}`;
cf section état des lieux des consonnes doubles).

Info peu intéressante, à ramener à `{c/q}`.

> + `{cc}` ~ `[k]` --> `{c}`
> + `{cqu}` --> `{q}`

#### C/S

Quant à la redondance entre `{c, ç}` et `{s, ss}`:
elle est *en général* étymologique et distingue de nombreux mots
(dont: "ça/sa, ce/se, c’est/s’est"),
on n’essaie pas d’y toucher dans le cas général
(c’est ballot: "ce/se" est de très loin
l’une des fautes les plus courantes en pratique…
mais qu’y peut-on ?
c’est plus qu’une faute d’orthographe, une faute de grammaire;
peut-être, peut-être,
après réduction de la complexité accidentelle de l’orthographe,
aurait-on dégagé suffisamment de temps d’enseignement
et de cerveau disponible chez les élèves
pour mieux leur faire assimiler la grammaire, qui sait ?).

Certaines familles dont la racine contient `{c/x}`
font apparaître la lettre `{-s}`
(ex: "tiers → tierce;
souris → souriceau;
radis → radicelle, radical")
ou bien `{-x}` qu’on propose par ailleurs (cf section -S, -Z, -X) de réécrire `{-s}`
(ex: "paix → pacifique; doux → douce").
Que faire de ces mots ?
Les régulariser avec `{s}` améliore la cohérence de la famille,
mais brouille un peu l’étymologie (relation avec les autres langues).

Même en dehors des consonnes finales,
l’étymologie n’est pas toujours respectée.
Certaines familles mélangent `{c/s}` de façon incohérente,
et devraient donc être rectifiés.
Par exemple, dans "gars / garçon" (du franc),
le `{c}` n’a aucune justification, à remplacer par `{s}`.
À l’inverse, "chausser, chaussure / caleçon" (d’une racine latine en `{c}`)
devraient s’écrire avec la même consonne, logiquement `{c}`.
On peut soupçonner qu’à l’instar de "chausser",
de nombreux `{c}` latins sont devenus `{s}` dans l’orthographe française…

#### S/Z

+ occurrences de l’alternance `[s / z]` : assez rare  
  (`!` indique que l’un des deux sons n’est de toute façon
  pas transcrit par un `{s}` simple):
  - le suffixe `{-isme}` admet les deux prononciations
  - ! certains `{-s}` muets prononcés `[z]` en liaison
    mais `[s]` dans mots dérivés (cf section -S, -Z, -X)
  - "sonner / résonner" (la formation "ré+sonner" remonte au latin)
  - "signer / résigner" (idem)
  - "consoler / désoler" (?)
  - "solitude / isolé" (lien para-étymologique)
  - "poste / position"
  - "saurien / dinosaure"
  - "sinus / sinusoïdal"
  - ! "mission / mise" (et dérivés: "émission, permission, soumission…")
  - ! "fassions / faisions"
  - ! "précieux / priser"
  - ! "six, dix / sixième, dixième"
  - ! "rationnel / raison"
  - ! le doublet de suffixes "-ation / -aison" ? (ex: "dérivation / dérivaison) (ancien)
  - …

Le cas de S/Z est plus simple que Q/C/Ç et GU/G/J.
L’alternance est rare
et, de plus, dans beaucoup de cas,
le son `[s]` n’est de toute façon pas écrit par la lettre `{s}`…

De plus, la règle qui détermine le son est problématique
dans au moins deux cas:
- le doublement `{ss}` qui force le son `[s]`
  n’est pas toujours fait dans les mots relativement récents
  (ex: "parasol, vraisemblable, asymétrique, resituer/résidu, désensibiliser/désosser, dysenterie/dysurie")
  et, s’il est fait après un schwa, il indique une mauvaise prononciation
  (ex: "dessus, dessous, ressource, ressac, ressembler, ressemeler, ressentir/pressentir")
  (cf consonnes doubles).
- `{s}` précédé d’une voyelle nasale:
  phonologiquement il est entouré de voyelles
  mais graphiquement il est précédé par une consonne `{n}`,
  quelle est alors la prononciation ?
  (ex: `[s]` dans "sensé, transe";
  `[z]` dans "transit";
  flottant dans "censitaire").

Par ailleurs, le principe que les lettres `{s}` et `{z}` sont sœurs
ne semble pas difficile à comprendre et à enseigner.
Il y a même une ressemblance graphique entre ces deux lettres.

Il y a un précédent: la paire `{f / v}` (ex: "tardif / tardive").

Par conséquent, on propose de réécrire systématiquement
`{s, ss}` en `{s}` ou `{z}` selon la prononciation.

> + `{s}` ~ `[s]` --> conservé
> + `{ss}` ~ `[s]` --> `{s}`
> + `{s}` ~ `[z]` --> `{z}`

La lettre `{s}` peut laisser un peu de latitude dans la prononciation,
selon le locuteur
(ex: suffixe `{-isme}`, "censitaire").

Il faut garder en mémoire qu’une finale `{-s}` muette
se prononce `[z]` en liaison.

C’est aussi l’occasion de régler le problème de `{sh}` prononcé `[z]`:
cf section SH.

Il faut aussi s’occuper des `{-s}` finaux muets,
sans quoi les dérivations seraient bizarres
(ex: "mis → #mize; français → #françaize; Paris → #parizien");
cf section -S, -Z, -X.

Une objection serait qu’actuellement la lettre Z, comme W et K,
permet de marquer les barbarismes exotiques
pour les distinguer des bonnes racines gréco-latines bien de chez nous.
C’est un comble que notre système d’écriture rende plus simple
l’écriture desdits mots exotiques que celle des mots typiques.

#### Autres façons d’écrire le son [s]

Il existe en fait de nombreuses graphies qui produisent le son `[s]`:

+ graphies principales:
  - `{s}`
  - `{ss}`
  - `{c}`
  - `{ç}`
+ graphies un peu spéciales:
  - `{sc}`
  - `{sç}` (ex: "acquiesça")
  - `{<t>i}`
+ graphies très exotiques:
  - `{cc}` (liste exhaustive: "succion" — car la prononciation `[ks]` co-existe)
  - `{x}` (liste exhaustive: "Bruxelles, Auxerre, coccyx, six, dix")
  - `{th}` (liste exhaustive: "forsythia, thriller")
  - `{sth}` (liste exhaustive: "asthme, isthme")
+ graphies qui produisent plusieurs sons dont `[s]`:
  - `{x}`
  - `{xc}` (liste assez complète: "excès, excéder, excellent, exceller, excentré, exception, excepter, excision, exciter")
  - `{xs}` (liste exhaustive: "exsangue, exsuder" — car "ex+mot")
  - `{<xt>i}` (liste exhaustive: "immixtion, admixtion" — car "mixture")

On aimerait réduire cette profusion…

Pour les graphies principales:
cf sections Q/C et S/Z.

Pour les graphies exotiques:
la plupart de ces cas particuliers ont une justification,
voir si on veut les supprimer ou pas…
Cependant:

+ pour `{x}` ~ `[s]`: à réécrire avec un `{s}`
  - "Bruxelles, Auxerre" sont des noms propres, ayons peu d’espoir,
    bien qu’ils s’écrivent avec `{s}` dans d’autres langues…
  - "coccyx" --> #"cocciss" par exemple (cf section -S, -Z, -X)
  - "six, dix" sont spéciaux (cf section "6, 10")
+ `{xc}` et `{xs}`: force la prononciation `[ks]` plutôt que `[gz]` du `{x}`;
  en général issu du préfixe `{ex-}` ajouté à un mot en `{c-}` ou `{s-}`;
  cependant pour la plupart des mots listés en `{xc}`
  (à part "ex-centré, ex-cision")
  cette étymologie ne semble pas particulièrement intéressante aujourd’hui;
  réécrire en `{cc}` ?
+ `{<xt>i}`: cas particulier de `{<t>i}` (cf section TI) précédé de `{x}`;
  ce serait donc à réécrire `{xc}` voire `{x}`.

##### SC, SÇ

La graphie `{sc, sç}` qui produit `[s]`
est purement étymologique et remonte au latin.
(ex: "adolescent, luminescent, évanescent, susciter, susceptible, scélérat, scission, science, scène, fascisme, absisse, transcender, discerner").
Dans quelques cas,
cette succession de lettres est le résultat de
la composition d’un préfixe en `{-s}` avec une racine en `{c-}`
("ex: "trans-cender, dis-cerner"),
mais en général elle n’apporte pas de telle information,
ou alors ce n’est plus utile
(ex: "adolescent, évanescent, science, abscisse, acquiescer").

Il ne semble pas non plus y avoir de cas
où ce `{sc}` serait prononcé `[sk]` dans un mot apparenté,
ce qui justifierait la graphie.

Donc réécrire ce digramme en `{c}`
(sauf éventuellement exceptions pertinentes):

> + `{sc}` ~ `[s]` --> `{c}`
> + `{sç}` --> `{ç}`

##### TI

La graphie `{ti}` peut produire
le son `[si]` (ex: "acrobatie, hématie, initie, inertie, ineptie, balbutiement")
ou `[sj]` (ex: "émotion, action, éruption, nationalisation, initiation, initial, nuptial, martial, démentiel, dalmatien, martien, patient, minutieux, minutieusement, potentialité, satiété, actionner, spartiate").

Note: on ne trouve de `{t}` prononcés `{s}` que devant `{i}`.

On le voit, cette prononciation spéciale
n’est pas réservée à un petit nombre de terminaisons
mais peut se trouver au milieu de mots,
en particulier à cause de mots dérivés,
donc difficilement prévisible
(ex: comment se prononcent "antienne, orthodontie, orthoptie, infectiologie" ?).

Elle sert à rappeler une étymologie en `{t}`,
qu’on peut parfois entendre dans d’autres mots de la même famille
(ex: "acrobatie → acrobatique, ineptie → inepte, émotion → émotif, minutieux → minute").

Sauf que:

+ de nombreux mots n’ont pas d’apparentés où on entendrait `[t]`
  (ou alors c’est difficile à trouver)
  - "balbutiement, initial, nuptial, martial, spatial,
    essentiel, différentiel, patient…"
  - le suffixe `{-tion}` est prolifique de ce point de vue, p.ex:
    "sensation, situation, condition, convention, subvention,
    intervention, considération, perdition, déperdition, superstition, munition,
    caution, précaution, portion, proportion, sanction…"
  - "martial, martien": de "Mars" avec un `{s}`, le `{t}` est parachuté !
  - graphie trompeuse:
    "contention → contenir" et non "contenter" !
    "rétention → retenir" et non "retenter/retendre" !
+ même quand il existe un mot de la même famille avec le son `[t]`,
  il peut être difficile à trouver, car peu connu ou lointain
  (ex: "substantiel → substantif, substantifique moelle (!); pestilentiel → pestilente,
  augmentation → augmentatif, amélioration → (a)mélioratif,
  insinuation → insinuatif, continuation → continuateur,
  faction → factice, nation → natal, ablution → ablutophobie (!)")
  - souvent la parenté n’est qu’indirecte
    (ex: "conviction → éviction → évicter, indignation → digne → dignité,
    (as,con,ré)signation → signature,
    (con,sub,inter)vention → prévention → préventif,
    (pré,pro)position → poste")
+ s’il faut chercher tous les mots de la famille pour savoir comment ça s’écrit,
  on n’est pas sorti de l’auberge
+ parfois le même son `[s]` est noté différemment
  dans d’autres mots de la même famille
  (ex: "spatial/spacieux(!) ← espace;
  différentier/différencier(!!!) ← différence;
  essentiel ← essence; démentiel ← démence; séquentiel ← séquence")
+ l’alternance `[s/t]` n’est pas signalée dans d’autres cas
  (ex: "suspicion → suspecte, connexion → connecter,
  science → scientifique, -ence → -ente, -ance → -ante,
  raccourcir → courte")
+ la graphie `{ti}` est en concurrence avec plusieurs autres,
  ce qui induit en confusion lors de l’écriture
  et est responsable de nombreuses fautes d’orthographe en pratique:
  - la graphie `{ci}`, qui évoque cette fois une racine en `[s]` ou `[k]`,
    est fréquente dans des contextes similaires
    (ex: "paramécie, spécial, social, logiciel, technicien")
  - la graphie `{tion}` est en concurrence avec `{(s)sion}`;
    celle-ci indique une racine en `{d}`
    (ex: "torsion → tordre, suspension → suspendre, (ap,com)préhension → prendre,
    cession → céder, scission → scinder, décision → décider, évasion → évader",
    et aussi "défense → défendre, réponse → répondre, morsure → mordre, accès → accéder")
    et peut aussi produire le son `[z]`
    (ex: "évasion, érosion, décision, division, persuasion")…
    mais:
    * `{d}` est très proche de `{t}`, leurs 2 sons sont associés
      (ex: "entendre → entente; attendre → attentif")
    * `{tion}` est parfois utilisé
      alors que la racine est `{d}`
      - "prétention → prétendre" (alors que "tension → tendre")
    * au contraire, `{sion}` est parfois utilisé
      alors que les principaux apparentés sont en `{t}` !
      - "discussion → discuter, taper la discute" ! (et vice-versa: "conversation → converser" ?)
      - "mission, mise → mettre" ! (et dérivés: "émission, permission, omission…")
  - la graphie `{cti}` est en concurrence avec `{xi}`
    (cf section CTI/XI)
+ ça crée une ambigüité de prononciation entre `[t]` et `[s]`,
  tel qu’il est difficile de deviner la prononciation attendue
  (ex: "soutien / tahitien; retient / patient; incitiez / initiez; quartier / partiel");
  terminaisons particulièrement problématiques:
  - `{-tie}`
    (à cause de participes passés)
    (ex: "bâtie / acrobatie; sentie / différentie;
    partie / partiel; démentie / démentiel")
  - `{-tions}`, qui engendre de nombreux homographes non homophones
    (à cause de la terminaison verbale `{-ions}`):
    (ex: "nous portions nos portions, rations, options, acceptions, mentions, éditions, dictions…" [homonymes.txt](./homonymes.txt))
    (cf aussi: -ENT)

Pour résumer, on a *généralement* les graphies suivantes:

 1. `{c}` pour le son `[s]` issu d’une racine latine `{c}` ;
 2. `{c}` pour le son `[s]` issu d’une racine latine `{t}`… sauf quand suivi de `{i}` ;
 3. `{t}` pour le son `[s]` issu d’une racine latine `{t}` quand suivi de `{i}` ;
 4. `{s}` pour le son `[s/z]` issu d’une racine latine `{d}`

Il semble nettement préférable,
premièrement, d’utiliser la même graphie pour les racines `{t}`
quelle que soit la voyelle qui suit;
et deuxièmement, d’utiliser une graphie qui reflète la prononciation `[s]`,
donc pas `{t}`.
Il y a donc deux options pour noter le son `[s]` issu d’un `{t}`:

+ soit toujours `{c}`;
  ceci supprime la redondance entre `{ti}` et `{ci}`,
  est étymologiquement cohérent,
  et c’est ce qui est fait en espagnol par exemple;
  - cette modif est plus légère car il faut juste remplacer `{ti}` par `{ci}`,
    les autres occurrences sont déjà notées par `{c}`;
  - après application de cette modif,
    il faudra se demander s’il y a un `{d}` dans la famille ou pas,
    pour savoir s’il faut écrire `{c}` ou `{s}`;
+ soit toujours `{s}`;
  ceci supprime la redondance entre `{tion}` et `{(s)sion}`,
  et met donc ensemble des racines similaires en `{t}` ou `{d}`;
  il y a des liens entre `{t}` et `{d}`
  (ex: "entendre → entente");
  de plus, dans la graphie actuelle, quelques racines en `{t}`
  sont déjà associées à des graphies en `{s}` prononcées `[s]`
  (ex: "nation → natal → naissance"),
  ou même `[z]`
  (ex: "rationnel → raison");
  - cette modif est plus lourde car en plus de remplacer `{ti}` par `{(s)si}`,
    il faut aussi remplacer de nombreux `{c}` par `{s(s)}`;
  - après application de cette modif,
    il faudra se demander s’il y a un `{t/d}` dans la famille ou pas,
    pour savoir s’il faut écrire `{c}` ou `{s}`.

Même après application d’une de ces deux mesures,
la situation resterait pénible
car il resterait la concurrence non évidente entre `{c}` et `{s}`.
Distinction peu utile
puisque l’info étymologique qu’elle fournit est de toute façon confuse !
Le plus raisonnable semble donc, en fait, d’unifier *toutes* ces graphies;
soit en `{s}` (ce qui est étymologiquement mauvais),
soit en `{c}`
(auquel cas on ne touche pas aux `{s}` prononcés `[z]` issus d’un `{d}`;
ceci dit, on peut les réécrire en `{z}`,
un peu comme l’espagnol "_razón_" et italien "_nazione_").

Tout réécrire en `{c}` apparait clairement comme le plus raisonnable.

> **alternative tout-C (la meilleure):**
> + `{t}` ~ `[s]` --> `{c}`
> + `{s(s)}` ~ `[s]` associé à `{d}` dans la famille --> `{c/ç}`
> + *optionnellement:* `{s}` ~ `[z]` associé à `{d}` dans la famille --> `{z}`
>
> **alternative T→C:**
> + `{t}` ~ `[s]` --> `{c}`
>
> **alternative T→S:**
> + `{t}` ~ `[s]` --> `{s(s)}`
> + `{c}` associé à `{t}` dans la famille --> `{s(s))}`

Remarque: alternative idéaliste mais peu pragmatique
vu les claviers actuels (qui sont nuls de toute façon):
réécrire plutôt avec un t cédille: `#{ţi}`…
c’est exactement ce qui est fait en roumain
(avec anciennement aussi le d cédille `{ḑ}`),
pour la même raison,
et ça a été [proposé pour le français][t-cédille]
il y a déjà plusieurs siècles,
notamment par Ambroise Firmin-Didot.
On peut juger de la justesse de ces mots écrits il y a 250 ans:

> Quand il en coûte si peu, pour rémédier à des imperfections ;
> c’est vouloir gratuitement les éterniser, que de les laisser
> subsister.  
> ~ abbé de Petity, 1766

[t-cédille]: https://fr.wikipedia.org/wiki/Cédille#Le_t_cédille_en_français

##### CTI/XI

Cas particulier de `{ti}`,
on trouve la terminaison `{-ction}` prononcée `[ksj§]`
(ex: "fiction, détection, action, correction, extinction, réduction, satisfaction").
Or, on trouve également la graphie `{-xion}`,
dans un nombre de mots plus réduit
(liste assez complète: "connexion, annexion, flexion (+ inflexion, réflexion…), complexion, crucifixion").

Du point de vue des mots apparentés,
il n’y a pas de motif clair qui rassemblerait tous les mots en `{-ction}`
et les distinguerait des mots en `{-xion}`:

+ mots en `{-ction}`:
  - "friction → fro**tt**er"
  - "satisfaction → satisfaire"
  - "élection → élire, éle**ct**eur"
  - "réduction → réduire, rédu**ct**eur"
  - "extinction → éteindre, extin**ct**eur"
  - "jonction → joindre, disjon**ct**eur"
  - "insurrection → insurger"
  - "action → agir, a**ct**if"
  - "direction → diriger, dire**ct**e"
  - "correction → corriger, corre**ct**e"
  - "objection → obje**ct**er"
  - "fiction → fi**ct**if"
  - "circonspection → circonspe**ct**e"
  - "inspection → inspe**ct**er"
  - "infection → infe**ct**er", infectieux, infectiologie
  - "détection → déte**ct**er"
  - "concoction → conco**ct**er"
+ mots en `{-xion}`:
  - "connexion → conne**ct**er, connexe"
  - "annexion → annexer, annexe"
  - "(ré,in)flexion → (ré,in)fléchir, flexible, réflexif, réfle**ct**eur"
  - "complexion → compliquer, complexe"
  - "crucifixion → crucifier" (pas d’apparenté avec `{x}` prononcé !)

Le seul motif semble être que
les mots en `{-xion}` admettent au moins un apparenté en `{x}`,
mais ils ont aussi des apparentés avec une variété d’autres consonnes…
De plus, encore une fois,
s’il faut chercher tous les mots de la même famille
pour savoir comment ça s’écrit, on n’est pas tiré d’affaire.

Si on généralise à `{cti / xi}`,
c’est encore moins clair:

+ autres mots avec `{xi}` + voyelle:
  - "galaxie → gala**ct**ique" (racine `{ct}`, pas d’apparenté avec `{x}` prononcé !)
  - "chiropraxie → chiropra**ct**eur"
  - "prophylaxie → prophyla**ct**ique"
  - "apoplexie → apople**ct**ique"
  - "asphyxie, anorexie…"
  - "dyslexie → lexique, le**ct**ure"
  - "orthodoxie → do**ct**e" ? (coïncidence étymologique)
  - "anxiété"
  - "axiome"

La graphie #"connection" est une «faute» très fréquente,
par analogie ("correction"),
déduction depuis le verbe "connecter",
et influence de l’anglais (car en anglais, c’est bien comme ça que ça s’écrit).
Difficile de la condamner comme illogique !

Il apparait plus raisonnable d’unifier ces deux graphies.
Soit en `#{-ccion}` ou `#{-csion}`
(conformément à la réécriture de `{ti}`, cf section TI)
soit en `{-xion}`.
La première option fonctionne mieux
avec les dérivés prononcés `[kt]` (`{cc → ct}`),
mais elle rend bizarre la dérivation `{cc → x}`
(ex: "#compleccion → complexe").
Il semble obscur que le même son `[ks]`
s’écrive de deux façons différentes
selon s’il est suivi par un `{i}` ou non.
La graphie avec `{x}` fonctionne mieux de ce point de vue,
et applique le principe qu’une lettre X
corresponde à un peu n’importe quoi en dérivé
(cf aussi section -S, -Z, -X).
La graphie avec `{x}` semble donc meilleure.

Si c’est la graphie avec `{x}` qui est retenue,
il faut réécrire `{ct}` en `{x}` dans tous les mots où ça se prononce `[ks]`,
pas uniquement dans les occurrences de la forme `{ction}`
(heureusement il y a peu d’autres occurrences:
"factieux, infectieux, infectiologie").

> + *soit* `{xion}` --> `#{ccion}` ou `#{csion}` (selon option retenue pour réécrire `{ti}`)
>   - à la possible exception de "annexion" dont tous les dérivés semblent être en `{x}`
> + *soit* `{ct}` ~ `[ks]` --> `#{x}`



### Mots composés

Bon, les mots composés… c’est réellement compliqué.
Je ne prétends pas les simplifier magiquement.

#### Tiret

La tendance actuelle (voir réforme de 1990) est à vouloir souder les mots.
La soudure se justifie quand le mot a acquis un sens propre,
différent de celui signifié par la composition de ses parties,
et qu’on le pense comme un tout indivisible
(ex: "rentrer" ne signifie plus "ré-entrer").
Mais quand ce n’est pas le cas
(ex: "ressortir" signifie bien "re-sortir"),
ça n’est pas une simplification de l’orthographe,
au contraire:
ça complique la reconnaissance des parties isolées
donc le devinement du sens,
et ça introduit plein d’irrégularités graphie–prononciation
(lettres muettes en milieu de mot, p.ex. "rondpoint";
création accidentelle de digrammes, p.ex. "déshonneur, coexister";
`{s}` simples prononcés `[s]`, p.ex. "dysenterie, resituer";
`{e}` prononcés `[°]` devant plusieurs consonnes, p.ex. "ressortir, restructurer").

Argument supplémentaire en faveur du tiret:
les possibilités de combinaison étant très nombreuses et en fait systématiques
(`{re-}` peut se mettre devant n’importe quel verbe),
les signaler par un tiret facilite la tâche des correcteurs orthographiques.

Il semble donc judicieux de ré-introduire le tiret
au minimum pour éviter des mauvaises prononciations
(ex: "co-exister, re-sortir, re-situer, re-structurer, inter-action").

Ça pourrait être fait de façon systématique pour certains préfixes
comme `{re-}` et `{dé(s)-}`.

Ceci dit, souder des mots composés a un avantage :
faciliter l’écriture du pluriel,
là où, avec plusieurs composantes séparées par des espaces ou des tirets,
on ne sait jamais trop quelle(s) composante(s) il faut accorder en nombre
(ex: le pluriel de "rondpoint" ne peut être que "rondpoints",
alors que celui de "rond-point" pourrait être "rond-points" ou "ronds-points").

En réalité, il faut tolérer les variations avec ou sans tiret,
et ne pas s’obséder pour *la* graphie normative…



### Mots divers

Indépendamment des changements systématiques
proposés ailleurs dans ce document,
il y a de nombreux mots ponctuels qui se prêtent
à des rectifications peu contestables
ou à diverses simplifications.

(Pour chaque modification listée ci-dessous,
il faut évidemment l’appliquer aussi
aux autres flexions et mots de la même famille.)

> + la plupart des **changements de 1990**
>   (consonnes doubles, trémas,
>   correction de bizarreries comme "asseoir"
>   et d’anomalies comme "nénuphar, relais"…);
>   deux points méritent *à mon avis* discussion:
>   - suppression de certains circonflexes (cf section Circonflexe):
>     beaucoup de circonflexes sont gratuits et à bazarder sans état d’âme,
>     mais certains maintiennent des liens entre mots apparentés
>     (ex: "île → insulaire, isolé; goût → gustatif");
>     cependant, l’accent circonflexe étant identifié
>     comme l’une des difficultés majeures de l’orthographe actuelle,
>     il parait presque impératif de lui régler son compte;
>   - soudure de mots composés (cf section Mots composés).
> + **ANOMALIES DIVERSES:**
>   - **"et" --> #"é"** (cf section "et")
>   - **"est" du verbe "être" --> #"êt"** (cf section "et")
>     + puis éventuelle modif concernant le circonflexe
>   - **"j’ai eu, nous eussions, nous eûmes…"
>     --> #"j’ai u, nous ussions, nous ûmes…"**
>     (cf section Formes du verbe "avoir" avec le son `[y]`)
>     + puis éventuelles modifs concernant les terminaisons verbales
>   - **"œil" --> #"œuil"** (cf section CUEIL)
>     + puis éventuelles modifs concernant `{œu}`, `{-il}`
>   - **"second, seconde…" --> #"segond, segonde…"**
>     (même écrits avec `{g}` ces mots restent bien reconnaissables)
>   - **"wagon" --> #"vagon"** (cf section V/W)
>   - **"piqûre, (1990) piqure" --> #"piqüre" ou #"picure"**
>     (façon normale de dissocier 2 lettres via tréma, comme dans "aigüe",
>     *ou* alternance normale `{q/c}`, comme dans "pic";
>     il est particulièrement déroutant que les 2 graphies autorisées
>     soient les graphies illogiques !)
>   - **"bonbonne" --> #"bombonne"**
>     (dérivé de "bombe, bombé",
>     façon normale de noter une nasale avant `{b}`,
>     comme "trombone, pomponne, combo, rembobine…"
>     la graphie actuelle ne semble exister que par analogie avec "bonbon";
>     cf section M avant B/P)
>     + puis dé-doublement de la consonne double
> + **SUPPRESSION DE -S/-X FINAUX:**
>   - **"hors, dehors" --> #"hor, dehor"**
>     (`{-s}` toujours muet,
>     ne se retrouve pas dans le dérivé "hormis",
>     ni dans les autres langues: italien "fuori", espagnol "fuera")
>   - **"velours" --> #"velour"**
>     (`{-s}` muet absent des apparentés "velouté, velvet, velu")
>   - **"cours, concours, discours, encours, parcours, recours, secours" -->
>     #"cour, concour, discour, encour, parcour, recour, secour"**
>     (`{s}` muet absent de la plupart des apparentés, dont les plus directes:
>     "courir, coureur, concourir, concurrent, discourir, encourir,
>     parcourir, recourir, secourir, secourisme, secourable"
>     et même "curriculum, chasse à courre";
>     *cependant* ce `{s}` est étymologique
>     et se trouve dans certains dérivés:
>     "course, cursus, discursif, récursif, incursion, excursion",
>     mais pour des mots comme "parcours, secours"
>     la parenté n’est qu’indirecte)
>     + l’homographie avec "la cour" est évitée
>       si on ajoute la marque du féminin dans ce dernier mot
>       (cf section Marque du féminin)
>   - **"(un) mets" --> #"met" ou #"mès"**
>     (`{t}` et `{s}` sont tous les 2 étymologiques,
>     `{t}` rappelle clairement "mettre" (comme "mettre la table"),
>     `{s}` rappelle "mess, messe" (service de table, d’église)
>     mais ça ne saute pas du tout aux yeux à cause du `{t}`,
>     de plus on dirait un pluriel,
>     il faut choisir)
>   - **"jars" singulier --> #"jar" ou #"jard" ou #"jarg"**
>     (`{-s}` toujours muet,
>     anti-étymologique;
>     `{-d}` possible pour faire la terminaison usuelle `{-ard}`
>     plutôt que la terminaison rare `{-ar}`;
>     `{-g}` possible (quoique obscur) car dérivé "jargauder"
>     et rapprochement avec "jargon")
>   - **suffixe `{-is}` au singulier --> `#{-i}` ?**
>     (noms communs masculins sans forme féminine
>     ni autre apparenté construit sur ce suffixe;
>     *cependant* le `{-s}` distingue ce suffixe
>     d’autres mots où le `{-i}` final fait partie de la racine,
>     comme "cri, repli, abri, relai, loi")
>     + p.ex: #"abatti, cliqueti, croqui, fouilli, gâchi, gazouilli, logi…"
>   - **"lilas" singulier --> #"lila"**
>     (`{-s}` muet,
>     comme le prénom "Lila"
>     et tous les autres noms botaniques "mimosa, pétunia, hortensia…";
>     le `{-s}` muet ne correspond pas à la racine arabe,
>     et fait décidément trop penser à un pluriel;
>     cf aussi section "accord des couleurs")
>   - **"vieux" singulier --> #"vieu"** (cf section "vieux")
>   - **"anchois" singulier --> #"anchoi"**
>     (`{-s}` muet non étymologique, apparenté "anchoïade")
>   - **"rubis" singulier --> #"rubi"**
>     (***`{-s}` muet même en liaison,
>     non étymologique,
>     vestige d’une terminaison du nominatif,
>     absent des apparentés***:
>     "rubicond, rubéole")
>   - **"volontiers" --> #"volontier"** (idem)
>   - **"corps" singulier --> #"corp"** (idem; cf section Liaison)
>     + puis éventuelle modif concernant la consonne finale muette
>     + voir comment marquer l’euphonie dans "corps z’et âme"
>   - **"temps" singulier --> #"temp" ?** (idem; cf section "temps")
>     + et dérivés: "longtemps, entretemps, printemps…"
>     + puis éventuelle modif concernant la consonne finale muette
>     + voir comment marquer l’euphonie dans "de temps z’en temps, de temps z’à autre"
>   - **"lacs, entrelacs" singulier --> #"lac, entrelac"**
>     (idem; apparentés "lacer, entrelacer")
>     + puis éventuelle modif concernant la consonne finale muette
>       (évite l’homographie avec "lac")
>   - **"legs" singulier --> #"leg"**
>     (idem; l’histoire de ce mot est intéressante)
>     + puis éventuelle modif concernant la consonne finale non-muette
>   - **"fonds, tréfonds" singulier --> "fond, tréfond"** (idem;
>     distinction artificielle entre "fond" et "fonds"
>     qui sont étymologiquement le même mot,
>     cf remarque dans le Littré)
>   - *… certainement plein d’autres `{-s}` de nominatif*
> + **AJUSTEMENT D’AUTRES CONSONNES FINALES:**
>   - **"intérêt" --> #"intérès"**
>     (le `{t}` est moins étymologique que le `{s}`
>     et c’est ce dernier qui se retrouve dans tous les apparentés:
>     "intéressant, intéresser, désintéressé, intéressement",
>     espagnol "_interés_", portugais et italien "_interesse_";
>     similaire à "progrès → progresser";
>     différent de "prêt → prêter; forêt → forestier")
>     + puis éventuelle modif concernant `{-s}`
>   - **"appétit" --> #"appétis"** (car "appétissant", pas d’apparenté avec `{t}`)
>     + puis dé-doublement de la consonne double
>     + puis éventuelle modif concernant `{-s}`
>   - **"repas" --> #"repât" --> #"repat"**
>     (car apparentés "repaître, appâter, pâture", du latin _pastus_,
>     il est bizarre qu’on ait gardé le `{s}` plutôt que le `{t}`;
>     ressemblance trompeuse avec "repasser, trépas")
>     + voire --> #"repa"
>       (mot extrêmement basique tandis que le plus proche apparenté "repaître"
>       est rare et soutenu)
>   - **"appas" --> #"appât"**
>     (idem; variante orthographique inutile de "appât" pour certains sens,
>     c’est le même mot !)
>     + puis dé-doublement de la consonne double et suppression du circonflexe
>   - **"différend" --> #"différent"** (aucune raison à cette petite fantaisie)
>     + puis éventuelle modif concernant `{-ent}`
>   - **"puits" --> #"puis"**
>     (car "puiser, épuiser, puisatier" et même "Dupuis",
>     le `{t}` ne se retrouve dans aucun apparenté
>     et a été rajouté dans l’orthographe récemment;
>     si l’homographie avec l’adverbe "puis" est jugée gênante,
>     réécrire ce dernier avec une diacritique distinctive,
>     cf section Diacritique distinctive)
>     + puis éventuelle modif concernant `{-s}`
>   - **"gens" --> #"gents"**
>     (omis des rectifications de 1835,
>     aligne avec les adjectifs "gent, gentil" et surtout "gentilé",
>     favorise la re-création d’un singulier épicène "un gent"
>     comme synonyme de "une personne" ?)
>   - **"objet" --> #"object" ??**
>     (car tous les dérivés contiennent `{ct}`: "objectiver, objectif, objection";
>     les autres mots dans ce cas s’écrivent `{-ct}`:
>     "abject, aspect, respect, suspect, circonspect, instinct";
>     *cependant* beaucoup d’autres mots seraient potentiellement concernés:
>     #"contract, faict, attraict…"
>     et insérer une consonne muette
>     ne semble pas une simplification,
>     encore moins s’il faut se demander si *tous* les dérivés contiennent
>     cette consonne ou s’il existe un dérivé sans)
>     + puis éventuelle modif concernant le `{-ct}` final muet;
>       le plus raisonnable est sans doute de réécrire tous ces mots en `{-t}`
>       comme "sujet, projet, rejet, préfet"
>       ce qui garde leur orthographe simple,
>       et les distingue des mots où `{-ct}` est prononcé
>       comme "correct, direct";
>       on ne peut pas le faire pour les adjectifs, à cause du féminin;
>       donc:
>       * **"aspect" --> #"aspet"**
>       * **"respect" --> #"respet"**
>       * **"instinct" --> #"instint"**
>   - **"cauchemar" --> #"cauchemard"**
>     (car "cauchemarder, cauchemardesque", terminaison habituelle `{-ard}`
>     alors que `{-ar}` est rare, faute fréquente)
>     + voire --> #"cochemard" (aucune parenté étymologique intéressante)
>   - **"bazar" --> #"bazard"**
>     (car "bazarder, bazardage", terminaison habituelle `{-ard}`,
>     rapprochement intéressant avec "hasard")
>     + puis éventuelle modif concernant `{s/z}`
>       (mot de la vie courante depuis longtemps,
>       inutile de discriminer son origine arabo-persane,
>       encore moins sachant que "hasard" aussi est arabe !)
>   - **"caviar" --> #"caviard"**
>     (idem, car "caviarder", pas d’autre dérivé)
>   - **"canular" --> #"canulard"**
>     (idem, comme "bobard", la graphie #"canulard" a déjà existé;
>     *cependant* il existe "canularesque")
>   - **"chaos" --> #"chaot"**
>     (car "chaotique";
>     le rapprochement avec "cahot" est accidentel mais intéressant)
>     + puis éventuelle modif concernant la lettre grecque
>   - **"habit" --> #"habil"**
>     (car "habiller";
>     2 origines différentes,
>     mais la proximité de "habit" avec "habiller"
>     est plus importante qu’avec "habitude, habiter",
>     et il n’y a pas d’apparenté dans le champ lexical du vêtement
>     où on entendrait le `{t}`;
>     c’est un exemple de rapprochement non-étymologique)
>     + voire --> #"abil", et enlever également le H aux mots de la famille de "habiller"
>       (ce H muet n’a été ajouté dans "habiller"
>       que pour rappeler la racine de "habit",
>       dont on est justement en train de s’affranchir;
>       réduit la ressemblance trompeuse avec "habile",
>       supprime le digramme accidentel `{sh}` dans "déshabiller")
>     + puis éventuelle modif concernant `{-il}`
>   - **"fourmi" --> #"fourmille" ?**
>     (car "fourmiller, fourmilière, fourmilier", marque du féminin)
>     + puis éventuelle modif concernant `{ill}`
>   - **"le foie" --> #"le foi"**
>     (aucun apparenté intéressant,
>     `{-e}` non-étymologique et contraire au genre,
>     cf section marque du féminin;
>     si on veut vraiment y mettre une lettre finale muette,
>     il faudrait plutôt écrire #"le foig",
>     qui rappelle les autres langues romanes:
>     italien "_fegato_", portugais "_fígado_", espagnol "_hígado_",
>     du latin "_ficatum_", apparenté… "figue")
>   - **"la foi" --> #"la foid" ?**
>     (car "fidèle", ressemblance trompeuse avec "foyer",
>     aucun apparenté en `{y}`
>     du type "loi → loyal, emploi → employer, joie → joyeux";
>     *cependant* il n’existe aucun mot féminin avec un `{-d}` muet;
>     à défaut, réécrire en #"la foie", cf section marque du féminin)
>   - **"numéro" --> #"numérot" ??**
>     (car "numéroter, numérotation",
>     terminaison `{-ot}` fréquente;
>     *cependant* la terminaison `{-o}` aussi est assez fréquente
>     notamment en apocope,
>     et on peut considérer que le `{t}` fait partie des suffixes
>     car ajouter `{t}` en suffixation est un phénomène fréquent;
>     insérer une lettre muette, non-étymologique,
>     dans un mot qui ne créait pas de problème
>     (et s’écrivait de façon parfaitement phonétique)
>     peut difficilement être considéré comme une simplification)
>   - **"abri" --> #"abrit" ??**
>     (car "abriter";
>     *cependant* mêmes objections que pour "numéro",
>     et de plus la terminaison `{-it}` est rare)
>   - **"jus" --> #"jut" ?** (car "juteux, juter"; *cependant* `{-ut}` est rare)
>   - **"pus" --> #"put" ?** (car "pustule", analogie avec #"jut"; *cependant* `{-ut}` est rare)
>   - **"shampoing" --> #"shampouin"**
>     (car "shampouiner")
>     + puis franciser `{sh}` --> `{ch}` (#"champouin")
>       pour en finir avec les difficultés créées par ce mot
>       (étymologiquement ok car du hindi _champo_)
>   - **"frais" (dépense) singulier --> #"frai"**
>     (car "défrayer, défraiement")
>   - **"frais" (froid) singulier --> #"fraic" ou à la rigueur #"fraix"**
>     (car "fraiche, fraicheur", comme "blanc, franc, croc";
>     étymologiquement pertinent car issu du vieux-francique _frisk_,
>     apparenté "frisquet")
>     + si un `{-c}` muet est inacceptable,
>       alors il faudrait au moins écrire #"fraix",
>       qui est plus logique que "frais" selon le système orthographique actuel
>     + puis éventuelles modifs concernant `{ai}` (`{ei}` serait plus logique),
>       la consonne finale muette
> + **AJUSTEMENT DE VOYELLES:**
>   - **"télescope" --> #"téléscope"**
>     (mot analysé comme la composition "télé+scope",
>     analogue à "péri+scope, micro+scope",
>     écrit comme "téléspectateur, téléski…"
>     *il ne devrait pas être nécessaire de connaitre la date
>     d’apparition d’un mot pour savoir l’écrire*)
>   - **"vrai, vraiment" --> #"vrei, vreiment" ?**
>     (car "vérité"; cf section AI;
>     *cependant* la terminaison `{-ei}` est rare,
>     et il y a aussi le mot "véracité")
>   - **"certain(e)(s)" --> #"certein(e)(s)" ?**
>     (apparentés "certes, (in)certitude, certifier", on ne trouve jamais `{a}`, 
>     malgré l’étymologie; cf section AI)
>   - **"équerre" --> #"écaire"** (car "carré, équarrir, quadrature")
>     + et `{qu}` --> `{c}` dans le reste de la famille
>       (aligne avec "carré"; cf section QU)
>     + puis dé-doublement de la consonne double
>     + puis éventuelle modif concernant `{ai}`
>   - **"dauphin" --> #"dophin"** ou à la rigueur #"deauphin"
>     (car "delphineau, delphinidé, Delphine",
>     du latin _delphinus_, du grec;
>     le `{a}` n’est pas étymologique
>     mais vestige de la prononciation en latin vulgaire;
>     les autres langues d’Europe l’écrivent soit avec `{e}`,
>     soit avec `{o}`, comme l’anglais "_dolphin_"
>     pourtant emprunté au français;
>     cf section AU)
>     + puis éventuelle modif concernant la lettre grecque
>   - **"sauvage" --> #"sovage"**
>     (idem: `{a}` non étymologique mais vestige de latin vulgaire;
>     ressemblance trompeuse avec "sauver, salvation"
>     et l’anglais "_salvage_";
>     pas d’apparenté proche, c’est de la famille de "sylvestre";
>     cf section AU)
>   - **"aurifère" --> #"orifère"** (car "or, doré"; cf section AU)
>   - **"auriculaire" --> #"oriculaire"** (car "oreille, oreiller"; cf section AU)
>   - **"restaurant, restaurer, instaurer" --> #"restorant, restorer, instorer"**
>     (comme l’apocope "resto";
>     conforme à la prononciation car "je restaure" est prononcé `[O]` ouvert
>     contrairement à "je cause, saute, sauve, embaume…" `[o]` fermé;
>     les apparentés qui justifieraient le `{a}`
>     sont chronologiquement lointaines:
>     "état, statut", anglais "_state, stand_", ultimement de l’indo-européen,
>     pas de `{a}` dans l’apparenté "restituer")
>   - **"examen" --> #"examin"** (car "examiner")
>   - **"copain" --> #"copin"** (car "copine, copinage", malgré "compagnon")
>   - **"style, stylo" --> #"stile, stilo"** (cf section lettres grecques)
>   - **"sylvestre" --> #"silvestre"** (cf section lettres grecques)
>   - **"lacrymal" --> #"lacrimal"** (cf section lettres grecques)
>   - **"girouette, giratoire" --> #"gyrouette, gyratoire"**
>     (car "gyrophare, gyroscope, autogyre…" du gréco-latin "_gyro_")
>     + puis éventuelle modif concernant la lettre grecque
> + **SUPPRESSION DE H:**
>   - **"huile" --> #"uile"** (cf section "huile")
>   - **"huître" --> (1990) "huitre" --> #"uitre"** (cf section "huile")
>   - **"souhait" --> #"souait"**
>     (`{h}` inutile et nuisible,
>     dissociant `{ou}` de `{ai}` alors qu’ils sont prononcés
>     en une seule syllabe, `[wE]` et non `[u-E]`)
>     + puis éventuelle modif concernant `{ai}`
>   - **"thé" --> #"té"**
>     (`{h}` inutile, pas du tout un `{th}` grec,
>     l’homographie avec "té" n’est pas gênante)
> + **SON `[s/z]` ISSU D’UN D OU D’UN T:**
>   cf section TI:
>   la façon usuelle de noter un son `[s/z]` issu d’une racine `{d}` est `{s}`,
>   et la façon usuelle de noter un son `[s]` issue d’une racine `{t}` est `{c}`,
>   sauf quand suivi de `{i}` auquel cas on note `{ti}`;
>   en attendant de réformer ces points,
>   on peut au moins rectifier dans la mesure du possible
>   les mots qui ne suivent pas ce principe
>   (quitte à ce que d’éventuels changements systématiques
>   défassent les corrections ponctuelles ci-dessous):
>   - **"foncer, enfoncer, défoncer, foncier, foncièrement"
>     --> #"fonser, enfonser, défonser, fonsier, fonsièrement"**
>     (dérivés de "fond")
>   - **"prétention" --> #"prétension"**
>     (dérivé de "prétendre", comme "tendre → tension")
>   - **"détention, rétention, contention, obtention, abstention"
>     --> #"détension, rétension, contension, obtension, abstension"**
>     (car les verbes associés sont "détenir, retenir, contenir, obtenir, abstenir",
>     pas de `{t}`, donc inutile de les écrire différemment de "tension",
>     et évite la ressemblance trompeuse avec "retenter, contenter"; cf section TI)
>     + les mots terminés par "-tention" qu’on conserve tels quels sont
>       "attention → attentif; intention → intenter"
>   - **"prévention, convention…" --> #"prévension, convension…" ?**
>     (idem: "prévenir, convenir…" *cependant* "préventif"…
>     vivement qu’on ne distingue plus les racines `{d}` de `{t}` !)
>   - **"martien, martial" --> #"marsien, marsial"**
>     (de "Mars", qui même en latin ne comportait pas de `{t}` !
>     le latin écrivait `{t}` dans les dérivés,
>     mais ça ne s’entend plus du tout en français)
>   - **"spatial, spatio-" --> #"spacial, spacio-"**
>     (de "espace", comme "spacieux", analogie avec "espèce, spécieux, spécial";
>     certes la racine est `{t}` mais ça ne s’entend dans aucun mot actuel)
>   - **"essentiel" --> #"essenciel" ?**
>     (idem, de "essence";
>     *cependant* ça enfreindrait le schéma habituel `{-ence/-entiel}`)
>   - **"tertiaire" --> #"terciaire"**
>     (idem, apparentés "tiers(!), tierce")
>     + puis éventuellement réécrire en `{s}` toute la famille
>       (cf section C/S)
>     + puis éventuelle modif concernant `{ai}`
>   - **"différencier" --> "différentier"**
>     (car "différent",
>     et "différentiel" qui suit le schéma habituel `{-ence/-entiel}`;
>     note: le verbe "différentier" existe déjà en mathématiques,
>     mais pas de raison de distinguer ces 2 sens par l’orthographe)
>   - **"passion, compassion" --> #"pation, compation" ?**
>     (car "compatir, patience"
>     et les cousins grecs "empathie, sympathie…"
>     *cependant* "impassible" et l’apparenté plus lointaine "passif")
>   - **"discussion" --> #"discution"**
>     (car "discuter, discute")
>   - **"percussion, répercussion" --> #"percution, répercution"**
>     (car "percuter, répercuter")
>   - **"naissance" --> #"naiçance"**
>     (car "natal, naitre")
>   - **"nourrisson" --> #"nourriçon"**
>     (car "nourrice, nourriture")
>   - **"polisson" --> #"poliçon"**
>     (car "policé, politesse")
>   - **"chanson" --> #"chançon"**
>     (car "chant, chanter", analogie avec "rançon, charançon"
>     et les suffixes `{-ant/-ance}`;
>     c’était d’ailleurs comme ça qu’on écrivait ce mot en ancien français)
>   - **"échanson" --> #"échançon"**
>     (analogie avec #"chançon",
>     étymologiquement pertinent car issu du vieux-francique _skankjo_,
>     apparenté "chinquer")
>   - **"pinson" --> "pinçon"**
>     (idem, d’ailleurs c’était comme ça qu’on l’écrivait en ancien français,
>     la racine latine est en `{c}`)
>   - **"anse, danse, panse" --> #"ance, dance, pance" ?**
>     (car `{-anse}` est très rare alors que `{-ance}` est courant;
>     le `{s}` n’est pas particulièrement étymologique dans "danse"
>     et est anti-étymologique dans "panse" car issu d’un `{t}`;
>     en revanche il semble utile de le garder dans "transe", de "transi",
>     du préfixe latin bien connu _trans-_)
> + **AUTRES CORRECTIONS D’ÉTYMOLOGIE:**
>   - **"chausser, chaussure, chaussette…" --> #"chaucer, chauçure, chaucette…"**
>     (car "caleçon", cf section S/C;
>     ou réécrire "caleçon" --> #"calesson",
>     mais contraire à l’étymologie, et `{ss}` après `{e}` est problématique)
>   - **"garce, garçon" --> #"garse, garson"** (car "gars", cf section S/C)
>   - **"magazine" --> #"magasine"**
>     (même famille que "magasin")
>     + puis éventuelle modif concernant `{s/z}`
>   - **"sino- (sinogramme, sino-japonais…)" --> #"cino-"**
>     (car "Chine", dérive du royaume "Qin"
>     où le `{q}` transcrit [un son proche de `[tS]`][pinyin-q];
>     le `{s}` semble venir de l’intermédiaire arabe;
>     attention à l’homophone "cyno-" signifiant "chien",
>     en particulier "sinophile/cynophile")
> + **CONSONNES DOUBLES:**
>   même dans l’hypothèse improbable
>   où l’on conserverait la majorité des consonnes doubles,
>   il y a de nombreux ajustements à faire…
>   - **"grecque" --> #"grèque"**
>     (cf section État des lieux des consonnes doubles: bizarrerie obscure,
>     confusante avec "turc/turque, franc/franque, basque, monégasque", inutile)
>   - **"salon" --> #"sallon"** (car "salle")
>     + puis dé-doublement de la consonne double
>       (qui n’est pas étymologique
>       et ne sert qu’à distinguer "salle" de l’adjectif "sale",
>       homographie non gênante)
>   - **"chopper" --> #"choper"** (c’est le même mot !)
>   - **"envelopper, développer, échoppe, achopper, stopper"
>     --> #"enveloper, déveloper, échope, achoper, stoper"**
>     (consonne doublée sans raison par rapport à l’orthographe ancienne,
>     sauf dans "stopper" où ça signale l’accolement de "stop" et `{-er}`)
>   - *… plein d’autres consonnes doubles*
> + **FRANCISATION DE MOTS IMPORTÉS:**
>   certains mots intégrés depuis longtemps
>   peuvent sans problème être régularisés…
>   - **"auburn" --> #"aubeurne"** (selon la prononciation; cf aussi section "accord des couleurs")
>   - **"leitmotiv" --> #"leïtmotif"** (selon la prononciation, dérivé de "motif")
>   - **"pedigree" --> #"pédigré"**
>   - **"haïku" --> #"haïkou"** (la graphie actuelle n’est qu’à moitié francisée !)
>   - **"Istanbul" --> #"Istamboul"**
> + **FUSION DE VARIANTES GRAPHIQUES:**
>   - **"le court" (de tennis) --> #"le cour"**
>     (étymologiquement le même mot que "la cour" avec un sens proche,
>     non lié à "court(e), écourter, raccourci")
>     + l’homographie avec "la cour" est évitée
>       si on ajoute la marque du féminin dans ce dernier mot
>       (cf section Marque du féminin)
>     + possible homographie avec "le cours"
>       si celui-ci est aussi réécrit #"le cour" (cf précédemment)…
>       rapprochement pas forcément indésirable
>       (proximité du sens en urbanisme, p.ex. "le #cour St-Pierre"…
>       et même aussi "faire la cour / courir les hommes/femmes" !)
>     + on pourrait au contraire réécrire "la cour" --> #"la court"
>       pour rapprocher des apparentés "courtiser, courtois",
>       et accepter l’homographie avec l’adjectif "court(e)";
>       en tout cas il est absurde
>       d’écrire "la cour" princière sans `{t}`
>       et "le court" de tennis avec `{t}`,
>       alors que les apparentés sont liés au 1er plutôt qu’au 2nd
>   - **"abyme" --> "abîme" --> (1990) "abime"**
>     (c’est le même mot !
>     distinction artificielle ne servant qu’à piéger mesquinement
>     des élèves dans les devoirs de français)
>   - **"ballade" --> "balade"** (idem)
>   - **"bitte" --> "bite"**
>     (c’est le même mot, ou presque !
>     le `{t}` double n’est pas étymologique
>     et est impuissant contre les blagues graveleuses)
>   - **"quant" --> "quand"** (ou le contraire; cf section "quand/quant")
>     + puis éventuelle modif concernant `{qu}`
> + **MOTS COMPOSÉS:**
>   - **"aujourd’hui" --> #"aujourdui"** (cf section "aujourd’hui")
>   - **"au revoir" --> #"aurevoir"**
>     (comme "bonjour, adieu, lendemain, #aujourdui…")
>   - **"s’il te plait, s’il vous plait" --> #"si(l)teplait, si(l)vouplait" ?**
>     (interjection pensée comme un mot unique,
>     dont l’usage dépasse largement le sens impliqué par ses composantes;
>     hésitation fréquente entre espaces et tirets)
>     + puis éventuelles modifs concernant `{ai}` et la consonne finale toujours muette
>   - **"tout à fait" --> #"tout-à-fait" ou #"toutafait"**
>     (locution adverbiale indivisible,
>     le sens n’est plus celui de la composition de ses parties,
>     p.ex. on ne peut pas dire "c’est à fait ça";
>     hésitation fréquente avec des tirets,
>     qu’on trouve déjà en usage au 19e siècle;
>     la graphie soudée empêche la faute "toute à fait")
>     + puis éventuelle modif concernant `{ai}`
>   - **"tout à coup" --> #"tout-à-coup" ou #"toutacoup"**
>     (idem, p.ex. on ne peut pas dire "ça s’est éteint à coup")
>   - **"en dessous, en dehors, en deçà…" --> #"en-dessous, en-dehors, en-deçà…"**
>     (cf section "en dessous")
>     + tolérer d’écrire #"au/par dessus/dessous" etc.
>       avec une espace plutôt qu’un tiret
>   - **"cerf-volant" --> #"servolant"**
>     (erreur d’étymologie, rien à voir avec un cerf
>     mais provient de "serpent volant",
>     à cause des serpentins et dragons asiatiques;
>     on pourrait s’amuser à l’écrire #"serp-volant"
>     mais "serp" n’existe pas et il reste la difficulté du pluriel;
>     même soudée, la composante "volant" reste reconnaissable)
>   - **"pique-nique" --> (1990) "piquenique" --> #"picnic"**
>     (la signification en tant que mot composé est obscure
>     pour un francophone du 21e siècle,
>     déjà au 18e siècle on le trouvait écrit phonétiquement,
>     le pluriel du mot composé est problématique;
>     cf section Marque du féminin pour la terminaison)
> + …
> + …
> + …

[pinyin-q]: https://fr.wikipedia.org/wiki/Consonne_affriquée_alvéolo-palatale_sourde

En feuilletant des livres tels que _Les Délires de l’orthographe_ de Nina Catach,
on trouve des listes entières de mots qu’on pourrait rectifier.
Mais il suffit d’ouvrir un dictionnaire au hasard
ou une liste des 1000 mots les plus fréquents du français,
d’en parcourir les mots,
de se demander pourquoi ils s’écrivent comme ça
et quels sont leurs apparentés…

#### aujourd’hui

Pourquoi "aujourd’hui", mot essentiel de la vie quotidienne,
est-il aussi difficile à écrire ?

C’est un mot vitrine : en conservant cette orthographe aussi
compliquée, on envoie très tôt un message clair à tous les
jeunes enfants qui apprennent à écrire, à tous les étrangers qui
apprennent le français : l’orthographe est tordue et vous allez
souffrir.

C’est un ancien mot composé ;
on a soudé "au, jour, d’"
alors pourquoi continue-t-on à séparer le mot "hui",
qui n’a plus d’existence autonome en français actuel
(hors jargon juridique),
avec en prime un H muet ?
Et quel est ce mot "aujourd"
dont l’existence est suggérée à tort par la graphie ?
On écrit bien "bonjour, lendemain, afin, audit, alarme, gendarme, lierre…"

> **très facile:**
> + "aujourd’hui" --> #"aujourdui"

#### en dessous

"au-dessus, par-dessus, ci-dessus, là-dessus…
au-dessous, par-dessous, ci-dessous, là-dessous…
au-delà…"
s’écrivent tous avec un tiret.
Tous ? Non ! Une irréductible préposition résiste encore et toujours à l’union :
la préposition "en" ! On écrit "en dessous, en dehors, en deçà…".
Le contraire de "au-dessus" (tiret) est donc "en dessous" (espace);
idem pour "au-delà / en deçà".
Fameux piège…
Quelle logique se cache là-dessous ? Réponse: aucune.

Une première mesure facile,
supprimant l’instance la plus courante de ce piège,
est donc d’ajouter un tiret dans ces locutions en "en".

> **très facile (?):**
> + "en dessous" --> #"en-dessous"
> + "en dehors" --> #"en-dehors"
> + "en deçà" --> #"en-deçà"
> + etc.

Mais en fait, pourquoi un tiret dans toutes ces locutions ?
Il n’y a pas lieu d’en mettre puisque
la signification desdites locutions est simplement
celle impliquée par la succession des mots qui les composent.
Le Littré [fait d’ailleurs remarquer][littré-dessus]
l’incohérence entre "par-dessus, par-dessous"
et "par devant, par derrière, par delà",
et déplore l’usage du tiret dans le 1er cas.
Si on met des tirets dans ces locutions, où s’arrête-t-on ?
Pourquoi n’écrit-on pas
#"par-ici, par-ailleurs, à-côté, à-propos, au-fait, à-la-maison, en-amont…" ?
Comment savoir quelles expressions prennent des tirets
et lesquelles n’en prennent pas ?
Le tiret ou la soudure se justifient
pour l’utilisation d’une locution comme un substantif
(ex: "l’au-delà, les à-côtés, un à-propos, un pardessus"),
mais pas autrement.

**raisonnable:**
Le plus logique serait donc de n’écrire que des espaces
dans ces locutions commencées par "au" ou "par".

Note:
d’aucuns (le dictionnaire de l’Académie française, repris par le TLFi)
établissent une nuance de sens entre "au-dess(o)us" et "en dess(o)us".
Ainsi, théoriquement:

+ "au-dessus (de)" signifierait "plus haut (que), supérieur (à)"
+ "en dessus (de)" signifierait "dans la partie supérieure (de),
  sur la face supérieure (de), sur le dessus (de)"

Par exemple (avec un abus de sens géographique):
"Lille est *au-dessus de* (au nord de) Paris",
mais "Montmartre est *en dessus de* (dans le nord de) Paris".
Et la même distinction s’appliquerait donc, logiquement, à "dessous":

+ "au-dessous (de)" signifierait "plus bas (que), inférieur (à)"
+ "en dessous (de)" signifierait "dans la partie inférieure (de),
  sur la face inférieure (de), sur le dessous (de), en contrebas (de)"

Sauf que la théorie est cassée pour "dessous" puisque, en réalité,
c’est "en dessous" qui est employé comme contraire de "au-dessus",
et cumule donc les deux sens !
La distinction n’est vraisemblablement pas faite par les locuteurs,
en particulier pour "dessous".

Les prépositions "en" et "à" [ont des usages tellement proches][en-à]
que la distinction entre les deux peut souvent paraitre accidentelle;
le choix n’est pas porté par le sens, mais par des questions
de genre grammatical, d’euphonie, de distance subjective,
ou d’autres catégories
(ex:
"en outre" mais "à fortiori"
"en dessous" mais "au-dessus";
"en deçà" mais "au-delà";
"en amont/aval, en haut/bas" mais "au recto/verso";
"en train" mais "à pied";
"en vélo" ou "à vélo" au choix;
"en usine" ou "à l’usine" au choix;
"en activité" mais "au chômage, à la retraite";
"en Belgique, en Corse, en Guadeloupe"
  mais "au Maroc, au Honduras, aux Antilles, à la Réunion, à Taïwan";
"en Martinique" ou "à la Martinique" au choix…).

[littré-dessus]: https://www.littre.org/definition/dessus
[en-à]: https://www.persee.fr/doc/lfr_0023-8368_1994_num_103_1_5727

#### quand/quant

Le couple "quand/quant" cause de nombreuses fautes d’orthographe
(étant donné que "quand" est beaucoup plus fréquent que "quant",
c’est le 2nd qui est souvent écrit comme le 1er, jamais l’inverse).

Certes, étymologiquement, "quand" et "quant" sont deux mots différents.
Cependant, ce sont deux mots grammaticaux qui se prononcent exactement pareil.
Il n’y a aucune ambigüité entre les deux
puisque "quant" est toujours suivi de "à",
tandis qu’il n’existe pas de construction "quand à"
(la preuve, c’est que les fautes d’orthographe rencontrées
n’empêchent jamais la compréhension).
De plus, l’étymologie de "quant" ("quantité")
est très peu éclairante vis-à-vis du sens,
et n’a donc aucun intérêt pratique.

Il est donc tout à fait raisonnable d’identifier ces deux mots.
On peut parfaitement prétendre que "quant à"
est une construction figée basée sur le mot "quand".

> **très facile:**
> + "quant" --> "quand"

On privilégie ici la graphie la plus fréquente, celle en `{-d}`.
On pourrait au contraire choisir la graphie en `{-t}`,
ce qui serait phonétiquement plus simple
(liaison `[t]`, aucun apparenté en `[d]`)
mais étymologiquement moins bon.

Note : la même opposition existe dans les autres langues latines,
mais alors la différence s’entend
(espagnol "cuando / en cuanto a";
portugais "quando / quanto a";
italien "quando / (in) quanto a").
De plus, la frontière n’est pas la même qu’en français
(l’espagnol "en cuanto" et le portugais "enquanto", sans "a",
signifient "tandis que, alors que, cependant que"
et se traduisent donc en français par "quand", pas par "quant" !
ce qui semble d’ailleurs plus logique
puisqu’il y a une notion de simultanéité *temporelle*).

#### temps

De nos jours, le `{-s}` de "temps"
ne semble pas faire de liaison au singulier:
on ne dit pas, ou plus, "un temps z’agréable"
(selon le Littré, on le disait au 19e siècle).

Il y a quelques exceptions cependant,
dans des locutions figées comme "de temps z’en temps, de temps z’à autre"
où la liaison est non seulement possible, mais même obligatoire
(je pense que ce sont les deux seules instances…
peut-être y a-t-il aussi une liaison dans
"en temps et en heure, en temps et lieu",
à moins que ce soit de l’hypercorrection ?).

Il semble relativement raisonnable de régulariser
ce mot très courant et très important
en enlevant le `{-s}`,
quitte à marquer l’euphonie dans les locutions problématiques
(à propos d’euphonie, cf section Liaison, ou section -S de l’impératif):
soit avec un `{z}` euphonique, comme "entre quatre z’yeux"
(#"de temps z’en temp, de temp z’à autre"),
soit en convertissant ces expressions au pluriel
(#"de temps en temps, de temps à autres").

> **possible:**
> + "temps" --> #"temp"
> + "de temps en temps" --> #"de temp z’en temp" ou "de temps en temps" (inchangé)
> + "de temps à autre" --> #"de temp z’à autre" ou #"de temps à autres"

#### vieux

"vieux" prend un `{-x}` au masculin singulier,
comme beaucoup de mots en `{-eux}`.
Pourtant "vieux" est bien différent des autres mots en `{-s, -x}` :
dans la plupart de ces mots, quand ils sont adjectifs,
le `{-s, -x}` produit une liaison en `[z]`
et s’entend dans le féminin, qui est en `{-s(s)e}` ~ `[z/s]`
(ex: "envieux, creux, valeureux, jaloux, doux")
(cf section singuliers en -S, -X).

Au contraire,
le `{-x}` de "vieux" n’est jamais prononcé en liaison
(rigoureusement impossible pour l’adjectif
puisqu’en liaison celui-ci devient "vieil" !
douteux pour le nom
car il me semble qu’on ne dira jamais
"le vieux z’a pris de la valeur,
le vieux z’a pris froid,
le vieux z’et le jeune,
un vieux z’accablé de chagrin")
et ne se trouve pas dans le féminin
(puisque le féminin est "vieille", et non "vieuse").

La graphie actuelle masque le problème dans "un vieux homme"
car le `{x}` sépare les voyelles de "vieux" et de "homme";
si on l’enlève, le hiatus (succession de voyelles) interdit devient apparent :
#"un vieu homme".

Les mots avec un comportement similaire
s’écrivent sans `{-x}` au masculin singulier :
"beau → bel(le), fou → fol(le)".

Aligner "vieux" sur "beau, fou"
et sur "dieu, lieu, pieu, bleu, vœu, neveu…"

> **très facile:**
> + "vieux" au singulier --> #"vieu"
>   (puis éventuelles modifs concernant les pluriels en -X)



### Nombres

cf aussi section Chiffres romains

##### 1, 11

"onze" (et dérivés: "onzième, onzièmement…")
commence par une voyelle mais ne fait jamais de liaison avec ce qui précède
(ex: "le onze novembre, le onzième mois, les [.] onze footballeurs,
"neuf-cent[.]-onze, j’en veux [.] onze"
et jamais #"l’onze novembre, l’onzième mois, les z’onze footballeurs,
neuf-cent’onze ou neuf-cent-z-onze, j’en veux z’onze").
C’est exactement le phénomène que d’habitude on note par un H aspiré,
comme dans "huit" !

Par souci de cohérence,
ajouter un H aspiré dans ces mots (cf aussi section H aspiré).

> **très facile:**
> + "onze" --> #"honze" (et dérivés)

Ce H ne sera pas étymologique, mais c’est déjà le cas de nombreux H aspirés
(ex: "huit, hauteur").

Le *nombre* "un" se comporte de la même manière
(ex: "le (numéro) 1, le beau (chiffre) 1,
le nombre un (prononcé "nom-breu-un"), cinq-cent[.]-un"
et jamais #"l’un, le bel un, le nombr’un, cinq-cent’un ou cinq-cent-z-un").
En revanche le *déterminant-pronom* "un" fait bien la liaison…
(ex: "l’un et l’autre").
Distinction amusante :
"j’en veux z’un" avec liaison = pronom, je veux un de ces trucs
/ "j’en veux [.] un" sans liaison = nombre, le nombre de trucs que je veux est 1;
fonctionnellement la signification est la même, mais c’est dit différemment !

Ajouter un H muet au nombre "un" aurait pour conséquence
de distinguer graphiquement le déterminant-pronom du nombre ("un / #hun").
Ça ne me semble pas désirable car ça rendrait l’écriture plus difficile :
alors que c’est l’un des mots les plus essentiels de la langue,
il faudrait sans cesse réfléchir si on l’emploie comme nombre ou comme déterminant,
distinction byzantine puisque le sens revient au même.
Mon opinion est qu’il est agréable et élégant de la part du français
de confondre les deux.
D’autres langues les distinguent ("un / uno" en espagnol, "a / one" en anglais).

sujet lié: cf aussi section accord de 100 et 20

##### 2, 6, 10

"deux, six, dix": cf section -S, -Z, -X.
Le `{-x}` n’a pas de raison d’être (cf section pluriels en -X).
Il entraîne une prononciation irrégulière de `{x}`
dans "deuxième, sixième, dixième"
(alors que "troisième" s’écrit avec `{s}` et "sizaine, dizaine" avec `{z}` !).

Le cas de "six, dix" est unique et justifie l’emploi d’une graphie spéciale:
leur terminaison se prononce `[-s]` quand le nombre est dit de façon isolée,
mais devient muette avec liaison `[-z]` devant un nom;
dans le cas de 10, la liaison `[z]` est faite même dans 18 et 19
alors que "huit, neuf" commencent par une consonne !

Aligner ces familles de mots;
au minimum, rectifier le `{-x}` dans "deux"
et rectifier le `{x}` prononcé `[z]` dans "deuxième, sixième, dixième".

> **très facile:**
> + "deux" --> #"deus"
>   - puis éventuelles modifs concernant la terminaison -S
>     (cf section -S -Z -X),
>     ce qui donnerait #"deuz" ou "deux" selon la proposition retenue
> + "six" --> conservé ou #"sis" ou #"siz" ou #"sisz" ?
> + "dix" --> conservé ou #"dis" ou #"diz" ou #"disz" ?
> + "deuxième" --> #"deusième" (--> #"deuzième" si modif proposée dans section S/Z)
> + ("troisième" --> #"troizième" si modif proposée dans section S/Z)
> + "sixième"  --> #"sisième"  (--> #"sizième"  si modif proposée dans section S/Z)
> + "dixième"  --> #"disième"  (--> #"dizième"  si modif proposée dans section S/Z)
> + "sizaine"  --> #"sisaine" (sauf si modif proposée dans section S/Z)
> + "dizaine"  --> #"disaine" (sauf si modif proposée dans section S/Z)

##### 17, 18, 19

(optionnel)
Il peut se justifier de lexicaliser les noms de 17, 18, 19
(sachant que leur prononciation est un peu spéciale, cf section "10",
et que tous les autres nombres inférieurs à 20 ont leur propre nom).

> + "dix-sept" --> #"dissept"
> + "dix-huit" --> #"dizuit"
> + "dix-neuf" --> #"dizneuf"

##### 20

"vingt": cf section consonnes muettes.
Le mérite du `{g}` étymologique est discutable,
il ne se retrouve guère que dans le mot très technique "vigésimal",
alors que "vingt" est un mot élémentaire du français.

Voir comment traiter l’homophonie avec "il vint" du verbe "venir",
qui est lui aussi bien plus rare que "vingt"
(passé simple = temps désuet à l’oral).
Peut-être réécrire le passé simple de "venir" en "je #veins, il #veint…",
le `{e}` rappelant les autres formes du verbe ?
Mais ça implique de modifier aussi
les dérivés de "venir" (ex: "devenir, parvenir, convenir")
et ceux de "tenir" (ex: "appartenir, obtenir, contenir").

Noter aussi que
(1) certains prononcent "vingt" avec le son `[-t]` même isolément,
et (2) il se prononce avant une unité ("vingt-et-un, vingt-deux…"):
dans ces situations,
le `{-ngt}` de "vingt" est prononcé comme le "-nte" de "trente, quarante…",
p.ex. "26" ne se prononce pas comme "vinsiss" mais comme "vint(e)siss",
il y a un `[t]` légèrement prononcé ou au minimum un petit `[n]`
en plus de la voyelle nasale.

##### 30

"trente" s’écrit `{-ente}`
alors que toutes les dizaines suivantes s’écrivent `{-ante}`.
Aucune raison valable.
(cf aussi -ENT dans section voyelles nasales)

> **très facile:**
> + "trente" --> #"trante"

##### 1000

"mille, million, milliard, millième, millier…": dédoubler le `{ll}`
(cf section ILL).

Éventuellement, enlever le `{-e}` dans "mille"
(cf section marque du féminin).

Note culturelle:
historiquement, en ancien français,
"mil" prononcé `[mil]`
était le singulier de "mille" prononcé `[mij]`,
mais ça fait très longtemps que cette distinction s’est oubliée.

##### Accord de 100 et 20

"cent" et "vingt" s’accordent en nombre
("vingt" uniquement dans "quatre-vingt(s)")
selon une règle de grammaire d’une délicieuse perversité,
qui n’est peut-être surpassée que par celle de l’accord de "tout":

 1. "cent" et "vingt" s’accordent en nombre selon la quantité qui les multiplie…
    - p.ex:
      * "quatre-vingt**s** pages"
      * "quatre-vingt**s** millions"
      * "deux-cent**s** pages"
      * "deux-cent**s** millions"
 2. … sauf quand ils sont suivis d’un autre numéral…  
    ("million", "milliard", etc. ne sont pas des numéraux mais des noms,
    et donc "vingt" et "cent" s’accordent en nombre devant)
    - p.ex:
      * "quatre-vingt-une pages"
      * "quatre-vingt-mille"
      * "deux-cent-une pages"
      * "deux-cent-mille"
 3. … ou postposés en position d’ordinal.
    - p.ex:
      * "page quatre-vingt"
      * "page deux-cent"

J’ignore la cause de cette règle,
je ne peux que supposer que c’est pour coller à la prononciation.
Auquel cas le `{-s}` serait euphonique (cf section -S de l’impératif)
et il faudrait plutôt écrire #"deux-cent z-habitants" par exemple…
Mais ça ne justifie pas de l’écrire quand il n’est pas prononcé
(ex: "deux-cents pages").

En clair, la règle est archi-tordue et doit être simplifiée de toute urgence !
Deux possibilités:

+ A - soit rendre ces mots régulièrement variables (`{-s}` au pluriel dans tous les cas);
+ B - soit rendre ces mots complètement invariables (comme "mille").

Attention: toucher à cette règle pourrait(?) être une modif «destructrice»
car ajoutant ou supprimant une terminaison `{-s}`,
ce qui pourrait(?) éventuellement avoir une conséquence en liaison.
Ce n’est en fait pas le cas.
Examinons donc les (non-)liaisons existantes:

+ "cents, quatre-vingts" devant un nom
  font indiscutablement une liaison `[z]`
  (ex: "deux-cents z-habitants")  
  => donc ne peuvent pas être réécrits sans `{-s}`
+ "cent" devant un autre numéral:
  les seules possibilités de liaison seraient devant "un(e), onze, huitante";  
  dans aucun de ces cas (101, 201, 111, 211, 180, 280)
  il ne semble y avoir de liaison `[t]` en pratique  
  => donc peut être réécrit avec `{-s}` sans changer les prononciations existantes
+ "quatre-vingt" devant un autre numéral:
  les seules possibilités de liaison seraient devant "un(e), onze";  
  dans aucun de ces cas (21, 81, 91)
  il ne semble y avoir de liaison `[t]` en pratique  
  => donc peut être réécrit avec `{-s}` sans changer les prononciations existantes

En fait, "un, onze" ont un H aspiré non-écrit, ils se comportent comme "huit" !
(cf section "11": il serait judicieux d’écrire ce H aspiré)
Donc en ajoutant un `{-s}` de pluriel à "cent, vingt",
on ne modifie pas du tout les liaisons:
non seulement on ne perd pas de liaison existante en `[t]`,
mais on ne crée pas non plus la possibilité d’une liaison en `[z]`.

**très facile:**
La nouvelle règle à adopter est donc la règle A:

> + "cent" s’accorde en nombre de façon régulière
> + "quatre-vingts" s’écrit toujours avec un `{-s}`

En passant, on peut envisager de lexicaliser le nom de 80,
comme le faisait Victor Hugo:

> + "quatre-vingt(s)" --> #"quatrevingts" (et éventuelle modif de "vingt")

Souci mineur:
on dit "quatre-ving**t**ième, deux-cen**t**ième".
(personnellement je ne serais pas contre que ça devienne "deux-cen**z**ième"…).



### Régularisations grammaticales

#### -S de je et tu

Dans les conjugaisons,
les 1re et 2e personnes (singulier comme pluriel)
sont habituellement marquées par un `{-s}`,
tandis que la 3e personne (singulier comme pluriel)
est habituellement marquée par un `{-t}`.
Cependant, dans les cas des conjugaisons en `{-e}`
  (ex: "j’aime, tu aimes, on aime")
ou en `{-a}`
  (ex: "j’aimerai, tu aimeras, on aimera;
  j’ai, tu as, on a"),
au singulier,
seule la 2e personne porte un `{-s}`;
les 1re et 3e personnes perdent leur lettre caractéristique.
Et ce, alors même que le `{-t}` de la 3e personne
s’entend encore par euphonie,
dans des constructions comme "aime-t-il, a-t-on".

Ce traitement spécial de la 2e personne est étrange,
d’autant plus que la 2e personne est similaire à la 1re.

Pour ajouter à la bizarrerie,
à l’impératif, contrairement aux autres temps,
la 2e personne perd elle aussi sa lettre caractéristique
  (ex: "va !")…
alors même qu’elle peut s’entendre par euphonie
  (ex: "vas-y !"),
ce qui crée une règle orthographique compliquée.

##### Conjugaisons en -E

Dans les conjugaisons,
quand la 3e personne du singulier fait `{-e}`,
alors les 3 personnes du singulier font `{-e, -es, -e}`.

Ceci concerne
l’indicatif présent des verbes du 1er groupe
  (ex: "j’aime, tu aimes, on aime")
et de certains verbes irréguliers calqués sur le 1er groupe
  (ex: "je/tu cueille(s), offre(s), ouvre(s), souffre(s)"),
le subjonctif présent de ces mêmes verbes
et de quelques autres verbes irréguliers
  (ex: "que je/tu aie(s), aille(s), puisse(s), sache(s), veuille(s), voie(s)…")
et le subjonctif imparfait de tous les verbes
  (ex: "que je/tu aimasse(s), finisse(s), fusse(s)").

La 2e personne est donc distinguée par l’ajout d’un `{-s}`.
Or, dans tous les autres cas:

+ soit les 1re et 2e personnes s’écrivent pareil, avec un `{-s}`,
  sans que ça cause d’ambigüité:
  - à l’imparfait de tous les verbes
      (ex: "je/tu aimais;
      je/tu finissais;
      je/tu étais"),
  - au passé simple hors 1er groupe
      (ex: "je/tu finis; je/tu fus");
  - et au conditionnel de tous les verbes
      (ex: "je/tu aimerais;
      je/tu finirais;
      je/tu serais");
+ soit elles s’écrivent différemment,
  respectivement `{-ai, -as}`,
  mais alors c’est parce qu’elles se prononcent différemment:
  - au passé simple du 1er groupe
      (ex: "j’aimai, tu aimas"),
  - au futur simple de tous les verbes
      (ex: "j’aimerai, tu aimeras;
      je finirai, tu finiras;
      je serai, tu seras");
  - et pour certains verbes irréguliers
      ("je suis(!), tu es;
      j’ai, tu as;
      je vais(!), tu vas;
      j’allai, tu allas").

Il est donc inutile et incohérent
de distinguer la 2e personne de la 1re à l’écrit
quand ce n’est pas fait à l’oral.
Il est incohérent
de marquer la 2e personne et uniquement elle d’un `{-s}`,
alors que dans les autres conjugaisons
ce `{-s}` caractérise aussi la 1re personne.

Ceci est une source de fautes très courantes.

De plus, ce `{-s}` après `{e}` est complètement muet,
il n’est jamais prononcé en liaison.

Deux alternatives possibles:

+ A — ajouter un `{-s}` à la 1re personne pour ces cas,
  les conjugaisons deviennent donc `#{-es, -es, -e}`
  (ex: "#j’aimes, tu aimes, on aime;
  que #j’aies, que tu aies, qu’on aie");
  inconvénients:
  - réduit peut-être le risque de faute, mais ne l’élimine pas,
    vu que la forme en `{-e}` continue d’exister
  - ajoute une consonne (complètement) muette
    dans des formes verbales très fréquentes au quotidien
+ B — supprimer ce `{-s}` à la 2e personne,
  les conjugaisons deviennent donc `#{-e, -e, -e}`
  (ex: "j’aime, tu #aime, on aime;
  que j’aie, que tu #aie, qu’on aie");
  avantages:
  - simplifie réellement les conjugaisons
  - élimine le risque de faute
  - supprime une consonne (complètement) muette
    dans des formes verbales très fréquentes au quotidien
  - la disparition du `{-s}` habituel des 1re et 2e personnes
    peut s’expliquer ainsi:
    le `{e}` muet remplace/absorbe/fait disparaitre le `{-s}`…
    tout comme il remplace le `{-t}` habituel de la 3e personne
    (alors même que celui-ci peut encore s’entendre: "aime-t-on")

La solution B semble bien meilleure.

Chaque solution se combine avec
une simplification possible du -S de l’impératif
(cf section -S de l’impératif)

##### Conjugaisons en -A

Dans le modèle de conjugaison `{-ai, -as, -a}`
(passé simple du 1er groupe,
futur simple de tous les verbes,
indicatif présent des verbes "avoir, aller"),
la 2e personne du singulier est marquée par `{-s}`,
mais la 1re non,
et la 3e n’est pas marquée par `{-t}`.

Comme pour les conjugaisons en -E
(cf section conjugaisons en -E, alternative A),
on pourrait envisager d’ajouter `{-s}` à la 1re personne,
pour donner `#{-ais, -as, -a}`.
Mais ça ne semble vraiment pas désirable
car ça confondrait le futur simple et le conditionnel à la 1re personne
  (ex: "j’aimerai / j’aimerais"),
et surtout ça alourdirait "j’ai" en #"j’ais"…

On peut admettre que la 1re personne ne porte pas de `{-s}`
étant donné qu’elle diffère de la 2e par un `{-i}`.
Concernant la 3e personne néanmoins,
il reste étrange qu’elle ne porte pas de `{-t}`,
à côté d’une 2e personne qui porte un `{-s}`.
D’autant plus étrange que ce `{-t}` est étymologique
(la racine latine de "aller" comportait un `{d}`)
et s’entend parfois en euphonie
(ex: "aimera-t-on, a-t-on, va-t-on, va-t-en-guerre, à Dieu vat").

On pourrait donc envisager d’ajouter `{-t}` à la 3e personne,
pour donner `#{-ai, -as, -at}`.
Mais encore une fois c’est trop lourd,
"on a" deviendrait #"on at"…

**Proposition:**
Alternativement, comme pour les conjugaisons en -E
(cf section conjugaisons en -E, alternative B),
on peut supprimer le `{-s}` inutile de la 2e personne,
pour donner `#{-ai, -a, -a}`
(ex: #"tu aimera, tu a, tu va").
En théorie ce `{-s}` peut être prononcé en liaison,
mais je pense que ce n’est pas courant.

##### -S de l’impératif

Rappel de la règle, telle qu’elle est traditionnellement présentée:

 1. L’impératif présent est en général identique à l’indicatif présent
    (ex: "finis ! cours ! bois ! fais !").
    + Pour 5 verbes irréguliers ("être, avoir, savoir, vouloir, aller"),
      l’impératif présent se base plutôt sur le subjonctif présent
      ("sois, aie(s), sache(s), veuille(s), va(s)").
 2. *Cependant,* à la 2e personne du singulier,
    si l’indicatif présent se termine par `{-es}`,
    alors pour l’impératif on enlève le `{-s}`,
    et la terminaison est donc `{-e}`
    (cela concerne donc les verbes en du 1er groupe
    et quelques verbes irréguliers)
    (ex: "mange ! aie ! sache ! veuille !").
    + Cette règle s’applique également au verbe "aller": "va !"
 3. *Toutefois*, si le verbe conjugué est suivi du pronom "en" ou "y",
    accolé avec un tiret,
    alors on remet ce `{-s}` et il se prononce en liaison
    (ex: "manges-en, penses-y, vas-y").
 4. *Nonobstant*, il convient de distinguer la situation précédente
    de celle où le pronom "en" ou "y" est
    complément d’un autre verbe à l’infinitif,
    et non du verbe à l’impératif;
    auquel cas on n’ajoute pas de `{−s}`
    (ex: "va en prendre").

**Première remarque:**
Le `{-s}` inséré dans la règle 3 est qualifié d’*euphonique* :
il n’est pas considéré comme faisant partie de la «vraie» orthographe,
mais on l’ajoute pour coller à la prononciation réelle.

C’est en fait exactement la même situation que le `{t}` dans "a-t-on".
Alors, pourquoi note-t-on ces deux situations différemment ?
Il serait plus cohérent d’écrire #"mange-z-en, pense-z-y, va-z-y".

Altérer la graphie du verbe lui-même selon le contexte de prononciation
est un cas très singulier.
En effet, un **grand principe** de l’orthographe française
(ce qui la distingue d’autres langues comme le breton)
est que les mots sont toujours écrits à l’identique, dans toute leur gloire,
indépendamment des accidents de l’oral:
par exemple, on écrit systématiquement les consonnes et les lettres E finales,
même dans les situations où elles ne sont pas prononcées
(rares exceptions à ce principe:
les articles contractés "à le(s) → au(x), de le(s) → du/des",
les élisions "le/la → l’, je → j’, que/qui → qu’…"
et l’altération de quelques adjectifs masculins "beau → bel, vieux → vieil…").

**Deuxième remarque:**
Il est en fait choquant de supprimer le `{-s}`
de la 2e personne du singulier de l’impératif,
alors que partout ailleurs cette personne est marquée par `{-s}`
(mais cf section conjugaisons en -E).
En fait, la 2e personne du singulier de l’impératif
est identique non pas à la 2e, mais à la *première* personne
du singulier de l’indicatif !
Et ce, même pour les verbes où ces deux personnes s’écrivent différemment
(ex: "je mange, tu manges").
Cette remarque permettrait d’abréger la formulation traditionnelle de la règle
(qui circonvolue inutilement),
mais elle met surtout en évidence la bizarrerie de la chose
(raison peut-être pour laquelle les cours de français
gardent la présentation compliquée :
ne pas rendre la bizarrerie trop flagrante ?).

Alors, pourquoi il n’y a pas de `{-s}` à l’impératif sauf euphonie ?
Selon Nina Catach (p45) :
parce qu’en latin c’était comme ça.

Deux alternatives raisonnables
pour réduire voire supprimer cette complexité:

+ A — toujours écrire un `{-s}`
  - le plus simple, supprime complètement la difficulté:
    les points 2, 3 et 4 de la règle ci-dessus
    disparaissent purement et simplement
  - suppose qu’on conserve le `{-s}` à l’indicatif
    (cf section conjugaisons en -E)
+ B — ne jamais écrire de `{-s}`
  ni à l’indicatif (cf section conjugaisons en -E, alternative B) ni à l’impératif,
  écrire un `{-z-}` euphonique (ex: #"mange-z-en, pense-z-y, va-z-y")
  - un peu plus complexe:
    le point 2 disparait mais il reste les points 3 (modifié) et 4;
    ceci dit, cette complexité-là reflète une réalité orale instinctive
    et devrait causer peu de fautes à l’écrit
    (tout comme le `{-t-}` euphonique dans "a-t-on" pose rarement problème)

La modif A n’est en fait pas «destructrice»
car elle ne supprime pas de liaison existante
(on ne fait qu’ajouter un `{-s}` après une voyelle `{e}`, ou "va").
Elle autorise en revanche de nouvelles liaisons.
Noter aussi que cela affecte le comptage des syllabes en poésie classique
(ex: "mange avec moi" ~ `[m@.Za.vEk.mwa]`
/ "manges avec moi" ~ `[m@.Z°.za.vEk.mwa]`).

(cf aussi section -S, -Z, -X)

##### ai, vais

Les verbes irréguliers "avoir" et "aller" sont similaires:
  "j’ai, tu as, il a;
  je vais, tu vas, il va".
À l’indicatif présent, au singulier,
ils suivent le modèle `{-ai, -as, -a}`
qu’on retrouve dans beaucoup de conjugaisons
(passé simple du 1er groupe, futur simple de tous les verbes)…
… Sauf que "je vais" prend un `{-s}`!

Il est tentant de le régulariser en #"je vai",
mais ceci supprime une liaison,
qui est facultative mais couramment pratiquée.

> **modif facile mais destructrice:**
> + "je vais" --> #"je vai"?
>   (supprime une liaison)

("être, avoir, aller" sont les seuls(?) verbes irréguliers
dont la 1re personne du singulier à l’indicatif présent
est phonétiquement différente de la 2e.)

##### peux, veux, vaux

Les verbes "pouvoir, vouloir, valoir+",
ont une forme conjuguée en `{-x}`:
"je/tu peux, veux, vaux".
La marque habituelle des 1re et 2e personnes du singulier est `{-s}`
et (comme les pluriels en -X)
ces 3 exceptions sont des irrégularités purement orthographiques
sans aucune justification
(à la 3e personne on trouve bien le `{-t}` habituel: "il peut, veut, vaut").
La seule raison semble être la fréquence des terminaisons `{-eux, -aux}`,
dont on a vu qu’elles n’étaient pas justifiées,
et qu’on propose par ailleurs de régulariser
(cf section pluriels en -X et section -S, -Z, -X).

Régulariser ces conjugaisons:

> **très facile:**
> + "peux, veux, vaux" --> #"peus, veus, vaus"
>   (ou #"peuz, veuz, vauz"
>   selon la modif proposée dans section -S, -Z, -X
>   pour les terminaisons verbales)

#### Verbes en -DRE

La plupart des verbes en `{-dre}`
(ex: "prendre, entendre, répondre, mordre, perdre, tordre")
gardent un `{d}` distinctif dans leurs formes conjuguées,
qui font `{-ds, -ds, -d}` au singulier du présent de l’indicatif
au lieu de `{-s, -s, -t}`
(et où le `{d}` se prononce `[t]` en liaison,
de façon tout à fait régulière).
Mais cette exception a une exception:
les verbes en `{-indre}`
(ex: "peindre, atteindre, craindre, joindre")
et en `{-soudre}`
(liste exhaustive: "résoudre, dissoudre, absoudre")
font `{-s, -s, -t}`…
alors que même les autres verbes en `{-oudre}`
(liste exhaustive: "coudre+, moudre")
font bien `{-ds, -ds, -d}` !

Le point commun à ces verbes semble être que
leur participe passé est en `{-t}`
(ex: "peint, dissout"… sauf "résolu" ! et "dissolu" existe aussi…)
au lieu d’être d’être en `{-u}`
(ex: "cousu(!), moulu(!), prendu(!!), entendu, répondu, mordu, perdu, tordu").
Mais pourquoi, sous prétexte que le participe passé est exceptionnel,
rendre également exceptionnel le présent de l’indicatif,
artificiellement, alors qu’il est en fait régulier ?
Il faut de toute façon apprendre la liste d’exceptions
et, pour les locuteurs natifs, cet apprentissage est oral…

Cette exception n’a pas de justification bien convaincante
et est responsable de nombreuses prétendues fautes
(ex: "on résoud").

**très facile:**
Régulariser ces conjugaisons en `{-ds, -ds, -d}`.

Alternativement,
on pourrait régulariser tous les verbes en `{-dre}`
vers la forme générale `{-s, -s, -t}`,
mais cela affecterait des verbes très courants
(ex: "prendre, entendre, répondre, perdre")
et, de plus, le `{d}` a une vertu distinctive.

#### Verbes en -TTRE

Les verbes en `{-tre}`
(ex: "naître, connaître, paraître, croître, foutre")
font régulièrement, au singulier du présent de l’indicatif,
`{-s, -s, -t}`.
Sauf les verbes en `{-ttre}`
(liste exhaustive: "mettre+, battre+")
qui font `{-ts, -ts, -t}`.
Justification: ce `{t}` fait partie du radical
et persiste dans les autres formes du verbe
(ex: "mettons, battons / naissons, croissons").

Considérations:

+ Demandons-nous si cette information est suffisamment utile
  pour justifier l’exception.
  - En tout cas, le séculaire et très important verbe "foutre"
    devrait rejoindre le club et être réécrit #"fouttre"
    (car "nous foutons, foutu")
+ Par ailleurs, on supprime les consonnes doubles
  (cf section consonnes doubles),
  les infinitifs de ces verbes deviendraient #"mètre, batre".

Voir ce qu’on veut faire de ces formes conjuguées…
Le `{t}` est un peu distinctif aussi
(ex: "je mets, bats" / #"je mès, bas" / #"je mèz, baz"
selon la proposition de la section -S, -Z, -X).

#### incluse, exclue

On a une incohérence dans la paire contraire "inclus(e) / exclu(e)"
alors que les deux mots sont formés sur la même racine !

"exclu" (et aussi "conclu") se conforme au principe général
des participes passés en `{-u}` pour les verbes en `{-re}`,
tandis que "inclus" (et aussi "reclus", rare) est une exception.

**modif facile mais destructrice:**
Régulariser "inclus(e), reclus(e)" en #"inclu(e), reclu(e)".

#### Infinitifs en -IR/-IRE, -OIR/-OIRE

TODO: tirer cette affaire au clair…

[wikip-3e-groupe]: https://fr.wikipedia.org/wiki/Conjugaison_des_verbes_du_troisième_groupe_en_français

Verbes irréguliers
(nommés par euphémisme le « [troisième groupe][wikip-3e-groupe] »,
comme dans « Rencontres du troisième type »):

Quel point commun entre tous les verbes en `{-ire}`,
quel point commun entre tous les verbes en `{-ir}` du 3e groupe,
quelle différence entre ces deux catégories?

+ "rire, sourire, dire, lire, luire, **haïr**"
  => le `{i}` fait partie du radical
  ("ris, ri-ons; souris, souri-ons; dis, dis-ons, lis, lis-ons;
  luis, luis-ons; hais, haïssons")
+ ≠
  "dormir, mentir, souffrir, courir, mourir"
  ("dors, dorm-ons; mens, ment-ons; souffre, souffr-ons;
  cours, cour-ons; meurs, mour-ons")

Même question pour `{-oire/-oir}`?

+ "croire, boire, **voir, assoir**"
  => le `{oi}` fait partie du radical
  (quitte à se transformer parfois en une autre voyelle)
+ ≠ "avoir, valoir, vouloir, pouvoir, devoir, falloir, mouvoir, savoir, recevoir"
+ "voir" est très similaire à "croire", ressemble pas mal à "boire",
  et est très différent de "avoir"…
  - => peut-être que #"voir" est censé être écrit #"voire"
    mais que ce n’est pas fait pour éviter l’homonymie avec l’adverbe "voire" ??

La grande tendance du 3e groupe semble être que,
pour les verbes dont l’infinitif est en `{-re}` (y compris `{-ire, -oire}`),
on obtient le radical en enlevant `{-re}`;
mais on voit qu’il y a des exceptions…
Faut-il les régulariser ?

> + infinitif "haïr" --> #"haïre" ?
> + infinitif "voir" --> #"voire" ?
>   (et "prévoir, pourvoir",
>   mais pas "concevoir, percevoir, apercevoir"
>   qui ne sont d’ailleurs pas apparentés à "voir")
> + infinitif "assoir" -> #"assoire" ?
>   (et "soir, sursoir")

#### Marque du féminin

De nombreux mots féminins
ne portent pas la marque régulière du féminin, `{-e}`,
alors qu’ils le pourraient facilement.
Inversement, de nombreux mots masculins se terminent par un `{-e}`
qu’on pourrait facilement ôter.

Non seulement ces mots font partie des nombreuses irrégularités
qui contribuent à la complexité de l’écriture,
mais en plus ces irrégularités-là sont particulièrement perverses
car elles réalisent précisément *l’inverse* de la règle générale.
Elles créent ainsi de la confusion mentale
(ex: "libéré, liberté, fauché / libérée, apogée;
acteur, sœur / auteure, beurre;
air, chair, pair / paire, repaire, salaire;
soi, foi / soie, foie").
On veut donc conformer autant de mots que possible à la règle générale.
Outre réduire la confusion mentale
et le nombre global d’irrégularités,
cela permet aussi
de clarifier le genre de certains mots qui actuellement sèment le doute
(ex: "un apogée, un gynécée, une orque, une spore"),
et de séparer des homographes
(ex: "le tour / la #toure; le #mémoir / la mémoire;
le #voil / la voile; le #pendul / la pendule, le #moul / la moule";
comme on distingue déjà "le pâté / la pâtée; le porté / la portée"),
De toute façon, un `{-e}` muet n’a pas grand-chose d’étymologique.

Bien sûr, on ne peut pas facilement aligner tous les mots,
notamment les mots masculins dont le dernier son est une consonne
(ex: "fantôme, domaine, ponte").
Notons cependant que d’autres modifications systématiques
proposées ailleurs permettraient d’aligner davantage de mots:

+ en décidant que le `{m}` n’est plus nasalisé
  (cf section M après B/P),
  on pourrait ôter `{-e}` après `{m}`
  (ex: #"fantôm, problem, théorem, régim, amalgam");
+ en systématisant l’usage du tréma
  (cf section Tréma),
  on pourrait ôter `{-e}` après `{n}`
  (ex: #"domai:n, mélomän" comme #"fän, tennismän");
+ en clarifiant la prononciation de `{-er}`
  (cf section -ER),
  on pourrait ôter `{-e}` dans `{-ère}`
  (ex: #"per, comper, frer, belvéder, crater");
+ en clarifiant le fait que les consonnes finales soient ou non prononcées
  (cf section Consonnes finales muettes),
  on pourrait enlever beaucoup plus de `{-e}` après consonne.

Ajouter ou ôter un `{-e}` après une consonne
est en principe une modif «destructrice»,
au sens où ça a pour conséquence sur la langue parlée
de modifier le compte des pieds en poésie classique
(ex: "peur bleue" = 2 pieds / "#peure bleue" = 3 pieds")…
ce dont à mon humble avis on se moque éperdument
(1° car la langue est un outil qui préexiste à la poésie,
laquelle n’est qu’un jeu avec ou sur lui;
2° car le classicisme n’a plus le vent en poupe;
3° car celui-ci se permet déjà des licences
vis-à-vis de la langue moderne quand ça l’arrange:
"encor, jusques…";
4° car il se réjouira sans doute de voir le «genre des rimes»
coïncider davantage avec le genre grammatical).
Ça n’a pas d’autre conséquence.

TODO: inventaire plus exhaustif…

##### Noms communs

On laisse de côté les apocopes (ex: "la télé")
et les mots composés (ex: "la belle-de-jour, le parterre, le parapluie").

+ `{-é(e)}`:
  - **masculins irréguliers:**
    "apogée, périgée, hypogée,
    athée, caducée, élysée (adj. bizarrement épicène: les Champs-Élysées),
    gynécée, lycée, musée, mausolée, macchabée, périnée,
    pygmée, scarabée, trophée"
  - **féminins irréguliers:**
    "clé, acné, psyché, moitié, amitié, pitié, piété",
    le prolifique suffixe `{-té}`
    désignant souvent une notion abstraite, « le fait d’être quelque chose »
    (ex: "quantité, société, santé, unité, liberté, beauté")
    * ≠ on ne peut pas prétendre que le `{-é}` joue le rôle du `{-e}` féminin,
      car ce n’est le cas dans d’autres mots masculins en `{-é}`
      ni dans d’autres mots féminins en `{-ée}`
      (ex: "une idée, la fée, la canopée, la flopée, la montée, la chevauchée;
      le côté, le raté;
      le/la porté(e), le/la jeté(e), le/la pâté(e);
      la facilité / le processus est facilité / la démarche est facilitée;
      la cité universitaire / le chiffre souvent cité / la phrase souvent citée")
+ `{-eur(r)(e)}`:
  - **masculins irréguliers:**
    "beurre, leurre"
  - **féminins irréguliers:**
    "sœur, fleur, couleur, peur, odeur, vapeur, rancœur",
    le prolifique suffixe `{-eur}`
    exprimant « le fait d’être quelque chose »
    (ex: "grandeur, épaisseur, pâleur, ardeur, chaleur, vigueur")
    * ≠ masculins réguliers:
      "pleur",
      le prolifique suffixe `{-eur}` indiquant « qui fait quelque chose »
      (ex: "acteur, coiffeur, moteur, voleur, rongeur");
      il parait donc particulièrement profitable
      de distinguer le suffixe `{-eur}` féminin du suffixe `{-eur}` masculin
    * le suffixe féminin `{-eur}` est similaire au suffixe féminin `{-ure}`
      (ex: "voilure, rognure, gageüre"; comparer "froideur≈froidure"),
      il parait donc désirable d’aligner la marque du féminin dans ces deux suffixes
+ `{-our(r)(e)}`:
  - **féminins irréguliers:**
    "cour (et dérivés: basse-cour…), tour"
+ `{-oir(e)}`:
  - **masculins irréguliers:**
    "mémoire, ivoire, déboire, pourboire",
    le prolifique suffixe `{-oire}`
    en général associé à l’idée de lieu ou d’instrument
    (ex: "territoire, accessoire, auditoire, moratoire, directoire, répertoire,
    giratoire, exutoire, laboratoire, observatoire, purgatoire, suppositoire…")
    * ≠ féminins réguliers:
      "armoire, histoire, gloire, foire, victoire…"
    * ≠ masculins réguliers:
      "noir, soir, espoir, devoir, loir, tamanoir…",
    * ≠ le prolifique suffixe `{-oir(e)}`,
      masculin ou féminin,
      exprimant « le lieu où l’on fait quelque chose,
      ou l’instrument avec lequel on fait quelque chose »
      (ex: masculin "miroir, mouchoir, accoudoir, comptoir, boudoir, trottoir, dévidoir";
      féminin "mâchoire, nageoire, patinoire, balançoire, bouilloire, passoire");
      il parait donc judicieux de fusionner le `{-oire}` masculin
      dans le `{-oir}` masculin, c’est essentiellement le même suffixe
+ `{-air(e)}`:
  - **masculins irréguliers:**
    "salaire, repaire…",
    le prolifique suffixe `{-aire}`, variante de `{-oire}`
    (ex: "inventaire, abécédaire, dictionnaire, luminaire, faussaire, moustiquaire…")
  - **féminins irréguliers:**
    "chair"
    * ≠ masculins réguliers:
      "air, éclair, pair, flair…"
+ `{-ir(e)}`:
  - **masculins irréguliers:**
    "rire, sourire, délire, empire, navire, vampire, sire, sbire, cachemire, autogire"
+ `{-if(f)e}`:
  - **masculins irréguliers:**
    "calife, pontife, escogriffe"
+ `{-ul(l)e}`:
  - **masculins irréguliers:**
    "pendule, bidule, module, nodule, ovule, véhicule, matricule, opercule,
    fascicule, crépuscule, testicule, tubercule, fécule, pustule, préambule, pécule…",
    suffixe `{-cule}` indiquant la petitesse
    (ex: "monticule, ventricule, groupuscule…")
    * ≠ mots réguliers:
      "cul, calcul, cumul, consul, nul(le), bulle, ridule, tarentule"
+ `{-oi(e)}`:
  - **masculins irréguliers:**
    "foie(!)"
  - **féminins irréguliers:**
    "foi(!), loi, paroi"
    * ≠ masculins réguliers:
      "soi, roi, emploi, envoi, convoi, désarroi, effroi, émoi, aboi, aloi, tournoi, beffroi"
    * ≠ féminins réguliers:
      "soie, voie, joie, courroie, oie, lamproie, Savoie"
+ divers (non exhaustif):
  - **masculins irréguliers:**
    "mille, domicile, volatile, voile, phare, tintamarre, golfe, pore, nombreux autres `{-ore}`…"
  - **féminins irréguliers:**
    "fourmi…"
+ `{-ic, -ique}`:
  plus difficile…
  chez les noms communs, `{-ique}` est plus fréquent que `{-ic}`,
  notamment parce qu’il y a aussi les adjectifs de cette forme, épicènes
  (cf plus loin)
  (ex: "plastique, tropique, tonique, téléphérique, zygomatique, cornique"),
  mais il y a aussi beaucoup de noms masculins en `{-ic}`:
  (ex: "aérobic, alambic, arsenic, diagnostic, plastic(!), trafic, flic, fric, loustic")
  et des paires masculin/féminin:
  (ex: "le basilic / la basilique, le tic / la tique, le cric / la crique,
  le clic / la clique, le pic / la pique, le panic / la panique");
  et des irrégularités même par rapport au reste,
  il faudrait au moins régulariser celles-ci
  vis-à-vis des règles particulières concernant ces suffixes:
  - "le public": adjectif "public/publique" unique en son genre
    * "public" --> "publique"
  - "le diagnostic": nom masculin en `{-ic}` mais adjectif épicène en `{-ique}` !
    * "diagnostic" --> "diagnostique"
  - "chic": adjectif invariable sans `{-e}` !
    * "chic" --> #"chique" (note: homographie avec "la chique")
  - au contraire, des noms masculins en `{-ique}`
    qui ne correspondent pourtant pas à des adjectifs:
    "moustique (loustic ! moujik !), nasique, cantique…"
    * --> #"moustic, nasic, cantic…"
+ autres `{-c, -que}`:
  encore moins clair…
  "le sac, le bec, le mec, le troc, le foc";
  "le lac / la laque, le trac / la traque, le coq / la coque, le toc / la toque";
  mais "un abaque, le macaque, le chèque, le métèque,
  le phoque, le roque, le cirque";
  "grec/grecque, turc/turque"
  mais "slovaque, bosniaque, aztèque, guatémaltèque, basque, monégasque"…

Attention:
certains noms en `{-e}`,
particulièrement en `{-oire, -aire, -ique}`
sont des noms épicènes de personnes
(ex: "un(e) antiquaire, sociétaire, commissaire, critique")
ou bien des noms masculins qui sont aussi des adjectifs épicènes
(ex: "accessoire, moratoire, contraire, honoraire, plastique, comique, tonique").
C’est problématique, éviter de toucher à ces mots-là ?…

##### Adjectifs

De nombreux adjectifs sont épicènes terminés par `{-e}`
(ex: "épicène, mélomane, capable, lâche, veule, facile, rare,
illusoire, provisoire, temporaire, vulgaire…").
Il me semble indésirable de
commencer à marquer leur genre dans l’orthographe,
alors que ni la langue orale ni l’orthographe actuelle ne le font.
Ci-dessus, on se limite donc aux noms communs.
Mais se pose alors la question inverse:
de nombreux adjectifs sont épicènes à l’oral
mais marqués en genre à l’écrit,
faut-il ajouter un `{-e}` à leur forme masculine ?
Ça devient infernal…

Plus parcimonieusement, on peut peut-être
aligner au moins certaines terminaisons, par exemple :

+ `{-oir(e)}` et `{-air(e)}`:
  la quasi-intégralité des adjectifs avec ces terminaisons
  s’écrivent avec un `{-e}` épicène,
  les rares exceptions sont "noir, rasoir, clair, (im)pair",
  qu’il faudrait donc rendre épicènes;
+ `{-eul(e)}`:
  "seul, peul / veule, bégueule, casse-gueule"
+ `{-oul(e)}`:
  "soul, maboul, tamoul / ∅"
+ `{-ul(e)}`:
  la grande majorité sont épicènes:
  "nul, cucul, faux-cul
  / (in)crédule, minuscule, majuscule, funambule, noctambule, somnambule"
+ `{-il(e)}`:
  la grande majorité sont épicènes:
  "bissextil, civil, gentil(!), puéril, vil, viril, volatil, subtil
  / facile, difficile, débile, indélébile, (im)mobile, habile, agile, gracile,
  fébrile, fertile, fossile, fissile, docile, servile, stérile, tactile,
  textile, versatile, tranquille(!), -phile………"
  (l’écriture de "volatil, subtil / facile, tactile…"
  est particulièrement incohérente,
  même d’un point de vue étymologique !)
+ `{-ol(l)(e)}`:
  la majorité sont épicènes:
  "espagnol, aérosol, antivol, mongol
  / mariole, frivole, bénévole, créole,
  agricole, arboricole, ostréicole et nombreux autres -icole"
+ `{-el, èle, elle}`:
  la quasi-intégralité sont non-épicènes,
  les rares exceptions sont:
  "femelle, rebelle, imbelle, (in)fidèle, isocèle, parallèle, frêle, grêle"
  (l’écriture de "bel(le) / rebelle, imbelle" est particulièrement incohérente)
+ `{-al(e)}`:
  les deux cas sont fréquents mais,
  si on exclut les masculins qui deviennent `{-aux}` au pluriel,
  le cas épicène est majoritaire;
  il pourrait être judicieux de rendre graphiquement épicène
  les quelques adjectifs qui ne font jamais `{-aux}` au masculin pluriel:
  "banal, bancal, fatal, fractal, naval, natal, halal, rital"
+ `{-c, -que}`:
  cf noms communs

Et que faire des mots qui sont à la fois noms et adjectifs ?
(ex: "public, plastique, rasoir, honoraire, athée")
Non seulement un adjectif se convertit facilement en nom
(ex: "un amateur"),
mais il y a aussi au contraire
des noms employés comme qualificatifs
(ex: "un logement poubelle"),
en particulier les adjectifs de couleurs
(ex: "bouteille, isabelle, argile, auburn")
et les expressions composées
(ex: "grande-gueule, faux-cul, antivol, hors-sol")
qui suivent leurs propres règles d’accord…

##### Contre-proposition radicale : systématiser l’épicène

On pourrait, au contraire, envisager de moins marquer le féminin:
n’ajouter de `{-e}` que quand il modifie la prononciation
(et donc enlever plein de `{-e}` actuels).
Ceci dans le but de rendre l’écrit **épicène** quand l’oral l’est déjà,
ce qui évite non seulement les dilemmes ci-dessus,
mais également, dans certains cas,
le recours à des graphies inclusives (points médians, etc.)
et les prises de bec qui en découlent.
Il s’agit d’un changement majeur vis-à-vis du système actuel
de la langue écrite, où le genre grammatical est une notion centrale.
Précisons donc qu’une fois de plus, ceci ne modifie pas réellement la langue
(hormis poésie classique etc.);
toutefois, cela prépare le terrain pour une évolution plus épicène de celle-ci.

p.ex: "un(e) écrivain(e) fameux(se)"
mais #"cet auteur réputé" (écrit de la même façon quel que soit le genre,
au lieu de "cet(te) auteur(e) réputé(e)").

Cette évolution faciliterait également une solution radicale
au problème des consonnes finales non-muettes
(cf sections Consonnes muettes finales),
qui serait d’ajouter `{-e}` pour forcer la prononciation des consonnes
(ex: #"un contacte directe, un laque de montagne, un gangue rival").

Pour les graphies inclusives à la point médian,
cf aussi section Point.

#### Accord des couleurs

En grammaire française, les adjectifs de couleur sont un monde à part.
La règle énoncée est la suivante :

 1. Les *adjectifs de couleur « simples »*
    s’accordent régulièrement en genre et en nombre
    (ex: "blanc, noir, gris, bleu, jaune, **rose, mauve, pourpre**").
    - Notons que les couleurs  simples »
      incluent des couleurs aussi communes que
      "aubère, tourdille, incane, fauve, opalin, zinzolin…"
 2. Les *noms communs ou propres employés comme adjectifs* de couleur
    sont invariables
    (ex: "**orange (fruit), marron (fruit)**, prune (fruit), banane (fruit),
    magenta (nom propre arbitraire)").
 3. Les adjectifs de couleur *empruntés à des langues étrangères*
    sont invariables
    (ex: "auburn, kaki").

Mais il y a des incohérences :
certains mots de couleurs sont considérées comme « simples »,
et donc s’accordent,
alors qu’ils sont sont eux aussi issus de noms communs,
notamment "rose (fleur), mauve (fleur), violet (fleur), châtain (fruit)…"
(et pour des origines plus anciennes :
"pourpre, écarlate, zinzolin…").

Cette règle est nécessairement un peu incohérente
car elle concerne la limite à partir de laquelle
on commence à penser à un mot directement comme un adjectif,
plutôt que comme un nom commun utilisé comme adjectif
(ce qui est courant en français, pas uniquement pour les couleurs) ;
or cette limite est floue et évolue selon les époques,
avec la familiarité et le degré d’assimilation du mot.

La question à se poser est la suivante :
quand je lis "une chemise abricot",
je pense "une chemise de la couleur d’un abricot" (nom commun),
mais si je lis "une chemise rose/mauve/violette",
je pense directement à la couleur rose/mauve/violette (adjectif),
pas à la fleur !
La plupart des gens ignorent même que "la mauve" est une fleur…
Maintenant, avec "orange" ou "marron",
c’est comme avec "rose",
je ne passe pas par le fruit pour comprendre la couleur,
et je doute que quiconque le fasse !
Au 21e siècle "orange, marron" sont des couleurs *élémentaires*,
aussi banales que "rose",
elles font partie du vocabulaire d’un enfant de 4 ans
et sont vraisemblablement apprises
avant les noms des fruits correspondants.
Rien d’étonnant à ce qu’on fasse souvent
la « faute » de les accorder en nombre.
Il est grand temps de les naturaliser !

De même,
qui sait qu’avant d’être une couleur,
Magenta était une ville italienne,
théâtre d’une bataille de la guerre d’indépendance italienne ?…
Cela aide-t-il si peu que ce soit à comprendre la couleur désignée ?
(pas du tout du tout).

À l’inverse,
qui sait que "fauve" était une couleur
*avant* d’être un animal ?
et donc que cette couleur s’accorde,
contrairement à "orange"…

Concernant les mots étrangers :
ça suit la tendance générale
selon laquelle les emprunts étrangers sont souvent invariables,
car issus d’un système différent de celui du français
(ex: "linguine, vegan").
Mais au fur et à mesure qu’ils sont assimilés,
on les adapte au système français
(par exemple, la réforme de 1990 régularise de nombreux pluriels étrangers),
et on finit par ne plus les percevoir comme étrangers.
Quand des mots sont implantés en français depuis plus d’un siècle,
ne sont plus perçus comme étrangers,
et pourraient sans souci de graphie s’accorder en genre et en nombre,
il n’y a vraiment aucune raison de ne pas le faire…
C’est notamment le cas de "kaki" (du persan, via l’anglais colonial)
et de "auburn" (de l’anglais).

Pour résumer :

> + "orange" --> "orange(s)"
> + "marron" --> "marron(s)" voire #"marron(ne)(s)"
> + "magenta" --> #"magenta(s)"
> + "auburn" --> #"aubeurne(s)"
> + "kaki" --> #"kaki(e)(s)"
> + …

Petite difficulté pour certaines couleurs ("marron, cyan") :
l’invariabilité en genre s’entend à l’oral,
et donc on ne peut pas accorder en genre sans modifier l’oral.
Notamment,
on ne dit jamais "une encre #cyanne",
et il est très rare de dire "une chemise marronne"
(mais ça existe effectivement,
et on pourrait tout à fait répandre cet usage ;
on dit bien "châtain" issu de "la châtaigne",
"violet" issu de "la violette"…).
Que ça ne nous empêche pas, au moins, d’accorder en nombre :
"des chemises marrons, des encres cyans".

Note:
Il y a une homonymie entre
"marron" (le fruit et la couleur, mot issu de l’italien)
et "marron" (désignant un animal redevenu sauvage ou un esclave en fuite, mot issu d’une langue caraïbe) ;
ce 2e mot s’accorde en genre (ex: "une jument marronne")…

Note :
Bien qu’issu du grec,
le mot "cyan" n’est employé pour désigner une couleur
que depuis le 19e siècle,
avec l’essor de l’imprimerie en couleur.
Cette récence explique sans doute
pourquoi il n’est pas accordé au féminin.

Note :
Il y a une homonymie entre
"kaki" (la couleur, mot issu du persan)
et "kaki" (le fruit, mot issu du japonais) ;
la couleur ne vient pas du fruit.

#### Accord de "tout"

"tout" s’accorde selon un ensemble inextricable de règles.
Pour commencer, il convient de distinguer
les différentes natures grammaticales de "tout".

 1. "tout" employé comme *déterminant* ou *pré-déterminant*
    (c’est-à-dire avant un autre déterminant, comme dans "tous les")
    s’accorde. p.ex:
    - "toute demande ou réclamation"
    - "toutes taxes comprises"
    - "toute la farine"
    - "tous les jours"
    - "toutes les après-midis"
    - "toute une aventure"
    - "tous ces engrenages"
    - "toute cette machinerie"
    - "toutes leurs lois"
 2. "tout" employé comme *pronom* ou *pré-pronom* s’accorde. p.ex:
    - "tous ont tort"
    - "toutes ont accouché"
    - "ils ont tous tort"
    - "tous ceux"
    - "toutes celles"
 3. "tout" employé comme *adverbe*, signifiant « très, entièrement »
    (renforçant soit un adjectif, soit un nom utilisé comme attribut,
    soit un adverbe ou locution adverbiale)…
     1. ne s’accorde pas (car un adverbe est invariable, n’est-ce pas ?); p.ex:
        - "elles parlent tout doucement" (renforce l’adverbe "doucement")
        - "elles sont tout en haut" (renforce la locution adverbiale "en haut")
        - "elles sont tout sourire" (renforce le nom "sourire" utilisé comme attribut)
        - "elles se sentent tout chose" (idem)
        - "elle court tout feu tout flamme" (idem)
        - "elle est tout émue" (renforce l’adjectif "émue")
        - "elles sont tout émues"
        - "ils sont tout contents" = ils sont très contents ("tout" est adverbe)
          + ≠ "ils sont tous contents" = chacun d’entre eux est content ("tous" est pronom ~ `[tus]`) !
     2. **_excepté_ avant un *adjectif* féminin (pas un nom)
        commençant par une consonne ou un H aspiré**,
        auquel cas "tout" s’accorde; p.ex:
        - "elle est toute contente"
        - "elles sont toutes contentes" = elles sont très contentes ("toutes" est adverbe)
          + indifférenciable de "elles sont toutes contentes" = chacune d’entre elles est contente ("toutes" est pronom)
     3. dans "tout en" ou "tout à"…
         1. si "tout" exprime une intensité,
            on n’accorde pas
            (logique, puisque c’est un adverbe et que de toute façon,
            "en/à" commence par une voyelle);
            p.ex:
            - "elle est tout en joie" = elle est très joyeuse
            - "elle est tout à la fois contente et déçue" = insiste sur "à la fois"
            - "je suis tout à vous" = je suis très attentive
         2. **_mais_ si "tout" signifie « entièrement, en totalité »**,
            l’usage hésite
            (car alors on peut voir "tout" comme un «adjectif détaché»
            qualifiant le sujet qui est en/à quelque chose… logique ?);
            p.ex:
            - "je suis toute à vous" = à vous je suis toute, toute ma personne est à vous (déclaration d’amour)
            - "elle est vêtue tout(e) en bleu" = tout son corps est vêtu de bleu
            - "une maison tout(e) en bois" = toutes les parties de la maison sont en bois
     4. dans "tout de",
        il semble que deux grammairiens diront deux choses différentes;
        suivre, selon gout personnel,
        soit l’invariabilité de l’adverbe,
        soit la même règle que devant les adjectifs commençant par une consonne (accorder au féminin),
        soit la même règle que devant "en/à" (distinguer intensité / totalité);
        noter que ce choix s’entend à l’oral; p.ex:
        - "elle est tout(e) de blanc vêtue"
        - "ses manières sont tout(es) de miel"
        - "une branche tout(e) de travers"
     5. dans "tout X que"…
         1. quand ça signifie "bien que Y soit X" (construction figée),
            "tout" s’accorde selon les règles précédentes
            mais (selon l’Académie)
            la règle voyelle/consonne s’applique aussi *devant un nom*(!); p.ex:
            - "tout absorbée qu’elle soit, elle entend la musique"
              (note: l’Académie emploie l’indicatif !)
            - "toute pensive qu’elle soit, …"
            - "tout pensifs qu’ils soient, …"
            - "tout avocate qu’elle soit, elle se permet des libertés"
            - "toute députée qu’elle soit, …" (!)
            - "tout députés qu’ils soient, …"
            - "tout votre amie qu’elle soit" ??? (les règles que je trouve ne disent rien sur ce cas)
            - ce n’est pas précisé, mais on peut supposer
              que la règle s’applique aussi à la variante "tout X soit-il/elle"
         2. quand ça signifie "en sa qualité de X" ou "Y est tellement X que"
            (simple cas particulier de l’expression "X que"
            où X est renforcé par l’adverbe "tout"),
            "tout" s’accorde selon les règles précédentes
            (donc devant un adjectif, mais pas un nom !):
            - "tout absorbée qu’elle est, elle va rater son bus"
            - "toute pensive qu’elle est, …"
            - "tout pensifs qu’ils sont, …"
            - "tout avocate qu’elle est, elle connait bien le droit"
            - "tout députée qu’elle est, …" (!)
            - "tout députés qu’ils sont, …"
            - "tout votre amie qu’elle est" ???
     6. *règle bonus:*
        dans toutes les règles ci-dessus concernant l’adverbe,
        certains préconisent d’accorder au féminin singulier (écrire "toute")
        mais pas au féminin pluriel (écrire "tout"),
        pour éviter l’ambigüité de sens sus-mentionnée; p.ex:
        - "elle est toute contente" mais "elles sont tout contentes" ???
        - "elle est toute en bleu" mais "elles sont tout en bleu"
        - "elle est toute de bleu vêtue" mais "elles sont tout de bleu vêtues"

Applications des règles qui précèdent:

+ dans la locution "tout autre":
   1. employé comme déterminant devant "autre"
      (signifiant alors "n’importe quel autre"),
      ça s’accorde; p.ex:
      - "tous autres exemples conviennent" (tous les autres exemples)
      - "toute autre personne se serait trompée" (toute personne autre)
   1. employé comme adverbe, renforçant l’adjectif "autre"
      (signifiant alors "très différent"),
      ça ne s’accorde pas; p.ex:
      - "elle, c’est une tout autre personne !"
      - "une tout autre paire de manches"
      - "de tout autres difficultés"
      - "la vie est tout autre ici"
+ divers adjectifs composés, où "tout" renforce l’adjectif:
  - "il est tout-puissant"
  - "ils sont tout-puissants" (≠ "ils sont tous puissants")
  - "elle est toute-puissante"
  - "elles sont toutes-puissantes" (quasi-indifférenciable de "elles sont toutes puissantes")
  - "les tout-petits"
  - "des cuisines tout-équipées"

(Passons sous silence les très nombreuses locutions
où "tout" est figé sous une forme ou une autre,
p.ex. "en tout cas, de toute façon" (pourquoi du singulier ?).)

Amusant: dans "il/elle est tout pour moi",
on peut comprendre "tout" comme adverbe ou comme pronom,
ce qui renverse entièrement le sens !

Voilà une tout autre plaie à enseigner que les pluriels en -X.
**La principale difficulté,** c’est la règle 3.2, en gras,
sur l’usage comme adverbe appliqué à un adjectif féminin.
Pourquoi cette règle ?
*D’habitude un adverbe est invariable*.
La graphie s’efforce donc de suivre ce principe le plus souvent possible.
Mais pas de chance ! dans la réalité orale,
devant un adjectif au féminin on prononce `[tut]`…
On peut encore éviter d’écrire "toute(s)"
devant un adjectif commençant par une voyelle ou un H muet,
car alors la liaison suffit à faire prononcer le `{-t}` final de "tout";
mais devant un adjectif commençant par une consonne ou un H aspiré, pas le choix !

En clair, il s’agit d’une altération *euphonique* de la graphie
pour coller à la prononciation réelle,
comme le `{-t-}` dans "a-t-on"
(cf section -S de l’impératif).

Elle permet de distinguer des nuances de sens
entre "tout" employé comme pronom, et "tout" employé comme adverbe
(ex: "ils sont tout contents" ≠ "ils sont tous contents"),
mais ça ne marche qu’au masculin !

Ce pseudo-accord est réellement boiteux parce que,
pour imiter le vrai accord en genre et en nombre,
au féminin pluriel on écrit "toute**s**"
alors que jamais, ni au féminin ni au masculin,
un `[z]` ne peut être prononcé
(ex: "elles sont toutes contentes" sans liaison `[z]` puisque suivi par consonne;
"elles sont tout émues" sans liaison `[z]` puisqu’il n’y a aucun `{s}` écrit;
"ils sont tout émus" idem).

**L’autre grande difficulté**
(qu’on peut raisonnablement passer sous silence à l’école,
puisque devant "en/à" ça n’a aucune incidence à l’oral),
c’est la distinction
entre "tout" exprimant l’intensité,
et "tout" exprimant la totalité.
Nos Immortels du siècle dernier (100 % masculins jusqu’en 1980)
fantasment à propos de la différence entre "tout à vous" et "toute à vous".
Cette distinction est en fait *très subtile voire complètement artificielle*.

 1. Même quand on parle de la *totalité* de quelque chose,
    ce n’est en général qu’une hyperbole
    (ex: "une femme toute en bleu" =
    à moins qu’elle porte une combinaison intégrale, avec gants et cagoule,
    recouvrant la moindre parcelle de peau et de cheveux,
    elle n’est pas réellement *toute* en bleu;
    "la façade est tout illuminée" =
    a-t-elle beaucoup d’illuminations, ou des illuminations "partout" ?
    à moins qu’on parle littéralement d’un mur d’ampoules, ça revient au même !
    "tout(e) à ses pensées, elle rata son bus" =
    était-elle "entièrement" absorbée par ses pensées,
    ou seulement "tellement" absorbée qu’elle rata son bus ? vous avez 4 H.
    "je suis tout à vous" = vous avez mon "entière" attention…).
 2. De plus, considérer "tout" comme [adjectif][wikip-adj] semble douteux:
    quel est l’état, la qualité (adjectif qualificatif)
    ou la relation ([adjectif relationnel][adj-rel-droits-humains]) qu’il décrit,
    que peut bien vouloir dire "je suis toute" ?
    Ça ne signifie pas "entière",
    mais "entièrement, totalement, intégralement…" qui sont des adverbes.
 3. Ajouter à ça que la distinction n’existe pas à l’oral,
    et ne peut être faite qu’au féminin…
 4. Et pourquoi faire cette distinction devant "en/à"
    mais pas devant les noms, adjectifs… ?
    (Certains le préconisent certainement.)

Enfin, la règle de "tout X que",
trouvée dans la 8e édition du dictionnaire de l’Académie française,
hum, bon.
C’est soutenu, donc rare à l’oral, donc je n’ai pas de certitude
sur les prononciations avérées de "tout(e) députée qu’elle est/soit".

[wikip-adj]: https://fr.wikipedia.org/wiki/Adjectif_en_français
[adj-rel-droits-humains]: https://www.tract-linguistes.org/non-lieu-pour-les-droits-humains/

Que peut-on faire ?

**Simplifications faciles:**

+ Supprimer la distinction entre "tout" exprimant l’intensité
  et "tout" exprimant la totalité.
  Vu les règles actuelles, ça revient à
  supprimer les règles pour "tout en", "tout à", et éventuellement "tout de";
  dans ces contextes, "tout" est maintenant invariable
  (ce qui n’a aucune incidence à l’oral hormis devant "de").
  - Tolérer l’accord au féminin devant "de",
    pour des raisons d’euphonie, *si* des locuteurs le prononcent effectivement.
+ Supprimer la règle de "tout X que";
  ce sont maintenant les autres règles qui s’appliquent,
  quel que soit le sens de l’expression
  (ce qui a une incidence à l’oral dans le cas d’un nom féminin
  quand l’expression exprime une contradiction "bien qu’elle soit X").
  - Tolérer l’accord au féminin devant un nom,
    quel que soit le sens de l’expression,
    pour des raisons d’euphonie, *si* des locuteurs le prononcent effectivement.
+ Simplifier la règle devant adjectif pour ne pas distinguer
  le cas où l’adjectif débute par une voyelle ou une consonne
  (ce changement n’a aucune incidence à l’oral).
  La règle devient donc:
  l’adverbe "tout", devant un adjectif féminin qu’il renforce, s’accorde.
  Ce changement crée quelques ambigüités à l’écrit
  (ex: "elles sont tout émues" ≠ "elles sont toutes émues").
  - Tolérer le non-accord.

Ces changements:

+ autorisent l’adverbe "tout" à être invariable *partout*,
  ce qui est beaucoup plus simple !
  à l’oral, ça autorise à ne pas prononcer l’euphonie du féminin,
  ce qui est donc une modification de la langue,
  mais on peut imaginer que certains locuteurs le font déjà
  (ex: "elle est tout bizarre" ~ `[tu.bi.zaR]`;
  noter aussi une tendance générale actuelle à ne plus marquer le féminin,
  p.ex: "elle est con",
  ou le fait que certains n’accordent plus du tout à l’oral
  le participe passé avec l’auxiliaire "avoir);
+ autorisent également un accord facultatif devant un adjectif féminin,
  pour des raisons d’euphonie.

**Modifications supplémentaires:**
Puisque, comme on a vu,
l’altération graphique de "tout"
n’est pas un véritable accord et ne sert qu’à simuler l’euphonie,
et qu’on n’entend jamais de `{s}` de pluriel,
on peut aller plus loin.
Plusieurs alternatives:

+ A - On peut décider que l’adverbe "tout"
  s’accorde *en genre mais pas en nombre*.
+ Au contraire,
  on peut décider que l’adverbe "tout" est toujours invariable,
  et suivre le même principe que dans "a-t-on" pour noter l’euphonie:
  #"elles sont tout t-contentes".
  Mais bizarre vis-à-vis des syllabes,
  et puis dans quel cas écrire ce `{t}` euphonique ?
  (#"ils sont tout t-émus, elles sont tout t-émues" ?)
  => Bof.
+ B - On peut décider que l’adverbe "tout" est toujours invariable,
  et utiliser une graphie alternative pour noter l’adverbe "tout"
  par opposition au déterminant/pronom/nom;
  p.ex. #"tout’" (cf section consonne muettes finales;
  bof car `{-t}` n’est pas toujours prononcé)
  ou #"toùt" (cf section diacritique de distinction des homographes).
  Alors, l’écrit (est compatible avec mais)
  ne précise plus la prononciation fluctuante de "tout",
  qui redevient une pratique exclusivement orale, comme les liaisons
  ou la prononciation de "huit, vingt, six, dix".
  Bonne chose ? À mon avis oui.
+ C - Plus drastique encore,
  on peut décider que l’adverbe "tout" est toujours invariable,
  ne pas le distinguer graphiquement,
  et ne pas noter l’euphonie du tout.
  Même remarque que B concernant le découplage écrit/oral.

Bénéfice: chacune de ces propositions
permet de lever l’ambigüité du pluriel
(adverbe signifiant "très" / pronom signifiant "chacun"),
non seulement au masculin comme actuellement
(ex: "ils sont tout contents" ≠ "ils sont tous contents")
mais aussi maintenant au féminin, du moins à l’écrit
(ex: "elles sont #tout(e) contentes" ≠ "elles sont toutes contentes").

En particulier, on peut oublier la «règle bonus» évoquée plus haut,
puisqu’on a maintenant une meilleure solution à l’ambigüité
(la règle bonus altérait la prononciation dans le cas du féminin pluriel,
p.ex: "elles sont tout contentes").

Avantages supplémentaires et inconvénients de A/B/C:

+ (avantage théorique de B/C)
  Respecte le grand principe qu’un adverbe est invariable.
+ (avantage théorique de B/C)
  Respecte le grand principe qu’on n’altère pas la graphie des mots
  selon leur contexte de prononciation.
  Décorrèle l’écrit des accidents de l’oral.
  - => (avantage pratique de B/C)
    Simplifie drastiquement les règles de l’écrit
    (pour l’adverbe, il ne reste plus que la règle générale des adverbes).
+ (inconvénient théorique de B)
  Distingue graphiquement le déterminant/pronom/nom "tout" de l’adverbe #"toùt",
  ce qui est contrariant
  car il s’agit plus ou moins du même mot employé de différentes façons
  (contrairement, par exemple, à "a/à", 2 mots qui n’ont rien à voir entre eux).
  - => (inconvénient pratique de B)
    Évaluation de la difficulté réelle:
    nécessite de constamment se demander, à l’écrit,
    si "tout" est adverbe ou pas;
    y compris au masculin singulier,
    où auparavant il n’y avait pas à réfléchir,
    et où il y a maintenant une nouvelle opportunité de faute (artificielle).
+ (avantage pratique de A)
  Évaluation de la difficulté réelle:
  il n’y a pas à se demander si "tout" est adverbe.
  La question ne se pose qu’au pluriel,
  et se résout en réfléchissant au sens.
  Au masculin pluriel, un locuteur se trompera rarement car la différence s’entend.
  Au féminin pluriel, on peut réfléchir en transposant au masculin.
+ (inconvénient pratique de A)
  L’absence de marque du pluriel, surtout entre deux mots qui la portent,
  peut être troublante… et créer un nouveau risque de faute
  (ex: "elles sont #toute contentes"…
  on pourrait inadvertamment écrire "elles sont #toute #contente").
+ (avantage pratique de A)
  Plus proche de l’orthographe actuelle.
  Compatible avec elle si on tolère l’accord au féminin pluriel.
+ ? (avantage pratique de A)
  On pourrait réfléchir à généraliser cette règle
  aux cas d’adjectifs employés comme adverbes (cf ci-dessous);
  en effet, l’oral distingue souvent le genre mais pas le nombre.

Garder à l’esprit que, même après simplification,
l’accord de "tout" restera un point difficile pour un élève.

Remarque: On peut trouver d’autres cas d’adverbes variables,
essentiellement dans le cas d’un adjectif employé comme adverbe;
dans ces cas, l’accord est assez hésitant…
(ex: "seule l’Histoire le dira, une porte grande ouverte,
des filles nouveau-nées / nouvelles-nées"…
[cf Wikipédia][wikip-adv-variable] pour plus de rigolade).

[wikip-adv-variable]: https://fr.wikipedia.org/wiki/Morphologie_de_l%27adverbe_en_français#Cas_particulier_des_adverbes_variables

Idées infructueuses:

+ Remarquons que, du fait de leur prononciation,
  on pourrait éventuellement différencier graphiquement
  "tous" employé comme déterminant (prononcé `[tu]`)
  de "tous" employé comme pronom (prononcé `[tus]`, le `{-s}` final est sonore),
  le 2nd pourrait p.ex. se réécrire #"touss"
  (cf section consonnes muettes finales).
  Ainsi, si on décide que "il sont tout contents"
  s’écrit plutôt "ils sont #tous contents",
  ça ne peut plus être confondu avec "ils sont #touss contents".
+ Malgré ça,
  on ne peut pas décider que "tout" est un adverbe spécial
  qui s’accorde tout le temps,
  car cela contredirait l’oral:
  on prononce bien "ils sont tout habillés" avec une liaison `[t]`,
  incompatible avec la graphie "ils sont #tous habillés".

#### Accord du participe passé

> « Mignonne, allons voir si la rose  
> Qui ce matin avoit *desclose*  
> Sa robe de pourpre au Soleil,  
> (…) »  
> ~ Pierre de Ronsard, _Odes_, 1545

> « Enfans, oyez vne Lecon:  
> Nostre Langue à ceste facon,  
> Que le terme, qui va deuant,  
> Voulentiers regist le suiuant.  
> Les vieilz Exemples ie suiuray  
> Pour le mieulz: car a dire vray,  
> La Chancon fut bien ordonnee,  
> Qui dit, m’Amour vous ay donnée:  
> Et du Basteau est estonné,  
> Qui dit, m’Amour vous ay donné.  
> Voyla la force, que possede  
> Le Femenin, quand il precede.  
> Or prouueray par bons Tesmoings,  
> Que tous Pluriers n’en font pas moins:  
> Il fault dire en termes parfaictz,  
> Dieu en ce Monde nous a faictz:  
> Fault dire en parolles parfaictes,  
> Dieu en ce monde, les a faictes.  
> Et ne fault point dire (en effect)  
> Dieu en ce Monde, les a faict:  
> Ne nous ha faict, pareillement:  
> Mais nous a faictz, tout rondement.  
> L’italien (dont la faconde  
> Passe les vulgaires du Monde)  
> Son langage a ainsi basty  
> En disant, Dio noi a fatti.  
> Parquoy (quand me suys aduisé)  
> Ou mes Iuges ont mal visé,  
> Ou en cela n’ont grand science,  
> Ou ilz ont dure conscience. »  
> ~ Clément Marot, _Épigrammes_, 16e siècle

> « Clément Marot a ramené deux choses d’Italie, la vérole et
> l’accord du participe passé. Je pense que c’est le deuxième
> qui a fait le plus de ravages ! »  
> ~ Voltaire.

(pour ceux qu’un argument d’autorité convainc davantage…)

Ça dépasse le champ de l’orthographe pour entrer dans celui de la grammaire,
mais on le mentionne ici à fin d’exhaustivité:
l’accord du participe passé est de l’avis général
l’un des pires fléaux de l’écriture du français.

La cause de la règle d’accord du participe passé avec le verbe "avoir"
(après, mais pas avant)
relève d’une complexité accidentelle comparable à celle des pluriels en -X
(cf l’explication historique par Arnaud Hoedt et Jérôme Piron,
dans _La Faute de l’orthographe_,
à propos des "pieds que Jésus a lavés") ;
on l’a prise à l’italien, qui l’a depuis assouplie.
Cette règle entraîne des complications sans fin
avec les verbes réfléchis ou pronominaux
(14 pages d’exceptions dans _Le Bon Usage_ de Grevisse).
À l’école, un temps démentiel est gaspillé à enseigner
— et stigmatiser les élèves avec —
ces règles complexes jusqu’à l’absurde,
avec un succès malgré tout mitigé.

C’est pourquoi le problème a été soulevé
dès l’instauration de l’école publique sous la IIIe République française,
et est depuis repris par tous les réformistes
(comme les pluriels en -X, les consonnes doubles et les lettres grecques).
Espoir:
le sujet semble connaitre un peu de dynamisme
depuis la fin des années 2010,
car la dernière proposition en date a reçu quelque attention médiatique
et le soutien d’institutions importantes
(le CILF, la fédération Wallonie-Bruxelles,
les associations belge et québécoise des professeurs de français),
outre celle de nombreux lingüistes.

[Ladite proposition][accord-pp-proposition]
rend le participe passé tout simplement invariable avec l’auxiliaire "avoir".
(Voir aussi l’instructif [article de Libération][accord-pp-libe],
[celui de l’Actualité][accord-pp-libe]
qui appuie le propos avec des chiffres,
et [celui de la revue TRACeS][accord-pp-traces] :
les règles tordues ne concernent
qu’une petite minorité des formes verbales écrites et orales,
et le non-accord est déjà pratiqué par certaines personnes.)

Le plus logique
(et ce qui était plus ou moins l’usage initial)
était de toujours accorder le participe passé,
considéré comme un adjectif.
Mais…

+ D’une part il n’est pas toujours évident d’identifier
  l’objet auquel s’applique le participe
  (ex: "la tarte que j’ai faite" → "j’ai fait \<la tarte\>"
  / "la tarte que j’ai fait cuire" → "j’ai fait (en sorte) \<que la tarte cuise\>" ;
  l’« objet » est ici un verbe ou plutôt, implicitement,
  une proposition subordonnée,
  qui par convention s’accorde au masculin singulier !
  / bonus: "la tarte que je me suis faite…") ;
  contrairement à ce qu’on aimerait enseigner,
  ce n’est pas donné mécaniquement
  par une fonction grammaticale telle que « le COD du verbe »
  (exemple : ?).
  - Parfois l’accord se fait en fonction d’un mot implicite,
    qui n’existe que dans l’esprit du locuteur et pas dans la phrase
    (ex: "Paris-Marseille, je l’ai pris(e) hier", le train / la ligne).
+ D’autre part,
  avec l’auxiliaire "avoir",
  l’habitude est désormais solidement ancrée de ne pas accorder
  (puisque c’est de loin le cas le plus courant).
  On pense alors au participe passé comme à un verbe,
  plus que comme à un adjectif.
+ On est de toute façon obligé
  de considérer le participe passé comme une forme verbale,
  car on ne peut *pas du tout*
  voir un temps composé comme un temps simple suivi d’un adjectif,
  ça se casse la figure à la moindre indication temporelle
  (ex: "la voiture que j’ai achetée" → on peut à la rigueur,
  avec un nœud au cerveau,
  voir ça comme "la voiture achetée que j’ai",
  càd. je possède une voiture, et elle est dans l’état « acheté »;
  mais "la voiture que j’ai achetée hier" → ???
  pire: "j’ai revendu ce matin la voiture que j’ai achetée hier" →
  je ne l’ai même plus, cette voiture !).

D’où la proposition actuelle
de ne plus accorder du tout avec l’auxiliaire "avoir".

Deux choses à noter:

+ Contrairement aux autres points soulevés dans ce document
  (qui au pire altèrent des liaisons),
  une modification de l’accord du participe passé
  a une incidence sérieuse sur l’oral
  (ex: "la tarte que je me suis fait**e**").
  En pratique cette prononciation,
  que la norme actuelle considère incorrecte,
  est déjà réalisée par certains locuteurs,
  [et pas toujours des moindres][accord-pp-traces],
  de façon occasionnelle ou systématique ;
  dressez les oreilles, écoutez autour de vous,
  vous en entendrez.
+ L’accord du participe passé,
  comme tout accord grammatical,
  permet parfois de lever une ambigüité de sens
  (ex: "la mort de l’homme que j’ai tant désiré" → ce que je désirais, c’était l’homme
  / "la mort de l’homme que j’ai tant désirée" → ce que je désirais, c’était sa mort…
  exemple [tout de même][mort-désirée-1] assez [artificiel][mort-désirée-2],
  il n’y a aucune différence orale,
  il n’y aurait aucune différence si on parlait d’une femme et,
  dans le second cas, l’ordre des mots est peu naturel !)…
  Mais les ambigüités sont omniprésentes dans les langues y compris en français,
  elles se lèvent par le contexte
  et, au pire, on peut toujours reformuler.

Ô raffinements subtils de l’écrit:

- "la voiture que j’ai vue percutée (par un camion)" =
  la voiture qui a été percutée (mode passif, accent sur la voiture)
- "la voiture que j’ai vue percuter (un cycliste)" =
  la voiture qui a percuté
- "la voiture que j’ai vu percuter" =
  la voiture qui a été percutée (mode actif, accent sur le sujet indéfini qui l’a percutée) (!)

[accord-pp-proposition]: http://www.participepasse.info/
[accord-pp-libe]: https://www.liberation.fr/debats/2018/09/02/les-crepes-que-j-ai-mange-un-nouvel-accord-pour-le-participe-passe_1676135/
[accord-pp-actu]: https://lactualite.com/societe/il-est-temps-de-reformer-le-participe-passe/
[accord-pp-traces]: https://changement-egalite.be/requiem-pour-le-participe-passe/
[mort-désirée-1]: https://www.tract-linguistes.org/la-fin-de-laccord-du-participe-que-jai-tant-desiree/
[mort-désirée-2]: https://www.tract-linguistes.org/la-mort-de-lhomme-que-jai-desire-e-arreter-la-supercherie-et-retrouver-la-raison/



### Majuscules

> Je ne capitule pas.  
> ~ _Rhinocéros_, 1959

Tout en évitant de s’aventurer trop avant en terre typographique,
(_hic sunt dracones_ ! péril mortel !)
remarquons que l’opposition majuscules/minuscules
relève encore de l’orthographe.
Ainsi, écrire une minuscule au lieu d’une majuscule ou vice-versa
est une FAUTE.
Oui, une FAUTE.

[règles-maj]: https://fr.wikipedia.org/wiki/Usage_des_majuscules_en_français
[règles-maj-simplif-géo]: https://fr.wikipedia.org/wiki/Usage_des_majuscules_en_français#Usage_simplifié
[règles-maj-simplif-titres]: https://fr.wikipedia.org/wiki/Usage_des_majuscules_en_français#Règles_simplifiées

Heureusement les [règles][règles-maj]
sont univoques, simples, connues de tous
et appliquées sans faute en toute circonstance.
Elles ne suscitent donc jamais
d’admonestations orthographiques
ni de guerres d’édition futiles. <!--
https://fr.wikipedia.org/wiki/Discussion:Hégire#Révocations
https://fr.wikipedia.org/wiki/Discussion_Wikipédia:Conventions_typographiques/Archive_11#Question_de_typographie_pour_«_armée_française_»_et_plus_généralement_pour_les_armées_nationales
https://fr.wikipedia.org/wiki/Discussion:Président_de_la_république_populaire_de_Chine
-->
Par exemple
(ce sont des VRAIS exemples,
écrits avec les VRAIES règles françaises) :

<!--
***ARR2TONS DE SE PRENDRE LA TETE AVEC LES MAJUSCULES***

* LES MAJUSCULES N’2TAIENT PAS CENS2ES CR2ER DES PROBL7MES
* des SI7CLES DE RECHERCHES
  mais on n’a toujours pas TROUV2 LE E-ACCENT-AIGU-MAJUSCULE
* tu veux séparer des phrases?
  On A Déjà Un Outil Pour: ça s’appelle le «POINT»
* « Oui, donnez-moi des haut-de-casse et des cadrats.
  Donnez-m’en treize en grosse nompareille. »
  — énoncés fantasmés par les typographes dérangés

REGARDEZ Ce Que Les Typographes Vous Ont Obligé A Respecter Tout Ce Temps,
Avec Tout Le Papier De l’Amazonie
(VRAIS titres d’Œuvres, avec les VRAIES règles Françaises):
-->

* saint Pierre
* Docteur Frankenstein
* le docteur Frankenstein
* l’Organisation des Nations unies (l’ONU)
* l’Organisation du traité de l'Atlantique Nord (l’OTAN)
* la Société nationale des chemins de fer français (la SNCF)
* les États du Golfe
* le golfe Persique (ou golfe Arabo-Persique)
* l’océan Glacial arctique (ou océan Arctique)
* la place Rouge
* l’Armée rouge
* l’armée française
* l’Empire romain
* le Royaume uni des Pays-Bas
* le Royaume-Uni de Grande-Bretagne et d’Irlande du Nord
* la fédération de Russie
* les États-Unis d’Amérique
* la Révolution française
* la révolution verte
* la révolution des Œillets
* la monarchie de Juillet
* la Guerre folle
* la guerre de Sept Ans
* la Première Guerre mondiale
* le Premier ministre
* l’île Vierge
* _L’Île mystérieuse_
* _Les Trois Mousquetaires_
* _Le Vilain Petit Canard_
* _Le Petit Livre rouge_
* _Quel petit vélo à guidon chromé au fond de la cour ?_
* _Sept Ans de réflexion_
* _À la recherche du temps perdu_
* _W ou le Souvenir d’enfance_
* _Pique-nique à Hanging Rock_
* _Vingt-quatre heures de la vie d'une femme_
* _24 Heures chrono_
* _Un jour sans fin_
* _Le Jour de la marmotte_
* _Un tramway nommé Désir_
* _Le Train bleu_
* _Le train sifflera trois fois_
* _À la poursuite d'Octobre rouge_
* _À l’Ouest, rien de nouveau_
* _À l’ouest d’octobre_
* _De Cape et de Crocs_

En cas de doute,
on pourra d’ailleurs puiser aux meilleurs sources,
en consultant la couverture ou le dos
de n’importe quel livre de sa bibliothèque.
Car qui, sinon les éditeurs eux-mêmes,
serait plus à même de montrer le droit chemin typographique ?

<!--
«Salut je voudrais une ᴅᴏᴜᴢᴀɪɴᴇ ᴅᴇ Gᴏʟᴅᴇɴ en petites capitales.»
**Ils se sont COMPL7TEMENT payé notre fiole**
-->

Bref…

Note culturelle:
Le latin n’avait que des capitales.
C’est dans les monastères du Moyen-Âge
que sont nées les minuscules,
sans doute pour gagner de la place
(le papyrus puis le parchemin coûtaient très cher)
et écrire plus vite.
D’autres systèmes d’écriture se passent très bien de la distinction,
comme le latin antique ou l’[Initial Teaching Alphabet][ita],
sans parler de l’arabe, du tamoul, du japonais ou du coréen.

Employer une écriture bicamérale n’est pas sans coût.
Ça implique qu’il faut apprendre
non pas un, mais deux symboles pour chaque lettre de l’alphabet.
Ça complique leur saisie informatique
puisqu’il faut deux fois plus de caractères,
qu’il faut faire des contorsions digitales au clavier,
et que les majuscules diacritées restent un problème en 2024
(ce qui est d’ailleurs parfaitement risible).

[ita]: https://en.wikipedia.org/wiki/Initial_Teaching_Alphabet

Les principaux usages des majuscules en français :

* les sigles (ex: "TGV") ;
* les noms propres (ex: "la Chine, Pékin, Jules César") ;
  d’où on fait découler d’autres usages :
  par exemple les titres de civilité
  (ex: "Docteur Frankenstein, cher Monsieur"),
  les noms des événements historiques
  (ex: "la Seconde Guerre mondiale, les Trente Glorieuses"),
  les titres d’œuvres
  (ex: "_La Cantatrice chauve_")…
* l’initiale des phrases.

L’usage en début de phrase ne sert à rien
(voir plus loin).

L’usage pour les sigles semble utile,
encore qu’on pourrait s’interroger sérieusement
sur la redondance entre les majuscules et les points
pour signaler une abréviation
(cf aussi section Point).

L’usage pour les noms propres semble également très utile,
car de nombreux noms propres sont également noms communs ou adjectifs,
(ex: "Clément, Constance, Julien, Auguste…"),
et aussi parce que de nombreux noms propres
sont formés en particularisant des noms communs
(ex: "les Trente Glorieuses, les Événements, la République,
le journal _Le Monde_", les titres d’œuvres).
Il permet de distinguer des sens homonymes
(ex: "Homme/homme, Terre/terre, État/état, Église/église").
Cependant, comme on le voit plus haut,
l’usage des majuscules est à géométrie variable
et les règles d’une complexité aberrante.
Il est impératif de simplifier ce micmac !
D’ailleurs plusieurs simplifications,
pour des catégories particulières,
sont déjà en usage :

* pour les noms de pays :
  une [simplification en 1993][règles-maj-simplif-géo]
  (recommandée par l’Académie française et l’OQLF,
  suivie par certains journaux) ;
  on capitalise systématiquement les noms génériques de pays
  (ex: "la Fédération de Russie") ;
* pour les titres d’œuvres :
  un [usage alternatif][règles-maj-simplif-titres]
  (qui n’est pas que le fait
  d’une plèbe ignorante de l’Art subtil des majuscules,
  mais est également recommandé par certains typographes
  au 21e siècle) ;
  on ne capitalise que le premier mot du titre…
  tout simplement !
  (ex: "_Le vilain petit canard_, _Les misérables_")

On peut aller beaucoup plus loin,
en uniformisant les usages des diverses catégories
(noms de pays, autres noms géographiques,
noms d’événements, titres d’œuvres…) ;
par exemple, on pourrait tout simplement décréter
que tous les « mots » d’un nom propre prennent une majuscule,
à l’exception éventuelle des articles et prépositions
(comme en anglais, donc).

Et, bien, sûr,
il faudrait cesser de croire qu’il existerait un Bon Usage unique
duquel tout écart serait une faute…

#### Gentilés et noms de langues

La langue française considère
"le Français" (le gentilé) comme un nom propre
et "le français" (la langue) comme un nom commun.
Ça devrait être exactement le contraire !

* "un Français" (gentilé) est un nom porté en *commun*
  par chaque individu ayant un rapport avec la France
  (tout comme "un loup, une personne, une ville, un pays, une planète"
  sont des noms communs à des catégories de sujets) ;
* "le français" (langue) est un nom qui est *propre*
  à une seule langue, une entité en particulier dans l’univers
  (tout comme "_Canis lupus_, Jules César, Rome, l’Empire romain, la Terre"
  sont des noms propres à une espèce, une personne, une ville, un pays,
  une planète particulières).

Certes, le gentilé peut s’employer sur un mode générique
pour désigner l’ensemble des individus de la catégorie
(ex: "le Français est râleur et mange du pain"),
et acquiert alors un usage comparable à celui d’un nom propre,
mais ce type d’emploi est en fait possible
avec n’importe quel nom commun
(ex: "le loup chasse en meute,
la ville est un espace social,
le thé se boit chaud ou froid").

Une raison qui peut justifier que les gentilés soient des noms propres,
c’est que certains gentilés ne dérivent pas d’un nom propre de nation
(ex: "un Peau-Rouge, un Micmac, un Osage, un Han") ;
au contraire c’est la nation qui est désignée par le gentilé au pluriel.
Contre-argument néanmoins :
comment délimiter ce qui est un gentilé et ce qui n’en est pas,
ou plutôt ce qui prend une majuscule et ce qui n’en prend pas ?
(ex:
"romanichel, Rom, juif, Chrétien, Blanc",
qui ne sont pas associés à des territoires ?
"Arabe" ?
"sudiste", habitant du Sud de la France,
qui n’a pas de statut administratif ?…).

Un intérêt de mettre des majuscules aux gentilés,
ce serait peut-être de distinguer certains mots exotiques 
et donc atypiques dans la langue,
qui pourraient autrement créer de la confusion
(ex: "Han, Peul, Osage")…
Mais pourtant on ne prend pas cette précaution
avec les autres mots de cultures exotiques
(ex: "aa, aï, bêt, dey, ney, fez, leu, peul…").

Même s’il peut se justifier de mettre une majuscule aux gentilés,
du moins il faut clairement (comme en anglais)
mettre une majuscule aux noms de langues.
La seule motivation pour écrire les langues en minuscules,
est de lever des ambigüités dans des phrases telles que
"le français/Français est tordu".
Ces exemples sont assez tordus,
l’ambigüité existe déjà à l’oral,
et on dirait plus clairement "les Français sont tordus"
ou "la langue française est tordue".

#### Points cardinaux

Concernant la majuscule à "nord, sud, est, ouest",
l’usage est extrêmement confus,
il est déraisonnable d’exiger des locuteurs
le respect d’une supposée norme orthographique.
La tendance générale se résume ainsi :
« le lieu prend une majuscule, la direction prend une minuscule… »
Mais cette règle est sujette à appréciation !
(ex:
"la route du sud" parce qu’elle va vers le sud,
ou bien "la route du Sud" parce qu’elle mène à la région qu’on appelle le Sud ?
"son cœur est tourné vers l’ouest" parce que c’est une direction,
ou "vers l’Ouest" parce qu’on parle d’une région, bien qu’elle soit vague ?
"le nord de Paris" parce que c’est ce qui se trouve au nord de la Seine,
ou bien "le Nord de Paris" parce que c’est une zone géographique ?).
On trouve aussi bien en usage courant
"le pôle Sud, l’hémisphère Sud"
(lieux uniques qui méritent bien un nom propre, n’est-ce pas ?)
que "le pôle sud, l’hémisphère sud"
(noms communs qui suffisent à identifier les lieux,
ces locutions n’ont pas de sens *propre*,
c’est-à-dire différent de "pôle/hémisphère" + "sud";
contrairement à "l’Afrique du Sud" par exemple,
qui n’est pas simplement le sud de l’Afrique,
mais un pays bien précis avec des frontières ad-hoc).

Il convient donc d’observer une tolérance totale
sur la casse des points cardinaux.
Et de clarifier une règle,
qui serait que ces mots ne prennent jamais de majuscule par eux-mêmes
(ex: "le pôle sud, le sud géographique, se trouve dans l’Antarctique"),
en revanche ils peuvent (comme n’importe quel nom commun)
en prendre une dans un nom propre (ex: "Afrique du Sud");
y compris comme antonomase,
dans des métonymies telles que "le Sud (de la France)".

#### Initiales de phrases

Utilité des majuscules initiales (débuts de phrase, de vers poétique, etc.) :

+ **Aucun.**
  À l’oral, les débuts de phrases n’ont pas de marqueur particulier.
  À l’écrit, les limites entre phrases — ou autres unités du discours —
  sont déjà signalées par
  des points,
  divers signes de ponctuation
  (on se passe très bien de majuscule après un point-virgule),
  des puces,
  des sauts de lignes
  ou tout un tas d’autres mises en forme selon les cas,
  comme la taille ou le soulignement pour les titres de section,
  le découpage en messages dans une messagerie instantanée…

Inconvénients des majuscules initiales :

+ usage en conflit
  avec celui des noms propres :
  quand un mot se trouve en début de phrase (ou titre, etc.),
  on ne sait pas si c’est un nom propre ou un nom commun.
+ rend pénible les citations :
  quand on commence une phrase par une citation,
  faut-il altérer la citation pour y mettre une majuscule ?
  Et vice-versa, quand on cite un début de phrase
  dans un milieu de phrase.
+ rend pénible la rédaction :
  quand je décide de couper une phrase en deux,
  ou au contraire de réunir deux phrases,
  je dois modifier l’initiale déjà écrite du 2e morceau.
+ Après un point d’interrogation ou d’exclamation,
  on est censé mettre une majuscule ou non, selon que la suite
  soit considérée comme la continuation de la phrase qui précède ;
  comment juger ? Quelle différence ça fait ?
+ suscite encore d’autres questions typographico-existentielles
  sans fin et sans le moindre intérêt :
  faut-il mettre une majuscule au début d’une ligne sans verbe
  (ex: "Inconvénients des majuscules initiales :") ?
  dans un diaporama ?
  aux items d’une liste ?
  aux éléments de légende d’une carte ?
  aux libelés d’un formulaire ?
  dans un diagramme ?…

Concernant le choc graphique
de ne plus mettre de majuscule aux initiales,
et le risque supposé de compréhension dégradée :
Aviez-vous déjà remarqué
que les panneaux de signalisation français n’en ont pas toujours ?
Avez-vous remarqué que certaines sections de ce document,
et même quelques titres de sections,
sont écrits sans majuscule ?
avez-vous remarqué l’usage incohérent
qui en est fait dans la présente section ?
Passé le manque d’habitude,
cela a-t-il si peu que ce soit entravé la compréhension ?
Quelle différence entre des mots désignant un lieu au moyen d’une flèche,
et des mots désignant quelque chose au moyen de deux-points,
ou la légende d’une image,
ou la légende d’une carte ?

Pour ma part, entre ça et la folie typographique, le choix est clair :
décapitons les phrases ! décapitons les vers !



### Point

On pourrait s’agacer que le même symbole
signale une fin de phrase et une abréviation
(cf section Majuscules)…
Ça complique parfois la lecture,
rend pénible l’emploi des correcteurs orthographiques,
et empêche même certaines conventions de mises en page,
à l’américaine,
où l’espace est légèrement plus large
entre les phrases qu’entre les mots
justement pour améliorer la lisibilité !

On pourrait tout à fait envisager
de remplacer le point « bas » habituel
par un point médian — ou autre signe — pour les abréviations ;
p·ex· : "adj· qual·, M· Martin, l’O·N·U·, atterré·e·s…"
(cf aussi section Marque du féminin)

(faudrait-il mettre un point
après la parenthèse fermante précédente ?
que de terribles questionnements !)



### Chiffres romains

À mort ! À mort les chiffres romains !

Combien de temps vous faut-il pour déchiffrer
« le XXVIIIe siècle » ?
« MDCCLXXXIX » ?
Arrivez-vous à bien distinguer le nombre de barres ?
Combien font XLIV−XV ?
Combien de mathématiciens romains connaissez-vous ?
Moi, zéro (zéro, nombre inconnu des Romains) ;
[Wikipédia en connait tout juste deux][wikip-math-romain],
dont l’un est un obscur astrologue (pouah !)
et l’autre est à l’origine de l’adoption des… chiffres arabes !

Les chiffres romains,
système redondant,
expression ultime de l’élitisme, de la pomposité !
Numération viciée,
où même une bête addition est compliquée !
Où les divisions sont un mystère !
Qui ne permet de compter que jusqu’à 4000 !
À laquelle la fameuse ingénierie romaine elle-même renonçait
quand il fallait calculer ! <!--
Quand il fallait calculer,
la fameuse ingénierie romaine abandonnait les chiffres romains
pour des abaques égyptiens ou grecs.
-->
Civilisation romaine,
sûrement habile à tracer des routes et aligner des soldats,
mais qui ne s’est jamais illustrée par ses mathématiques…
Le grand Archimède tué bêtement par un soldat romain :
tout un symbole !

Il faut détruire les chiffres romains !

[wikip-math-romain]: https://fr.wikipedia.org/wiki/Catégorie:Mathématicien_romain



### Compatibilité avec les langues étrangères

Les autres langues transcrivent souvent leurs sons en utilisant les mêmes
lettres latines que le français, mais d’une façon différente et incompatible,
même quand les sons existent en français. Il en résulte que pour un mot
d’emprunt dont on veut préserver (à peu près) la prononciation, soit il faut le
réécrire selon la graphie française et alors le mot n’est plus forcément
reconnaissable graphiquement (exemple inversé : sauriez-vous deviner quel mot
français est transcrit en suédois par "fåtölj" ?), soit (cas le plus courant
pour les emprunts récents) le mot est écrit tel quel et enfreint tous les
principes d’orthographe du français. Ce qui complexifie pas mal l’orthographe
(les emprunts sont en fait très fréquents… et pas que les anglicismes !), et
nécessite de savoir comment se prononcent ces mots dans leur langue d’origine.

Évidemment on ne peut pas supporter toutes les langues du monde avec une
orthographe universelle, mais ça ne veut pas dire qu’on ne peut pas améliorer
la situation. Une cause majeure de beaucoup d’incompatibilités entre
l’orthographe française et celle d’autres langues est que le français a modifié
la prononciation de plusieurs lettres par rapport au latin, en conservant les
lettres latines d’origine. Les lettres les plus fréquemment problématiques sont
`{u}` et `{e}` mais il y en a d’autres:

+ `{u}` ne se prononce plus `[u]` mais `[y]` (son absent en latin)
+ `{e}` ne se prononce plus `[e]` mais `[°]` (son absent en latin, typiquement français)
+ beaucoup de voyelles suivies de `{n}` sont nasalisées
  et le `{n}` n’est plus prononcé
+ consonnes finales muettes
+ `{s}` ne se prononce plus toujours `[s]` mais parfois `[z]`
+ `{g}` ne se prononce plus toujours `[g]` mais parfois `[Z]`
  (voire `[N]` dans le digramme `{gn}`)
+ `{q}` ne se prononce pas `[q]` (son inconnu en français comme en latin)
  mais `[k]` (son qui y ressemble)
+ `{qu}` ne se prononce plus que rarement `[kw]` mais en général `[k]`
+ de nombreuses successions de voyelles forment des n-grammes

Cela démarque le français de nombreuses autres langues qui utilisent ces mêmes
lettres avec leur son latin: non seulement des langues romanes comme
l’espagnol, mais aussi beaucoup de langues du monde qui ont ultérieurement
adopté l’alphabet latin, et même des langues qui n’utilisent pas l’alphabet
latin mais sont translitérées en alphabet latin suivant une norme
internationale: arabe, russe, chinois, japonais (qui a pourtant un ensemble de
sons particulièrement simple)… Souvent des mots du monde entier transitent par
l’anglais sans que leur orthographe soit adaptée pour le français
(ex: "telugu" ~ [telegu] n’est que rarement écrit "télougou";
"sensei" ~ [sEnsEj] et non [s@sE];
et pourquoi, pourquoi "Istanbul", cité millénaire d’une importance cruciale
aux portes de l’Europe, n’est-elle jamais écrite "Istamboul" ?).

Ainsi, face à la graphie d’un mot soupçonné d’emprunt,
il est difficile de deviner la prononciation de certaines lettres
(ex: comment se prononce "ginseng" ???).
Cas courants:

+ `{u}` : `[u]` ou `[y]` ?
+ `{e}` : `[e/E]` ou `[°]` ?
+ `{-er}` : `[-e]` ou `[-ER]` ou `[-9R]` ?
+ `{an}` : `[an]` ou `[@]` ?
+ `{in}` : `[in]` ou `[5]` ?
+ `{s}` : `[s]` ou `[z]` ?
+ `{g}` : `[g]` ou `[(d)Z]` ?
+ `{ch}` : `[k]` ou `[(t)S]` ou même `[x]` (allemand) ?
+ `{ei}` : `[ei/Ej]` ou `[E]` ? (on trouve `{ei}` ~ `[Ej]` en japonais)
+ `{ai}` : `[ai/aj]` ou `[E]` ? (idem japonais)

De plus, même quand on fait l’effort d’adapter la graphie d’un mot d’emprunt,
et que tous les sons voulus existent en français, il n’est pas toujours facile
de transcrire une succession donnée de sons ! (cf sections H, tréma, lettres
muettes). En effet l’orthographe française est assez contextuelle:
la prononciation de certaines lettres dépend du contexte orthographique,
comme `{e}`, `{s}`, `{g}`, `{c}`,
mais aussi `{p}` (à cause de `{ph}`),
n’importe quelle consonne en position finale (est-elle muette ?),
n’importe quelle voyelle (à cause des nombreux n-grammes),
ou encore la lettre `{n}` (à cause des voyelles nasales et de `{gn}` et `{ng}`)…
Sans oublier les liaisons.

Certaines autres propositions de ce document
facilitent la transcription de mots d’emprunt.
Par exemple on peut
adapter la graphie `{an}` quand elle n’est pas nasalisée (ex: #"tennismän"),
clarifier si une consonne finale est censée être prononcée
(ex: "jet d’eau / jet privé", le second se pourrait se réécrire #"djet’"), etc.
Mais ces nouvelles possibilités ne changent pas le fait que
des mots sont souvent repris sans adapter leur graphie…
(ex: "haïku" ~ [aiku] n’est qu’à moitié adapté !
le tréma laisse penser à tort que la graphie est francisée
et donc que `{u}` se prononcerait `[y]`, alors qu’il se prononce `[u]`)

On peut envisager de modifier *radicalement* le système orthographique français
pour se rapprocher de la phonétique latine, en adoptant pour les nouveaux sons
du français des notations moins ambigües trouvées dans d’autres langues:

+ `{u}` ~ `[y]` --> `#{ü}` (comme en allemand et turc; mais bof car autre signification du tréma, confusion avec `{ii}` en écriture cursive)
  - autres possibilités (à condition de dégager les usages existants du symbole concerné):
    une autre variante de `{u}` (tolérant les diacritiques comme le tréma);
    `#{y}` (comme en grec ancien, scandinave, API; ressemble à `{u}`);
    `#{w}` (ressemble à `{u}`)
+ `{ou}` ~ `[u]` --> `{u}` (comme en latin, espagnol, API) ou `#{ů}` (comme en tchèque)
+ `{e}` ~ `[°]` --> nouveau symbole à déterminer (son typiquement français; cf section accent plat)
+ `{é, è}` --> `{e}` (comme en latin, espagnol, API; cf section accent plat)
+ `{an}` ~ `[@]` --> `#{ã}` (similaire au portugais; cf section voyelles nasales)
  - idem pour les autres voyelles nasales: `#{ĩ/ẽ, ũ, õ}`
+ `{eu}` ~ `[ø/œ]` --> `#{ø}` (comme en scandinave et API; cf section EU)
+ `{(e)au}` ~ `[o]` --> `#{å}` (comme en scandinave)
+ `{ei}` ~ `[E]` --> `#{ǐè}` (cf section diacritique lettre muette)
+ `{s}` ~ `[z]` --> `{z}` (cf section S/Z)
+ `{g}` ~ `[Z]` --> `{j}` (cf section G/J)
+ `{gn}` ~ `[N]` --> `#{ñ}` (comme en espagnol; mais attention à l’autre usage du tilde, pour les voyelles nasales)
+ `{ch, sh}` ~ `[S]` --> `#{č, š}` (comme dans de nombreuses langues d’Europe)
+ …

Énormément de possibilités, mais modif extrêmement lourde !
C’est en tout cas un enjeu à garder à l’esprit lorsqu’on réfléchit à des modifications.

Inversement certains changements proposés,
en s’éloignant de l’étymologie au profit de la prononciation,
pourraient faire perdre en transparence et en intercompréhension
entre le français et les autres langues;
en premier lieu l’anglais,
_lingua franca_ mondiale
et langue de notre voisin favori,
qui nous a tellement emprunté
(ex:
le mot anglais "quality" est transparent pour un francophone
comme le mot français "qualité" l’est pour un anglophone;
si le second était réécrit #"calité",
ce serait moins certain).





<!------------------------------------------------------------->

## Inventaire des changements

Ci-dessous un inventaire **non-exhaustif** de modifications possibles.

* Modifications faciles qui n’altèrent pas radicalement l’aspect de l’écrit:

  + CUEIL, GUEIL
  + SH
  + CH (réécrire en `#{č}` ou `{çh}`)
  + **lettres grecques: FH, KH, Î**
  + suppression de H inutiles ("huile, huitre", etc.)
  + systématisation du tréma
  + suppression de circonflexes inutiles ("âme", passé simple, etc.)
  + "déjà"
  + "si"
  + "plus"
  + "quand/quant"
  + **accent plat**
  + -ER, -EZ, -ED
  + "et"
  + "très, près", etc.
  + "est"
  + E prononcé [a]
  + **EU prononcé [y] dans les formes du verbe "avoir"**
  + **Æ, Œ**
  + **voyelles nasales: -ENCE, -ENT, -EMMENT**
  + voyelles nasales: UN prononcé `[§ / On]`
  + voyelles nasales: AON prononcé `[@]`
  + voyelles nasales: EN- prononcé `[@n]`
  + **Y dans des n-grammes de voyelles**
  + **ILL prononcé `[il]` ("mille+, ville+, tranquille")**
  + MN
  + CC, CQU
  + SC, SÇ
  + TI, CTI
  + suppression des consonnes doubles sans effet phonétique
    (donc sauf SS, sauf consonnes doubles après E)
  + suppression de lettres muettes inutiles
    (ex: "poids?, arctique, automne, montgolfière…")
  + **pluriels en -X**
  + **"peux, veux, vaux"**
  + **-S des conjugaisons en -E et -A**
  + **-S de l’impératif**
  + **verbes en -DRE**
  + adjectifs de couleurs
  + marque du féminin: rectifications
  + **accord de "tout"**
  + écriture des nombres: 2, 6, 10, 11, 30, **accord de 100 et 20**
  + nombreuses autres corrections ponctuelles
    (ex: "aujourd’hui, corps, vieux, wagon, garçon, cauchemar…")

* Modifications non problématiques avec un impact visuel modéré:

  + GU --> GH
  + **suppression des consonnes doubles et ajout d’accent grave si nécessaire**
  + S/Z (nécessaire pour la modif précédente)

* Modifications non problématiques avec un impact visuel élevé:

  + QU --> Q
  + EU --> Ø
  + **voyelles nasales: EN --> AN
    dans de nombreuses familles où EN est toujours nasalisé**
  + **-S, -Z, -X**

* Modifications éventuellement polémiques:

  + supprimer tous les H non-aspirés
  + supprimer/remplacer tous les circonflexes
  + diacritique de distinction des homographes
  + diacritique lettre muette
    (nouveau symbole exotique, difficile à saisir au clavier)
  + AU, EAU
  + AI, EI
  + **voyelles nasales: remaniement global**
  + ILL (polémique car éventuelle perte d’info étymologique)
  + **G/J**
  + **Q/C**
  + marque du féminin: révolution épicène





<!------------------------------------------------------------->

## Exemples



### Français

(texte d’après Wikipédia)

Le Francèz èt une langhe indo-øropēène de la famiye des langhes
romanes dont les locutørs sont apelēs francofhones. Èle èt
parfoiz surnomēe la langhe de Moliēre.

Le Francèz èt parlē, en 2023 (døz-mil-vint-troiz), sur tous les
continants par environ 321 (troiz-cents-vint-ē-un) milions de
persones : 235 milions l’emploient cotidiènemant ē 90
(catrevints-disz) milions en sont des locutørs natifs. En 2018,
80 milions d’ēlēves ē ētudiants s’instruizent en Francèz danz le
monde. Selon l’Organizacion internacionale de la Francofhonie,
il pourait y avoir 700 milions de francofhones sur Tère en 2050
(døz-mil-cincante).

Le Francèz èt la cinqième langhe la plùz parlēe au monde aprèz
l’Anglèz, le Mandarin, le Hïndi ē l’Espagnol. Èle èt ēgalemant
la døzième langhe des afaires ē du comerce, la catrième langhe
emploiyēe sur Internett.

28 Ētats ont le Francèz come langhe oficièle.



### Orque — version minimale

+ la plupart des changements d’impact visuel faible, y compris
  ceux souvent identifiés comme prioritaires:
  - suppression des consonnes doubles sauf SS (y compris dans "mille")
  - pluriels en -X
  - lettres grecques
  - suppression des circonflexes inutiles
  - suppression des H non-aspirés

(texte d’après Wikipédia)

L’orque, ou épaulard, **èt** une espèce de mamifères marins du
sous-ordre des cétacés à dents, les odontocètes.

**Èle** a une répartition cosmopolite ; èle vit dans les régions
**artiques** é antartiques jusqu’aus mers tropicales. Son régime
alimentaire èt **tres** diversifié, bien que les populations se
spécialisent souvent dans des tîpes **particuliérs** de proies.
Certaines se **nourissent** de poissons, tandis que d’autres
chassent les mamifères marins, tels que lions de mer,
**fhoques**, morses é **mème** de grandes balènes (généralement
des **balèneaus**). Les orques sont considérées come des
superprédateurs. Les anglofhones les surnoment balènes tueuses
bien que le genre soit propre **aus** seules orques.

Les orques sont fortement sociables ; certaines populations sont
composées de plusieurs familles matrilinéaires qui sont parmi
les **plùs** stables de toutes les espèces animales. Les
**tekhniques** de chasse sofhistiquées **é** les comportements
**vocaus** qui sont souvent spécifiques à un groupe particuliér,
é sont transmis à travers les générations, ont été décrits par
les **cientifiques** come des manifestations **culturèles**.

L’Union internationale pour la conservation de la nature évalue
actuèlement le statut de conservation de l’orque come « donées
insufisantes » en raison de la **probabilitée** que les **tîpes**
d’orques soient des espèces distinctes. Certaines populations
locales sont menacées ou en voie de disparition à cause de la
disparition de leur **abitat**, de la polution, de la capture
des mamifères marins é de la compétition alimentaire avec
l’industrie de la **pèche**. C’èt l’espèce marine qui présente
en 2016 (deus-**mil**-seize) la plùs forte concentration de PCB
dans le sang, malgré leur **interdixion** depuis les anées 1970
(**mil-neuf-cents-soissante-dix**) aus États-Unis, 1980
(mil-neuf-cents-quatrevingts) dans l’Union européenne é depuis
la Convention de Stockholm sur les poluants organiques
persistants de 2004 (deus-mil-quatre).

Les orques sauvages ne sont pas considérées come une menace pour
**l’Ome** ; certaines s’aprochent des embarcations dans le but
d’établir un contact. Cependant, il y **ut** des cas de
spécimëns captifs tuant ou blessant leurs dresseurs dans des
parcs à thème marin.

Les orques sont tres présentes dans les mîthologies des peuples
navigateurs, avec une réputation alant du protecteur d’**ames**
umaines à cèle de tueur **impitoiyable**. Les orques, les plùs
grands delfhinidés, animent les plùs anciennes légendes, ce qui
explique qu’èles sont mises en **cène** dans des films é la
litérature.



### Orque — version modérée

+ la plupart des changements non problématiques
  d’impact visuel au plus modéré
+ EU --> Ø
+ AI, EI --> È quand l’info étym. est inutile ("certènes, sèze")
+ ILL --> Y
+ suppression des H non-aspirés
+ marquage des consonnes finales non muettes
  ("jusq’, aveq, parqs, butt, contactt")
+ EN --> AN quand la distinction est inutile

L’orque, ou ēpaulard, èt une espēce de mamifēres marins du
sous-ordre des cētacēs à dents, les odontocētes.

Ēle a une **rēparticion** cosmopolite ; ēle vit dans les rēgions
artiques ē antartiques **jusq’aus** mers tropicales. Son rēgime
**alimantaire** èt tres diversifiē, bien que les populacions se
spēcializent **souvant** dans des tîpes particuliērs de proies.
Certènes se **nourisent** de poisons, tandis que d’autres
chasent les mamifēres marins, tels que lions de mer, fhoques,
morses ē mēme de grandes balēnes (**gēnēralemant** des
balēneaus). Les orques sont considērēes come des superprēdatørs.
Les anglofhones les surnoment balēnes tuøzes bien que le genre
soit propre aus **søles** orques.

Les orques sont fortemant sociables ; **certènes** populacions
sont compozēes de pluziørs **famiyes** matrilinēaires qui sont
parmi les plùs stables de toutes les espēces animales. Les
tekhniques de chase sofhistiquēes **ē** les comportemants vocaus
qui sont souvant spēcifiques à un groupe particuliēr, ē sont
transmis à travers les gēnēracions, ont ētē dēcrits par les
**ciantifiques** come des manifestacions culturèles.

L’Union internacionale pour la conservacion de la nature ēvalue
actuèlemant le statut de conservacion de l’orque come « donēes
insufizantes » en raizon de la probabilitēe que les tîpes
d’orques soient des espēces distinctes. Certènes populacions
locales sont menacēes ou en voie de disparicion à cauze de la
disparicion de lør abitat, de la polucion, de la capture des
mamifēres marins ē de la compēticion alimantaire **aveq**
l’industrie de la pèche. C’èt l’espēce marine qui prēzante en
2016 (**døs-mil-sèze**) la plùs forte conçantracion de PCB dans
le sang, malgrē lør interdixion depuis les anēes 1970
(**mil-nøf-cents-soisante-disz**) aus Ētats-Unis, 1980
(mil-nøf-cents-quatrevints) dans l’Union øropēène ē depuis la
Convencion de Stockholm sur les poluants organiques persistants
de 2004 (døs-mil-quatre).

Les orques sauvages ne sont pas considērēes come une menace pour
l’Ome ; certènes s’aprochent des embarcacions dans le **butt**
d’ētablir un **contactt**. Cepandant, il y ut des cas de
spēcimëns captifs tuant ou **blèsant** lørs drèsørs dans des
**parqs** à thēme marin.

Les orques sont tres **prēzantes** dans les mîthologies des
pøples navigatørs, aveq une rēputacion alant du protectør d’ames
umaines à cèle de tuør impitoiyable. Les orques, les plùs grands
delfhinidēs, animent les plùs anciènes lēgeandes, ce qui
explique q’ēles sont **mizes** en cēne dans des films ē la
litērature.



### Orque — version radicale

+ diacritique lettre muette pour réécrire des digrammes voyelles
  (selon si on veut garder l’info étym.):
  + AU --> ǍO / O
  + EAU --> ĚO / O
  + AI --> ǍÈ / È
  + EN --> ǑAN / ĚAN / AN / ÀN
  + marquage de consonnes muettes ("sanǧ")
+ ILL --> Ŷ
+ GE --> J
+ QU --> Q
+ Q/C ("catre")
+ -S, -Z, -X (réécriture des -S muets non pluriels en -Z)
  ("trez, paz, pluz, danz, depuiz, souz, døz, transmiz")

L’**orqe**, ou **ēpolard**, èt une espēce de mamifēres marins du
souz-ordre des cētacēs à **dǒants** (_ou_ dants), les
odontocētes.

Ēle a une rēparticion cosmopolite ; ēle vit **danz** les rēgions
artiqes ē antartiqes jusq’ǎos mers tropicales. Son rēgime
**alimantǎère** èt trez diversifiē, bien qe les populacions se
spēcializent souvant danz des tîpes particuliērs de proies.
Certènes se nourisent de poisons, tandis qe d’**ǎotres** chasent
les mamifēres marins, tels qe lions de mer, fhoqes, morses
ē mēme de grandes balēnes (gēnēralemant des **balēněos**). Les
orqes sont considērēes come des superprēdatørs. Les anglofhones
les surnoment balēnes tuøzes bien qe le gěanre soit propre
**ǎos** søles orqes.

Les orqes sont fortemant sociables ; certènes populacions sont
compozēes de pluziørs **famiŷes** matrilinēǎères qi sont parmi
les **plùz** stables de toutes les espēces animales. Les
tekhniqes de chase sofhistiqēes ē les comportemants **vocǎos**
qi sont souvant spēcifiqes à un groupe particuliēr, ē sont
**transmiz** à travers les gēnēracions, ont ētē dēcrits par les
ciantifiqes come des manifestacions culturèles.

L’Union internacionale pour la conservacion de la nature ēvalue
actuèlemant le statut de conservacion de l’orqe come « donēes
insufizantes » àn **rǎèzon** (_ou_ rèzon) de la probabilitēe qe
les tîpes d’orqes soient des espēces distinctes. Certènes
populacions locales sont menacēes ou **àn** voie de disparicion
à **coze** de la disparicion de lør abitat, de la polucion, de
la capture des mamifēres marins ē de la compēticion alimantǎère
**aveq** l’industrie de la pèche. C’èt l’espēce marine qi
prēzante àn 2016 (**døz**-mil-sèze) la plùz forte conçantracion
de PCB danz le sanǧ, malgrē lør interdixion depuiz les anēes
1970 (mil-nøf-çants-soisante-disz) ǎos Ētats-Unis, 1980
(mil-nøf-çants-catrevints) danz l’Union øropēène ē depuiz la
**Convěancion** de Stockholm sur les poluants organiqes
persistants de 2004 (døs-mil-**catre**).

Les orqes **sovages** ne sont **paz** considērēes come une
menace pour l’Ome ; certènes s’aprochent des **ambarcacions**
danz le butt d’ētablir un contactt. Cepandant, il y ut des cas
de spēcimēns captifs tuant ou blèsant lørs drèsørs danz des
parqs à thēme marin.

Les orqes sont trez prēzantes danz les mîthologies des pøples
navigatørs, aveq une rēputacion alant du protectør d’ames
**umǎènes** à cèle de tuør impitoiyable. Les orqes, les plùz
grands delfhinidēs, animent les plùz anciènes **lējandes**, ce
qi expliqe q’ēles sont mizes àn cēne danz des films ē la
litērature.



### Déclaration des droits de l’homme et du citoyen

Les reprézantants du pøple **francèz**, constitués en Asamblée
nacionale, considérant qe l’ignorance, l’oubli ou le **mépriz**
des droits de **l’ȟome** sont les søles cozes des **malørs**
publiqs é de la corupcion des gouvernemants, ont rézolu
d’exposér, danz une déclaracion **solanèle**, les droits
naturels, inaliénables é sacrés de l’ȟome ; afin qe cète
déclaracion, **constamant** prézante à tous les mambres du
**corp̌** social, lør rapèle sans cèse lørs droits é lørs
devoirs ; afin qe les actes du pouvoir législatif, é **çøx** du
pouvoir exécutif, pouvant **ètre** à chaqe instant comparés aveq
le butt de toute institucion politiqe, en soient plùz
respectés ; afin qe les réclamacions des **citoiyens**, fondées
dézormaiz sur des principes simples é incontestables, tournent
toujourz au maintien de la Constitucion é au **bonør** de tous.

En consécance, l’Asamblée nacionale reconait é déclare, en
prézance é souz les auspices de l’**Ètre suprème**, les droits
suivants de l’ȟome é du citoiyen.

_Article **premiér**_. Les ȟomes naisent é demørent **égaus** en
droits. Les **distinxions** sociales ne pøvent ètre fondées qe
sur l’**utilitée** comune.

_Article **døz**_. Le butt de toute asociacion politiqe èt la
conservacion des droits naturels é imprescriptibles de l’ȟome.
Ces droits sont la libertée, la propriétée, la **suretée** é la
rézistance à l’oprècion.

_Article **troiz**_. Le principe de toute **souverènetée**
rézide **èsancièlemant** danz la nacion. Nul corp̌s, nul individu
ne pøt exercér d’autoritée qi n’en émane **exprèsémant**.

(…)



### Clair de lune

(texte de Guillaume Apollinaire)

Lune mèlifluante aus lēvres des dēmants  
Les vergērs et les bourǧs cète nuit sont gourmands  
Les astres asēz bien figurent les abeiyes  
De ce miel lumineuz qi dēgoute des treiyes  
Car voici qe tout dous ē lør tombant du ciel  
Chaqe raiyon de lune èt un raiyon de miel  
Or cachē je conçoiz la trez douse avanture  
J’ai pør du dard de fø de cète abeiye Arcture  
Qi poza dans mes mains des raiyons dēcevants  
Ē prit son miel lunaire à la roze des vants





<!------------------------------------------------------------->

## Validation expérimentale ?

Puisque de très nombreuses personnes se sont déjà penchées sur la question de la
simplification, avec tous les meilleurs arguments du monde, et qu’on est
toujours au point mort, il m’a semblé qu’une démonstration informatique,
*quantifiée*, de la pertinence ou de l’inutilité des complexités existantes et
des simplifications envisagées, serait une nouvelle façon d’enfoncer le clou…
On pourrait par exemple identifier des incohérences, quantifier la
reconnaissabilité des mots d’une même famille, le taux d’erreurs étymologiques
dans une orthographe qui justifie sa complexité par son ambition étymologisante,
le nombre de façons de prononcer une graphie donnée ou d’écrire une
prononciation donnée, le nombre de règles nécessaires pour y parvenir… À chaque
fois, avant et après réforme.

Par ailleurs, il est hautement désirable de présenter un dictionnaire des mots
dans leur nouvelle graphie, non seulement pour montrer *concrètement* à quoi ça
ressemblerait, mais aussi pour repérer des problèmes qu’on n’aurait pas
anticipés (p.ex. des homographies gênantes). Or, il y a de très nombreuses
modifications indépendantes, et parfois plusieurs alternatives possibles ; on
aimerait pouvoir sélectionner ou désélectionner chaque modification, et observer
ses effets. Mon idée était donc de générer les nouvelles graphies
automatiquement (je n’ai de toute façon pas les moyens humains pour produire
tout un dictionnaire manuellement). Pour ça il faudrait disposer d’une base de
données lexicographique indiquant non seulement les graphies et les
prononciations, mais aussi des données étymologiques, de parentés entre mots… Ce
dont on est hélas loin.

### Relation entre graphie et prononciation

On dispose de plusieurs base de données qui renseignent, au minimum, la graphie
et la prononciation d’un ensemble relativement complet de mots du français.

Pour le travail présenté ici, la base de données lexicographique Lexique3 et
parfois le Wiktionnaire ont été utilisés pour évaluer la fréquence ou la rareté
de graphies, en relation avec les sons produits, ainsi que pour trouver des
exemples.

Sur Lexique3, on a également calculé pour chaque mot (hormis mots d’emprunt)
l’association graphie-prononciation graphème par graphème
(par exemple `"oiseau" ~ [wazo]` se découpe `{oi}~[wa], {s}~[z], {eau}~[o]`),
ce qui permet des requêtes plus précises
et constitue un préliminaire à des travaux plus poussés.

En particulier, on a identifié toutes les paires graphème/son existant dans
cette base.

Pistes de travaux:

+ [ ] Implémenter des algorithmes qui essaient de **deviner comment s’écrit** un mot,
  dans l’orthographe actuelle et dans l’orthographe réformée.
   1. niveau 1: sachant seulement la prononciation; plusieurs variantes:
      - soit (bête) lister toutes les graphies possibles  
        => calculer le nombre de possibilités
      - soit (bête) choisir pour chaque son la graphie majoritaire  
        => calculer le taux d’erreur
      - soit (mieux) faire la combinaison des deux,
        c.-à-d. calculer toutes les graphies avec la probabilité de chacune  
        => calculer le taux d’erreur
   2. niveau 2: sachant comment ça se prononce dans une phrase avec liaison
   3. niveau 3: sachant des informations grammaticales
      (féminin, pluriel, forme conjuguée…)
   4. niveau 4: sachant d’autres mots de la même famille
      - intéressant à corréler avec des données sur le niveau de connaissance des
        mots par la population
   5. niveau 5: sachant la langue source (grec ancien, arabe…)
   6. niveau 666: sachant la décomposition en préfixes, radicaux et suffixes
      (utile pour les consonnes doubles…)
+ [ ] Vice-versa, **de la graphie vers la prononciation**.
+ [ ] Plus important que le point précédent (cf section « Méthodologie »):
  **de la graphie vers la compréhension**
  (étant donné un texte écrit, identifier correctement les mots,
  les relier aux bonnes significations;
  les homographies compliquent cette tâche).
+ [ ] Classer les erreurs commises par les algorithmes précédents
  (p.ex. oublier une lettre muette étymologique = pas grave,
  oublier une diacritique = plus ou moins grave…).

Le but est de quantifier le gain effectivement fourni par les simplifications:
l’algorithme pour l’orthographe réformée devrait être plus simple
et échouer moins souvent.

### Une base de données étymologique ?

Pour aller plus loin que la simple phonétique, il serait très utile de disposer
d’une base de données étymologiques et morphologiques. Je n’ai rien trouvé qui
s’approche de ce que j’ai en tête… On pourrait constituer manuellement une base
minimale, pour les quelques milliers de mots les plus fréquents, histoire de
tester le concept. Une base plus complète pourrait peut-être se constituer en
crowdsourcing ? (une interface web où on présenterait un mot à l’internaute et
on lui demanderait de lister d’autres mots de la même famille… on en
profiterait pour vérifier les données de notre base, car il y a beaucoup
d’erreurs dans les bases actuelles…)

Ce n’est pas tant les racines (latines ou autres) que les apparentés entre mots
français qui nous intéressent. Il faudrait des données qui mettent en
correspondance les lettres des mots de la même famille. Par exemple :

  - les lettres `C, O, R` dans "cœur" correspondent respectivement aux lettres
    `C, O, R` dans "coronaire" ;
  - `QU` dans "queue" correspond à `C` dans "caudal" ;
  - `^` dans "hôpital" correspond à `S` dans "hospitalier";
  - `AI` dans "grain" correspond à `A` dans "granule"
    (ce pourquoi on n’écrit pas #"grin") ;
  - `CH` dans "cheval" correspond à `C` dans "cavalier" ;
  - `CH` dans "choléra" correspond à `C` dans "colère"…

Exemple plus développé :

    P  I  N C       - EAU     (en réalité, pas issu de la même racine latine)
    |  |  | :
    P  I  | C T U R - A L
    |  |  |   | | |
    P EI  N   T U R - E
    |  |  |   :   |
    P EI  N   D   R - E
    |  |  :
    P EI GN         - ON S

(Considérations pratiques : pour éviter de devoir lier tous les mots 2 à 2, on
pourrait synthétiser pour chaque famille une « pseudo-racine » qui contiendrait
les lettres pertinentes de tous les mots de la famille, et lier chaque mot à
cette pseudo-racine ; dans cet exemple, ça pourrait être #"PINCTUR".)

Ce qui serait encore plus fort,
ce serait que la base indique des degrés de proximité entre mots
(par exemple "caillou/calcul", "captif/chétif" ou "choléra/colère",
bien qu’issus des mêmes racines, ne sont pas très liés;
en revanche "cheval/cavalier" sont du même champ lexical).
À la rigueur, c’est en fait plus intéressant que l’étymologie proprement dite
(par exemple, "pinceau/pictural" ne sont pas de la même racine,
pas plus que "habit/habiller",
leur rapprochement est une heureuse coïncidence,
avec influence d’un mot sur l’autre) !
Suggestion d’échelle de proximité :

  - degré 0 = variante orthographique (ex: "maitre -- maître; fond -- fonds") ;
  - degré 1 = flexion (ex: "maîtresse -- maître; beau -- belles") ;
  - degré 2 = apparenté « directe » vis-à-vis du sens
    (ex: "maître -- maîtrise; beau -- beauté, embellir; chaux -- calcifère; habit -- habiller") ;
  - degré 3 = apparenté dans le même champ lexical
    (ex: "maître -- mastère; chaux -- calcaire")
  - degré 4 = apparenté étymologique secondairement intéressante
    (ex: "habit -- habitude") ;
  - ne pas s’encombrer avec les parentés étymologiques
    sans intérêt vis-à-vis du sens
    (ex: "spatule -- épaulard; chausser -- cauchemar; habiller -- bille; panser -- penser").
  - distinguer liens étymologiques (ex: "habit -- habitude")
    et para-étymologiques (ex: "habit -- habiller");

cf aussi [cahier-des-charges-base-lexico.txt](./cahier-des-charges-base-lexico.txt)

Avec ces données, on pourrait détecter les incohérences orthographiques et les
complexités inutiles, candidates à simplification. Par exemple :

  - pour la famille de "peinture, peindre", la seule raison d’utiliser le
    graphème `EIN` plutôt que le graphème plus simple `IN` est que le `E(I)` [ɛ]
    s’entend dans les formes conjuguées telles que "nous peignons" ; autrement,
    le `E` ne s’entend dans aucun autre mot de la famille (et ne se trouve ni
    dans la racine latine "pictura, pingere", ni dans les langues voisines comme
    l’espagnol "pintura" ou l’anglais "paint") ;
  - "choléra / colère" ;
  - les consonnes parfois doublées et parfois non
    ("résonner / résonance, chariot / charrette"…).

On pourrait également établir des statistiques globales sur l’usage des
graphèmes, en relation avec leur raison d’être, et faire des choix de
simplification éclairés. Par exemple :

  - Quelles lettres muettes sont inutiles ?
  - Peut-on remplacer systématiquement le graphème `EN` prononcé [ã] par `AN`,
    ou bien l’orthographe `EN` porte-t-elle de l’information utile ?
  - Y a-t-il des raisons suffisantes de distinguer deux façons d’écrire le son
    [ʒ], ou bien peut-on décider que `G` produira toujours [g] et `j` produira
    toujours [ʒ] ?
  - Peut-on considérer que `Q(U)` n’est que la variante « dure » de `C` (tout
    comme `Ç` en est la variante « douce »), et simplifier en conséquence
    l’alternance phonétique de `C` et la multitude de façon d’écrire le son [k] ?
  - Quels sont toutes les familles de mots où s’entend une alternance de sons [s/z] ?
  - etc.





<!------------------------------------------------------------->

## Bibliographie

 1. **_Les délires de l’orthographe_, Nina Catach, 1989**
 1. _Une brève histoire de l’orthographe_, Pascale Cheminée, 2017
 1. [_La bataille de l’orthographe aux alentours de 1900_][bataille-1900], Nina Catach, dans _Histoire de la langue française 1880-1914_, Gérald Antoine et Robert Martin
 1. _À quoi sert l’accent circonflexe ?_, Maurice Tournier, dans _Mots_ n°28, sept. 1991
 1. [_Orthotypographie : Orthographe & Typographie françaises, Dictionnaire raisonné_][orthotypo], Jean-Pierre Lacroux, 2007
    + possible mine d’or, que j’ai découverte trop tard pour l’exploiter

Mais aussi :

 1. le tract _Le français va très bien, merci_,
    collectif des Linguistes atterré·e·s
    ([sommaire sur le site du collectif][tract-linguistes])
 1. le livre et spectacle _La Faute de l’orthographe_ (ou : _La convivialité_),
    Arnaud Hoedt et Jérôme Piron ([compte-rendu][convivialité])

[bataille-1900]: https://books.openedition.org/editionscnrs/9268
[orthotypo]: https://www.orthotypographie.fr/



### Mes notes de lecture

_Une brève histoire de l’orthographe_:

+ p26, p28: diphtongues ŒU, EAU
+ p26, p28: L devenu voyelle, ALP --> AUP
+ p29: H bidouille ("huile, huître, huis")
+ p32: CH prononcé `[k]`
+ p32-33: C / QU
+ p33: particularités ("laïc/laïque")
+ p34: PH
+ p34: particularités ("rhythme, monarque")
+ p36: G / J
+ p36: H: aspiré, muet, grec, de majesté, hiatus, bidouille
  - (de majesté: "posthume, hasard, luth, otage")
+ p37-38: ILL
+ p38: -R muet
+ p39: -T muet
+ p39: -S- simple prononcé [s]
+ p40: particularités ("six, dix/decem!")
+ p40: -X (particularités: "vaux, veux, vœux")
+ p41: [w] est l’un des sons les plus courants du français !
+ p51: particularité ("lys")
+ p52: particularités ("doute, sculpture, baptême, aulne")
+ p??, p52-54: effet Buben: lettres muettes étym. devenues prononcées
  ("aulne, sanctifier, exact, subtil, ustensile, festoyer, obscur, dompter, cheptel…")
+ p55: ortho étym. compliquée mais qui surmonte les différences dialectales
+ p61: ouistes et non-ouistes
+ p66-67: histoire des accents

_Les délires de l’orthographe_:

+ glossaire: archigraphèmes\*\* et phonogrammes\*\*
+ p19: Alfonic ?
+ p21: arrêté 1900 = accord pp.
+ p27: particularité ("abîme(r)")
+ p29: particularités
+ p30-31 (dérivaison\*): particularités !
+ p38 (étymologie\*): particularités, homophones
+ p45: -S de l’impératif
+ p83: Francophonie
+ p97: O ouvert/fermé
+ p126, p299: consonnes doubles
+ p127-129: lettres mouillées GN et ILL
+ accent plat:
  - p134: l’écriture n’a pas à être entièrement phonétique
    (cf permanence graphique p210)
  - p139: circonflexe pour voyelle longue = info superflue (dépend de l’accent)
  - p145
+ p137: tréma !
+ p170: citation D’Alembert
+ p174: particularités
+ p213-215: les 3 E
+ p224: particularités: erreurs étym. ("poids", H-)
+ p229: alternances
+ p233: Charles Beaulieux = histoire très détaillé de l’écriture fr.
+ p238 (à propos du chinois): idée amusante:
  suggère que l’écrit pourrait davantage refléter la phrase «abstraite»
  sans les accidents de l’oral
  (ex: "(moi)-(futur)-(manger) (à) (restaurant)")
+ p241 (Dangeau): lettres grecques PH, CH --> FH, KH
+ p245: particularités: lettres doubles absurdes
+ p245: Vladimir Gak = descr. juste et neutre de l’ortho fr.
+ p251: l’effet Buben, ou le français lu par des extraterrestres du futur

<!-- vim: set filetype=markdown et ts=2 sts=0 sw=0 tw=80: -->
