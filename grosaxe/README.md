# Le Gros Axe

## Prérequis

 *  Perl5 et `cpanm` (gestionnaire de paquets pour Perl)
 *  `svn`
 *  OCaml et `opam` (gestionnaire de paquets pour OCaml)
 *  une machine assez puissante pour compiler Alpage (au moins 2 G de mémoire
    vive)
 *  assez d’espace pour compiler Alpage (au moins 7 G d’espace disque) ; après
    compilation, 3 G suffisent si on jette les sources.

## Configuration

	# dossier où mettre le tout:
	root="$PWD"

## Installation de Lexique3

Liens :
 *  [documentation de Lexique3][doc-lexique]
 *  [interroger Lexique3 en ligne][tester-lexique]

[doc-lexique]: http://lexique.org/outils/Manuel_Lexique.htm
[tester-lexique]: http://lexique.org/moteur/Open.php?base[0]=lexique3&nbfields=8&Open_Lexique=Recherche

_Espace nécessaire :_ 30 M.

	# dossier où mettre Lexique3:
	lexique="$root"/lexique
	# version de Lexique3:
	lexver=382

	mkdir -p "$lexique" && cd "$lexique"
	wget "http://lexique.org/public/Lexique$lexver.zip"
	unzip "Lexique$lexver.zip" "Lexique$lexver.txt"
	ln -sf "Lexique$lexver.txt" "Lexique.txt"

## Installation de FRMG

Liens :
 *  [aide concise pour installer la chaîne Alpage avec alpi][aide-alpi]
 *  [documentation plus détaillée d’alpi][doc-alpi]
    (la documentation complète et à jour s’obtient avec `perldoc alpi/alpi.pl`)
 *  [aide pour utiliser FRMG][aide-frmg]
 *  [tester FRMG en ligne][tester-frmg]

[aide-alpi]: http://alpage.inria.fr/frmgwiki/wiki/installer-la-chaine-alpage
[doc-alpi]: https://www.rocq.inria.fr/alpage-wiki/tiki-index.php?page=alpi&bl=y
[aide-frmg]: http://alpage.inria.fr/frmgwiki/wiki/utiliser-lanalyseur-frmg
[tester-frmg]: http://alpage.inria.fr/frmgwiki/frmg_main/frmg_server

_Espace nécessaire :_ 2,8 G (10 G avec les sources).

On ne peut pas installer la chaine Alpage sur une machine A puis copier-coller
le résultat sur une machine B, car les chemins sont codés en dur dans les
programmes produits.

Procédure d’installation sur zamok :

	# dossier de l’installeur alpi:
	alpi="$root"/alpi
	# dossier des outils de l’équipe Alpage:
	alpage="$root"/alpage
	# dossier où installer localement des modules Perl:
	#localperl5lib=~/.perl5lib
	localperl5lib="$alpage"

	cd "$root"
	# installation de quelques modules Perl qui ne sont pas installés
	# automatiquement par alpi:
	# TODO:
	#   Il faudrait vérifier ce qui est vraiment nécessaire, je ne comprends pas
	#   très bien. Dans une tentative précédente, j’avais dû installer IPC::Run.
	cpanm -l "$localperl5lib" DBI DBD::SQLite CGI
	# nécessaire pour compiler certains paquets alpi:
	# TODO:
	#   Redondant si $localperl5lib = $alpage ?
	#   En tout cas sur zamok ça a marché sans.
	export PERL5LIB="$localperl5lib/lib/perl5:$PERL5LIB"
	# installation d’alpi:
	svn checkout svn://scm.gforge.inria.fr/svnroot/lingwb/alpi/trunk "$alpi"
	# installation des outils Alpage:
	# NOTE:
	#   La commande suivante fait tout ce qu’il faut:
	#       perl "$alpi"/alpi --svn=anonymous --prefix="$alpage" --aclocaldirs=/usr/share/aclocal/ --pkg=frmg --pkg=sxpipe
	#   mais le dossier "$alpi" résultant fait 10 G et on a un espace limité.
	#   Heureusement, il y a 7,2 G de sources qu’on peut jeter. On sépare donc
	#   l’installation en plusieurs temps pour pouvoir supprimer les sources au
	#   fur et à mesure.
	#   «sxpipe» est de loin le paquet le plus gros (4,0 G de sources).
	#   «aleda» est aussi très gros (1,7 G de sources).
	# NOTE:
	#   Si SVN demande à stocker des mots de passe en clair, répondre «no».
	# TODO:
	#   Faut-il que sqlite soit installé (sudo apt-get install sqlite) ?
	# installation 1/2:
	perl "$alpi"/alpi --svn=anonymous --prefix="$alpage" --aclocaldirs=/usr/share/aclocal/ --pkg=frmg --log=alpi-1-frmg.log
	# bilan: 1,9 G dans "$alpage"/ dont 1,5 G pour "$alpage"/src/
	rm -fr "$alpage"/src
	# installation 2/2:
	# NOTE:
	#   On gère nous-mêmes les dépendances pour éviter qu’alpi retraite tout ce
	#   qu’on a déjà installé (et dont on a jeté les sources), ce qui serait
	#   très long… (le paquet «lefff» est particulièrement long à traiter)
	perl "$alpi"/alpi --svn=anonymous --prefix="$alpage" --skipdep --pkg=aleda --pkg=syntax --pkg=sxpipe --log=alpi-2-sxpipe.log
	# bilan: 8,5 G dans "$alpage"/ dont 5,7 G pour "$alpage"/src/
	rm -fr "$alpage"/src
	# bilan: 2,8 G dans "$alpage"/

Configuration de l’environnement :

	# Ajouter ces lignes à ~/bash_profile ou ~/.zshrc ou autre
	# (remplacer $alpage et $localperl5lib par les valeurs idoines):
	source "$alpage/sbin/setenv.sh"
	export PERL5LIB="$localperl5lib/lib/perl5:$PERL5LIB" # redondant si $localperl5lib = $alpage

Exemples d’usage :

	frmg_shell
	echo ':passage:xml Bonjour, je suis la pomme de terre sacrée.' | frmg_shell --quiet > analyse.xml

## Installation de l’environnement OCaml

TODO
