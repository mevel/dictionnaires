#!env ocaml

(* charger findlib (pour #require): *)
try Topdirs.dir_directory (Sys.getenv "OCAML_TOPLEVEL_PATH") with Not_found -> () ;;
#use "topfind"

#require "ppx_deriving.std"
#require "re"

type mot =
  {
    (* documentation extraite de la page ci-dessus: *)
    ortho : string ; (* (1) le mot *)
    phon : string ; (* (2) les formes phonologiques du mot *)
    mutable lemme : string ; (* (3) les lemmes de ce mot *)
    cgram : string ; (* (4) les catégories grammaticales de ce mot *)
    genre : string ; (* (5) le genre *)
    nombre : string ; (* (6) le nombre *)
    mutable freqlemfilms : string ; (* (7) la fréquence du lemme selon le corpus de sous-titres (par million d’occurrences) *)
    mutable freqlemlivres : string ; (* (8) la fréquence du lemme selon le corpus de livres (par million d’occurrences) *)
    mutable freqfilms : string ; (* (9) la fréquence du mot selon le corpus de sous-titres (par million d’occurrences) *)
    mutable freqlivres : string ; (* (10) la fréquence du mot selon le corpus de livres (par million d’occurrences) *)
    infover : string ; (* (11) modes, temps, et personnes possibles pour les verbes *)
    nbhomogr : string ; (* (12) nombre d'homographes *)
    nbhomoph : string ; (* (13) nombre d'homophones *)
    mutable islem : string ; (* (14) indique si c'est un lemme ou pas *)
    nblettres : string ; (* (15) le nombre de lettres *)
    nbphons : string ; (* (16) nombre de phonèmes *)
    cvcv : string ; (* (17) la structure orthographique *)
    p_cvcv : string ; (* (18) la structure phonologique *)
    voisorth : string ; (* (19) nombre de voisins orthographiques *)
    voisphon : string ; (* (20) nombre de voisins phonologiques *)
    puorth : string ; (* (21) point d'unicité orthographique *)
    puphon : string ; (* (22) point d'unicité phonologique *)
    syll : string ; (* (23) forme phonologique syllabée *)
    nbsyll : string ; (* (24) nombre de syllabes  *)
    cv_cv : string ; (* (25) structure phonologique syllabée *)
    orthrenv : string ; (* (26) forme orthograhique inversée *)
    phonrenv : string ; (* (27) forme phonologique inversée *)
    orthosyll : string ; (* (28) forme orthographique syllabée *)
    (* ces champs sont présents dans le fichier mais non documentés: *)
    cgramortho : string ; (* (29) *)
    deflem : string ; (* (30) *)
    defobs : string ; (* (31) *)
    old20 : string ; (* (32) *)
    pld20 : string ; (* (33) *)
    morphoder : string ; (* (34) *)
    nbmorph : string ; (* (35) *)
    (* champs ajoutés par ce script: *)
    id : int ; (* numéro de ligne *)
    mutable parent : mot option ; (* pour une flexion, lemme auquel elle se rapporte *)
    mutable flexions : mot list ; (* pour un lemme, liste de ses flexions *)
    mutable image : mot option ;
  }
[@@deriving show { with_path = false }]

(* pour copié-collé rapide: *)
(*
ortho phon lemme cgram genre nombre freqlemfilms freqlemlivres freqfilms freqlivres infover nbhomogr nbhomoph islem nblettres nbphons cvcv p_cvcv voisorth voisphon puorth puphon syll nbsyll cv_cv orthrenv phonrenv orthosyll cgramortho deflem defobs old20 pld20 morphoder nbmorph
*)



(*
 * Affichage
 *)

let rec pp_mot (output : Format.formatter) (m : mot) : unit =
  if m.islem = "1" then begin
    Format.fprintf output "[%i]{ «%s» %s " m.id m.ortho m.cgram ;
    begin match m.image with
    | None     -> ()
    | Some img -> Format.fprintf output "=> «%s» " img.ortho
    end ;
    if m.flexions <> [] then begin
      Format.fprintf output "[@\n" ;
      List.iter (Format.fprintf output "  @[%a@]@\n" pp_mot) m.flexions ;
      Format.fprintf output "]"
    end ;
    Format.fprintf output "}"
  end else begin
    Format.fprintf output "[%i]{ «%s» %s → «%s» " m.id m.ortho m.cgram m.lemme ;
    begin match m.parent with
    | None     -> ()
    | Some lem ->
        begin match lem.image with
        | None     -> ()
        | Some img -> Format.fprintf output "=> «%s» " img.ortho
        end
    end ;
    Format.fprintf output "}"
  end

exception Erreur

let avertissement ~msg ?(mots=[]) =
  Format.eprintf "ATTENTION: %s@\n" msg ;
  List.iter (Format.eprintf "  @[%a@]@\n" pp_mot) mots

let erreur ~msg ?(mots=[]) =
  Format.eprintf "ERREUR: %s@\n" msg ;
  List.iter (Format.eprintf "  @[%a@]@\n" pp_mot) mots ;
  raise Erreur

let my_assert ?(msg="") ?(mots=[]) assertion =
  if not assertion then erreur ~msg ~mots



(*
 * Lecture du fichier
 *)

let lire_ligne (input : Scanf.Scanning.in_channel) : mot =
  Scanf.bscanf input
    "%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\t%[^\t\n]\n"
  @@fun ortho phon lemme cgram genre nombre freqlemfilms freqlemlivres freqfilms freqlivres infover nbhomogr nbhomoph islem nblettres nbphons cvcv p_cvcv voisorth voisphon puorth puphon syll nbsyll cv_cv orthrenv phonrenv orthosyll cgramortho deflem defobs old20 pld20 morphoder nbmorph ->
  { ortho ; phon ; lemme ; cgram ; genre ; nombre ; freqlemfilms ; freqlemlivres ; freqfilms ; freqlivres ; infover ; nbhomogr ; nbhomoph ; islem ; nblettres ; nbphons ; cvcv ; p_cvcv ; voisorth ; voisphon ; puorth ; puphon ; syll ; nbsyll ; cv_cv ; orthrenv ; phonrenv ; orthosyll ; cgramortho ; deflem ; defobs ; old20 ; pld20 ; morphoder ; nbmorph ; id = 0 ; parent = None ; flexions = [] ; image = None }

let lire_fichier (input : Scanf.Scanning.in_channel) : mot list =
  let mots = ref [] in
  let nb = ref 0 in
  (* on ignore l’en-tête du fichier (par chance il satisfait notre scanf): *)
  ignore (lire_ligne input) ;
  begin try while true do
    let m = lire_ligne input in
    mots := { m with id = !nb } :: !mots ;
    incr nb
  done with
  | End_of_file ->
      Format.printf "fin du fichier\n"
  | Scanf.Scan_failure msg ->
      Format.printf "erreur de scanf: %s\n" msg
  end ;
  Format.printf "nombre de mots lus: %u\n" !nb ;
  Format.printf "dernier mot lu: %a\n" pp_mot (List.hd !mots) ;
  List.rev !mots

let dico : mot list =
  let input = Scanf.Scanning.open_in "Lexique3.tsv" in
  let mots = lire_fichier input in
  Scanf.Scanning.close_in input ;
  mots

let () =
  dico |> List.iter begin fun m ->
    my_assert ~msg:"orthographe des lemmes et des flexions" ~mots:[m]
      (  (m.islem = "1" && m.ortho = m.lemme)
      || (m.islem = "0" && m.ortho <> m.lemme) )
  end

(* Quelques assertions *)

let est_trie (type a) ?(cmp : a -> a -> bool = (<=)) (li : a list) : bool =
  let rec aux x xs =
    begin match xs with
    | []      -> true
    | y :: ys -> cmp x y && aux y ys
    end
  in
  begin match li with
  | []      -> true
  | x :: xs -> aux x xs
  end

let () =
  my_assert ~msg:"la liste n’est pas triée" (est_trie ~cmp:(<) dico) ;
  my_assert ~msg:"la liste n’est pas triée par ordre alphabétique" (est_trie ~cmp:(fun e1 e2 -> e1.ortho <= e2.ortho) dico)



(*
 * Regroupement par lemmes
 *)

let lemmes_par_ortho : (string, mot list) Hashtbl.t =
  Hashtbl.create 50_000

(* note: le nombre maximal de lemmes homographes est de 5. *)

let () =
  dico |> List.iter begin fun m ->
    if m.islem = "1" then begin
      begin match Hashtbl.find lemmes_par_ortho m.ortho with
      | exception Not_found ->
          Hashtbl.add lemmes_par_ortho m.ortho [m]
      | lems ->
          Hashtbl.replace lemmes_par_ortho m.ortho (m :: lems)
      end
    end
  end

(* Assertion: deux lemmes homographes ont des ctégories grammaticales distinctes
 * (autrement dit, la donnée de l’orthographe et de la catégorie grammaticale
 * suffit à idenitfier un lemme). *)
(* TODO: faire la même vérification pour tous les mots. *)

let () =
  lemmes_par_ortho |> Hashtbl.iter begin fun _ortho lems ->
    lems |> List.iteri begin fun i m1 ->
      lems |> List.iteri begin fun j m2 ->
        my_assert ~msg:"deux lemmes homographes ont la même catégorie grammaticale" ~mots:[m1;m2]
          (i = j || m1.cgram <> m2.cgram)
      end
    end
  end

let nb_lemmes_crees = ref 0

let creer_lemme (flexion : mot) : mot =
  incr nb_lemmes_crees ;
  {
    ortho = flexion.lemme ;
    phon = "" ;
    lemme = flexion.lemme ;
    cgram = flexion.cgram ;
    genre = "" ;
    nombre = "" ;
    freqlemfilms = flexion.freqlemfilms ;
    freqlemlivres = flexion.freqlemlivres ;
    freqfilms = "0.00" ;
    freqlivres = "0.00" ;
    infover = "" ;
    nbhomogr = "" ;
    nbhomoph = "" ;
    islem = "1" ;
    nblettres = "" ;
    nbphons = "" ;
    cvcv = "" ;
    p_cvcv = "" ;
    voisorth =  "" ;
    voisphon = "" ;
    puorth = "" ;
    puphon = "" ;
    syll = "" ;
    nbsyll = "" ;
    cv_cv = "" ;
    orthrenv = "" ;
    phonrenv = "" ;
    orthosyll = "" ;
    cgramortho = "" ;
    deflem = "" ;
    defobs = "" ;
    old20 = "" ;
    pld20 = "" ;
    morphoder = "" ;
    nbmorph = "" ;
    id = ~- !nb_lemmes_crees ;
    parent = None ;
    flexions = [] ;
    image = None ;
  }

let () =
  dico |> List.iter begin fun m ->
    if m.islem = "0" then begin
      let lem =
        begin match Hashtbl.find lemmes_par_ortho m.lemme with
        | lems ->
            begin try
              lems |> List.find (fun lem -> lem.cgram = m.cgram)
            with Not_found ->
              avertissement ~msg:"le lemme n’existe pas dans cette catégorie grammaticale, on le crée" ~mots:[m] ;
              let lem = creer_lemme m in
              Hashtbl.replace lemmes_par_ortho lem.ortho (lem :: lems) ;
              lem
            end
        | exception Not_found ->
            avertissement ~msg:"le lemme n’existe pas, on le crée" ~mots:[m] ;
            let lem = creer_lemme m in
            Hashtbl.add lemmes_par_ortho lem.ortho [lem] ;
            lem
        end
      in
      m.parent <- Some lem ;
      lem.flexions <- m :: lem.flexions
    end
  end

(* Quelques assertions sur les fréquences *)

(* Dans la base de données, les fréquences sont des nombres décimaux avec deux
 * chiffres après la virgule (par exemple 123.40). Ici, on les représente par un
 * entier (12340) pour éviter les erreurs d’arrondis dues aux flottants. *)
let parser_freq freq_string =
  Scanf.sscanf freq_string "%u.%c%c" @@fun partie_entiere decimale1 decimale2 ->
    assert ('0' <= decimale1 && decimale1 <= '9') ;
    assert ('0' <= decimale2 && decimale2 <= '9') ;
    let d1 = Char.code decimale1 - Char.code '0' in
    let d2 = Char.code decimale2 - Char.code '0' in
    partie_entiere * 100 + d1 * 10 + d2

let ecrire_freq freq =
  Printf.sprintf "%u.%u%u" (freq / 100) (freq / 10 mod 10) (freq mod 10)

let addition_freq f1 f2 =
  ecrire_freq (parser_freq f1 + parser_freq f2)

let soustraction_freq f1 f2 =
  ecrire_freq (parser_freq f1 - parser_freq f2)

let () =
  lemmes_par_ortho |> Hashtbl.iter begin fun _ortho lems ->
    lems |> List.iter begin fun lem ->
      let somme_freqfilms = ref lem.freqfilms in
      let somme_freqlivres = ref lem.freqlivres in
      lem.flexions |> List.iter begin fun m ->
        my_assert ~msg:"la flexion n’indique pas la même fréquence filmographique totale que son lemme" ~mots:[m]
          (m.freqlemfilms = lem.freqlemfilms) ;
        my_assert ~msg:"la flexion n’indique pas la même fréquence livresque totale que son lemme" ~mots:[m]
          (m.freqlemlivres = lem.freqlemlivres) ;
        somme_freqfilms := addition_freq !somme_freqfilms m.freqfilms ;
        somme_freqlivres := addition_freq !somme_freqlivres m.freqlivres
      end ;
      (*
      begin try
      my_assert ~msg:"la somme des fréquences filmographiques des flexions n’égale pas la fréquence filmographique du lemme" ~mots:[lem]
        (!somme_freqfilms = lem.freqlemfilms) ;
      my_assert ~msg:"la somme des fréquences livresques des flexions n’égale pas la fréquence livresque du lemme" ~mots:[lem]
        (!somme_freqlivres = lem.freqlemlivres)
      with Erreur -> () end
      *)
    end
  end



(*
 *  Regroupement par catégories grammaticales
 *)

let lemmes_par_cgram : (string, mot list) Hashtbl.t =
  let h = Hashtbl.create 10 in
  lemmes_par_ortho |> Hashtbl.iter begin fun _ortho lems ->
    lems |> List.iter begin fun lem ->
      begin match Hashtbl.find h lem.cgram with
      | exception Not_found ->
          Hashtbl.add h lem.cgram [lem]
      | lems' ->
          Hashtbl.replace h lem.cgram (lem :: lems')
      end
    end
  end ;
  h

let statistiques_par_cgram ~msg =
  Format.printf "%s:\n" msg ;
  let li = ref [] in
  lemmes_par_cgram |> Hashtbl.iter begin fun cgram lems ->
    let lems_crees = lems |> List.filter (fun lem -> lem.id < 0) in
    li := (cgram, List.length lems, List.length lems_crees) :: !li
  end ;
  !li
  |> List.sort Pervasives.compare
  |> List.iter begin fun (cgram, nb, nb_crees) ->
    Format.printf "\tcatégorie [%-7s] : %5u lemmes (dont %4u créés)\n" cgram nb nb_crees
  end

let () =
  statistiques_par_cgram ~msg:"après lecture du fichier et groupement par lemme"



(*
 * Outils pratiques
 *)

let iter ?islem ?cgram (k : mot -> unit) : unit =
  let k =
    begin match islem with
    | None       -> (fun lem -> k lem ; List.iter k lem.flexions)
    | Some true  -> k
    | Some false -> (fun lem -> List.iter k lem.flexions)
    end
  in
  begin match cgram with
  | None   -> Hashtbl.iter (fun _ -> List.iter k) lemmes_par_cgram
  | Some c -> List.iter k (Hashtbl.find lemmes_par_cgram c)
  end

let chercher ?islem ?filtre ?cgram ?ortho ?regex () : mot list =
  let test_ortho =
    begin match regex, ortho with
    | None,    None   -> (fun m -> true)
    | Some re, None   -> Re.execp (Re.compile @@ Re.Posix.re re)
    | None,    Some o -> (=) o
    | _ -> erreur ~msg:"les options ~ortho et ~regex sont mutuellement exclusives" ~mots:[]
    end
  in
  let f =
    begin match filtre with
    | None   -> (fun m -> test_ortho m.ortho)
    | Some f -> (fun m -> test_ortho m.ortho && f m)
    end
  in
  let li = ref [] in
  iter ?islem ?cgram begin fun m ->
    if f m then li := m :: !li
  end ;
  List.rev !li

(* NOTE: on a vu que les lemmes sont identifiables par la donnée de leur
 * orthographe et de leur catégorie grammaticale; on espère que c’est le cas de
 * tous les mots…
 * En fait ce n’est pas le cas, au moins à cause d’erreurs dans la base de
 * données, qu’on corrige plus bas. Par exemple, le mot ADV «d'emblée» est
 * dédoublé par erreur, mais seul l’un des exemplaires est un lemme car l’autre
 * pointe par erreur vers «d'embléee».
 * Pour traiter ces erreurs, il est suffisant de restreindre la recherche aux
 * lemmes. *)
let mot ?islem ?filtre cgram ortho : mot =
  begin match chercher ?islem ?filtre ~cgram ~ortho () with
  | [m]  -> m
  | []   -> erreur ~msg:"aucun mot trouvé" ~mots:[]
  | mots -> erreur ~msg:"plusieurs mots trouvés" ~mots
  end



(*
 * Opérations sur la base de données
 *)

let list_remove x li =
  List.filter (fun y -> x.id <> y.id) li

let supprimer_lemme lem : unit =
  assert (lem.islem = "1") ;
  if lem.flexions <> [] then
    avertissement ~msg:"le lemme contient des flexions, on les supprime aussi" ~mots:[lem] ;
  let lems = Hashtbl.find lemmes_par_cgram lem.cgram in
  assert (List.mem lem lems) ;
  Hashtbl.replace lemmes_par_cgram lem.cgram (list_remove lem lems)

let supprimer_flexion_de_lemme ~lem flexion : unit =
  assert (lem.islem = "1") ;
  assert (flexion.islem = "0") ;
  assert (List.mem flexion lem.flexions) ;
  lem.freqlemfilms <- soustraction_freq lem.freqlemfilms flexion.freqfilms ;
  lem.freqlemlivres <- soustraction_freq lem.freqlemlivres flexion.freqlivres ;
  lem.flexions |> List.iter begin fun flexion ->
    flexion.freqlemfilms <- lem.freqlemfilms ;
    flexion.freqlemlivres <- lem.freqlemlivres
  end ;
  lem.flexions <- list_remove flexion lem.flexions ;
  if lem.id < 0 && lem.flexions = [] then
    supprimer_lemme lem

let supprimer_flexion flexion : unit =
  assert (flexion.islem = "0") ;
  begin match flexion.parent with
  | None     -> assert false
  | Some lem -> supprimer_flexion_de_lemme ~lem flexion
  end

let supprimer m : unit =
  if m.islem = "1" then
    supprimer_lemme m
  else
    supprimer_flexion m

let ajouter_lemme lem : unit =
  assert (lem.islem = "1") ;
  let lems = Hashtbl.find lemmes_par_cgram lem.cgram in
  assert (not @@ List.mem lem lems) ;
  Hashtbl.replace lemmes_par_cgram lem.cgram (lem :: lems)

let ajouter_flexion_a_lemme flexion ~lem : unit =
  assert (lem.islem = "1") ;
  assert (flexion.islem = "0") ;
  assert (not @@ List.mem flexion lem.flexions) ;
  lem.flexions <- flexion :: lem.flexions ;
  lem.freqlemfilms <- addition_freq lem.freqlemfilms flexion.freqfilms ;
  lem.freqlemlivres <- addition_freq lem.freqlemlivres flexion.freqlivres ;
  lem.flexions |> List.iter begin fun flexion ->
    flexion.freqlemfilms <- lem.freqlemfilms ;
    flexion.freqlemlivres <- lem.freqlemlivres
  end

let lemmifier flexion : unit =
  assert (flexion.islem = "0") ;
  begin match flexion.parent with
  | None     -> assert false
  | Some lem -> supprimer_flexion_de_lemme ~lem flexion
  end ;
  flexion.islem <- "1" ;
  flexion.lemme <- flexion.ortho ;
  flexion.parent <- None ;
  assert (flexion.flexions = []) ;
  ajouter_lemme flexion

let changer_de_lemme mot ~lem : unit =
  assert (mot.flexions = []) ;
  assert (lem.islem = "1") ;
  begin match mot.parent with
  | None     -> assert (mot.islem = "1")
  | Some lem -> assert (mot.islem = "0") ; supprimer_flexion_de_lemme ~lem mot
  end ;
  mot.islem <- "0" ;
  mot.lemme <- lem.ortho ;
  mot.parent <- Some lem ;
  ajouter_flexion_a_lemme mot ~lem

let fusionner_dans ~mot1 ~mot2 : unit =
  assert (mot1.flexions = []) ;
  let lem2 =
    begin match mot2.parent with
    | None      -> assert (mot2.islem = "1") ; mot2
    | Some lem2 -> assert (mot2.islem = "0") ; lem2
    end
  in
  lem2.freqlemfilms <- addition_freq lem2.freqlemfilms mot1.freqfilms ;
  lem2.freqlemlivres <- addition_freq lem2.freqlemlivres mot1.freqlivres ;
  lem2.flexions |> List.iter begin fun flexion ->
    flexion.freqlemfilms <- lem2.freqlemfilms ;
    flexion.freqlemlivres <- lem2.freqlemlivres
  end ;
  mot2.freqfilms <- addition_freq mot2.freqfilms mot1.freqfilms ;
  mot2.freqlivres <- addition_freq mot2.freqlivres mot1.freqlivres ;
  supprimer mot1

(*
let fusionner_faux_lemme_dans_lemme ~lem1 ~lem2 : unit =
  assert (lem1.islem = "1") ;
  assert (lem2.islem = "1") ;
  begin match lem1.flexions with
  | [flexion1] ->
      lemmifier flexion1 ;
      fusionner_dans ~mot1:flexion1 ~mot2:lem2 ;
  | _ -> assert false
  end
*)

(*
 * Corrections ad-hoc
 *)

let fusionner_dans_cgram ortho ~cgram1 ~cgram2 =
  assert (cgram1 <> cgram2) ;
  fusionner_dans (mot cgram1 ortho) (mot cgram2 ortho)

let corriger_adv_doublon ortho =
  lemmifier (mot "ADV" ortho) ;
  fusionner_dans_cgram ortho ~cgram1:"" ~cgram2:"ADV"

(*
let corriger_adv_doublon' ortho =
  fusionner_faux_lemme_dans_lemme
    (mot "ADV" (ortho^"e"))
    (mot ~islem:true "ADV" ortho)
*)
let corriger_adv_doublon' ortho =
  fusionner_dans
    (mot ~islem:false "ADV" ortho)
    (mot ~islem:true  "ADV" ortho)

let () =
  (* mots ADV dont le lemme est mal orthographié: *)
  lemmifier (mot "ADV" "y") ; (* pointait vers "yu" *)
  lemmifier (mot "ADV" "re") ; (* pointait vers "r" *)
  (* mots ADV en double, un exemplaire ayant son lemme bien orthographié et
   * l’autre non: *)
  corriger_adv_doublon' "aujourd'hui" ;  (* pointait vers "aujourd'huie" *)
  corriger_adv_doublon' "c'est-à-dire" ; (* pointait vers "c'est-à-diree" *)
  corriger_adv_doublon' "d'emblée" ;     (* etc· *)
  (* adverbes en double, un exemplaire ayant son lemme mal orthographié et
   * l’autre étant classé sans cgram: *)
  corriger_adv_doublon "à brûle-pourpoint" ; (* pointait vers "à_brûle-pourpoint" *)
  corriger_adv_doublon "à cloche-pied" ;     (* pointait vers "à_cloche-pied" *)
  corriger_adv_doublon "à rebrousse-poil" ;  (* etc· *)
  corriger_adv_doublon "à tire-larigot" ;
  corriger_adv_doublon "à touche-touche" ;
  corriger_adv_doublon "à tue-tête" ;
  (* autres lemmes sans cgram: *)
  fusionner_dans_cgram "o" ~cgram1:"" ~cgram2:"NOM" ;
  fusionner_dans_cgram "team" ~cgram1:"" ~cgram2:"NOM" ;
  assert (Hashtbl.find lemmes_par_cgram "" = []) ;
  Hashtbl.remove lemmes_par_cgram "" ;
  (* lemmes mal classés dans ADJ:ind: *)
  fusionner_dans_cgram "tout-fait" ~cgram1:"ADJ:ind" ~cgram2:"ADJ" ; (* ou "ADV" ou "NOM" *)
  (* lemmes mal classés dans ADJ:num: *)
  fusionner_dans_cgram "autres" ~cgram1:"ADJ:num" ~cgram2:"ADJ" ; (* ou "ADV" ou "NOM" *)
  fusionner_dans_cgram "centaines"   ~cgram1:"ADJ:num" ~cgram2:"NOM" ;
  fusionner_dans_cgram "centenaires" ~cgram1:"ADJ:num" ~cgram2:"NOM" ; (* ou "ADJ" *)
  fusionner_dans_cgram "centrales"   ~cgram1:"ADJ:num" ~cgram2:"NOM" ; (* ou "ADJ" *)
  fusionner_dans_cgram "ici" ~cgram1:"ADJ:num" ~cgram2:"ADV" ;
  fusionner_dans_cgram "midi" ~cgram1:"ADJ:num" ~cgram2:"NOM" ;
  (* lemmes mal classés dans ART:def: *)
  supprimer (mot "ART:def" "la-la-la") ; (* TODO: reclasser en ONO ou NOM ? *)
  (* lemmes mal classés dans ADV: *)
  fusionner_dans_cgram "bons-cadeaux" ~cgram1:"ADV" ~cgram2:"NOM" ;
  (* lemmes mal classés dans CON: *)
  fusionner_dans_cgram "puissamment" ~cgram1:"CON" ~cgram2:"ADV" ;
  fusionner_dans_cgram "comment"     ~cgram1:"CON" ~cgram2:"ADV" ;
  fusionner_dans_cgram "pourquoi"    ~cgram1:"CON" ~cgram2:"ADV" ;
  fusionner_dans_cgram "néanmoins"   ~cgram1:"CON" ~cgram2:"ADV" ;
  supprimer (mot "CON" "ains") ; (* ??? *)
  (* lemmes mal classés dans PRE: *)
  fusionner_dans_cgram "aube" ~cgram1:"PRE" ~cgram2:"NOM" ;
  fusionner_dans_cgram "sous-prieur" ~cgram1:"PRE" ~cgram2:"NOM" ;
  (* lemmes mal classés dans PRO:ind: *)
  fusionner_dans_cgram "autrichiens" ~cgram1:"PRO:ind" ~cgram2:"ADJ" ; (* ou "NOM" *)
  (* lemmes mal classés dans PRO:per: *)
  fusionner_dans_cgram "ou" ~cgram1:"PRO:per" ~cgram2:"CON" ;
  fusionner_dans_cgram "con" ~cgram1:"PRO:per" ~cgram2:"NOM" ; (* ??? ou "ADJ" *)
  fusionner_dans (mot "PRO:per" "to") (mot "PRO:per" "toi") ;
  (* lemmes PRO:pos mal orthographiés: *)
  fusionner_dans (mot "PRO:pos" "notre") (mot "PRO:pos" "nôtre") ;
  fusionner_dans (mot "PRO:pos" "votre") (mot "PRO:pos" "vôtre") ;
  (* lemmes AUX fallacieux: *)
  fusionner_dans
    (mot ~islem:true  "AUX" "aurai")
    (mot ~islem:false "AUX" "aurai") ;
  (* lemmes VER fallacieux: *)
  (* … *)
  (* verbes à l’impératif mal classés:
   * TODO les reclasser au lieu de les supprimer. *)
  supprimer (mot "ADJ:pos" "tiens-la-moi") ;
  supprimer (mot "NOM" "faîtes-la-moi") ;
  supprimer (mot "NOM" "file-la-moi") ;
  supprimer (mot "NOM" "glisse-la-moi") ;
  supprimer (mot "NOM" "laisse-la-moi") ;
  supprimer (mot "NOM" "mets-la-toi") ;
  supprimer (mot "NOM" "montre-la-moi") ;
  supprimer (mot "VER" "montre-la-moi") ;
  supprimer (mot "NOM" "passe-la-moi") ;
  supprimer (mot "NOM" "pose-la-moi") ;
  supprimer (mot "NOM" "décrochez-moi-ça") ;
  (* autres: *)
  supprimer (mot "ADV" "re") ; (* ??? *)
  supprimer (mot "VER" "y") ; (* ??? *)
  supprimer (mot "NOM" "ya") ; (* ??? *)
  supprimer (mot "ART:ind" "pa") ;
  supprimer (mot "NOM" "pa") ;
  supprimer (mot "NOM" "to") ; (* ??? *)
  (* ↑ NOTE: ce mot existe mais les fréquences semblent très élevées pour ce que
   * ça signifie (bouillie traditionnelle du Mali). *)
  supprimer (mot "ADV" "il") ;
  supprimer (mot "ONO" "hai") ; (* ??? *)
  fusionner_dans_cgram "n'" ~cgram1:"AUX" ~cgram2:"ADV" ; (* !!! fréquences très élevées *)
  fusionner_dans_cgram "n'" ~cgram1:"NOM" ~cgram2:"ADV" ;
  fusionner_dans_cgram "n'" ~cgram1:"PRO:per" ~cgram2:"ADV" ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "tig")  "NOM" "tiges")
    (mot ~filtre:(fun m -> m.lemme = "tige") "NOM" "tiges") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "rester")  "VER" "regretter")
    (mot "VER" "regrettez") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "bouffi")  "VER" "bouffis")
    (mot ~filtre:(fun m -> m.lemme = "bouffir") "VER" "bouffis") ;
  changer_de_lemme (mot "VER" "bourrées") (mot "VER" "bourrer") ;
  changer_de_lemme (mot "VER" "choses") (mot "VER" "choser") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "connais")   "VER" "connais")
    (mot ~filtre:(fun m -> m.lemme = "connaître") "VER" "connais") ;
  fusionner_dans (mot "NOM" "court-circuits") (mot "NOM" "courts-circuits") ;
  fusionner_dans (mot "ADJ" "court-circuits") (mot "NOM" "courts-circuits") ;
  fusionner_dans (mot "VER" "court-circuits") (mot "VER" "court-circuite") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "débraillé")  "VER" "débraillés")
    (mot ~filtre:(fun m -> m.lemme = "débrailler") "VER" "débraillés") ;
  changer_de_lemme (mot "VER" "découvertes") (mot "VER" "découvrir") ;
  changer_de_lemme (mot "VER" "découverte") (mot "VER" "découvrir") ;
  (* ↑ FIXME: échoue car «découverte» est déclarée comme lemme, à tort. *)
  changer_de_lemme (mot "VER" "instruite") (mot "VER" "instruire") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "mentez") "VER" "mentez")
    (mot ~filtre:(fun m -> m.lemme = "mentir") "VER" "mentez") ;
  changer_de_lemme (mot "VER" "nue") (mot "VER" "nuer") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "parait") "VER" "parait")
    (mot ~filtre:(fun m -> m.lemme = "parer")  "VER" "parait") ;
  (* TODO: Le mot suivant devrait être renommé, pas supprimé (c’est le participe
   * passé «plu» de «plaire» avec une faute d’orthographe «plus» ?). *)
  supprimer (mot ~filtre:(fun m -> m.lemme = "plus") "VER" "plus") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "réélus")  "VER" "réélus")
    (mot ~filtre:(fun m -> m.lemme = "réélire") "VER" "réélus") ;
  fusionner_dans
    (mot ~filtre:(fun m -> m.lemme = "sais")   "VER" "sais")
    (mot ~filtre:(fun m -> m.lemme = "savoir") "VER" "sais") ;
  (* On supprime des mots anglais: *)
  supprimer (mot "NOM" "line") ;
  supprimer (mot "NOM" "team") ;
  supprimer (mot "CON" "because") ;
  supprimer (mot "PRE" "bicause") ; (* en tout cas ça devrait être CON *)
  (* On supprime les chiffres romains, enregistrés comme adjectifs numéraux: *)
  [ "i" ; "v" ; "x" ; "l" ; "c" ; "d" ; "m" ; "dc" ; "dm" ; "il" ; "ml" ; "mm" ]
  |> List.iter (fun ortho -> supprimer (mot "ADJ:num" ortho)) ;
  statistiques_par_cgram ~msg:"après nettoyage"



(*
 * Tri alphabétique (cosmétique)
 *)

(* TODO: vrai ordre alphabétique *)
let mesure_alphabetique m =
  (m.ortho, m.phon, m.cgram)
let comparer_alphabetique m1 m2 =
  Pervasives.compare (mesure_alphabetique m1) (mesure_alphabetique m2)

let () =
  lemmes_par_cgram |> Hashtbl.iter begin fun cgram lems ->
    Hashtbl.replace lemmes_par_cgram cgram (List.sort comparer_alphabetique lems) ;
    lems |> List.iter begin fun lem ->
      lem.flexions <- List.sort comparer_alphabetique lem.flexions
    end
  end



(*
 * Traduction
 *)

let mesure_frequence lem =
  (float_of_string lem.freqlemfilms, float_of_string lem.freqlemlivres)
let comparer_frequence lem1 lem2 =
  Pervasives.compare (mesure_frequence lem1) (mesure_frequence lem2)

let mesure_longueur lem =
  if lem.id >= 0 then
    (float @@ int_of_string lem.nbsyll,
     float @@ String.length lem.p_cvcv,
     float @@ String.length lem.ortho)
  else if float_of_string lem.freqlemfilms > 0.0 then begin
    let somme_nbsyll = lem.flexions |> List.fold_left
      (fun s e -> s +. float (int_of_string e.nbsyll) *. float_of_string e.freqfilms)
      0.0
    in
    let somme_pcvcv = lem.flexions |> List.fold_left
      (fun s e -> s +. float (String.length e.p_cvcv) *. float_of_string e.freqfilms)
      0.0
    in
    let n = float_of_string lem.freqlemfilms in
    (somme_nbsyll /. n,
     somme_pcvcv /. n,
     float @@ String.length lem.ortho)
  end
  else begin
    let somme_nbsyll = lem.flexions |> List.fold_left
      (fun s e -> s + int_of_string e.nbsyll)
      0
    in
    let somme_pcvcv = lem.flexions |> List.fold_left
      (fun s e -> s + String.length e.p_cvcv)
      0
    in
    let n = float (List.length lem.flexions) in
    (float somme_nbsyll /. n,
     float somme_pcvcv /. n,
     float @@ String.length lem.ortho)
  end
let comparer_longueur lem1 lem2 =
  Pervasives.compare (mesure_longueur lem1) (mesure_longueur lem2)

let () =
  lemmes_par_cgram |> Hashtbl.iter begin fun cgram lems ->
    let lems_tries_par_freq =
      lems |> List.sort (fun lem1 lem2 -> comparer_frequence lem2 lem1) in
    let lems_tries_par_longueur =
      lems |> List.sort comparer_longueur in
    List.iter2 begin fun frequent_lem short_lem ->
      frequent_lem.image <- Some short_lem
    end
      lems_tries_par_freq lems_tries_par_longueur
  end
