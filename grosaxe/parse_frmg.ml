#!env ocaml

(* load findlib (to be able to #require libraries) *)
try Topdirs.dir_directory (Sys.getenv "OCAML_TOPLEVEL_PATH") with Not_found -> () ;;
#use "topfind"

#require "unix"
#require "xml-light"

type mstag =
  {
    mode : string option ; (* indicative | subjonctive | conditional | imperative | ? *)
    tense : [ `present | `imperfect | `past_historic | `future ] option ; (* present | imperfect | past-historic | future *)
    person : [ `per1 | `per2 | `per3 ] option ; (* 1 | 2 | 3 *)
    gender : [ `masc | `fem ] option ;
    number : [ `sg | `pl ] option ;
  }

let parse_mstag tag =
  begin match Xml.attrib tag "mstag" with
  | value -> value
  | exception Xml.No_attribute _ -> ""
  end
  |> String.split_on_char ' '
  |> List.filter (fun chunk -> chunk <> "")
  |> List.map begin fun chunk ->
    begin match String.split_on_char '.' chunk with
    | [ key ; value ] -> (key, value)
    | [ "gender" ; "masc" ; "fem" ] -> ("gender", "masc.fem")
(*     | [ k1 ; k2 ; value ] -> (k1 ^ ":::::" ^ k2, value) *)
    | _               -> assert false
    end
  end

type word =
  {
    label : string ;
    form : string ;
    lemma : string ;
    pos : string ;
    mstag : (string * string) list ;
  }

let pp_mstag output mstag =
  if mstag <> [] then begin
    Printf.fprintf output "{" ;
    mstag |> List.iter begin fun (key, value) ->
      Printf.fprintf output " %s=%s" key value
    end ;
    Printf.fprintf output " }"
  end

let parse sentence : word list =
  let (cmd_out, cmd_in) = Unix.open_process "frmg_shell --quiet" in
  Printf.fprintf cmd_in ":passage:xml %s\n" sentence ;
  close_out cmd_in ;
  let x = Xml.parse_in cmd_out in
  close_in cmd_out ;
  let rec folder ((t_tags, w_tags) as acc) x =
    begin match x with
    | Xml.PCData s -> assert false
    | Xml.Element ("T", _, _)      -> (x :: t_tags, w_tags)
    | Xml.Element ("W", _, _)      -> (t_tags, x :: w_tags)
    | Xml.Element (_, _, children) -> List.fold_left folder acc children
    end
  in
  x
  |> Xml.fold folder ([], [])
  |> (fun (t_tags,w_tags) -> List.combine t_tags w_tags)
  |> List.rev
  |> List.map begin fun (t, w) ->
    assert (Xml.attrib w "tokens" = Xml.attrib t "id") ;
    let label = String.trim @@ Xml.pcdata @@ List.hd @@ Xml.children t in
    let form  = Xml.attrib w "form" in
    let lemma = Xml.attrib w "lemma" in
    let pos   = Xml.attrib w "pos" in
    let mstag = parse_mstag w in
    { label ; form ; lemma ; pos ; mstag }
  end

let () =
  begin try while not (Scanf.Scanning.(end_of_input stdin)) do
    Scanf.scanf "%s@\n" @@fun sentence ->
    parse sentence
    |> List.iter begin fun w ->
      Printf.printf "\t«%s»\t[%s «%s» → «%s» %a]\n" w.label w.pos w.form w.lemma pp_mstag w.mstag
    end
  done with End_of_file ->
    ()
  end
