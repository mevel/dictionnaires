Ce dépôt contient divers travaux liés à l’orthographe de la langue française.
Il contient plusieurs bases de données de mots, des programmes informatiques qui
produisent ou exploitent ces bases (les plus notables sont dans les dossiers
`traiter-lex3/` et `traiter-wikt/`), et **[une réflexion sur la rationalisation
de l’orthographe française](./SIMPLIFICATION-ORTHOGRAPHE.md)**.

[dossier-dicos-src]: https://perso.crans.org/mevel/dictionnaires/dicos/
[dossier-dicos-gen]: https://perso.crans.org/mevel/dictionnaires/dicos-générés/

## Bases sources

**[Le dossier `dicos/`][dossier-dicos-src]** contient des dictionnaires récoltés
depuis diverses sources. Comme certains de ces fichiers sont très gros, ce
dossier est exclu du dépôt Git et est à télécharger séparément, à l’adresse
donnée en lien.

- **Dicollecte** :
  composante de Grammalecte, projet open-source de correcteur ortho-grammatical
  pour le français
  (mises à jour régulières)
  [[source]](https://grammalecte.net/)
- **Francais-GUTenberg** 1.0 :
  projet obsolète de correcteur orthographique (publié en 1999 par Christophe
  Pythoud, jamais mis à jour depuis) dont le contenu semble avoir été intégré
  dans Dicollecte ;
  devenu difficile à trouver sur le web, j’ai trouvé une archive ici :
  [[source]](http://ftp.free.fr/mirrors/ftp.gentoo.org/distfiles/28/)
- **Lexique 3** :
  projet de recherche par Boris New et Christophe Pallier
  (mises à jour occasionnelles)
  [[source]](http://www.lexique.org/)
- le **wiktionnaire** francophone :
  dictionnaire en ligne collaboratif
  (constamment modifié)
  [[source]](https://dumps.wikimedia.org/frwiktionary/)
- **FranceTerme** :
  base lexicale de néologismes émise par le Ministère français de la Culture
  (mises à jour régulières)
  [[source]](https://www.culture.fr/franceterme)
- un obscur jeu du pendu

NOTE : un recensement de plein de bases téléchargeables :
https://github.com/chrplr/openlexicon/tree/master/datasets-info

TODO : Utiliser la base [Morphalou][morphalou-doc]
([téléchargement][morphalou-telech]), plus complète que Lexique3, avec des liens
plus riches entre mots (variantes orthographiques), et qui contient elle aussi
des prononciations (issues du TLFi ?).

[morphalou-doc]: https://repository.ortolang.fr/api/content/morphalou/latest/LISEZ-MOI.html
[morphalou-telech]: https://repository.ortolang.fr/api/content/morphalou/latest

## Bases générées

**[Le dossier `dicos-générés/`][dossier-dicos-gen]** contient des fichiers qui
sont générés à partir des sources via nos divers programmes. Les fichiers
`*.mots` sont des listes de mots du français, avec un mot par ligne. De même, le
contenu du dossier est à télécharger séparément via l’adresse donnée.

### Depuis Francais-GUTenberg

Les fichiers `frgut*.mots` sont produits avec iSpell à partir du dictionnaire
Français-GUTenberg 1.0 de Christophe Pythoud.

Le dictionnaire principal est `frgut-général.mots`.
Les fichiers `frgut.*.mots` sont des dictionnaires thématiques.
`frgut.mots` est l’union de tous ces dictionnaires thématiques.

Procédure :

	cd dicos/Francais-GUTenberg-v1.0/
	# génère le fichier francais.hash requis par ispell (avec francais.aff):
	./makehash
	# génère la liste de mots avec les parties voulues (remarque: ./makehash
	# permet également de sélectionner des parties et les concatène dans un
	# fichier francais.dico):
	cat dicos/{series,nonverbes,verbes-gp*,verbes-varia}.dico  \
	| ispell -e -d ./francais  \
	| iconv -f latin1 -t utf8  \
	| tr ' ' '\n' | grep .  \
	| grep -v -e "^[DLdljmtn]'" -e "^qu'"  \
	| sort -u  \
	> frgut-général.mots
	# on enlève les élisions de mots indépendants (l’, m’, qu’…)
	# sauf s’ (pour les verbes pronominaux) et c’ (pour «c’est-à-dire»)

### Depuis Dicollecte

Dicollecte est fourni sous deux formes :

- un tableur au format TSV (c’est-à-dire où les champs sont séparés par des tabulations),
- et une combinaison d’un fichier d’affixes et d’un fichier de racines
  (`*.aff` et `*.dic`) pour le correcteur orthographique `hunspell`.

Le fichier `dicollecte-vXXX-tsv.mots` est généré directement à partir du
tableur, ainsi :

	cat dicos/Dicollecte/lexique-*lecte-fr-vXXX/lexique-*lecte-fr-vXXX.txt  \
	| tail -n +17  \
	| cut -d$'\t' -f3  \
	| sed "s/’/'/g"  \
	| sort -u  \
	> dicollecte-vXXX-tsv.mots

Le fichier `dicollecte-vXXX-unmunch.mots` est généré depuis les fichiers du
correcteur orthographique, au moyen du programme `unmunch` de la suite
logicielle hunspell, ou de ses réimplémentations (voir ci-dessous; c’est la
version `ocaml-unmunch` qu’il faut privilégier, les autres contiennent un grand
nombre de bugs) :

	unmunch dicos/Dicollecte/hunspell-french-dictionaries-vXXX/fr-toutesvariantes.{dic,aff}  \
	| sed 's/\/[^|]*|//g'  \
	| cut -d' ' -f1  \
	| grep -v '\(^\|[^0-9]\)0'  \
	| grep -v -i "^\([dljmtcçsn]\|qu\|jusqu\|lorsqu\|puisqu\|quoiqu\)'."  \
	| sort -u  \
	> dicollecte-vXXX-unmunch.mots

	unmunch/other-implementations/unmunch.sh dicos/Dicollecte/hunspell-french-dictionaries-vXXX/fr-toutesvariantes.{aff,dic}  \
	| grep -v '\(^\|[^0-9]\)0'  \
	| grep -v -i "^\([dljmtcçsn]\|qu\|jusqu\|lorsqu\|puisqu\|quoiqu\)'."  \
	| sort -u  \
	> dicollecte-vXXX-unmunch-sh.mots

	unmunch/ocaml-unmunch dicos/Dicollecte/hunspell-french-dictionaries-vXXX/fr-toutesvariantes.{aff,dic}  \
	| tee dicollecte-vXXX-ocaml-unmunch.mots-et-morph  \
	| cut -d$'\t' -f1  \
	| grep -v -i "^\([dljmtcçsn]\|qu\|jusqu\|lorsqu\|puisqu\|quoiqu\)'."  \
	| sort -u  \
	> dicollecte-vXXX-ocaml-unmunch.mots

Une des commandes `grep` supprime des mots avec apostrophes qui sont
incorrectement générés par les règles d’affixes de Dicollecte ; attention,
cela supprime aussi les mots valides suivants, qui se trouvent dans le tableur :

	c'est-à-dire       jusqu'au-boutisme     m'dame       t'elle
	d'aucuns           jusqu'au-boutismes    m'sieur      t'elles
	D'Holbach          jusqu'au-boutiste     n'importe    t'il
	m'as-tu-vu         jusqu'au-boutistes    m'en         t'ils
	qu'en-dira-t-on    N'Djamena             t'en         t'on

Un autre `grep` supprime des mots qui sont générés par erreur, et la commande
`sed` efface des dérivations non terminées, dans les deux cas à cause de bugs
dans `unmunch` (voir ci-dessous).

### Depuis Lexique3

Le fichier `lexique383.tsv` est un tableur au format TSV (une entrée par
ligne, les champs sont séparés par des tabulations) qui liste les mots du
français avec leur prononciation, leur graphémisation et diverses données
grammaticales. Il est produit à partir de la base de données Lexique 3.83.

    cd traiter-wikt/
    ./traiter_lex3.py | tee log.txt
    # la commande ci-dessus lit le fichier `Lexique383.tsv`
    # et produit le fichier `Lexique383.traité.tsv`

Pour plus de détails, cf `traiter-lex3/README.md`.

### Depuis le Wiktionnaire

Le fichier `frwikt-XXX.tsv` est un tableur au format TSV (une entrée par
ligne, les champs sont séparés par des tabulations) qui liste les mots du
français avec leur(s) prononciation(s). Il est extrait à partir d’un dump du
Wiktionnaire (`XXX` est la date qui figure dans le nom du dump).

    # [voir instructions détaillées dans le README du sous-dossier]

    # traiter le contenu du Wiktionnaire (long) :
    traiter-wikt/extract-prons-frwikt.py > frwikt-XXX.vrac.tsv
    # trier le tableur résultat :
    traiter-wikt/sort-tsv.sh < frwikt-XXX.vrac.tsv > frwikt-XXX.tsv

Pour plus de détails, cf `traiter-wikt/README.md`.

## Programmes

### `unmunch/`

Le programme officiel `unmunch` est obsolète et ne supporte pas correctement
toutes les fonctionnalités de hunspell utilisées par Dicollecte (voir [ce post
sur StackOverflow][unmunch-so]), ce qui fait qu’il génère un peu n’importe quoi
et ne génère pas certains mots attendus.

Le dossier `unmunch/` contient une réimplémentation de ce programme, nommée
`ocaml-unmunch`, qui ne supporte pas toutes les fonctions de hunspell mais
semble supporter tout ce qui est nécessaire pour Dicollecte, et donne de bien
meilleurs résultats que le programme officiel. L’usage est le même, à la
différence que `ocaml-unmunch` sépare le mot lui-même et les informations
dérivées de morphologie par une tabulation plutôt que par un espace blanc (car
certains mots peuvent contenir des espaces), et il faut donc remplacer `cut -d'
'` par `cut -d$'\t'` dans la chaîne ci-dessus.

Le dossier `unmunch/other-implementations/`collecte diverses autres
réimplémentations trouvées sur Internet (voir le post StackOverlow mentionné
précédemment), qui marchent plus ou moins bien. La seule que j’ai réussi
à utiliser est `unmunch.sh`, qui est meilleure que le programme officiel mais
moins bon que `ocaml-unmunch`.

[unmunch-so]: https://stackoverflow.com/questions/13725861/generate-all-word-forms-using-lucene-hunspell/#66661578

### `grosaxe/`

Grosaxe est un projet-jouet sur la base Lexique3 et l’analyseur grammatical FRMG
(projet de recherche, qui étiquette chaque mot d’un texte fourni avec sa
fonction grammaticale). Le résultat n’est pas très intéressant (calcule une
permutation des mots du dictionnaire en associant les signifiés les plus
fréquents aux signifiants les plus courts) mais le projet démontre comment
traiter Lexique3 et FRMG en OCaml.

Il contient une liste d’erreurs corrigées dans Lexique3
(subsumée par la liste de corrections dans le projet ci-dessous).

### `traiter-lex3/`

Ce dossier contient un programme qui re-traite les données de la base Lexique3,
tout particulièrement la prononciation des mots. Il calcule une graphémisation
et une nouvelle syllabation, meilleure que celle fournie dans Lexique3.

Ce projet corrige également un grand nombre d’erreurs dans Lexique3.

### `traiter-wikt/`

Un programme qui traite le Wiktionnaire pour en extraire les prononciations des
mots de la langue française.

## Licence

Les bases de données sources sont sujettes à leur licence respective. Les codes
contenus dans ce dépôt sont soumis à la licence [WTFPL][] (version 2).

[WTFPL]: https://fr.wikipedia.org/wiki/WTFPL
