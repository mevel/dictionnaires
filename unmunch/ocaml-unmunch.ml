(*! #!/usr/bin/env ocaml !*)

(***
 ***  A reimplementation of hunspell’s program `unmunch`
 ***)

(* (April 2021)
 *
 * The `unmunch` program from hunspell (1.7.2) is obsolete and does not support
 * all features of hunspell. Tested on the reference French affix dictionary
 * (Dicollecte), it generates garbage and misses some expected words.
 *
 * Here are some observed failures:
 *
 *   - It ignores the NEEDAFFIX flag
 *     (unless this is a consequence of not supporting "FLAG long").
 *   - It derives words that start or end with "0",
 *     because it seems unaware that an affix string of "0" means the empty string.
 *   - It applies many affix rules out of the blue,
 *     because it does not seem to support "FLAG long" and thus matches flag
 *     names by their first character only.
 *       * e.g. with Dicollecte:
 *           ohm/Um()               =>  flags applied: Um, Ui, U., m'
 *     Also because it parses morph data in *.dic files as affix flag names.
 *       * e.g. with Dicollecte:
 *           nu/F.() po:adj         =>  flags applied: d', j'
 *           nu/S.() po:nom is:mas  =>  flags applied: n', m', s'
 *   - It misses applying some expected affix rules,
 *     because it seems it does not expand continuation flags.
 *       * e.g. with Dicollecte, many affixes have the continuation flags L', D', Q'
 *         which respectively add the prefixes "l', d', qu'";
 *         `unmunch` does not apply these affix rules.
 *   - It does not write derived morphological data (?).
 *
 * This is a partial reimplementation of `unmunch`. It does not support all
 * features, but it fixes all the defects above and seems enough for Dicollecte.
 *
 * Usage:
 *
 *     ocaml-unmunch AFFIXES.aff STEMS.dic > WORDS_WITH_MORPH.txt
 *
 * The output is compatible with that of `unmunch`, except that between the
 * generated word and its morphological data, there is a tabulation rather than
 * a space character (because words may contain spaces).
 *
 * Compile this code with:
 *
 *     ocamlfind ocamlopt -package re -linkpkg ocaml-unmunch.ml -o ocaml-unmunch
 *
 * About supported features: the documentation I could find:
 *
 *   - `man 5 hunspell`
 *   - https://www.systutorials.com/docs/linux/man/4-hunspell/
 *
 * is often imprecise or incomplete, so I had to guess the intended behavior of
 * some features; I may have got them wrong. Some other features I could not
 * even understand, thus I did not implement them. Unsupported features are
 * listed below.
 *
 * TODO: support -p and -s for prefix-only and suffix-only
 *
 * UNSUPPORTED:
 *   - personal dictionaries
 *   - SET: character encoding: only UTF-8 is supported
 *   - LANG: language-specific functions
 *   - AF: aliases for vectors of flags:
 *       + using positional numbers as identifiers is obscure and error-prone.
 *       + it is unclear how to parse flags where there can be aliases.
 *           * is 1 a flag name or a flag alias?
 *           * what about 11 in single-byte mode?
 *           * do the answers depend on how many aliases are defined?
 *       + recursive expansion of affix rules already allows to define a flag
 *         whose rule (prefix or suffix is irrelevant) adds more flags without
 *         altering the stem, in addition we get a custom, human-friendly name.
 *   - AM: aliases for morphological fields: same reasons
 *   - COMPLEXPREFIXES: ???
 *   - CIRCUMFIX: ???
 *   - generation of compound words (not sure if relevant):
 *     + COMPOUNDRULE
 *     + COMPOUNDMORESUFFIXES
 *     + COMPOUNDWORDMAX
 *     + CHECKCOMPOUNDDUP
 *     + CHECKCOMPOUNDCASE
 *     + CHECKCOMPOUNDTRIPLE
 *     + SIMPLIFIEDTRIPLE
 *     + CHECKCOMPOUNDPATTERN
 *     + FORCEUCASE
 *     + COMPOUNDSYLLABLE
 *     + SYLLABLENUM
 *
 * TODO: flags to forward to output:
 *   - NOSUGGEST
 *   - SUBSTANDARD
 *   - WARN
 *   - COMPOUNDROOT
 *   - COMPOUNDFLAG
 *   - COMPOUNDBEGIN
 *   - COMPOUNDMIDDLE
 *   - COMPOUNDLAST
 *   - ONLYINCOMPOUND
 *   - COMPOUNDPERMITFLAG
 *   - COMPOUNDFORBIDFLAG
 * flags to implement:
 *   - FORBIDDENWORD
 *   - FULLSTRIP
 *   - KEEPCASE
 *   - ICONV
 *   - OCONV
 *   - LEMMA_PRESENT
 *   - PSEUDOROOT
 *   - WORDCHARS
 *
 * NOTE: these flags are rrelevant for word generation:
 *   - IGNORE
 *   - KEY
 *   - TRY
 *   - REP
 *   - MAP
 *   - PHONE
 *   - MAXCPDSUGS
 *   - MAXNGRAMSUGS
 *   - MAXDIFF
 *   - ONLYMAXDIFF
 *   - NOSPLITSUGS
 *   - SUGSWITHDOTS
 *   - BREAK
 *   - CHECKCOMPOUNDREP
 *   - CHECKSHARPS
 *   - FORBIDWARN
 *
 * FIXME: more accurate parsing
 *   - dictionary words can contain spaces!
 *       + can morphological data only be written after a slash?
 *         no! there is this example:
 *             do not know ph:dunno
 *         "space  or  tabulator separated  morphological description fields,
 *         started with 3-character (two letters and a colon) field IDs:"
 *       + should extremities be stripped? should middle spaces be collapsed?
 *   - according to the man page, stems can be escaped with " ", in which case
 *     they can contain slashes (what about colons, quotes, #… ?)
 *
 * FIXME: we use regular expressions from library "re", but these apparently do
 * not support UTF-8, hence in theory we can have wrong results when testing the
 * pattern-condition of affixes!
 *)

(*
 *  Misc. string processing
 *)

(*! #require "re" ;; !*)

let re (s : string) : Re.re =
  Re.compile (Re.Posix.re s)

let split_on_blanks : string -> string list =
  Re.split (re"[ \t]+")

let is_num : string -> bool =
  Re.execp (re"[0-9]+")

let string_drop (s : string) (n : int) =
  if n = 0 then
    s
  else if n > 0 then
    String.sub s n (String.length s - n)
  else
    String.sub s 0 (String.length s + n)

let string_starts_with ?(from : int = 0) (s : string) (sub : string) : bool =
  let len_s = String.length s in
  let len_sub = String.length sub in
  if from < 0 || len_s < from then
    raise @@ Invalid_argument "string_starts_with" ;
  if len_s < from + len_sub then
    false
  else begin
    let exception Break in
    begin try
      for i = 0 to len_sub - 1 do
        if s.[from + i] <> sub.[i] then
          raise Break
      done ;
      true
    with Break ->
      false
    end
  end

let string_ends_with ?(til : int option) (s : string) (sub : string) : bool =
  let len_s = String.length s in
  let len_sub = String.length sub in
  let til = Option.value ~default:len_s til in
  if til < 0 || len_s < til then
    raise @@ Invalid_argument "string_ends_with" ;
  if til < len_sub then
    false
  else begin
    let exception Break in
    begin try
      for i = 1 to len_sub do
        if s.[til - i] <> sub.[len_sub - i] then
          raise Break
      done ;
      true
    with Break ->
      false
    end
  end

(*
 *  Error handling
 *)

let current_filename = ref ""
let current_line = ref 0

let error fmt =
  Printf.kprintf (fun s -> print_endline s ; failwith s)
    ("ERROR: %s:%i: " ^^ fmt) !current_filename !current_line

let warning fmt =
  Printf.eprintf
    ("WARNING: %s:%i: " ^^ fmt ^^ "\n") !current_filename !current_line

let info fmt =
  Printf.eprintf
    ("INFO: %s:%i: " ^^ fmt ^^ "\n") !current_filename !current_line

(*
 *  Parsing the affix file
 *)

type flag_syntax =
  | Byte (* a flag is one octet *)
  | Long (* a flag is two octets *)
  | Utf8 (* a flag is one UTF-8–encoded code point *)
  | Num  (* a flag is a base-10 integer from 1 to 65000 *)
    (* NOTE: We do not check the range of numeric flags, so we accept more flags
     * than what is prescribed by hunspell’s documentation. However we require
     * them to start with a non-zero digit (in particular, "0" is forbidden). *)

let parse_flag_syntax : string -> flag_syntax = function
  | "long"  -> Long
  | "UTF-8" -> Utf8
  | "num"   -> Num
  | fs      -> error "wrong flag syntax: %S" fs

type flag_name = string

type flag_kind =
  | Prefix
  | Suffix

type flag =
  {
    name : flag_name ;
    kind : flag_kind ;
    crossable : bool ;
  }

let parse_flag_kind : string -> flag_kind = function
  | "PFX" -> Prefix
  | "SFX" -> Suffix
  | kind  -> error "wrong flag kind: %S" kind

let re_flag_long = re""

let parse_flag_list ~(fs : flag_syntax) ?(from : int = 0) (s : string)
  : flag_name list =
  if String.length s <= from then
    []
  else begin match fs with
  | Byte ->
      Re.matches (re".") ~pos:from s
  | Long ->
      if (String.length s - from) land 1 <> 0 then
        error "wrong long flag list: %S" s ;
      Re.matches (re"..") ~pos:from s
  | Utf8 ->
      error "FIXME: UTF-8 flag type is not supported"
  | Num ->
      (* we keep flags as strings, but, to ensure canonical representation of
       * numbers, we forbid leading zeros. *)
      if not @@ Re.execp (re"^[1-9][0-9]*(,[1-9][0-9]*)*$") ~pos:from s then
        error "wrong numeric flag list: %S" s ;
      Re.split (re",") ~pos:from s
  end

(* word form, i.e. a final term *)
type word =
  {
    word : string ;
    morph : string list ;
  }

type flag_partition =
  {
    crossable_pfx    : flag list ;
    noncrossable_pfx : flag list ;
    crossable_sfx    : flag list ;
    noncrossable_sfx : flag list ;
    needs_affix : bool ;
  }

(* a stem, i.e. a term which can lead to further affixing, and is not
 * necessarily final *)
type stem =
  {
    stem : string ;
    flags : flag_partition ;
    morph : string list ;
  }

type affix_rule =
  {
    flag : flag ;
    condition : Re.re ;
    stripped : int ;
    affix : stem ;
  }

type affix_file =
  {
    mutable flag_syntax : flag_syntax ;
    mutable needaffix_flag : flag_name ;
    (* TODO: carry these special flags to the output *)
    mutable circumfix_flag : flag_name ; (* ??? *)
    mutable keepcase_flag  : flag_name ;
    mutable nosuggest_flag : flag_name ;
    mutable forbidden_flag : flag_name ;
    flags : (flag_name, flag) Hashtbl.t ; (* → only one flag *)
    rules : (flag_name, affix_rule) Hashtbl.t ; (* → a list of rules *)
    (*! mutable fullstrip : bool ; !*) (* TODO: support this *)
  }

let zero_as_empty_string (s : string) : string =
  if s = "0" then "" else s

let is_st_morph : string -> bool =
  Re.execp (re"^st:")

let partition_flag_list ~(aff : affix_file) : flag_name list -> flag_partition =
  let rec partition p = function
    | []                 -> p
    | flname :: flnames' ->
        begin match Hashtbl.find aff.flags flname with
        | { kind=Prefix ; _ } as flag ->
            if flag.crossable then
              partition { p with crossable_pfx = flag :: p.crossable_pfx } flnames'
            else
              partition { p with noncrossable_pfx = flag :: p.noncrossable_pfx } flnames'
        | { kind=Suffix ; _ } as flag ->
            if flag.crossable then
              partition { p with crossable_sfx = flag :: p.crossable_sfx } flnames'
            else
              partition { p with noncrossable_sfx = flag :: p.noncrossable_sfx } flnames'
        | exception Not_found ->
            if flname = aff.needaffix_flag then
              partition { p with needs_affix = true } flnames'
            else if List.mem flname [ aff.forbidden_flag ; aff.circumfix_flag ;
                                      aff.keepcase_flag ; aff.nosuggest_flag ] then
              (* TODO *)
              partition p flnames'
            else
              error "unknown flag: %S" flname
        end
  in
  partition { crossable_pfx=[] ; noncrossable_pfx=[] ;
              crossable_sfx=[] ; noncrossable_sfx=[] ;
              needs_affix=false }

let parse_stem ?(is_stem=true) ~(aff : affix_file) (s : string) (morph : string list) : stem =
  let i = begin try String.index s '/' with Not_found -> String.length s end in
  let stem = zero_as_empty_string (String.sub s 0 i) in
  let flags = parse_flag_list ~fs:aff.flag_syntax ~from:(i+1) s in
  let flags = partition_flag_list ~aff flags in
  let morph =
    if List.exists is_st_morph morph || not is_stem then
      morph
    else
      morph @ [ ("st:" ^ stem) ]
  in
  { stem ; flags ; morph ; }

let parse_affix_file (filename : string) : affix_file =
  let aff =
    {
      flag_syntax = Byte ;
      needaffix_flag = "" ;
      circumfix_flag = "" ;
      keepcase_flag  = "" ;
      nosuggest_flag = "" ;
      forbidden_flag = "" ;
      flags = Hashtbl.create 16 ;
      rules = Hashtbl.create 256 ;
    } in
  let file = open_in filename in
  current_filename := filename ;
  current_line := 0 ;
  begin try while true do
      incr current_line ;
      let line = input_line file in
      begin match split_on_blanks line with
      (* the only syntax that we recognize as a comment is a line starting with #
        * (after optional blank characters). *)
      | c :: _ when c.[0] = '#'         -> ()
      | "FLAG" :: fs :: []              -> aff.flag_syntax <- parse_flag_syntax fs
      | "NEEDAFFIX" :: flname :: []     -> aff.needaffix_flag <- flname
      | "CIRCUMFIX" :: flname :: []     -> aff.circumfix_flag <- flname
      | "KEEPCASE"  :: flname :: []     -> aff.keepcase_flag  <- flname
      | "NOSUGGEST" :: flname :: []     -> aff.nosuggest_flag <- flname
      | "FORBIDDENWORD" :: flname :: [] -> aff.forbidden_flag <- flname
      | ("PFX" | "SFX" as kind) :: flname :: ("Y" | "N" as cross) :: num :: []
        when is_num num && not @@ Hashtbl.mem aff.rules flname ->
          (* this syntax is ambiguous, as it can also be understood as defining
            * a rule for `flag` where "Y" or "N" is to be stripped and `num` is to
            * be affixed (especially meaningful when num = "0"); we mitigate this
            * by testing whether this is the first time we encounter that flag. *)
          (* we simply ignore counts silently *)
          let kind = parse_flag_kind kind in
          let flag = { name = flname ; kind ; crossable = (cross = "Y") } in
          Hashtbl.add aff.flags flname flag
      (* TODO: condition is optional *)
      | ("PFX" | "SFX" as kind) :: flname :: strip :: affix :: condition :: morph ->
          let kind = parse_flag_kind kind in
          let flag =
            begin try Hashtbl.find aff.flags flname with Not_found ->
              error "undeclared flag: %S" flname
            end in
          if flag.kind <> kind then
            error "the kind of the affix mismatches the declared kind of its flag in rule: %s" line ;
          (* FIXME: condition is not actually compatible with POSIX regexes
           * (dash has no special meaning) *)
          let condition = if kind = Prefix then "^" ^ condition else condition ^ "$" in
          let strip = zero_as_empty_string strip in
          let wit = Re.witness (Re.Posix.re condition) in
          if kind = Prefix then begin
            if not @@ string_starts_with wit strip then
              warning "the prefix condition %S does not imply the prefix %S"
                condition strip
          end else begin
            if not @@ string_ends_with wit strip then
              warning "the suffix condition %S does not imply the suffix %S"
                condition strip
          end ;
          let rule =
            {
              flag ;
              condition = re condition ;
              stripped = String.length strip ;
              affix = parse_stem ~is_stem:false ~aff affix morph ;
            } in
          Hashtbl.add aff.rules flname rule
      | ("FLAG" | "NEEDAFFIX" | "CIRCUMFIX" | "KEEPCASE" | "NOSUGGEST" |
          "FORBIDDENWORD" | "PFX" | "SFX") :: _ ->
          error "FIXME: unsupported syntax: %s" line
      | ("TRY" | "KEY" | "MAP" | "REP") :: _ ->
          (* ignore silently (they are only useful for suggestions) *)
          ()
      | [] -> ()
      | _ ->
          info "ignoring line: %s" line
      end
    done with End_of_file -> () end ;
  close_in file ;
  aff

(*
  *  Deriving word forms
  *)

let apply_rule (rule : affix_rule) (term : stem) : stem option =
  if Re.execp rule.condition term.stem then
    (* NOTE: possible optimization, but impact seems negligible:
      * reverse-append morphological fields. *)
    Some {
      stem =
        if rule.flag.kind = Prefix then
          rule.affix.stem ^ string_drop term.stem rule.stripped
        else
          string_drop term.stem (-rule.stripped) ^ rule.affix.stem ;
      flags = begin
        let aflags = rule.affix.flags in
        if rule.flag.crossable then
          if rule.flag.kind = Prefix then
            { aflags
              with crossable_sfx = term.flags.crossable_sfx @ aflags.crossable_sfx }
          else
            { aflags
              with crossable_pfx = term.flags.crossable_pfx @ aflags.crossable_pfx }
        else
          aflags
      end ;
      morph = term.morph @ rule.affix.morph @ [ "fl:" ^ rule.flag.name ] ;
    }
  else
    None

let rec derive ~(aff : affix_file) (term : stem) : word Seq.t =
  let tflags = term.flags in
  let seq : word Seq.t =
      tflags.crossable_pfx @ tflags.noncrossable_pfx
    @ tflags.crossable_sfx @ tflags.noncrossable_sfx
    |> List.to_seq
    |> Seq.flat_map begin fun flag ->
      Hashtbl.find_all aff.rules flag.name
      |> List.to_seq
      |> Seq.filter_map (fun rule -> apply_rule rule term)
      |> Seq.flat_map (derive ~aff)
    end
  in
  if tflags.needs_affix then
    seq
  else
    let word = { word = term.stem ; morph = term.morph } in
    fun () -> Seq.Cons (word, seq)

(*
  *  Processing a dictionary
  *)

let do_dictionary ~(f : stem -> unit) ~(aff : affix_file) (filename : string) : unit =
  let dic = open_in filename in
  current_filename := filename ;
  current_line := 1 ;
  begin try
    let first_line = input_line dic in
    if not @@ is_num first_line then
      warning "the first line is not a number, ignoring: %s" first_line ;
    while true do
      incr current_line ;
      begin match split_on_blanks (input_line dic) with
      | []            -> ()
      | stem :: morph -> f (parse_stem ~aff stem morph)
      end
    done
  with End_of_file -> () end ; (* FIXME: this catches exceptions raised by f *)
  close_in dic ;
  ()

(*
  *  Main
  *)

let () =
  let aff_filename = Sys.argv.(1) in
  let dic_filename = Sys.argv.(2) in
  let aff = parse_affix_file aff_filename in
  do_dictionary ~f: begin fun stem ->
    derive ~aff stem
    |> Seq.iter begin fun word ->
      (*! Printf.printf "%s\n" (String.concat "\t" (word.word :: word.morph)) !*)
      Printf.printf "%s\t%s\n" word.word (String.concat " " word.morph)
    end
  end
    ~aff dic_filename
