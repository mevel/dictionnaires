#!/usr/bin/sh

# https://stackoverflow.com/questions/13725861/generate-all-word-forms-using-lucene-hunspell/#66661578

# /!\ EXTREMELY SLOW, DON’T USE
aff='affixes.aff'
dic='stems.dic'
aff="$1"
dic="$2"
cat "$dic" | while read -r stem ; do # read each stem of the file
	stem="${stem%%/*}" # strip the stem from the optional slash (attached affix rules)
	wordforms "$aff" "$dic" "$stem" # generate all forms for this stem
done
