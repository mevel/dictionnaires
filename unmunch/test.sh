#!/usr/bin/sh

NAME=${1%.dic}

[ -z "$NAME" ] && exit 1

unmunch 2>&- "$NAME.dic" fr_FR.aff  \
| tee "$NAME.unmunch.mots-et-morph"  \
| cut -d/ -f1 | sort -u  \
> "$NAME.unmunch.mots"

other-implementations/unmunch.sh fr_FR.aff "$NAME.dic"  \
| sort -u  \
> "$NAME.unmunch-sh.mots"

./ocaml-unmunch 2>/dev/null fr_FR.aff "$NAME.dic"  \
| tee "$NAME.ocaml-unmunch.mots-et-morph"  \
| cut -d$'\t' -f1 | sort -u  \
> "$NAME.ocaml-unmunch.mots"
