hunspell’s `unmunch` programm is obsolete and does not support all hunspell
features (see the introductory comment in the source code `ocaml-unmunch.ml` for
a list of defects).

`ocaml-unmunch` is a reimplementation of it. Contrary to the official version,
it does support the reference French dictionary (Dicollecte).

The `other-implementations/` folder contains various other implementations
[gathered around the web][so], which might or might not work. The only one
I managed to run is `unmunch.sh`. It is slightly better than the official
`unmunch` because it supports `FLAG long`, but still incorrect for the French
dictionary.

Most notable defects of both `unmunch` and `unmunch.sh`: they seem to limit rule
chains to a depth of 2, i.e. from each initial stem, they apply at most
2(?) rules. But the French dictionary has longer rule chains, so these program
miss some words that should be generated. Also, neither one support `0` as
a special syntax to mean an empty affix string, thus they generate garbage words
that contain the character `0`. Our implementation `ocaml-unmunch` fixes both
these issues: it support arbitrary depths and the `0` string.

[so]: https://stackoverflow.com/questions/13725861/generate-all-word-forms-using-lucene-hunspell/#66661578
