# PLAGES DE LEXIQUE 3.83 DONT LES PHONOLOGIES ONT ÉTÉ VÉRIFIÉES MANUELLEMENT EXHAUSTIVEMENT:
#     w -- z*
#     tous les mots commençant par une lettre diacritée sauf "é"

import dataclasses as dc
import difflib

import alphabet
import lexique

correctifs_verbose = False

def log_correctif(id_mot_ou_lem, genre, succes):
    if not succes:
        print(f'! correctif non appliqué: "{id_mot_ou_lem}" ({genre})')
    elif correctifs_verbose:
        mot_ou_lem = \
          "lemme" if lexique.est_identifiant_lemme(id_mot_ou_lem) else "mot"
        print(f'{mot_ou_lem} corrigé: "{id_mot_ou_lem}" ({genre})')

##
## OPÉRATIONS DE CORRECTION UTILES
##

def supprimer(lex, id_mot):
    log_correctif(id_mot, 'supprime', \
      lex.supprimer(id_mot))

def fusionner_dans(lex, id_mot, id_mot_cible, **kwargs):
    log_correctif(id_mot, f'fusionne dans "{id_mot_cible}"', \
      lex.fusionner_dans(id_mot, id_mot_cible, **kwargs))

def corriger_mot(lex, id_mot, id_mot_cible, **kwargs):
    log_correctif(id_mot, f'corrige mot en "{id_mot_cible}"', \
      lex.changer_mot(id_mot, id_mot_cible, **kwargs))

def corriger_cgram(lex, id_mot, cgram_cible, **kwargs):
    log_correctif(id_mot, f'corrige cgram en "{cgram_cible}"', \
      lex.changer_cgram(id_mot, cgram_cible, **kwargs))

def corriger_lemme(lex, id_mot, nouveau_lem, **kwargs):
    log_correctif(id_mot, f'corrige lemme en "{nouveau_lem}"', \
      lex.changer_lemme(id_mot, nouveau_lem, **kwargs))

def lemmifier(lex, id_mot, **kwargs):
    log_correctif(id_mot, 'lemmifie', \
      lex.lemmifier(id_mot, **kwargs))

def corriger_ortho(lex, id_mot, nouvelle_ortho, **kwargs):
    log_correctif(id_mot, f'corrige ortho en "{nouvelle_ortho}"', \
      lex.changer_ortho(id_mot, nouvelle_ortho, **kwargs))

def corriger_underscore(lex, cgram, bonne_ortho):
    mauvaise_ortho = bonne_ortho.replace(' ', '_')
    assert mauvaise_ortho != bonne_ortho
    mauvaise_id1 = f'{mauvaise_ortho};{cgram};{bonne_ortho}'
    mauvaise_id2 = f'{bonne_ortho};;'
    bonne_id     = f'{bonne_ortho};{cgram};'
    lemmifier(lex, mauvaise_id1)
    fusionner_dans(lex, mauvaise_id2, bonne_id)

# pour un mot élidé dont il manque l’apostrophe finale,
# ajoute l’apostrophe et lie le mot au lemme non élidé
# (se terminant par "e"):
# exemple: "puisqu;CON;" --> "puisque;CON;puisqu'"
def corriger_apostrophe(lex, mauvaise_ortho, cgram):
    mauvaise_id = f'{mauvaise_ortho};{cgram};'
    bonne_id    = f"{mauvaise_ortho}e;{cgram};{mauvaise_ortho}'"
    corriger_mot(lex, mauvaise_id, bonne_id)

def _corriger_phono(lex, id_mot, new, old=None):
    try:
        mot = lex._mot(id_mot)
    except lexique.MotInconnu:
        return False
    old_syll = mot["syll"]
    old = mot["phon"]
    if not (all(ord(a) in alphabet.alphabet_phono for a in new)):
        print(f'! ERREUR: caractères de prononciation erronés pour le correctif "{id_mot}": [{new}]')
        return False
    if not (old == None or (old == mot["phon"] and old != new)):
        print(f'! ERREUR: le correctif de prononciation ne s’applique pas: "{id_mot}"')
        return False
    mot["phon"] = new
    # on corrige aussi le champ "syll"; on préserve autant que possible les
    # coupures entre syllabes.
    # FIXME le résultat est souvent incorrect, il vaudrait mieux que la
    # syllabation soit fournie manuellement avec la nouvelle phonologie…
    d = difflib.SequenceMatcher(a=old, b=new, autojunk=False).get_opcodes()
    new_syll = ""
    # k1,k2 sont les indices dans old_syll;
    # i1,i2 sont les indices dans old;
    # j1,j2 sont les indices dans new;
    k1 = 0
    for op, i1, i2, j1, j2 in d:
        k2 = k1
        i = i1
        while i < i2:
            if old_syll[k2] != "-":
                assert old_syll[k2] == old[i]
                i += 1
            k2 += 1
        match op:
            case "equal":
                new_syll += old_syll[k1:k2]
            case "replace":
                new_syll += new[j1:j2]
            case "insert":
                new_syll += new[j1:j2]
                k2 = k1
            case "delete":
                pass
        if k2 < len(old_syll) and old_syll[k2] == "-":
            new_syll += "-"
            k2 += 1
        k1 = k2
    assert k1 == len(old_syll)
    mot["syll"] = new_syll
    return True

def corriger_phono(lex, id_mot, new, old=None):
    log_correctif(id_mot, 'phono', \
      _corriger_phono(lex, id_mot, new, old=old))

##
## ENSEMBLE DES CORRECTIFS MANUELS À APPORTER À LEXIQUE3
##

# Les correctifs s’appliquent à un mot donné, identifié par son ID, c.-à-d. une
# chaîne de caractères "lemme;CGRAM;flexion". La flexion peut être omise quand
# il s’agit du lemme (dans ce cas l’ID est de la forme "lemme;CGRAM;").

def appliquer_correctifs(lex, verbose=False):

    global correctifs_verbose
    correctifs_verbose = verbose

    # numéraux:
    corriger_cgram(lex, "dm;ADJ:num;", "NOM") # unité (décimètre)
    corriger_cgram(lex, "ml;ADJ:num;", "NOM") # unité (millilitre)
    corriger_cgram(lex, "mm;ADJ:num;", "NOM") # unité (millimètre)
    supprimer(lex, "il;ADJ:num") # ??? pourrait à la rigueur être un chiffre romain exotique mais alors il faut corriger la phono
    # supprime les chiffres romains
    # (sinon, il faudrait corriger leur prononciation):
    supprimer(lex, "i;ADJ:num")
    supprimer(lex, "v;ADJ:num")
    supprimer(lex, "x;ADJ:num")
    supprimer(lex, "l;ADJ:num")
    supprimer(lex, "c;ADJ:num")
    supprimer(lex, "d;ADJ:num")
    supprimer(lex, "m;ADJ:num")
    supprimer(lex, "dc;ADJ:num") # ???

    # autres mots mono-alphabétiques:
    corriger_phono(lex, "c;NOM;", "se", old="s")
    corriger_apostrophe(lex, "c", "PRO:dem")
    corriger_phono(lex, "d;NOM;", "de", old="d")
    fusionner_dans(lex, "d;NOM;d'", "d;NOM;") # ou "de;PRE;d'"
    corriger_apostrophe(lex, "d", "PRE")
    corriger_lemme(lex, "d';PRE;", "de")
    supprimer(lex, "2e;ADJ")
    supprimer(lex, "7e;ADJ")
    supprimer(lex, "58e;ADJ")
    supprimer(lex, "é;ADV") # ???
    corriger_phono(lex, "e;NOM;", "°", old="2")
    fusionner_dans(lex, "ie;NOM;i", "i;NOM;")
    corriger_phono(lex, "j;NOM;", "Zi", old="Z")
    corriger_apostrophe(lex, "j", "PRO:per")
    corriger_phono(lex, "l;NOM;", "El", old="l")
    fusionner_dans(lex, "l;NOM;l'", "l;NOM;") # ou "le;PRE;l'"
    corriger_apostrophe(lex, "l", "ART:def")
    corriger_apostrophe(lex, "l", "PRO:per")
    corriger_lemme(lex, "l';ART:def;", "le")
    corriger_lemme(lex, "l';PRO:per;", "le")
    corriger_phono(lex, "m;NOM;", "Em", old="m")
    corriger_apostrophe(lex, "m", "PRO:per")
    corriger_cgram(lex, "ne;NOM;n", "ADV", garder_gn=False) # OU:
    #lemmifier(lex, "ne;NOM;n"); corriger_phono(lex, "n;NOM;", "En", old="n")
    fusionner_dans(lex, "n;NOM;n'", "ne;ADV;n'", garder_gn=False) # ou dans "n;NOM;"
    corriger_ortho(lex, "ne;ADV;n", "n'")
    #fusionner_dans(lex, "avoir;AUX;n'", "ne;ADV;n'", garder_infover=False) # OU:
    fusionner_dans(lex, "avoir;AUX;n'", "avoir;AUX;a") # erreur d’OCR ?
    #fusionner_dans(lex, "n';PRO:per;", "ne;ADV;n'", garder_gn=False) # OU:
    fusionner_dans(lex, "n';PRO:per;", "me;PRO:per;m'", garder_gn=False) # erreur d’OCR ?
    corriger_cgram(lex, "o;;", "NOM")
    fusionner_dans(lex, "s;NOM;s'", "s;NOM;") # ou "se;PRO:per;s'"
    #fusionner_dans(lex, "avoir;AUX;s", "se;PRO:per;s'", garder_infover=False) # OU:
    fusionner_dans(lex, "avoir;AUX;s", "avoir;AUX;a") # erreur d’OCR ?
    corriger_phono(lex, "s;CON;", "s", old="Es") # PUIS:
    corriger_mot(lex, "s;CON;", "si;CON;s'")
    corriger_lemme(lex, "s';CON;", "si")
    corriger_phono(lex, "s;PRO:per;", "s", old="Es") # PUIS:
    corriger_apostrophe(lex, "s", "PRO:per")
    corriger_lemme(lex, "s';PRO:per;", "se")
    corriger_phono(lex, "t;NOM;", "te", old="t")
    fusionner_dans(lex, "t;NOM;t'", "t;NOM;") # ou "te;PRO:per;t'"
    corriger_apostrophe(lex, "t", "PRO:per")
    corriger_lemme(lex, "t';PRO:per;", "te")
    corriger_phono(lex, "y;NOM;", "igREk", old="i")
    supprimer(lex, "y;VER") # ??? erreur d’OCR pour "avoir;VER;a" ?
    supprimer(lex, "yu;ADV;y") # ??? ou fusionner dans "y;PRO:per;" ?

    # autres apostrophes manquantes (mots terminés par " d" ou "qu"):
    corriger_apostrophe(lex, "afin d", "CON")
    corriger_apostrophe(lex, "afin qu", "CON")
    corriger_apostrophe(lex, "est-ce qu", "ADV")
    corriger_apostrophe(lex, "jusqu", "PRE")
    corriger_apostrophe(lex, "lorsqu", "CON")
    corriger_apostrophe(lex, "parce qu", "CON")
    corriger_apostrophe(lex, "puisqu", "CON")
    corriger_apostrophe(lex, "qu", "CON")
    corriger_apostrophe(lex, "qu", "PRO:int")
    corriger_apostrophe(lex, "qu", "PRO:rel")
    corriger_apostrophe(lex, "tandis qu", "CON")

    # mots ADV dont le lemme est mal orthographié:
    lemmifier(lex, "r;ADV;re")
    lemmifier(lex, "y;NOM;yu")

    # mots ADV en double, un exemplaire ayant son lemme bien orthographié
    # et l’autre non:
    lemmifier(lex, "aujourd'huie;ADV;aujourd'hui")
    lemmifier(lex, "c'est-à-diree;ADV;c'est-à-dire")
    lemmifier(lex, "d'embléee;ADV;d'emblée")

    # mots en double, un exemplaire ayant son lemme mal orthographié
    # (avec un underscore) et l’autre étant classé sans cgram:
    corriger_underscore(lex, "ADV", "à brûle-pourpoint")
    corriger_underscore(lex, "ADV", "à cloche-pied")
    corriger_underscore(lex, "ADV", "à rebrousse-poil")
    corriger_underscore(lex, "ADV", "à tire-larigot")
    corriger_underscore(lex, "ADV", "à touche-touche")
    corriger_underscore(lex, "ADV", "à tue-tête")

    # autres lemmes sans cgram:
    corriger_cgram(lex, "team;;", "NOM")

    # lemmes mal classés dans ADJ:ind:
    corriger_cgram(lex, "tout-fait;ADJ:ind;", "ADJ") # ou ADV ou NOM
    # lemmes mal classés dans ADJ:num:
    fusionner_dans(lex, "autres;ADJ:num;", "autre;ADJ;autres") # ou ADV ou NOM ou PRO:ind
    fusionner_dans(lex, "centaines;ADJ:num;", "centaine;NOM;centaines")
    fusionner_dans(lex, "centenaires;ADJ:num;", "centenaire;NOM;centenaires") # ou ADJ
    fusionner_dans(lex, "centrales;ADJ:num;", "centrale;NOM;centrales") # ou ADJ
    corriger_cgram(lex, "midi;ADJ:num;", "NOM")
    corriger_cgram(lex, "ici;ADJ:num;", "ADV")
    # lemmes mal classés dans ART:def:
    supprimer(lex, "la-la-la;ART:def") # TODO: reclasser en ONO ou NOM ?
    # lemmes mal classés dans ADV:
    corriger_cgram(lex, "bon-cadeaux;ADV;bons-cadeaux", "NOM")
    corriger_cgram(lex, "sitcom;ADV;", "NOM")
    # lemmes mal classés dans CON:
    corriger_cgram(lex, "puissamment;CON;", "ADV")
    corriger_cgram(lex, "comment;CON;", "ADV")
    corriger_cgram(lex, "pourquoi;CON;", "ADV")
    corriger_cgram(lex, "néanmoins;CON;", "ADV")
    supprimer(lex, "ains;CON") # ???
    # lemmes mal classés dans PRE:
    corriger_cgram(lex, "aube;PRE;", "NOM")
    corriger_cgram(lex, "sous-prieur;PRE;", "NOM")
    corriger_cgram(lex, "bicause;PRE;", "CON")
    # lemmes mal classés dans PRO:ind:
    fusionner_dans(lex, "autrichiens;PRO:ind;", "autrichien;ADJ;autrichiens") # ou NOM
    # lemmes mal classés dans PRO:per:
    corriger_cgram(lex, "ou;PRO:per;", "CON", garder_gn=False)
    corriger_cgram(lex, "con;PRO:per;", "NOM") # ??? ou ADJ
    corriger_ortho(lex, "to;PRO:per;", "toi", garder_gn=False) # ???

    # lemmes PRO:pos mal orthographiés:
    corriger_ortho(lex, "notre;PRO:pos;", "nôtre") # ???
    corriger_ortho(lex, "votre;PRO:pos;", "vôtre") # ???

    # lemmes VER fallacieux:
    # ... TODO (on en détecte automatiquement)

    # verbes à l’impératif mal classés:
    # TODO les reclasser au lieu de les supprimer?
    # dans ce cas il faut aussi corriger leur genre, nombre, infover
    supprimer(lex, "tiens-la-moi;ADJ:pos")
    supprimer(lex, "faîtes-la-moi;NOM")
    supprimer(lex, "file-la-moi;NOM")
    supprimer(lex, "glisse-la-moi;NOM")
    supprimer(lex, "laisse-la-moi;NOM")
    supprimer(lex, "mets-la-toi;NOM")
    supprimer(lex, "montre-la-moi;NOM")
    supprimer(lex, "montre-la-moi;VER")
    supprimer(lex, "passe-la-moi;NOM")
    supprimer(lex, "pose-la-moi;NOM")
    supprimer(lex, "décrochez-moi-ça;NOM")

    # mots VER dont le lemme n’est pas l’infinitif:
    # (dans certains cas, ça semble lié au fait que le verbe soit défectif)
    #
    # NOTE: on a ainsi corrigé tous les infinitifs qui ne se terminent pas par
    # "er|ir|haïr|ouïr|ire|ndre|oudre|ourdre|ordre|perdre|ttre|ître|être|foutre|rompre|suivre|vivre|vaincre|clore|clure"
    corriger_lemme(lex, "adire;VER;adira", "adirer", garder_infover=False) # TODO: infover
    corriger_lemme(lex, "appert;VER;", "apparoir", garder_infover=False) # défectif; TODO: infover (ou ADJ, variante ortho de "apert")
    corriger_lemme(lex, "bouffi;VER;bouffis", "bouffir")
    corriger_lemme(lex, "bourrée;VER;bourrées", "bourrer")
    corriger_lemme(lex, "chaut;VER;", "chaloir", garder_infover=False) # défectif; TODO: infover
    corriger_lemme(lex, "choses;VER;", "choser")
    corriger_lemme(lex, "connais;VER;", "connaître", garder_infover=False)
    corriger_lemme(lex, "débraillé;VER;débraillés", "débrailler")
    corriger_lemme(lex, "découverte;VER;", "découvrir")
    corriger_lemme(lex, "découverte;VER;découvertes", "découvrir")
    corriger_lemme(lex, "florissait;VER;", "florir", garder_infover=False) # défectif; TODO: infover
    corriger_lemme(lex, "florissaient;VER;", "florir", garder_infover=False) # défectif; TODO: infover
    corriger_lemme(lex, "forfait;VER;", "forfaire", garder_infover=False) # défectif; TODO: genre, nombre, infover
    corriger_lemme(lex, "instruit;VER;instruites", "instruire")
    corriger_lemme(lex, "mentez;VER;", "mentir", garder_infover=False)
    corriger_lemme(lex, "nu;VER;nue", "nuer")
    corriger_lemme(lex, "parait;VER;", "parer", garder_infover=False)
    corriger_lemme(lex, "plus;VER;", "plaire")
    corriger_lemme(lex, "réélus;VER;", "réélire")
    corriger_lemme(lex, "sais;VER;", "savoir", garder_infover=False)
    corriger_lemme(lex, "sourdait;VER;", "sourdre", garder_infover=False) # défectif; TODO: infover
    corriger_lemme(lex, "sourdaient;VER;", "sourdre", garder_infover=False) # défectif; TODO: infover
    corriger_lemme(lex, "suradapté;VER;", "suradapter")

    # féminins pas liés à leur lemme masculin:
    corriger_lemme(lex, "akkadienne;ADJ;", "akkadien")
    corriger_lemme(lex, "nietzschéenne;NOM;", "nietzschéen")
    corriger_lemme(lex, "nietzschéenne;NOM;nietzschéennes", "nietzschéen")
    corriger_lemme(lex, "quakeresse;NOM;", "quaker")
    corriger_lemme(lex, "quasi-jungienne;NOM;", "quasi-jungien")
    corriger_lemme(lex, "quadruplée;NOM;quadruplées", "quadruplé")

    # pluriels pas liés à leur lemme singulier:
    corriger_lemme(lex, "in-folios;NOM;", "in-folio")
    corriger_lemme(lex, "in-octavos;NOM;", "in-octavo")
    corriger_lemme(lex, "scenarii;NOM;", "scénario")
    corriger_lemme(lex, "self-made-men;NOM;", "self-made-man")
    corriger_lemme(lex, "nurseries;NOM;", "nursery") # ou "nurserie"

    # autres erreurs de lemme:
    corriger_lemme(lex, "aurai;AUX;", "avoir", garder_infover=False)
    corriger_lemme(lex, "lunche;NOM;lunches", "lunch")
    corriger_lemme(lex, "matche;NOM;matches", "match")
    corriger_lemme(lex, "sandwiche;NOM;sandwiches", "sandwich")
    corriger_lemme(lex, "tig;NOM;tiges", "tige")
    lemmifier(lex, "plume;NOM;plum")
    corriger_lemme(lex, "vouler;VER;voulez", "vouloir", garder_infover=False)
    corriger_lemme(lex, "wagon-lits;NOM;", "wagon-lit") # variante ortho rare (singulier)
    lemmifier(lex, "jodhpur;NOM;jodhpurs") # n’existe qu’au pluriel et avec "s"

    # locutions avec "à": fautes d’orthographe:
    corriger_ortho(lex, "a l'instar;PRE;", "à l'instar") # "a" ~> "à"
    corriger_ortho(lex, "à jeun;ADV;a jeun", "à jeun") # "a" ~> "à"
    corriger_ortho(lex, "a posteriori;ADV;à posteriori", "à postériori") # "e" ~> "é"
    # marque la variante ortho francisée (1990) comme flexion de l’ortho tradi,
    # comme pour d’autres mots:
    corriger_lemme(lex, "à fortiori;ADV;", "a fortiori")
    corriger_lemme(lex, "à giorno;ADV;", "a giorno")

    # fautes d’orthographe:
    corriger_ortho(lex, "fettucine;NOM;", "fettuccine")
    corriger_ortho(lex, "schampooing;NOM;", "shampooing")
    corriger_mot(lex, "téléférique;NOM;téléfériques", "téléphérique;NOM;téléphériques")

    # divers:
    supprimer(lex, "re;ADV") # ???
    supprimer(lex, "ya;NOM") # ???
    supprimer(lex, "pa;ART:ind")
    supprimer(lex, "pa;NOM")
    supprimer(lex, "to;NOM") # ??? existe mais fréq très élevées vu la signification (bouillie traditionnelle du Mali)
    supprimer(lex, "il;ADV")
    supprimer(lex, "dig;ONO") # ou fusionner dans "ding;ONO;" ?
    supprimer(lex, "hai;ONO") # ???
    supprimer(lex, "skaal;ONO")
    fusionner_dans(lex, "eue;ADJ;eues", "avoir;VER;eues") # ou AUX
    fusionner_dans(lex, "court-circuits;NOM;", "court-circuit;NOM;courts-circuits", garder_gn=False) # indiqué à tort comme singulier
    fusionner_dans(lex, "court-circuits;ADJ;", "court-circuit;NOM;courts-circuits") # ou alors "quelqu’un qui court (courir) les circuits auto/moto" ???
    fusionner_dans(lex, "court-circuit;VER;court-circuits", "court-circuit;NOM;courts-circuits")
    fusionner_dans(lex, "rester;VER;regretter", "regretter;VER;regrettez")

    #
    # CORRECTIFS DE PRONONCIATION
    #

#
# français:
#

    corriger_phono(lex, "afin que;CON;afin qu'", "af5k", old="af5k8y") # [8y] en trop après [k]
    corriger_phono(lex, "agenouillement;NOM;", "aZ°nuj°m@", old="aZnuj°m@") # manque [°] après [Z]
    corriger_phono(lex, "anglo-russe;ADJ;", "@gloRys", old="@gloRus") # [u] ~> [y]
    corriger_phono(lex, "antiémeute;ADJ;", "@tiem2t", old="@jem2t") # [@j] ~> [@ti]
    corriger_phono(lex, "antirusse;ADJ;", "@tiRys", old="@tiR9s") # [9] ~> [y]
    corriger_phono(lex, "asocial;ADJ;asociales", "asosjal", old="azosjal") # [z] ~> [s] (préfixe a-)
    corriger_phono(lex, "autocritiquer;VER;", "otokRitike", old="otok°Ritike") # [°] en trop avant [R]
    corriger_phono(lex, "baisade;NOM;", "bEzad", old="b°zad") # [°] ~> [E]
    corriger_phono(lex, "barbe-de-capucin;NOM;", "baRbd°kapys5", old="baRbdkapys5") # manque [9] ou [°] après [d] (schwa obligatoire)
    corriger_phono(lex, "ber;NOM;", "bER", old="b9R") # [9] ~> [E]
    corriger_phono(lex, "bitter;VER;", "bite", old="bitER") # [ER] ~> [e]
    corriger_phono(lex, "brise-jet;NOM;", "bRizZE", old="bRizdZEt") # "jet" incorrectement prononcé à l’anglaise
    corriger_phono(lex, "burundais;NOM;", "buRundE", old="byRyndE") # les [y] devraient être [u]
    corriger_phono(lex, "cardinal-légat;NOM;", "kaRdinallega", old="kaRdinalega") # [l] ~> [ll] (pour découper correctement les syllabes de ce mot composé)
    corriger_phono(lex, "chum;NOM;", "tSOm", old="Sum") # [S] ~> [tS], [u] ~> [O]
    corriger_phono(lex, "command;NOM;", "kom@", old="komand") # [and] ~> [@] (mot français de droit)
    corriger_phono(lex, "confiturier;NOM;confiturière", "k§fityRjER", old="k§fituRjER") # [u] ~> [y]
    corriger_phono(lex, "consortium;NOM;consortiums", "k§sORsjOm", old="k§sORtijOm") # [tij] ~> [sj]
    corriger_phono(lex, "courtelinesque;ADJ;", "kuRt°linEsk", old="kuRtElinEsk") # [E] ~> [°]
    corriger_phono(lex, "curer;VER;curant", "kyR@", old="kyR@t") # [t] en trop à la fin
    corriger_phono(lex, "décati;ADJ;décatie", "dekati", old="dekasi") # [s] ~> [t]
    corriger_phono(lex, "décatir;VER;décatie", "dekati", old="dekasi") # [s] ~> [t]
    corriger_phono(lex, "desquamant;ADJ;desquamante", "deskwam@t", old="deskam@t") # [k] ~> [kw]
    corriger_phono(lex, "dessaper;VER;", "desape", old="dsape") # [e] manquant
    corriger_phono(lex, "désuétude;NOM;", "dez8etyd", old="des8etyd") # [s] ~> [z] (comme "désuet")
    corriger_phono(lex, "dexaméthasone;NOM;", "dEksametazOn", old="dEgzametazOn") # [gz] ~> [ks]
    corriger_phono(lex, "diplômer;VER;", "diplome", old="diplOme") # [O] ~> [o]
    corriger_phono(lex, "diplopie;NOM;", "diplopi", old="diplOpi") # [O] ~> [o]
    corriger_phono(lex, "dommages-intérêts;NOM;", "domaZ5teRE", old="domaZze5teRE") # prononcé à tort comme "dommages-z-ET-intérêts"
    corriger_phono(lex, "duc-d'albe;NOM;ducs-d'albe", "dykdalb", old="dykdalbe") # [e] en trop à la fin
    corriger_phono(lex, "écheveau;NOM;", "eS°vo", old="ES°vo") # [E] ~> [e]
    corriger_phono(lex, "échevin;NOM;", "eS°v5", old="ES°v5") # [E] ~> [e]
    corriger_phono(lex, "ecsta;ADV;", "Eksta", old="2ksta") # [2] ~> [E]
    corriger_phono(lex, "ectasie;NOM;", "Ektazi", old="2ktazi") # [2] ~> [E]
    corriger_phono(lex, "égailler;VER;", "egaje", old="egeje") # [ej] ~> [aj] (d’après TLFi)
    corriger_phono(lex, "égailler;VER;égaillent", "egaj", old="egEj") # [Ej] ~> [aj] (d’après TLFi)
    corriger_phono(lex, "égailler;VER;égaillèrent", "egajER", old="egejER") # [ej] ~> [aj] (d’après TLFi)
    corriger_phono(lex, "égailler;VER;égaillée", "egaje", old="egeje") # [ej] ~> [aj] (d’après TLFi)
    corriger_phono(lex, "égailler;VER;égaillés", "egaje", old="egeje") # [ej] ~> [aj] (d’après TLFi)
    corriger_phono(lex, "égailler;VER;égaillées", "egaje", old="egeje") # [ej] ~> [aj] (d’après TLFi)
    corriger_phono(lex, "enchtiber;VER;", "@Stibe", old="@tibe") # manque [S] après [@]
    corriger_phono(lex, "englander;VER;", "@gl@de", old="@gl@d9R") # [9R] ~> [e]
    corriger_phono(lex, "énième;ADJ;", "enjEm", old="EnjEm") # 1er [E] ~> [e]
    corriger_phono(lex, "énième;NOM;", "enjEm", old="EnjEm") # 1er [E] ~> [e]
    corriger_phono(lex, "épidémiologique;ADJ;", "epidemjoloZik", old="epidemjOlOgik") # [g] ~> [Z] (les [O] devraient être [o])
    corriger_phono(lex, "épiploon;NOM;", "epiplo§", old="epiplun") # [un] ~> [o§]
    corriger_phono(lex, "épiploon;NOM;épiploons", "epiplo§", old="epiplun") # [un] ~> [o§]
    corriger_phono(lex, "équin;ADJ;", "ek5", old="ekin") # [in] ~> [5]
    corriger_phono(lex, "esbaudir;VER;", "EsbodiR", old="EzbodiR") # [z] ~> [s]
    corriger_phono(lex, "esbroufeur;NOM;", "EsbRuf9R", old="EzbRuf9R") # [z] ~> [s] (comme les autres mots de la famille)
    corriger_phono(lex, "esbroufeur;NOM;esbroufeurs", "EsbRuf9R", old="EzbRuf9R") # [z] ~> [s] (comme les autres mots de la famille)
    corriger_phono(lex, "esseulement;NOM;", "Es9l°m@", old="s9l°m@") # manque [E] au début
    corriger_phono(lex, "éthologue;NOM;", "etolOg", old="EtOlOg") # [E] ~> [e], 1er [O] ~> [o]
    corriger_phono(lex, "étonnement;NOM;étonnements", "etOn°m@", old="EtOn°m@") # [E] ~> [e]
    corriger_phono(lex, "être-là;NOM;", "EtR°la", old="EtRla") # manque [°] (imprononçable sinon)
    corriger_phono(lex, "exo;NOM;", "Egzo", old="Ekso") # [ks] ~> [gz]
    corriger_phono(lex, "exo;NOM;exos", "Egzo", old="Ekso") # [ks] ~> [gz]
    corriger_phono(lex, "ex-secrétaire;NOM;", "Ekss°kRetER", old="Ekss°kREtER") # 2e [E] ~> [e]
    corriger_phono(lex, "faisan;NOM;", "f°z@", old="f2z@") # [2] ~> [°] (comme "faisane" et dérivés)
    corriger_phono(lex, "fidéliser;VER;", "fidelize", old="fidElize") # [E] ~> [e]
    corriger_phono(lex, "fielleux;ADJ;fielleuses", "fjEl2z", old="fjElyz") # [y] ~> [2]
    corriger_phono(lex, "fixette;NOM;", "fiksEt", old="figzEt") # [gz] ~> ks
    corriger_phono(lex, "forsythia;NOM;forsythias", "fORsisja", old="fORsitja") # [t] ~> [s]
    corriger_phono(lex, "gageure;NOM;gageures", "gaZyR", old="gaZ9R") # [9] ~> [y]
    corriger_phono(lex, "gatte;NOM;", "gat", old="gEt") # [E] ~> [a]
    corriger_phono(lex, "grand-oncle;NOM;grands-oncles", "gR@z§kl", old="gR@t§kl") # [t] ~> [z] (liaison)
    corriger_phono(lex, "groumer;VER;", "gRume", old="gRum9R") # [9R] ~> [e]
    corriger_phono(lex, "haut-commandement;NOM;", "okom@d°m@", old="okOm@nd°m@") # [n] en trop après [@]
    corriger_phono(lex, "hertzien;ADJ;hertziens", "ERtsj5", old="ERtzj5") # [z] ~> [s]
    corriger_phono(lex, "hostellerie;NOM;", "OstEl°Ri", old="OtEl°Ri") # manque [s] (ou alors [O] ~> [o])
    corriger_phono(lex, "hostellerie;NOM;hostelleries", "OstEl°Ri", old="OtEl°Ri") # manque [s] (ou alors [O] ~> [o])
    corriger_phono(lex, "hutter;VER;", "yte", old="yt9R") # [9R] ~> [e]
    corriger_phono(lex, "hyperespace;NOM;", "ipEREspas", old="ipERspas") # manque [E] après [R]
    corriger_phono(lex, "hyposulfite;NOM;", "iposylfit", old="ipozylfit") # [z] ~> [s]
    corriger_phono(lex, "intransgressible;ADJ;", "5tR@sgREsibl", old="5tR@zgREsibl") # [z] ~> [s] (comme les autres mots de la famille)
    corriger_phono(lex, "isthme;NOM;", "ism", old="izm") # [z] ~> [s]
    corriger_phono(lex, "isthme;NOM;isthmes", "ism", old="izm") # [z] ~> [s]
    corriger_phono(lex, "jodler;VER;", "jOdle", old="jOdl9R") # [9R] ~> [e]
    corriger_phono(lex, "kilbus;NOM;", "kilbys", old="kilbus") # [u] ~> [y]
    corriger_phono(lex, "lapis-lazuli;NOM;", "lapislazyli", old="lapizlazyli") # 1er [z] ~> [s]
    corriger_phono(lex, "lardeuss;NOM;", "laRd2s", old="laRdeus") # [eu] ~> [2]
    corriger_phono(lex, "lesbien;ADJ;", "lEsbj5", old="lEzbj5") # [z] ~> [s] (comme "lesbienne, lesbianisme")
    corriger_phono(lex, "lesbien;NOM;", "lEsbj5", old="lEzbj5") # [z] ~> [s] (comme "lesbienne, lesbianisme")
    corriger_phono(lex, "lithotripsie;NOM;", "litotRipsi", old="litotRipzi") # [z] ~> [s]
    corriger_phono(lex, "mi-gigolpince;NOM;", "miZigOlp5s", old="miZigolpis") # [o] ~> [O], dernier [i] ~> [5]
    corriger_phono(lex, "moellon;NOM;moellons", "mwal§", old="mwel§") # [we] ~> [wa] (comme lemme) ou à la rigueur [wE]
    corriger_phono(lex, "mucher;VER;muchas", "mySa", old="mutSas") # prononcé à tort comme un mot espagnol (ou alors classifié à tort comme une flexion du verbe français "mucher")
    corriger_phono(lex, "névralgie;NOM;névralgies", "nevRalZi", old="nEvRalZi") # [E] ~> [e] (comme lemme)
    corriger_phono(lex, "oenothera;NOM;", "enoteRa", old="2notRa") # manque [e] (et éventuellement [2] ~> [e]
    corriger_phono(lex, "oille;NOM;", "Oj", old="waj") # [waj] ~> [Oj]
    corriger_phono(lex, "oindre;VER;oignons", "waN§", old="ON§") # [O] ~> [wa] (verbe "oindre")
    corriger_phono(lex, "pacser;VER;pacsés", "pakse", old="pakze") # [z] ~> [s]
    corriger_phono(lex, "pacsif;NOM;", "paksif", old="pakif") # manque [s] après [k]
    corriger_phono(lex, "pacsif;NOM;pacsifs", "paksif", old="pakif") # manque [s] après [k]
    corriger_phono(lex, "paddocker;VER;", "padoke", old="padok9R") # [9R] ~> [e]
    corriger_phono(lex, "pagaye;NOM;", "pagEj", old="pagaj") # [aj] ~> [Ej] (variante de "pagaie")
    # en fait on a deux noms homographes non homophones:
    # - "pagaye" variante de "pagaie", prononcé [E(j)]
    # - "pagaye" variante de "pagaille", prononcé [aj] (plus rare)
    corriger_phono(lex, "pagayer;VER;pagaye", "pagEj", old="pagaj") # [aj] ~> [Ej] (verbe "pagayer")
    corriger_phono(lex, "pélasgique;ADJ;pélasgiques", "pelaZik", old="pelasZik") # [s] en trop
    corriger_phono(lex, "péquenaud;NOM;péquenaude", "pEk°nod", old="pek°nod") # [e] ~> [E] (comme lemme)
    corriger_phono(lex, "périurbain;ADJ;périurbaines", "peRiyRbEn", old="peRjoRbEn") # [jo] ~> [iy]
    corriger_phono(lex, "plein-emploi;NOM;", "plEn@plwa", old="pl5n@plwa") # [5] ~> [E]
    corriger_phono(lex, "polysaccharide;NOM;polysaccharides", "polisakaRid", old="polizakaRid") # [z] ~> [s]
    corriger_phono(lex, "polysyllabe;ADJ;", "polisilab", old="pOlisilab") # [O] ~> [o]
    corriger_phono(lex, "porte-foret;NOM;porte-forets", "pORt°foRE", old="pOR°tfoRE") # [°] mal placé
    corriger_phono(lex, "porte-tambour;NOM;", "pORt°t@buR", old="pORt@buR") # [t] ~> [t°t] (pour découper correctement les syllabes de ce mot composé)
    corriger_phono(lex, "présélection;NOM;présélections", "pReselEksj§", old="pRezElEksj§") # [z] ~> [s], [E] ~> [e]
    corriger_phono(lex, "présénile;ADJ;préséniles", "pResenil", old="pRezenil") # [z] ~> [s]
    corriger_phono(lex, "prophète;NOM;prophétesse", "pRofetEs", old="pRofEtEs") # [E] ~> [e]
    corriger_phono(lex, "quadrillion;NOM;", "kadRilj§", old="kadRij§") # [j] ~> [lj] (éventuellement [ka] ~> [kwa])
    corriger_phono(lex, "quadrumane;NOM;", "kwadRyman", old="kwadRuman") # [u] ~> [y]
    corriger_phono(lex, "quadruple;ADJ;", "kwadRypl", old="kwadRupl") # [u] ~> [y]
    corriger_phono(lex, "quadruple;NOM;", "kwadRypl", old="kwadRupl") # [u] ~> [y]
    corriger_phono(lex, "quadrupède;ADJ;", "kwadRypEd", old="kwadRupEd") # [u] ~> [y]
    corriger_phono(lex, "quadrupède;ADJ;quadrupèdes", "kwadRypEd", old="kwadRupEd") # [u] ~> [y]
    corriger_phono(lex, "quadrupède;NOM;", "kwadRypEd", old="kwadRupEd") # [u] ~> [y]
    corriger_phono(lex, "quadrupède;NOM;quadrupèdes", "kwadRypEd", old="kwadRupEd") # [u] ~> [y]
    corriger_phono(lex, "quadruple;NOM;quadruples", "kwadRypl", old="kwadRupl") # [u] ~> [y]
    corriger_phono(lex, "quadrupler;VER;quadrupla", "kwadRypla", old="kwadRupla") # [u] ~> [y]
    corriger_phono(lex, "quadrupler;VER;quadruplait", "kwadRyplE", old="kwadRuplE") # [u] ~> [y]
    corriger_phono(lex, "quadrupler;VER;quadruple", "kwadRypl", old="kwadRupl") # [u] ~> [y]
    corriger_phono(lex, "questeur;NOM;", "kEst9R", old="k8Est9R") # [8E] ~> [E]
    corriger_phono(lex, "quinquennat;NOM;", "k5kEna", old="k85kena") # [85] ~> [5], [e], ~> [E]
    corriger_phono(lex, "quintolet;NOM;", "k5tOlE", old="k85tOlE") # [85] ~> [5]
    corriger_phono(lex, "quinton;NOM;", "k5t§", old="k85t§") # [85] ~> [5]
    corriger_phono(lex, "quintupler;VER;quintuplé", "k5typle", old="k85typle") # [85] ~> [5] (comme apparentés)
    corriger_phono(lex, "râleux;NOM;", "Ral2", old="Ral2z") # [z] en trop après [2]
    corriger_phono(lex, "ralléger;VER;", "RaleZe", old="RalEZe") # [E] ~> [e]
    corriger_phono(lex, "ralléger;VER;rallégé", "RaleZe", old="RalEZe") # [E] ~> [e]
    corriger_phono(lex, "reparcourir;VER;", "R°paRkuRiR", old="R2paRkuRiR") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "reparcourir;VER;reparcouru", "R°paRkuRy", old="R2paRkuRy") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "repavage;NOM;", "R°pavaZ", old="R2pavaZ") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "repaver;VER;", "R°pave", old="R2pave") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "repêcher;VER;repêchant", "R°pES@", old="R2pES@") # [2] ~> [°] (comme autres flexions)
    corriger_phono(lex, "repéreur;NOM;", "R°peR9R", old="R2peR9R") # [2] ~> [°] (comme apparentés)
    corriger_phono(lex, "reposséder;VER;", "R°posede", old="R2pOsede") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "repoudrer;VER;repoudré", "R°pudRe", old="R2pudRe") # [2] ~> [°] (comme autres flexions)
    corriger_phono(lex, "reprogrammer;VER;", "R°pRogRame", old="R°pRogRam9R") # [9R] ~> [e]
    corriger_phono(lex, "reproposer;VER;", "R°pRopoze", old="R2pROpoze") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "republication;NOM;", "R°pyblikasj§", old="R2pyblikasj§") # [2] ~> [°] (comme apparentés)
    corriger_phono(lex, "resalir;VER;", "R°saliR", old="R2saliR")
    corriger_phono(lex, "resavater;VER;re-savaté", "R°savate", old="R°savat") # manque [e] à la fin
    corriger_phono(lex, "reservir;VER;", "R°sERviR", old="R2sERviR")
    corriger_phono(lex, "resocialisation;NOM;", "R°sosializasj§", old="R2sosializasj§") # [2] ~> [°] (comme autres préfixes re-)
    corriger_phono(lex, "resonger;VER;", "R°s§Ze", old="R°z§Ze") # [z] ~> [s] (préfixe re-)
    corriger_phono(lex, "resonger;VER;resongeait", "R°s§ZE", old="R°z§ZE") # [z] ~> [s] (préfixe re-)
    corriger_phono(lex, "ressauter;VER;ressauté", "R°sote", old="R2sote") # [2] ~> [°]
    corriger_phono(lex, "ressayer;VER;ressaie", "REsE", old="R°sE") # [°] ~> [E]
    corriger_phono(lex, "ressemer;VER;", "R°s°me", old="R2s°me") # [2] ~> [°]
    corriger_phono(lex, "ressui;NOM;", "REs8i", old="R°s8i") # [°] ~> [E]
    corriger_phono(lex, "ressuiement;NOM;", "REs8im@", old="R2s8im@") # [2] ~> [E]
    corriger_phono(lex, "ressuyer;VER;ressuyé", "REs8ije", old="R°s8ije") # [°] ~> [E]
    corriger_phono(lex, "ressuyer;VER;ressuyée", "REs8ije", old="R°s8ije") # [°] ~> [E]
    corriger_phono(lex, "réutilisation;NOM;", "Reytilizasj§", old="Reytilizaj§") # manque [s] après [a]
    corriger_phono(lex, "rosbif;NOM;rosbifs", "ROzbif", old="ROsbif") # [s] ~> [z] (comme le lemme)
    corriger_phono(lex, "rouspéter;VER;rouspétais", "RuspetE", old="RuspEtE") # [E] ~> [e]
    corriger_phono(lex, "subsaharien;ADJ;subsaharienne", "sybsaaRjEn", old="sybzaaRjEn") # [z] ~> [s]
    corriger_phono(lex, "subside;NOM;", "sybzid", old="sypzid") # [pz] ~> [bz] (comme "subsides") ou [ps]
    corriger_phono(lex, "surconsommer;VER;surconsomme", "syRk§sOm", old="syRkosOm") # [o] ~> [§]
    corriger_phono(lex, "surfait;ADJ;surfaites", "syRfEt", old="s9RfEt") # [9] ~> [y]
    corriger_phono(lex, "talisman;NOM;", "talism@", old="talisman") # [an] ~> [@]
    corriger_phono(lex, "talisman;NOM;talismans", "talism@", old="talisman") # [an] ~> [@]
    corriger_phono(lex, "tangerine;NOM;", "t@Z°Rin", old="t@n°Z2Rajn") # incorrectement prononcé à l’anglaise
    corriger_phono(lex, "taste-vin;NOM;", "tast°v5", old="tajst°v5") # [aj] ~> [a]
    corriger_phono(lex, "télétexte;NOM;", "teletEkst", old="teletEks°t") # [°] en trop après [ks]
    corriger_phono(lex, "templier;NOM;templiers", "t@plije", old="t@plje") # [j] ~> [ij]
    corriger_phono(lex, "templier;ADJ;templière", "t@plijER", old="t@pliER") # [i] ~> [ij]
    corriger_phono(lex, "tipper;VER;", "tipe", old="tip9R") # [9R] ~> [e]
    corriger_phono(lex, "toman;NOM;", "tom@", old="tOman") # [an] ~> [@] ([O] ~> [o])
    corriger_phono(lex, "tomodensitométrie;NOM;", "tomod@sitometRi", old="tomod@sitOmEtRi") # [O] ~> [o], [E] ~> [e]
    corriger_phono(lex, "transgresser;VER;transgressez", "tR@sgREse", old="tR@zgREse") # [z] ~> [s] (comme les autres mots de la famille)
    corriger_phono(lex, "turcoman;ADJ;", "tyRkom@", old="tyRkoman") # [an] ~> [@]
    corriger_phono(lex, "turcoman;NOM;", "tyRkom@", old="tyRkoman") # [an] ~> [@]
    corriger_phono(lex, "turcoman;NOM;turcomans", "tyRkom@", old="tyRkoman") # [an] ~> [@]
    corriger_phono(lex, "vénézuélien;ADJ;vénézuélienne", "venez8eljEn", old="venezyEljEn") # [yE] ~> [8e] (comme lemme)
    corriger_phono(lex, "xérographie;NOM;", "gzeRogRafi", old="kseROgRafi") # [ks] ~> [gz], [O] ~> [o]
    corriger_phono(lex, "xiphoïde;ADJ;", "gzifoid", old="ksifoid") # [ks] ~> [gz]
    corriger_phono(lex, "xylène;NOM;", "gzilEn", old="ksilEn") # [ks] ~> [gz]
    corriger_phono(lex, "xylographie;NOM;", "gzilogRafi", old="ksilOgRafi") # [ks] ~> [gz], [O] ~> [o]
    corriger_phono(lex, "zirconium;NOM;", "ziRkonjOm", old="ziRkonjom") # [o] à la fin ~> [O]
    corriger_phono(lex, "zyeuter;VER;zyeuté", "zj2te", old="zi2te") # [i] ~> [j] (comme autres flexions)

#
# anglais:
#

    corriger_phono(lex, "after-shave;NOM;", "aft9RSEjv", old="aft9RSav") # [a] ~> [Ej]
    corriger_phono(lex, "after-shave;NOM;after shave", "aft9RSEjv", old="aftERSav") # [a] ~> [Ej], [E] ~> [9]
    corriger_phono(lex, "all right;ADV;", "olRajt", old="alRajt") # [a] ~> [o]
    corriger_phono(lex, "awacs;NOM;", "awaks", old="awak") # [s] manquant à la fin (sigle)
    corriger_phono(lex, "ampli-tuner;NOM;", "@plityn9R", old="@plitjyn9R") # [jy] ~> [ju] ou [y]
    corriger_phono(lex, "backgammon;NOM;", "bakgamOn", old="bakgamon") # [o] ~> [O]
    corriger_phono(lex, "baseball;NOM;", "bEzbol", old="bezbol") # [e] ~> [E] (comme "base-ball")
    corriger_phono(lex, "basket-ball;NOM;", "baskEtbol", old="baskEtbOl") # [O] ~> [o]
    corriger_phono(lex, "beagle;NOM;beagles", "big9l", old="big9ls") # [s] en trop à la fin
    corriger_phono(lex, "birth control;NOM;", "b9Rsk§tRol", old="b9Rsk§tROl") # [O] ~> [o]
    corriger_phono(lex, "boat people;NOM;", "botpipOl", old="bOwtpip9l") # [Ow] ~> [o], [9] ~> [O]
    corriger_phono(lex, "body-building;NOM;", "bodibildiG", old="bOdibildiG") # [O] ~> [o]
    corriger_phono(lex, "bodybuilding;NOM;", "bodibildiG", old="bOdibildiG") # [O] ~> [o]
    corriger_phono(lex, "booster;NOM;", "bust9R", old="buste") # [e] ~> [9R]
    corriger_phono(lex, "borough;NOM;", "b9Ro", old="boRO") # meilleure approximation des voyelles
    corriger_phono(lex, "boxer;NOM;boxers", "bOksER", old="bOks9R") # [9] ~> [E] (comme lemme)
    corriger_phono(lex, "boxer-short;NOM;", "bOksERSORt", old="bOks9RSORt") # [9] ~> [E] (comme "boxer")
    corriger_phono(lex, "boy-friend;NOM;", "bOjfREnd", old="bOjfR5nd") # [5] ~> [E]
    corriger_phono(lex, "brown sugar;NOM;", "bRawnSug9R", old="bRawnSugaR") # [u] ~> [9] ?, [a] ~> [9]
    corriger_phono(lex, "bug;NOM;bugs", "b9g", old="b9gz") # [z] en trop à la fin
    #corriger_phono(lex, "cake;NOM;cakes", "kEjk", old="kEk") # [E] ~> [Ej] ? (pron. anglaise / francisation)
    #corriger_phono(lex, "cake;NOM;", "kEjk", old="kEk") # [E] ~> [Ej] ? (pron. anglaise / francisation)
    #corriger_phono(lex, "cheese-cake;NOM;", "tSizkEjk", old="tSizkEk") # [E] ~> [Ej] ? (pron. anglaise / francisation)
    #corriger_phono(lex, "cheese-cake;NOM;cheese-cakes", "tSizkEjk", old="tSizkEk") # [E] ~> [Ej] ? (pron. anglaise / francisation)
    corriger_phono(lex, "call-girl;NOM;", "kolg9Rl", old="kOlg9Rl") # [a] ~> [o]
    corriger_phono(lex, "call-girl;NOM;call girl", "kolg9Rl", old="kalg9Rl") # [a] ~> [o]
    corriger_phono(lex, "call-girl;NOM;call-girls", "kolg9Rl", old="kOlg9Rl") # [a] ~> [o]
    corriger_phono(lex, "call-girl;NOM;call girls", "kolg9Rl", old="kalg9Rl") # [a] ~> [o]
    corriger_phono(lex, "check-up;NOM;check up", "tSEkyp", old="SEkype") # [S] ~> [tS], [e] en trop à la fin
    corriger_phono(lex, "chopper;NOM;", "SOp9R", old="SOpER") # [E] ~> [9]
    corriger_phono(lex, "chop suey;NOM;", "SOpswEj", old="SOps8Ej") # [8] ~> [w]
    corriger_phono(lex, "clubiste;NOM;", "kl9bist", old="klybist") # [y] ~> [9]
    corriger_phono(lex, "coffee shop;NOM;", "kofiSOp", old="kOfiSOp") # 1er [O] ~> [o]
    corriger_phono(lex, "computer;NOM;computers", "k§pjut9R", old="k§pyt9R") # [y] ~> [ju]
    corriger_phono(lex, "cover-girl;NOM;", "kov9Rg9Rl", old="kOv9Rg9Rl") # [O] ~> [o]
    corriger_phono(lex, "cover-girl;NOM;cover-girls", "kov9Rg9Rl", old="kOv9Rg9Rl") # [O] ~> [o]
    corriger_phono(lex, "cow-boy;NOM;cow-boys", "kobOj", old="kobOjz") # [z] en trop à la fin
    corriger_phono(lex, "cracker;NOM;", "kRak9R", old="kRakER") # [E] ~> [9]
    corriger_phono(lex, "cutter;NOM;", "k9t9R", old="kytER") # [y] ~> [9], [E] ~> [9]
    corriger_phono(lex, "cutter;NOM;cutters", "k9t9R", old="kytER") # [y] ~> [9], [E] ~> [9]
    corriger_phono(lex, "cyberspace;NOM;", "sibERspEjs", old="sibERspes") # [e] ~> [Ej]
    corriger_phono(lex, "dealer;VER;", "dile", old="dil9R") # [9R] ~> [e]
    corriger_phono(lex, "duffel-coat;NOM;", "d9f9lkot", old="d9f9lkOwt") # [Ow] ~> [o]
    corriger_phono(lex, "duffle-coat;NOM;", "d9f9lkot", old="d9f9lkOwt") # [Ow] ~> [o]
    corriger_phono(lex, "duffle-coat;NOM;duffle-coats", "d9f9lkot", old="d9f9lkOwt") # [Ow] ~> [o]
    corriger_phono(lex, "email;NOM;emails", "imEjl", old="imEl") # [E] ~> [Ej] (comme "e-mail")
    corriger_phono(lex, "ex-dealer;NOM;", "Eksdil9R", old="EksdilER") # [E] ~> [9]
    corriger_phono(lex, "ferry-boat;NOM;", "fERibot", old="fERibOwt") # [OW] ~> [o]
    corriger_phono(lex, "fifties;NOM;", "fiftiz", old="fifti") # manque [z] à la fin
    corriger_phono(lex, "football;NOM;", "futbol", old="futbOl") # [O] ~> [o]
    corriger_phono(lex, "footballeur;NOM;", "futbol9R", old="futbOl9R") # [O] ~> [o]
    corriger_phono(lex, "footballeur;NOM;footballeurs", "futbol9R", old="futbal9R") # [a] ~> [o]
    corriger_phono(lex, "footballeur-vedette;NOM;", "futbol9Rv°dEt", old="futbOl9Rv°dEt") # [O] ~> [o]
    corriger_phono(lex, "footballistique;ADJ;", "futbolistik", old="futbalistik") # [a] ~> [o]
    corriger_phono(lex, "footballistique;ADJ;footballistiques", "futbolistik", old="futbalistik") # [a] ~> [o]
    corriger_phono(lex, "freak;NOM;", "fRik", old="fREk") # [E] ~> [i]
    corriger_phono(lex, "freak;NOM;freaks", "fRik", old="fREk") # [E] ~> [i]
    corriger_phono(lex, "fun;NOM;", "f9n", old="fyn") # [y] ~> [9]
    corriger_phono(lex, "gentleman-farmer;NOM;gentlemen-farmers", "ZEntl°mEnfaRm9R", old="ZEntl°manfaRm9R") # [Z] ~> [dZ] ?, [man] ~> [mEn]
    corriger_phono(lex, "gin-fizz;NOM;", "dZinfiz", old="dZinfidz") # dernier [dz] ~> [z]
    corriger_phono(lex, "gin-rummy;NOM;", "dZinR9mi", old="dZinRymi") # [u] ~> [9]
    corriger_phono(lex, "gipsy;NOM;", "dZipsi", old="Zipsi") # [Z] ~> [dZ]
    corriger_phono(lex, "girl friend;NOM;", "g9RlfREnd", old="g9RlfR5d") # [5] ~> [En]
    corriger_phono(lex, "hammerless;NOM;", "am9RlEs", old="amERlEs") # 1er [E] ~> [9]
    corriger_phono(lex, "has been;NOM;", "azbin", old="abij5n") # n’importe quoi
    corriger_phono(lex, "house;NOM;", "aws", old="awz") # [z] ~> [s]
    corriger_phono(lex, "house-boat;NOM;", "awsbot", old="awsbOwt") # [Ow] ~> [o]
    corriger_phono(lex, "house-boat;NOM;house-boats", "awsbot", old="awzbOwts") # [awz] ~> [aws], [Ow] ~> [o], dernier [s] en trop
    corriger_phono(lex, "house music;NOM;", "awsmjuzik", old="awzmjyzik") # [awz] ~> [aws], [y] ~> [u]
    corriger_phono(lex, "hovercraft;NOM;", "ov9RkRaft", old="Ov9RkRaft") # [O] ~> [o]
    corriger_phono(lex, "hurrah;ONO;", "uRa", old="yRa") # [y] ~> [u]
    corriger_phono(lex, "hurrah;NOM;", "uRa", old="yRa") # [y] ~> [u]
    corriger_phono(lex, "hurrah;NOM;hurrahs", "uRa", old="yRa") # [y] ~> [u]
    corriger_phono(lex, "hurricane;NOM;", "yRikan", old="9Rikan") # [9] ~> [y] (ou alors [a] ~> [Ej])
    corriger_phono(lex, "hurricane;NOM;hurricanes", "yRikan", old="9Rikan") # [9] ~> [y] (ou alors [a] ~> [Ej])
    corriger_phono(lex, "ice-cream;NOM;ice-creams", "ajskRim", old="ajskRimz") # dernier [z] en trop
    corriger_phono(lex, "insight;NOM;insights", "insajt", old="5sit") # n’importe quoi
    corriger_phono(lex, "interclubs;ADJ;", "5tERkl9b", old="5tERklyb") # [y] ~> [9]
    corriger_phono(lex, "irish coffee;NOM;", "ajRiSkofi", old="ajRiSkOfi") # [O] ~> [o]
    corriger_phono(lex, "jack;NOM;jacks", "dZak", old="Zak") # [Z] ~> [dZ] (comme lemme)
    corriger_phono(lex, "jacket;NOM;", "dZakEt", old="ZakEt") # [Z] ~> [dZ]
    corriger_phono(lex, "jacket;NOM;jackets", "dZakEt", old="ZakE") # [Z] ~> [dZ], [E] ~> [Et]
    corriger_phono(lex, "jackpot;NOM;", "dZakpOt", old="Zakpo") # [Z] ~> [dZ], [o] ~> [Ot]
    corriger_phono(lex, "jam-session;NOM;", "dZamsesj§", old="dZamsesj9n") # [9n] ~> [§]
    corriger_phono(lex, "jam-session;NOM;jam-sessions", "dZamsesj§", old="dZamsesj9n") # [9n] ~> [§]
    corriger_phono(lex, "jingle;NOM;jingles", "dZing9l", old="dZing9ls") # [s] en trop à la fin
    corriger_phono(lex, "joggeur;NOM;joggeurs", "dZOg9R", old="dZOgZ9R") # [gZ] ~> [g]
    corriger_phono(lex, "joker;NOM;", "dZokER", old="ZokER") # [Z] ~> [dZ]
    corriger_phono(lex, "joker;NOM;jokers", "dZokER", old="ZokER") # [Z] ~> [dZ]
    corriger_phono(lex, "junkie;NOM;", "dZ9nki", old="Z9nki") # [Z] ~> [dZ]
    corriger_phono(lex, "junkie;NOM;junkies", "dZ9nki", old="Z9nki") # [Z] ~> [dZ]
    corriger_phono(lex, "junky;NOM;", "dZ9nki", old="Zynki") # [Z] ~> [dZ], [yn] ~> [9n] ou [5]
    #corriger_phono(lex, "keepsake;NOM;", "kipsEjk", old="kipsEk") # [Ej] ~> [E] ? (pron. anglaise / francisation du XIXe s.)
    corriger_phono(lex, "line;NOM;", "lajn", old="lin") # [i] ~> [aj]
    corriger_phono(lex, "lock-outer;VER;lock-outés", "lOkawte", old="lokawte") # [o] ~> [O]
    corriger_phono(lex, "lord;NOM;lords", "lORd", old="lORdz") # [z] en trop à la fin
    corriger_phono(lex, "loser;NOM;losers", "luz9R", old="lozER")
    corriger_phono(lex, "luncher;VER;", "l9ntSe", old="l1Se") # prononcé à tort comme un mot français
    corriger_phono(lex, "made in;ADV;", "mEjdin", old="mejdin") # [e] ~> [E]
    corriger_phono(lex, "mail-coach;NOM;", "mEjlkotS", old="mEjlkOwtS") # [Ow] ~> [o]
    corriger_phono(lex, "management;NOM;", "manadZ°m@", old="manaZ°m@") # [Z] ~> [dZ] (comme "manager")
    corriger_phono(lex, "manager;NOM;managers", "manadZ9R", old="manadZER") # [E] ~> [9] (comme lemme)
    corriger_phono(lex, "manager;VER;manage", "manadZ", old="manaZ") # [Z] ~> [dZ] (comme lemme)
    corriger_phono(lex, "manager;VER;manageait", "manadZE", old="manaZE") # [Z] ~> [dZ] (comme lemme)
    corriger_phono(lex, "manager;VER;managerais", "manadZ°RE", old="manaZ°RE") # [Z] ~> [dZ] (comme lemme)
    corriger_phono(lex, "manageur;NOM;", "manadZ9R", old="manaZ9R") # [Z] ~> [dZ] (comme "manager")
    corriger_phono(lex, "medicine-ball;NOM;", "medisinbol", old="medisinbOl") # [O] ~> [o]
    corriger_phono(lex, "merchandising;NOM;", "mERtS@dajziG", old="mERtS@diziG") # 1er [i] ~> [aj]
    corriger_phono(lex, "milk-shake;NOM;", "milkSEk", old="milkSEjk") # [Ej] ~> [E] (francisation)
    corriger_phono(lex, "milk-shake;NOM;milk shake", "milkSEk", old="milkSEjk") # [Ej] ~> [E] (francisation)
    corriger_phono(lex, "milk-shake;NOM;milk-shakes", "milkSEk", old="milkSEjk") # [Ej] ~> [E] (francisation)
    corriger_phono(lex, "muffin;NOM;muffins", "m9fin", old="m9finz") # [z] en trop à la fin
    corriger_phono(lex, "overdose;NOM;", "ov9Rdoz", old="Ov9Rdoz") # [O] ~> [o]
    corriger_phono(lex, "overdose;NOM;overdoses", "ov9Rdoz", old="Ov9Rdoz") # [O] ~> [o]
    corriger_phono(lex, "overdrive;NOM;", "ov9RdRajv", old="Ov9RdRajv") # [O] ~> [o]
    corriger_phono(lex, "oxer;NOM;", "OksER", old="Okse") # [e] ~> [ER]
    corriger_phono(lex, "pancake;NOM;", "pankEk", old="p@kEk") # [@] ~> [an]
    corriger_phono(lex, "pancake;NOM;pancakes", "pankEk", old="p@kEk") # [@] ~> [an]
    corriger_phono(lex, "patchwork;NOM;", "patSwORk", old="patSw9Rk") # [9] ~> [O]
    corriger_phono(lex, "pattern;NOM;", "patERn", old="pat9Rn") # [9] ~> [E]
    corriger_phono(lex, "penthouse;NOM;", "pEntawz", old="p@tuz") # incorrectement prononcé à la française
    corriger_phono(lex, "people;ADJ;", "pipOl", old="pip9l") # [9] ~> [O]
    corriger_phono(lex, "people;NOM;", "pipOl", old="pip9l") # [9] ~> [O]
    corriger_phono(lex, "piercing;NOM;", "pERsiG", old="piRsiG") # 1er [i] ~> [E] ou à la rigueur [jE]
    corriger_phono(lex, "piercing;NOM;piercings", "pERsiG", old="piRsiG") # 1er [i] ~> [E] ou à la rigueur [jE]
    corriger_phono(lex, "pipeline;NOM;", "pajplajn", old="pajp°lajn") # [°] en trop
    corriger_phono(lex, "pipeline;NOM;pipelines", "pajplajn", old="pajp°lajn") # [°] en trop
    corriger_phono(lex, "pitchpin;NOM;", "pitSp5", old="pitSpin") # [in] ~> [5] (francisation)
    corriger_phono(lex, "playboy;NOM;", "plEbOj", old="plEjbwaj")
    corriger_phono(lex, "playmate;NOM;", "plEjmEt", old="plEjmat") # [a] ~> [E]
    corriger_phono(lex, "playmate;NOM;playmates", "plEjmEt", old="plEjmat") # [a] ~> [E]
    corriger_phono(lex, "plum;NOM;", "pl9m", old="plum") # [u] ~> [9]
    corriger_phono(lex, "plum-pudding;NOM;", "pl9mpudiG", old="plumpudiG") # 1er [u] ~> [9]
    corriger_phono(lex, "porter;NOM;", "pORt9R", old="pORtER") # [E] ~> [9]
    corriger_phono(lex, "prime time;NOM;", "pRajmtajm", old="pRimtajm") # [i] ~> [aj]
    corriger_phono(lex, "punching-ball;NOM;", "p9nSiGbol", old="p9nSiGbOl") # [O] ~> [o]
    corriger_phono(lex, "punching-ball;NOM;punching ball", "p9nSiGbol", old="p9nSiGbOl") # [O] ~> [o]
    corriger_phono(lex, "punt;NOM;", "p9nt", old="punt") # [u] ~> [9]
    corriger_phono(lex, "ranger;NOM;", "R@Z9R", old="R@ZER") # [E] ~> [9]
    corriger_phono(lex, "reset;NOM;", "R2sEt", old="R2zEt") # [z] ~> [s]
    corriger_phono(lex, "roadster;NOM;", "Rodst9R", old="ROwdst9R") # [Ow] ~> [o]
    corriger_phono(lex, "roast-beef;NOM;roast beef", "Rosbif", old="ROsbif") # [O] ~> [o] (comme "roast-beef")
    corriger_phono(lex, "rock'n'roll;NOM;", "ROkEnRol", old="ROkEnROl") # 2e [O] ~> [o]
    corriger_phono(lex, "rock-n-roll;NOM;rock-'n-roll", "ROkEnRol", old="ROkEnROl") # 2e [O] ~> [o]
    corriger_phono(lex, "rosbif;NOM;", "ROzbif", old="ROsbif") # [s] ~> [z]
    corriger_phono(lex, "running;NOM;", "R9niG", old="RyniG") # [y] ~> [9]
    corriger_phono(lex, "scenic railway;NOM;", "sinikREjlwEj", old="snikRajwEj") # n’importe quoi
    corriger_phono(lex, "schooner;NOM;schooners", "skun9R", old="Sun9R") # [S] ~> [sk] (comme le lemme et comme l’anglais), ou alors [9] ~> [E] (francisation)
    corriger_phono(lex, "scrub;NOM;", "skR9b", old="skROb") # [O] ~> [9]
    corriger_phono(lex, "setter;NOM;", "sEt9R", old="setER") # [e] ~> [E], [E] ~> [9]
    corriger_phono(lex, "setter;NOM;setters", "sEt9R", old="setER") # [e] ~> [E], [E] ~> [9]
    corriger_phono(lex, "seventies;NOM;", "sev9ntiz", old="s°v@ti") # prononcé à tort à la française
    corriger_phono(lex, "shirting;NOM;", "S9RtiG", old="SiRt5G") # [i] ~> [9], [5] ~> [i]
    corriger_phono(lex, "single;NOM;singles", "sing9l", old="s5gl"), # prononcé à tort à la française
    corriger_phono(lex, "skate;NOM;skates", "skEjt", old="skEjts") # [s] en trop à la fin
    corriger_phono(lex, "skipper;VER;", "skipe", old="skip9R") # [9R] ~> [e]
    corriger_phono(lex, "slice;NOM;", "slajs", old="slis") # [i] ~> [aj]
    corriger_phono(lex, "slush;NOM;", "slOS", old="sl9S") # [9] ~> [O] (Canada)
    corriger_phono(lex, "smoking;NOM;", "smokiG", old="smOkiG") # [O] ~> [o]
    corriger_phono(lex, "smoking;NOM;smokings", "smokiG", old="smOkiG") # [O] ~> [o]
    corriger_phono(lex, "snipe;NOM;", "snajp", old="snip") # [i] ~> [aj]
    corriger_phono(lex, "sniper;NOM;snipers", "snajp9R", old="snipER") # [i] ~> [aj], [E] ~> [9] (comme lemme)
    corriger_phono(lex, "soap-opéra;NOM;", "sopopeRa", old="sOwpopeRa") # [OW] ~> [o]
    corriger_phono(lex, "soap-opéra;NOM;soap opéra", "sopopeRa", old="sOwpopeRa") # [OW] ~> [o]
    corriger_phono(lex, "soccer;NOM;", "sok9R", old="sOs9R") # [o] ~> [O], [s] ~> [k]
    corriger_phono(lex, "softball;NOM;", "sOftbol", old="sOftbal") # [a] ~> [o]
    corriger_phono(lex, "soya;NOM;", "soja", old="swaja")
    # en fait on a deux homographes non homophones:
    # - "soya" variante de "soja", prononcé [soja]
    # - "soya" flexion du verbe "soyer", prononcé [swaja] (plus rare?)
    corriger_phono(lex, "spoiler;NOM;", "spOjl9R", old="spOjlER") # [E] ~> [9]
    corriger_phono(lex, "spray;NOM;", "spRE", old="spREj") # [j] en trop à la fin
    corriger_phono(lex, "spray;NOM;sprays", "spRE", old="spREj") # [j] en trop à la fin
    corriger_phono(lex, "sprinkler;NOM;sprinklers", "spRinkl9R", old="spRinklER") # [E] ~> [9] (comme son lemme singulier)
    corriger_phono(lex, "squatter;VER;", "skwate", old="skwat9R") # [9R] ~> [e]
    corriger_phono(lex, "squeezer;VER;", "skwize", old="skwiz9R") # [9R] ~> [e]
    corriger_phono(lex, "steamboat;NOM;", "stimbot", old="stimbOwt") # [Ow] ~> [o]
    corriger_phono(lex, "stetson;NOM;", "stEtsOn", old="stEts§") # [§] ~> [On]
    corriger_phono(lex, "stock-option;NOM;stock-options", "stOkopsj§", old="stOkOpS9n") # 2e [O] ~> [o], [S9n] ~> [sj§]
    corriger_phono(lex, "stripper;VER;", "stRipe", old="stRip9R") # [9R] ~> [e]
    corriger_phono(lex, "struggle for life;NOM;", "stR9g°lfORlajf", old="stR9g°lfoRlajf") # [o] ~> [O]
    corriger_phono(lex, "supertanker;NOM;", "sypERt@k9R", old="sypERt@kER") # [E] ~> [9]
    corriger_phono(lex, "supporter;NOM;", "sypORtER", old="sypoRt9R") # [o] ~> [O], [9] ~> [E]
    corriger_phono(lex, "supporter;NOM;supporters", "sypORtER", old="sypORt9R") # [9] ~> [E]
    corriger_phono(lex, "surfer;VER;surfait", "s9RfE", old="syRfE") # [y] ~> [9]
    corriger_phono(lex, "surfer;VER;surfant", "s9Rf@", old="syRf@") # [y] ~> [9]
    corriger_phono(lex, "surfer;VER;surfons", "s9Rf§", old="syRf§") # [y] ~> [9]
    corriger_phono(lex, "talkie;NOM;", "tolki", old="tOlki") # [O] ~> [o]
    corriger_phono(lex, "talkie;NOM;talkies", "tolki", old="tOlki") # [O] ~> [o]
    corriger_phono(lex, "talkie-walkie;NOM;", "tolkiwolki", old="tOlkiwOlki") # les [O] ~> [o]
    corriger_phono(lex, "talkie-walkie;NOM;talkies-walkies", "tolkiwolki", old="tOlkiwOlkiz") # les [O] ~> [o], [z] en trop à la fin
    corriger_phono(lex, "teenager;NOM;", "tinedZ9R", old="tinedz9R") # [z] ~> [Z]
    corriger_phono(lex, "tender;NOM;tenders", "t@d9R", old="t@dER") # [E] ~> [9] (comme lemme)
    corriger_phono(lex, "thriller;NOM;thrillers", "tRil9R", old="sRil9R") # [s] ~> [t] (comme lemme, bien que les 2 se disent)
    corriger_phono(lex, "toaster;NOM;", "tost9R", old="toaste") # prononcé à tort comme un mot français
    corriger_phono(lex, "toaster;VER;", "toste", old="toaste") # prononcé à tort comme un mot français
    corriger_phono(lex, "toffee;NOM;", "tofi", old="tOfi") # [O] ~> [o]
    corriger_phono(lex, "topless;ADJ;", "tOplEs",old="toplEs") # [o] ~> [O]
    corriger_phono(lex, "topless;NOM;", "tOplEs",old="toplEs") # [o] ~> [O]
    corriger_phono(lex, "trench-coat;NOM;", "tREnSkot", old="tREnSkOwt") # [Ow] ~> [o]
    corriger_phono(lex, "trench-coat;NOM;trench-coats", "tREnSkot", old="tREnSkOwt") # [Ow] ~> [o]
    corriger_phono(lex, "trusteeship;NOM;trusteeships", "tR9stiSip", old="tR9stiSips") # [s] en trop à la fin
    corriger_phono(lex, "twist;NOM;twists", "twist", old="twis") # manque [t] à la fin
    corriger_phono(lex, "vanity-case;NOM;", "vanitikEjz", old="vanitikEjs") # [s] ~> [z]
    corriger_phono(lex, "volley-ball;NOM;", "volEbol", old="volEbOl") # [O] ~> [o]
    corriger_phono(lex, "walkie-talkie;NOM;", "wolkitolki", old="wOlkitOlki") # les [O] ~> [o]
    corriger_phono(lex, "walkie-talkie;NOM;walkies-talkies", "wolkitolki", old="wOlkitOlkiz") # les [O] ~> [o], [z] en trop à la fin
    corriger_phono(lex, "wall street;NOM;", "wolstRit", old="wOlstRit") # [O] ~> [o]
    corriger_phono(lex, "woofer;NOM;woofers", "wuf9R", old="vuf9R") # [v] ~> [w] (comme lemme)
    corriger_phono(lex, "world music;NOM;", "wORldmjuzik", old="w9Rldmjuzik") # [9] ~> [O]
    corriger_phono(lex, "yacht;NOM;", "jot", old="jOt") # [O] ~> [o]
    corriger_phono(lex, "yacht;NOM;yachts", "jot", old="jOt") # [O] ~> [o]
    corriger_phono(lex, "yacht-club;NOM;", "jotkl9b", old="jOtkl9b") # [O] ~> [o]
    corriger_phono(lex, "yachting;NOM;", "jotiG", old="jOtiG") # [O] ~> [o]
    corriger_phono(lex, "yachtman;NOM;", "jotman", old="jOtman") # [O] ~> [o]
    corriger_phono(lex, "yachtman;NOM;yachtmen", "jotmEn", old="jOtmEn") # [O] ~> [o]
    corriger_phono(lex, "yachtwoman;NOM;", "jotwuman", old="jOtwuman") # [O] ~> [o]
    corriger_phono(lex, "yard;NOM;", "jaRd", old="jaR") # manque [d] à la fin
    corriger_phono(lex, "yard;NOM;yards", "jaRd", old="jaR") # manque [d] à la fin
    corriger_phono(lex, "yorkshire;NOM;", "jORkS9R", old="jORkSiR") # [i] ~> [9]

#
# allemand:
#

    corriger_phono(lex, "alpenstock;NOM;", "alp9nstOk", old="alp@stOk") # [@] ~> [9n]
    corriger_phono(lex, "ausweis;NOM;", "awsvajs", old="oswE"), # n’importe quoi
    corriger_phono(lex, "bitter;NOM;", "bit9R", old="bitER") # [E] ~> [9] (comme pluriel)
    corriger_phono(lex, "bunker;NOM;", "bunk9R", old="bunkER") # [E] ~> [9]
    corriger_phono(lex, "bunker;NOM;bunkers", "bunk9R", old="bunkER") # [E] ~> [9]
    corriger_phono(lex, "clausewitzien;ADJ;clausewitziens", "kloz°vitsj5", old="kloziwitsj5") # 1er [i] ~> [°], [w] ~> [v]
    corriger_phono(lex, "deutsche mark;ADJ;", "d2tSmaRk", old="dOjtSmaRk") # [Oj] ~> [2] (francisation du mot allemand)
    corriger_phono(lex, "doppler;NOM;", "dopl9R", old="dOplER") # [E] ~> [9]
    corriger_phono(lex, "einsteinien;ADJ;einsteinienne", "ajnStajnjEn", old="5stEnjEn") # prononciation allemande
    corriger_phono(lex, "freesia;NOM;", "fREzja", old="fRizja") # [i] ~> [E]
    corriger_phono(lex, "freesia;NOM;freesias", "fREzja", old="fRizja") # [i] ~> [E]
    corriger_phono(lex, "gretchen;NOM;", "gREtSEn", old="gR°tSEn") # [°] ~> [E]
    corriger_phono(lex, "gretchen;NOM;gretchens", "gREtSEn", old="gR°tSEn") # [°] ~> [E]
    corriger_phono(lex, "hand-ball;NOM;", "@dbal", old="@dbOl") # [O] ~> [a]
    corriger_phono(lex, "hégélianisme;NOM;", "egeljanizm", old="eZeljanizm") # [Z] ~> [g]
    corriger_phono(lex, "hégélien;ADJ;", "egelj5", old="eZelj5") # [Z] ~> [g]
    corriger_phono(lex, "hégélien;ADJ;hégélienne", "egeljEn", old="eZeljEn") # [Z] ~> [g]
    corriger_phono(lex, "hégélien;ADJ;hégéliens", "egelj5", old="eZelj5") # [Z] ~> [g]
    corriger_phono(lex, "hornblende;NOM;", "ORnbl5d", old="ORnbl5nd") # [5n] ~> [5]
    corriger_phono(lex, "johannisberg;NOM;", "ZoanisbERg", old="ZOanisb9Rg") # [O] ~> [o], [9] ~> [E]
    corriger_phono(lex, "junker;NOM;", "ZunkER", old="Z9nkER") # [9] ~> [u]
    corriger_phono(lex, "junker;NOM;junkers", "ZunkER", old="Z9nkER") # [9] ~> [u]
    corriger_phono(lex, "kommandantur;NOM;", "kom@d@tuR", old="kom@d@ntuR") # [n] en trop après [d@]
    corriger_phono(lex, "kreutzer;NOM;", "kR2tsER", old="kROjts9R") # francisation du mot allemand
    corriger_phono(lex, "muesli;NOM;", "mysli", old="m82sli") # [82] ~> [y]
    corriger_phono(lex, "munster;NOM;", "m5stER", old="mynst9R") # [yn] ~> [5], [9] ~> [E]
    corriger_phono(lex, "reichswehr;NOM;", "RajSsvER", old="RajSvER") # [S] ~> [Ss]
    corriger_phono(lex, "welter;NOM;", "vEltER", old="vElt9R") # [9] ~> [E]
    corriger_phono(lex, "welter;NOM;welters", "vEltER", old="wEltER") # [w] ~> [v] (comme le lemme)
    corriger_phono(lex, "westphalien;ADJ;", "vEstfalj5", old="vEstpalj5") # [p] ~> [f]
    corriger_phono(lex, "westphalien;ADJ;westphaliennes", "vEstfaljEn", old="vEstpaljEn") # [p] ~> [f]
    corriger_phono(lex, "witz;NOM;", "vits", old="witz") # [w] ~> [v], [z] ~> [s]
    corriger_phono(lex, "wurtembergeois;ADJ;", "vyRt@bERZwa", old="vuRtEmbERZwa") # [u] ~> [y], [Em] ~> [@]
    corriger_phono(lex, "wurtembergeois;ADJ;wurtembergeoise", "vyRt@bERZwaz", old="vuRtEmbERZwaz") # [u] ~> [y], [Em] ~> [@]
    corriger_phono(lex, "wurtembergeois;NOM;", "vyRt@bERZwa", old="vuRtEmbERZwa") # [u] ~> [y], [Em] ~> [@]

#
# néerlandais:
#

    corriger_phono(lex, "afrikaner;NOM;afrikaners", "afRikan9R", old="afRikanER") # [E] ~> [9]
    corriger_phono(lex, "bintje;NOM;", "bintZe", old="bintS") # [S] ~> [ke] (prononciation néerlandaise) ou [Ze] (francisation)
    corriger_phono(lex, "boer;NOM;",      "buR", old="b9R") # [9] ~> [u]
    corriger_phono(lex, "boer;NOM;boers", "buR", old="beR") # [e] ~> [u]
    corriger_phono(lex, "boskoop;NOM;", "bOskOp", old="bOskup") # [u] ~> [O]

#
# suédois, danois, norvégien:
#

    corriger_phono(lex, "smorrebrod;NOM;", "sm2R°bR2d", old="smoR°bRod") # les [o] ~> [2]

#
# hébreu:
#

    corriger_phono(lex, "bar-mitsva;NOM;", "baRmitsva", old="baRmisva") # [s] ~> [ts]
    corriger_phono(lex, "kacher;ADJ;", "kaSER", old="kaSe") # [e] ~> [ER]

#
# portugais:
#
    corriger_phono(lex, "cruzeiro;NOM;cruzeiros", "kRuzERo", old="kRuzEROs") # [O] ~> [o], [s] en trop à la fin
    corriger_phono(lex, "sertao;NOM;", "sERt@w", old="sERtaw") # [aw] ~> [@w]
    corriger_phono(lex, "sertão;NOM;", "sERt@w", old="sERtaw") # [aw] ~> [@w]

#
# espagnol:
#

    corriger_phono(lex, "angustura;NOM;", "@gustuRa", old="@gystyRa") # les [y] ~> [u]
    corriger_phono(lex, "azulejo;NOM;azulejos", "azulexos", old="azulexOs") # [O] ~> [o]
    corriger_phono(lex, "buen retiro;NOM;", "bwEnRetiRo", old="b8EnR2tiRo") # [8] ~> [w], [2] ~> [e]
    corriger_phono(lex, "caballero;NOM;", "kabaljeRo", old="kabajeRo") # [j] ~> [lj]
    corriger_phono(lex, "chiricahua;ADJ;", "tSiRikawa", old="SiRikaua") # [S] ~> [tS], [u] ~> [w]
    corriger_phono(lex, "chiricahua;ADJ;chiricahuas", "tSiRikawa", old="SiRikaua") # [S] ~> [tS], [u] ~> [w]
    corriger_phono(lex, "chiricahua;NOM;", "tSiRikawa", old="SiRikaua") # [S] ~> [tS], [u] ~> [w]
    corriger_phono(lex, "chiricahua;NOM;chiricahuas", "tSiRikawa", old="SiRikaua") # [S] ~> [tS], [u] ~> [w]
    corriger_phono(lex, "churrigueresque;ADJ;", "tSuRig°REsk", old="SyRig°REsk") # [S] ~> [tS], [y] ~> u
    corriger_phono(lex, "churro;NOM;churros", "tSuRo", old="SyRo") # [S] ~> [tS], [y] ~> u
    corriger_phono(lex, "cueva;NOM;", "kweva", old="kueva") # [u] ~> [w]
    corriger_phono(lex, "cueva;NOM;cuevas", "kweva", old="kuevas") # [u] ~> [w]
    corriger_phono(lex, "faena;NOM;", "faena", old="fena") # [e] ~> [ae] (espagnol et non latin)
    corriger_phono(lex, "gringo;NOM;gringos", "gRiGos", old="gR5gos") # [5g] ~> [iG] (comme lemme)
    corriger_phono(lex, "habanera;NOM;", "abaneRa", old="abanERa") # [E] ~> [e]
    corriger_phono(lex, "llano;NOM;llanos", "ljanos", old="lanos") # [l] ~> [lj]
    corriger_phono(lex, "macumba;NOM;", "makumba", old="makymba") # [y] ~> [u]
    corriger_phono(lex, "madre de dios;NOM;", "madRededjos", old="madRededeioEs") # n’importe quoi
    corriger_phono(lex, "milonga;NOM;", "mil§ga", old="miloga") # [o] ~> [§]

#
# italien:
#

    corriger_phono(lex, "aggiornamento;NOM;", "adZjORnamEnto", old="aZjORnamEnto") # [Z] ~> [dZ]
    corriger_phono(lex, "gusto;ADV;", "gusto", old="gysto") # [y] ~> [u]
    corriger_phono(lex, "jacuzzi;NOM;", "Zakuzi", old="Zakyzi") # [y] ~> [u]
    corriger_phono(lex, "jacuzzi;NOM;jacuzzis", "Zakuzi", old="Zakyzi") # [y] ~> [u]
    corriger_phono(lex, "messer;NOM;", "mesER", old="mes9R") # [9] ~> [E]
    corriger_phono(lex, "scénario;NOM;scenarii", "senaRi", old="s°naRi") # [°] ~> [e]
    corriger_phono(lex, "tempo;NOM;tempi", "tEmpi", old="t@pi") # [@] ~> [Em]
    corriger_phono(lex, "tutti frutti;ADV;", "tutifRuti", old="tytifRyti") # les [y] devraient être [u]
    corriger_phono(lex, "tutti quanti;ADV;", "tutikwanti", old="tytikwanti") # [y] ~> [u]
    corriger_phono(lex, "zani;NOM;", "dzani", old="zani") # [z] ~> [dz]

#
# latin:
#

    corriger_phono(lex, "ad nutum;ADV;", "adnytOm", old="adnutOm") # [u] ~> [y]
    corriger_phono(lex, "conjungo;NOM;",          "k§Z§Go", old="k§j§Go") # [j] ~> [Z] (pron. francisée)
    corriger_phono(lex, "conjungo;NOM;conjungos", "k§Z§Go", old="k§j§Go") # [j] ~> [Z] (pron. francisée)
    corriger_phono(lex, "ecce homo;ADV;", "Ekseomo", old="EtSeomo") # [tS] ~> [ks] (latin et non italien)
    corriger_phono(lex, "ex nihilo;ADV;", "Eksniilo", old="Eksnajilo") # n’importe quoi
    corriger_phono(lex, "in absentia;ADV;", "inabsEnsja", old="inabsEntja") # [tj] ~> [sj]
    corriger_phono(lex, "in extenso;ADV;", "inEkstEnso", old="inEkst5so") # [5] ~> [En]
    corriger_phono(lex, "in memoriam;ADV;", "inmemoRjam", old="inm°moRjam") # [°] ~> [e]
    corriger_phono(lex, "in pace;NOM;", "inpase", old="inpas") # manque [e] après [s]
    corriger_phono(lex, "minus habens;NOM;", "minysab5s", old="minyszabEns") # [z] en trop, [En] ~> [5]
    corriger_phono(lex, "nihil obstat;ADV;", "niilOpstat", old="niilOpsta") # manque [t] à la fin
    corriger_phono(lex, "numerus clausus;NOM;", "nymeRysklozys", old="nym°Rysklozys") # [°] ~> [e]
    corriger_phono(lex, "post mortem;ADV;", "pOstmORtEm", old="pOst°mORtEm") # [°] en trop après [st]
    corriger_phono(lex, "presbyterium;NOM;", "pREsbiteRjOm", old="pREzbit°RjOm") # [z] ~> [s], [°] ~> [e]
    corriger_phono(lex, "requiescat in pace;NOM;", "Rekwieskatinpase", old="R°kijEskainpas") # n’importe quoi
    corriger_phono(lex, "sui generis;ADJ;", "s8iZeneRis", old="Es8iZ°n°Ri") # n’importe quoi
    corriger_phono(lex, "summum;NOM;", "sOmOm", old="s9mOm") # [9] ~> [O]
    corriger_phono(lex, "velux;NOM;", "velyks", old="v2luks") # [2] ~> [e], [u] ~> [y]

#
# arabe:
#

    corriger_phono(lex, "ayan;NOM;ayans", "aj@", old="Ej@") # [E] ~> [a]
    corriger_phono(lex, "fedayin;NOM;", "fedajin", old="fedaj5") # [5] ~> [in]
    corriger_phono(lex, "kandjar;NOM;", "k@dZaR", old="k@djaR") # [j] ~> [Z]
    corriger_phono(lex, "leben;NOM;", "lebEn", old="l°b@") # [°] ~> [e], [@] ~> [En]
    corriger_phono(lex, "rambla;NOM;", "R@bla", old="Rambla") # [am] ~> [@] ou à la rigueur [@m]
    corriger_phono(lex, "willaya;NOM;", "vilaja", old="wilEja") # [w] ~> [v], [E] ~> [a]
    corriger_phono(lex, "willaya;NOM;willayas", "vilaja", old="wilEja") # [w] ~> [v], [E] ~> [a]

#
# turc:
#

    corriger_phono(lex, "kan;NOM;", "k@", old="kan") # [an] ~> [@]
    corriger_phono(lex, "khan;NOM;", "k@", old="kan") # [an] ~> [@]
    corriger_phono(lex, "khan;NOM;khans", "k@", old="kan") # [an] ~> [@]
    corriger_phono(lex, "ney;NOM;", "nE", old="ne") # [e] ~> [E] ou [Ej] (comme "nay")
    corriger_phono(lex, "sandjak;NOM;", "s@dZak", old="s@djak") # [j] ~> [Z]

#
# russe, ukrainien:
#

    corriger_phono(lex, "intelligentsia;NOM;", "inteligEntsja", old="5teliZEnsja") # [5] ~> [in], [Z] ~> [g], manque [t] avant [sj] (mot russe)
    corriger_phono(lex, "isba;NOM;isbas", "izba", old="isba") # [s] ~> [z] (comme lemme)
    corriger_phono(lex, "kalachnikov;NOM;kalachnikovs", "kalaSnikOf", old="kalaknikOv") # 2e [k] ~> [S], [v] ~> [f]

#
# japonais:
#

    corriger_phono(lex, "bunraku;NOM;", "bunRaku", old="b1Raky") # prononcé à tort comme un mot français
    corriger_phono(lex, "haïkaï;NOM;", "aikaj", old="aikai") # [i] ~> [j]
    corriger_phono(lex, "haïku;NOM;", "aiku", old="aiky") # [y] ~> [u]
    corriger_phono(lex, "haïku;NOM;haïkus", "aiku", old="aiky") # [y] ~> [u]
    corriger_phono(lex, "keiretsu;NOM;", "kEjREtsu", old="keREtsu") # [e] ~> [Ej]
    corriger_phono(lex, "sepuku;NOM;", "sepuku", old="s2puku") # [2] ~> [e]
    corriger_phono(lex, "shamisen;NOM;", "SamizEn", old="Samiz°n") # [°] ~> [E]

#
# autres langues:
#

    corriger_phono(lex, "asana;NOM;", "asana", old="azana") # [z] ~> [s]
    corriger_phono(lex, "banyan;NOM;", "banj@", old="banjan") # [an] ~> [@]
    corriger_phono(lex, "bodhisattva;NOM;", "bodisatva", old="bodizatva") # [z] ~> [s]
    corriger_phono(lex, "bécabunga;NOM;", "bekab§Ga", old="bekabuGa") # [uG] ~> [§G]
    corriger_phono(lex, "epsilon;NOM;", "EpsilOn", old="Epsil§") # [§] ~> [On]
    corriger_phono(lex, "han;ADJ;", "an", old="@") # [@] ~> [an]
    corriger_phono(lex, "han;NOM;", "an", old="@") # [@] ~> [an]
    corriger_phono(lex, "kevlar;NOM;", "kEvlaR", old="k°vlaR") # [°] ~> [E]
    corriger_phono(lex, "kula;NOM;", "kula", old="kyla") # [y] ~> [u]
    corriger_phono(lex, "luffa;NOM;", "lufa", old="l9fa") # [9] ~> [u]
    corriger_phono(lex, "marae;NOM;", "maRae", old="maRe") # [e] ~> [ae] (mot polynésien)
    corriger_phono(lex, "menhaden;NOM;", "mEnadEn", old="mEnad°n") # [°] ~> [E]
    corriger_phono(lex, "mooré;NOM;", "moRe", old="mORe") # [O] ~> [o]
    corriger_phono(lex, "pschent;NOM;", "pskEnt", old="psSEnt") # [S] ~> [k]
    corriger_phono(lex, "quechua;NOM;", "ketSwa", old="keSua") # [u] ~> [w] ([S] pourrait être [tS])
    corriger_phono(lex, "schproum;NOM;", "SpRum", old="spRum") # [s] ~> [S]
    corriger_phono(lex, "tomahawk;NOM;", "tomaok", old="tomaOk") # [O] ~> [o]
    corriger_phono(lex, "tomahawk;NOM;tomahawks", "tomaok", old="tomaOk") # [O] ~> [o]
    corriger_phono(lex, "tokay;NOM;", "tokaj", old="tokE") # [E] ~> [aj]
    corriger_phono(lex, "tsuica;NOM;", "tsujka", old="ts8ika") # [8i] ~> [uj]
    corriger_phono(lex, "ylang-ylang;NOM;", "il@gil@g", old="il@il@") # les [@] ~> [@g]
    corriger_phono(lex, "ziggourat;NOM;ziggourats", "ziguRat", old="ziguRa") # manque [t] à la fin (les 2 phon. sont possibles, mais on aligne sur le lemme "ziggourat")
    corriger_phono(lex, "zingaro;NOM;", "dzingaRo", old="zingaRo") # [z] ~> [dz]
    corriger_phono(lex, "zydeco;NOM;", "zideko", old="zid2ko") # [2] ~> [e]
    corriger_phono(lex, "yu;NOM;", "ju", old="jy") # [y] ~> [u]
    corriger_phono(lex, "yue;NOM;", "jue", old="jy") # [y] ~> [ue]

# prononciation du "x" dans "dix-sept, dix-huit, dix-neuf":

    #corriger_phono(lex, "dix-neuf;ADJ:num;", old="dizn9f") # déjà correct
    #corriger_phono(lex, "dix-neuvième;ADJ;", old="dizn9vjEm") # déjà correct
    #corriger_phono(lex, "dix-neuvième;NOM;", old="dizn9vjEm") # déjà correct
    #corriger_phono(lex, "quatre-vingt-dix-neuf;ADJ:num;", old="katR°v5dizn9f") # déjà correct
    #corriger_phono(lex, "soixante-dix-neuf;ADJ:num;", old="swas@tdizn9f") # déjà correct
    corriger_phono(lex, "soixante-dix-neuvième;ADJ;", "swas@tdizn9vjEm", old="swas@tdisn9vjEm") # [sn] ~> [zn]

# [°] final:

    # supprime [°] à la fin (comme partout ailleurs dans la base):
    corriger_phono(lex, "appui-tête;NOM;", "ap8itEt", old="ap8itEt°")
    corriger_phono(lex, "ardre;VER;", "aRdR", old="aRdR°")
    corriger_phono(lex, "blanc;ADJ;blanche", "bl@S", old="bl@S°")
    corriger_phono(lex, "blanche;NOM;", "bl@S", old="bl@S°")
    corriger_phono(lex, "botulisme;NOM;", "botylizm", old="botylizm°")
    corriger_phono(lex, "boudeur;ADJ;boudeuse", "bud2z", old="bud2z°")
    corriger_phono(lex, "boudeur;NOM;boudeuse", "bud2z", old="bud2z°")
    corriger_phono(lex, "érable;NOM;", "eRabl", old="eRabl°")
    corriger_phono(lex, "probable;ADJ;probables", "pRobabl", old="pRobabl°")
    corriger_phono(lex, "probable;ADJ;", "pRobabl", old="pRobabl°")
    corriger_phono(lex, "sourdre;VER;", "suRdR", old="suRdR°")

    # pour ce mot il s’agit d’une vraie erreur:
    corriger_phono(lex, "est-ce que;ADV;est-ce qu'", "Es°k", old="Es°k°")

    # discutable pour les mots suivants:
    #corriger_phono(lex, "lorsque;ADV;", "lORsk", old="lORsk°")
    #corriger_phono(lex, "lorsque;CON;", "lORsk", old="lORsk°")
    #corriger_phono(lex, "puisque;ADV;", "p8isk", old="p8isk°")
    #corriger_phono(lex, "puisque;CON;", "p8isk", old="p8isk°")
    #corriger_phono(lex, "quoique;ADV;", "kwak", old="kwak°")
    #corriger_phono(lex, "quoique;CON;", "kwak", old="kwak°")

# autres schwas:

    # on réserve le graphème "e":[9] pour les anglicismes,
    # on préfère "e":[°] pour les mots bien français
    # (car c’est déjà le cas presque partout dans la base):
    corriger_phono(lex, "entreprendre;VER;entreprenez", "@tR°pR°ne", old="@tR°pR9ne") # [9] ~> [°] (comme mots similaires)
    corriger_phono(lex, "entreprendre;VER;entreprenons", "@tR°pR°n§", old="@tR°pR9n§") # [9] ~> [°] (comme mots similaires)

# [G]: devient [g] si le "n" est nasalisé, devient [Gg] avant une voyelle:

    corriger_phono(lex, "bécabunga;NOM;", "bekab§ga", old="bekab§Ga") # [§G] ~> [§g]
    corriger_phono(lex, "bingo;NOM;", "biGgo", old="biGo") # [iG] ~> [iGg]
    corriger_phono(lex, "conjungo;NOM;",          "k§Z§go", old="k§Z§Go") # [§G] ~> [§g]
    corriger_phono(lex, "conjungo;NOM;conjungos", "k§Z§go", old="k§Z§Go") # [§G] ~> [§g]
    corriger_phono(lex, "ginseng;NOM;", "Zins@g", old="Zins@G") # [@G] ~> [@g]
    corriger_phono(lex, "gringo;NOM;",        "gRiGgo", old="gRiGo") # [iG] ~> [iGg]
    #corriger_phono(lex, "gringo;NOM;gringos", "gRiGgos", old="gR5gos") # [5g] ~> [iGg]
    corriger_phono(lex, "gringo;NOM;gringos", "gRiGgos", old="gRiGos") # [iG] ~> [iGg]
    corriger_phono(lex, "jungien;ADJ;",         "juGgj5", old="juGj5") # [uG] ~> [uGg]
    corriger_phono(lex, "jungien;ADJ;jungiens", "juGgj5", old="juGj5") # [uG] ~> [uGg]
    corriger_phono(lex, "jungien;NOM;",         "juGgj5", old="juGj5") # [uG] ~> [uGg]
    corriger_phono(lex, "jungien;NOM;jungiens", "juGgj5", old="juGj5") # [uG] ~> [uGg]
    corriger_phono(lex, "quasi-jungien;NOM;quasi-jungienne", "kazijuGgjEn", old="kazijuGjEn") # [uG] ~> [uGg]
    corriger_phono(lex, "ma-jong;NOM;", "maZ§g", old="maZ§G") # [§G] ~> [§g]
    corriger_phono(lex, "mah-jong;NOM;", "maZ§g", old="maZ§G") # [§G] ~> [§g]
    corriger_phono(lex, "mungo;NOM;", "muGgo", old="muGo") # [uG] ~> [uGg]
    corriger_phono(lex, "ping-pong;NOM;", "piGp§g", old="piGp§G") # [§G] ~> [§g]
    corriger_phono(lex, "sampang;NOM;", "s@p@g", old="s@p@G") # [@G] ~> [@g]
    corriger_phono(lex, "sampang;NOM;sampangs", "s@p@g", old="s@p@G") # [@G] ~> [@g]
    corriger_phono(lex, "swinguer;VER;swinguaient", "swiGgE", old="swiGE") # [iG] ~> [iGg]
    corriger_phono(lex, "toungouse;ADJ;", "tuGguz", old="tuGuz") # [uG] ~> [uGg]

# [j] redoublé [jj] dans les conjugaisons "-yions, -illions, -yiez, -illiez":

    corriger_phono(lex, "ailler;VER;aillions", "ajj§", old="aj§")
    corriger_phono(lex, "appareiller;VER;appareillions", "apaREjj§", old="apaREj§")
    corriger_phono(lex, "biller;VER;billions", "bijj§", old="bilj§") # corrige aussi une vraie erreur: [l] en trop
    corriger_phono(lex, "briller;VER;brillions", "bRijj§", old="bRij§")
    corriger_phono(lex, "chamailler;VER;chamaillions", "Samajj§", old="Samaj§")
    corriger_phono(lex, "détailler;VER;détaillions", "detajj§", old="detaj§")
    corriger_phono(lex, "ennuyer;VER;ennuyions", "@n8ijj§", old="@n8ij§")
    corriger_phono(lex, "essuyer;VER;essuyions", "Es8ijj§", old="Es8ij§")
    corriger_phono(lex, "extraire;VER;extrayions", "EkstREjj§", old="EkstREj§")
    corriger_phono(lex, "foudroyer;VER;foudroyions", "fudRwajj§", old="fudRwaj§")
    corriger_phono(lex, "fuir;VER;fuyions", "f8ijj§", old="f8ij§")
    corriger_phono(lex, "fusiller;VER;fusillions", "fyzijj§", old="fyzij§")
    corriger_phono(lex, "patrouiller;VER;patrouillions", "patRujj§", old="patRuj§")
    corriger_phono(lex, "renvoyer;VER;renvoyions", "R@vwajj§", old="R@vwaj§")
    corriger_phono(lex, "réveiller;VER;réveillions", "RevEjj§", old="RevEj§")
    corriger_phono(lex, "triller;VER;trillions", "tRijj§", old="tRilj§") # corrige aussi une vraie erreur: [l] en trop

    corriger_phono(lex, "appuyer;VER;appuyiez", "ap8ijje", old="ap8ije")
    corriger_phono(lex, "asseoir;VER;asseyiez", "asEjje", old="asEje")
    corriger_phono(lex, "bousiller;VER;bousilliez", "buzijje", old="buzije")
    corriger_phono(lex, "croire;VER;croyiez", "kRwajje", old="kRwaje")
    corriger_phono(lex, "employer;VER;employiez", "@plwajje", old="@plwaje")
    corriger_phono(lex, "ennuyer;VER;ennuyiez", "@n8ijje", old="@n8ije")
    corriger_phono(lex, "envoyer;VER;envoyiez", "@vwajje", old="@vwaje")
    corriger_phono(lex, "fouiller;VER;fouilliez", "fujje", old="fuje")
    corriger_phono(lex, "griller;VER;grilliez", "gRijje", old="gRije")
    corriger_phono(lex, "habiller;VER;habilliez", "abijje", old="abije")
    corriger_phono(lex, "maquiller;VER;maquilliez", "makijje", old="makije")
    corriger_phono(lex, "mitrailler;VER;mitrailliez", "mitRajje", old="mitRaje")
    corriger_phono(lex, "patrouiller;VER;patrouilliez", "patRujje", old="patRuje")
    corriger_phono(lex, "payer;VER;payiez", "pEjje", old="pEje")
    corriger_phono(lex, "piller;VER;pilliez", "pijje", old="pije")
    corriger_phono(lex, "revoir;VER;revoyiez", "R°vwajje", old="R°vwaje")
    corriger_phono(lex, "rhabiller;VER;rhabilliez", "Rabijje", old="Rabije")
    corriger_phono(lex, "réessayer;VER;réessayiez", "ReesEjje", old="ReesEje")
    corriger_phono(lex, "réveiller;VER;réveilliez", "RevEjje", old="RevEje")
    corriger_phono(lex, "surveiller;VER;surveilliez", "syRvEjje", old="syRvEje")

# [j] prolongé en [ij] après certaines séquences de consonnes,
# où il est difficile de le prononcer court:

    # on a trouvé ces mots avec le motif "[bdgkptfv][lR]j" dans la phonologie:
    corriger_phono(lex, "abstenir;VER;abstiendrions", "apstj5dRij§", old="apstj5dRj§")
    corriger_phono(lex, "comprendre;VER;comprendriez", "k§pR@dRije", old="k§pR@dRje")
    corriger_phono(lex, "consacrer;VER;consacriez", "k§sakRije", old="k§sakRje")
    corriger_phono(lex, "devoir;VER;devriez", "d°vRije", old="d°vRje")
    corriger_phono(lex, "découvrir;VER;découvriez", "dekuvRije", old="dekuvRje")
    corriger_phono(lex, "défendre;VER;défendriez", "def@dRije", old="def@dRje")
    corriger_phono(lex, "déplier;VER;dépliâmes", "deplijam", old="depljam")
    corriger_phono(lex, "enregistrer;VER;enregistriez", "@R°ZistRije", old="@R°ZistRje")
    corriger_phono(lex, "entendre;VER;entendriez", "@t@dRije", old="@t@dRje")
    corriger_phono(lex, "entrer;VER;entriez", "@tRije", old="@tRje")
    corriger_phono(lex, "ganglionnaire;ADJ;ganglionnaires", "g@glijonER", old="g@gljonER")
    corriger_phono(lex, "huîtrier;NOM;huîtriers", "8itRije", old="8itRje")
    corriger_phono(lex, "inoublié;ADJ;", "inublije", old="inublje")
    corriger_phono(lex, "inoublié;ADJ;inoubliées", "inublije", old="inublje")
    corriger_phono(lex, "maintenir;VER;maintiendriez", "m5tj5dRije", old="m5tj5dRje")
    corriger_phono(lex, "marbrier;ADJ;", "maRbRije", old="maRbRje")
    corriger_phono(lex, "multiplier;VER;multipliât", "myltiplija", old="myltiplja")
    corriger_phono(lex, "plier;VER;pliât", "plija", old="plja")
    corriger_phono(lex, "précambrien;ADJ;", "pRek@bRij5", old="pRek@bRj5")
    corriger_phono(lex, "précambrien;NOM;", "pRek@bRij5", old="pRek@bRj5")
    corriger_phono(lex, "publieur;NOM;", "pyblij9R", old="pyblj9R")
    corriger_phono(lex, "replier;VER;repliât", "R°plija", old="R°plja")
    corriger_phono(lex, "supplier;VER;suppliât", "syplija", old="syplja")
    corriger_phono(lex, "tripler;VER;triplions", "tRiplij§", old="tRiplj§")

    # "ski":
    corriger_phono(lex, "skiable;ADJ;", "skijabl", old="skjabl")
    corriger_phono(lex, "skier;VER;", "skije", old="skje")
    corriger_phono(lex, "skier;VER;skiais", "skijE", old="skjE")
    corriger_phono(lex, "skier;VER;skiait", "skijE", old="skjE")
    corriger_phono(lex, "skier;VER;skié", "skije", old="skje")
    corriger_phono(lex, "skier;VER;skiez", "skije", old="skje")
    corriger_phono(lex, "skieur;NOM;", "skij9R", old="skj9R")
    corriger_phono(lex, "skieur;NOM;skieurs", "skij9R", old="skj9R")
    corriger_phono(lex, "skieur;NOM;skieuse", "skij2z", old="skj2z")
    corriger_phono(lex, "skieur;NOM;skieuses", "skij2z", old="skj2z")

    #
    # suppression de mots douteux
    #

    # NOTE: on fait ces suppressions après les autres correctifs,
    # car pour certains de ces mots, on a d’autres correctifs
    # si jamais on décide finalement de les garder

    supprimer(lex, "humano;ADV") # ???
    supprimer(lex, "être-là;NOM") # pas un mot en soi
    # anglais:
    #supprimer(lex, "because;CON")
    #supprimer(lex, "bicause;CON")
    supprimer(lex, "brown sugar;NOM")
    supprimer(lex, "cyberspace;NOM")
    supprimer(lex, "field;NOM")
    supprimer(lex, "line;NOM")
    supprimer(lex, "marker;NOM")
    supprimer(lex, "plum;NOM")
    supprimer(lex, "red river;NOM") # nom propre
    supprimer(lex, "rutherford;NOM") # nom propre (l’élément chimique est le "rutherfordium")
    supprimer(lex, "railway;NOM") # vieilli, employé au XIXe s.
    supprimer(lex, "scenic railway;NOM") # vieilli, maintenant traduit par "montagnes russes"
    supprimer(lex, "slice;NOM")
    supprimer(lex, "snipe;NOM")
    # allemand:
    supprimer(lex, "ausweis;NOM")
    supprimer(lex, "vater;NOM")
    # espagnol:
    supprimer(lex, "retiro;NOM")
    # italien:
    supprimer(lex, "gusto;ADV")
    supprimer(lex, "plazza;NOM") # probab. confusion espagnol "plaza" / italien "piazza"

# TODO:
# - nombreuses confusions voyelles ouvertes/fermées (cf + bas)
#   + [e] qui devrait être [E] (quand produit par "ai, ay, ei", etc.)
#     * "aîné"
#     * "balayer"
#     * "blêmir"
#     * "baleinier"
#     * "messieurs"
#   + [2] qui devrait être [9] ou plutôt [°] (quand produit par "e")
#     * "bachelier"
#     * "ce"
#   + [o] qui devrait être [O] ou réciproquement (très dépendant du locuteur)
# - "abstient, abstrait, absurde…": [p] devrait être [b] ?
# - "hadji" est indiqué comme pluriel ~> singulier
# - "audit, auxdits, dudit, desdit(e)s" (PRE): à lier ?

# TODO: [9] ~> [2] ?
#à l'aveuglette	alav9glEt	à:a; :_,l:l,':_,a:a;v:v,eu:9;g:g,l:l,e:E,tt:t,e:_			ADV			

# TODO: "é":[E] ~> [e]
#
#éleva	El°va	é:E;l:l,e:°;v:v,a:a		élever	VER			ind:pas:3s;
#élevage	El°vaZ	é:E;l:l,e:°;v:v,a:a,g:Z,e:_			NOM	m	s	
#élevages	El°vaZ	é:E;l:l,e:°;v:v,a:a,g:Z,e:_,s:_		élevage	NOM	m	p	
#élevai	El°vE	é:E;l:l,e:°;v:v,ai:E		élever	VER			ind:pas:1s;
#élevaient	El°vE	é:E;l:l,e:°;v:v,ai:E,ent:_		élever	VER			ind:imp:3p;
#élevais	El°vE	é:E;l:l,e:°;v:v,ai:E,s:_		élever	VER			ind:imp:1s;ind:imp:2s;
#élevait	El°vE	é:E;l:l,e:°;v:v,ai:E,t:_		élever	VER			ind:imp:3s;
#élevant	El°v@	é:E;l:l,e:°;v:v,an:@,t:_		élever	VER			par:pre;
#élever	El°ve	é:E;l:l,e:°;v:v,er:e			VER			inf;;
#éleveur	El°v9R	é:E;l:l,e:°;v:v,eu:9,r:R			NOM	m	s	
#éleveurs	El°v9R	é:E;l:l,e:°;v:v,eu:9,r:R,s:_		éleveur	NOM	m	p	
#éleveuse	el°v2z	é:e;l:l,e:°;v:v,eu:2,s:z,e:_		éleveur	NOM	f	s	
#élevez	El°ve	é:E;l:l,e:°;v:v,ez:e		élever	VER			imp:pre:2p;ind:pre:2p;
#éleviez	el°vje	é:e;l:l,e:°;v:v,i:j,ez:e		élever	VER			ind:imp:2p;
#élevions	El°vj§	é:E;l:l,e:°;v:v,i:j,on:§,s:_		élever	VER			ind:imp:1p;
#élevons	El°v§	é:E;l:l,e:°;v:v,on:§,s:_		élever	VER			imp:pre:1p;ind:pre:1p;
#élevons	El°v§	é:E;l:l,e:°;v:v,on:§,s:_		élevon	NOM	m	p	
#élevures	El°vyR	é:E;l:l,e:°;v:v,u:y,r:R,e:_,s:_		élevure	NOM	f	p	
#élevât	El°va	é:E;l:l,e:°;v:v,â:a,t:_		élever	VER			sub:imp:3s;
#élevèrent	El°vER	é:E;l:l,e:°;v:v,è:E,r:R,ent:_		élever	VER			ind:pas:3p;
#élevé	El°ve	é:E;l:l,e:°;v:v,é:e		élever	VER	m	s	par:pas;
#élevé	El°ve	é:E;l:l,e:°;v:v,é:e			ADJ	m	s	
#élevée	El°ve	é:E;l:l,e:°;v:v,é:e,e:_		élever	VER	f	s	par:pas;
#élevée	El°ve	é:E;l:l,e:°;v:v,é:e,e:_		élevé	ADJ	f	s	
#élevées	El°ve	é:E;l:l,e:°;v:v,é:e,e:_,s:_		élever	VER	f	p	par:pas;
#élevées	El°ve	é:E;l:l,e:°;v:v,é:e,e:_,s:_		élevé	ADJ	f	p	
#élevés	El°ve	é:E;l:l,e:°;v:v,é:e,s:_		élever	VER	m	p	par:pas;
#élevés	El°ve	é:E;l:l,e:°;v:v,é:e,s:_		élevé	ADJ	m	p	

# TODO: "o":[O] ~> [o] en syllabe ouverte
#
# regex: om[éè]
#
#biométrie	bjOmetRi	b:b,i:j,o:O;m:m,é:e;t:t,r:R,i:i,e:_			NOM	f	s	
#cardiomégalie	kaRdjOmegali	c:k,a:a,r:R;d:d,i:j,o:O;m:m,é:e;g:g,a:a;l:l,i:i,e:_			NOM	f	s	
#demi-kilomètre	d2mikilOmEtR	d:d,e:2;m:m,i:i;-:_,k:k,i:i;l:l,o:O;m:m,è:E,t:t,r:R,e:_			NOM	m	s	
#électromètre	elEktROmEtR	é:e;l:l,e:E,c:k;t:t,r:R,o:O;m:m,è:E,t:t,r:R,e:_			NOM	m	s	
#endométriose	@dOmetRijoz	en:@,d:d,o:O,m:m,é:e,t:t,r:R,i:ij,o:o,s:z,e:_			NOM	f	s	
#endométrite	@dOmetRit	en:@;d:d,o:O;m:m,é:e;t:t,r:R,i:i,t:t,e:_			NOM	f	s	
#homéostatique	OmeOstatik	h:_,o:O;m:m,é:e;o:O,s:s;t:t,a:a;t:t,i:i,qu:k,e:_			ADJ		s	
#homéothermie	OmeOtERmi	h:_,o:O;m:m,é:e;o:O;th:t,e:E,r:R;m:m,i:i,e:_			NOM	f	s	
#hépatomégalie	epatOmegali	h:_,é:e;p:p,a:a;t:t,o:O;m:m,é:e;g:g,a:a;l:l,i:i,e:_			NOM	f	s	
#interférométrie	5tERfeROmetRi	in:5;t:t,e:E,r:R;f:f,é:e;r:R,o:O;m:m,é:e;t:t,r:R,i:i,e:_			NOM	f	s	
#isométrique	izOmetRik	i:i;s:z,o:O;m:m,é:e;t:t,r:R,i:i,qu:k,e:_			ADJ	f	s	
#nanomètre	nanOmEtR	n:n,a:a;n:n,o:O;m:m,è:E,t:t,r:R,e:_			NOM	m	s	
#odomètre	OdOmEtR	o:O;d:d,o:O;m:m,è:E,t:t,r:R,e:_			NOM	m	s	
#olfactomètre	OlfaktOmEtR	o:O,l:l;f:f,a:a,c:k;t:t,o:O;m:m,è:E,t:t,r:R,e:_			NOM	m	s	
#phénoménalement	fenOmenal°m@	ph:f,é:e;n:n,o:O;m:m,é:e;n:n,a:a;l:l,e:°;m:m,en:@,t:_			ADV			
#podomètre	pOdOmEtR	p:p,o:O;d:d,o:O;m:m,è:E,t:t,r:R,e:_			NOM	m	s	
#prométhéen	pROmete5	p:p,r:R,o:O;m:m,é:e;th:t,é:e;en:5			ADJ	m	s	
#prométhéenne	pROmeteEn	p:p,r:R,o:O;m:m,é:e;th:t,é:e;e:E,nn:n,e:_		prométhéen	ADJ	f	s	
#psychomotrice	psikomOtRis	p:p,s:s,y:i;ch:k,o:o;m:m,o:O;t:t,r:R,i:i,c:s,e:_		psychomoteur	ADJ	f	s	
#psychométrique	psikOmetRik	p:p,s:s,y:i;ch:k,o:O;m:m,é:e;t:t,r:R,i:i,qu:k,e:_			ADJ		s	
#
#halogénure	alOZenyR	h:_,a:a;l:l,o:O;g:Z,é:e;n:n,u:y,r:R,e:_			NOM	m	s	
#
# regex: idRO, igRO, ipnO
#
#hydroptère	idROptER	h:_,y:i;d:d,r:R,o:O,p:p;t:t,è:E,r:R,e:_			NOM	m	s	
#!!!hydrox	idROks	h:_,y:i;d:d,r:R,o:O,x:ks			NOM	m		
#hydroxyde	idROksid	h:_,y:i,d:d,r:R,o:O,x:ks,y:i,d:d,e:_			NOM	m	s	
#hygroma	igROma	h:_,y:i;g:g,r:R,o:O;m:m,a:a			NOM	m	s	
#hypnotiquement	ipnOtik°m@	h:_,y:i,p:p;n:n,o:O;t:t,i:i;qu:k,e:°;m:m,en:@,t:_			ADV			
#
# regex: hypo.*\t.*ipO
#
#hypotonie	ipOtOni	h:_,y:i;p:p,o:O;t:t,o:O;n:n,i:i,e:_			NOM	f	s	
#hypoténuse	ipotenyz	h:_,y:i;p:p,o:o;t:t,é:e;n:n,u:y,s:z,e:_			NOM	f	s	
#hypoxie	ipOksi	h:_,y:i,p:p,o:O,x:ks,i:i,e:_			NOM	f	s	
#hypoxique	ipOksik	h:_,y:i,p:p,o:O,x:ks,i:i,qu:k,e:_			ADJ		s	
#
# divers:
#
#autoritairement
#hovercraft	Ov9RkRaft	h:_,o:O;v:v,e:9,r:R;c:k,r:R,a:a,f:f,t:t			NOM	m	s	
#hyménoptère	imenOptER	h:_,y:i;m:m,é:e;n:n,o:O,p:p;t:t,è:E,r:R,e:_			NOM	m	s	
#hyménoptères	imenOptER	h:_,y:i;m:m,é:e;n:n,o:O,p:p;t:t,è:E,r:R,e:_,s:_		hyménoptère	NOM	m	p	
#hypercholestérolémie	ipERkOlEsteROlemi	h:_,y:i;p:p,e:E,r:R;ch:k,o:O;l:l,e:E,s:s;t:t,é:e;r:R,o:O;l:l,é:e;m:m,i:i,e:_			NOM	f	s	
#hypercoagulabilité	ipERkOagylabilite	h:_,y:i;p:p,e:E,r:R;c:k,o:O;a:a;g:g,u:y;l:l,a:a;b:b,i:i;l:l,i:i;t:t,é:e			NOM	f	s	
#hypersonique	ipERsOnik	h:_,y:i;p:p,e:E,r:R;s:s,o:O;n:n,i:i,qu:k,e:_			ADJ	m	s	
#hyperémotive	ipERemOtiv	h:_,y:i;p:p,e:E;r:R,é:e;m:m,o:O;t:t,i:i,v:v,e:_		hyperémotif	ADJ	f	s	
#hypnagogique	ipnagOZik	h:_,y:i,p:p;n:n,a:a;g:g,o:O;g:Z,i:i,qu:k,e:_			ADJ		s	
#hypnotiquement	ipnOtik°m@	h:_,y:i,p:p;n:n,o:O;t:t,i:i;qu:k,e:°;m:m,en:@,t:_			ADV			
#hypochlorite	ipoklORit	h:_,y:i;p:p,o:o;ch:k,l:l,o:O;r:R,i:i,t:t,e:_			NOM	m	s	
#hypogonadisme	ipogOnadism	h:_,y:i;p:p,o:o;g:g,o:O;n:n,a:a;d:d,i:i,s:s,m:m,e:_			NOM	m	s	
#hélicoptère	elikOptER	h:_,é:e;l:l,i:i;c:k,o:O,p:p;t:t,è:E,r:R,e:_			NOM	m	s	
#hélicoptères	elikOptER	h:_,é:e;l:l,i:i;c:k,o:O,p:p;t:t,è:E,r:R,e:_,s:_		hélicoptère	NOM	m	p	
#héliographe	eljOgRaf	h:_,é:e;l:l,i:j,o:O;g:g,r:R,a:a,ph:f,e:_			NOM	m	s	
#hématologique	ematOlOZik	h:_,é:e;m:m,a:a;t:t,o:O;l:l,o:O;g:Z,i:i,qu:k,e:_			ADJ	f	s	
#hémorroïdaire	emOROidER	h:_,é:e;m:m,o:O;rr:R,o:O;ï:i;d:d,ai:E,r:R,e:_			ADJ		s	
#hémostase	emOstaz	h:_,é:e;m:m,o:O,s:s;t:t,a:a,s:z,e:_			NOM	f	s	
#hépatomégalie	epatOmegali	h:_,é:e;p:p,a:a;t:t,o:O;m:m,é:e;g:g,a:a;l:l,i:i,e:_			NOM	f	s	
#hétérochromie	eteRokROmi	h:_,é:e;t:t,é:e;r:R,o:o;ch:k,r:R,o:O;m:m,i:i,e:_			NOM	f	s	
#hétéroptère	eteROptER	h:_,é:e;t:t,é:e;r:R,o:O,p:p;t:t,è:E,r:R,e:_			NOM	m	s	
#iboga	ibOga	i:i;b:b,o:O;g:g,a:a			NOM	m	s	
#yakitori	jakitORi	y:j,a:a;k:k,i:i;t:t,o:O;r:R,i:i			NOM	m	s	
#yougoslave	jugOslav	y:j,ou:u;g:g,o:O;s:s,l:l,a:a,v:v,e:_			ADJ		s	
#yougoslave	jugOslav	y:j,ou:u;g:g,o:O;s:s,l:l,a:a,v:v,e:_			NOM		s	
#yougoslaves	jugOslav	y:j,ou:u;g:g,o:O;s:s,l:l,a:a,v:v,e:_,s:_		yougoslave	ADJ		p	
#yougoslaves	jugOslav	y:j,ou:u;g:g,o:O;s:s,l:l,a:a,v:v,e:_,s:_		yougoslave	NOM		p	
#zodiac	zOdjak	z:z,o:O;d:d,i:j,a:a,c:k			NOM	m	s	
#zoreille	zOREj	z:z,o:O;r:R,e:E,ill:j,e:_			NOM	m	s	
#à cropetons	akROp°t§	à:a; :_,c:k,r:R,o:O;p:p,e:°;t:t,on:§,s:_			ADV			
#allostérique	alOsteRik	a:a;ll:l,o:O,s:s;t:t,é:e;r:R,i:i,qu:k,e:_			ADJ	m	s	
#ammonitrate	amOnitRat	a:a;mm:m,o:O;n:n,i:i;t:t,r:R,a:a,t:t,e:_			NOM	m		
#choline	kOlin	ch:k,o:O;l:l,i:i,n:n,e:_			NOM	f	s	
#cholines	kolin	ch:k,o:o;l:l,i:i,n:n,e:_,s:_		choline	NOM	f	p	
#cholinestérase	kOlinEsteRaz	ch:k,o:O;l:l,i:i;n:n,e:E,s:s;t:t,é:e;r:R,a:a,s:z,e:_			NOM	f	s	
#cholique	kOlik	ch:k,o:O;l:l,i:i,qu:k,e:_			ADJ	m	s	
#cholécystectomie	kOlesistEktOmi	ch:k,o:O;l:l,é:e;c:s,y:i,s:s;t:t,e:E,c:k;t:t,o:O;m:m,i:i,e:_			NOM	f	s	
#cholécystite	kOlesistit	ch:k,o:O;l:l,é:e;c:s,y:i,s:s;t:t,i:i,t:t,e:_			NOM	f	s	
#chrysolite	kRizOlit	ch:k,r:R,y:i;s:z,o:O;l:l,i:i,t:t,e:_			NOM	f	s	
#chrysolithe	kRizOlit	ch:k,r:R,y:i;s:z,o:O;l:l,i:i,th:t,e:_			NOM	f	s	
#cochonnée	kOSOne	c:k,o:O;ch:S,o:O;nn:n,é:e,e:_			NOM	f	s	
#colostrum	kOlOstROm	c:k,o:O;l:l,o:O,s:s;t:t,r:R,u:O,m:m			NOM	m	s	
#cystotomie	sistOtOmi	c:s,y:i,s:s;t:t,o:O;t:t,o:O;m:m,i:i,e:_			NOM	f	s	
#diagnostiqueur	djagnOstik9R	d:d,i:j,a:a,g:g;n:n,o:O,s:s;t:t,i:i;qu:k,eu:9,r:R			NOM	m	s	
#diabolisme	djabOlism	d:d,i:j,a:a;b:b,o:O;l:l,i:i,s:s,m:m,e:_			NOM	m	s	
#dysphonie	disfOni	d:d,y:i,s:s;ph:f,o:O;n:n,i:i,e:_			NOM	f	s	
#dystocie	distOsi	d:d,y:i,s:s;t:t,o:O;c:s,i:i,e:_			NOM	f	s	
#estrogène	EstROZEn	e:E,s:s;t:t,r:R,o:O;g:Z,è:E,n:n,e:_			NOM		s	
#glioblastome	glijOblastom	g:g,l:l,i:ij,o:O,b:b,l:l,a:a,s:s,t:t,o:o,m:m,e:_			NOM	m	s	
#historiographe	istORjogRaf	h:_,i:i,s:s;t:t,o:O;r:R,i:j,o:o;g:g,r:R,a:a,ph:f,e:_			NOM		s	
#astrologiquement	astROlOZik°m@	a:a,s:s;t:t,r:R,o:O;l:l,o:O;g:Z,i:i;qu:k,e:°;m:m,en:@,t:_			ADV			
#astronomiquement	astROnOmik°m@	a:a,s:s;t:t,r:R,o:O;n:n,o:O;m:m,i:i;qu:k,e:°;m:m,en:@,t:_			ADV			
#archiviste-paléographe	aRSivistpaleOgRaf	a:a,r:R;ch:S,i:i;v:v,i:i,s:s;t:t,e:_,-:_,p:p,a:a;l:l,é:e;o:O;g:g,r:R,a:a,ph:f,e:_			NOM		s	
#archivistes-paléographes	aRSivistpaleOgRaf	a:a,r:R;ch:S,i:i;v:v,i:i,s:s;t:t,e:_,s:_,-:_,p:p,a:a;l:l,é:e;o:O;g:g,r:R,a:a,ph:f,e:_,s:_		archiviste-paléographe	NOM		p	
#scotchais	skOtSE	s:s,c:k,o:O,t:t;ch:S,ai:E,s:_		scotcher	VER			ind:imp:1s;
#scotcher	skOtSe	s:s,c:k,o:O,t:t;ch:S,er:e			VER			inf;
#scotches	skOtS	s:s,c:k,o:O,t:t,ch:S,e:_,s:_		scotcher	VER			ind:pre:2s;
#scotchons	skOtS§	s:s,c:k,o:O,t:t;ch:S,on:§,s:_		scotcher	VER			ind:pre:1p;
#scotchs	skOtS	s:s,c:k,o:O,t:t,ch:S,s:_		scotch	NOM	m	p	
#scotché	skOtSe	s:s,c:k,o:O,t:t;ch:S,é:e		scotcher	VER	m	s	par:pas;
#scotchée	skOtSe	s:s,c:k,o:O,t:t;ch:S,é:e,e:_		scotcher	VER	f	s	par:pas;
#scotchées	skOtSe	s:s,c:k,o:O,t:t;ch:S,é:e,e:_,s:_		scotcher	VER	f	p	par:pas;
#scotchés	skOtSe	s:s,c:k,o:O,t:t;ch:S,é:e,s:_		scotcher	VER	m	p	par:pas;
#scottish	skOtiS	s:s,c:k,o:O;tt:t,i:i,sh:S			NOM	f	s	
#postdoctoral	pOstdOktoRal	p:p,o:O,s:s;t:t,d:d,o:O,c:k;t:t,o:o;r:R,a:a,l:l			ADJ	m	s	
#kosovar	kOsOvaR	k:k,o:O;s:s,o:O;v:v,a:a,r:R			NOM	m	s	
#aponévrotique	aponevROtik	a-po-nevRO-tik	a:a;p:p,o:o;n:n,é:e;v:v,r:R,o:O;t:t,i:i,qu:k,e:_			ADJ	f	s	
#
# TODO: mots en -ptère, -ptér- (coléoptère, hélicoptère, etc.)
#
# TODO: mots en -ose
#
# TODO: mots en co--, cosmo-, chrono-, cyclo-, spéléo-, paléo-, anthropo-, andro-
#
# TODO: inversement [o] ~> [O]
#bastonnent	baston	b:b,a:a,s:s;t:t,o:o,nn:n,ent:_		bastonner	VER			ind:pre:3p;
# préfixe post--
