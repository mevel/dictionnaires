# PLAGES DE LEXIQUE 3.83 PARCOURUES EXHAUSTIVEMENT POUR ÉTIQUETAGE MANUEL DES BARBARISMES:
#     h -- iboga
#     w -- z*
#     kot -- kéfir
#     tous les mots commençant par une lettre diacritée sauf "é"

##
## ÉTIQUETAGE MANUEL DES BARBARISMES DANS LEXIQUE3
##

# Le but premier est de signaler les mots qui suivent un système orthographique
# différent du français et pour lesquels il n’est pas grave que les algorithmes
# de phonologie échouent, mais on étiquette aussi, au petit bonheur la chance,
# les mots d’origine étrangère compatibles avec les règles françaises. La limite
# est assez arbitraire.
#
# Un étiquetage s’applique à tous les mots d’un lemme donné, identifié par une
# chaîne de caractères "lemme;CGRAM" (sans ";" final). On peut aussi cibler une
# flexion spécifique avec son ID "lemme;CGRAM;flexion".
#
# Cet ID est précédé d’un "!" quand le mot n’est pas à priori conforme au
# système orthographique français.
#
# (## indique des mots souhaitables mais pas forcément reconnus actuellement.)

barbarismes_par_langue = {

#
# abréviations:
# (mots dont la phonologie est bonne mais ne suit pas les règles françaises)
#

  "abr" : {
    "!abc;NOM",
    "!adp;NOM",
    "!anti-g;ADJ",
    "!b;NOM",
    "!bcbg;ADJ",
    "!brrr;ONO",
    "!c;NOM",
    "!cm;NOM",
    "!d;NOM",
    #"!dc;ADJ:num",
    "!dc;NOM",
    #"!dm;ADJ:num",
    "!dm;NOM",
    "!dna;NOM",
    "!etc;ADV",
    "!etc.;ADV",
    "!f;NOM",
    "!g;NOM",
    "!hiv;ADJ",
    "!i.e.;NOM",
    "!ibm;NOM",
    "!j;NOM",
    "!k;NOM",
    "!k-o;ADJ",
    "!k-way;NOM",
    "!km;ADV",
    "!l;NOM",
    "!l-dopa;NOM",
    "!lsd;NOM",
    "!m;NOM",
    #"!ml;ADJ:num",
    "!ml;NOM",
    "!mlle;ADJ",
    "!mlle;NOM",
    #"!mm;ADJ:num",
    "!mm;NOM",
    "!mme;ADJ",
    "!mme;NOM",
    "!mn;ADV",
    "!mn;NOM",
    "!n;NOM",
    "!ok;ADJ",
    "!ok;ADV",
    "!p.;NOM",
    "!p.m.;NOM",
    "!pc;NOM",
    "!ph;NOM",
    "!q;NOM",
    "!s;NOM",
    "!svp;ADV",
    "!t;NOM",
    #"!v;ADJ:num",
    "!v;NOM",
    "!w;NOM",
    #"!x;ADJ:num",
    "!xième;ADJ",
    "!y;NOM",
    "!z;NOM",
    "!zzz;ONO",
    "!zzzz;ONO",
  },

#
# anglais:
#

  "en" : {
  # vrac:
    "!ace;NOM",
    "!action painting;NOM",
    "!after-shave;NOM", # [aft9RSav]
    "!after-shave;NOM;after shave", # [aftERSav]
    "!airedale;NOM",
    "!all right;ADV",
    "!ampli-tuner;NOM",
    "!antifading;NOM",
    "!antiskating;ADJ",
    "!antitrust;ADJ",
    "!anti-trust;NOM",
    "baby-sitter;NOM", # [babisit9R] ##
    "!baby;NOM",
    "backgammon;NOM", # [bakgamOn]
    "!background;NOM",
    "!back up;NOM",
    "!bacon;NOM", ##
    "ball-trap;NOM", # [baltRap]
    "!barbecue;NOM", ##
    "!base-ball;NOM", ##
    "!baseball;NOM", ##
    "!basket-ball;NOM", # [baskEtbol] ##
    "basket;NOM", # [baskEt] ##
    "basketteur;NOM", # [baskEt9R] ##
    "!battle-dress;NOM",
    "!beagle;NOM",
    "!beat;ADJ",
    "!beat generation;NOM",
    "!beatnik;ADJ",
    "!beatnik;NOM",
    "!be-bop;NOM",
    "!because;CON",
    "!beefsteak;NOM", ##
    "!beeper;NOM",
    "best;NOM", # [bEst]
    "best of;NOM", # [bEstOf] ##
    "best-seller;NOM", # [bEstsel9R] ##
    "!birth control;NOM",
    "biseness;NOM", # [biznEs]
    "bishop;NOM", # [biSOp]
    "bisness;NOM", # [biznEs]
    "bisness;NOM", # [biznEs]
    "bizness;NOM", # [biznEs]
    "black;ADJ", # [blak]
    "black-bass;NOM", # [blakbas]
    "blackbouler;VER", # [blakbule]
    "black;NOM", # [blak]
    "!black-out;NOM", # [blakaut]
    "blazer;NOM", # [blazER], e:E,r:R$
    "bloody mary;NOM", # [blOdimaRi], oo:O
    "!bloomer;NOM",
    "!bloom;NOM",
    "!blue-jean;NOM",
    "!blue-jean;NOM;blue jeans",
    "!bluffer;VER", ##
    "!bluffeur;NOM", ##
    "!bluff;NOM", ##
    "!blush;NOM",
    "!boat people;NOM",
    "!body-building;NOM", ##
    "!bodybuilding;NOM", ##
    "!boggie;NOM",
    "!boogie-woogie;NOM",
    "!bookmaker;NOM", ##
    "!book;NOM",
    "!boomerang;NOM",
    "!boomer;NOM",
    "!boom;NOM", ##
    "!booster;NOM",
    "!booster;VER",
    "!bootlegger;NOM",
    "!boots;NOM",
    "!borough;NOM",
    "boston;NOM", # [bOstOn]
    "boulder;NOM", # [buld9R]
    "!bowling;NOM", ##
    "!bow-window;NOM",
    "box-calf;NOM", # [bOkskalf]
    "box;NOM", # [bOks] ##
    "boxer;NOM", # [bOksER] ou [bOks9R]
    "boxer-short;NOM", # [bOksERSORt] ou [bOks9R…]
    "box-office;NOM", # [bOksofis] ##
    "!boy-friend;NOM",
    "boy-scoutesque;ADJ", # [bOjskutEsk]
    "boy-scout;NOM", # [bOjskut]
    "!brainstorming;NOM", ##
    "!brain-trust;NOM",
    "!breakdance;NOM",
    "!break-down;NOM",
    "!breakfast;NOM",
    "!break;NOM", ##
    "broker;NOM", # [bRok9R]
    "!brook;NOM",
    "!browning;NOM",
    "!brown sugar;NOM",
    "!brunch;NOM", ##
    "!brushing;NOM",
    "!bubble-gum;NOM",
    "!bubble-gum;NOM;bubble gum",
    "!buggy;NOM",
    "!bug;NOM", ##
    "!building;NOM",
    "bulldozer;NOM", # [byldozER], e:E,r:R$ ##
    "!bulge;NOM",
    "!bumper;NOM",
    "bungalow;NOM", # [b1galo] ##
    "!burger;NOM", ##
    "!burg;NOM",
    "!businessman;NOM",
    "!business;NOM", ##
    "!bye-bye;ONO",
    "!bye;ONO",
    "!by night;ADJ",
    "!by-pass;NOM",
    "!cake;NOM",
    "!cake-walk;NOM",
    "!call-girl;NOM", ##
    "!call-girl;NOM;call girl", ##
    "canter;NOM", # [k@tER] ou [k@t9R]
    "carter;NOM", # [kaRtER]
    "catcher;VER", # [katSe] ##
    "catcheur;NOM", # [katS9R] ##
    "catch;NOM", # [katS] ##
    "caterpillar;NOM", # [katERpilaR]
    "!catgut;NOM",
    "!challenge;NOM", ##
    "!challenger;NOM",
    "!challenger;VER",
    "charleston;ADJ", # [SaRlEstOn]
    "charleston;NOM", # [SaRlEstOn]
    "charter;NOM", # [SaRtER]
    "!chatter;VER",
    "!cheap;ADJ",
    "!cheap;NOM",
    "!check-list;NOM", ##
    "!check-up;NOM",
    "!check-up;NOM",
    "!check-up;NOM;check up",
    "!cheeseburger;NOM", ##
    "!cheese-cake;NOM", ##
    "!cherokee;NOM", ##
    "chester;NOM", # [SEstER]
    "!chippendale;ADJ",
    "!chippendale;NOM",
    "chopper;NOM", # [SOp9R]
    "chop suey;NOM", # [SOpswEj]
    "!christmas;NOM",
    "!clean;ADJ",
    "!clearing;NOM",
    "clipper;NOM", # [klip9R]
    "close-combat;NOM", # [klozk§ba]
    "!close-up;NOM",
    "!club-house;NOM",
    "!clubhouse;NOM",
    "!clubman;NOM",
    "!cluster;NOM",
    "cocker;NOM", # [kokER] ##
    "cockpit;NOM", # [kOkpit] ##
    "cocktail;NOM", # [kOktEl] ##
    "!coffee shop;NOM",
    "!cold;ADJ",
    "!cold-cream;NOM",
    "comic book;NOM", # [kOmikbuk]
    "comics;NOM", # [komik]
    "compound;ADJ", # [k§pund]
    "compound;NOM", # [k§pund]
    "!computer;NOM", # [k§pjut9R]
    "container;NOM", # [k§tEnER]
    "!copyright;NOM",
    "corner;NOM", # [koRnER] ##
    "!corn flakes;NOM", ##
    "!corn-flakes;NOM", ##
    "coroner;NOM", # [koRonER]
    "cosy-corner;NOM", # [kozikORnER]
    "!countries;NOM",
    "!country;ADJ",
    "!cover-girl;NOM",
    "cow-boy;NOM", # [kobOj] ##
    "crabs;NOM", # [kRabs]
    "cracker;NOM", # [kRak9R]
    "!crawlé;ADJ", ##
    "!crawler;VER",
    "!crawl;NOM", ##
    "!creek;NOM",
    "!crooner;NOM",
    "!cross-country;NOM",
    "!crown;NOM",
    "!cruiser;NOM",
    "!crumble;NOM",
    "!cubitainer;NOM", # [kybitEn9R], e:9,r:R$
    "!curling;NOM", ##
    "!custom;NOM",
    "cutter;NOM", # [k9t9R]
    "!cyberspace;NOM",
    "!deadline;NOM", ##
    "!dealer;NOM", ##
    "!dealer;VER", ##
    "!deal;NOM", ##
    "!denim;NOM", # [d°nim]
    "!designer;NOM", ##
    "!design;NOM", ##
    "desk;NOM", # [dEsk]
    "discount;NOM", # [diskunt] ##
    "dispatcher;VER", # [dispatSe] ##
    "docker;NOM", # [dok9R]
    "!dogger;NOM",
    "!down;ADJ",
    "!downing street;NOM",
    "dragster;NOM", # [dRagstER]
    "!dreadlocks;NOM", ##
    "!dream team;NOM",
    "!dressing-room;NOM",
    "drifter;NOM", # [dRift9R]
    "!drive-in;NOM",
    "!drive;NOM",
    "!driver;NOM",
    "!driver;VER",
    "!drugstore;NOM",
    "!dry;ADJ",
    "!duffel-coat;NOM",
    "!duffle-coat;NOM",
    "!dundee;NOM",
    "!east river;NOM",
    "!e-commerce;NOM", ##
    "!e-mail;NOM", ##
    "edwardien;ADJ",
    "!email;NOM", ##
    "!emergency;NOM",
    "!engineering;NOM",
    "!event;NOM",
    "!ex-beatnik;NOM",
    "!ex-dealer;NOM", ##
    "ex-drifter;NOM", # [EksdRift9R]
    "ex-kid;NOM", # [Ekskid]
    "!ex-leader;NOM", ##
    "!ex-strip-teaseur;NOM", ##
    "!extra-dry;NOM",
    "!eye-liner;NOM",
    "!eyeliner;NOM",
    "!fading;NOM",
    "!fahrenheit;ADJ", ##
    "fair-play;ADJ", # [fERplE] ##
    "fair-play;NOM", # [fERplE] ##
    "far-west;NOM", # [faRwEst] ##
    "!fashionable;ADJ",
    "!fashion;NOM",
    "!fast-food;NOM", ##
    "!fast-food;NOM;fast food", ##
    "!fathom;NOM",
    "!feed-back;NOM", ##
    "!feedback;NOM", ##
    "!feeder;NOM",
    "!feeling;NOM", ##
    "ferry-boat;NOM", # [fERibot]
    "fifties;NOM", # [fiftiz]
    "fifty;NOM", # [fifti]
    "fifty-fifty;ADV", # [fiftififty]
    "fifty-fifty;NOM", # [fiftififty]
    "fitness;NOM", # [fitnEs] ##
    "!five o'clock;NOM",
    "flipper;NOM", # [flip9R]
    "!flirtation;NOM", ##
    "!flirter;VER", ##
    "!flirteur;ADJ", ##
    "!flirteur;NOM", ##
    "!flirt;NOM", ##
    "!footballeur;NOM", ##
    "!footballeur-vedette;NOM", ##
    "!footballistique;ADJ", ##
    "!football;NOM", ##
    "!footing;NOM", ##
    "!foreign office;NOM",
    "!freak;NOM",
    "!free jazz;NOM",
    "!free-jazz;NOM",
    "!free-lance;ADJ", ##
    "!free-lance;NOM", ##
    "!freelance;NOM", ##
    "!freezer;NOM",
    "!frisbee;NOM", ##
    "!fuel;NOM",
    "!fuel-oil;NOM",
    "fun;NOM", # [f9n] ##
    "!game;NOM",
    "gangster;NOM", # [g@gstER] ##
    "gay;ADJ", # [gE] ##
    "gay;NOM", # [gE] ##
    "!geez;NOM",
    "!gentleman-farmer;NOM",
    "gin-fizz;NOM", # [dZinfiz]
    "gin-rummy;NOM", # [dZinR9mi]
    "gipsy;NOM", # [Zipsi]
    "!girl friend;NOM",
    "!girl;NOM",
    "!girl-scout;NOM",
    "globe-trotter;NOM", # [glObtROt9R] ##
    "gopher;NOM", # [gOf9R]
    "gray;NOM", # [gRE]
    "!green;NOM",
    "!grill-room;NOM",
    "grizzli;NOM", # [gRizli] ##
    "grizzly;NOM", # [gRizli] ##
    "!groggy;ADJ",
    "!ground;NOM",
    "hacker;NOM", # [ak9R] ##
    "hack;NOM", # [ak] ##
    "haddock;NOM", # [adOk]
    "half-track;NOM", # [alftRak]
    "!hall;NOM", ##
    "!halloween;NOM", ##
    "!hamburger;NOM", ##
    "hammerless;NOM", # [am9RlEs]
    "!happy few;NOM",
    "hard;ADJ", # [aRd]
    "hard edge;NOM", # [aRdEdZ]
    "hard;NOM", # [aRd]
    "hard-top;NOM", # [aRdtOp]
    "!has been;NOM", ##
    "hello;ONO", # [Elo]
    "!hemlock;NOM", # [EmlOk]
    "hickory;NOM", # [ikoRi]
    "hi-fi;NOM", # [ifi]
    "!highlander;NOM",
    "!high life;NOM",
    "!high-life;NOM",
    "!high-tech;ADJ", ##
    "!high-tech;ADJ", ##
    "!high-tech;ADJ;high tech", ##
    "!high-tech;NOM", ##
    "hippie;ADJ", # [ipi] ##
    "hippie;NOM", # [ipi] ##
    "hippy;ADJ", # [ipi] ##
    "hippy;NOM", # [ipi] ##
    "hitchcockien;ADJ", # [itSkOkj5]
    "hitchcockien;ADJ", # [itSkOkj5]
    "hit;NOM", # [it]
    "hit;NOM", # [it]
    "hit-parade;NOM", # [itpaRad]
    "hit-parade;NOM", # [itpaRad]
    "hobby;NOM", # [Obi]
    "hockeyeur;NOM", # [OkEj9R] ##
    "hodgkinien;ADJ", # [OdZkinj5]
    "holster;NOM", # [Olst9R]
    "homeland;NOM", # [omEl@d]
    "home;NOM", # [om]
    "!home-trainer;NOM", # [omtREjn9R]
    "!hooligan;NOM",
    "horse-guard;NOM", # [ORsgwaRd]
    "hot;ADJ", # [Ot]
    "hot dog;NOM", # [OtdOg] ##
    "hot-dog;NOM", # [OtdOg] ##
    "!hotline;NOM", # [Otlajn]
    "hot;NOM", # [Ot]
    "houliganisme;NOM", # [uliganizm]
    "!house-boat;NOM",
    "!house music;NOM",
    "!house;NOM",
    "hovercraft;NOM", # [Ov9RkRaft]
    "hurrah;NOM", # [uRa]
    "hurrah;ONO", # [uRa]
    "huskies;NOM", # [yski]
    "husky;NOM", # [yski]
    "hydrofoil;NOM", # [idRofOjl]
    "!iceberg;NOM", ##
    "!ice-cream;NOM",
    "!ice-cream;NOM;ice cream",
    "!impeachment;NOM",
    "!insight;NOM",
    "!interviewé;ADJ", ##
    "!interviewé;NOM", ##
    "!interviewer;NOM", ##
    "!interviewer;VER", ##
    "!intervieweur;NOM", ##
    "!interview;NOM", ##
    "!irish coffee;NOM",
    "!jamboree;NOM",
    "!jam-session;NOM",
    "!jean;NOM",
    "!jeep;NOM", ##
    "!jet society;NOM",
    "!jet-stream;NOM",
    "!jigger;NOM",
    "!jingle;NOM", ##
    "jodhpurs;NOM;", # [ZodpyR]
    "!jogger;NOM",
    "!jogger;VER",
    "!joggeur;NOM", ##
    "!jogging;NOM", ##
    "!joint-venture;NOM",
    "joker;NOM", # [dZokER] ##
    "kart;NOM", # [kaRt]
    "!keepsake;NOM",
    "ketch;NOM", # [kEtS]
    "ketchup;NOM", # [kEtS9p] ##
    "kicker;NOM", # [kike]
    "kick;NOM", # [kik]
    "kid;NOM", # [kid]
    "killer;NOM", # [kil9R]
    "kilt;NOM", # [kilt] ##
    "kiosk;NOM", # [kjOsk]
    "kipper;NOM", # [kip9R]
    "kitchenette;NOM", # [kitS°nEt] ##
    "kit;NOM", # [kit] ##
    "!kleenex;NOM", ##
    "knickerbocker;NOM", # [knik9RbOk9R]
    "knicker;NOM", # [knik9R]
    "!knock-out;ADJ",
    "!knock-out;NOM",
    "!ladies;NOM",
    "!lady;NOM",
    "!lambswool;NOM",
    "!leader;NOM", ##
    "!leadership;NOM",
    "!leasing;NOM",
    "!leggins;NOM",
    "!light;ADJ",
    "!line;NOM",
    "!living-room;NOM",
    "!loader;NOM",
    "!lock-outer;VER",
    "!lock-out;NOM",
    "!login;ADJ",
    "!looping;NOM", ##
    "!looser;NOM", ##
    "lord-maire;NOM", # [lORdmER]
    "lord;NOM", ##
    "!loser;NOM", # [luz9R]
    "!lychee;NOM",
    "!macintosh;NOM",
    "!made in;ADV",
    "!mail-coach;NOM",
    "!mailer;VER",
    "!mailing;NOM",
    "!mail;NOM", ##
    "manifold;NOM",
    "marker;NOM", # [maRk9R]
    "maser;NOM", # [mazER]
    "mass media;NOM", # [masmedja]
    "matcher;VER", # [matSe]
    "match;NOM", # [matS] ##
    "!médecine-ball;NOM",
    "!medicine-ball;NOM",
    "!meeting;NOM", ##
    "!merchandising;NOM", # [mERtS@dajziG]
    "!mi-building;NOM",
    "!middle class;NOM",
    "!middle-west;NOM",
    "!milady;NOM",
    "!mile;NOM",
    "milk-bar;NOM", # [milkbaR]
    "!milk-shake;NOM", ##
    "!milk-shake;NOM;milk shake", ##
    "mister;NOM", # [mist9R] ##
    "mixer;NOM", # [miks9R] ##
    "modern style;ADJ", # [modERnstil]
    "modern style;NOM", # [modERnstil]
    "modern-style;NOM", # [modERnstil]
    "!moose;ADJ",
    "mound;NOM", # [mund], ou:u, d:d
    "!music-hall;NOM",
    "!music-hall;NOM;music hall",
    "!muskogee;NOM",
    "nasdaq;NOM", # [nazdak]
    "!negro spiritual;NOM",
    "néo-gangster;NOM", # [neog@gstER]
    "!new;ADJ",
    "!new;NOM",
    "!new wave;NOM",
    "!new-yorkais;ADJ",
    "!new-yorkais;NOM",
    "!night-club;NOM",
    "!night-club;NOM;night club",
    "non-stop;ADJ", # [nOnstOp] ##
    "off;ADJ", # [Of] ##
    "offset;ADJ", # [OfsEt]
    "offset;NOM", # [OfsEt]
    "offshore;ADJ", # [OfSOR]
    "off-shore;NOM", # [OfSOR]
    "offshore;NOM", # [OfSOR]
    "!one-step;NOM",
    "!on-line;ADV",
    "!ounce;NOM",
    "!out;ADJ",
    "!out;ADV",
    "!outlaw;NOM",
    "!outrigger;NOM",
    "!outsider;NOM",
    "overdose;NOM", # [ov9Rdoz], e:9 ##
    "!overdrive;NOM",
    "!oxer;NOM",
    "!pacemaker;NOM", ##
    "!pancake;NOM", ##
    "partner;NOM", # [paRtnER]
    "!part time;NOM",
    "!patchwork;NOM", ##
    "pattern;NOM", # [patERn]
    "!peanuts;NOM",
    "!peeling;NOM",
    "!penthouse;NOM", # [pEntawz]
    "!people;ADJ", ##
    "!people;NOM", ##
    "performer;NOM", # [pERfORm9R]
    "picker;NOM", # [pik9R]
    "!pickle;NOM", # [pik9l]
    "pickpocket;NOM", # [pikpokEt] ##
    "pick-up;NOM", # [pik9p]
    "pier;NOM", # [pjER]
    "!piercing;NOM", # [pERsiG] ##
    "!pipe-line;NOM", ##
    "!pipeline;NOM", ##
    "!pipi-room;NOM",
    "pitcher;VER", # [pitSe]
    "pitchpin;NOM", # [pitSp5]
    "placer;NOM", # [plasER]
    "play;ADV", # [plE]
    "play-back;NOM;play back", # [plEbak]
    "play-back;NOM", # [plEbak]
    "play-boy;NOM", # [plEbOj] ##
    "playboy;NOM", # [plEbOj] ##
    "!playmate;NOM", # [plEjmEt]
    "!please;ADV",
    "plum;NOM", # [pl9m]
    "plum-pudding;NOM", # [pl9mpudiG]
    "poche-revolver;NOM", # [pOSRevOlvER]
    "pointer;NOM", # [pw5t9R]
    "poker;NOM", # [pokER] ##
    "!pom-pom-girl;NOM", ##
    "!pom-pom-girl;NOM;pom-pom girl", ##
    "!pom-pom-girl;NOM;pom-pom girls", ##
    "!pool;NOM",
    "pop-corn;NOM", # [pOpkORn] ##
    "porter;NOM", # [pORt9R]
    "portland;NOM", # [pORtl@d]
    "poster;ADJ", # [pOstER]
    "poster;NOM", # [pOstER] ##
    "!pound;NOM",
    "!press-book;NOM",
    "!prime time;NOM",
    "!provider;NOM",
    "!public-relations;NOM",
    "!public school;NOM",
    "pull-over;NOM", # [pylovER] ##
    "!punching-ball;NOM", ##
    "!punching-ball;NOM;punching ball", ##
    "punt;NOM", # [p9nt]
    "!puzzle;NOM", ##
    "!quaker;NOM",
    "!racingman;NOM",
    "!ragtime;NOM",
    "raider;NOM", # [Rajd9R]
    "railway;NOM", # [RElwE]
    "!ranche;NOM",
    "!rancher;NOM",
    "!ranch;NOM", ##
    "ranger;NOM", # [R@Z9R]
    "ray-grass;NOM", # [REgRas]
    "!reader;NOM",
    "!ready-made;NOM",
    "red river;NOM", # [REdRiv9R]
    "!remake;NOM", ##
    "reporter;NOM", # [R°poRtER] ##
    "reset;NOM",
    "!retriever;NOM",
    "!revival;NOM",
    "revolver;NOM", # [RevOlvER] ##
    "!rewriter;VER",
    "!rhythm and blues;NOM",
    "!rhythm'n'blues;NOM",
    "ripper;NOM", # [Rip9R]
    "!riser;NOM",
    "!roadster;NOM",
    "!roast-beef;NOM",
    "!roast-beef;NOM;roast beef",
    "rocker;NOM", # [Rok9R] ##
    "!rock'n'roll;NOM", ##
    "!rock-n-roll;NOM", ##
    "!rock-n-roll;NOM;rock-'n-roll", ##
    "rolle;NOM", # [ROl]
    "roller;NOM", # [ROl9R]
    "rollmops;NOM", # [ROlmOps]
    "roll;NOM", # [ROl]
    "!roof;NOM",
    "rosbif;NOM", # [ROzbif] ##
    "!rough;NOM",
    "!rumsteck;NOM", # [ROmstEk], u:O
    "running;NOM", # [R9niG] ##
    "scanner;NOM", # [skanER] ##
    "!scenic railway;NOM", # [sinikREjlwEj]
    "!schooner;NOM",
    "!scooter;NOM", ##
    "scotcher;VER", # [skOtSe] ##
    "scotch;NOM", # [skOtS] ##
    "scotch-terrier;NOM", # [skOtStERje]
    "scottish;NOM", # [skOtiS]
    "scratcher;VER", # [skRatSe] ##
    "!script-girl;NOM",
    "!scrub;NOM",
    "!seaborgium;NOM",
    "!seersucker;NOM",
    "select;ADJ", # [selEkt] ##
    "self-contrôle;NOM", # [sElfk§tRol]
    "self-control;NOM", # [sElfk§tRol]
    "self-défense;NOM", # [sElfdef@s]
    "!self-made man;NOM", # [sElfmEjdman]
    "!self-made-man;NOM", # [sElfmEjdman]
    "self;NOM", # [sElf] ##
    "self-service;NOM", # [sElfsERvis] ##
    "setter;NOM", # [set9R]
    "seventies;NOM", # [sev9ntiz]
    "!sex-appeal;NOM", # [sEksapil], ea:i
    "!sex-appeal;NOM;sex appeal", # [sEksapil], ea:i
    "!sex-shop;NOM",
    "!sex-shop;NOM;sex shop",
    "!sex-symbol;NOM",
    "!shake-hand;NOM",
    "!shaker;NOM", ##
    "!shakespearien;ADJ",
    "!shakespearien;NOM",
    "shed;NOM", # [SEd]
    "sheriff;NOM", # [SERif]
    "shérif;NOM", # [SeRif] ##
    "sherry;NOM", # [SERi]
    "shetland;NOM", # [SEtl@d]
    "shift;ADJ", # [Sift]
    "shimmy;NOM", # [Simi]
    "shingle;NOM", # [S5gl]
    "shipchandler;NOM", # [SipS@dl9R]
    "!shirting;NOM",
    "!shooter;VER", ##
    "!shooteur;NOM", ##
    "!shoot;NOM",
    "short;NOM", # [SORt] ##
    "!showbiz;NOM",
    "!show-business;NOM", ##
    "show;NOM", # [So]
    "shunter;VER", # [S1te]
    "!side-car;NOM", ##
    "!single;ADJ", ##
    "!single;NOM", ##
    "sitcom;NOM", # [sitkOm] ##
    "sixties;NOM", # [siks-tiz]
    "!skate-board;NOM", ##
    "!skateboard;NOM", ##
    "!skate;NOM", ##
    "!skating;NOM",
    "!skeet;NOM",
    "sketch;NOM", # [skEtS] ##
    "skiff;NOM", # [skif]
    "!skinhead;ADJ",
    "!skinhead;NOM",
    "skip;NOM", # [skip]
    "skipper;NOM", # [skip9R]
    "skipper;VER", # [skipe]
    "!skunks;NOM", # [sk§s] !!!
    "!skydome;NOM",
    "slalomer;VER", # [slalome] ##
    "slalom;NOM", # [slalOm] ##
    "slang;NOM", # [sl@g]
    "slash;NOM", # [slaS] ##
    "!sleeping;NOM",
    "slice;NOM", # [slajs]
    "!sloop;NOM", # [slup]
    "slow;NOM", # [slo]
    "!slush;NOM", # [slOS]
    "smack;NOM", # [smak]
    "smart;ADJ", # [smaRt]
    "smasher;VER", # [smaSe]
    "smash;NOM", # [smaS]
    "!smiley;NOM", # [smajli] ##
    "smocks;NOM", # [smok]
    "smog;NOM", # [smOg]
    "!smurf;NOM", # [sm9Rf]
    "snack-bar;NOM", # [snakbaR]
    "snack;NOM", # [snak]
    "!snipe;NOM", # [snajp]
    "!sniper;NOM", # [snajp9R] ##
    "!snow-boot;NOM",
    "soap-opéra;NOM;soap opéra", # [sopopeRa]
    "soap-opéra;NOM", # [sopopeRa] ##
    "!soccer;NOM", # [sok9R]
    "soft;ADJ", # [sOft]
    "!softball;NOM", # [sOftbol]
    "soft;NOM", # [sOft]
    "soya;NOM", # [soya]
    "!sparring-partner;NOM",
    "!speakeasy;NOM",
    "!speaker;NOM",
    "!speaker;NOM;speakerine",
    "!speech;NOM",
    "!speedé;ADJ",
    "!speeder;VER",
    "!speed;NOM",
    "!spider;NOM",
    "spinnaker;NOM", # [spinak°R]
    "!spleen;NOM", ##
    "!spoiler;NOM", # [spOjl9R] ##
    "!spoon;NOM",
    "!sportswear;NOM", # [spORtswER]
    "sprat;NOM", # [spRat]
    "spray;NOM", # [spRE]
    "!springer;NOM",
    "square;NOM", # [skwaR]
    "squash;NOM", # [skwaS] ##
    "squat;NOM", # [skwat] ##
    "squattériser;VER", # [skwateRize]
    "squatter;NOM", # [skwat9R]
    "squatter;VER", # [skwate] ##
    "squatteur;ADJ", # [skwat9R] ##
    "squatteur;NOM", # [skwat9R] ##
    "!squeeze;NOM",
    "!squeezer;VER",
    "!squire;NOM",
    "!stand-by;NOM",
    "starter;NOM", # [staRt9R]
    "!starting-gate;NOM",
    "!stater;VER",
    "stayer;NOM", # [stEj9R]
    "!steak;NOM", ##
    "!steamboat;NOM",
    "!steamer;NOM",
    "!steeple-chase;NOM",
    "!steeple;NOM",
    "stem;NOM", # [stEm]
    "stepper;NOM", # [stEp9R]
    "stetson;NOM", # [stEtsOn]
    "!steward;NOM", ##
    "sticker;NOM", # [stik9R] ##
    "stock-car;NOM", # [stOkkaR]
    "stock-option;NOM", # [stOkopsj§] ##
    "stoker;NOM", # [stok9R]
    "stokes;NOM", # [stOks]
    "stone;ADJ", # [ston]
    "stout;NOM", # [stut]
    "stretch;NOM", # [stREtS]
    "strip;NOM", # [stRip]
    "stripper;VER", # [stRipe]
    "strip-poker;NOM", # [stRippokER]
    "!strip-tease;NOM", ##
    "!striptease;NOM", ##
    "!strip-tease;NOM", # [stRiptiz] ##
    "!striptease;NOM", # [stRiptiz] ##
    "!strip-teaseur;NOM", ##
    "!stripteaseur;NOM", ##
    "!stripteaseur;NOM", # [stRiptiz2z] ##
    "!strip-teaseur;NOM", # [stRiptiz9R] ##
    "!struggle for life;NOM",
    "!sunlight;NOM",
    "supertanker;NOM", # [sypERt@k9R] ##
    "supporter;NOM", # [sypORtER] ##
    "!sweater;NOM",
    "!sweat-shirt;NOM",
    "!sweepstake;NOM",
    "swiftien;ADJ",
    "!swinguer;VER", ##
    "!tagger;NOM",
    "!take-off;NOM",
    "!talkie;NOM",
    "!talkie-walkie;NOM", ##
    "tanker;NOM", # [t@k9R]
    "!taxi-girl;NOM",
    "tayloriser;VER", # [tEloRize]
    "taylorisme;NOM", # [tEloRizm]
    "!team;NOM",
    "!tea-room;NOM",
    "!teaser;NOM",
    "!teen-ager;NOM",
    "!teenager;NOM",
    "!tee;NOM",
    "!tee-shirt;NOM", ##
    "tender;NOM", # [t@d9R], e:9,r:R$
    "tennis-ballon;NOM", # [tenisbal§]
    "test-match;NOM", # [tEstmatS]
    "!thanksgiving;NOM",
    "thriller;NOM", # [tRil9R] ##
    "!tie-break;NOM",
    "tilbury;NOM", # [tilbyRi]
    "till;NOM", # [til]
    "!time-sharing;NOM",
    "!timing;NOM",
    "toboggan;NOM", # [tobog@] ##
    "!toffee;NOM",
    "toner;NOM", # [tonER]
    "!too much;ADJ", # [tum9tS]
    "topless;ADJ", # [tOplEs]
    "topless;NOM", # [tOplEs]
    "top-modèle;NOM", # [tOpmodEl] ##
    "top model;NOM", # [tOpmodEl] ##
    "top;NOM", # [tOp]
    "!trader;NOM", ##
    "!training;NOM",
    "tramway;NOM", # [tRamwE] ##
    "traveller;NOM", # [tRav°l9R]
    "traveller's chèque;NOM", # [tRav°l9RsSEk]
    "trekkeur;NOM", # [tREk9R], kk:k
    "!trench-coat;NOM",
    "!trigger;NOM",
    "trimmer;NOM", # [tRim9R]
    "!trustee;NOM",
    "!trusteeship;NOM",
    "!t-shirt;NOM", ##
    "tuner;NOM", # [tynER]
    "!tweed;NOM", ##
    "twill;NOM", # [twil]
    "twister;VER", ##
    "twist;NOM", # [twist] ##
    "!underground;ADJ",
    "!underground;NOM",
    "!update;NOM",
    "uppercut;NOM", # [ypERkyt] ##
    "vamp;NOM", # [v@p]
    "!vanity-case;NOM",
    "!vauxhall;NOM",
    "!volley-ball;NOM", ##
    "!wait and see;NOM",
    "!walkie-talkie;NOM",
    "!walkman;NOM",
    "wallace;NOM", # [walas]
    "!wall street;NOM",
    "walter;ADJ", # [waltER]
    "walter;NOM", # [waltER]
    "warrant;NOM", # [waR@]
    "water-closet;NOM", # [watERklozEt]
    "water-closet;NOM", # [watERklozEt]
    "water;NOM", # [watER] ##
    "water-polo;NOM", # [watERpolo] ##
    "waterproof;ADJ", # [watERpRuf]
    "watt;NOM", # [wat] ##
    "kilowatt;NOM", ##
    "kilowattheure;NOM", ##
    "mégawatt;NOM", ##
    "wax;NOM", # [waks]
    "!way of life;NOM",
    "webcam;NOM", # [wEbkam] ##
    "web;NOM", # [wEb] ##
    "!week-end;NOM", ##
    "!week-end;NOM;week end", ##
    "western;NOM", # [wEstERn] ##
    "western-spaghetti;NOM", # [wEstERnspageti] ##
    "wharf;NOM", # [waRf]
    "whig;NOM", # [wig]
    "whipcord;NOM", # [wipkORd]
    "whiskey;NOM", # [wiski]
    "whiskies;NOM", # [wiski]
    "whisky;NOM", # [wiski] ##
    "whisky-soda;NOM", # [wiskisoda]
    "whist;NOM", # [wist]
    "!white-spirit;NOM",
    "wicket;NOM", # [wikEt]
    "!wildcat;NOM",
    "!william;NOM", # [wiljam]
    "!williams;NOM", # [wiljams]
    "!wintergreen;NOM",
    "wishbone;NOM", # [wiSbon]
    "!woofer;NOM",
    "!world music;NOM",
    "!yacht-club;NOM",
    "!yachting;NOM",
    "!yachtman;NOM",
    "!yacht;NOM", ##
    "!yachtwoman;NOM",
    "!yankee;NOM",
    "yard;NOM", # [jaRd]
    "!yeah;ONO",
    "!yearling;NOM",
    "yeti;NOM", # [jeti], e:e
    "york;NOM", # [jORk]
    "!yorkshire;NOM", # [jORkS9R], i:9
    "zapper;VER", ##
    "zip;NOM", ##
    "zipper;VER",
  # "u" ~ [9]:
    "!aéro-club;NOM", ##
    "!anti-club;NOM",
    "!auburn;ADJ", # [ob9Rn], u:9 ##
    "!automobile-club;NOM", ##
    "!ciné-club;NOM", ##
    "!club;NOM", # [kl9b], u:9 ##
    "!clubiste;NOM", # [kl9bist], u:9
    "!drumlin;NOM",
    "!drummer;NOM",
    "!drums;NOM",
    "!dumper;NOM",
    "!dumping;NOM",
    "!ex-junkie;NOM",
    "!fauteuil-club;NOM",
    "!fauteuils-club;NOM",
    "!field;NOM",
    "!flush;NOM",
    "!funky;ADJ",
    "!golf-club;NOM",
    "!grunge;NOM",
    "!gun;NOM",
    "!hold-up;NOM", # [Old9p], u:9 ##
    "!hold-up;NOM;hold up", # [Old9p], u:9 ##
    "!hum;ONO",
    "!humbug;ADJ",
    "!hunter;NOM",
    "!hurricane;NOM",
    "!interclubs;ADJ", # [5tERkl9b], u:9 ##
    "!jumbo;NOM",
    "!jumper;NOM",
    "!jumping;NOM",
    "!junkie;NOM", # [dZ9nki]
    "!junky;NOM", # [dZ9nki]
    "!livres-club;NOM",
    "!lunch;NOM",
    "!luncher;VER",
    "!moto-club;NOM",
    "!must;NOM",
    "!nurse;NOM",
    "!nursery;NOM",
    "!nursing;NOM",
    "!pop-club;NOM",
    "!post-punk;NOM",
    "!pub;NOM", # [p9b], u:9 ##
    "!puddler;VER",
    "!puff;NOM",
    "!punch;NOM", # [p9nS], u:9,n:n ##
    "!puncheur;NOM",
    "!punkette;NOM",
    "!putt;NOM",
    "!putter;VER",
    "!putting;NOM",
    "!rush;NOM", # [R9S], u:9 ##
    "!shunt;NOM",
    "!slug;NOM",
    "!start-up;NOM", # [staRt9p], u:9 ##
    "!surf;NOM", # [s9Rf], u:9 ##
    "!surfer;VER", # [s9Rfe], u:9 ##
    "!surfeur;NOM", # [s9Rf9R], u:9 ##
    "!tennis-club;NOM",
    "!trust;NOM",
    "!truster;VER",
    "!tub;NOM",
    "!turnover;NOM",
    "!updater;VER",
    "!upgrader;VER",
    "!vamp-club;NOM",
    "!velvet;NOM",
    "!vidéo-club;ADJ", # [videokl9b], u:9 ##
    "!vidéo-club;NOM", # [videokl9b], u:9 ##
    "!vidéoclub;NOM", # [videokl9b], u:9 ##
    "!windsurf;NOM",
  # "ey" ~ [E]:
    "attorney;NOM",         # [atORnE], ey:E
    "cockney;NOM",          # [koknE], ey:E
    "colley;NOM",           # [kolE], ey:E
    "disc-jockey;NOM",      # [diskZokE], ey:E
    "disque-jockey;NOM",    # [diskZokE], ey:E
    "hockey;NOM",           # [OkE], ey:E
    "jersey;NOM",           # [ZERzE], ey:E
    "jockey;NOM",           # [ZokE], ey:E
    "mi-jersey;NOM",        # [miZERzE], ey:E
    "mickey;NOM",           # [mikE], ey:E
    "poney;NOM",            # [ponE], ey:E
    "trolley;NOM",          # [tRolE], ey:E
    "trolleybus;NOM",       # [tRolEbys], ey:E
    "volley;NOM",           # [volE], ey:E
  # mixte:
    "!chutney;NOM",          # [S9tnE], u:9, ey:E
  # "ee" ~ [e]:
    "!pedigree;NOM",
  # "oa" ~ [o/O]:
    "coach;NOM",            # [kotS], oa:o, ch:tS
    "!coacher;VER",          # [kotSe], oa:o, ch:tS
    "!coache;NOM",          # [kotS], oa:o, ch:tS
    "!coaltar;NOM",          # [kOltaR], oa:O
    "goal;NOM",             # [gol], oa:o
    "!story-board;NOM",      # [stoRibORd], oa:O
    "!story-board;NOM;story board",      # [stoRibORd], oa:O
    "toast;NOM",            # [tost], oa:o
    "toasteur;NOM",         # [tost9R], oa:o
    "toaster;NOM",          # [tost9R], oa:o, e:9
    "toaster;VER",          # [toste], oa:o
  # "u" ~ [u]:
    "!blue note;NOM", # [blunOt], u:u
    "!blues;NOM", # [bluz], u:u
    "!bull-dog;NOM", # [buldOg], u:u
    "!bulldog;NOM", # [buldOg], u:u
    "!bush;NOM", # [buS], u:u
    "!bushman;NOM", # [buSman], u:u
    "!full;NOM", # [ful], u:u
    "!full-contact;NOM", # [fulk§takt], u:u
    "!input;NOM", # [input], u:u
    "!juke-box;NOM", # [dZukbOks], u:u
    "!pit-bull;NOM", # [pitbul], u:u
    "!pit-bull;NOM;pit bull", # [pitbul], u:u
    "!pitbull;NOM", # [pitbul], u:u
    "!pudding;NOM", # [pudiG], u:u
    "!pullman;NOM", # [pulman], u:u
    "!yuppie;NOM", # [jupi], u:u
  # "ow" ~ [u]:
    "clown;NOM", # [klun], ow:u
    "clownerie;NOM", # [klun°Ri], ow:u
    "clownesque;ADJ", # [klunEsk], ow:u
  # "oo" ~ [w]:
    "après-shampooing;NOM", # [apRES@pw5], oo:w
    "shampooiner;VER", # [S@pwine], oo:w
    "shampooing;NOM", # [S@pw5], oo:w
  # "oo" ~ [u]:
    "baby-boom;NOM", # [babibum], oo:u ##
    "baby-foot;NOM", # [babifut], oo:u ##
    "bazooka;NOM", # [bazuka], oo:u ##
    "cartoon;NOM", # [kartun], oo:u ##
    "cocoon;NOM", ##
    "cookie;NOM", ##
    "cool;ADJ", ##
    "coolie;NOM",
    "coolie-pousse;NOM",
    "coolos;ADJ",
    "flood;ADJ", ##
    "flood;NOM", ##
    "footeux;ADJ", ##
    "footeux;NOM", ##
    "foot;NOM", ##
    "groom;NOM",
    "hollywoodien;ADJ",
    "igloo;NOM", # [iglu], oo:u
    "look;NOM", ##
    "relooker;VER", ##
    "saloon;NOM",
    "scoop;NOM", ##
    "surbooké;ADJ",
    "vroom;ONO", # [vRum], oo:u
    "zoomer;VER", # [zume], oo:u ##
    "zoom;NOM", # [zum], oo:u ##
  # "j/g" ~ [dZ], "ch" ~ [tS]:
    "!acid jazz;NOM", # [asiddZaz], j:dZ
    "!black-jack;NOM", # [blakdZak], j:dZ
    "!challengeur;NOM", # [SalEndZ9R], g:dZ
    "chum;NOM", # [tSOm], ch:tS
    "!gentleman;NOM", # [dZEntl°man], g:dZ, a:a,n:n$ ##
    "!gentry;NOM", # [dZEntRi], g:dZ
    "!gin;NOM", # [dZin], g:dZ, i:i,n:n$
    "!gin-rami;NOM", # [dZinRami], g:dZ, i:i,n:n$
    "homme-sandwich;NOM", # [Oms@dwitS], ch:tS
    "!jack;NOM", # [dZak], j:dZ
    "jacket;NOM", # [dZakEt]
    "jackpot;NOM", # [dZakpOt] ##
    "!jam;NOM", # [dZam], j:dZ
    "jazz;NOM", # [dZaz], j:dZ
    "!jazz-band;NOM", # [dZazb@d], j:dZ
    "!jazz-rock;NOM", # [dZazROk], j:dZ
    "!jazzman;NOM", # [dZazman], j:dZ
    "!jazzy;ADJ", # [dZazi], j:dZ
    "!jerk;NOM", # [dZERk], j:dZ
    "!jet;NOM", # [dZEt], j:dZ
    "!jet-set;NOM", # [dZEtsEt], j:dZ
    "!job;NOM", # [dZOb], j:dZ
    "!management;NOM", # [manadZ°m@], g:dZ
    "!manager;NOM", # [manadZ9R], g:dZ, e:9,r:R$
    "!manager;VER", # [manadZe], g:dZ
    "!manageur;NOM", # [manadZ9R], g:dZ
    "!rocking-chair;NOM", # [ROkiGtSER], ch:tS, ing:iG
    "!rocking-chair;NOM;rocking chair", # [ROkiGtSER], ch:tS, ing:iG
    "sandwich;NOM", # [s@dwitS], ch:tS
    "!winchester;NOM", # [wintSEstER], ch:tS, i:i,n:n
  # "ing" ~ [iG]:
    "baby-sitting;NOM", # [babisitiG], i:i,ng:G ##
    "betting;NOM", # [bEtiG], i:i,ng:G
    "blessing;NOM", # [blEsiG], i:i,ng:G
    "!briefing;NOM", # [bRifiG], i:i,ng:G
    "camping;NOM", # [k@piG], i:i,ng:G ##
    "camping-car;NOM", # [k@piGkaR], i:i,ng:G ##
    "camping-gaz;NOM", # [k@piGgaz], i:i,ng:G ##
    "caravaning;NOM", # [kaRavaniG], i:i,ng:G ##
    "casting;NOM", # [kastiG], i:i,ng:G ##
    "!chewing-gum;NOM", # [SwiGgOm], i:i,ng:G ##
    "!chewing-gum;NOM;chewing gum", # [SwiGgOm], i:i,ng:G ##
    "cracking;NOM", # [kRakiG], i:i,ng:G
    "!dancing;NOM", # [d@siG], i:i,ng:G
    "dispatching;NOM", # [dispatSiG], i:i,ng:G
    "doping;NOM", # [dOpiG], i:i,ng:G
    "dressing;NOM", # [dResiG], i:i,ng:G
    "!débriefing;NOM", # [debRifiG], i:i,ng:G
    "fixing;NOM", # [fiksiG], i:i,ng:G
    "forcing;NOM", # [fORsiG], i:i,ng:G
    "happening;NOM", # [ap°niG], i:i,ng:G
    "holding;NOM", # [OldiG], i:i,ng:G
    "karting;NOM", # [kaRtiG], i:i,ng:G ##
    "kidnapping;NOM", # [kidnapiG], i:i,ng:G ##
    "king-charles;NOM", # [kiGSaRl], i:i,ng:G
    "!landing;NOM", # [l@diG], i:i,ng:G
    "lemming;NOM", # [lEmiG], i:i,ng:G ##
    "lifting;NOM", # [liftiG], i:i,ng:G ##
    "listing;NOM", # [listiG], i:i,ng:G
    "living;NOM", # [liviG], i:i,ng:G
    "!lobbying;NOM", # [lObijiG], i:i,ng:G ##
    "marketing;NOM", # [maRketiG], i:i,ng:G ##
    "!melting-pot;NOM", # [mEltiGpOt], i:i,ng:G ##
    "!melting-pot;NOM;melting pot", # [mEltiGpOt], i:i,ng:G ##
    "monitoring;NOM", # [monitoRiG], i:i,ng:G
    "morphing;NOM", # [mORfiG], i:i,ng:G
    "!packaging;NOM", # [pakaZiG], i:i,ng:G
    "parking;NOM", # [paRkiG], i:i,ng:G ##
    "passing;NOM", # [pasiG], i:i,ng:G
    "planning;NOM", # [planiG], i:i,ng:G ##
    "pouding;NOM", # [pudiG], i:i,ng:G ##
    "pressing;NOM", # [pResiG], i:i,ng:G ##
    "rafting;NOM", # [RaftiG], i:i,ng:G ##
    "!riesling;NOM", # [RisliG], i:i,ng:G
    "ring;NOM", # [RiG], i:i,ng:G
    "!sanderling;NOM", # [s@dERliG], i:i,ng:G
    "scanning;NOM", # [skaniG], i:i,ng:G
    "schilling;NOM", # [SiliG], i:i,ng:G
    "scratching;NOM", # [skRatSiG], i:i,ng:G
    "shilling;NOM", # [SiliG], i:i,ng:G
    "shipping;NOM", # [SipiG], i:i,ng:G
    "shocking;ADJ", # [SOkiG], i:i,ng:G
    "shopping;NOM", # [SOpiG], i:i,ng:G ##
    "smoking;NOM", # [smOkiG], i:i,ng:G ##
    "spamming;NOM", # [spamiG], i:i,ng:G
    "!sponsoring;NOM", # [sp§soRiG], i:i,ng:G ##
    "spring;NOM", # [spRiG], i:i,ng:G
    "standing;NOM", # [st@diG], i:i,ng:G
    "starking;NOM", # [staRkiG], i:i,ng:G
    "sterling;ADJ", # [stERliG], i:i,ng:G
    "sterling;NOM", # [stERliG], i:i,ng:G
    "stretching;NOM", # [stREtSiG], i:i,ng:G
    "string;NOM", # [stRiG], i:i,ng:G ##
    "swing;NOM", # [swiG], i:i,ng:G ##
    "travelling;NOM", # [tRav°liG], i:i,ng:G ##
    "trekking;NOM", # [tREkiG], kk:k, i:i,ng:G ##
    "télémarketing;NOM", # [telemaRk°tiG], i:i,ng:G ##
    "warning;NOM", # [waRniG], i:i,ng:G
    "zapping;NOM", # [zapiG], i:i,ng:G
    "zoning;NOM", # [zoniG], i:i,ng:G
  # "an" ~ [an]:
    "!alderman;NOM", # [aldERman], a:a,n:n
    "!barman;NOM", # [baRman], a:a,n:n ##
    "!cameraman;NOM", # [kameRaman], a:a,n:n
    "!caméraman;NOM", # [kameRaman], a:a,n:n ##
    "!clergyman;NOM", # [klERZiman], a:a,n:n
    "!crossman;NOM", # [kROsman], a:a,n:n
    "!doberman;NOM", # [dobERman], a:a,n:n
    "!drogman;NOM", # [dROgman], a:a,n:n
    "!ex-barman;NOM", # [EksbaRman], a:a,n:n ##
    "!fan;NOM", # [fan], a:a,n:n ##
    "!fan-club;NOM", # [fankl9b], a:a,n:n ##
    "!houligan;NOM", # [uligan], a:a,n:n ##
    "!jerrican;NOM", # [ZeRikan], a:a,n:n ##
    "!jerrycan;NOM", # [ZeRikan], a:a,n:n ##
    "!manhattan;NOM", # [manatan], a:a,n:n
    "!no man's land;NOM", # [nomansl@d], a:a,n:n ##
    "!perchman;NOM", # [pERSman], a:a,n:n
    "!policeman;NOM", # [polis°man], a:a,n:n
    "!recordman;NOM", # [R°kORdman], a:a,n:n
    "!rugbyman;NOM", # [Rygbiman], a:a,n:n
    "!sportsman;NOM", # [spORtsman], a:a,n:n
    "!superman;NOM", # [sypERman], a:a,n:n ##
    "!taximan;NOM", # [taksiman], a:a,n:n
    "!tennisman;NOM", # [tenisman], a:a,n:n ##
    "!van;NOM", # [van], a:a,n:n ##
    "!waterman;NOM", # [wat2Rman], a:a,n:n
    "!wattman;NOM", # [watman], a:a,n:n
  # "en" ~ [En/en/2n/°n]:
    "!anti-establishment;NOM", # [@tiEstabliSmEnt], e:E,n:n
    "!citizen band;NOM", # [sitiz2nb@d], e:2,n:n
    "!establishment;NOM", # [EstabliSmEnt], e:E,n:n
    "!french;NOM", # [fREnS], e:E,n:n
    "!garden-party;NOM", # [gaRdEnpaRti], e:E,n:n
    "!golden;NOM", # [gOldEn], e:E,n:n
    "!happy end;NOM", # [apiEnd], e:E,n:n
    "!open;ADJ", # [op°n], e:°,n:n
    "!open;NOM", # [op°n], e:°,n:n
    "!pence;NOM", # [pEns], e:E,n:n
    "!spencer;NOM", # [spEnsER], e:E,n:n
    "!stencil;NOM", # [stEnsil], e:E,n:n
    "!suspense;NOM", # [syspEns], e:E,n:n ##
    "!trench;NOM", # [tREnS], e:E,n:n
  # "in" ~ [in]:
    "!badminton;NOM", # [badmintOn], i:i,n:n, o:O,n:n ##
    "!bin's;NOM", # [bins], i:i,n:n ##
    "!binz;NOM", # [binz], i:i,n:n ##
    "!bull-finch;NOM", # [bylfinS], i:i,n:n
    "!chintz;NOM", # [Sints], i:i,n:n
    "!clinker;NOM", # [klink9R], i:i,n:n
    "!flint;NOM", # [flint], i:i,n:n
    "!in-bord;ADJ", # [inbORd], i:i,n:n
    "!in-bord;NOM", # [inbORd], i:i,n:n
    "!indoor;ADJ", # [indOR], i:i,n:n, oo:O
    "!linter;NOM", # [lint9R], i:i,n:n, e:9,r:R
    "!mackintosh;NOM", # [makintOS], i:i,n:n
    "!muffin;NOM", # [m9fin], u:9, i:i,n:n ##
    "!parkinson;NOM", # [paRkinsOn], i:i,n:n, o:O,n:n ##
    "!parkinsonien;ADJ", # [paRkinsonj5], i:i,n:n, o:O,n:n ##
    "!peppermint;NOM", # [pEpERmint], i:i,n:n
    "!pidgin;NOM", # [pidZin], i:i,n:n
    "!pin's;NOM", # [pins], i:i,n:n ##
    "!pin-up;NOM", # [pin9p], i:i,n:n, u:9 ##
    "!pink;NOM", # [pink], i:i,n:n
    "!rink;NOM", # [Rink], i:i,n:n
    "!rinker;VER", # [Rinke], i:i,n:n
    "!sit-in;NOM", # [sitin], i:i,n:n
    "!skin;NOM", # [skin], i:i,n:n
    "!spin;NOM", # [spin], i:i,n:n
    "!sprinkler;NOM", # [spRinkl9R], i:i,n:n
    "!sprint;NOM", # [spRint], i:i,n:n ##
    "!sprinter;VER", # [spRinte], i:i,n:n ##
    "!sprinter;NOM", # [spRinte], i:i,n:n ##
    "!sprinter;VER", # [spRint9R], i:i,n:n ##
    "!sprinter;NOM", # [spRint9R], i:i,n:n ##
    "!sprinteur;NOM", # [spRint9R], i:i,n:n
    "!twin-set;NOM", # [twinsEt], i:i,n:n
  },

#
# allemand:
#

  "de" : {
    "!alpenstock;NOM", # [alp9nstOk], e:9,n:n
    "!ausweis;NOM", # [awsvajs], w:v, ei:aj
    "!beethovénien;ADJ",
    "bismarckien;ADJ", # [bismaRkj5]
    "bitter;NOM", # [bit9R], e:9,r:R$
    "!blitzkrieg;NOM", # [blitskRik], z:s, ie:i, g:k
    "!breitschwanz;NOM",
    "bunker;NOM", # [bunk9R]
    "bunsen;NOM", # [b1zEn], e:E,n:n ##
    "clausewitzien;ADJ", # [kloz°vitsj5]
    "!cronstadt;NOM", # [kr§stat], dt:t
    "deutsche mark;ADJ", # [d2tSmaRk]
    "doppler;NOM", # [dOplER]
    "!dieffenbachia;NOM",
    "!edelweiss;NOM",
    "!einsteinien;ADJ", # [ajnStajnjEn], ei:aj,n:n, s:S, ei:aj
    "ersatz;NOM", # [ERzats]
    "feldgrau;NOM", # [fEldgRo]
    "feldspath;NOM", # [fEldspat]
    "feldwebel;NOM", # [fEldvebEl], w:v
    "fitzgéraldien;ADJ", # [fitsZeRaldjEn]
    "foehn;NOM", # [f2n], oe:2
    "führer;NOM", # [fyR9R]
    "!freesia;NOM", # [fREzja]
    "!freesia;NOM", # [fREzja]
    "fritz;NOM", # [fRits]
    "!gauleiter;NOM",
    "gestapiste;NOM", # [gEstapist], g:g
    "gestapo;NOM", # [gEstapo], g:g
    "gigahertz;NOM", # [ZigaERts]
    "!glockenspiel;NOM",
    "graben;NOM", # [gRab9n], e:9,n:n$
    "gretchen;NOM", # [gREtSEn]
    "groschen;NOM", # [gRoS°n], e:°,n:n
    "hamster;NOM", # [amstER], e:E,r:R$
    "hand-ball;NOM", # [@dbal]
    "handball;NOM", # [@dbal]
    "hégélianisme;NOM", # [egeljanizm]
    "hégélien;ADJ", # [egelj5]
    "hertzien;ADJ", # [ERtsj5]
    "hertz;NOM", # [ERts]
    "hornblende;NOM", # [ORnbl5d]
    "jodler;VER", # [jOdle], j:j
    "johannisberg;NOM", # [ZoanisbERg]
    "!jungien;ADJ",
    "!jungien;NOM",
    "junker;NOM", # [ZunkER]
    "lied;NOM", # [lid]
    "kaiser;NOM", # [kajz9R]
    "kapo;NOM", # [kapo]
    "kapout;NOM", # [kaput]
    "kilohertz;NOM", # [kiloERts]
    "kirsch;NOM", # [kiRS]
    "kitch;ADJ", # [kitS]
    "kitsch;ADJ", # [kitS]
    "kommandantur;NOM", # [kom@d@tuR]
    "kommando;NOM", # [kOm@do]
    "kouglof;NOM", # [kuglOf]
    "krach;NOM", # [kRak], ch:k
    "kraft;NOM", # [kRaft]
    "kreutzer;NOM", # [kR2tsER]
    "!kriegspiel;NOM",
    "!kugelhof;NOM",
    "kummel;NOM", # [kumEl]
    "kursaal;NOM", # [kyRsal], aa:a
    "landsturm;NOM", # [l@dstuRm], u:u
    "!landwehr;NOM",
    "!leitmotive;NOM", ##
    "lutz;NOM", # [lyts]
    "mauser;NOM", # [moz9R]
    "mégahertz;NOM", # [megaERts]
    "muesli;NOM", # [m8sli]
    "!munster;NOM", # [m5stER]
    "!nietzschéen;ADJ",
    "!nietzschéen;NOM",
    "!panzer;NOM",
    "!paulownia;NOM",
    "putschiste;ADJ", # [putSist], u:u
    "putsch;NOM", # [putS], u:u
    "quartz;NOM", # [kwaRts]
    "!quasi-jungien;NOM",
    "!reich;NOM",
    "!reichsmark;NOM",
    "!reichstag;NOM",
    "!reichswehr;NOM",
    "roentgen;NOM", # [R9ntgEns], oe:9, g:g, e:E,n:n
    "röntgen;NOM", # [R2ntgEns], ö:2, g:g, e:E,n:n
    "sandow;NOM", # [s@do]
    "schlass;ADJ", # [Slas]
    "schlass;NOM", # [Slas]
    "schlem;NOM", # [SlEm]
    "schleu;ADJ", # [Sl2]
    "schleu;NOM", # [Sl2]
    "schlinguer;VER", # [Sl5ge]
    "schlitte;NOM", # [Slit]
    "schmecter;VER", # [SmEkte]
    "schnaps;NOM", # [Snaps]
    "schnauzer;NOM", # [Snawz9R]
    "schnick;NOM", # [Snik]
    "schnitzel;NOM", # [SnitzEl]
    "schnock;ADJ", # [SnOk]
    "schnock;NOM", # [SnOk]
    "schnoque;ADJ", # [SnOk]
    "schnoque;NOM", # [SnOk]
    "schnouffer;VER", # [Snufe]
    "schnouff;NOM", # [Snuf]
    "schnouf;NOM", # [Snuf]
    "schpile;NOM", # [Spil]
    "schuss;NOM", # [Sus], u:u
    "seltz;NOM", # [sElts]
    "shrapnell;NOM", # [SRapnEl]
    "shrapnel;NOM", # [SRapnEl]
    "silvaner;NOM", # [silvanER]
    "sylvaner;NOM", # [silvanER]
    "spitz;NOM", # [spits]
    "!sprechgesang;NOM",
    "strelitzia;NOM", # [stRElitsja]
    "strudel;NOM", # [StRud9l], s:S, u:u
    "thaler;NOM", # [talER]
    "thalweg;NOM", # [talvEg], w:v
    "wagnérien;ADJ", # [vagneRj5], w:v
    "walkyrie;NOM", # [valkiRi], w:v
    "!weimarien;ADJ",
    "weltanschauung;NOM", # [vEltanSawuG], w:v, a:a,n:n, u:u
    "welter;NOM", # [vEltER], w:v
    "westphalien;ADJ", # [vEstfalj5]
    "wilhelmien;ADJ", # [vilElmj5], w:v
    "wisigothique;ADJ", # [vizigotik], w:v
    "wisigoth;NOM", # [vizigo]
    "witz;NOM", # [vits]
    "wurtembergeois;ADJ", # [vyRt@bERZwa]
    "wurtembergeois;NOM", # [vyRt@bERZwa]
    "yodler;VER", # [jOdle]
    "zeppelin;NOM", # [zEp°l5]
    "zinc;NOM", # [z5g], c:g
  },

#
# néerlandais:
#

  "nl" : {
    "afrikaans;ADJ", # [afRik@s], aan:@
    "afrikaans;NOM", # [afRik@s], aan:@
    "afrikaner;NOM", # [afRikan9R], e:9,r:R$
    "!bintje;NOM",
    "!boer;NOM",  # [buR], oe:u
    "boskoop;NOM", # [bOskOp], oo:O
    "!jonkheer;NOM",
    "gulden;NOM", # [gyldEn], e:E,n:n
    "kot;NOM", # [kot], t:t$
    "kraal;NOM", # [kRal], aa:a
    "polder;NOM", # [pOldER]
    "spéculoos;NOM", # [spekylos], oo:o
    "wagon;NOM", # [vag§], w:v
    "wagon-bar;NOM", # [vag§baR], w:v
    "wagon-lit;NOM", # [vag§li], w:v
    "wagon-restaurant;NOM", # [vag§REstoR@], w:v
    "wagon-salon;NOM", # [vag§sal§], w:v
    "wagonnet;NOM", # [vagonE], w:v
    "wagon-citerne;NOM", # [vag§sitERn], w:v
  },

#
# hébreu:
#

  "he" : {
    "akkadien;NOM", # [akadj5], kk:k
    "akkadien;ADJ", # [akadjEn, kk:k
    "bar-mitsva;NOM", # [baRmitsva]
    "cascher;ADJ", # [kaSER], sch:S, e:E,r:R
    "casher;ADJ", # [kaSER], sh:S, e:E,r:R
    "haggadah;NOM", # [agada], gg:g
    "hanoukka;NOM", # [anuka], kk:k
    "hassidim;NOM", # [asidim], i:i,m:m$
    "hassidique;ADJ", # [asidik]
    "hassidisme;NOM", # [asidizm]
    "hosanna;NOM", # [ozana]
    "hosannah;NOM", # [ozana]
    "kacher;ADJ", # [kaSER], e:E,r:R
    "kascher;ADJ", # [kaSER], sch:S, e:E,r:R
    "kasher;ADJ", # [kaSER], sh:S, e:E,r:R
    "kaddisch;NOM", # [kadiS], sch:S
    "kaddish;NOM", # [kadiS], sh:S
    "kibboutz;NOM", # [kibuts], z:s
    "kibboutzim;NOM", # [kibutsim], z:s
    "kipa;NOM", # [kipa]
    "kippa;NOM", # [kipa]
    "kinnor;NOM", # [kinOR]
    "shekel;NOM", # [SEkEl], sh:S
    "shéol;NOM", # [SeOl], sh:S
    "yeshiva;NOM", # [jESiva], sh:S
    "yiddish;ADJ", # [jidiS], sh:S
    "yom kippour;NOM", # [jOmkipuR], u:u
    "yom kippur;NOM", # [jOmkipuR], u:u
  },

#
# suédois, danois, norvégien, islandais:
#

  "sv" : {
    "akvavit;NOM", # [akvavit]
    "angström;NOM", # [@gStR2m], s:S, ö:2
    "christiania;NOM", # [kRistjanja]
    "drakkar;NOM", # [dRakaR], kk:k
    "maelström;NOM", # [mElstR2m], ae:E, ö:2
    "fjord;NOM", # [fjORd], j:j ##
    "geyser;NOM", # [ZEzER] ##
    "groenlandais;ADJ", # [gRoenl@dE], e:e,n:n ##
    "groenlandais;NOM", # [gRoenl@dE], e:e,n:n ##
    "kierkegaardien;NOM", # [kiRk°gaaRdjEn], ie:i, aa:aa
    "krill;NOM", # [kRil]
    "lur;NOM", # [luR], u:u
    "!smorrebrod;NOM", # [sm2R°bR2d], o:2
  },

#
# portugais:
#

  "pt" : {
    "cruzeiro;NOM", # [kRuzERo], u:u
    "fazenda;NOM", # [fazEnda], e:E,n:n
    "seringueiro;NOM", # [s2RingEjRos], i:i,n:n, e:E,i:j
    "!sertao;NOM", # [sERt@w]
    "!sertão;NOM", # [sERt@w]
  },

#
# espagnol:
#

  "es" : {
    # NOTE: j’ai vérifié les phonologies de tous ces mots
    "aguardiente;NOM", # [agwaRdjEnte], e:E,n:n
    "angustura;NOM", # [@gustuRa], u:u
    "anti-desperado;NOM", # [@tidEspeRado]
    "ayuntamiento;NOM", # [ajuntamjEnto], u:u,n:n, e:E,n:n
    "azulejo;NOM", # [azulexo], u:u, j:x
    "barracuda;NOM", # [baRakuda], u:u
    "buen retiro;NOM", # [bwEnRetiRo], u:w
    "!caballero;NOM", # [kabaljeRo], ll:lj
    "canasta;NOM", # [kanasta]
    "cañon;NOM", # [kaNOn], ñ:N
    "cha-cha-cha;NOM", # [tSatSatSa], ch:tS
    "chiricahua;ADJ", # [tSiRikawa], ch:tS, u:w
    "chiricahua;NOM", # [tSiRikawa], ch:tS, u:w
    "chorizo;NOM", # [tSoRizo], ch:tS
    "churrigueresque;ADJ", # [tSuRig°REsk], ch:tS, u:u
    "churro;NOM", # [tSuRo], ch:tS, u:u
    "comprador;ADJ", # [k§pRadOR]
    "comprador;NOM", # [k§pRadOR]
    "conquistador;NOM", # [k§kistadOR]
    "cucaracha;NOM", # [kukaRatSa], u:u, ch:tS
    "cuesta;NOM", # [kwEsta], u:w
    "cueva;NOM", # [kweva], u:w
    "descamisado;NOM", # [dEskamizado]
    "desperado;NOM", # [despeRado]
    "doña;NOM", # [dONa], ñ:N
    "eldorado;NOM", # [EldoRado]
    "escudo;NOM", # [Eskydo]
    "faena;NOM", # [faena]
    "fiesta;NOM", # [fjEsta]
    "flamenco;ADJ", # [flamEnko], e:E,n:n
    "flamenco;NOM", # [flamEnko], e:E,n:n
    "gaspacho;NOM", # [gaspatSo], ch:tS
    "gazpacho;NOM", # [gaspatSo], ch:tS, z:s
    "gringo;NOM", # [gRiGgo], i:i,ng:Gg
    "habanera;NOM", # [abaneRa], e:e
    "hacienda;NOM", # [asjEnda], e:E,n:n
    "hombre;NOM", # [§bR]
    "huerta;NOM", # [wERta], u:w
    "jerez;NOM", # [xeREs], j:x
    "juan;NOM", # [xwan], j:x, a:a,n:n
    "!llano;NOM", # [ljanos], ll:lj
    "machisme;NOM", # [matSizm], ch:tS
    "machiste;ADJ", # [matSist], ch:tS
    "macho;ADJ", # [matSo], ch:tS
    "macho;NOM", # [matSo], ch:tS
    "macumba;NOM", # [makumba], u:u,m:m
    "madre de dios;NOM", # [madRededjos]
    "marihuana;NOM", # [maRiRwana], h:R
    "marijuana;NOM", # [maRiRwana], j:R
    "matchiche;NOM", # [matSiS]
    "milonga;NOM", # [mil§ga]
    "muchacho;NOM", # [mutSatSo], u:u, ch:tS
    "navajo;ADJ", # [navaRo], j:R
    "navajo;NOM", # [navaRo], j:R
    "poncho;NOM", # [p§tSo], ch:tS
    "pronunciamiento;NOM", # [pRonunsjamjEnto], u:u,n:n, e:E,n:n
    "quechua;NOM", # [ketSwa], ch:tS, u:w
    "rancho;NOM", # [R@tSo], ch:tS
    "retiro;NOM", # [RetiRo]
    "rioja;NOM", # [Rjoxa], j:x
    "rumba;NOM", # [Rumba], u:u,m:m
    "señor;NOM", # [seNOR], ñ:N
    "yanqui;ADJ", # [j@ki]
  },

#
# italien:
#

  "it" : {
    # NOTE: j’ai vérifié les phonologies de tous ces mots
    "accelerando;ADV", # [akseleRando], a:a,n:n
    "adagio;ADV", # [adadZjo], g:dZ
    "adagio;NOM", # [adadZjo], g:dZ
    "aggiornamento;NOM", # [adZjORnamEnto], gg:dZ, e:E,n:n
    "a giorno;ADV", # [adZjORno], g:dZ
    "a giorno;ADV;à giorno", # [adZjORno], g:dZ
    "al dente;ADV", # [aldEnte], e:E,n:n
    "anch'io son pittore;ADV", # [@kjosOnpitORe]
    "appoggiature;NOM", # [apOdZjatyR], gg:dZ
    "arioso;NOM", # [aRjozo]
    "cappuccino;NOM", # [kaputSino], cc:tS
    "capuccino;NOM", # [kaputSino], cc:tS
    "capucino;NOM", # [kaputSino], c:t
    "carpaccio;NOM", # [kaRpatSo], cci:tS
    "ciao;ONO", # [tSao], ci:tS
    "cinzano;NOM", # [tSinzano], c:tS, i:i,n:n
    "crescendo;ADJ", # [kReSEndo], sc:S, e:E,n:n
    "crescendo;ADV", # [kReSEndo], sc:S, e:E,n:n
    "crescendo;NOM", # [kReSEndo], sc:S, e:E,n:n
    "decrescendo;ADV", # [dEkReSEndo], sc:S, e:E,n:n
    "decrescendo;NOM", # [dEkReSEndo], sc:S, e:E,n:n
    "della francesca;NOM", # [dElafR@sEska]
    "della porta;NOM", # [dElapORta]
    "della robbia;NOM", # [dElaRObja]
    "divertimento;NOM", # [divERtimEnto], e:E,n:n
    "dolce;ADV", # [dOltSe], c:tS
    "duce;NOM", # [dutSe], c:tS
    "farniente;NOM", # [faRnjEnte], e:E,n:n
    "fettuccine;NOM", # [fetutSine], cc:tS
    "giocoso;ADJ", # [dZjokozo], g:dZ
    "gnocchi;NOM", # [NOki], cch:k
    "gruppetto;NOM", # [gRupEto], u:u
    "gusto;ADV", # [gusto], u:u
    "impresario;NOM", # [5pResaRjo], e:e, s:s, i:ii (au pluriel)
    "in petto;ADV", # [inpeto], i:i,n:n
    "intermezzo;NOM", # [5tERmEdzo], zz:dz
    "jacuzzi;NOM", # [Zakuzi], u:u, zz:z
    "lazzi;NOM", # [ladzi], zz:dz
    "loggia;NOM", # [lOdZja], gg:dZ
    "messer;NOM", # [mesER]
    "mezzanine;NOM", # [mEdzanin], zz:dz
    "mezza voce;ADV", # [mEdzavotSe], c:tS
    "mezza-voce;ADV", # [mEdzavotSe], c:tS
    "mezzo;ADV", # [mEdzo], zz:dz
    "mezzo;NOM", # [mEdzo], zz:dz
    "mezzo-soprano;NOM", # [mEdzosopRano], zz:dz
    "mozzarella;NOM", # [modzaREla], zz:dz
    "mozzarelle;NOM", # [mOdzaREl], zz:dz
    "non troppo;ADV", # [nOntRopo], o:O,n:n
    "palazzo;NOM", # [paladzo], zz:dz
    "paparazzi;NOM", # [papaRadzi], zz:dz
    "paparazzo;NOM", # [papaRadzo], zz:dz
    "piazza;NOM", # [pjadza], zz:dz
    "piazzetta;NOM", # [pjadzeta], zz:dz
    "pizza;NOM", # [pidza], zz:dz
    "pizzaiolo;NOM", # [pidzajOlo], zz:dz
    "pizzeria;NOM", # [pidzeRja], zz:dz
    "pizzicato;NOM", # [pidzikato], zz:dz
    "polenta;NOM", # [polEnta], e:E,n:n
    "pouzzolane;NOM", # [pudzolan], zz:dz
    "pupazzo;NOM", # [pupadzo], u:u, zz:dz
    "osso buco;NOM", # [osobuko], u:u
    "piu;ADV", # [piu], u:u
    "più;ADV", # [piu], ù:u
    "putto;NOM", # [puti], u:u
    "quattrocento;NOM", # [kwatRotSEnto], c:tS, e:E,n:n
    "razzia;NOM", # [Radzja], zz:dz
    "razzier;VER", # [Radzje], zz:dz
    "rezzou;NOM", # [R°zu], zz:z
    "rinforzando;ADV", # [RinfORdzando], z:dz, a:a,n:n, i:i,n:n
    "scénario;NOM;scenarii", # [senaRi], ii:i
    "sostenuto;ADV", # [sOstenuto], u:u
    "tempo;NOM", # [tEmpo], e:E,m:m
    "trecento;NOM", # [tRetSEnto], c:tS, e:E,n:n
    "tutti frutti;ADV", # [tutifRuti], u:u
    "tutti quanti;ADV", # [tutikwanti], u:u, q:k,u:w, a:a,n:n
    "valpolicella;NOM", # [valpolitSEla], c:tS
    "antifascisme;NOM", # [@tifaSizm], sc:S
    "anti-fasciste;ADJ", # [@tifaSist], sc:S
    "antifasciste;ADJ", # [@tifaSist], sc:S
    "antifasciste;NOM", # [@tifaSist], sc:S
    "fascisme;NOM", # [faSizm], sc:S
    "fasciste;ADJ", # [faSist], sc:S
    "fasciste;NOM", # [faSist], sc:S
    "fasciser;VER", # [faSize], sc:S
    "néo-fascisme;NOM", # [neofaSizm], sc:S
    "néofascisme;NOM", # [neofaSizm], sc:S
    "néo-fasciste;ADJ", # [neofaSist], sc:S
    "néo-fasciste;NOM", # [neofaSist], sc:S
    "acqua-toffana;NOM", # [akwatofana], cq:k OU q:k,u:w
    "zani;NOM", # [dzani], z:dz
    "zingaro;NOM", # [dzingaRo], z:dz, i:i,n:n
  },

#
# latin:
#

  "la" : {
    # NOTE: j’ai vérifié les phonologies de tous ces mots
    "ab absurdo;ADV", # [abapsuRdo], u:u
    "ad hoc;ADJ", # [adOk]
    "ad infinitum;ADV", # [ad5finitOm], u:O,m:m
    "ad libitum;ADV", # [adlibitOm], u:O,m:m
    "ad limina;ADV", # [adlimina]
    "ad majorem dei gloriam;ADV", # [admaZoREmdeigloRjam], e:e,i:i
    "ad nutum;ADV", # [adnytOm], u:O,m:m
    "ad patres;ADV", # [adpatREs], e:E,s:s$
    "ad vitam aeternam;ADV", # [advitametERnam], ae:e
    "aegipan;NOM",
    "a fortiori;ADV", # [afORsjoRi], t:s,i:j
    "agnus dei;NOM", # [agnusdei], u:u
    "agnus-dei;NOM", # [agnusdei], u:u
    "alléluia;NOM", # [aleluja], u:u
    "alléluia;ONO", # [aleluja], u:u
    "alléluia;NOM", # [aleluja], u:u
    "alléluia;ONO", # [aleluja], u:u
    "althaea;NOM",
    "amen;NOM", # [amEn], e:E,n:n
    "amen;ONO", # [amEn], e:E,n:n
    "ante;NOM",
    "a posteriori;ADV", # [apOsteRjoRi], e:e
    "a priori;ADV", # [apRijoRi]
    "bucco;NOM", # [buko], u:u # un genre biologique d’oiseaux
    "bufo;NOM", # [bufo], u:u # genre biologique d’amphibiens
    "caecum;NOM", # orthographe pédante de "cécum"
    "caesium;NOM", # orthographe pédante de "césium"
    "caput mortuum;NOM", # [kaputmORtuOm], u:u
    "casus belli;NOM", # [kazysbElli]
    "chlamydia;NOM",
    "!conjungo;NOM",
    "colostrum;NOM", # [kolostROm], u:O
    "corpus delicti;NOM", # [kORpysdelikti]
    "crataegus;NOM",
    "curriculum;NOM", # [kyRikylOm], u:O
    "curriculum vitae;NOM", # [kyRikylOmvite], u:O
    "de amicis;NOM", # [deamikis], c:k
    "de auditu;ADV", # [deodity]
    "de facto;ADV", # [defakto]
    "delirium;NOM", # [deliRjOm]
    "delirium tremens;NOM", # [deliRjOmtRemEns], e:E,n:n
    "de plano;ADV", # [deplano]
    "de profundis;NOM", # [depRofundis], u:u,n:n
    "de santis;NOM", # [des@tis]
    "de visu;ADV", # [d2vizy]
    "deus ex machina;NOM", # [deuseksmakina], u:u
    "dies irae;NOM",
    "dignus est intrare;ADV", # [dignusEstintRaRe], u:u, i:i,n:n
    "ecce homo;ADV", # [Ekseomo]
    "et caetera;ADV",
    "et cetera;ADV", # [etseteRa]
    "etcetera;ADV", # [EtseteRa]
    "et seq;ADV", # [Etsek]
    "ex abrupto;ADV", # [EksabRypto]
    "ex nihilo;ADV", # [Eksniilo]
    "ex-voto;NOM", # [Eksvoto]
    "frater;NOM", # [fRatER], e:E,r:R
    "forum;NOM", # [foROm], u:O
    "gratis pro deo;ADV", # [gRatispRodeo]
    "hic et nunc;ADV", # [ikEtnunk], u:u,n:n
    "homo erectus;NOM", # [omoeRektys], e:e
    "homo habilis;NOM", # [omoabilis], i:i,s:s$
    "in;ADJ", # [in], i:i,n:n
    "in absentia;ADV", # [inabsEnsja], i:i,n:n, e:E,n:n
    "in abstracto;ADV", # [inapstRakto], i:i,n:n
    "in anima vili;ADV", # [inanimavili], i:i,n:n
    "in cauda venenum;ADV", # [inkodavenenOm], i:i,n:n
    "in extenso;ADV", # [inEkstEnso], i:i,n:n, e:E,n:n
    "in extremis;ADV", # [inEkstRemis], i:i,n:n
    "in memoriam;ADV", # [inmemoRjam], i:i,n:n
    "in pace;NOM", # [inpase], i:i,n:n
    "in utero;ADJ", # [inyteRo], i:i,n:n
    "in utero;ADV", # [inyteRo], i:i,n:n
    "in vino veritas;ADV", # [invinovERitas], i:i,n:n
    "in vitro;ADJ", # [invitRo], i:i,n:n
    "in vitro;ADV", # [invitRo], i:i,n:n
    "in vivo;ADV", # [invivo], i:i,n:n
    "in-folio;ADJ", # [infoljo], i:i,n:n
    "in-folio;NOM", # [infoljo], i:i,n:n
    "in-octavo;ADJ", # [inOktavo], i:i,n:n
    "in-octavo;NOM", # [inOktavo], i:i,n:n
    "in-quarto;ADJ", # [inkwaRto], i:i,n:n
    "in-quarto;NOM", # [inkwaRto], i:i,n:n
    "in-seize;NOM", # [insEz], i:i,n:n
    "lamento;NOM", # [lamEnto], e:E,n:n
    "lento;ADV", # [lEnto], e:E,n:n
    "liber;NOM", # [libER], e:E,r:R$
    "magister;NOM", # [maZistER], e:E,r:R$
    "master;NOM", # [mast9R], e:9,r:R$
    "minus habens;NOM", # [minysab5s]
    "modus operandi;NOM", # [modusopeR@di], u:u
    "modus vivendi;NOM", # [modusvivEndi], u:u, e:E,n:n
    "mutatis mutandis;ADV", # [mutatismut@dis], u:u
    "naevus;NOM", # orthographe pédante de *"névus"
    #"numerus clausus;NOM" : [nymeRysklozys]
    "nihil obstat;ADV", # [niilOpstat], t:t$
    "pater;NOM", # [patER]
    "pater noster;NOM", # [patERnOstER]
    "post mortem;ADV", # [pOstmORtEm]
    "post-scriptum;NOM", # [pOstskRiptOm], u:O
    "praesidium;NOM", # orthographe pédante de "présidium"
    "presbyterium;NOM", # [pREsbiteRjOm], u:O,m:m
    "princeps;ADJ", # [pR5sEps], s:s$
    "quater;ADV", # [kwatER]
    "requiescat in pace;NOM", # [Rekwieskatinpase], q:k,u:w, i:i,n:n
    "secundo;ADV", # [s°g§do], c:g
    "semen-contra;NOM", # [semEnk§tRa], e:E,n:n
    "sic transit gloria mundi;ADV", # [siktR@zitgloRjamundi], u:u,n:n
    "sine qua non;ADV", # [sinekwanOn], e:e, qu:kw, o:O,n:n$
    "stabat mater;NOM", # [stabamatER]
    "status;NOM", # [statys]
    "suber;NOM", # [sybER]
    "sui generis;ADJ", # [s8iZeneRis]
    "summum;NOM", # [sOmOm], u:O
    "sursum corda;ADV", # [syRsOmkORda]
    "taenia;NOM", # orthographe pédante de "ténia"
    "te deum;NOM", # [tedeum], u:u
    "terra incognita;NOM", # [tERainkOgnita], i:i,n:n
    "triumvir;NOM", # [tRijOmviR]
    "triumvirat;NOM", # [tRijOmviRa]
    "tu autem;NOM", # [tyotEm]
    "urbi et orbi;ADV", # [uRbiEtORbi], u:u
    "valgus;ADJ", # [valgys]
    "vulgum pecus;NOM", # [vylgOmpekys]
    "velux;NOM", # [velyks], e:e (pseudo-latin)
  },

#
# arabe:
#

  "ar" : {
    "bordj;NOM", # [bORdZ]
    "bousbir;NOM", # [busbiR]
    "casbah;NOM", # [kasba], h:_$
    "fathma;NOM", # [fatma]
    "fatma;NOM", # [fatma]
    "fedayin;NOM", # [fedajin], i:i,n:n$
    "hadith;NOM", # [adit]
    "hadj;NOM", # [adZ]
    "hadji;NOM", # [adZi]
    "haïk;NOM", # [aik]
    "hallali;NOM", # [alali]
    "halva;NOM", # [alva]
    "hamada;NOM", # [amada]
    "hammam;NOM", # [amam]
    "hanap;NOM", # [anap]
    "harem;NOM", # [aREm]
    "harissa;NOM", # [aRisa]
    "harki;NOM", # [aRki]
    "hasch;NOM", # [aS]
    "haschich;NOM", # [aSiS]
    "haschisch;NOM", # [aSiS]
    "hégire;NOM", # [eZiR]
    "houari;NOM", # [waRi]
    "husseinite;NOM", # [ysEnit]
    "inch allah;ONO", # [inSala], i:i,n:n
    "jellaba;NOM", # [dZElaba], j:dZ
    "jihad;NOM", # [dZijad], j:dZ
    "kandjar;NOM", # [k@dZaR]
    "kasbah;NOM", # [kasba], h:_$
    "katiba;NOM", # [katiba]
    "kawa;NOM", # [kawa]
    "kebab;NOM", # [kebab], e:e
    "keffieh;NOM", # [kefje], eh:e
    "khalife;NOM", # [kalif]
    "khamsin;NOM", # [kamsin], i:i,n:n
    "khat;NOM", # [kat]
    "khôl;NOM", # [kol]
    "kohl;NOM", # [kol]
    "kief;NOM", # [kjEf]
    "ksar;NOM", # [ksaR]
    "leben;NOM", # [lebEn], e:E,n:n$
    "maghzen;NOM", # [makzEn], g(h):k, e:E,n:n
    "mameluk;NOM", # [mam°luk], u:u
    "minbar;NOM", # [minbaR], i:i,n:n
    "muezzin;NOM", # [m8Edzin], zz:dz, i:i,n:n
    "mufti;NOM", # [mufti], u:u
    "muphti;NOM", # [mufti], u:u
    "rambla;NOM", # [R@bla]
    "sloughi;NOM", # [slugi], g:g,h:_
    "smala;NOM", # [smala]
    "smalah;NOM", # [smala], h:_$
    "!taifa;NOM", # [tEifa]
    "wali;NOM", # [wali]
    "willaya;NOM", # [vilaja], w:v
  },

#
# turc:
#

  "tr" : {
    "ayan;NOM", # [aj@]
    "bey;NOM", # [bE]
    "dey;NOM", # [dE]
    "nay;NOM", # [nE]
    "ney;NOM", # [nE]
    "gengis khan;NOM", # [dZEndZiskan], g:dZ, e:E,n:n, a:a,n:n
    "haseki;NOM", # [aski], s:s
    "hodja;NOM", # [OdZa]
    "kan;NOM", # [k@]
    "khan;NOM", # [k@]
    "khanat;NOM", # [kana]
    "khazar;ADJ", # [kazaR]
    "khédival;ADJ", # [kedival]
    "khédive;NOM", # [kediv]
    "lokoum;NOM", # [lokum]
    "loukoum;NOM", # [lukum]
    "rahat-lokoum;NOM", # [Raatlokum]
    "sandjak;NOM", # [s@dZak]
    "yali;NOM", # [jali]
    "yaourt;NOM", # [jauRt]
    "yaourtière;NOM", # [jauRtjER]
    "yatagan;NOM", # [jatag@]
    "yoghourt;NOM", # [joguRt]
    "yogourt;NOM", # [joguRt]
    "yourte;NOM", # [juRt]
  },

#
# russe, ukrainien:
#

  "ru" : {
    "agit-prop;NOM", # [aZitpROp], p:p$
    "apparatchik;NOM", # [apaRatSik]
    "!borchtch;NOM", # [bORtS], ch:_
    "chaman;NOM", # [Saman], a:a,n:n
    "datcha;NOM", # [datSa]
    #"dostoïevskien;ADJ" ,# [dOstojEvskj5]
    "glasnost;NOM", # [glasnOst]
    "gosplan;NOM", # [gospl@]
    "hopak;NOM", # [Opak]
    "!intelligentsia;NOM", # [inteligEntsja]], i:i,n:n, g:g, e:E,n:n
    "isba;NOM", # [izba], s:z
    "!kalachnikov;NOM", # [kalaSnikOf], v:f
    "kazakh;ADJ", # [kazak]
    "kazakh;NOM", # [kazak]
    "kéfir;NOM", # [kefiR]
    "knout;NOM", # [knut], t:t$
    "kolkhoze;NOM", # [kOlkoz]
    "kolkhozien;NOM", # [kOlkozj5]
    "komintern;NOM", # [kOmintERn], i:i,n:n
    "komsomol;NOM", # [kOmsomOl] o:O,m:m
    "koulak;NOM", # [kulak]
    "kremlin;NOM", # [kREml5]
    "kvas;NOM", # [kvas]
    "mazout;NOM", # [mazut], t:t$
    "mazouté;ADJ", # [mazute]
    "mazouter;VER", # [mazute]
    "menchevik;NOM", # [mEnSevik], e:E,n:n
    "mitchourinien;ADJ", # [mitSuRinj5]
    "pavlovien;ADJ", # [pavlOvj5]
    "politburo;NOM", # [politbyRo]
    "polski;NOM", # [pOl-ski]
    "sambo;NOM", # [sambo], a:a,m:m
    "samizdat;NOM", # [samizda]
    "shaman;NOM", # [Saman], a:a,n:n
    "sovkhoze;NOM", # [sOvkoz]
    "spetsnaz;NOM", # [spEtsnaz], z:z$
    "spoutnik;NOM", # [sputnik]
    "tchékhovien;ADJ", # [tSekOvjEn]
    "tchékiste;NOM", # [tSekist]
    "tokamak;NOM", # [tokamak]
    "toungouse;ADJ", # [tuGguz]
    "trotskisme;NOM", # [tROtskizm]
    "trotskiste;ADJ", # [tROtskist]
    "trotskiste;NOM", # [tROtskist]
    "trotskyste;ADJ", # [tROtskist]
    "trotskyste;NOM", # [tROtskist]
    "tsar;NOM", # [tsaR]
    "tsar;NOM;tsarine", # [tsaRin]
    "tsarévitch;NOM", # [tsaRevitS]
    "tsarisme;NOM", # [tsaRizm]
    "tsariste;ADJ", # [tsaRist]
    "tsariste;NOM", # [tsaRist]
    "tsigane;ADJ", # [tsigan]
    "tsigane;NOM", # [tsigan]
    "tzar;NOM", # [tsaR], z:s
    "tzar;NOM;tzarine", # [tsaRin], z:s
    "tzarevitch;NOM", # [tsaRevitS], z:s
    "tzigane;ADJ", # [tsigan], z:s
    "tzigane;NOM", # [tsigan], z:s
    "zakouski;NOM", # [zakuski]
    "zemstvo;NOM", # [zemstvo]
  },

#
# japonais:
#

  "ja" : {
    "bushi;NOM", # [buSi], u:u
    "bushido;NOM", # [buSido], u:u
    "bunraku;NOM", # [bunRaku], u:u,r:R
    "dan;NOM", # [dan], a:a,n:n
    "geisha;NOM", # [geSa], g:g
    "haïkaï;NOM", # [aikaj]
    "haïku;NOM", # [aiku], u:u
    "hara-kiri;NOM", # [aRakiRi]
    "jiu-jitsu;NOM", # [ZjyZitsy]
    "kabuki;NOM", # [kabuki], u:u
    "kami;NOM", # [kami]
    "kamikaze;ADJ", # [kamikaz]
    "kamikaze;NOM", # [kamikaz]
    "karaoké;NOM", # [kaRaoke]
    "karaté;NOM", # [kaRate]
    "karatéka;NOM", # [kaRateka]
    "kata;NOM", # [kata]
    "keiretsu;NOM", # [kEjREtsu], u:u
    "kendo;NOM", # [kEndo], e:E,n:n
    "kimono;NOM", # [kimono]
    "komi;ADJ", # [komi]
    "komi;NOM", # [komi]
    "koto;NOM", # [koto]
    "nikkei;NOM", # [nikEj]
    "nunchaku;NOM", # [nunSaku], u:u,n:n
    "pachinko;NOM", # [patSinko], ch:tS, i:i,n:n
    "samurai;NOM", # [samuRaj], u:u
    "seppuku;NOM", # [sepuku], u:u
    "sepuku;NOM", # [sepuku], u:u
    "shamisen;NOM", # [SamizEn], u:u
    "shiatsu;NOM", # [Sjatsu], u:u
    "shintô;NOM", # [Sinto], i:i,n:n
    "shogun;NOM", # [Sogun], u:u,n:n
    "shôgun;NOM", # [Sogun], u:u,n:n
    "shogunat;NOM", # [SOguna], u:u
    "sushi;NOM", # [suSi], u:u
    "sutémi;NOM", # [sutemi], u:u
    "tamagotchi;NOM", # [tamagotSi]
    "tsunami;NOM", # [tsynami]
    "yakitori;NOM", # [jakitoRi]
    "yakusa;NOM", # [jakuza], u:u
    "yakuza;NOM", # [jakuza], u:u
    "yen;NOM", # [jEn], e:E,n:n$
    "zaibatsu;NOM", # [zajbatsu], u:u
    "zen;ADJ", # [zEn]
    "zen;NOM", # [zEn]
  },

#
# autres langues:
# TODO: isoler le grec ancien et les langues chinoises
#

  "autre" : {
    "banjo;NOM", # [b@dZo], j:dZ
    "bantu;NOM", # [b@tu], u:u
    "banyan;NOM", # [banj@]
    "bécabunga;NOM", # [bekab§ga], un:§
    "bodhisattva;NOM", # [bodisatva]
    "burundais;NOM", # [buRundE], u:u
    "chapska;NOM",  # [Sapska]
    "chantoung;NOM", # [S@tuG], ng:G$
    "!chinook;NOM",
    "!chorten;NOM", # [SORt°n], e:°,n:n
    "cromlech;NOM", # [kROmlEk], ch:k
    "çruti;NOM", # [sRyti], ç:s,r:R
    "!dông;NOM",
    "dum-dum;ADJ", # [dumdum], u:u
    "éwé;ADJ", # [eve], w:v
    "éwé;NOM", # [eve], w:v
    "forint;NOM", # [foRint], i:i,n:n
    "!fung;ADJ", # [fuG], u:u
    "gan;NOM", # [gan], a:a,n:n
    "gymkhana;NOM", # [Zimkana], i:i,m:m
    "ginkgo;NOM", # [Z5ko], kg:k
    "ginseng;NOM", # [Zins@g], i:i,n:n, g:g$
    "!guru;NOM", # [guRu], u:u
    "gut;NOM", # [gyt], t:t$
    "gutta-percha;NOM", # [gytapERka], ch:k
    "guzla;NOM", # [gyzla]
    "haggis;NOM", # [agis], gg:g
    "haka;NOM", # [aka]
    "hakka;NOM", # [aka], kk:k
    "han;ADJ", # [an], a:a,n:n$
    "han;NOM", # [an], a:a,n:n$
    "harari;NOM", # [aRaRi]
    "harmattan;NOM", # [aRmat@]
    "hetman;NOM", # [Etm@]
    "hévéa;NOM", # [evea]
    "hindi;NOM", # [indi], i:i,n:n
    "hopi;ADJ", # [opi]
    "hopi;NOM", # [opi]
    "hospodar;NOM", # [OspodaR]
    "housard;NOM", # [uzaR]
    "huipil;NOM", # [8ipil]
    "hun;NOM", # [1]
    "ibo;ADJ", # [ibo]
    "iboga;NOM", # [iboga]
    "jinjin;NOM", # [dZindZin], j:dZ, i:i,n:n
    "hong kong;NOM", # [§gk§g]
    "hongkongais;NOM", # [§gk§gE]
    "kala-azar;NOM", # [kalaazaR]
    "kino;NOM", # [kino]
    "katchina;NOM", # [katSina]
    "kelvin;NOM", # [kElvin], i:i,n:n
    "kirghiz;NOM", # [kiRgiz]
    "kirghize;ADJ", # [kiRgiz]
    "kirghize;NOM", # [kiRgiz]
    "kiva;NOM", # [kiva]
    "kiwi;NOM", # [kiwi]
    "khmer;ADJ", # [kmER]
    "kodiak;NOM", # [kodjak]
    "kondo;NOM", # [k§do]
    "kopeck;NOM", # [kopEk]
    "kopek;NOM", # [kopEk]
    "kora;NOM", # [kORa]
    "koran;NOM", # [kOR@]
    "krak;NOM", # [krak]
    "kriss;NOM", # [kRis]
    "kula;NOM", # [kula], u:u
    "!kumquat;NOM",
    "!kung-fu;NOM", # [kuGfu], u:u
    "!kung-fu;NOM;kung fu", # [kuGfu], u:u
    "kuru;NOM", # [kyry]
    "kyrie;NOM", # [kiRje], e:e
    "kyrie eleison;NOM", # [kiRjeeleisOn], e:e, e:e,i:i, s:s, o:O,n:n$
    "letchi;NOM", # [lEtSi]
    "limbo;NOM", # [limbo], i:i,m:m
    "linga;NOM", # [linga], i:i,n:n
    "lingala;NOM", # [lingala], i:i,n:n
    "lingam;NOM", # [lingam], i:i,n:n
    "luffa;NOM", # [lufa]
    "magyar;ADJ", # [magjaR], g:g,y:j
    "magyar;NOM", # [magjaR], g:g,y:j
    "ma-jong;NOM", # [maZ§g], g:g$
    "mah-jong;NOM", # [maZ§g], g:g$
    "maharadja;NOM", # [maaRadZa]
    "maharadjah;NOM", # [maaRadZa]
    "maharaja;NOM", # [maaRadZa], j:dZ
    "maharajah;NOM", # [maaRadZa], j:dZ
    "mahatma;NOM", # [maatma]
    "marae;NOM", # [maRae]
    "marimba;NOM", # [maRimba], i:i,m:m
    "mazdéen;ADJ", # [mazde5]
    "mazurka;NOM", # [mazyRka]
    "menhaden;NOM", # [mEnadEn]
    "mezcal;NOM", # [mezkal]
    "min;NOM", # [min], i:i,n:n
    "milonga;NOM", # [mil§ga]
    "moonisme;NOM", # [munizm], oo:u
    "mooniste;NOM", # [munizm], oo:u
    "mooré;NOM", # [moRe], oo:o
    "!mungo;NOM", # [muGgo], u:u,ng:Gg
    "oenothera;NOM", # [enoteRa], oe:e, e:e (grec)
    "pendjabi;NOM", # [pEndZabi], e:E,n:n
    "potlatch;NOM", # [potlatS]
    "pschent;NOM", # [pskEnt]
    "quetzal;NOM", # [kEtzal]
    "!quipu;NOM", # [kipu], u:u
    "reggae;NOM", # [Rege], ae:e, gg:g
    "sampang;NOM", # [s@p@g], g:g$
    "!shantung;NOM", # [S@tuG], u:u
    "sherpa;NOM",  # [SERpa], sh:S
    "sherpa;NOM", # [SERpa], sh:S
    "shilom;NOM", # [SilOm], sh:S
    "shoshone;NOM", # [SoSon], sh:S
    "!squaw;NOM", # [skwo]
    "!sutra;NOM", # [sutRa], u:u
    "tai-chi;NOM", # [tajtSi], ch:tS
    "tandoori;NOM", # [t@doRi], oo:o
    "tchetnik;NOM", # [tSEtnik]
    "tchin tchin;ONO", # [tSintSin], i:i,n:n
    "tchin-tchin;ONO", # [tSintSin], i:i,n:n
    "tokay;NOM", # [tokaj]
    "!tomahawk;NOM", # [tomaok]
    "trochanter;NOM", # [tROk@tER] (grec)
    "tsuica;NOM", # [tsujka], u:u,i:j
    "!umbanda;NOM", # [umb@da], u:u
    "wallaby;NOM", # [walabi]
    "wapiti;NOM", # [wapiti]
    "wombat;NOM", # [w§ba]
    "wigwam;NOM", # [wigwam]
    "xiang;NOM", # [gzj@g], g:g$
    "yack;NOM", # [jak]
    "yak;NOM", # [jak]
    "yama;NOM", # [jama]
    "yang;NOM", # [j@g], g:g$
    "yin;NOM", # [jin], i:i,n:n
    "ylang-ylang;NOM", # [il@gil@g], g:g$
    "yoga;NOM", # [joga]
    "yogi;NOM", # [jogi], g:g
    "yu;NOM;", # [ju], u:u
    "yue;NOM", # [jue], u:u, e:e
    "yuan;NOM", # [jyan], a:a,n:n
    "yucca;NOM", # [juka], u:u
    "zénana;NOM", # [zenana]
    "ziggourat;NOM", # [ziguRat], gg:g, t:t$
    "zloty;NOM", # [zloti]
    "zombi;NOM", # [z§bi]
    "zombie;NOM", # [z§bi]
    "zombification;NOM", # [z§bifikasj§]
    "zouk;NOM", # [zuk]
    "zydeco;NOM", # [zideko]
  # mots bien français mais "ing" ~ [iG]:
    "bing;ONO", # [biG], i:i,ng:G ##
    "ding;ONO", # [diG], i:i,ng:G ##
    "dring;ONO", # [dRiG], i:i,ng:G ##
    "ming;ADJ", # [miG], i:i,ng:G
    "ping-pong;NOM", # [piGp§g], i:i,ng:G, g:g$ ##
    "viking;ADJ", # [vikiG], i:i,ng:G ##
    "viking;NOM", # [vikiG], i:i,ng:G ##
  },

}
