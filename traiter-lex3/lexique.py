import dataclasses as dc
from collections import defaultdict

# on a jeté le champ "islem" de la base Lexique3,
# à la place on code cette info dans le champ "lemme"
# (ce champ est vide si le mot lui-même est un lemme):
def est_lemme(mot):
    return mot["lemme"] == "" or mot["lemme"] == mot["ortho"]

# Façon unique d’identifier des mots et des lemmes
# par une chaine de caractères:
#
# avec les données de Lexique3, les 3 données suivantes suffisent à identifier
# un mot: la graphie du lemme, la catégorie grammaticale, la graphie du mot.
#
# On ordonne les champs dans cet ordre (lemme, cgram, ortho) afin qu’un tri
# alphabétique sur les ID mette ensemble les flexions d’un même lemme. De plus,
# l’ID du lemme (voir ci-dessous) est alors une sous-chaine de l’ID du mot.
# Enfin, l’orthographe du mot est laissée vide quand elle est égale à celle du
# lemme (ATTENTION: dans le contenu de la base de données, c’est le contraire:
# le lemme est laissé vide quand il est égal à l’orthographe).
def composer_identifiant_mot(lem, cgram, ortho):
    if lem == "" or lem == ortho:
        lem = ortho
        ortho = ""
    #return (lem, cgram, ortho)
    return f'{lem};{cgram};{ortho}'

def decomposer_identifiant_mot(id_mot):
    parties = id_mot.split(";")
    assert len(parties) == 3
    (lem, cgram, ortho) = parties
    if ortho == "" or ortho == lem:
        ortho = lem
        lem = ""
    return (lem, cgram, ortho)

def identifiant_mot(mot):
    return composer_identifiant_mot(mot["lemme"], mot["cgram"], mot["ortho"])

# avec les données de Lexique3, les 2 données suivantes suffisent à identifier
# un lemme: la graphie du lemme, la catégorie grammaticale.
def identifiant_lemme(mot):
    cgram = mot["cgram"]
    if est_lemme(mot):
        lem = mot["ortho"]
    else:
        lem = mot["lemme"]
    #return (lem, cgram)
    return f'{lem};{cgram}'

def est_identifiant_lemme(id_mot_ou_lem):
    n = id_mot_ou_lem.count(";")
    assert n == 1 or n == 2
    return n == 1

# Quelques dataclasses pour structurer nos données:
@dc.dataclass(frozen=True)
class MotOuLemme:
    pass

@dc.dataclass(frozen=True)
class Mot(MotOuLemme):
    # un «mot» est une entrée de la base de données,
    # représentée par un dictionnaire Python:
    mot : dict

@dc.dataclass(frozen=True)
class Lemme(MotOuLemme):
    # un «lemme» est la donnée de l’ID de lemme
    # et d’un ensemble de mots qui sont ses flexions:
    id_lemme : str
    flexions : list[dict]

@dc.dataclass(frozen=True)
class MotInconnu(Exception):
    id_mot_ou_lem : str

@dc.dataclass
class GenreNombre:
    """
    Petite classe qui représente la donnée du genre et du nombre,
    et l’affiche sous une forme compacte (p.ex. "m", "s", ou "fp").
    """
    genre : str # "" ou "m" ou "f"
    nombre : str # "" ou "s" ou "p"
    # personnalise l’affichage avec une syntaxe compacte:
    def __repr__(self):
        return f'"{self.__str__()}"'
    def __str__(self):
        return self.genre + self.nombre
    # réciproquement, parse la syntaxe compacte:
    @staticmethod
    def parse(s):
        g = ""
        n = ""
        match len(s):
          case 0: pass
          case 1:
            if s in [ "m", "f" ]:
                g = s
            else:
                n = s
          case 2:
            g = s[0]
            n = s[1]
          case _:
            assert False
        assert g in [ "", "m", "f" ]
        assert n in [ "", "s", "p" ]
        return GenreNombre(g, n)

# Fonctions pour traiter les informations de verbes:
def normaliser_infover(infover):
    infover = set(infover.split(";"))
    infover.discard("")
    return ";".join(sorted(infover)) + ";"

def fusion_infover(infover1, infover2):
    return normaliser_infover(infover1 + infover2)

def infover_contient(infover, elt):
    return infover.startswith(elt+";") or ";"+elt+";" in infover

# Dans la base de données, les fréquences sont des nombres décimaux avec deux
# chiffres après la virgule (par exemple "123.40"). Ici, on les représente par
# un entier (12340) pour éviter les erreurs d’arrondis dues aux flottants.
def parser_freq(freq_string):
    assert len(freq_string) >= 3 and freq_string[-3] == "."
    return int(freq_string[:-3] + freq_string[-2:])

def ecrire_freq(freq):
    return f'{freq // 100}.{(freq // 10) % 10}{freq % 10}'

def addition_freq(f1, f2):
    return ecrire_freq(parser_freq(f1) + parser_freq(f2))

def soustraction_freq(f1, f2):
    return ecrire_freq(parser_freq(f1) + parser_freq(f2))

# La classe Lexique représente une base de données de mots en mémoire.
class Lexique:

    # liste des mots dans l’ordre d’entrée
    liste : list[dict] = [ ]
    # dictionnaire (lemme,cgram,ortho) → mot:
    par_id : dict[str, dict] = { }
    # dictionnaire (lemme,cgram) → [ liste de mots ]:
    par_lemme : dict[str, list[dict]] = defaultdict(list)

    # Enregistre un nouveau mot dans la base de données
    # en supposant que toutes les données sont déjà cohérentes
    # avec le reste de la base (les fréquences du mot sont déjà comptabilisées)
    def enregistrer_mot(self, mot):
        id_mot = identifiant_mot(mot)
        assert id_mot not in self.par_id
        self.liste.append(mot)
        self.par_id[id_mot] = mot
        id_lem = identifiant_lemme(mot)
        self.par_lemme[id_lem].append(mot)

    # Méthode interne qui facilite d’appeler la plupart des méthodes de cette
    # classe soit avec un ID de lemme ou de mot, soit directement avec un mot,
    # en vérifiant à chaque fois que le mot/lemme existe dnas la base:
    def _mot_ou_lemme(self, mot_ou_id):
        # si l’argument est un ID:
        if isinstance(mot_ou_id, str):
          try:
            # si c’est un ID de lemme, on renvoie toutes ses flexions:
            if est_identifiant_lemme(mot_ou_id):
                return Lemme(mot_ou_id, self.par_lemme[mot_ou_id])
            # sinon, c’est un ID de mot, on renvoie juste le mot correspondant:
            else:
                return Mot(self.par_id[mot_ou_id])
          except KeyError:
            raise MotInconnu(mot_ou_id)
        # sinon, c’est directement un mot:
        else:
            assert isinstance(mot_ou_id, dict)
            if mot_ou_id not in self.liste:
                raise MotInconnu(identifiant_mot(mot_ou_id))
            return Mot(mot_ou_id)

    # Idem quand on accepte un ID de mot mais pas un ID de lemme:
    def _mot(self, mot_ou_id):
        match self._mot_ou_lemme(mot_ou_id):
          case Lemme():
            assert False
          case Mot(mot):
            return mot

    # Méthodes internes pour maintenir à jour les fréquences dans la base:
    def _soustraire_de_flexions(self, mot, flexions):
        freq_livres = mot["freqlivres"]
        freq_films2 = mot["freqfilms2"]
        for flexion in flexions:
            flexion["freqlemlivres"] = soustraction_freq(flexion["freqlemlivres"], freq_livres)
            flexion["freqlemfilms2"] = soustraction_freq(flexion["freqlemfilms2"], freq_films2)

    def _additionner_a_flexions(self, mot, flexions):
        freq_livres = mot["freqlivres"]
        freq_films2 = mot["freqfilms2"]
        for flexion in flexions:
            flexion["freqlemlivres"] = addition_freq(flexion["freqlemlivres"], freq_livres)
            flexion["freqlemfilms2"] = addition_freq(flexion["freqlemfilms2"], freq_films2)

    def _additionner_a_mot(self, mot, mot2):
        freq_livres = mot["freqlivres"]
        freq_films2 = mot["freqfilms2"]
        mot2["freqlivres"] = addition_freq(mot2["freqlivres"], freq_livres)
        mot2["freqfilms2"] = addition_freq(mot2["freqfilms2"], freq_films2)

    # Méthode interne qui supprime un mot
    # sans mettre à jour la table `par_lemme` ni les fréquences
    def _supprimer_mot(self, mot, garder_entree=False):
        id_mot = identifiant_mot(mot)
        assert id_mot in self.par_id and mot in self.liste
        if not garder_entree:
            self.liste.remove(mot)
        del self.par_id[id_mot]

    def _supprimer_lemme(self, id_lem, flexions, **kwargs):
        # supprime toutes les flexions du lemme, incluant le mot-lemme lui-même:
        for flexion in self.par_lemme[id_lem]:
            self._supprimer_mot(flexion, **kwargs)
        del self.par_lemme[id_lem]

    def _supprimer_flexion(self, mot, **kwargs):
        self._supprimer_mot(mot, **kwargs)
        id_lem = identifiant_lemme(mot)
        # supprime juste cette flexion du lemme:
        flexions = self.par_lemme[id_lem]
        flexions.remove(mot)
        # met à jour les fréquences du lemme:
        if flexions:
            self._soustraire_de_flexions(mot, flexions)
        else:
            del self.par_lemme[id_lem]

    # Méthodes publiques pour modifier les mots de la base.
    # Ces méthodes prennent en argument des mots ou des ID,
    # et retournent un booléen indiquant si elles ont réussi.
    # Elles maintiennent à jour les fréquences.

    # Si l’argument est un mot ou un ID de mot, supprime ce mot de la base.
    # Si l’argument est un ID de lemme, supprime toutes les flexions du lemme.
    def supprimer(self, mot_ou_id):
      try:
        match self._mot_ou_lemme(mot_ou_id):
          case Lemme(id_lem, flexions):
            self._supprimer_lemme(id_lem, flexions)
          case Mot(mot):
            self._supprimer_flexion(mot)
        return True
      except MotInconnu:
        return False

    # Fusionne le 1er mot dans le 2nd (utile p.ex. si le 1er mot a une mauvaise
    # graphie, ou une mauvaise catégorie grammaticale, et que l’entrée correcte
    # existe déjà).
    # Les paramètres `garder_gn` et `garder_infover` indiquent s’il faut ajouter
    # au 2nd mot les informations de genre/nombre et de formes verbales du 1er,
    # ou bien les jeter.
    def fusionner_dans(self, mot_ou_id1, mot_ou_id2, garder_gn=True, garder_infover=True):
      try:
        mot1 = self._mot(mot_ou_id1)
        mot2 = self._mot(mot_ou_id2)
        id_lem2 = identifiant_lemme(mot2)
        # enleve le 1er mot de la base:
        self._supprimer_flexion(mot1)
        # ajoute les fréquences du 1er mot:
        self._additionner_a_flexions(mot1, self.par_lemme[id_lem2])
        self._additionner_a_mot(mot1, mot2)
        # ajoute les infos de genre et de nombre du 1er mot:
        if garder_gn:
            mot2["gn"].genre = mot2["gn"].genre or mot1["gn"].genre
            mot2["gn"].nombre = mot2["gn"].nombre or mot1["gn"].nombre
        # ajoute les infos de formes verbales du 1er mot:
        if garder_infover and mot2["cgram"] in ["AUX", "VER"]:
            mot2["infover"] = fusion_infover(mot2["infover"], mot1["infover"])
        return True
      except MotInconnu:
        return False

    # Change l’orthographe du mot, l’orthographe de son lemme et/ou sa catégorie
    # grammaticale.
    # Si on demande seulement à changer l’orthographe du mot mais que c’est un
    # lemme, alors le lemme est changé aussi (en revanche, les autres flexions
    # pointeront toujours vers l’ancien lemme).
    # Si un mot identique au résultat existe déjà, fusionne dans ce mot existant.
    def _changer_mot(self, mot_ou_id,
        nouvelle_ortho=None, nouveau_lem=None, nouveau_cgram=None, **kwargs
    ):
      try:
        mot = self._mot(mot_ou_id)
        ancien_id_mot = identifiant_mot(mot)
        ancien_id_lem = identifiant_lemme(mot)
        # simule les changements sans les effectuer réellement
        # (car pour supprimer l’ancien mot, on appelle des méthodes
        # comme `fusionner_dans` ou `_supprimer_flexion` qui ont besoin
        # de l’ancien ID):
        tmp = mot.copy()
        if nouveau_cgram != None:   tmp["cgram"] = nouveau_cgram
        if nouvelle_ortho != None:  tmp["ortho"] = nouvelle_ortho
            # si le mot d’origine est un lemme, alors son champ "lemme" est vide
            # donc on a aussi modifié l’orthographe du lemme (implicitement).
        if nouveau_lem != None:
            if nouveau_lem == tmp["ortho"]:
                nouveau_lem = ""
            tmp["lemme"] = nouveau_lem
        nouvel_id_mot = identifiant_mot(tmp)
        nouvel_id_lem = identifiant_lemme(tmp)
        # si l’ID est inchangé, alors rien n’a changé,
        # ce qui est sans doute une erreur:
        if nouvel_id_mot == ancien_id_mot:
            return False
        # si le mot cible existe déjà, on fusionne:
        elif nouvel_id_mot in self.par_id:
            return self.fusionner_dans(mot_ou_id, nouvel_id_mot, **kwargs)
        # sinon, si le lemme est inchangé, on doit juste mettre à jour
        # la table `par_id` (seule l’orthographe du mot a changé):
        elif nouvel_id_lem == ancien_id_lem:
            del self.par_id[ancien_id_mot]
            self.par_id[nouvel_id_mot] = mot
            mot.update(tmp)
            return True
        # sinon, le lemme a changé, on doit aussi mettre à jour
        # l’ancien et le nouveau lemme:
        else:
            # détache le mot de l’ancien lemme:
            self._supprimer_flexion(mot, garder_entree=True)
            # modifie le mot:
            mot.update(tmp)
            # met à jour les fréquences du nouveau lemme:
            flexions = self.par_lemme[nouvel_id_lem]
            if len(flexions):
                self._additionner_a_flexions(mot, flexions)
                mot["freqlemlivres"] = flexions[0]["freqlemlivres"]
                mot["freqlemfilms2"] = flexions[0]["freqlemfilms2"]
            else:
                mot["freqlemlivres"] = mot["freqlivres"]
                mot["freqlemfilms2"] = mot["freqfilms2"]
            # met à jour les infos de genre et de nombre:
            if not kwargs.get("garder_gn", True):
                mot["gn"].genre = ""
                mot["gn"].nombre = ""
            # met à jour les infos de formes verbales:
            if not kwargs.get("garder_infover", True) \
              or mot["cgram"] not in ["AUX", "VER"]:
                mot["infover"] = ""
            # attache le mot au nouveau lemme:
            self.par_id[nouvel_id_mot] = mot
            flexions.append(mot)
            return True
      except MotInconnu:
        return False

    # Comme `_changer_ortho` mais les nouvelles valeurs sont données
    # sous la forme du nouvel ID du mot.
    # Si le mot cible existe déjà, fusionne dans le mot existant.
    def changer_mot(self, mot_ou_id, nouvel_id_mot, **kwargs):
        (lem, cgram, ortho) = decomposer_identifiant_mot(nouvel_id_mot)
        return self._changer_mot(mot_ou_id,
            nouvelle_ortho=ortho, nouveau_lem=lem, nouveau_cgram=cgram,
            **kwargs)

    # Change l’orthographe du mot.
    # Même remarque que `_changer_mot` concernant l’orthographe du lemme.
    # Si le mot cible existe déjà, fusionne dans le mot existant.
    def changer_ortho(self, mot_ou_id, nouvelle_ortho, **kwargs):
        return self._changer_mot(mot_ou_id,
            nouvelle_ortho=nouvelle_ortho, nouveau_lem=None, nouveau_cgram=None,
            **kwargs)

    # Change l’orthographe du lemme du mot.
    # Si le mot cible existe déjà, fusionne dans le mot existant.
    def changer_lemme(self, mot_ou_id, nouveau_lem, **kwargs):
        #return self._changer_mot(mot_ou_id, nouveau_lem=nouveau_lem, **kwargs)
        return self._changer_mot(mot_ou_id,
            nouvelle_ortho=None, nouveau_lem=nouveau_lem, nouveau_cgram=None,
            **kwargs)

    def lemmifier(self, mot_ou_id, **kwargs):
        return self.changer_lemme(mot_ou_id, "", **kwargs)

    # Change la catégorie grammaticale du mot.
    # Si le mot cible existe déjà, fusionne dans le mot existant.
    def changer_cgram(self, mot_ou_id, nouveau_cgram, **kwargs):
        #return self._changer_mot(mot_ou_id, nouveau_cgram=nouveau_cgram, **kwargs)
        return self._changer_mot(mot_ou_id,
            nouvelle_ortho=None, nouveau_lem=None, nouveau_cgram=nouveau_cgram,
            **kwargs)
