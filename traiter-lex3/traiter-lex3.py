#!/usr/bin/python

import csv
from collections import defaultdict

###
### NOS PROPRES MODULES
###

from alphabet import *
import lexique
import graphemisation
import syllabation
import lex3_patchs
import lex3_barbarismes

barbarismes = { }
for (lg, mots_lg) in lex3_barbarismes.barbarismes_par_langue.items():
    lg_bang = lg + "!"
    for mot in mots_lg:
        if mot[0] == "!":
            barbarismes[mot[1:]] = lg_bang
        else:
            barbarismes[mot] = lg

###
### DESCRIPTION DE L’ENTRÉE (LEXIQUE3) ET DE LA SORTIE
###

categories_grammaticales = [
  "ADJ",
  "ADJ:dem",
  "ADJ:ind",
  "ADJ:int",
  "ADJ:num",
  "ADJ:pos",
  "ADV",
  "ART:def",
  "ART:ind",
  "AUX",
  "CON",
  "LIA",
  "NOM",
  "ONO",
  "PRE",
  "PRO:dem",
  "PRO:ind",
  "PRO:int",
  "PRO:per",
  "PRO:pos",
  "PRO:rel",
  "VER",
]

# CHAMPS DE LEXIQUE 3.83
#
# (- = redondant;
#  ~ = redondant si on lit la base dans son ensemble)
#
#   orthographe:
#     01. [ortho] = orthographe
#   lemme:
#     14. [islem] = "0" | "1"
#     03. [lemme] = ortho du lemme
#   identification grammaticale:
#     04. [cgram] = "NOM|ADJ|VER|AUX|ADV|..."
#     05. [genre] = "" | "m" | "p"
#     06. [nombre] = "" | "s" | "p"
#     11. [infover] = "imp:pre:2p;ind:pre:2p;" p.ex.
#   ~ 29. [cgramortho] = liste de tous les cgram de tous les homographes
#   phonologie:
#   - 02. [phon] = phonologie
#     28. [orthosyll] = découpage de l’orthographe en syllabes (tirets, conserve les apostrophes, espaces et tirets)
#     23. [syll] = découpage de la phonologie en syllabes (tirets)
#   consonnes et voyelles:
#   - 17. [cvcv] = consonnes/voyelles de l’orthographe (conserve les apostrophes, espaces et tirets)
#   - 18. [p_cvcv] = consonnes/voyelles de la phonologie (aucun séparateur)
#   - 25. [cv-cv] = consonnes/voyelles de la phonologie syllabée (tirets)
#   morphèmes (données très lacunaire):
#     34. [morphoder] = morphèmes séparés par des tirets
#   mots voisins:
#     12. [nbhomogr] = nb d’homographes
#     13. [nbhomoph] = nb d’homophones
#     19. [voisorth] = nb de voisins (= à distance d’édition 1) orthographiques
#     20. [voisphon] = nb de voisins (= à distance d’édition 1) phonologiques
#     32. [old20] = distance d’édition moyenne aux 20 mots les plus proches, orthographiquement
#     33. [pld20] = distance d’édition moyenne aux 20 mots les plus proches, phonologiquement
#   comptages divers:
#   - 15. [nblettres] = nb de CARACTÈRES dans ortho (y compris apostrophes, espaces et tirets)
#   - 16. [nbphons] = nb de phonèmes dans phon
#   - 24. [nbsyll] = nb de syllabes dans syll et cv-cv
#   - 35. [nbmorph] = nb de morphèmes dans morphoder
#   ~ 21. [puorth] = (lemme seulement) point d’unicité orthographique
#   ~ 22. [puphon] = (lemme seulement) point d’unicité phonologique
#   miroir:
#   - 26. [orthrenv] = miroir de ortho
#   - 27. [phonrenv] = miroir de phon
#   fréquences:
#     08. [freqlemlivres]
#     07. [freqlemfilms2]
#     10. [freqlivres]
#     09. [freqfilms2]
#     30. [deflem] = pourcentage de personnes déclarant connaître le mot, parmi defobs
#     31. [defobs] = pourcentage de personnes répondant à la question

def verifie_invariants_mot(mot):
    """
    Vérifie certains invariants sur une entrée de Lexique3
    (garantit notamment que certaines infos sont redondantes).
    """
    assert mot["islem"] in ["0", "1"]
    if mot["islem"] == "1":
        assert mot["ortho"] == mot["lemme"]
    else:
        assert mot["lemme"] != ""
    assert all(ord(a) in alphabet_ortho or a in ponctuation for a in mot["ortho"])
    assert all(ord(a) in alphabet_phono for a in mot["phon"])
    assert mot["genre"] in ["", "m", "f"]
    assert mot["nombre"] in ["", "s", "p"]
    assert mot["orthosyll"] == "" \
        or mot["ortho"].replace("-", "") == mot["orthosyll"].replace("-", "")
    assert mot["phon"] == mot["syll"].replace("-", "")
    assert mot["p_cvcv"] == mot["cv-cv"].replace("-", "")
    assert mot["cvcv"] == cvcv_ortho(mot["ortho"])
    assert mot["cv-cv"] == cvcv_phono(mot["syll"])
    assert int(mot["nblettres"]) == len(mot["ortho"])
    assert int(mot["nbphons"]) == len(mot["phon"])
    nb_syll = int(mot["nbsyll"])
    assert nb_syll == len(mot["syll"].split("-"))
    assert nb_syll == len(mot["cv-cv"].split("-"))
    #assert mot["orthosyll"] == "" \
    #    or nb_syll == len(mot["orthosyll"].replace(" ", "-").split("-"))
    assert int(mot["nbmorph"]) == len(mot["morphoder"].split("-"))
    assert mot["orthrenv"] == mot["ortho"][::-1]
    assert mot["phonrenv"] == mot["phon"][::-1]
    assert mot["cgram"] in categories_grammaticales or mot["cgram"] == ""
    assert (mot["cgram"] in ["AUX", "VER"]) == (mot["infover"] != "") or print(mot)
    #if mot["cgram"] in ["AUX", "VER"]:
    #    if lexique.infover_contient(mot["infover"], "inf"):
    #        assert mot["islem"] == "1"
    #    else:
    #        assert mot["islem"] == "0"

# En sortie on ne conserve que certains champs de Lexique3, ceux qui nous
# intéressent et qui ne sont pas redondants. On y ajoute certaines informations
# de notre cru (graphèmes, langue).

champs_sortie = [
  # orthographe:
  "ortho",
  #"orthosyll", # avec syllabes => incomplet et pas vérifié
  # phonologie:
  #"phon", # => subsumé par "syll" / "syll2"
  #"syll", # avec syllabes
  # nouvelle syllabation (synthétisée par nous):
  "syll2",
  # correspondance orthographe--phonologie (synthétisée par nous):
  "graphemes",
  # «langue» (indique si le mot suit un système orthographique autre que français):
  "lang",
  # lemme (vide si le mot lui-même est un lemme):
  "lemme",
  # informations grammaticales (identification du mot):
  "cgram", "gn", "infover",
  # fréquences:
  "freqlivres", "freqlemlivres",
  "freqfilms2", "freqlemfilms2",
]

###
### DONNÉES GLOBALES CALCULÉES PAR LE PROGRAMME
###

# la base de mots en mémoire:
lex = lexique.Lexique()

# le programme calcule des statistiques sur les graphèmes rencontrés:
def new_stat():
    def new_stat_lang():
        return { "lemmes": set(), "mots": set(), "freq1": 0.0, "freq2": 0.0 }
    return (new_stat_lang(), new_stat_lang())

# dictionnaire (graphème,phonème) → (stats mots français, stats tous mots):
stats_graphemes = defaultdict(new_stat)

def ajouter_grapheme_stats(ortho, phono, mot):
    def ajouter_grapheme_stat_lang(stat_lang, mot):
        stat_lang["lemmes"].add(mot["lemme"] or mot["ortho"])
        stat_lang["mots"].add(mot["ortho"])
        stat_lang["freq1"] += float(mot["freqlivres"])
        stat_lang["freq2"] += float(mot["freqfilms2"])
    po = (phono, ortho)
    (stat_fr, stat_tous) = stats_graphemes[po]
    ajouter_grapheme_stat_lang(stat_tous, mot)
    if "lang" not in mot:
        ajouter_grapheme_stat_lang(stat_fr, mot)

# ensemble des séquences de consonnes rencontrées
# (info utile pour mettre au point l’algo de syllabation):
sequences_consonantiques = set()

def ajouter_consonnes(mot):
    seq = ""
    for c in mot["phon"] + "a":
        if alphabet_phono[ord(c)] == "C":
            seq += c
        elif seq != "":
            sequences_consonantiques.add(seq)
            seq = ""

###
### TRAITEMENT DE LEXIQUE3
###

verbose = True

print("")
print("#")
print("# TRAITEMENT DE LEXIQUE3…")
print("#")
print("")

# PASSE 1:
# lecture du tableur d’entrée

with open("Lexique383.tsv", "r") as fichier_entree:
    # l’entrée et la sortie sont des tableurs TSV:
    tableur_entree = csv.DictReader(fichier_entree, delimiter="\t")
    # pour chaque mot du lexique…
    for mot in tableur_entree:
        #
        # on vérifie des invariants
        # (pour être sûr que les infos qu’on jette sont redondantes):
        #
        verifie_invariants_mot(mot)
        # on va jeter le champ "islem" et coder l’info qu’il donne dans "lemme":
        if mot["islem"] == "1":
            mot["lemme"] = ""
        # on fusionne les champs "genre" et "nombre" pour gagner en compacité:
        mot["gn"] = lexique.GenreNombre(mot["genre"], mot["nombre"])
        del mot["genre"]
        del mot["nombre"]
        # le champ "infover" contient souvent des doublons et des entrées vides,
        # on le simplifie, et on le trie (pour faciliter les diffs de fichiers):
        if mot["infover"] != "":
            mot["infover"] = lexique.normaliser_infover(mot["infover"])
        #
        # on ajoute le mot dans notre base en mémoire:
        #
        lex.enregistrer_mot(mot)

# PASSE 2:
# application des correctifs
#
# NOTE: comme ces correctifs peuvent changer la structure de la base
# (suppression de mots, relations lemme/flexion, fréquences), il est
# nécessaire d’avoir construit en mémoire la base de données entière.

lex3_patchs.appliquer_correctifs(lex, verbose=verbose)

# PASSE 3:
# étiquetage des barbarismes
# (càd ajout d’un éventuel champ "lang")
#
# NOTE: les ID de barbarismes se réfèrent donc aux mots après correctifs.

barbarismes_pas_vus = set()

for id_barb, lang in barbarismes.items():
    no_match = True
    if lexique.est_identifiant_lemme(id_barb):
        if id_barb in lex.par_lemme:
            for flexion in lex.par_lemme[id_barb]:
                flexion["lang"] = lang
                no_match = False
    else:
        if id_barb in lex.par_id:
            lex.par_id[id_barb]["lang"] = lang
            no_match = False
    if no_match:
        barbarismes_pas_vus.add(id_barb)

if verbose:
    for id_barbarisme in sorted(barbarismes_pas_vus):
        print(f'ID de barbarisme ne correspondant à aucun mot: "{id_barbarisme}"')

# PASSE 4:
# graphémisation et syllabation

for mot in lex.liste:
    id_mot = lexique.identifiant_mot(mot)
    ortho = mot["ortho"]
    phono = mot["phon"]
    syll_ortho = mot["orthosyll"]
    syll_phono = mot["syll"]
    # pour les mots français réguliers, on voudrait vraiment obtenir une
    # correspondance et une seule, donc les avertissements pour ces mots
    # sont plus visibles:
    warn = '! '
    if "lang" in mot:
        if mot["lang"][-1] == "!":
            warn = f'({mot["lang"][:-1]}) '
        else:
            warn = f'! ({mot["lang"]}) '
    #
    # on re-calcule la syllabation avec notre algo:
    #
    syll_phono2 = syllabation.syllabation(phono)
    mot["syll2"] = syll_phono2
    if syll_phono != syll_phono2 and verbose:
        print(f'{warn}syllabation différente: "{id_mot}" [{syll_phono}] ≠ [{syll_phono2}]')
    #
    # on calcule les correspondances orthographe--phonologie
    # (dans l’idéal il ne devrait y en avoir qu’une):
    #
    corresps = list(graphemisation.corresps_ortho_phono(ortho, phono))
    graphemes = None
    if len(corresps) == 0:
        if verbose:
            print(f'{warn}pas de graphémisation: "{id_mot}" [{phono}]')
    elif len(corresps) > 1:
        if verbose:
            print(f'{warn}plusieurs graphémisations: "{id_mot}" [{phono}]')
            graphemisation.print_corresps(corresps)
    else:
        assert len(corresps) == 1
        graphemes = corresps[0]
        #
        # on intègre les données de syllabation:
        #
        graphemes.integre_syllabation(syll_ortho, syll_phono2)
        mot["graphemes"] = graphemes
    #
    # on calcule des stats globales:
    #
    # … sur les séquences de consonnes
    # (travail préliminaire pour la syllabation):
    #ajouter_consonnes(mot)
    # … sur les graphèmes utilisés:
    if graphemes != None:
        for (o,p) in graphemes:
            ajouter_grapheme_stats(o, p, mot)
    #
    # on détecte de potentiels barbarismes pas encore étiquetés,
    # avec quelques heuristiques:
    #
    if "lang" not in mot:
        # … mots en "-er" prononcés [-9R] ou [-ER]:
        if (ortho[-2:] == "er" or ortho[-3:] == "ers") \
            and phono[-1] != "e":
            print(f'barbarisme(en/de/nl)? "{id_mot}" [{phono}]')
        # … d’après certains graphèmes symptômatiques:
        if graphemes != None:
            str_graphemes = str(graphemes)
            s = ""
            for (op, re_op) in graphemisation.morphemes_barbares:
                if re_op.search(str_graphemes):
                    s += f', {op}'
            if s:
                print(f'barbarisme? "{id_mot}" [{phono}]{s}')
    #
    # on détecte des erreurs avec quelques heuristiques:
    #
    if mot["cgram"] in ["AUX", "VER"]:
        inf_in_infover = lexique.infover_contient(mot["infover"], "inf")
        # … verbes dont le champ "lemme" et le champ "infover" sont
        # contradictoires vis-à-vis de l’infinitif:
        if inf_in_infover != (mot["lemme"] == ""):
            print(f'! verbe lemme-conjugué ou flexion-infinitif: "{id_mot}" ({mot["infover"]})')
        # … verbes qui sont à la fois infinitfs et conjugués:
        if inf_in_infover and mot["infover"] != "inf;":
            print(f'! infinitif mal étiqueté: "{id_mot}" ({mot["infover"]})')
        # … infinitifs en "-er" prononcés incorrectement:
        if inf_in_infover and ortho[-2:] == "er" and phono[-1] != "e":
            print(f'! infinitif en -er mal prononcé: "{id_mot}" [{phono}]')

# PASSE 5:
# écriture du tableur de sortie

with open("Lexique383.traité.tsv", "w") as fichier_sortie:
    # l’entrée et la sortie sont des tableurs TSV:
    tableur_sortie = csv.DictWriter(fichier_sortie, delimiter="\t", \
      fieldnames=champs_sortie, extrasaction="ignore", restval=None)
    tableur_sortie.writeheader()
    # pour chaque mot du lexique…
    for mot in lex.liste:
        #
        # on écrit le mot dans le tableur de sortie:
        #
        tableur_sortie.writerow(mot)

###
### COMPTE-RENDU
###

print("")
print("#")
print("# GRAPHÈMES")
print("#")
print("")

def cle_tri_grapheme(grapheme):
    ((p,o), (stat_fr,stat_tous)) = grapheme
    return (alphabet_phono[ord(p[0])] if p else "", p.lower(), p, -len(stat_fr["lemmes"]), o)
stats_graphemes = list(stats_graphemes.items())
stats_graphemes.sort(key=cle_tri_grapheme)
p_precedent = ""
for ((p,o), (stat_fr,stat_tous)) in stats_graphemes:
    if p == "":
        p = "_"
    # mots français:
    lemmes_fr = stat_fr["lemmes"]
    n_fr = len(lemmes_fr)
    f1_fr = stat_fr["freq1"]
    f2_fr = stat_fr["freq2"]
    s = f'[{p}]:"{o}" : n = {n_fr}; f1 = {f1_fr:.3g}; f2 = {f2_fr:.3g}'
    if 1 <= n_fr and n_fr <= 30:
        s += f' ("{",".join(sorted(list(lemmes_fr)))}")'
    # tous les mots:
    lemmes_tous = stat_tous["lemmes"]
    n_tous = len(lemmes_tous)
    f1_tous = stat_tous["freq1"]
    f2_tous = stat_tous["freq2"]
    if n_tous > n_fr:
        s += f'  [n = {n_tous}; f1 = {f1_tous:.3g}; f2 = {f2_tous:.3g}'
        if n_tous - n_fr <= 30:
            s += f' ("{",".join(sorted(list(lemmes_tous - lemmes_fr)))}")'
        s += "]"
    if p_precedent != p:
        print()
    p_precedent = p
    print(s)

#print("")
#print("#")
#print("# SÉQUENCES DE CONSONNES")
#print("#")
#print("")
#
#for seq in sorted(list(sequences_consonantiques)):
#    print(seq)
