Ce dossier contient le programme `traiter-lex3.py` qui traite les données
d’orthographe et de prononciation de la base de données Lexique3.

À partir du tableur TSV d’origine (`Lexique383.tsv`), il produit un nouveau
tableur (`Lexique383.traité.tsv`), avec un mot par ligne, qui contient les
champs suivants :

+ `ortho` : orthographe du mot (comme Lexique3)
+ `syll2` : prononciation du mot, avec syllabes (la syllabation diffère de celle
  de Lexique3, cf plus loin)
+ `graphemes` : graphémisation du mot, également avec syllabes (cf plus loin)
+ `lang` : un étiquetage manuel de la langue d’origine du mot (cf plus loin)
+ `lemme` : orthographe du lemme, si différent du mot lui-même (comme Lexique3)
+ `cgram` : catégorie grammaticale (comme Lexique3)
+ `gn` : genre et nombre (comme Lexique3, mais combinés)
+ `infover` : pour une forme verbale conjuguée : liste des modes, temps et
  personnes auxquelles correspond cette graphie (comme Lexique3, mais nettoyé)
+ `freqlivres, freqlemlivres, freqfilms2, freqlemfilms2` :
  fréquence du mot et de son lemme dans divers corpus (comme Lexique3)

On jette les champs de Lexique3 qui ne nous intéressent pas, qui sont redondants
avec d’autres champs (par exemple les champs CVCV ou l’orthographe renversée),
ou qui sont trop pénibles à maintenir à travers les correctifs effectués.

Ce projet corrige également beaucoup d’erreurs trouvées dans Lexique3.

## Usage

Ce programme est codé en Python, il faut donc que Python soit installé sur la
machine.

En ligne de commande, une fois dans le dossier, s’assurer que le fichier
`Lexique383.tsv` est présent (et que, si c’est un lien symbolique, la cible du
lien existe également), puis lancer la commande suivante qui produit le tableur
résultat `Lexique383.traité.tsv` et consigne son fonctionnement dans le fichier
`log.txt` :

    ./traiter-lex3.py | tee log.txt

Par souci de praticité, le tableur résultat et le log associé sont sauvegardés
dans ce dossier: [Lexique383.traité.SAVE.tsv](./Lexique383.traité.SAVE.tsv) et
[log.SAVE.txt](./log.SAVE.txt)

## Syllabation

Ce programme calcule une nouvelle syllabation phonologique, avec un algorithme
simple, qui remplace la syllabation déjà présente dans Lexique3. Il affiche un
avertissement quand les deux syllabations diffèrent. La plupart du temps, la
nouvelle syllabation est meilleure (à mon avis). Exemples (à gauche l’ancienne
syllabation, à droite la nouvelle) :

    ! (en) syllabation différente: "grizzli;NOM;" [gRi-zli] ≠ [gRiz-li]
    ! syllabation différente: "infarctus;NOM;" [5-faR-ktys] ≠ [5-faRk-tys]
    ! syllabation différente: "mots-croisés;NOM;" [mok-Rwa-ze] ≠ [mo-kRwa-ze]
    ! syllabation différente: "parapluie;NOM;" [pa-Rap-l8i] ≠ [pa-Ra-pl8i]
    ! syllabation différente: "table-coiffeuse;NOM;" [tab-lkwa-f2z] ≠ [tabl-kwa-f2z]
    (en) syllabation différente: "time-sharing;NOM;" [taj-mSE-RiG] ≠ [tajm-SE-RiG]

Le code source se trouve dans **[syllabation.py](./syllabation.py)**.

Résumé de l’algorithme : si une consonne est suivie par une voyelle, alors la
consonne va avec cette voyelle. Quand une succession de plusieurs consonnes se
trouve entre deux voyelles, il faut décider où couper la syllabe. On le fait
avec un choix ad-hoc, en fonction de la séquence de consonnes. Par exemple,
`"st"` sera toujours coupée `"s-t"`, `"bl"` sera toujours coupée `"-bl"`. La
liste de ces choix se trouve dans le fichier source.

Attention : il n’y a pas de règle de coupure générique. Si, dans une nouvelle
version de Lexique3, apparaissent des séquences de sons consonantiques qui ne
sont pas déjà connues de l’algorithme, alors le programme entier plantera
brutalement. Il faut alors ajouter la nouvelle séquence aux règles de coupure
écrites en dur dans le code.

Bien que meilleur que l’existant, cet algorithme a deux limitations.

 1. Il déplace des consonnes vers la syllabe suivante à travers des `"e"` muets.

    Par exemple `"lutherie"` est syllabé `[ly-tRi]` mais on préférerait `[lyt-Ri]` ;
    `"monte-charge"` est syllabé `[m§-tSaRZ]` mais on préférerait `[m§t-SaRZ]`.
    Heureusement, il semble y avoir très peu d’instances problématiques dans la
    base et avec les choix ad-hoc actuels. De plus, grâce à la graphémisation,
    on peut maintenant détecter les `"e"` muets ; on pourrait donc les prendre
    en compte dans la syllabation, et ainsi régler ce problème.

 2. Il ignore totalement la morphologie des mots.

    Par exemple `"aide-jardinier"` est syllabé `[E-dZaR-di-nje]` au lieu de `[d-Z]` ;
    `"sublunaire"` est syllabé `[sy-bly-nER]` au lieu de `[b-l]` ;
    `"yougoslave"` est syllabé `[ju-gOs-lav]` au lieu de `[-sl]`.
    Ce problème est difficile à régler automatiquement sans données de
    morphologie. Toutefois, encore grâce à la graphémisation, on pourrait au
    moins prendre en compte les espaces et les tirets de la graphie dans la
    syllabation. De plus, les choix de découpage pour chaque séquence de
    consonnes ayant été faits de façon ad-hoc, en fonction des mots présents
    dans la base, le choix donne souvent le bon résultat quand la séquence est
    suffisamment spécifique ; par exemple, pour `"ltm"` on a choisi de découper
    `"lt-m"`, ce qui fait que `"voltmètre"` est bien syllabé `[vOlt-mEtR]`
    (seule occurrence de "ltm").

## Graphémisation

Ce programme calcule l’association entre graphie et prononciation ;
autrement dit, quelles lettres du mot produisent quels sons.

Par exemple, pour le mot `"oiseaux"` prononcé `[wazo]`, la graphémisation est :

    oi:wa,s:z,eau:o,x:_

Le code source de la graphémisation se trouve dans le fichier
**[graphemisation.py](./graphemisation.py)**.

L’algorithme de graphémisation est le suivant : on a une liste pré-déterminée de
graphèmes autorisés (par exemple, `o:o, i:i, oi:wa, x:ks, x:_` sont autorisés,
mais pas `o:_, o:wa, s:k`…). Étant donnés la graphie et la prononciation d’un
mot, on calcule tous les découpages possibles de la graphie et de la
prononciation tels que, en associant dans l’ordre chaque portion graphique avec
chaque portion prononcée, on ne forme que des graphèmes autorisés (en pratique,
c’est implémenté par un algorithme récursif avec backtracking).

La liste des graphèmes autorisés se trouve en haut du fichier source, dans la
variable `morphemes_par_phoneme` : on y décrit, sous la forme d’expressions
régulières, les graphies autorisées pour chaque son. Le fichier
[liste-graphèmes-lex3-après-correctifs.txt](./liste-graphèmes-lex3-après-correctifs.txt)
liste les graphèmes rencontrés en pratique, avec leurs fréquences (`n` est le
nombre de lemmes, `f1` et `f2` sont les fréquences cumulées qui correspondent
à `freqlivres` et `freqfilms2` ; les nombres donnés en dehors des crochets
excluent les barbarismes, les nombres entre crochets les incluent).

La graphémisation pourrait ne pas être unique ; on a inclus des arbitrages pour
s’efforcer qu’elle soit unique (par exemple, on préfère `eau:o` plutôt que
`e:_,au:o` — ces arbitrages sont listés dans un commentaire dans le fichier
source). De plus, le programme affiche un avertissement si, malgré tout,
plusieurs graphémisations sont possibles pour un mot (ce qui n’arrive pas avec
les données actuelles).

Au contraire, certains « barbarismes » (mots étrangers) n’admettent pas de
découpage du tout. Ils ont été marqués manuellement (cf plus loin), ce sont tous
des mots étrangers dont les graphèmes diffèrent trop du français. Le programme
affiche un avertissement quand il n’y a pas de graphémisation, mais le tolère
quand il s’agit d’un barbarisme.

Le champ `graphemes` dans le tableur produit intègre aussi la syllabation : les
syllabes sont séparées par un point-virgule `;` au lieu d’une virgule `,` .

La graphémisation ouvre la voie à d’autres améliorations, p.ex. :

- restaurer les ligatures `"œ"` et `"æ"`, actuellement confondues avec `"oe"` et `"ae"`
  alors qu’elles devraient être considérés comme des lettres à part ;
- marquer les `"h"` aspirés (p.ex. en croisant avec le Wiktionnaire) ;
- ajouter dans la phonologie les schwas qui ont été complètement omis
  (en détectant les `"e"` muets),
  ou du moins les prendre en compte dans la syllabation (cf avant) ;
- aider à calculer une syllabation orthographique sur la base de la syllabation
  phonologique, en veillant à casser les consonnes doubles ;
- détecter et corriger des phonologies un peu bizarres,
  surtout des confusions entre voyelle ouverte ou fermée
  (p.ex. `"au"` prononcé `[O]`, o ouvert qui devrait plutôt être fermé ;
  ou alors `"e"` isolé prononcé `[2]`, qui devrait plutôt être `[°]`) ;
- …

On peut aussi envisager d’activer des graphèmes supplémentaires en fonction de
la langue.

Voir le fichier [TODO.txt](TODO.txt)

Autant d’améliorations que j’aurais aimées faire moi-même, mais mon temps alloué
à ce projet touche à sa fin.

## Barbarismes

On étiquète manuellement quelques milliers de barbarismes, classés par (groupes
de) langues. Il y a aussi une « langue » spéciale `abr` pour les abréviations
(p.ex. "etc, ok, lsd").

Tous les mots pour lesquels la graphémisation échoue sont étiquetés. En effet,
le but premier est d’identifier les mots pour lesquels on autorise la
graphémisation à échouer. Cependant sont aussi marqués des mots dont
l’orthographe est compatible avec le français, donc la limite est assez
arbitraire, et l’étiquetage est incomplet de ce point de vue. Il s’agit des mots
qui sont tombés dans les limites de nos algorithmes, qui présentent certaines
caractéristiques de phonologie ou d’orthographe, ou qui ont été aperçus en
parcourant manuellement la base.

La langue indiquée dans le champ `lang` est suffixée par un point d’exclamation
`!` si la graphémisation est autorisée à échouer.

La liste des mots à étiqueter se trouve dans le fichier source
**[lex3_barbarismes.py](./lex3_barbarismes.py)**.

## Correctifs

On corrige manuellement de nombreuses erreurs trouvées dans la base Lexique3.
Celle-ci ont été détectées soit par hasard, soit parce qu’elles présentaient des
caractéristiques inhabituelles détectées par nos algorithmes, soit en faisant
des requêtes ciblées.

Les corrections concernent en majorité la prononciation, mais il y a aussi des
lemmes ou catégories grammaticales incorrects, voire quelques coquilles
d’orthographe. Les correctifs effectués s’efforcent de maintenir les fréquences
des mots (`freqfilms2` et `freqlivres`) : par exemple, quand deux mots sont
fusionnés, on additionne leurs fréquences ; quand un mot change de lemme, on
soustrait ses fréquences de celles de son ancien lemme, et on les ajoute
à celles de son nouveau lemme (ces modifications sont implémentées dans le
fichier source [lexique.py](./lexique.py)).

En plus de ces corrections ponctuelles, dans le futur, on pourrait effectuer des
corrections systématiques, par exemple basées sur la graphémisation (cf avant).
À l’heure actuelle il n’y a pas de correction systématique (hormis la nouvelle
syllabation).

La liste des correctifs se trouve dans le fichier source
**[lex3_patchs.py](./lex3_patchs.py)**. Les correctifs sont présentés sous forme
d’opérations appliquées par le programme à partir du tableur source, ce qui
devrait mieux résister à des mises à jour de la base source, et devrait
faciliter la relecture des correctifs et leur intégration à la source.

## Identifiants de mots

Dans ce programme, on désigne un mot au moyen d’un identifiant de la forme
`"orthographe_du_lemme;catégorie_grammaticale;orthographe_du_mot"`.
L’`orthographe_du_mot` est laissée vide quand elle est égale à celle du lemme.

On désigne un *lemme* (c’est-à-dire tous les mots de ce lemme) au moyen d’un
identifiant de la forme `"orthographe_du_lemme;catégorie_grammaticale"` (sans
point-virgule `;` à la fin).
