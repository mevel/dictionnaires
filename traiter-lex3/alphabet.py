#from unidecode import unidecode
#import unicodedata

###
### ALPHABET PHONOLOGIQUE
###

# (en crochets l’alphabet ad-hoc utilisé dans Lexique3,
#  puis en crochets le symbole API si différent,
#  puis en guillemets une transcription en orthographe française si pas évidente)
#
#   voyelles et semi-voyelles:
#     [a]
#     [@] = [ɑ̃] = 'an'
#     [°] = [ə] = schwa
#     [2] = [ø] = 'eû' fermé ("deux")
#     [9] = [œ] = 'eu' ouvert ("neuf")
#     [e]       = 'é'
#     [E] = [ɛ] = 'è'
#     [i]
#     [j]       = 'i' court
#     [5] = [ɛ̃] = 'in'
#     [o]       = 'ô' fermé ("côte")
#     [O] = [ɔ] = 'o' ouvert ("cote")
#     [§] = [ɔ̃] = 'on'
#     [u]       = 'ou'
#     [w]       = 'ou' court ("moi")
#     [y]       = 'u'
#     [8] = [ɥ] = 'u' court ("huit, visuel")
#     [1] = [œ̃] = 'un'
#   consonnes:
#     [b]
#     [d]
#     [f]
#     [g]
#     [k]
#     [l]
#     [m]
#     [n]
#     [N] = [ɲ] = 'gn' ~ [nj]
#     [G] = [ŋ] = 'ng' (anglicismes)
#     [p]
#     [R] = [ʁ] = 'r"
#     [s]
#     [S] = [ʃ] = 'ch'
#     [t]
#     [v]
#     [x]       = jota (emprunts espagnols; extrêmement rare dans la base)
#     [z]
#     [Z] = [ʒ] = 'j'

voyelles_phono = "a@°29eEi5oO§uy1"
semivoyelles_phono = "jw8"
consonnes_phono = "bdfgklmnNGpRsStvxzZ"

alphabet_phono = {}
for v in voyelles_phono:        # voyelles
    alphabet_phono[ord(v)] = "V"
for y in semivoyelles_phono:    # semi-voyelles
    alphabet_phono[ord(y)] = "Y"
for c in consonnes_phono:       # consonnes
    alphabet_phono[ord(c)] = "C"

def cvcv_phono(phono):
    """
    Calcule la classification consonne/voyelle/semi-voyelle de la phonologie donnée,
    en reproduisant l’algo utilisé par Lexique3.
    """
    # remplace chaque symbole phonologique par C/Y/V:
    return phono.translate(alphabet_phono)

###
### ALPHABET ORTHOGRAPHIQUE
###

#   lettres orthographiques extrêmement rares dans Lexique3:
#     "ö" ("angström, maelström, röntgens")
#     "ü" ("capharnaüm, führer, volapük")
#     "ã" ("sertão")
#     "ñ" ("cañon")
#
#   ligatures non conservées:
#     "œ" est écrit "oe"
#     "æ" est écrit "ae"
#
#   symboles de ponctuation conservés:
#     " "
#     "-"
#     "'"
#     "." (dans quelques abréviations)

ponctuation = " -'."
voyelles_dures = "aâàãoôöuûùü"
voyelles_douces = "eéèêëiîïy"
voyelles = voyelles_dures + voyelles_douces
consonnes = "bcçdfghjklmnñpqrstvwxz"

alphabet_ortho = {}
for v in voyelles:
    alphabet_ortho[ord(v)] = "V"
for c in consonnes:
    alphabet_ortho[ord(c)] = "C"

def cvcv_ortho(ortho):
    """
    Calcule la classification consonne/voyelle de l’orthographe donnée,
    en reproduisant l’algo utilisé par Lexique3.
    """
    # supprime toutes les diacritiques:
    #     https://stackoverflow.com/questions/517923
    #ortho = unidecode(ortho)
    #ortho = unicodedata.normalize("NFKD", ortho).encode("ASCII", "ignore").decode()
    # remplace chaque lettre orthographique par C/V:
    return ortho.lower().translate(alphabet_ortho)
