from itertools import groupby

from alphabet import alphabet_phono

###
### DÉCOUPAGE DE LA PHONOLOGIE EN SYLLABES
###

# Algorithme de syllabation:
#
# - chaque voyelle forme le noyau d’une syllabe;
# - pour une semi-voyelle "simple":
#   + si elle est suivie par une voyelle, on la colle à cette voyelle;
#   + sinon (suivie par consonne ou fin du mot), on la colle à la voyelle qui précède;
# - pour une semi-voyelle "double" (les seules occurrences sont [jj] et [jw],
#   et à chaque fois elles sont précédées et suivies d’une voyelle):
#   on coupe entre les 2 semi-voyelles;
# - une consonne "simple" pas en bout de mot est mise dans la syllabe suivante;
# - une séquence de consonnes pas en bout de mot est soit mise entièrement avec
#   la syllabe suivante, soit répartie entre la syllabe précédente et la
#   suivante, selon les règles ad-hoc ci-dessous.
#
# autrement dit, on coupe:
# - entre 2 voyelles;
# - avant une semi-voyelle simple si elle est précédée ET suivie par une voyelle;
# - entre 2 semi-voyelles;
# - avant une consonne simple qui n’est pas à un bout du mot;
# - avant ou au milieu d’une séquence de consonnes, selon les règles ad-hoc
#   ci-dessous.
#
# ci-dessous toutes les séquences consonantiques qui apparaissent au milieu d’un
# mot de Lexique 3.83; pour chaque séquence, un tiret indique où notre
# algorithme coupe la syllabe qui précède (pour une séquence d’un seul son,
# c’est toujours avant le son, autrement dit la consonne seule est mise au début
# de la syllabe qui suit).
#
# cet algo est simple mais imparfait, dans certains cas l’endroit où on aimerait
# couper dépend par exemple de la morphologie (p.ex. "disloquer" [dis-lo-ke]
# / "yougoslave" [ju-go-slav]). ci-dessous, les cas problématiques sont indiqués
# en commentaires.

syllabation_consonnes = {

  "G" : "-G",
  "GR" : "G-R",
  "GS" : "G-S",
  "Gb" : "G-b",
  "Gf" : "G-f",
  "Gg" : "G-g",
  "Gk" : "G-k",
  "Gm" : "G-m",
  "Gp" : "G-p",
  "GstR" : "G-stR",
  "GtS" : "G-tS",

  "N" : "-N",
  "Nk" : "N-k",
  "Nn" : "N-n",
  "Np" : "N-p",
  "Nz" : "N-z",

  "R" : "-R",
  "RN" : "R-N",
  "RR" : "R-R",
  "RS" : "R-S",
  "RSm" : "RS-m",
  "RZ" : "R-Z",
  "RZd" : "RZ-d",
  "Rb" : "R-b",
  "RbR" : "R-bR",
  "Rbd" : "Rb-d",
  "Rbdk" : "Rb-d-k",
  "Rbl" : "R-bl",
  "Rd" : "R-d",
  "RdR" : "R-dR",
  "RdZ" : "R-dZ",
  "Rdm" : "Rd-m",
  "Rdt" : "Rd-t",
  "Rdz" : "R-dz",
  "Rf" : "R-f",
  "RfR" : "R-fR",
  "Rfl" : "R-fl",
  "Rg" : "R-g",
  "RgR" : "R-gR",
  "Rgl" : "R-gl",
  "Rgm" : "Rg-m",
  "Rk" : "R-k",
  "RkR" : "R-kR",
  "RkS" : "Rk-S",
  "Rkb" : "Rk-b",
  "Rkl" : "R-kl",
  "Rkm" : "Rk-m",
  "Rks" : "R-ks",
  "Rkt" : "Rk-t",
  "Rl" : "R-l",
  "Rldm" : "Rld-m",
  "RlfR" : "Rl-fR",
  "Rlsk" : "Rl-sk",
  "Rm" : "R-m",
  "Rmm" : "Rm-m",
  "Rmn" : "R-mn",
  "Rn" : "R-n",
  "Rnbl" : "Rn-bl",
  "Rnf" : "Rn-f",
  "Rnfl" : "Rn-fl",
  "Rnsp" : "Rn-sp",
  "Rnst" : "Rn-st",
  "Rnt" : "Rn-t",
  "Rp" : "R-p",
  "RpR" : "R-pR",
  "Rpl" : "R-pl",
  "Rps" : "R-ps", # ???
  "Rs" : "R-s",
  "RsS" : "Rs-S",
  "Rsg" : "Rs-g",
  "Rsk" : "R-sk", # mais "Rs-k" dans "birth control"
  "Rsp" : "Rs-p",
  "Rst" : "R-st",
  "RstR" : "R-stR",
  "Rt" : "R-t",
  "RtR" : "R-tR",
  "RtS" : "R-tS", # mais "Rt-S" dans "porte-chapeaux"
  "Rtb" : "Rt-b",
  "Rtd" : "Rt-d",
  "RtdR" : "Rt-dR",
  "Rtf" : "Rt-f",
  "Rtk" : "Rt-k",
  "RtkR" : "Rt-kR",
  "Rtl" : "Rt-l",
  "Rtm" : "Rt-m",
  "Rtn" : "Rt-n",
  "Rtp" : "Rt-p",
  "Rts" : "Rt-s", # mais "R-ts" dans "hertzien, hertzienne"
  "Rtsm" : "Rts-m",
  "Rv" : "R-v",
  "Rz" : "R-z",

  "S" : "-S",
  "SR" : "S-R",
  "SS" : "S-S",
  "Sb" : "S-b",
  "SbR" : "S-bR",
  "Sd" : "S-d",
  "Sf" : "S-f",
  "SfR" : "S-fR",
  "Sg" : "S-g",
  "Sk" : "S-k",
  "Skl" : "S-kl",
  "Sl" : "S-l",
  "Sm" : "S-m",
  "Sn" : "S-n",
  "Sp" : "S-p",
  "SpR" : "S-pR",
  "Ss" : "S-s",
  "Ssm" : "S-sm",
  "Sst" : "S-st",
  "Ssv" : "S-sv",
  "St" : "S-t",
  "StR" : "S-tR",
  "Sv" : "S-v",

  "Z" : "-Z",
  "ZbR" : "Z-bR",
  "Zd" : "Z-d",
  "Zf" : "Z-f",
  "Zg" : "Z-g",
  "Zk" : "Z-k",
  "ZkR" : "Z-kR",
  "Zkl" : "Z-kl",
  "Zs" : "Z-s",
  "Zsl" : "Z-sl",
  "Zt" : "Z-t",
  "Zv" : "Z-v",

  "b" : "-b",
  "bR" : "-bR",
  "bRs" : "bR-s",
  "bZ" : "b-Z",
  "bd" : "b-d",
  "bk" : "b-k",
  "bkl" : "b-kl",
  "bl" : "-bl", # mais "b-l" dans "sublunaire" p.ex.
  "blk" : "bl-k",
  "bm" : "b-m",
  "bn" : "b-n",
  "bs" : "b-s",
  "bsl" : "b-sl",
  "bt" : "b-t",
  "btR" : "b-tR",
  "bv" : "b-v",
  "bz" : "b-z",

  "d" : "-d",
  "dR" : "-dR",
  "dS" : "d-S",
  "dZ" : "-dZ", # ???
  "dZk" : "dZ-k",
  "db" : "d-b",
  "dd" : "d-d",
  "ddZ" : "d-dZ",
  "df" : "d-f",
  "dgR" : "d-gR",
  "dk" : "d-k",
  "dkl" : "d-kl",
  "dl" : "d-l",
  "dm" : "d-m",
  "dn" : "d-n",
  "dp" : "d-p",
  "dpl" : "d-pl",
  "ds" : "d-s",
  "dst" : "d-st",
  "dt" : "d-t",
  "dv" : "d-v",
  "dz" : "-dz", # ??? "-dz" dans les mots italiens, "d-z" dans "bretzel, brindezingue, pedzouille"

  "f" : "-f",
  "fR" : "-fR",
  "fS" : "f-S",
  "fb" : "f-b",
  "fg" : "f-g",
  "fk" : "f-k",
  "fkR" : "f-kR",
  "fl" : "-fl",
  "fm" : "f-m",
  "fn" : "f-n",
  "fpl" : "f-pl",
  "fs" : "f-s",
  "ft" : "f-t",
  "ftb" : "ft-b",

  "g" : "-g",
  "gR" : "-gR",
  "gStR" : "g-StR",
  "gZ" : "g-Z", # ne concerne que la famille "suggérer"
  "gb" : "g-b",
  "gd" : "g-d",
  "gfl" : "g-fl",
  "gk" : "g-k",
  "gl" : "-gl",
  "gm" : "g-m",
  "gn" : "g-n",
  "gst" : "g-st",
  "gt" : "g-t",
  "gv" : "g-v",
  "gz" : "g-z",

  "k" : "-k",
  "kR" : "-kR",
  "kS" : "k-S",
  "kSp" : "k-Sp",
  "kb" : "k-b",
  "kd" : "k-d",
  "kdZ" : "k-dZ",
  "kf" : "k-f",
  "kfR" : "k-fR",
  "kg" : "k-g",
  "kgR" : "k-gR",
  "kk" : "k-k",
  "kkR" : "k-kR",
  "kkl" : "k-kl",
  "kl" : "-kl",
  "km" : "k-m",
  "kn" : "k-n",
  "kp" : "k-p",
  "ks" : "k-s",
  "ksR" : "ks-R",
  "ksS" : "ks-S",
  "ksZ" : "ks-Z",
  "ksb" : "ks-b",
  "ksbR" : "ks-bR",
  "ksd" : "ks-d",
  "ksdR" : "ks-dR",
  "ksdZ" : "ks-dZ",
  "ksf" : "ks-f",
  "ksfl" : "ks-fl",
  "ksg" : "ks-g",
  "ksgR" : "ks-gR",
  "ksk" : "ks-k", # mais "k-sk" dans "public school"
  "kskR" : "ks-kR",
  "kskl" : "ks-kl",
  "ksl" : "ks-l",
  "ksm" : "ks-m",
  "ksn" : "ks-n",
  "ksp" : "ks-p", # ??? ou "k-sp" dans des mots comme "expert, expier…"
  "kspR" : "ks-pR",
  "kspl" : "ks-pl",
  "kss" : "ks-s",
  "ksst" : "ks-st",
  "ksstR" : "ks-stR",
  "kst" : "ks-t",
  "kstR" : "ks-tR",
  "ksv" : "ks-v",
  "kt" : "k-t",
  "ktR" : "k-tR",
  "kv" : "k-v",
  "kz" : "k-z",

  "l" : "-l",
  "lR" : "l-R",
  "lS" : "l-S",
  "lZ" : "l-Z",
  "lb" : "l-b",
  "ld" : "l-d",
  "ldR" : "l-dR",
  "ldZ" : "l-dZ",
  "ldgR" : "ld-gR",
  "ldk" : "ld-k",
  "ldkR" : "ld-kR",
  "ldm" : "ld-m",
  "ldsp" : "ld-sp",
  "ldv" : "ld-v",
  "lf" : "l-f",
  "lfR" : "l-fR",
  "lfd" : "lf-d",
  "lfk" : "lf-k",
  "lfkl" : "lf-kl",
  "lfm" : "lf-m",
  "lfs" : "lf-s",
  "lftR" : "lf-tR",
  "lg" : "l-g",
  "lgR" : "l-gR",
  "lk" : "l-k",
  "lkR" : "l-kR",
  "lkS" : "lk-S",
  "lkb" : "lk-b",
  "lkl" : "l-kl",
  "lkm" : "lk-m",
  "ll" : "l-l",
  "lm" : "l-m",
  "lml" : "lm-l",
  "ln" : "l-n",
  "lp" : "l-p",
  "lpR" : "l-pR",
  "ls" : "l-s",
  "lsk" : "l-sk",
  "lst" : "l-st",
  "lstR" : "l-stR",
  "lt" : "l-t",
  "ltR" : "l-tR",
  "ltS" : "l-tS",
  "ltg" : "lt-g",
  "ltm" : "lt-m",
  "lv" : "l-v",
  "lz" : "l-z",
  "lzb" : "lz-b",

  "m" : "-m",
  "mR" : "m-R",
  "mS" : "m-S",
  "mZ" : "m-Z",
  "mb" : "m-b",
  "md" : "m-d",
  "mf" : "m-f",
  "mfl" : "m-fl",
  "mg" : "m-g",
  "mgR" : "m-gR",
  "mk" : "m-k",
  "mkl" : "m-kl",
  "ml" : "m-l",
  "mm" : "m-m",
  "mn" : "m-n",
  "mp" : "m-p",
  "mpR" : "m-pR",
  "ms" : "m-s",
  "mst" : "m-st",
  "mstR" : "m-stR",
  "mstv" : "m-stv",
  "mt" : "m-t",
  "mtR" : "m-tR",
  "mv" : "m-v",
  "mz" : "m-z",

  "n" : "-n",
  "nR" : "n-R",
  "nS" : "n-S",
  "nSk" : "nS-k",
  "nSp" : "n-Sp",
  "nSt" : "n-St",
  "nZ" : "n-Z",
  "nb" : "n-b",
  "nbl" : "n-bl",
  "nd" : "n-d",
  "ndR" : "n-dR",
  "ndZ" : "n-dZ",
  "nds" : "nd-s",
  "nf" : "n-f",
  "ng" : "n-g",
  "nk" : "n-k",
  "nkl" : "n-kl",
  "nl" : "n-l",
  "nm" : "n-m",
  "np" : "n-p",
  "ns" : "n-s",
  "nsl" : "n-sl",
  "nst" : "n-st",
  "nt" : "n-t",
  "ntR" : "n-tR",
  "ntS" : "n-tS",
  "ntZ" : "nt-Z",
  "ntg" : "nt-g",
  "ntl" : "n-tl",
  "nts" : "n-ts",
  "ntv" : "nt-v",
  "nv" : "n-v",
  "nz" : "n-z",

  "p" : "-p",
  "pR" : "-pR",
  "pS" : "p-S",
  "pZ" : "p-Z",
  "pb" : "p-b",
  "pd" : "p-d",
  "pf" : "p-f",
  "pfR" : "p-fR",
  "pfl" : "p-fl",
  "pg" : "p-g",
  "pgR" : "p-gR",
  "pk" : "p-k",
  "pkl" : "p-kl",
  "pl" : "-pl",
  "pm" : "p-m",
  "pn" : "p-n",
  "pp" : "p-p",
  "ps" : "p-s", # ??? "p-s" dans "chop suey, coupe-cigare, abcès, subsonique, réception, conception…"; discutable dans "lapsus, capsule, option, éclipser, observer, exception, inscription…"; "-ps" dans "nécropsie, médico-psychologique, métapsychique…"
  "psk" : "p-sk", # ??? ou "ps-k"
  "pst" : "p-st",
  "pstR" : "p-stR",
  "pt" : "p-t",
  "ptR" : "p-tR",
  "ptg" : "pt-g",
  "pv" : "p-v",

  "s" : "-s",
  "sR" : "s-R",
  "sS" : "s-S",
  "sZ" : "s-Z",
  "sb" : "s-b",
  "sbR" : "s-bR",
  "sd" : "s-d",
  "sdR" : "s-dR",
  "sf" : "s-f", # ??? "s-f" dans "asphalte, satisfait, basse-fosse, dysfonction, transfert…";  discutable dans "phosphore…"; "-sf" dans "hémisphère, atmosphère, biosphère…"
  "sfR" : "s-fR",
  "sfl" : "s-fl",
  "sg" : "s-g",
  "sgR" : "s-gR",
  "sk" : "s-k", # ??? "s-k" dans "mascotte, ascorbique, ausculter, bascule, basse-cour, batyscaphe, biscornu, biscuit, casse-cou, confisquer, cross-country, discuter…"; "-sk" dans "antiskating, après-ski, microscopique, boy-scout"
  "skR" : "s-kR",
  "skZ" : "sk-Z",
  "skl" : "s-kl",
  "sl" : "s-l", # mais "-sl" dans "déslipe, tchécoslovaque, yougoslave, islam, islandais"
  "sm" : "s-m",
  "sn" : "s-n",
  "sp" : "s-p", # ??? "s-p" dans "espion, suspect, respirer, transport, passeport, chausse-pied, lance-pierre, despote, espace…"; "-sp" dans "antisportif, angiosperme, aérospatial, téléspectateur, rétrospective, non-spécialiste…"
  "spR" : "s-pR",
  "spl" : "s-pl",
  "spn" : "s-pn",
  "ss" : "s-s",
  "st" : "s-t", # ??? "s-t" dans "pasteur, tester, assister, ajuster, moustique, dystopie, casse-tête, espace-temps"; discutable dans "poster, agnostique, instinct, eustache"; "-st" dans "auto-stop, déstocker, acrostiche, allostérique, géostationnaire, hydrostatique, hémostase, chrysostome"; "st-" dans "est-allemand"
  "stR" : "s-tR", # mais "-stR" dans "anti-stress, apostrophe, catastrophe, autostrade, géostratégie"; "st-r" dans "east river"
  "std" : "st-d",
  "stf" : "st-f",
  "stk" : "st-k",
  "stm" : "st-m",
  "stn" : "st-n",
  "stp" : "st-p",
  "stpR" : "st-pR",
  "sts" : "st-s",
  "stskR" : "st-skR",
  "sttR" : "st-tR",
  "stv" : "st-v",
  "stz" : "st-z",
  "sv" : "s-v",

  "t" : "-t",
  "tR" : "-tR",
  "tRd" : "tR-d",
  "tRf" : "tR-f",
  "tRg" : "tR-g",
  "tRk" : "tR-k",
  "tRkl" : "tR-kl",
  "tRl" : "tR-l",
  "tRm" : "tR-m",
  "tS" : "-tS", # mais "t-S" dans "sweat-shirt"; "tS-" dans "patchwork"
  "tSk" : "tS-k",
  "tSm" : "tS-m",
  "tSp" : "tS-p",
  "tSt" : "tS-t",
  "tSv" : "t-Sv",
  "tZ" : "t-Z",
  "tb" : "t-b",
  "td" : "t-d",
  "tf" : "t-f",
  "tg" : "t-g",
  "tgl" : "t-gl",
  "tk" : "t-k",
  "tkR" : "t-kR",
  "tkl" : "t-kl",
  "tl" : "t-l",
  "tm" : "t-m",
  "tn" : "t-n",
  "tp" : "t-p",
  "tpR" : "t-pR",
  "tpl" : "t-pl",
  "tpn" : "t-pn",
  "ts" : "t-s", # mais discutable dans "clausewitzien, jiu-jitsu, keiretsu, kibboutzim…"; "-ts" dans "tsoin-tsoin", "tsé-tsé"
  "tsZ" : "ts-Z",
  "tsk" : "ts-k",
  "tskR" : "ts-kR",
  "tsn" : "ts-n",
  "tsp" : "ts-p",
  "tstR" : "t-stR",
  "tsv" : "ts-v",
  "tt" : "t-t",
  "ttR" : "t-tR",
  "tv" : "t-v",
  "tz" : "t-z",

  "v" : "-v",
  "vR" : "-vR",
  "vRkl" : "vR-kl",
  "vd" : "v-d",
  "vgl" : "v-gl",
  "vk" : "v-k",
  "vl" : "v-l",
  "vm" : "v-m",
  "vn" : "v-n",
  "vp" : "v-p",
  "vs" : "v-s",
  "vsk" : "v-sk",
  "vv" : "v-v",
  "vz" : "v-z",

  "x" : "-x",

  "z" : "-z",
  "zR" : "z-R",
  "zZ" : "z-Z",
  "zb" : "z-b",
  "zbR" : "z-bR",
  "zd" : "z-d",
  "zdZ" : "z-dZ",
  "zf" : "z-f",
  "zg" : "z-g",
  "zgR" : "z-gR",
  "zgl" : "z-gl",
  "zk" : "z-k",
  "zkR" : "z-kR",
  "zkl" : "z-kl",
  "zl" : "z-l",
  "zm" : "z-m",
  "zn" : "z-n",
  "zp" : "z-p",
  "zt" : "z-t",
  "zv" : "z-v",

}

for seq, syll in syllabation_consonnes.items():
    assert type(syll) == str
    assert syll.replace("-", "") == seq
    assert syll.count("-") == 1 or seq == "Rbdk"
    assert syll[-1] != "-"
    if len(seq) == 1: assert syll == "-" + seq

def syllabation(phono):
    morceaux = [ (typ, "".join(lettres))
                for (typ,lettres) in groupby(phono, key=lambda c: alphabet_phono[ord(c)]) ]
    syll_phono = ""
    for i, (typ, lettres) in enumerate(morceaux):
        match typ:
            case "C": # consonnes
                # si on est à une extrémité de mot, on ne coupe pas:
                if i == 0 or i == len(morceaux)-1:
                    syll_phono += lettres
                # sinon, on coupe avant ou entre les consonnes selon les règles
                # ci-dessus (qui gèrent aussi le cas des consonnes simples):
                #elif len(lettres) == 1:
                #    syll_phono += "-" + lettres
                else:
                  try:
                    syll_phono += syllabation_consonnes[lettres]
                  except KeyError:
                    print(f'!!! ERREUR: syllabation: séquence consonantique inconnue: [{lettres}] dans [{phono}]')
                    exit(1)
            case "Y": # semi-voyelles
                if len(lettres) == 1:
                    # si la semi-voyelle simple est précédée ET suivie par une
                    # voyelle, on coupe avant la semi-voyelle:
                    if i > 0 and morceaux[i-1][0] == "V" \
                      and i < len(morceaux)-1 and morceaux[i+1][0] == "V":
                        syll_phono += "-"
                    syll_phono += lettres
                else:
                    # semi-voyelle double, on coupe entre les 2 semi-voyelles:
                    assert len(lettres) == 2
                    assert lettres in [ "jj", "jw" ]
                    assert i > 0 and morceaux[i-1][0] == "V"
                    assert i < len(morceaux)-1 and morceaux[i+1][0] == "V"
                    syll_phono += "-".join(lettres)
            case "V": # voyelles
                # on coupe entre chaque voyelle:
                syll_phono += "-".join(lettres)
            case _:
                assert False
    return syll_phono
