import regex as re # on a besoin de la lib tierce `regex` pour le flag `re.POSIX`

from alphabet import *

###
### CORRESPONDANCE ORTHOGRAPHE--PHONOLOGIE
###

morphemes_par_phoneme = {
  # NOTE: "!!" indique les phonologies douteuses dans Lexique3.
  #
  # NOTE: Les conditions lookahead / lookbehind servent:
  #   - soit à vérifier des invariants de l’orthographe
  #     (par exemple, 'an' prononcé [@] n’est jamais suivi d’une voyelle);
  #     ces conditions sont superflues puisqu’on part de la phonologie déjà donnée;
  #   - soit à résoudre des ambigüités (cf. plus bas);
  #   - soit pour le suffixe '-er' prononcé [e] qui doit se trouver en fin
  #     de mot mais peut tout de même être suivi d’un '-s' de pluriel.
  #
  # VOYELLES ET SEMI-VOYELLES:
  #
  "a" : r"[aâà]|ao(?=[mn])|aa|e(?=[mn][mn])",
        # 'ao' dans "paonne";
        # 'aa' dans "graal";
        # 'e' dans "femme, apparemment, solemnel, couenne";
  "@" : rf"([ae]|ao|aa)[mn](?![{voyelles}])",
        # 'aon' dans "paon";
        # 'aan' dans "afrikaans";
  "@n" : r"\ben", # "en-avant, enamourer, enivrer"
  "°" : r"e|ai|on",
        # 'ai' dans "bienfaisant";
        # 'on' dans "monsieur"!!;
  "2" : r"o?e[uû]?|ai|ö",
        # 'oeu' dans "œufs, bœufs";
        # 'e' dans "ce"!!;
        # 'ai' dans "faisan";
        # 'ö' dans "angström, maelström" (scandinavismes);
  "9" : r"o?eu?|(?<=[cg])ue|(?<!e)u(?!e|\b)",
        # 'ue' dans "accueil, orgueil";
        # 'u' dans "auburn" et nombreux autres anglicismes;
  "e" : r"e(?!rs?\b|z\b)|[éêë]|oe|ae|er(?=s?\b)|ez\b|a[iî]|e[iy]",
        # 'e' dans "pied";
        # 'ê' dans "blêmir"!!;
        # 'ë' dans "canoë";
        # 'oe' dans "cœlacanthe, Œdipe";
        # 'ae' dans latinismes ("taenia, caecum, et caetera…");
        # 'er(s)' dans "aller, boulanger(s)";
        # 'ez' dans "allez";
        # 'ai' dans "j’allai" (prononciation vieillie)
        #   et dans plein de phonologies de Lexique3 qui devraient être [E]!!;
        # 'aî' dans "aîné"!!;
        # 'ei' dans "baleinier"!!;
        # 'ey' dans "ney"!!;
  "E" : r"[eéèêë]|ez\b|a[iîy]|e[iîy]|ae",
        # 'é' dans "allégrement" (pré-1990);
        # 'ez' dans "lez";
        # 'ei' dans "reître";
        # 'ey' dans "dey, geyser, dreyfusard" et nombreux anglicismes ("poney");
        # 'ae' dans "maelström";
  "Ej" : r"ay",
        # 'ay' dans "balayette, rayer";
        # 'ey' dans "asseye, cheyenne";
        #      NOTE: obtenu décomposé comme e:E,y:j
        # 'eyi' dans "asseyiez";
        #      NOTE: obtenu décomposé comme ey:E,i:j
        # 'eil(l(i))' dans "vermeil; abeille, meilleur; émerveillions";
        #      NOTE: obtenu décomposé comme e:E,il:j
  "ej" : r"ay", # "balayer"!!
  "Ei" : r"ay", # "pays"
  "ei" : r"ay", # "paysan, abbaye"!!
  "i" : r"[iîïy]|ii\b",
        # 'ii' dans "scénarii, imprésarii"
  "ij" : r"[iy](?!l)|il(li?|(?=s?\b|h))",
        # 'i' dans "à priori, requiem" mais pas "gentilhommière";
        # 'y' dans "appuyer, ennuyer, écuyer, bruyère";
        # 'il' dans "gentilhommière";
        # 'ill' dans "fille, aiguille, cuillère";
  "j" : r"(?<!oy)i(?!l)|[ïyj]|ii|il(li?|(?=s?\b))",
        # 'ii' dans "hawaiien";
        # 'j' dans "fjord";
  "5" : rf"([iîïy]|ai|ei|e)[mn](?![{voyelles}])",
        # 'în' dans "tînt";
        # 'ïn' dans "coïncider";
  "5n" : rf"en(?=[h{voyelles}]|\b)", # "bien-aimé, bienheureux"
  "o" : r"[oô]|(?<!e)au|eau|oo|oa",
        # 'oo' dans "alcootest"
        # 'oa' dans "toast" (anglicisme);
  "O" : r"o|[uü](?=m)|(?<!e)au|eau|oo|oi|oa",
        # 'u' dans "aquarium";
        # 'ü' dans "capharnaüm";
        ## 'ô' dans "diplômer"!!;
        # 'oo' dans "alcool";
        # 'oi' dans "oignon, encoignure" (techniquement c’est 'ign' qui produit [N]);
        # 'oa' dans "coaltar, goal, toast…" (anglicisme);
  "§" : rf"[ou][mn](?![{voyelles}])",
        # 'un' dans "acupuncture" (pré-1990);
  "§n" : r"on\b", # "non-être"
  "u" : r"a?o[uûù]|[uù]|oo|ow",
        # 'aoû' dans "août";
        # 'u' dans "agnus dei";
        # 'ù' dans "più";
        # 'oo' dans "vroom" et nombreux autres anglicismes;
        # 'ow' dans "clown";
  "w" : r"w|ou|u|oo|wou",
        # 'u' dans "équateur, jaguar";
        # 'oo' dans "shampooing";
        # 'wou' dans "wouah";
  "wa" : r"o[iîy]|o[eê]",
        # oy dans "oye" (archaïsme);
        # oe dans "moelle, moelleux, moellon";
        # oê dans "poêle, poêlon";
  "waj" : r"o[ïy]|oyi|oil(li?|(?=s\b))",
        # 'oî' dans "???";
        # 'oï' dans "anchoïade";
        # 'oy' dans "moyen, royaume";
        # 'oyi' dans "croyiez";
        # 'oill' dans "cancoillote";
  "wajj" : r"oyi", # dans Lexique3, "oyi" est prononcé [waj] ou [wajj] sans cohérence (cf conjugaison de "croire")
  "w5" : r"oin",
  "wE" : r"oë", # "foëne"
  #"we" : r"oe", # "moellons"!!
  "y" : r"[uûü]|uë|\be[uû]",
        # 'uë' dans "aiguë, ciguë" (pré-1990);
        # 'eu/eû' dans diverses flexions du verbe "avoir";
  "8" : r"[uü]",
  "1" : rf"e?[uû][mn](?![{voyelles}])",
  #
  # CONSONNES:
  #
  "b" : r"bb?",
  "d" : r"dd?|t",
        # 't' dans "bretzel"
  "f" : r"ff?|ph",
  "g" : r"gg?(u(?!eil))?|c",
        # 'g' + 'ue' dans "orgueil"
        # 'g' + voyelle douce dans "magyar, geisha, yogi", anglicismes, germanismes;
        # 'gg' dans "aggravé";
        # 'c' dans "second";
  "k" : r"cc?|c?qu?|c?kk?|c?ch|k?g",
        # 'c' + voyelle douce dans "de amicis" (latinisme);
        # 'q' sans 'u' dans "coq, cinq; piqûre; équidistant; équateur"
        # 'cch' dans "bacchanale";
        # 'ck' dans "teck";
        # 'kk' dans "akkadien";
        # 'g' dans "tungstène";
        # 'kg' dans ginkgo;
  "l" : r"ll?",
  "m" : r"mm?",
  "n" : r"nn?|mn",
        # 'mn' dans "automne, solemnel";
  "p" : r"pp?(?!h)|b",
        # 'p' dans "abstient, absurde…"!!;
  "R" : r"rr?h?|(?<!r-?)[hj](?!r)",
        # 'h/j' dans "marihuana, marijuana, navaho, navajo" (espagnolismes)
  "x" : r"j",
  "s" : r"s?[cç]|cc|ss?|(?<!s)th|sth|t(?=i)|x|z",
        # 'sç' dans "acquiesçant";
        # 'cc' dans "succion";
        # 'th' dans "forsythia";
        # 'sth' dans "asthme, isthme";
        # 'x' dans "six, dix";
        # 'z' dans "quartz, aztèque";
  "t" : r"tt?|th|tth|d\b",
        # 'd' dans "grand-oncle";
        # 'tth' dans "kilowattheure" (pas idéal…);
  "v" : r"[vw]",
  "z" : r"zz?|s|x",
        # 'zz' dans "jazz";
        # 'x' dans "sixième, dixième, aux aguets";
        ## 'sth' dans "isthme"!!;
  "dz" : r"(?<![td])zz?", # italismes
  "ks" : rf"x(c(?=[{voyelles_douces}])|s|t(?=i))?",
        # 'xs' dans "exsangue, exsuder";
        # 'xt' dans "admixtion";
  "gz" : r"x",
  "S" : r"s?ch|sh|chs(?!\b)|sc|s(?=tr)|j",
        # 'sc' dans "fascisme";
        # 'chs' dans "fuchsia";
        # 's' dans "angström, strudel";
        # 'j' dans "pirojki";
  "tS" : rf"(?<!t)ch|cc?i?(?=[{voyelles}])",
        # 'ch' dans anglicismes, espagnolismes;
        # 'c(c)(i)' dans italismes ("dolce, mezza voce, cappuccino, carpaccio, ciao");
  "Z" : rf"j|g(e(?=[{voyelles_dures}]))?",
  "dZ" : rf"(?<!d)(j|gg?(e(?=[{voyelles_dures}]))?)",
        # "jazz, adagio", anglicismes, italismes
  "N" : r"gni?|(?<!g)n?ni|ñ",
        # 'ñ' dans "cañon, doña, señor"
  "G" : rf"ng(?![{voyelles_dures}])|(?<=n)g",
        # 'g' dans "sampang, ginseng, ma(h)-jong, ping-pong"!!
  "Gg" : rf"ngu?(?=[{voyelles}])",
        # 'dans "bingo, gringo, jungien, toungouse"
  #
  # LETTRES MUETTES:
  # (une seule à la fois pour la canonicité, sauf 'th' et 'ent$')
  #
  "" : rf"""
          [ '.-]               # ponctuation
        | (?<![crt]) h         # 'h' sauf dans lettre grecque
        | (?<![st]) th         # 'th' dans ???
        | (?<!b)    b (?=s?\b) # 'b' dans "plomb"
        | (?<!\b|p) p (?!p) (?![{voyelles}])
                               # 'b' dans "loup; corps"
        | (?<![xc]) c (?![ckq) (?![{voyelles}])
                               # 'c' dans "blanc, clerc, entrelacs; instinct, aspect, arctique"
        | (?<!f)    f (?=s?\b) # 'f' dans "clef; œufs, bœufs"
        | (?<!\b|g) g (?!g) (?![{voyelles}])
                               # 'g' dans "sang; amygdale"
        #| (?<!\b|l) l (?!l) (?!s?\b) (?![{voyelles}])
        | (?<=au) l (?!l) (?!s?\b) (?![{voyelles}])
                               # 'l' dans "aulne, aulx, gentilshommes"
        | (?<!\b|ai|ei|eui|oui|l) l (?=s|\b)
                               # 'l' dans "fusil, fils, gentil, cul; gentilshommes"
        |           w (?=s?\b) # 'w' dans "bungalow"
        | (?<!\b|ch|[xs]) s (?![scç]) (?![{voyelles}])
                               # 's' dans plein de mots en -s; "est"
        | (?<=ch) s \b         # 's' pluriel des mots en '-ch'
        | x                    # 'x' dans "feux, houx, aux; auxdits, auxquels"
        | (?<![ez]) z \b       # 'z' dans "raz-de-marée"
        | (?<![er]) r (?=s?\b) # 'r' dans "monsieur, gars"
        | (?<!\b|d) d (?!d) (?![{voyelles}])
                               # 'd' dans "chaud, pied; piedmont"
        | (?<!\b|[xt]) t (?!t) (?![aâàãeéèêëiîïoôöuûùüy])
                               # 't' dans plein de mots en -t; "hautbois"
        | (?<!\b|[ge]) e (?!e|au|in|un|nt?\b)
                               # 'e' schwa (il faudrait rectifier la phonologie en [°])
        | (?<=g) e (?=s?\b)    # 'e' dans terminaisons -ge(s)
        | ent\b                # '-ent' dans verbes conjugués
  """,
}

# AMBIGÜITÉS:
#
# Plusieurs découpages de la même orthographe peuvent donner la même phonologie.
# Ci-dessous les paires rencontrées. On veut l’unicité du découpage, on doit donc
# résoudre chaque ambigüité en choisissant l’une des alternatives.
#
#   légende:
#     </> = résolu à gauche/droite par les regex ci-dessus
#
#   consonnes:
#     < 'ch':        ch:k / c:k,h:_
#     < 'rh':        rh:R / r:R,h:_
#     < 'th':        th:t / t:t,h:_
#     < 'tth':       tth:t / t:t,th:_ / t:_,th:t / tt:t,h:_
#     < 'zz':        zz:z / z:z,z:_ / z:_,z:z
#     < 'ss':        ss:s / s:s,s:_ / s:_,s:s
#     < ... idem pour toutes les consonnes qui peuvent être doubles et muettes
#     < 'sth':       sth:s / s:s,th:_ / s:_,th:s
#     < 'sc':        sc:s / s:s,c:_ / s:_,c:s
#     < 'sç':        sç:s / s:_,ç:s
#     < 'xc':        xc:ks / x:ks,c:_
#     < 'xs':        xs:ks / x:ks,s:_
#     < 'xt':        xt:ks / x:ks,t:_
#     ~ 'chs':       chs:S / ch:S,s:_
#         < si suivi par i ("fuchsia")
#         > si fin de mot ("varechs, patchs…")
#     < 'tch':       t:t,ch:S / t:_,ch:tS
#     < 'dg':        d:d,g:Z / d:_,g:dZ
#     < 'dz':        d:d,z:z / d:_,z:dz
#     < 'tz':        t:d,z:z / t:_,z:dz
#     < 'ck':        ck:k / c:_,k:k
#     < 'cqu':       cqu:k / c:_,qu:k
#     > 'hr':        h:R,r:_ / h:_,r:R
#
#   consonne puis voyelle:
#     > 'qû':        qû:ky / q:k,û:y
#     > 'qu':        qu:k8 / q:k,u:8
#     > 'qu':        qu:kw / q:k,u:w
#     < 'ge':        ge:Z / g:Z,e:_
#     > 'geu':       ge:Z,u:9 / (g:Z,e:_,u:9) / g:Z,eu:9
#     > 'gein':      ge:Z,in:5 / (g:Z,e:_,in:5) / g:Z,ein:5
#     > 'geau':      ge:Z,au:o / (g:Z,e:_,au:o) / g:Z,eau:o
#     > 'gueil':     gu:g,e:9,il:j / g:g,ue:9,il:j
#
#   voyelles:
#     < 'eau':       eau:o / e:_,au:o
#     < 'ein':       ein:5 / e:_,in:5
#     < 'eun':       eun:1 / e:_,un:1
#     < '^eu':       eu:y / e:_,u:y
#     < 'ent$':      ent:_ / e:_,n:_,t:_
#     > 'il(s)$':    il:i / i:i,l:_ ("fusil, fils, gentil, cul")
#     < 'il(s)$':    il:j / i:j,l:_ (toujours après voyelle: "ail, éveil, oeil, écueil, treuil, fenouil")
#     < 'ill':       ill:ij / il:ij,l:_
#     < 'oill':      oill:waj / oil:waj,l:_
#     < 'oyi':       oyi:waj / oy:wa,i:j
#     < 'er(s)$':    er:e / e:e,r:_
#     < 'ez$':       ez:e / e:e,z:_
#     < 'ces$':      c:s,e:_,s:_ / c:_,e:_,s:s
#     < 'ses$':      s:z,e:_,s:_ / s:_,e:_,s:z
#     < 'gni':       gni:N / g:_,ni:N
#     > 'ing':       ing:iG / i:i,ng:G
#
#   frontière entre mots:
#     > 'e(s)-e':    e:E,…,e:_ / e:_,…,e:E ("contre-exemple, drone-espion")
#     < 'lle(s)-l':  ll:l,…,l:l / l:l,l:l,…,l:_ ("celle-là, belle lurette, belles-lettres, cardinal-légat?…")
#     > 'p-p':       p:p,…,p:_ / p:_,…,p:p ("trop-plein")
#     > 't-t':       t:t,…,t:_ / t:_,…,t:t ("avant-toit")
#     > 't-d':       t:d,…,d:_ / t:_,…,d:d ("avant-dernier")
#     > 'c-g':       c:g,…,g:_ / c:_,…,g:g ("blanc-gris")
#     < 'r-h':       r:R,…,h:_ / r:_,…,h:R ("bar-hôtel")
#     < 'de-j':      d:d,…,j:Z / d:_,…,j:dZ ("aide-jardinier")

morphemes_par_phoneme = {
  p : re.compile(r, flags = re.POSIX | re.IGNORECASE | re.VERBOSE)
  for p, r in morphemes_par_phoneme.items()
}

max_len_phoneme = max(len(p) for p in morphemes_par_phoneme)

# PHONOLOGIE BARBARE:
#
# Certaines associations orthographe--phonologie trahissent souvent des
# barbarismes. Voici une liste ci-dessous, pour faciliter la détection et
# l’étiquetage de tels barbarismes.
morphemes_barbares = {
  "ae:e",  # latinismes, "reggae"
  "ey:E",  # nombreux anglicismes ("poney…"); quelques autres emprunts ("dey, geyser")
  "u:9", "u:9,m:m", "u:9,n:n",  # nombreux anglicismes ("auburn…")
  "oa:o",  # anglicismes ("coaltar, goal, toast…")
  "oa:O",  # idem
  "u:u",  # latinismes, italismes, espagnolismes, anglicismes, germanismes, japonismes…
  "ow:u",  # "clown."
  "oo:u",  # nombreux anglicismes ("vroom…")
  "oo:w",  # "shampooing."
  "j:j",  # "fjord, yodler, jungien."
  rf"c?c:k,[{voyelles_douces}][^,]*",  # "de amicis."
  rf"g?g:g,[{voyelles_douces}][^,]*",  # "magyar, geisha, yogi"; nombreux anglicismes
                                       # (doublé gg ou précédé de r); quelques germanismes
  "h:R", "j:R", "j:x",  # quelques espagnolismes ("marihuana, marijuana, navaho, navajo; azulejo, jerez, rioja, juan.")
  "sc:S",  # "fascisme, crescendo"
  "s:S",   # "angström, strudel"
  "ch:tS", # anglicismes; espagnolismes
  "c:tS", "ci:tS", "cc:tS", "cci:tS",  # italismes
  "j:dZ", "g:dZ", "gg:dZ",  # anglicismes ('j/g'), italismes ('g/gg'),
                            # quelques autres mots ("banjo, jellaba, maharaja")
  "z:dz", "zz:dz",  # italismes; "muezzin"
  "ñ:N",  # quelques espagnolismes ("cañon, doña, señor.")
  "ö:2",  # "angström, maelström, röntgens."
  "w:v",
  "i:i,ng:G",  # nombreux anglicismes, quelques autres mots ("dring, ping-pong")
  "[ae]ng:@G",  # "sampang, ginseng"
  "ong:§G",  # "ma(h)-jong, ping-pong"
  # dénasalisations:
  rf"a:a,n:n($|,(?![{voyelles}])[^,]*)",
  #rf"e:.,n:n($|,(?![{voyelles}])[^,]*)", # => je n’ai pas catégorisé comme barbarisme, "kraken, yen, zen…" ou "en" ~ [En] se trouve en fin de mot, car c’est aussi le cas de mots bien français "dolmen, cérumen…"
  rf"i:i,n:n($|,(?![{voyelles}])[^,]*)",
  rf"u:.,n:n($|,(?![{voyelles}])[^,]*)",
  rf"y:.,n:n($|,(?![{voyelles}])[^,]*)",
  rf"[aeiuy]:.,m:m,[bp][^,]*",
}

morphemes_barbares = { (s, re.compile(r'(^|[,;])' + s + '([,;]|$)')) for s in morphemes_barbares }

class CorrespOrthoPhono(list):
    """
    Petite classe qui représente une correspondance orthographe--phonologie,
    et l’affiche sous une forme compacte.
    Par exemple, pour l’orthographe "oiseaux" et la phonologie [wazo],
    des correspondances possibles sont (ambigüité à lever):
      "oi:wa,s:z,eau:o,x:_"
      "oi:wa,s:z,e:_,au:o,x:_"
    ce qui est une syntaxe compacte pour les listes suivantes:
      [ ('oi', 'wa'), ('s', 'z'), ('eau', 'o'), ('x', '') ]
      [ ('oi', 'wa'), ('s', 'z'), ('e', ''), ('au', 'o'), ('x', '') ]

    Cette classe contient aussi la syllabation du mot. Dans la représentation
    compacte, une coupure entre syllabes (phonologique) est marquée par un
    point-virgule ";" au lieu d’une virgule "," . Exemple:
      "a:a;pp:p,e:°;l:l,er:e"  pour "appeler" ~ [a-p°-le]
    Les lettres et autres caractères muets sont inclus dans la syllabe qui les
    précèdent. Exemple:
      "t:t,r:R,en:@,t:t,e:_,-:_;d:d,eu:2,x:_"  pour "trente-deux" ~ [tR@t-d2]

    Quand une coupure tombe au milieu d’un graphème (par exemple "ay" prononcé
    [E-i] dans "pays", "oy" prononcé [wa-j] dans "noyade", etc.), on insère ";:"
    au milieu de la prononciation; autrement dit on coupe le graphème en deux,
    le 1er pseudo-graphème contient toutes les lettres de l’orthographe
    et le 2e pseudo-graphème, dit de «continuation», ne contient aucune lettre.
    Cette convention est possible car aucun son n’est produit ex-nihilo,
    sans rien qui corresponde dans l’orthographe. Exemples:
      "p:p,ay:E;:i;s:z,an:@"  pour "paysan" ~ [pE-i-z@]
      "en:@;:n,-:_,a:a;v:v,an:@,t:_"  pour "en-avant" ~ [@-na-v@]

    NOTE: Dans Lexique 3.83, les graphèmes concernés sont:

      x:g;:z    ("exister")
      x:k;:s    ("fixer")
      xc:k;:s   ("excellent")
      xs:k;:s   ("exsuder")
      xt:k;:s   ("immixtion")

      ng:G;:g   ("bingo")
      ngu:G;:g  ("swinguer")
      en:@;:n   ("enivrer, en-avant")
      en:5;:n   ("bien-aimé")
      on:§;:n   ("non-être")

      i:i;:j    ("à priori, étrier")
      il:i;:j   ("gentilhomme")
      ill:i;:j  ("briller")
      illi:i;:j ("serpillière", ortho pré-1990)
      y:i;:j    ("écuyer")
      ay:E;:i   ("pays")
      ay:e;:i
      ay:E;:j   ("payer")
      ay:e;:j
      oill:wa;:j  ("cancoillotte")
      oï:wa;:j    ("anchoïade")
      oy:wa;:j    ("aboyer")
      oyi:waj;:j  ("aboyiez")
    """
    # On implémente cette classe simplement en enveloppant la classe `list`,
    # avec une variable d’instance supplémentaire pour la syllabation. Le champ
    # `self.sylls` contient la liste des longueurs (len_o, len_p) de chaque
    # syllabe, où `len_o` est la longueur orthographique et `len_p` la longueur
    # phonologique. Cette liste est vide si la syllabation n’est pas renseignée.
    sylls : list
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sylls = [ ]
    # Surcharge l’opérateur `+`.
    # NOTE: Suppose que la syllabation est vide.
    def __add__(self, other):
        return self.__class__(super().__add__(other))
    def __radd__(self, other):
        return self.__class__(super().__radd__(other))
    # Extrait l’orthographe ou la phonologie:
    def ortho(self):
        return "".join(o for (o,p) in self)
    def phono(self):
        return "".join(p for (o,p) in self)
    # Personnalise l’affichage avec une syntaxe compacte:
    def __repr__(self):
        return 'CorrespOrthoPhono.parse("' + self.__str__() + '")'
    def __str__(self):
        s = ""
        sylls = self.sylls or [ (len(self.ortho()), len(self.phono())) ]
        graphemes = list(reversed(self)) # pile de graphèmes, qu’on va consommer
        debut_mot = True
        # affiche chaque syllabe, séparée par ";" :
        for (len_syll_o, len_syll_p) in sylls:
            if not debut_mot:
                s += ";"
            debut_syll = True
            # affiche chaque graphème de la syllabe, séparé par "," ;
            # la syllabe se prolonge tant qu’on n’a pas le compte de lettres
            # orthographiques et le compte de sons phonologiques:
            while 0 < len_syll_o or 0 < len_syll_p:
                if not debut_syll:
                    s += ","
                (o, p) = graphemes.pop()
                # le graphème peut être coupé au milieu de sa prononciation,
                # mais pas au milieu de son orthographe:
                assert len(o) <= len_syll_o and 0 <= len_syll_p
                lp = min(len(p), len_syll_p)
                # une prononciation vide est représentée par "_" :
                s += f'{o}:{p[:lp] if lp else "_"}'
                # si on a coupé la prononciation, on remet le reste dans la pile
                # avec une orthographe vide, ainsi ce pseudo-graphème de
                # continuation commencera la syllabe suivante:
                if lp < len(p):
                    graphemes.append( ("", p[lp:]) )
                # calcule les longueurs restant à consommer:
                len_syll_o -= len(o)
                len_syll_p -= lp
                debut_syll = False
            assert 0 == len_syll_o and 0 == len_syll_p
            debut_mot = False
        assert graphemes == []
        return s
    # Réciproquement, parse la syntaxe compacte:
    @staticmethod
    def parse(s):
        corresp = CorrespOrthoPhono()
        # parse chaque syllabe:
        for syll in s.split(";"):
            len_syll_o = 0
            len_syll_p = 0
            # parse chaque graphème de la syllabe:
            for op in syll.split(","):
                (o, p) = op.split(":")
                # une prononciation vide est représentée par "_" :
                if p == "_":
                    p = ""
                assert all(ord(a) in alphabet_ortho or a in ponctuation for a in o)
                assert all(ord(a) in alphabet_phono for a in p)
                # si l’orthographe du graphème est vide, alors c’est la
                # continuation du graphème précédent, après coupure de syllabe:
                if o == "":
                    assert len(corresp) > 0
                    assert len_syll_o == 0 and len_syll_p == 0
                    assert p != ""
                    (prev_o, prev_p) = corresp[-1]
                    corresp[-1] = (prev_o, prev_p + p)
                # sinon, on ajoute ce graphème à la liste:
                else:
                    corresp.append( (o, p) )
                # calcule la longueur de la syllabe:
                len_syll_o += len(o)
                len_syll_p += len(p)
            corresp.sylls.append( (len_syll_o, len_syll_p) )
        return corresp
    # Intègre le découpage en syllabes dans le découpage en graphèmes:
    #
    # Le paramètre `sylls_phono` est la prononciation où les syllabes sont
    # séparées par des tirets (comme le champ "syll" de Lexique3).
    #
    # NOTE: le paramètre `sylls_ortho` est pour les syllabes orthographes,
    # peut-être dans le futur; pour l’instant on l’ignore.
    def integre_syllabation(self, _sylls_ortho, sylls_phono):
        sylls_phono = sylls_phono.split("-")
        # la prononciation donnée doit être égale à celle qu’on connait déjà:
        assert "".join(sylls_phono) == self.phono()
        graphemes = list(reversed(self)) # pile de graphèmes, qu’on va consommer
        # ré-initialise la syllabation (jette la valeur pré-existante):
        self.sylls = [ ]
        # pour chaque syllabe:
        for syll_p in sylls_phono:
            len_syll_p = len(syll_p)
            len_syll_o = 0
            # additionne chaque graphème de la syllabe,
            # tant qu’on n’a pas le compte de sons phonologiques:
            while 0 < len_syll_p:
                (o, p) = graphemes.pop()
                # le graphème peut être coupé au milieu de sa prononciation:
                lp = min(len(p), len_syll_p)
                # ajoute le graphème à la syllabe actuelle:
                len_syll_o += len(o)
                # si on a coupé la prononciation, on remet le reste dans la pile
                # avec une orthographe vide, ainsi ce pseudo-graphème de
                # continuation commencera la syllabe suivante:
                if lp < len(p):
                    graphemes.append( ("", p[lp:]) )
                # calcule la longueur restant à consommer:
                len_syll_p -= lp
            # la syllabe inclut aussi les lettres et autres caractères muets qui
            # suivent le dernier son:
            while graphemes != [] and graphemes[-1][1] == "":
                (o, p) = graphemes.pop()
                len_syll_o += len(o)
            self.sylls.append( (len_syll_o, len(syll_p)) )

def corresps_ortho_phono(ortho, phono, verbose=False):
    """
    Génère toutes les correspondances possibles graphèmes--phonèmes
    pour une orthographe et une phonologie données.
    Chaque correspondance est une liste de paires (graphème,phonème).
    La concaténation des graphèmes est égale à l’orthographe donnée.
    La concaténation des phonèmes est égale à la phonologie donnée.
    """
    return corresp_backtrack(ortho, phono, verbose=verbose)

def corresp_backtrack(ortho, phono, io=0, ip=0, corresp=CorrespOrthoPhono(), verbose=False):
    if io >= len(ortho) and ip >= len(phono):
        yield corresp
    else:
        if verbose:
            ident = ((1 + len(corresp)) * 4) * " "
            print(f'{ident}trying to match: {corresp}, "{ortho[io:]}" <?> [{phono[ip:]}]')
        for lp in range(min(len(phono)-ip, max_len_phoneme), -1, -1):
            p = phono[ip:ip+lp]
            if p in morphemes_par_phoneme:
                for m in re_match_all(morphemes_par_phoneme[p], ortho, pos=io):
                    o = m.group()
                    if verbose:
                        print(f'{ident}  "{o}" ~ [{p}]')
                    yield from corresp_backtrack(ortho, phono, \
                      io = m.end(), \
                      ip = ip+lp, \
                      corresp = corresp + [ (o,p) ], \
                      verbose = verbose)

def re_match_all(r, s, pos=0, endpos=None):
    """
    Génère toutes les occurrences du motif dans la chaîne de caractères
    à la position fixée donnée, par longueur décroissante.
    """
    assert type(r) == re.Pattern
    # le flag `re.POSIX` impose que le match soit le plus long possible;
    # ce flag n’est pas fourni par la lib standard `re` mais par son extension `regex`.
    assert r.flags & re.POSIX != 0
    if endpos == None:
        endpos = len(s)
    while True:
        # FIXME: `endpos` agit comme si le texte était tronqué à cette position,
        # donc les motifs '$', '\b' et les motifs lookahead sont faussés!
        m = r.match(s, pos=pos, endpos=endpos)
        if m == None:
            break
        yield m
        endpos = m.end() - 1

# TESTS

#c = CorrespOrthoPhono.parse("an:@,d:d,r:R,o:o,g:Z,y:i,n:n,e:_,s:_")
#print(c)
#print(c.sync_syllabes("an-dro-gy-nes", "@-dRo-Zin"))
#print(c.syll)
#exit(0)

#c = CorrespOrthoPhono.parse("a:a, :_,c:k,a:a,p:p,e:E,ll:l,a:a")
#print(c)
#print(c.sync_syllabes("a ca-pel-la", "a-ka-pE-la"))
#print(c.syll)
#print(c)
#exit(0)

def print_corresps(corresps):
    print("{ " + "\n| ".join(str(corr) for corr in corresps) + " }")

def test_corresps(ortho, phono):
    return print_corresps(corresps_ortho_phono(ortho, phono, verbose=True))

#test_corresps("oiseaux", "wazo")
#test_corresps("étoile", "etwal°")
#test_corresps("geai", "ZE")
#test_corresps("accueils", "ak9j")
#test_corresps("qui", "k8i")
#test_corresps("qua", "kwa")
#test_corresps("chauds", "So")
#test_corresps("babillages", "babijaZ")
#test_corresps("millions", "milj§")
#test_corresps("aquariums", "akwaRjOm")
#test_corresps("apriori", "apRijoRi")
#test_corresps("rythme", "Ritm")
#test_corresps("fier", "fjER")
#test_corresps("fier", "fje")
#test_corresps("changeaient", "S@ZE")
#test_corresps("badigoinces", "badigw5s")
#test_corresps("backgammon", "bakgamon")
#test_corresps("baguage", "bagaZ")
#test_corresps("fatiguant", "fatig@")
#test_corresps("appeaux", "apo")
#test_corresps("bacheliers", "baS2lje")
#test_corresps("bestseller", "bEstsEl9R")
#test_corresps("appellations", "apelasj§")
#test_corresps("appendicectomie", "ap5disektomi")
#test_corresps("blêmir", "blemiR")
#test_corresps("chiez", "Sje")
#test_corresps("bons-cadeaux", "b§kado")
#test_corresps("blancs-becs", "bl@bEk")
#test_corresps("boeufs", "b2")
#test_corresps("bonsaïs", "b§zaj")
#test_corresps("bonshommes", "b§zOm")
#test_corresps("carriérisme", "kaRjeRizm")
#test_corresps("amygdalectomies", "amidalEktomi")
#test_corresps("diarrhée", "djaRe")
#test_corresps("acquiesçaient", "akjEsE")
#test_corresps("adolescences", "adoles@s")
#test_corresps("acupuncteur", "akyp§kt9R")
#test_corresps("aggravées", "agRave")
#test_corresps("agnus dei", "agnusdei")
#test_corresps("août", "ut")
#test_corresps("tînt", "t5")
#test_corresps("appareillions", "apaREj§")
#test_corresps("apparemment", "apaRam@")
#test_corresps("femme", "fam")
#test_corresps("automne", "otOn")
#test_corresps("solemnel", "solanEl")
#test_corresps("anchoïade", "@Swajad")
#test_corresps("allégrement", "alEgR°m@")
#test_corresps("anarcho-syndicalistes", "anaRkos5dikalist")
#test_corresps("anti-instinct", "@ti5st5")
#test_corresps("antifascistes", "@tifaSist")
#test_corresps("antidreyfusisme", "@tidREfyzizm")
#test_corresps("anti-asthmatique", "@tiasmatik")
#test_corresps("aztèques", "astEk")
#test_corresps("aérospatiale", "aeRospasjal")
#test_corresps("aînées", "ene")
#test_corresps("bacchanale", "bakanal")
#test_corresps("aux aguets", "ozagE")
#test_corresps("auxdits", "odi")
#test_corresps("auxquels", "okEl")
#test_corresps("avantageusement", "av@taZ2z°m@")
#test_corresps("autoritairement", "OtORitER°m@")
#test_corresps("alcools", "alkOl")
#test_corresps("alcootest", "alkotEst")
#test_corresps("acid jazz", "asiddZaz")
#test_corresps("adagio", "adadZjo")
#test_corresps("appoggiatures", "apodZjatyR")
#test_corresps("aggiornamento", "aZjORnamEnto")
#test_corresps("et caetera", "etseteRa")
#test_corresps("ad vitam aeternam", "advitametERnam")
#test_corresps("admixtion", "admiksj§")
#test_corresps("accroître", "akRwatR")
#test_corresps("abstient", "apstj5")
#test_corresps("abstrait", "apstRE")
#test_corresps("absurde", "apsyRd")
#test_corresps("abbayes", "abei")
#test_corresps("accompagniez", "ak§paNe")
#test_corresps("aiguës", "egy")
#test_corresps("ambiguës", "@bigy")
#test_corresps("afrikaans", "afRik@s")
#test_corresps("akkadienne", "akadjEn")
#test_corresps("après-shampooing", "apRES@pw5")
#test_corresps("auburn", "ob9Rn") # FIXME
#test_corresps("bien-aimé", "bj5neme")
#test_corresps("bienheureusement", "bj5n2R2z°m@")
#test_corresps("bienfaisance", "bj5f°z@s")
#test_corresps("bignonia", "biNoNa")
#test_corresps("bizuth", "bizy")
#test_corresps("bretzel", "bREdzEl")
#test_corresps("c'est-à-dire", "sEtadiR")
#test_corresps("cancoillotte", "k@kwajOt")
#test_corresps("canoë", "kanoe")
#test_corresps("ce", "s2")
#test_corresps("cessez-le-feu", "sEsel°f2")
#test_corresps("champignonnière", "S@piNoNER")
#test_corresps("clowneries", "klun°Ri")
#test_corresps("couenne", "kwan")
#test_corresps("coïncider", "ko5side")
#test_corresps("dommages-intérêts", "domaZ5teRE")
#test_corresps("dommages-intérêts", "domaZz5teRE")
#test_corresps("dépaysera", "depEiz°Ra")
#test_corresps("encoignure", "@kONyR")
#test_corresps("en-avant", "@nav@")
#test_corresps("enivrèrent", "@nivRER")
#test_corresps("non-être", "n§nEtR")
#test_corresps("faisan", "f2z@")
#test_corresps("fjord", "fjORd")
#test_corresps("croque-monsieur", "kROkm°sj2")
#test_corresps("monsieur", "m°sj2")
#test_corresps("messieurs", "mEsj2")
#test_corresps("messieurs-dames", "mEsj2dam")
#test_corresps("gars", "ga")
#test_corresps("graal", "gRal")
#test_corresps("grand-angle", "gR@t@gl")
#test_corresps("haut-commandement", "okOm@d°m@")
#test_corresps("hautbois", "obwa")
#test_corresps("hawaiienne", "awajEn")
#test_corresps("impresarii", "5pResaRi")
#test_corresps("kommandantur", "kom@d@tuR")
#test_corresps("lez", "lE")
#test_corresps("magyar", "magjaR")
#test_corresps("mah-jong", "maZ§G")
#test_corresps("ping-pong", "piGp§G")
#test_corresps("moelle", "mwal")
#test_corresps("navajo", "navaRo")
#test_corresps("piedmont", "pjem§")
#test_corresps("plein-emploi", "plEn@plwa")
#test_corresps("tungstène", "t1kstEn")
#test_corresps("épidémiologique", "epidemjOlOZik")
#test_corresps("accueil", "ak9j")
#test_corresps("vermeil", "vERmEj")
#test_corresps("spoiler", "spOjlER")
#test_corresps("thaïlandais", "tajl@dE")
#test_corresps("trop-plein", "tRopl5")
#test_corresps("rougeauds", "RuZo")
#exit()
